-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 10.91.1.122    Database: wp_new_version
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.48-MariaDB-0+deb9u2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3731 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `akibat`
--

DROP TABLE IF EXISTS `akibat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `akibat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` varchar(20) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alat`
--

DROP TABLE IF EXISTS `alat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_alat` varchar(255) DEFAULT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `upt` int(11) NOT NULL,
  PRIMARY KEY (`id`,`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alat_kerja`
--

DROP TABLE IF EXISTS `alat_kerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alat_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alat_transaksi_pemeliharaan`
--

DROP TABLE IF EXISTS `alat_transaksi_pemeliharaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alat_transaksi_pemeliharaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `alat` int(11) NOT NULL,
  `berat` double DEFAULT NULL,
  `status_alat` int(11) NOT NULL,
  `keterangan` text,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `document` int(11) NOT NULL,
  `pembersihan` varchar(255) DEFAULT NULL,
  `tekanan` varchar(255) DEFAULT NULL,
  `petugas` varchar(255) DEFAULT NULL,
  `lokasi_tujuan` int(11) DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `approve_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`alat`,`status_alat`,`document`)
) ENGINE=InnoDB AUTO_INCREMENT=2297 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alat_transaksi_pemeliharaan_kelengkapan`
--

DROP TABLE IF EXISTS `alat_transaksi_pemeliharaan_kelengkapan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alat_transaksi_pemeliharaan_kelengkapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `alat` int(11) NOT NULL,
  `document` int(11) NOT NULL,
  `hanger` varchar(255) DEFAULT NULL,
  `ketinggian_apar` varchar(255) DEFAULT NULL,
  `segitiga_apar` varchar(255) DEFAULT NULL,
  `indikator_tekanan` varchar(255) DEFAULT NULL,
  `hose` varchar(255) DEFAULT NULL,
  `tabung` varchar(255) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `lokasi_tujuan` int(11) DEFAULT NULL,
  `alat_transaksi_pemeliharaan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`alat`,`document`)
) ENGINE=InnoDB AUTO_INCREMENT=2280 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `approval_apar`
--

DROP TABLE IF EXISTS `approval_apar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `approval_apar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `sub_upt` int(11) DEFAULT NULL,
  `upt` int(11) DEFAULT NULL,
  `gi` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `apps`
--

DROP TABLE IF EXISTS `apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aplikasi` varchar(45) DEFAULT NULL,
  `version` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `catatan_khusus`
--

DROP TABLE IF EXISTS `catatan_khusus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `catatan_khusus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catatan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_person`
--

DROP TABLE IF EXISTS `contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upt` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`upt`),
  KEY `fk_contact_person_upt1_idx` (`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `detail_tamu`
--

DROP TABLE IF EXISTS `detail_tamu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detail_tamu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `remarks` text,
  `tamu` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `checlist` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=291 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_document` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `doc_type` varchar(45) DEFAULT NULL,
  `verbatim` text,
  `approve_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2607 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_picture`
--

DROP TABLE IF EXISTS `document_picture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture` longtext,
  `document` int(11) NOT NULL,
  PRIMARY KEY (`id`,`document`)
) ENGINE=InnoDB AUTO_INCREMENT=3969 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `document_transaction`
--

DROP TABLE IF EXISTS `document_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  `document` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3485 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dp3`
--

DROP TABLE IF EXISTS `dp3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dp3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_dokumen` varchar(150) DEFAULT NULL,
  `edisi` varchar(45) DEFAULT NULL,
  `berlaku_efektif` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `formula_condition`
--

DROP TABLE IF EXISTS `formula_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formula_condition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keterangan` varchar(150) DEFAULT NULL,
  `bobot` double DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gardu_induk`
--

DROP TABLE IF EXISTS `gardu_induk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gardu_induk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upt` int(11) NOT NULL,
  `nama_gardu` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`upt`),
  KEY `fk_gardu_induk_upt1_idx` (`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=189 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gardu_induk_bay`
--

DROP TABLE IF EXISTS `gardu_induk_bay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gardu_induk_bay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_induk` int(11) NOT NULL,
  `bay` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`gardu_induk`),
  KEY `fk_gardu_induk_bay_gardu_induk1_idx` (`gardu_induk`)
) ENGINE=InnoDB AUTO_INCREMENT=1931 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gardu_induk_has_pegawai`
--

DROP TABLE IF EXISTS `gardu_induk_has_pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gardu_induk_has_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_induk` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`gardu_induk`,`pegawai`),
  KEY `fk_gardu_induk_has_pegawai_gardu_induk1_idx` (`gardu_induk`),
  KEY `fk_gardu_induk_has_pegawai_pegawai1_idx` (`pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gardu_induk_has_satpam`
--

DROP TABLE IF EXISTS `gardu_induk_has_satpam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gardu_induk_has_satpam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_induk` int(11) NOT NULL,
  `pegawai` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gedung`
--

DROP TABLE IF EXISTS `gedung`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gedung` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upt` int(11) NOT NULL,
  `nama_gedung` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`upt`),
  KEY `fk_gardu_induk_upt1_idx` (`upt`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `group_transaction`
--

DROP TABLE IF EXISTS `group_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `group_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(45) DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja`
--

DROP TABLE IF EXISTS `instruksi_kerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestor` int(11) DEFAULT NULL,
  `no_instruksi` varchar(150) DEFAULT NULL,
  `tanggal_instruksi` date DEFAULT NULL,
  `pekerjaan` varchar(250) DEFAULT NULL,
  `tujuan_unit` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `analisa_resiko` varchar(255) DEFAULT NULL,
  `document` int(11) DEFAULT NULL,
  `mitigasi_resiko` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_alat_kerja`
--

DROP TABLE IF EXISTS `instruksi_kerja_alat_kerja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_alat_kerja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `nama_alat` varchar(250) DEFAULT NULL,
  `satuan` varchar(250) DEFAULT NULL,
  `volume` varchar(250) DEFAULT NULL,
  `penanggung_jawab` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_analisa_resiko`
--

DROP TABLE IF EXISTS `instruksi_kerja_analisa_resiko`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_analisa_resiko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `aktifitas` varchar(250) DEFAULT NULL,
  `resiko` varchar(250) DEFAULT NULL,
  `dampak` varchar(250) DEFAULT NULL,
  `mitigasi` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_approval`
--

DROP TABLE IF EXISTS `instruksi_kerja_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `nip` varchar(150) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `remarks` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_distribution`
--

DROP TABLE IF EXISTS `instruksi_kerja_distribution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_distribution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `jabatan` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_lampiran`
--

DROP TABLE IF EXISTS `instruksi_kerja_lampiran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_lampiran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `file` varchar(250) DEFAULT NULL,
  `path` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_material`
--

DROP TABLE IF EXISTS `instruksi_kerja_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `nama_alat` varchar(250) DEFAULT NULL,
  `satuan` varchar(250) DEFAULT NULL,
  `volume` varchar(250) DEFAULT NULL,
  `penanggung_jawab` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_mitigasi_resiko`
--

DROP TABLE IF EXISTS `instruksi_kerja_mitigasi_resiko`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_mitigasi_resiko` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `bay_dikerjakan` varchar(250) DEFAULT NULL,
  `terminal_proteksi` varchar(250) DEFAULT NULL,
  `mitigasi_resiko` varchar(250) DEFAULT NULL,
  `arah_trip` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_perlengkapan`
--

DROP TABLE IF EXISTS `instruksi_kerja_perlengkapan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_perlengkapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `nama_alat` varchar(250) DEFAULT NULL,
  `satuan` varchar(250) DEFAULT NULL,
  `volume` varchar(250) DEFAULT NULL,
  `penanggung_jawab` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_tujuan`
--

DROP TABLE IF EXISTS `instruksi_kerja_tujuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_tujuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `tujuan` longtext,
  `ruang_lingkup` longtext,
  `referensi` longtext,
  `arah_trip` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  `pelaksanaan_pekerjaan` longtext,
  `tahap_persiapan` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instruksi_kerja_uraian_pekerjaan`
--

DROP TABLE IF EXISTS `instruksi_kerja_uraian_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instruksi_kerja_uraian_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `instruksi_kerja` int(11) DEFAULT NULL,
  `uraian` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jenis_alat`
--

DROP TABLE IF EXISTS `jenis_alat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `jenis_fik`
--

DROP TABLE IF EXISTS `jenis_fik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_fik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_email`
--

DROP TABLE IF EXISTS `log_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` text,
  `to` text,
  `message` text,
  `waktu_kirim` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=230498 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_error_user`
--

DROP TABLE IF EXISTS `log_error_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_error_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8387 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_fcm`
--

DROP TABLE IF EXISTS `log_fcm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_fcm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `content` text,
  `data` text,
  `to` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log_sms`
--

DROP TABLE IF EXISTS `log_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) DEFAULT NULL,
  `message` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `waktu_kirim` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedy` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=232867 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lokasi_apar`
--

DROP TABLE IF EXISTS `lokasi_apar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lokasi_apar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lokasi` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `upt` int(11) NOT NULL,
  `wilayah` int(11) NOT NULL,
  PRIMARY KEY (`id`,`upt`,`wilayah`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lokasi_penempatan`
--

DROP TABLE IF EXISTS `lokasi_penempatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lokasi_penempatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `lokasi_tujuan` int(11) NOT NULL,
  `tempat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`lokasi_tujuan`)
) ENGINE=InnoDB AUTO_INCREMENT=295 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lokasi_penempatan_alat`
--

DROP TABLE IF EXISTS `lokasi_penempatan_alat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lokasi_penempatan_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `lokasi_penempatan` int(11) NOT NULL,
  `alat` int(11) NOT NULL,
  `jenis_alat` int(11) DEFAULT NULL,
  `merk_alat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`lokasi_penempatan`,`alat`)
) ENGINE=InnoDB AUTO_INCREMENT=541 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lokasi_tujuan`
--

DROP TABLE IF EXISTS `lokasi_tujuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lokasi_tujuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `lokasi_apar` int(11) NOT NULL,
  `id_tujuan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`lokasi_apar`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `merk_alat`
--

DROP TABLE IF EXISTS `merk_alat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `merk_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_merk` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upt` int(11) NOT NULL,
  `nip` varchar(150) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `posisi_lengkap` text,
  `pegawai_type` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `file_ttd` text,
  PRIMARY KEY (`id`,`upt`),
  KEY `fk_pegawai_upt1_idx` (`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=2413 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pegawai_has_sub_upt`
--

DROP TABLE IF EXISTS `pegawai_has_sub_upt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pegawai_has_sub_upt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai` int(11) DEFAULT NULL,
  `sub_unit` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pegawai_type`
--

DROP TABLE IF EXISTS `pegawai_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pegawai_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(255) DEFAULT NULL,
  `context` varchar(100) DEFAULT NULL,
  `term_id` varchar(100) DEFAULT NULL,
  `context_id` varchar(100) DEFAULT NULL,
  `parent_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `peluang`
--

DROP TABLE IF EXISTS `peluang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `peluang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` varchar(20) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pemaparan`
--

DROP TABLE IF EXISTS `pemaparan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pemaparan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nilai` varchar(20) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pemohon`
--

DROP TABLE IF EXISTS `pemohon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pemohon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text,
  `nama_pemohon` varchar(255) DEFAULT NULL,
  `perusahaan` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `alamat` text,
  `no_telp` varchar(45) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9633 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pengajuan_vendor`
--

DROP TABLE IF EXISTS `pengajuan_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengajuan_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_pengajuan` varchar(150) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `nama_vendor` varchar(255) DEFAULT NULL,
  `pimpinan` varchar(255) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2289 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pengendalian`
--

DROP TABLE IF EXISTS `pengendalian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pengendalian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengendalian` text,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit`
--

DROP TABLE IF EXISTS `permit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_wp` varchar(150) DEFAULT NULL,
  `user` int(11) NOT NULL,
  `pemohon` int(11) NOT NULL,
  `tanggal_wp` date DEFAULT NULL,
  `tipe_permit` int(11) NOT NULL,
  `file_spk` text,
  `penanggung_jawab_pekerjaan` varchar(255) DEFAULT NULL,
  `jabatan_penanggung_jawab` varchar(255) DEFAULT NULL,
  `pengawas_k3` varchar(255) DEFAULT NULL,
  `jabatan_pengawas_k3` varchar(255) DEFAULT NULL,
  `pengawas_pekerjaan` varchar(255) DEFAULT NULL,
  `jabatan_pengawas_pekerjaan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `file_sld` text,
  `file_lokasi_kerja` text,
  PRIMARY KEY (`id`,`user`,`pemohon`,`tipe_permit`),
  KEY `fk_permit_user1_idx` (`user`),
  KEY `fk_permit_tipe_permit1_idx` (`tipe_permit`),
  KEY `fk_permit_pemohon1_idx` (`pemohon`)
) ENGINE=InnoDB AUTO_INCREMENT=63148 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_apd`
--

DROP TABLE IF EXISTS `permit_apd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_apd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `nama_alat` varchar(255) DEFAULT NULL,
  `ketersediaan` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `kondisi_baik` varchar(100) DEFAULT NULL,
  `kondisi_rusak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_apd_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=451655 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_attandance`
--

DROP TABLE IF EXISTS `permit_attandance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_attandance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pekerja` varchar(255) DEFAULT NULL,
  `hadir` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `permit` int(11) DEFAULT NULL,
  `document` int(11) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1479 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_category`
--

DROP TABLE IF EXISTS `permit_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_cetak`
--

DROP TABLE IF EXISTS `permit_cetak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_cetak` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `permit` int(11) NOT NULL,
  `tanggal_cetak` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`user`,`permit`),
  KEY `fk_permit_cetak_permit1_idx` (`permit`),
  KEY `fk_permit_cetak_user1_idx` (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_file`
--

DROP TABLE IF EXISTS `permit_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_sk3` text,
  `fle_spk` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_category`
--

DROP TABLE IF EXISTS `permit_has_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_category` (
  `id` int(11) NOT NULL,
  `permit` int(11) NOT NULL,
  `permit_category` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`permit_category`),
  KEY `fk_permit_has_category_permit1_idx` (`permit`),
  KEY `fk_permit_has_category_permit_category1_idx` (`permit_category`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_fik`
--

DROP TABLE IF EXISTS `permit_has_fik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_fik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `jenis_fik` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`jenis_fik`),
  KEY `fk_permit_has_fik_permit1_idx` (`permit`),
  KEY `fk_permit_has_fik_jenis_fik1_idx` (`jenis_fik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_kelayakan_apd`
--

DROP TABLE IF EXISTS `permit_has_kelayakan_apd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_kelayakan_apd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `body_harnes` varchar(45) DEFAULT NULL,
  `lanyard` varchar(45) DEFAULT NULL,
  `hook_or_step` varchar(45) DEFAULT NULL,
  `pullstrap` varchar(45) DEFAULT NULL,
  `safety_helmet` varchar(45) DEFAULT NULL,
  `kacamata_uv` varchar(45) DEFAULT NULL,
  `earplug` varchar(45) DEFAULT NULL,
  `masker` varchar(45) DEFAULT NULL,
  `wearpack` varchar(45) DEFAULT NULL,
  `sepatu_safety` varchar(45) DEFAULT NULL,
  `sarung_tangan` varchar(45) DEFAULT NULL,
  `stick_grounding` varchar(45) DEFAULT NULL,
  `grounding_cable` varchar(45) DEFAULT NULL,
  `stick_isolasi` varchar(45) DEFAULT NULL,
  `voltage_detector` varchar(45) DEFAULT NULL,
  `scaffolding` varchar(45) DEFAULT NULL,
  `tambang` varchar(45) DEFAULT NULL,
  `tagging` varchar(45) DEFAULT NULL,
  `pembatas_area` varchar(45) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `permit_transaction_log` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_has_kelayakan_apd_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=4048 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_safety_cond`
--

DROP TABLE IF EXISTS `permit_has_safety_cond`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_safety_cond` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `kondisi_tanah` varchar(150) DEFAULT NULL,
  `kondisi_lingkungan` varchar(150) DEFAULT NULL,
  `kondisi_cuaca` varchar(45) DEFAULT NULL,
  `ketinggian_tk` varchar(45) DEFAULT NULL,
  `tegangan_tk` varchar(45) DEFAULT NULL,
  `lokasi` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `permit_transaction_log` int(11) DEFAULT NULL,
  `jenis_pekerjaan` varchar(255) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `nilai_apd` double DEFAULT NULL,
  `resiko_pekerjaan` varchar(255) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_has_safety_cond_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=4048 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_sidak_detail_pict`
--

DROP TABLE IF EXISTS `permit_has_sidak_detail_pict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_sidak_detail_pict` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit_transaction_log` int(11) NOT NULL,
  `picture` longtext,
  PRIMARY KEY (`id`,`permit_transaction_log`),
  KEY `fk_permit_has_sidak_detail_pict_permit_transaction_log1_idx` (`permit_transaction_log`)
) ENGINE=InnoDB AUTO_INCREMENT=13753 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_swa`
--

DROP TABLE IF EXISTS `permit_has_swa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_swa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `nama_swa` varchar(255) DEFAULT NULL,
  `email_swa` text,
  `lokasi_pekerjaan` text,
  `uraian_pekerjaan` text,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `permit_transaction_log` int(11) DEFAULT NULL,
  `tipe` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_has_swa_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=2759 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_swa_kelengkapan`
--

DROP TABLE IF EXISTS `permit_has_swa_kelengkapan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_swa_kelengkapan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `wp` varchar(45) DEFAULT NULL,
  `sop` varchar(45) DEFAULT NULL,
  `jsa` varchar(45) DEFAULT NULL,
  `pengawas_k3` varchar(45) DEFAULT NULL,
  `sertifikat_kom` varchar(45) DEFAULT NULL,
  `peralatan` varchar(45) DEFAULT NULL,
  `rambu_k3` varchar(45) DEFAULT NULL,
  `apd` varchar(45) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `permit_transaction_log` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_has_swa_kelengkapan_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=2759 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_has_swa_pekerjaan`
--

DROP TABLE IF EXISTS `permit_has_swa_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_has_swa_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `jam_pekerjaan_dihentikan` varchar(45) DEFAULT NULL,
  `jam_pekerjaan_dilanjutkan` varchar(45) DEFAULT NULL,
  `ket_pekerjaan_dihentikan` text,
  `temuan_1` varchar(155) DEFAULT NULL,
  `jenis_temuan_1` varchar(155) DEFAULT NULL,
  `temuan_2` varchar(155) DEFAULT NULL,
  `jenis_temuan_2` varchar(155) DEFAULT NULL,
  `rekomendasi` text,
  `nama_pengawas_k3` varchar(155) DEFAULT NULL,
  `nama_perusahaan` varchar(255) DEFAULT NULL,
  `ttd_k3` longtext,
  `ttd_swa` longtext,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `ket_melanjutkan_pekerjaan` varchar(255) DEFAULT NULL,
  `status_temuan_1` varchar(100) DEFAULT NULL,
  `status_temuan_2` varchar(100) DEFAULT NULL,
  `permit_transaction_log` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_has_swa_pekerjaan_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=2759 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_ibppr`
--

DROP TABLE IF EXISTS `permit_ibppr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_ibppr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `kegiatan` text,
  `potensi_bahaya` text,
  `resiko` text,
  `akibat` int(11) DEFAULT NULL,
  `paparan` int(11) DEFAULT NULL,
  `peluang` int(11) DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  `tingkat_resiko` varchar(45) DEFAULT NULL,
  `pengendalian` text,
  `akibat_after` int(11) DEFAULT NULL,
  `paparan_after` int(11) DEFAULT NULL,
  `peluang_after` int(11) DEFAULT NULL,
  `nilai_after` double DEFAULT NULL,
  `tingkat_resiko_after` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `pihak` varchar(255) DEFAULT NULL,
  `pr` varchar(100) DEFAULT NULL,
  `kon` varchar(100) DEFAULT NULL,
  `kem` varchar(100) DEFAULT NULL,
  `tr` varchar(100) DEFAULT NULL,
  `kon_after` varchar(100) DEFAULT NULL,
  `kem_after` varchar(100) DEFAULT NULL,
  `tr_after` varchar(100) DEFAULT NULL,
  `status_pengendalian` varchar(100) DEFAULT NULL,
  `penanggung_jawab` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_ibppr_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=263882 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_jsa`
--

DROP TABLE IF EXISTS `permit_jsa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_jsa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `no_jsa` varchar(150) DEFAULT NULL,
  `judul_jsa` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_jsa_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=59790 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_jsa_item`
--

DROP TABLE IF EXISTS `permit_jsa_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_jsa_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit_jsa` int(11) NOT NULL,
  `tahapan_pekerjaan` varchar(255) DEFAULT NULL,
  `potensi_bahaya` varchar(255) DEFAULT NULL,
  `pengendalian` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit_jsa`),
  KEY `fk_permit_jsa_item_permit_jsa1_idx` (`permit_jsa`)
) ENGINE=InnoDB AUTO_INCREMENT=268469 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_jsa_item_template`
--

DROP TABLE IF EXISTS `permit_jsa_item_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_jsa_item_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahapan_pekerjaan` text,
  `potensi_bahaya` text,
  `pengendalian` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `document` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_pelaksana`
--

DROP TABLE IF EXISTS `permit_pelaksana`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_pelaksana` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `permit` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_pelaksana_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=292080 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_penanggung_jawab`
--

DROP TABLE IF EXISTS `permit_penanggung_jawab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_penanggung_jawab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `file_sk3` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_penanggung_jawab_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=68877 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_pengawas`
--

DROP TABLE IF EXISTS `permit_pengawas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_pengawas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_pengawas_permit1_idx` (`permit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_pic`
--

DROP TABLE IF EXISTS `permit_pic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `jabatan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`),
  KEY `fk_permit_pic_permit1_idx` (`permit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_purpose`
--

DROP TABLE IF EXISTS `permit_purpose`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_purpose` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `upt` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`upt`),
  KEY `fk_permit_purpose_permit1_idx` (`permit`),
  KEY `fk_permit_purpose_upt1_idx` (`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=63148 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_risk`
--

DROP TABLE IF EXISTS `permit_risk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_risk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `risk_management` int(11) NOT NULL,
  `foto_breafing` text,
  `berita_acara` text,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`risk_management`),
  KEY `fk_permit_risk_permit1_idx` (`permit`),
  KEY `fk_permit_risk_risk_management1_idx` (`risk_management`)
) ENGINE=InnoDB AUTO_INCREMENT=11197 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_single_line`
--

DROP TABLE IF EXISTS `permit_single_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_single_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `single_line` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`single_line`),
  KEY `fk_permit_single_line_single_line1_idx` (`single_line`),
  KEY `fk_permit_single_line_permit1_idx` (`permit`)
) ENGINE=InnoDB AUTO_INCREMENT=477 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_sld`
--

DROP TABLE IF EXISTS `permit_sld`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_sld` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `sld` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`sld`),
  KEY `fk_permit_sld_permit1_idx` (`permit`),
  KEY `fk_permit_sld_sld1_idx` (`sld`)
) ENGINE=InnoDB AUTO_INCREMENT=3004 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_status`
--

DROP TABLE IF EXISTS `permit_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `permit` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nAPPROVE\nREJECT',
  `level` int(11) DEFAULT '0',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`user`,`permit`),
  KEY `fk_permit_status_permit1_idx` (`permit`),
  KEY `fk_permit_status_user1_idx` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=253958 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_transaction_log`
--

DROP TABLE IF EXISTS `permit_transaction_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_transaction_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_trans` varchar(150) DEFAULT NULL,
  `permit` int(11) NOT NULL,
  `group_transaction` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`permit`,`group_transaction`),
  KEY `fk_permit_transaction_log_permit1_idx` (`permit`),
  KEY `fk_permit_transaction_log_group_transaction1_idx` (`group_transaction`)
) ENGINE=InnoDB AUTO_INCREMENT=6837 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_verifikasi`
--

DROP TABLE IF EXISTS `permit_verifikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_verifikasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hal` varchar(45) DEFAULT NULL,
  `ttd_k3` text,
  `ttd_penanggung_jawab` text,
  `document` int(11) DEFAULT NULL,
  `permit` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `kondisi_apd` longtext,
  `checklist_aktifitas` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6449 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permit_work`
--

DROP TABLE IF EXISTS `permit_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permit_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permit` int(11) NOT NULL,
  `work_place` int(11) NOT NULL,
  `place` int(11) DEFAULT NULL COMMENT 'ID SUB UNIT\nATAU\nID UNIT\nATAU\nID GARDU INDUK\nATAU\nID SUTET',
  `tgl_pekerjaan` date DEFAULT NULL,
  `lokasi_pekerjaan` varchar(255) DEFAULT NULL,
  `tgl_awal` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  `uraian_pekerjaan` text,
  `is_need_sistem` int(11) DEFAULT '0',
  `keterangan_need_sistem` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`permit`,`work_place`),
  KEY `fk_permit_work_permit1_idx` (`permit`),
  KEY `fk_permit_work_work_place1_idx` (`work_place`)
) ENGINE=InnoDB AUTO_INCREMENT=63148 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `potensi_bahaya`
--

DROP TABLE IF EXISTS `potensi_bahaya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `potensi_bahaya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `potensi` text,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `risk_management`
--

DROP TABLE IF EXISTS `risk_management`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `risk_management` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_apps_akses`
--

DROP TABLE IF EXISTS `role_apps_akses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_apps_akses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `apps` int(11) NOT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `approveby` int(11) DEFAULT NULL,
  `rejectedby` int(11) DEFAULT NULL,
  `keterangan_reject` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`,`user`,`apps`),
  KEY `fk_role_apps_akses_user_idx` (`user`),
  KEY `fk_role_apps_akses_apps1_idx` (`apps`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `role_apps_apar`
--

DROP TABLE IF EXISTS `role_apps_apar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_apps_apar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_apps_akses` int(11) NOT NULL,
  `wilayah` int(11) NOT NULL,
  PRIMARY KEY (`id`,`role_apps_akses`,`wilayah`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hak_akses` varchar(250) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `routing_approval_ika`
--

DROP TABLE IF EXISTS `routing_approval_ika`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routing_approval_ika` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upt` int(11) DEFAULT NULL,
  `nip` varchar(150) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `deleted` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `simson_training`
--

DROP TABLE IF EXISTS `simson_training`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simson_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pekerjaan` text,
  `bin` varchar(45) DEFAULT NULL,
  `kondisi_tanah` text,
  `nilai_kondisi_tanah` double DEFAULT NULL,
  `bin_kondisi_tanah` varchar(25) DEFAULT NULL,
  `kondisi_lingkungan` text,
  `nilai_kondisi_lingkungan` double DEFAULT NULL,
  `bin_kondisi_lingkungan` varchar(45) DEFAULT NULL,
  `kondisi_cuaca` text,
  `nilai_kondisi_cuaca` double DEFAULT NULL,
  `bin_kondisi_cuaca` varchar(45) DEFAULT NULL,
  `ketinggian_tk` text,
  `nilai_ketinggian_tk` double DEFAULT NULL,
  `bin_ketinggian_tk` varchar(45) DEFAULT NULL,
  `tegangan_tk` text,
  `nilai_tegangan_tk` double DEFAULT NULL,
  `bin_tegangan_tk` varchar(45) DEFAULT NULL,
  `lokasi_tk` text,
  `nilai_lokasi_tk` double DEFAULT NULL,
  `bin_lokasi_tk` varchar(45) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `status_kondisi` varchar(45) DEFAULT NULL,
  `address_bin` text,
  `nilai_true_body_harnes` double DEFAULT NULL,
  `nilai_false_body_harnes` double DEFAULT NULL,
  `nilai_true_lanyard` double DEFAULT NULL,
  `nilai_false_lanyard` double DEFAULT NULL,
  `nilai_true_hook` double DEFAULT NULL,
  `nilai_false_hook` double DEFAULT NULL,
  `nilai_true_pullstrap` double DEFAULT NULL,
  `nilai_false_pullstrap` double DEFAULT NULL,
  `nilai_true_safety_helmet` double DEFAULT NULL,
  `nilai_false_safety_helmet` double DEFAULT NULL,
  `nilai_true_kacamata` double DEFAULT NULL,
  `nilai_false_kacamata` double DEFAULT NULL,
  `nilai_true_earplug` double DEFAULT NULL,
  `nilai_false_earplug` double DEFAULT NULL,
  `nilai_true_masker` double DEFAULT NULL,
  `nilai_false_masker` double DEFAULT NULL,
  `nilai_true_wearpack` double DEFAULT NULL,
  `nilai_false_wearpack` double DEFAULT NULL,
  `nilai_true_sepatu` double DEFAULT NULL,
  `nilai_false_sepatu` double DEFAULT NULL,
  `nilai_true_sarungtangan` double DEFAULT NULL,
  `nilai_false_sarungtangan` double DEFAULT NULL,
  `nilai_true_stick` double DEFAULT NULL,
  `nilai_false_stick` double DEFAULT NULL,
  `nilai_true_grounding` double DEFAULT NULL,
  `nilai_false_grounding` double DEFAULT NULL,
  `nilai_true_isolasi` double DEFAULT NULL,
  `nilai_false_isolasi` double DEFAULT NULL,
  `nilai_true_voltage` double DEFAULT NULL,
  `nilai_false_voltage` double DEFAULT NULL,
  `nilai_true_tangga` double DEFAULT NULL,
  `nilai_false_tangga` double DEFAULT NULL,
  `nilai_true_tambang` double DEFAULT NULL,
  `nilai_false_tambang` double DEFAULT NULL,
  `nilai_true_tagging` double DEFAULT NULL,
  `nilai_false_tagging` double DEFAULT NULL,
  `nilai_true_kerja` double DEFAULT NULL,
  `nilai_false_kerja` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12545 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `single_line`
--

DROP TABLE IF EXISTS `single_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `single_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sld`
--

DROP TABLE IF EXISTS `sld`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sld` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) DEFAULT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` date DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_alat`
--

DROP TABLE IF EXISTS `status_alat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_alat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `craetedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `status_pengajuan_vendor`
--

DROP TABLE IF EXISTS `status_pengajuan_vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status_pengajuan_vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `pengajuan_vendor` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nAPPROVE\nREJECT',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`pengajuan_vendor`),
  KEY `fk_status_pengajuan_vendor_pengajuan_vendor1_idx` (`pengajuan_vendor`)
) ENGINE=InnoDB AUTO_INCREMENT=3293 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `struktur_approval`
--

DROP TABLE IF EXISTS `struktur_approval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `struktur_approval` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `upt` int(11) NOT NULL,
  `tipe_permit` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `ttd` text,
  `is_last` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`upt`,`tipe_permit`,`user`),
  KEY `fk_struktur_approval_user1_idx` (`user`),
  KEY `fk_struktur_approval_tipe_permit1_idx` (`tipe_permit`),
  KEY `fk_struktur_approval_upt1_idx` (`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `struktur_approval_has_sub_upt`
--

DROP TABLE IF EXISTS `struktur_approval_has_sub_upt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `struktur_approval_has_sub_upt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `struktur_approval` int(11) NOT NULL,
  `sub_upt` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`struktur_approval`,`sub_upt`),
  KEY `fk_struktur_approval_has_sub_upt_struktur_approval1_idx` (`struktur_approval`),
  KEY `fk_struktur_approval_has_sub_upt_sub_upt1_idx` (`sub_upt`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `struktur_approval_paraf`
--

DROP TABLE IF EXISTS `struktur_approval_paraf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `struktur_approval_paraf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `struktur_approval` int(11) NOT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`struktur_approval`),
  KEY `fk_struktur_approval_paraf_struktur_approval1_idx` (`struktur_approval`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sub_upt`
--

DROP TABLE IF EXISTS `sub_upt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_upt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `upt` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`upt`),
  KEY `fk_sub_upt_upt1_idx` (`upt`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sub_upt_has_gardu`
--

DROP TABLE IF EXISTS `sub_upt_has_gardu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_upt_has_gardu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_upt` int(11) NOT NULL,
  `gardu_induk` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`sub_upt`,`gardu_induk`),
  KEY `fk_sub_upt_has_gardu_sub_upt1_idx` (`sub_upt`),
  KEY `fk_sub_upt_has_gardu_gardu_induk1_idx` (`gardu_induk`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sutet`
--

DROP TABLE IF EXISTS `sutet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sutet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gardu_induk` int(11) NOT NULL,
  `nama` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`gardu_induk`),
  KEY `fk_sutet_gardu_induk1_idx` (`gardu_induk`)
) ENGINE=InnoDB AUTO_INCREMENT=451 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tahapan_pekerjaan`
--

DROP TABLE IF EXISTS `tahapan_pekerjaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tahapan_pekerjaan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahapan` text,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tamu`
--

DROP TABLE IF EXISTS `tamu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tamu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text,
  `lokasi_tujuan` text,
  `bertemu` varchar(200) DEFAULT NULL,
  `keperluan` text,
  `tanggal_bertemu` date DEFAULT NULL,
  `jam_bertemu` varchar(200) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `approve_date` datetime DEFAULT NULL,
  `approve_user` varchar(200) DEFAULT NULL,
  `reason_reject` varchar(255) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `jumlah_tamu` int(11) DEFAULT NULL,
  `foto` text,
  `path` varchar(255) DEFAULT NULL,
  `email` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipe_permit`
--

DROP TABLE IF EXISTS `tipe_permit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipe_permit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipe` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ultg_pegawai`
--

DROP TABLE IF EXISTS `ultg_pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ultg_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `sub_upt` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `upt`
--

DROP TABLE IF EXISTS `upt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text,
  `password` varchar(45) DEFAULT NULL,
  `pegawai` int(11) DEFAULT NULL,
  `vendor` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `player_id` text,
  `roles` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2404 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_type_swa`
--

DROP TABLE IF EXISTS `user_type_swa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type_swa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `tipe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendor`
--

DROP TABLE IF EXISTS `vendor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengajuan_vendor` int(11) DEFAULT NULL,
  `nama_vendor` varchar(255) DEFAULT NULL,
  `pimpinan` varchar(255) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wilayah`
--

DROP TABLE IF EXISTS `wilayah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wilayah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_wilayah` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `work_place`
--

DROP TABLE IF EXISTS `work_place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'wp_new_version'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-20 13:59:50
