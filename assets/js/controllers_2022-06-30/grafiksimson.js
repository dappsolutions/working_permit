var GrafikSimson = {
	module: function () {
		return "grafiksimson";
	},

	setGrafik: function () {
		var bar = new Morris.Bar({
			element: 'bar-chart-simson',
			resize: true,
			data: JSON.parse($('input#data_wp_simson').val()),
			barColors: ['#49a65b'],
			xkey: 'y',
			ykeys: ['a'],
			ymin: 0,
			labels: ['Jumlah'],
			hideHover: 'auto'
		});
	},


	// changeFilter: function (elm, module = '') {
	//  var year = $("#tahun").val();
	//  var month = $("#month").val();
	// 	let upt = $('#upt').val();
	// 	if(module != ''){
	// 		window.location.href = url.base_url(module) + `index?year=${year}&month=${month}&upt=${upt}`;
	// 	}else{
	// 		window.location.href = url.base_url(GrafikSimson.module()) + `index?year=${year}&month=${month}&upt=${upt}`;
	// 	}
	// }
	changeFilter: function (elm, module = '') {
  let params = {};
		params.year = $("#tahun-simson").val();
		params.month = $("#month-simson").val();
		params.upt = $('#upt-simson').val();
  let uptLabel = params.upt == '' ? '-' : $.trim($('#upt-simson').find(`option[value="${params.upt}"]`).text());

  
		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'json',
			url: url.base_url(GrafikSimson.module()) + "changeFilter",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Pengambilan Data...");
			},

			success: function (resp) {
				message.closeLoading();
    $('input#data_wp_simson').val(resp.data_wp_simson);
    $('#bar-chart-simson').html('');
    GrafikSimson.setGrafik();

    let htmlText = `<i><b>${params.year}</b> <b>${params.month}</b> ${uptLabel}</b></i>`;
    $('#term-label-simson').html(htmlText);
			}
		});
	}
};

$(function () {
//	GrafikSimson.setGrafik();
GrafikSimson.changeFilter();
});
