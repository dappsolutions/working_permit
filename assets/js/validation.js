var validation = {
 run: function () {
		console.log('validation');
  $('.data-error').remove();
  var required = $('.required');
  var empty = 0;
  var is_valid = 1;  
  
  $.each(required, function () {
   var value = $(this).val();      
   if (value == '') {
    empty += 1;
    $(this).after('<p style="color:red" class="data-error">* ' + $(this).attr('error') + ' Harus Diisi</p>');
   }
  });

  if (empty > 0) {
   is_valid = 0;
  }

  return is_valid;
 },

 runWithElement: function (elm) {
		console.log('elmData',elm);
  elm.find('.data-error').remove();
  var required = elm.find('.required');
  var empty = 0;
  var is_valid = 1;  
  
  $.each(required, function () {
   var value = $(this).val();      
   if (value == '') {
    empty += 1;
    $(this).after('<p style="color:red" class="data-error">* ' + $(this).attr('error') + ' Harus Diisi</p>');
   }
  });

  if (empty > 0) {
   is_valid = 0;
  }

  return is_valid;
 }
};
