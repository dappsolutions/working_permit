var GrafikSwa = {
 module: function () {
  return "grafikswa";
 },

 setGrafik: function () {
  var bar = new Morris.Bar({
   element: 'bar-chart',
   resize: true,
   data: JSON.parse($('input#data_wp').val()),
   barColors: ['#00a2b9'],
   xkey: 'y',
   ykeys: ['a'],
   ymin: 0,
   labels: ['Jumlah'],
   hideHover: 'auto'
  });
 },


 changeFilter: function (elm, module = '') {
  var year = $("#tahun").val();
  var month = $("#month").val();
		let upt = $('#upt').val();
		let pelaksana = $('#pelaksana').val();
		if(module != ''){
			window.location.href = url.base_url(module) + `index?year=${year}&month=${month}&upt=${upt}&pelaksana=${pelaksana}`;
		}else{
			window.location.href = url.base_url(GrafikSwa.module()) + `index?year=${year}&month=${month}&upt=${upt}&pelaksana=${pelaksana}`;
		}
 }
};

$(function () {
 GrafikSwa.setGrafik();
});

