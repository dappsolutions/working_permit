var Pekerjaan = {
 module: function () {
  return "pekerjaan";
 },

 getDataPekerjaan: function () {

  var date = $('#default_date').length == 1 ? $('#default_date').val() : '';
  var id_place = $('#id_place').length == 1 ? $('#id_place').val() : '';
  var upt = $('#upt').length == 1 ? $('#upt').val() : '';
  $.ajax({
   type: 'POST',
   data: {
    date: date,
    id_place: id_place,
    upt: upt,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Pekerjaan.module()) + "getCurrentEventPermit",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    var data = resp.data;
    var event = [];

    $('#calendar').fullCalendar('removeEventSources', null);
    for (var i = 0; i < data.length; i++) {
     var value = data[i];
     var month = data[i].month;
     var year = data[i].year;
     var day = data[i].day;
     var tgl_pekerjaan = data[i].tgl_pekerjaan;
     var status = data[i].status;
     event.push({
      title: '\n Ada Pekerjaan \n\n\n',
      start: new Date(year, month, day),
      backgroundColor: '#00a65a',
      borderColor: '#00a65a',
      url: url.base_url("pekerjaan") + "view/" + tgl_pekerjaan + '/' + upt + '/' + id_place,
     });
    }

    $('#calendar').fullCalendar('addEventSource', event);
   }
  });
 },

 setDateData: function () {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  console.log(y + " dan " + m + " dan " + d);

  $('#calendar').fullCalendar({
   defaultDate: $('input#default_date').val(),
   header: {
    left: 'prev,next today',
    center: 'title',
//    right: 'month,agendaWeek,agendaDay'
    right: 'month,'
   },
   buttonText: {
    today: 'today',
    month: 'month',
    week: 'week',
    day: 'day'
   },
   events: [],
  });

  Pekerjaan.getDataPekerjaan();
 },

 updateCalendarEvent: function (date) {
  var id_place = $('#id_place').val();
  var upt = $('#upt').val();
  $.ajax({
   type: 'POST',
   data: {
    date: date,
    id_place: id_place,
    upt: upt
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Pekerjaan.module()) + "getDataPermit",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    var data = resp.data;
    var event = [];

    $('#calendar').fullCalendar('removeEventSources', null);
    for (var i = 0; i < data.length; i++) {
     var value = data[i];
     var month = data[i].month;
     var year = data[i].year;
     var day = data[i].day;
     var tgl_pekerjaan = data[i].tgl_pekerjaan;
     var status = data[i].status;
     event.push({
      title: '\n Ada Pekerjaan \n\n\n',
      start: new Date(year, month, day),
      backgroundColor: '#00a65a',
      borderColor: '#00a65a',
      url: url.base_url("pekerjaan") + "view/" + tgl_pekerjaan + '/' + upt + '/' + id_place,
     });
    }

    $('#calendar').fullCalendar('addEventSource', event);
   }
  });
 },

 detail: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Pekerjaan.module()) + "detailPermit/" + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
 
 expanSidebar: function(){
  var sidebar = $('a.sidebar-toggle');
  sidebar.trigger('click');
 }
};


$(function () {
 Pekerjaan.expanSidebar();
 Pekerjaan.setDateData();

 $('.fc-corner-right[aria-label="next"]').click(function () {
  var date = $('.fc-center').find('h2');
  var date = date.text();
  Pekerjaan.updateCalendarEvent(date);
 });

 $('.fc-corner-left[aria-label="prev"]').click(function () {
  var date = $('.fc-center').find('h2');
  var date = date.text();
  Pekerjaan.updateCalendarEvent(date);
 });

});