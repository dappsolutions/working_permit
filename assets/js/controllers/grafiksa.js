var GrafikSa = {
 module: function () {
  return "grafikswa";
 },

 setGrafik: function () {
  var bar = new Morris.Bar({
   element: 'bar-chart-sa',
   resize: true,
   data: JSON.parse($('input#data_wp_sa').val()),
   barColors: ['#00a2b9'],
   xkey: 'y',
   ykeys: ['a'],
   ymin: 0,
   labels: ['Jumlah'],
   hideHover: 'auto'
  });
 },


 changeFilter: function (elm, module = '') {
  var year = $("#tahun-sa").val();
  var month = $("#month-sa").val();
		let upt = $('#upt-sa').val();
		let pelaksana = $('#pelaksana-sa').val();
		if(module != ''){
			window.location.href = url.base_url(module) + `index?year=${year}&month=${month}&upt=${upt}&pelaksana=${pelaksana}&tipe=SA`;
		}else{
			window.location.href = url.base_url(GrafikSa.module()) + `index?year=${year}&month=${month}&upt=${upt}&pelaksana=${pelaksana}&tipe=SA`;
		}
 }
};

$(function () {
 GrafikSa.setGrafik();
});

