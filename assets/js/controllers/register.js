var Register = {
 module: function () {
  return 'register';
 },

 register: function (elm, e) {
  e.preventDefault();

  var nama = $('#nama').val();
  var pimpinan = $('#pimpinan').val();
  var email = $('#email').val();
  var no_hp = $('#no_hp').val();

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     nama_vendor: nama,
     pimpinan: pimpinan,
     email: email,
     no_hp: no_hp,
    },
    dataType: 'json',
//     async: false,
    url: url.base_url(Register.module()) + 'registerVendor',
    error: function () {
//     message.error('.message', 'Register Gagal, Terjadi Error di Server');
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Registrasi Vendor...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Register Berhasil");
      setTimeout(window.location.reload(), 1000);
     } else {
//      message.error('.message', 'Username atau Password Tidak Valid');
      toastr.error("Gagal Registrasi");
     }
     message.closeLoading();
    }
   });
  }
 },

 searchInTableContent: function (elm, e) {
  Register.searchInTable($(elm).val(), '#tb_content');
 },

 searchInTable: function (value, elm) {
  $("" + elm + " tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 setDataTable: function () {
  $('#tb_content').DataTable({
   'paging': true,
   'lengthChange': true,
   'searching': true,
   'ordering': true,
   'info': false,
   'autoWidth': false
  })
 },

};

$(function () {
 Register.setDataTable();
});
