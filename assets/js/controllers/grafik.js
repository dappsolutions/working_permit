var Grafik = {
 module: function () {
  return "grafik";
 },

 setGrafik: function () {
  var bar = new Morris.Bar({
   element: 'bar-chart',
   resize: true,
   data: JSON.parse($('input#data_wp').val()),
   barColors: ['#00a2b9', '#f39c12'],
   xkey: 'y',
   ykeys: ['a', 'b'],
   ymin: 0,
   labels: ['Internal', 'Eksternal'],
   hideHover: 'auto'
  });
 },

 changeYear: function (elm) {
  var year = $('#tahun').val();
  var upt = $('#upt').val();
  if(upt == ''){
	  window.location.href = url.base_url(Grafik.module()) + "index/" + year;
  }else{
	  window.location.href = url.base_url(Grafik.module()) + "index/" + year+'/'+upt;
  }
 }
};

$(function () {
 Grafik.setGrafik();
});