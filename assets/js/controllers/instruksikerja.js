let editor = null;
let editorMitigasiNew = null;
let editorTujuan = null;
let editorRuangLingkup = null;
let editorReferensi = null;
let editorArahTrip = null;
let editorPelaksanaanPekerjaan = null;
let editorSubPelaksanaanPekerjaan = null;
let editorTahapanPekerjaan = null;
var InstruksiKerja = {
	module: function () {
		return "instruksikerja";
	},

	moduleWpEks: function () {
		return "wpeksternal";
	},

	add: function () {
		window.location.href = url.base_url(InstruksiKerja.module()) + "add";
	},

	main: function () {
		window.location.href = url.base_url(InstruksiKerja.module()) + "index";
	},

	back: function () {
		window.location.href = url.base_url(InstruksiKerja.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != "") {
				window.location.href =
					url.base_url(InstruksiKerja.module()) + "search" + "/" + keyWord;
			} else {
				window.location.href = url.base_url(InstruksiKerja.module()) + "index";
			}
		}
	},

	getPostDistribusi: function () {
		var data = [];
		var table_data = $("table#table_distribution").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				jabatan: $(this).find("input#jabatan").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostAnalisaResiko: function () {
		var data = [];
		var table_data = $("table#table_analisa_resiko").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				aktifitas: $(this).find("input#aktifitas").val(),
				resiko: $(this).find("input#resiko").val(),
				dampak: $(this).find("input#dampak").val(),
				mitigasi: $(this).find("input#mitigasi").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostMitigasiResiko: function () {
		var data = [];
		var table_data = $("table#table_mitigasi_resiko").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				bay_dikerjakan: $(this).find("input#bay_dikerjakan").val(),
				terminal_proteksi: $(this).find("input#terminal_proteksi").val(),
				mitigasi_resiko: $(this).find("input#mitigasi_resiko").val(),
				arah_trip: $(this).find("input#arah_trip").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostAlatKerja: function () {
		var data = [];
		var table_data = $("table#table_alat_kerja").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				nama_alat: $(this).find("input#nama_alat").val(),
				satuan: $(this).find("input#satuan").val(),
				volume: $(this).find("input#volume").val(),
				penanggung_jawab: $(this).find("input#penanggung_jawab").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostPerlengkapan: function () {
		var data = [];
		var table_data = $("table#table_perlengkapan").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				nama_alat: $(this).find("input#nama_alat").val(),
				satuan: $(this).find("input#satuan").val(),
				volume: $(this).find("input#volume").val(),
				penanggung_jawab: $(this).find("input#penanggung_jawab").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostMaterial: function () {
		var data = [];
		var table_data = $("table#table_material").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				nama_alat: $(this).find("input#nama_alat").val(),
				satuan: $(this).find("input#satuan").val(),
				volume: $(this).find("input#volume").val(),
				penanggung_jawab: $(this).find("input#penanggung_jawab").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostUraian: function () {
		var data = [];
		let boxLast = $('div#box-sub-pelaksanaan').find('div.row');
		$.each(boxLast, function () {
			let params = {};
			params.sub_pelaksanaan_pekerjaan = $(this).find('#sub_pelaksanaan_pekerjaan').val();
			var table_data = $(this).find("table#table_uraian").find("tbody").find("tr");
			let detailData = [];
			$.each(table_data, function () {
				detailData.push({
					id: $(this).attr("data_id"),
					uraian: $(this).find("input#uraian").val(),
					deleted: $(this).hasClass("deleted") ? "1" : "0",
				});
			});

			params.detail_item = detailData;
			data.push(params);
		});

		return data;
	},

	getPostDokumen: function () {
		var data = [];
		var table_data = $("table#table_dokumen_text").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				remarks: $(this).find("input#remarks").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostLampiran: function () {
		var data = [];
		var table_data = $("table#table_lampiran_text").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				remarks: $(this).find("input#remarks").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostApproval: function () {
		var data = [];
		var table_data = $("table#table_penyusun").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				pegawai: $(this).attr("pegawai"),
				nama_penyusun: $(this).find("input#nama_penyusun").val(),
				email_penyusun: $(this).find("input#email_penyusun").val(),
				no_hp_penyusun: $(this).find("input#no_hp_penyusun").val(),
				jabatan_penyusun: $(this).find("input#jabatan_penyusun").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostApprovalDiperiksa: function () {
		var data = [];
		var table_data = $("table#table_diperiksa").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				pegawai: $(this).attr("pegawai"),
				nama_diperiksa: $(this).find("#nama_diperiksa").val(),
				email_periksa: $(this).find("input#email_periksa").val(),
				no_hp_periksa: $(this).find("input#no_hp_periksa").val(),
				jabatan_periksa: $(this).find("input#jabatan_periksa").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostApprovalDisahkan: function () {
		var data = [];
		var table_data = $("table#table_disahkan").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				pegawai: $(this).attr("pegawai"),
				nama_diperiksa: $(this).find("#nama_diperiksa").val(),
				email_periksa: $(this).find("input#email_periksa").val(),
				no_hp_periksa: $(this).find("input#no_hp_periksa").val(),
				jabatan_periksa: $(this).find("input#jabatan_periksa").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostCatatan: function () {
		var data = [];
		var table_data = $("table#table_catatan").find("tbody").find("tr");
		$.each(table_data, function () {
			data.push({
				id: $(this).attr("data_id"),
				edisi: $(this).find("#edisi").val(),
				tanggal: $(this).find("#tanggal").val(),
				halaman: $(this).find("input#halaman").val(),
				paragraf: $(this).find("input#paragraf").val(),
				alasan: $(this).find("input#alasan").val(),
				disetujui_oleh: $(this).find("input#disetujui_oleh").val(),
				jabatan: $(this).find("input#jabatan").val(),
				deleted: $(this).hasClass("deleted") ? "1" : "0",
			});
		});

		return data;
	},

	getPostData: function () {
		var data = {
			id: $("#id").val(),
			instruksi: {
				no_instruksi: $("#no_instruksi").val(),
				tanggal_instruksi: $("#tanggal_instruksi").val(),
				pekerjaan: $("#pekerjaan").val(),
				nama_pekerjaan_kontrak: $("#nama_pekerjaan_kontrak").val(),
				no_kontrak: $("#no_kontrak").val(),
				tujuan_unit: $("#tujuan_unit").val(),
				analisa_resiko: editor.html.get(),
				mitigasi_resiko: editorMitigasiNew.html.get(),
			},
			distribusi: InstruksiKerja.getPostDistribusi(),
			analisa_resiko: InstruksiKerja.getPostAnalisaResiko(),
			mitigasi_resiko: InstruksiKerja.getPostMitigasiResiko(),
			alat_kerja: InstruksiKerja.getPostAlatKerja(),
			perlengkapan: InstruksiKerja.getPostPerlengkapan(),
			material: InstruksiKerja.getPostMaterial(),
			uraian: InstruksiKerja.getPostUraian(),
			dokumen: InstruksiKerja.getPostDokumen(),
			lampiran: InstruksiKerja.getPostLampiran(),
			approval_disusun: InstruksiKerja.getPostApproval(),
			approval_diperiksa: InstruksiKerja.getPostApprovalDiperiksa(),
			approval_disahkan: InstruksiKerja.getPostApprovalDisahkan(),
			catatan: InstruksiKerja.getPostCatatan(),
			tahapan: {
				tujuan: editorTujuan.html.get(),
				ruang_lingkup: editorRuangLingkup.html.get(),
				referensi: editorReferensi.html.get(),
				// arah_trip: editorArahTrip.html.get(),
				pelaksanaan_pekerjaan: editorPelaksanaanPekerjaan.html.get(),
				// sub_pelaksanaan_pekerjaan: editorSubPelaksanaanPekerjaan.html.get(),
				tahap_persiapan: editorTahapanPekerjaan.html.get(),
			},
		};

		return data;
	},

	simpan: function (id, e, elm) {
		e.preventDefault();
		var data = InstruksiKerja.getPostData();
		data.state = $(elm).attr('state');

		var formData = new FormData();
		formData.append("data", JSON.stringify(data));
		formData.append("id", id);
		var table_data = $("table#table_lampiran_text").find("tbody").find("tr");
		var index = 0;
		$.each(table_data, function () {
			var fileLampiran = $(this).find("input#file").prop("files")[0];
			formData.append("file_lampiran_" + index, fileLampiran);
			index += 1;
		});
		index = 0;
		var table_data_dok = $("table#table_dokumen_text").find("tbody").find("tr");
		$.each(table_data_dok, function () {
			var fileLampiran = $(this).find("input#file").prop("files")[0];
			formData.append("file_dokumen_" + index, fileLampiran);
			index += 1;
		});
		formData.append('file_logo_vendor', $('input#file_logo_vendor').prop('files')[0]);

		let validations = 1;
		// let state = $(elm).attr('state');
		// switch (state) {
		// 	case 'instruksi':
		let elmData = $(elm).closest('form.form-horizontal');
		validations = validation.runWithElement(elmData);
		// 		break;

		// 	default:
		// 		validations = validations.run();
		// 		break;
		// }

		if (validations) {
			$.ajax({
				type: "POST",
				data: formData,
				dataType: "json",
				processData: false,
				contentType: false,
				// async: false,
				url: url.base_url(InstruksiKerja.module()) + "simpan",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						if (resp.is_valid_notif) {
							InstruksiKerja.sendNotifikasiWa(resp.data_notif, resp.id, 'ubah');
						}else{
							var reload = function () {
								window.location.href =
									url.base_url(InstruksiKerja.module()) + "ubah/" + resp.id;
							};
	
							setTimeout(reload(), 1000);
						}
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				},
			});
		}
	},

	sendNotifikasiWa: function (data, id, link = 'ubah') {
		let params = {};
		let dataParam = [];
		for (let i = 0; i < data.length; i++) {
			const element = data[i];
			dataParam.push({
				'message' : element.message,
				'no_hp' : element.no_hp,
				'nip' : element.nip,
			});
		}
		params.data = dataParam;
		console.log('params notif', params);

		$.ajax({
			type: "POST",
			data: params,
			dataType: "json",
			url: "http://invetory.solutionsdapps.com/public/api/sendwa",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Data...");
			},

			success: function (resp) {
				message.closeLoading();
				if (resp.is_valid) {
					toastr.success("Berhasil Diproses");
						var reload = function () {
							if (link == 'detail') {
								window.location.href =
									url.base_url(InstruksiKerja.module()) + link +"?id=" + id;
							}else{
								window.location.href =
									url.base_url(InstruksiKerja.module()) + link +"/" + id;
							}
						};

						setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Diproses "+resp.message);
				}
			},
		});
	},
	
	approve: function (elm, e) {
		e.preventDefault();
		let params = {};
		params.id = $(elm).closest("tr").attr("data_id");
		params.nip = $(elm).attr("nip");
		params.instruksi = $("#id").val();

		$.ajax({
			type: "POST",
			data: params,
			dataType: "json",
			// processData: false,
			// contentType: false,
			// async: false,
			url: url.base_url(InstruksiKerja.module()) + "approve",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Simpan...");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Disimpan");
					if (resp.is_valid_notif) {
						InstruksiKerja.sendNotifikasiWa(resp.data_notif, params.instruksi, 'detail');
					}else{
						var reload = function () {
							window.location.reload();
						};
	
						setTimeout(reload(), 1000);
					}
				} else {
					toastr.error("Gagal Disimpan");
				}
				message.closeLoading();
			},
		});
	},

	showConfirm: function (params, action) {
		var keterangan =
			action == "reject" ? "Keterangan Di Tolak" : "Keterangan Di Approve";
		var html = "<div class='row'>";
		html += "<div class='col-md-12'>";
		html += "<h5><u>" + keterangan + "</u></h5>";
		html +=
			"<textarea class='form-control required' error='Keterangan' id='keterangan_pop'></textarea>";
		html += "<br/>";
		html += "<div class='text-right'>";
		html +=
			"<button action='" +
			action +
			"' data_id='" +
			params.id +
			"' nip='" +
			params.nip +
			"' instruksi='" +
			params.instruksi +
			"' class='btn btn-success font-10'onclick='InstruksiKerja.execApprove(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
		html +=
			"<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		bootbox.dialog({
			message: html,
		});
	},

	reject: function (elm) {
		let params = {};
		params.id = $(elm).closest("tr").attr("data_id");
		params.instruksi = $("#id").val();
		params.nip = $(elm).attr("nip");
		InstruksiKerja.showConfirm(params, "reject");
	},

	execApprove: function (elm) {
		let params = {};
		params.id = $(elm).attr("data_id");
		params.nip = $(elm).attr("nip");
		params.instruksi = $("#id").val();
		params.keterangan = $("#keterangan_pop").val();

		if (validation.run()) {
			$.ajax({
				type: "POST",
				data: params,
				dataType: "json",
				// processData: false,
				// contentType: false,
				// async: false,
				url: url.base_url(InstruksiKerja.module()) + "approve",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.reload();
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				},
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(InstruksiKerja.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href =
			url.base_url(InstruksiKerja.module()) + "detail/" + id;
	},

	delete: function (id) {
		var html = "<div class='row'>";
		html += "<div class='col-md-12 text-center'>";
		html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
		html += "<div class='text-center'>";
		html +=
			"<button class='btn btn-success font-10'onclick='InstruksiKerja.execDeleted(" +
			id +
			")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
		html +=
			"<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		bootbox.dialog({
			message: html,
		});
	},

	execDeleted: function (id) {
		$.ajax({
			type: "POST",
			dataType: "json",
			async: false,
			url: url.base_url(InstruksiKerja.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href =
							url.base_url(InstruksiKerja.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			},
		});
	},

	addDetail: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr
			.find("td:eq(3)")
			.html(
				'<i class="fa fa-minus-circle fa-2x hover" onclick="InstruksiKerja.removeDetail(this)"></i>'
			);
		tr.after(newTr);
	},

	addDetailAgama: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var seq = tr.find("td:eq(0)").find("input").attr("id");
		seq = parseInt(seq.toString().replace("tanggal_agama_", ""));
		var next_id = seq + 1;
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:eq(0)")
			.find("input")
			.attr("id", "tanggal_agama_" + next_id)
			.removeClass("hasDatepicker")
			.removeData("datepicker")
			.unbind()
			.datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
			});
		newTr
			.find("td:eq(2)")
			.html(
				'<i class="fa fa-minus-circle fa-2x hover" onclick="InstruksiKerja.removeDetail(this)"></i>'
			);
		tr.after(newTr);
	},

	addDetailKesehatan: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var seq = tr.find("td:eq(0)").find("input").attr("id");
		seq = parseInt(seq.toString().replace("tanggal_kesehatan_", ""));
		var next_id = seq + 1;
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:eq(0)")
			.find("input")
			.attr("id", "tanggal_kesehatan_" + next_id)
			.removeClass("hasDatepicker")
			.removeData("datepicker")
			.unbind()
			.datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
			});
		newTr
			.find("td:eq(3)")
			.html(
				'<i class="fa fa-minus-circle fa-2x hover" onclick="InstruksiKerja.removeDetail(this)"></i>'
			);
		tr.after(newTr);
	},

	removeDetail: function (elm) {
		$(elm).closest("tr").remove();
	},

	upload: function (elm) {
		$("input#file").click();
	},

	getFilename: function (elm) {
		InstruksiKerja.checkFile(elm);
	},

	checkFile: function (elm) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split(".");

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			if (type_file == "png") {
				if (data_file.size <= 1324000) {
					$(elm)
						.closest("div")
						.find("span.fileinput-filename")
						.text($(elm).val());
				} else {
					toastr.error("Gagal Upload, Ukuran File Maximal 1 MB");
					message.closeLoading();
				}
			} else {
				toastr.error("File Harus Berformat Png");
				$(elm).val("");
				message.closeLoading();
			}
		} else {
			toastr.error("FileReader is Not Supported");
			message.closeLoading();
		}
	},

	// showLogo: function (elm, e) {
	// 	e.preventDefault();
	// 	window.open = $(elm).attr('url');
	// 	// return;
	// 	// $.ajax({
	// 	// 	type: "POST",
	// 	// 	data: {
	// 	// 		foto: $.trim($(elm).attr('url')),
	// 	// 	},
	// 	// 	dataType: "html",
	// 	// 	async: false,
	// 	// 	url: url.base_url(InstruksiKerja.module()) + "showLogo",
	// 	// 	success: function (resp) {
	// 	// 		bootbox.dialog({
	// 	// 			message: resp,
	// 	// 			//     size: 'large'
	// 	// 		});
	// 	// 	},
	// 	// });
	// },

	changeManual: function (elm) {
		$("div.manual_detail").addClass("display-none");
		$("div.manual_add").removeClass("display-none");
		$("div.manual_add").append(
			"<i class='mdi mdi-close mdi-24px hover' onclick='InstruksiKerja.cancelChangeManual(this)'></i>"
		);
	},

	cancelChangeManual: function (elm) {
		$("div.manual_detail").removeClass("display-none");
		$("div.manual_add").addClass("display-none");
		$("i.mdi-close").remove();

		var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
		inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
		inputFile += '<span class="fileinput-filename"></span>';
		inputFile += "</div> ";
		inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
		inputFile +=
			'<span class="fileinput-new" onclick="InstruksiKerja.upload(this)">Select file</span> ';
		inputFile +=
			'<input type="file" style="display: none;" id="file" onchange="InstruksiKerja.getFilename(this)"/>';
		inputFile += "</span>";
		$("div.manual_upload").html(inputFile);
	},

	showUpdateFoto: function (elm) {
		bootbox.dialog({
			message: "Ganti Foto",
		});
	},

	showTooltip: function (elm) { },

	setDate: function () {
		$("input#tanggal_instruksi").datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
		});
	},

	setDataTable: function () {
		$("#tb_content").DataTable({
			paging: false,
			lengthChange: true,
			searching: false,
			ordering: true,
			info: false,
			autoWidth: false,
		});
	},

	setEditor: () => {
		editor = new FroalaEditor(
			"#analisa_resiko",
			{
				// imageUpload: false,
				// imageInsertButtons: false,
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		editorMitigasiNew = new FroalaEditor(
			"#mitigasi_resiko_new",
			{
				// imageUpload: false,
				// imageInsertButtons: false,
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		editorTujuan = new FroalaEditor(
			"#tujuan",
			{
				// imageUpload: false,
				// imageInsertButtons: false,
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		editorRuangLingkup = new FroalaEditor(
			"#ruang_lingkup",
			{
				// imageUpload: false,
				// imageInsertButtons: false,
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		editorReferensi = new FroalaEditor(
			"#referensi",
			{
				// imageUpload: false,
				// imageInsertButtons: false,
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		// editorArahTrip = new FroalaEditor(
		// 	"#arah_trip_editor",
		// 	{
		// 		imageUpload: false,
		// 		imageInsertButtons: false,
		// 	},
		// 	function () {
		// 		// console.log(editor.html.get())
		// 	}
		// );

		editorPelaksanaanPekerjaan = new FroalaEditor(
			"#pelaksanaan_pekerjaan",
			{
				// imageUpload: false,
				// imageInsertButtons: false,
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		editorTahapanPekerjaan = new FroalaEditor(
			"#tahap_persiapan",
			{
				// Set the image upload parameter.
				imageUploadParam: "image_param",

				// Set the image upload URL.
				imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

				// Additional upload params.
				imageUploadParams: {
					id: "my_editor",
					type: "tahapan",
					no_instruksi: $("#no_instruksi").val(),
				},

				// Set request type.
				imageUploadMethod: "POST",

				// Set max image size to 5MB.
				imageMaxSize: 5 * 1024 * 1024,

				// Allow to upload PNG and JPG.
				imageAllowedTypes: ["jpeg", "jpg", "png"],
				events: {
					"image.beforeUpload": function (images) {
						// Return false if you want to stop the image upload.
					},
					"image.uploaded": function (response) {
						// Image was uploaded to the server.
						console.log("uploaded", response);
					},
					"image.inserted": function ($img, response) {
						// Image was inserted in the editor.
					},
					"image.replaced": function ($img, response) {
						// Image was replaced in the editor.
					},
					"image.error": function (error, response) {
						console.log(error);
						console.log(response);
						// Bad link.
						if (error.code == 1) {
						}

						// No link in upload response.
						else if (error.code == 2) {
						}

						// Error during image upload.
						else if (error.code == 3) {
						}

						// Parsing response failed.
						else if (error.code == 4) {
						}

						// Image too text-large.
						else if (error.code == 5) {
						}

						// Invalid image type.
						else if (error.code == 6) {
						}

						// Image can be uploaded only to same domain in IE 8 and IE 9.
						else if (error.code == 7) {
						}

						// Response contains the original server response to the request if available.
					},
				},
			},
			function () {
				// console.log(editor.html.get())
			}
		);

		// editorSubPelaksanaanPekerjaan = new FroalaEditor(
		// 	"#sub_pelaksanaan_pekerjaan",
		// 	{
		// 		// Set the image upload parameter.
		// 		imageUploadParam: "image_param",

		// 		// Set the image upload URL.
		// 		imageUploadURL: url.base_url(InstruksiKerja.module()) + "upload_image",

		// 		// Additional upload params.
		// 		imageUploadParams: {
		// 			id: "my_editor",
		// 			type: "tahapan",
		// 			no_instruksi: $("#no_instruksi").val(),
		// 		},

		// 		// Set request type.
		// 		imageUploadMethod: "POST",

		// 		// Set max image size to 5MB.
		// 		imageMaxSize: 5 * 1024 * 1024,

		// 		// Allow to upload PNG and JPG.
		// 		imageAllowedTypes: ["jpeg", "jpg", "png"],
		// 		events: {
		// 			"image.beforeUpload": function (images) {
		// 				// Return false if you want to stop the image upload.
		// 			},
		// 			"image.uploaded": function (response) {
		// 				// Image was uploaded to the server.
		// 				console.log("uploaded", response);
		// 			},
		// 			"image.inserted": function ($img, response) {
		// 				// Image was inserted in the editor.
		// 			},
		// 			"image.replaced": function ($img, response) {
		// 				// Image was replaced in the editor.
		// 			},
		// 			"image.error": function (error, response) {
		// 				console.log(error);
		// 				console.log(response);
		// 				// Bad link.
		// 				if (error.code == 1) {
		// 				}

		// 				// No link in upload response.
		// 				else if (error.code == 2) {
		// 				}

		// 				// Error during image upload.
		// 				else if (error.code == 3) {
		// 				}

		// 				// Parsing response failed.
		// 				else if (error.code == 4) {
		// 				}

		// 				// Image too text-large.
		// 				else if (error.code == 5) {
		// 				}

		// 				// Invalid image type.
		// 				else if (error.code == 6) {
		// 				}

		// 				// Image can be uploaded only to same domain in IE 8 and IE 9.
		// 				else if (error.code == 7) {
		// 				}

		// 				// Response contains the original server response to the request if available.
		// 			},
		// 		},
		// 	},
		// 	function () {
		// 		// console.log(editor.html.get())
		// 	}
		// );

		setInterval(function () {
			$('div[style="z-index:9999;width:100%;position:relative"]').remove();
		}, 1000); //untuk menghilangkan watermak
	},

	removeDistribusi: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addDistribusi: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeDistribusi(this)"></i>'
			);
		tr.after(newTr);
	},

	addCatatan: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeCatatan(this)"></i>'
			);
		tr.after(newTr);
	},

	removeCatatan: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	removeAnalisaResiko: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addAnalisaResiko: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeAnalisaResiko(this)"></i>'
			);
		tr.after(newTr);
	},

	removeMitigasiResiko: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addUraian: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeUraian(this)"></i>'
			);
		tr.after(newTr);
	},

	removeUraian: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addDokumen: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeDokumen(this)"></i>'
			);
		tr.after(newTr);
	},

	removeDokumen: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addMitigasiResiko: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeMitigasiResiko(this)"></i>'
			);
		tr.after(newTr);
	},

	removeAlatKerja: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addAlatKerja: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeAlatKerja(this)"></i>'
			);
		tr.after(newTr);
	},

	removeMaterial: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addMaterial: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeMaterial(this)"></i>'
			);
		tr.after(newTr);
	},

	removeLampiran: function (elm) {
		var data_id = $(elm).closest("tr").attr("data_id");
		if (data_id == "") {
			$(elm).closest("tr").remove();
		} else {
			$(elm).closest("tr").addClass("deleted");
			$(elm).closest("tr").addClass("hidden");
		}
	},

	addLampiran: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeLampiran(this)"></i>'
			);
		tr.after(newTr);
	},

	showLogo: function (elm, e) {
		e.preventDefault();
		$.ajax({
			type: "POST",
			data: {
				foto: $.trim($(elm).attr("url")),
			},
			dataType: "html",
			url: url.base_url(InstruksiKerja.module()) + "showLogo",
			success: function (resp) {
				bootbox.dialog({
					message: resp,
				});
			},
		});
	},

	gantiFileItem: function (elm, e) {
		e.preventDefault();
		var file_input = $(elm).closest("td").find("div#file_input_file");
		console.log(file_input);
		file_input.removeClass("hidden");

		$(elm).closest("td").find("div#detail_file").addClass("hidden");
	},

	gantiFileItemLogoVendor: function (elm, e) {
		e.preventDefault();
		var file_input = $(elm).closest("td").find("div#file_input_file_logo_vendor");
		console.log(file_input);
		file_input.removeClass("hidden");

		$(elm).closest("td").find("div#detail_file").addClass("hidden");
	},

	checkFile: function (elm) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split(".");

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			type_file = type_file.toLowerCase();
			if (type_file == "png" || type_file == "jpeg" || type_file == "jpg") {
				if (data_file.size <= 1324000) {
					$(elm)
						.closest("div")
						.find("span.fileinput-filename")
						.text($(elm).val());
				} else {
					toastr.error("Gagal Upload, Ukuran File Maximal 1 MB");
					message.closeLoading();
				}
			} else {
				toastr.error("File Harus Berformat Png, Jpg, Jpeg");
				$(elm).val("");
				message.closeLoading();
			}
		} else {
			toastr.error("FileReader is Not Supported");
			message.closeLoading();
		}
	},

	getData: async () => {
		let tableData = $("table#table-data");

		let updateAction = $("#update").val();
		let deleteAction = $("#delete").val();
		let vendorId = $("#vendor_id").val();
		let userId = $("#user_id").val();

		var data = tableData.DataTable({
			processing: true,
			serverSide: true,
			ordering: true,
			autoWidth: false,
			order: [[0, "desc"]],
			aLengthMenu: [
				[25, 50, 100],
				[25, 50, 100],
			],
			lengthChange: !1,
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>",
				},
			},
			drawCallback: function () {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
			},
			ajax: {
				url: url.base_url(InstruksiKerja.module()) + `getData`,
				type: "POST",
			},
			deferRender: true,
			createdRow: function (row, data, dataIndex) {
				// console.log('row', $(row));
			},
			buttons: ["copy", "excel", "pdf", "colvis"],
			columnDefs: [
				{
					targets: 0,
					className: "text-center",
				},
				{
					targets: 1,
					orderable: false,
					className: "text-center",
				},
				{
					targets: 2,
					orderable: false,
				},
				// {
				// 	targets: 3,
				// 	orderable: false,
				// },
				// {
				// 	targets: 4,
				// 	orderable: false,
				// 	className: "text-start",
				// },
				// {
				// 	targets: 5,
				// 	orderable: false,
				// 	className: "text-start",
				// },
			],
			columns: [
				{
					data: "id",
					render: function (data, type, row, meta) {
						return meta.row + meta.settings._iDisplayStart + 1;
					},
				},
				{
					data: "no_instruksi",
					render: function (data, type, row) {
						return data+"-"+row.id;
					},
				},
				{
					data: "pekerjaan",
				},
				{
					data: "tanggal_instruksi",
				},
				{
					data: "tujuan_unit",
					render: function (data, type, row) {
						try {							
							return row.tujuan_detail.nama;
						} catch (error) {
							return "-";
						}
					},
				},
				{
					data: "sudah_lengkap_acc",
					render: function (data, type, row) {
						console.log(row);
						return data == "" ? "Menunggu Proses Approval" : "Sudah Disetujui";
					},
				},
				{
					data: "id",
					render: function (data, type, row) {
						var html = ``;
						console.log('row', row, vendorId);
						html += `<a href='${url.base_url(
							InstruksiKerja.module()
						)}cetak?id=${data}' data_id="${row.id
							}" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="Cetak" class="btn btn-warning editable-submit btn-sm waves-effect waves-light"><i class="fa fa-print grey-text  hover"></i></a>&nbsp;`;
						html += `<a href='${url.base_url(
							InstruksiKerja.module()
						)}detail?id=${data}' data_id="${row.id
							}" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="Detail" class="btn btn-info editable-submit btn-sm waves-effect waves-light"><i class="fa fa-file grey-text  hover"></i></a>&nbsp;`;
						if (row.sudah_lengkap_acc != "" && vendorId != '') {
							html += `<a href='${url.base_url(
								InstruksiKerja.moduleWpEks()
							)}add?instruksi_kerja=${data}' data_id="${row.id
								}" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="Buat WP" class="btn btn-primary editable-submit btn-sm waves-effect waves-light">Buat WP</a>&nbsp;`;
						} else {
							if (row.requestor == userId) {								
								html += `<a href='${url.base_url(
									InstruksiKerja.module()
								)}ubah/${data}' data_id="${row.id
									}" data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="Ubah" class="btn btn-default editable-submit btn-sm waves-effect waves-light"><i class="fa fa-pencil grey-text  hover"></i></a>&nbsp;`;
							}
						}
						// html += `<button data-trigger="hover" data-toggle="tooltip" data-placement="bottom" title="Hapus" type="button" data_id="${row.id}" onclick="PemasukanMaterial.delete(this, event)" class="btn btn-danger editable-cancel btn-sm waves-effect waves-light"><i class="fa fa-trash grey-text  hover"></i></button>`;
						return html;
					},
				},
			],
		});
	},

	getUrlParams: () => {
		let url = window.location.href;
		var params = {};
		(url + "?")
			.split("?")[1]
			.split("&")
			.forEach(function (pair) {
				pair = (pair + "=").split("=").map(decodeURIComponent);
				if (pair[0].length) {
					params[pair[0]] = pair[1];
				}
			});

		return params;
	},

	addSubPelaksanaan: (elm, e) => {
		e.preventDefault();
		let boxLast = $('div#box-sub-pelaksanaan').find('div.row:last');
		let cloneRows = boxLast.clone();
		cloneRows.find('input').val('');
		cloneRows.find('textarea').val('');
		cloneRows.find('div#action-delete-sub').html('<a href="" onclick="InstruksiKerja.hapusSubPelaksaan(this, event)">Hapus Sub</a>');
		let tableUraian = cloneRows.find('table#table_uraian').find('tbody').find('tr');
		$.each(tableUraian, function () {
			let btnDelete = $(this).find('i.fa-trash');
			let btnDeleteMinus = $(this).find('i.fa-minus');
			if (btnDelete.length > 0 || btnDeleteMinus.length > 0) {
				$(this).remove();
			}
		});
		boxLast.after(cloneRows);
	},

	hapusSubPelaksaan: (elm, e) => {
		e.preventDefault();
		$(elm).closest('div.row').remove();
	},

	removeAprovalRoles: (elm) => {
		$(elm).closest('tr').remove();
	},

	addAprovalRoles: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr.attr("data_id", '');
		newTr.attr("pegawai", '');
		newTr.find('td:eq(0)').html(`<input type="text" value="" id="nama_penyusun" class="form-control required" error="Nama Penyusun" />`);
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeAprovalRoles(this)"></i>'
			);
		tr.after(newTr);
	},

	addAprovalRoles2: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr.attr("data_id", '');
		newTr.attr("pegawai", '');
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeAprovalRoles(this)"></i>'
			);
		tr.after(newTr);
	},

	addAprovalRoles3: function (elm) {
		var tr = $(elm).closest("tbody").find("tr:last");
		var newTr = tr.clone();
		newTr.find("input").val("");
		newTr.attr("data_id", '');
		newTr.attr("pegawai", '');
		newTr
			.find("td:last")
			.html(
				'<i class="fa fa-minus fa-lg hover-content" onclick="InstruksiKerja.removeAprovalRoles(this)"></i>'
			);
		tr.after(newTr);
	},

	setDetailRoles2: (elm, e) => {
		let data = $(elm).closest('tr');
		let val = $(elm).val();
		let select = data.find('#nama_diperiksa').find(`option[value="${val}"]`);
		data.find('#email_periksa').val(select.attr('email'));
		data.find('#no_hp_periksa').val(select.attr('no_hp'));
		data.find('#jabatan_periksa').val(select.attr('jabatan'));
	}
};

$(function () {
	InstruksiKerja.getData();
	InstruksiKerja.setDate();
	InstruksiKerja.setEditor();
	InstruksiKerja.setDataTable();
});
