var TemplateJsa = {
	module: function () {
		return 'template_jsa';
	},

	add: function () {
		window.location.href = url.base_url(TemplateJsa.module()) + "add";
	},

	main: function () {
		window.location.href = url.base_url(TemplateJsa.module()) + "index";
	},

	back: function () {
		window.location.href = url.base_url(TemplateJsa.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(TemplateJsa.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(TemplateJsa.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'form_jsa': TemplateJsa.getPostItemJsaData(),
		};

		return data;
	},

	getDataItemPotensiBahaya:(elm)=>{
		let data = [];
		let tableData = elm.find('table#table-potensi').find('tr');
		$.each(tableData, function(){
			let params = {};
			params.potensi = $.trim($(this).find('td:eq(0)').text());
			data.push(params);
		});

		return data;
	},
	
	getDataItePengendalian:(elm)=>{
		let data = [];
		let tableData = elm.find('table#table-pengendalian').find('tr');
		$.each(tableData, function(){
			let params = {};
			params.pengendalian = $.trim($(this).find('td:eq(0)').text());
			data.push(params);
		});

		return data;
	},

	getPostItemJsaData:()=>{
		let data = [];
		let tableData = $('table#tb_content_jsa').find('tbody').find('tr.input');
		$.each(tableData, function(){
			let params = {};
			params.tahapan_pekerjaan = $.trim($(this).find('td#tahapan_pekerjaan').find('label#str_tahapan').text());
			params.potensi_bahaya = TemplateJsa.getDataItemPotensiBahaya($(this));
			params.pengendalian = TemplateJsa.getDataItePengendalian($(this));
			params.id = $(this).attr('data_id');
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			if(params.tahapan_pekerjaan != '' && params.potensi_bahaya.length > 0 && params.pengendalian.length > 0){
				data.push(params);
			}
		});

		return data;
	},

	simpan: function (elm, e) {
		e.preventDefault();

		var data = TemplateJsa.getPostData();
		console.log('data', data);

		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append("id", '');
		//  formData.append('file', $('input#file').prop('files')[0]);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				async: false,
				url: url.base_url(TemplateJsa.module()) + "simpan",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.reload();
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(TemplateJsa.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(TemplateJsa.module()) + "detail/" + id;
	},

	delete: function (id) {
		var html = "<div class='row'>";
		html += "<div class='col-md-12 text-center'>";
		html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
		html += "<div class='text-center'>";
		html += "<button class='btn btn-success font-10'onclick='TemplateJsa.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
		html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
		html += "</div>";
		html += "</div>";
		html += "</div>";

		bootbox.dialog({
			message: html,
		});
	},

	execDeleted: function (id) {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(TemplateJsa.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(TemplateJsa.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	addDetail: function (elm) {
		var tr = $(elm).closest('tbody').find('tr:last');
		var newTr = tr.clone();
		newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="TemplateJsa.removeDetail(this)"></i>');
		tr.after(newTr);
	},

	addDetailAgama: function (elm) {
		var tr = $(elm).closest('tbody').find('tr:last');
		var seq = tr.find('td:eq(0)').find('input').attr('id');
		seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
		var next_id = seq + 1;
		var newTr = tr.clone();
		newTr.find('input').val('');
		newTr.find('td:eq(0)').find('input')
			.attr('id', 'tanggal_agama_' + next_id)
			.removeClass('hasDatepicker')
			.removeData('datepicker')
			.unbind()
			.datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
			});
		newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="TemplateJsa.removeDetail(this)"></i>');
		tr.after(newTr);
	},

	addDetailKesehatan: function (elm) {
		var tr = $(elm).closest('tbody').find('tr:last');
		var seq = tr.find('td:eq(0)').find('input').attr('id');
		seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
		var next_id = seq + 1;
		var newTr = tr.clone();
		newTr.find('input').val('');
		newTr.find('td:eq(0)').find('input')
			.attr('id', 'tanggal_kesehatan_' + next_id)
			.removeClass('hasDatepicker')
			.removeData('datepicker')
			.unbind()
			.datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
			});
		newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="TemplateJsa.removeDetail(this)"></i>');
		tr.after(newTr);
	},

	removeDetail: function (elm) {
		$(elm).closest('tr').remove();
	},

	upload: function (elm) {
		$('input#file').click();
	},

	getFilename: function (elm) {
		TemplateJsa.checkFile(elm);
	},

	checkFile: function (elm) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split('.');

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			if (type_file == 'png') {
				if (data_file.size <= 1324000) {
					$(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
				} else {
					toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
					message.closeLoading();
				}
			} else {
				toastr.error('File Harus Berformat Png');
				$(elm).val('');
				message.closeLoading();
			}
		} else {
			toastr.error('FileReader is Not Supported');
			message.closeLoading();
		}
	},

	showLogo: function (elm, e) {
		e.preventDefault();
		$.ajax({
			type: 'POST',
			data: {
				foto: $.trim($(elm).text())
			},
			dataType: 'html',
			async: false,
			url: url.base_url(TemplateJsa.module()) + "showLogo",
			success: function (resp) {
				bootbox.dialog({
					message: resp,
					//     size: 'large'
				});
			}
		});
	},

	choosePotensiBahaya: function (elm, e) {
		e.preventDefault();
		let params = {};
		params.index = $(elm).closest('tr').index();
		params.index_parent = $(elm).closest('table#table-potensi').closest('tr').index();

		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'html',
			async: false,
			url: url.base_url(TemplateJsa.module()) + "choosePotensiBahaya",
			success: function (resp) {
				bootbox.dialog({
					message: resp,
					//     size: 'large'
				});
			}
		});
	},

	choosePengendalian: function (elm, e) {
		e.preventDefault();
		let params = {};
		params.index = $(elm).closest('tr').index();
		params.index_parent = $(elm).closest('table#table-pengendalian').closest('tr').index();

		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'html',
			async: false,
			url: url.base_url(TemplateJsa.module()) + "choosePengendalian",
			success: function (resp) {
				bootbox.dialog({
					message: resp,
					//     size: 'large'
				});
			}
		});
	},

	chooseTahapanPekerjaan: function (elm, e) {
		e.preventDefault();
		let params = {};
		params.index = $(elm).closest('tr').index();

		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'html',
			async: false,
			url: url.base_url(TemplateJsa.module()) + "chooseTahapanPekerjaan",
			success: function (resp) {
				bootbox.dialog({
					message: resp,
					//     size: 'large'
				});
			}
		});
	},

	pilihPotensi: (elm) => {
		let potensi = $.trim($(elm).closest('tr').find('td:eq(0)').text());
		console.log('potensi', potensi);
		let index = $(elm).attr('index');
		let index_parent = $(elm).attr('index_parent');
		let tb_potensi = $('table#tb_content_jsa').find('tbody').find(`tr.input:eq(${index_parent})`).find('table#table-potensi').find('tbody').find(`tr:eq(${index})`);
		// console.log('tb_potensi', tb_potensi);
		let html = `${potensi}`
		tb_potensi.find('td:eq(0)').html(html);
		message.closeDialog();
	},

	pilihPengendalian: (elm) => {
		let pengendalian = $.trim($(elm).closest('tr').find('td:eq(0)').text());
		console.log('tb_pengendalian', pengendalian);
		let index = $(elm).attr('index');
		let index_parent = $(elm).attr('index_parent');
		let tb_pengendalian = $('table#tb_content_jsa').find('tbody').find(`tr.input:eq(${index_parent})`).find('table#table-pengendalian').find('tbody').find(`tr:eq(${index})`);
		let html = `${pengendalian}`
		tb_pengendalian.find('td:eq(0)').html(html);
		message.closeDialog();
	},

	pilihTahapan: (elm) => {
		let tahapan = $.trim($(elm).closest('tr').find('td:eq(0)').text());
		let index = $(elm).attr('index');
		let tb_content_jsa = $('table#tb_content_jsa').find('tbody').find(`tr.input:eq(${index})`);
		let html = `
		<label id="str_tahapan">${tahapan}</label>		
		<br/>
		<a href="" onclick="TemplateJsa.chooseTahapanPekerjaan(this, event)">Pilih Tahapan Pekerjaan</a>`;
		tb_content_jsa.find('td#tahapan_pekerjaan').html(html);
		message.closeDialog();
	},

	addPotensi: (elm) => {
		let tr = $(elm).closest('tr');
		let newTr = tr.clone();
		newTr.find('td:eq(0)').html(`<a href="" onclick="TemplateJsa.choosePotensiBahaya(this, event)">Pilih Potensi Bahaya</a>`);
		newTr.find('td:eq(1)').html(`<i class="fa fa-trash" onclick="TemplateJsa.removePotensi(this)"></i>`);
		tr.after(newTr);
	},

	addPengendalian: (elm) => {
		let tr = $(elm).closest('tr');
		let newTr = tr.clone();
		newTr.find('td:eq(0)').html(`<a href="" onclick="TemplateJsa.choosePengendalian(this, event)">Pilih Potensi Bahaya</a>`);
		newTr.find('td:eq(1)').html(`<i class="fa fa-trash" onclick="TemplateJsa.removePengendalian(this)"></i>`);
		tr.after(newTr);
	},

	removePotensi: (elm) => {
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id != '') {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		} else {
			$(elm).closest('tr').remove();
		}
	},

	removePengendalian: (elm) => {
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id != '') {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		} else {
			$(elm).closest('tr').remove();
		}
	},

	addItemTemplate: (elm) => {
		let tr = $('table#tb_content_jsa').find('tbody').find('tr.input:last');
		let newTr = tr.clone();
		newTr.find('td:eq(0)').html('-');
		newTr.find('td#tahapan_pekerjaan').html(`<a href="" onclick="TemplateJsa.chooseTahapanPekerjaan(this, event)">Pilih Tahapan Pekerjaan</a>`);
		newTr.find('td#potensi_bahaya').html(`	<table style="width: 100%;" id="table-potensi">
		<tr data_id="">
			<td class="td_jsa">
				<a href="" onclick="TemplateJsa.choosePotensiBahaya(this, event)">Pilih Potensi Bahaya</a>
			</td>
			<td class="td_jsa text-center">
				<i class="fa fa-plus" onclick="TemplateJsa.addPotensi(this)"></i>
			</td>
		</tr>
	</table>`);
		newTr.find('td#pengendalian').html(`<table style="width: 100%;" id="table-pengendalian">
		<tr data_id="">
			<td class="td_jsa">
				<a href="" onclick="TemplateJsa.choosePengendalian(this, event)">Pilih Pengendalian</a>
			</td>
			<td class="td_jsa text-center">
				<i class="fa fa-plus" onclick="TemplateJsa.addPengendalian(this)"></i>
			</td>
		</tr>
	</table>`);
		newTr.find('td#action').html(`<i class="fa fa-trash" onclick="TemplateJsa.removeItemTemplate(this)"></i>`);
		tr.after(newTr);
	},

	removeItemTemplate: (elm) => {
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id != '') {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		} else {
			$(elm).closest('tr').remove();
		}
	},

	changeManual: function (elm) {
		$('div.manual_detail').addClass('display-none');
		$('div.manual_add').removeClass('display-none');
		$('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='TemplateJsa.cancelChangeManual(this)'></i>");
	},

	cancelChangeManual: function (elm) {
		$('div.manual_detail').removeClass('display-none');
		$('div.manual_add').addClass('display-none');
		$('i.mdi-close').remove();

		var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
		inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
		inputFile += '<span class="fileinput-filename"></span>';
		inputFile += '</div> ';
		inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
		inputFile += '<span class="fileinput-new" onclick="TemplateJsa.upload(this)">Select file</span> ';
		inputFile += '<input type="file" style="display: none;" id="file" onchange="TemplateJsa.getFilename(this)"/>';
		inputFile += '</span>';
		$('div.manual_upload').html(inputFile);
	},

	showUpdateFoto: function (elm) {
		bootbox.dialog({
			message: 'Ganti Foto'
		});
	},

	showTooltip: function (elm) {

	},

	setDate: function () {
		$('input#tanggal_lahir').datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
		});
	},

	setDataTable: function () {
		$('#tb_content').DataTable({
			'paging': false,
			'lengthChange': true,
			'searching': false,
			'ordering': true,
			'info': false,
			'autoWidth': false
		})
	}
};

$(function () {
	// TemplateJsa.setDate();
	TemplateJsa.setDataTable();
});
