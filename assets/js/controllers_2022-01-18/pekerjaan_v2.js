var Pekerjaan = {
 module: function () {
  return "pekerjaan";
 },

 getDataPekerjaan: function () {

  var date = $('#default_date').length == 1 ? $('#default_date').val() : '';
  var id_place = $('#id_place').length == 1 ? $('#id_place').val() : '';
  var upt = $('#upt').length == 1 ? $('#upt').val() : '';
  $.ajax({
   type: 'POST',
   data: {
    date: date,
    id_place: id_place,
    upt: upt,
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Pekerjaan.module()) + "getCurrentEventPermit",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    var data = resp.data;
    var event = [];

    $('#calendar').fullCalendar('removeEventSources', null);
    var temp_date_awal_pekerjaan = [];
    for (var i = 0; i < data.length; i++) {
     var tgl_pekerjaan = data[i].tgl_pekerjaan;
     var value = data[i];
     var month = data[i].month;
     var year = data[i].year;
     var day = data[i].day;
     var day_end = data[i].day_end;
     var tgl_pekerjaan = data[i].tgl_pekerjaan;
     var status = data[i].status;
     var dif_day = day_end - day;

     if ($.inArray(tgl_pekerjaan, temp_date_awal_pekerjaan) == -1) {
      if (dif_day > 0) {
       for (var x = 0; x < dif_day; x++) {
        day = x == 0 ? day : day + 1;

        var month_url = month < 10 ? "0" + (month + 1).toString() : month.toString();
        var day_url = day < 10 ? "0" + (day + 1).toString() : day.toString();
        var tgl_fix = year.toString() + "-" + month_url + "-" + day_url;
        event.push({
         title: x == 0 ? 'Ada Pekerjaan \n\n\n' : '',
         start: new Date(year, month, day),
         backgroundColor: '#00a65a',
         borderColor: '#00a65a',
         tgl: year.toString() + "-" + month.toString() + "-" + day,
         url: url.base_url("pekerjaan") + "view/" + tgl_fix + '/' + upt + '/' + id_place,
        });
       }
      } else {
       var month_url = month < 10 ? "0" + (month + 1).toString() : month.toString();
       var day_url = day < 10 ? "0" + (day + 1).toString() : day.toString();
       var tgl_fix = year.toString() + "-" + month_url + "-" + day_url;
       event.push({
        title: 'Ada Pekerjaan',
        start: new Date(year, month, day),
        tgl: year.toString() + "-" + month.toString() + "-" + day,
        backgroundColor: '#00a65a',
        borderColor: '#00a65a',
        url: url.base_url("pekerjaan") + "view/" + tgl_fix + '/' + upt + '/' + id_place,
       });
      }

      temp_date_awal_pekerjaan.push(tgl_pekerjaan);
     }
    }

    for (var x = 0; x < temp_date_awal_pekerjaan.length; x++) {
     var head_tgl = temp_date_awal_pekerjaan[x];
     for (var i = 0; i < data.length; i++) {
      var tgl_pekerjaan = data[i].tgl_pekerjaan;
      var value = data[i];
      var month = data[i].month;
      var month_end = data[i].month_end;
      var year = data[i].year;
      var day = data[i].day;
      var year_end = data[i].year_end;
      var day_end = data[i].day_end;
      var tgl_pekerjaan = data[i].tgl_pekerjaan;
      var tgl_akhir = data[i].tgl_akhir;
      var status = data[i].status;
      var dif_day = day_end - day;
//      console.log('dif day', dif_day);
      if (tgl_pekerjaan == temp_date_awal_pekerjaan[x]) {
       if (tgl_akhir != tgl_pekerjaan) {
        if (dif_day > 0) {
         for (var y = 1; y < dif_day + 1; y++) {
          day = day + 1;
          var month_url = month < 10 ? "0" + (month + 1).toString() : month.toString();
          var day_url = day < 10 ? "0" + (day + 1).toString() : day.toString();
          var tgl_fix = year.toString() + "-" + month_url + "-" + day_url;
          event.push({
           title: '\n\n\n',
           start: new Date(year, month, day),
           tgl: year.toString() + "-" + month.toString() + "-" + day,
           backgroundColor: '#00a65a',
           borderColor: '#00a65a',
           url: url.base_url("pekerjaan") + "view/" + tgl_fix + '/' + upt + '/' + id_place,
          });
         }
        }
       }
      }
     }
    }

//
    var event_result = [];
    var temp = [];
    for (var i = 0; i < event.length; i++) {
//     console.log(event[i]);
     if ($.inArray(event[i].tgl, temp) == -1) {
      event_result.push(event[i]);
      temp.push(event[i].tgl);
     }
    }

//    console.log(event_result);
    $('#calendar').fullCalendar('addEventSource', event_result);

    var fc_time = $('span.fc-time');
    $.each(fc_time, function () {
     $(this).text('');
    });
   }
  });
 },

 setDateData: function () {
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();
  console.log(y + " dan " + m + " dan " + d);

  $('#calendar').fullCalendar({
   defaultDate: $('input#default_date').val(),
   header: {
    left: 'prev,next today',
    center: 'title',
//    right: 'month,agendaWeek,agendaDay'
    right: 'month,'
   },
   buttonText: {
    today: 'today',
    month: 'month',
    week: 'week',
    day: 'day'
   },
   events: [],
  });

  Pekerjaan.getDataPekerjaan();
 },

 updateCalendarEvent: function (date) {
  var id_place = $('#id_place').val();
  var upt = $('#upt').val();
  $.ajax({
   type: 'POST',
   data: {
    date: date,
    id_place: id_place,
    upt: upt
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Pekerjaan.module()) + "getDataPermit",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    var data = resp.data;
    var event = [];

    $('#calendar').fullCalendar('removeEventSources', null);
    var temp_date_awal_pekerjaan = [];
    for (var i = 0; i < data.length; i++) {
     var tgl_pekerjaan = data[i].tgl_pekerjaan;
     var value = data[i];
     var month = data[i].month;
     var year = data[i].year;
     var day = data[i].day;
     var day_end = data[i].day_end;
     var tgl_pekerjaan = data[i].tgl_pekerjaan;
     var status = data[i].status;
     var dif_day = day_end - day;

     if ($.inArray(tgl_pekerjaan, temp_date_awal_pekerjaan) == -1) {
      if (dif_day > 0) {
       for (var x = 0; x < dif_day; x++) {
        day = x == 0 ? day : day + 1;

        var month_url = month < 10 ? "0" + (month + 1).toString() : month.toString();
        var day_url = day < 10 ? "0" + (day + 1).toString() : day.toString();
        var tgl_fix = year.toString() + "-" + month_url + "-" + day_url;
        event.push({
         title: x == 0 ? 'Ada Pekerjaan \n\n\n' : '',
         start: new Date(year, month, day),
         backgroundColor: '#00a65a',
         borderColor: '#00a65a',
         tgl: year.toString() + "-" + month.toString() + "-" + day,
         url: url.base_url("pekerjaan") + "view/" + tgl_fix + '/' + upt + '/' + id_place,
        });
       }
      } else {
       var month_url = month < 10 ? "0" + (month + 1).toString() : month.toString();
       var day_url = day < 10 ? "0" + (day + 1).toString() : day.toString();
       var tgl_fix = year.toString() + "-" + month_url + "-" + day_url;
       event.push({
        title: 'Ada Pekerjaan',
        start: new Date(year, month, day),
        tgl: year.toString() + "-" + month.toString() + "-" + day,
        backgroundColor: '#00a65a',
        borderColor: '#00a65a',
        url: url.base_url("pekerjaan") + "view/" + tgl_fix + '/' + upt + '/' + id_place,
       });
      }

      temp_date_awal_pekerjaan.push(tgl_pekerjaan);
     }
    }

    for (var x = 0; x < temp_date_awal_pekerjaan.length; x++) {
     var head_tgl = temp_date_awal_pekerjaan[x];
     for (var i = 0; i < data.length; i++) {
      var tgl_pekerjaan = data[i].tgl_pekerjaan;
      var value = data[i];
      var month = data[i].month;
      var month_end = data[i].month_end;
      var year = data[i].year;
      var day = data[i].day;
      var year_end = data[i].year_end;
      var day_end = data[i].day_end;
      var tgl_pekerjaan = data[i].tgl_pekerjaan;
      var tgl_akhir = data[i].tgl_akhir;
      var status = data[i].status;
      var dif_day = day_end - day;
//      console.log('dif day', dif_day);
      if (tgl_pekerjaan == temp_date_awal_pekerjaan[x]) {
       if (tgl_akhir != tgl_pekerjaan) {
        if (dif_day > 0) {
         for (var y = 1; y < dif_day + 1; y++) {
          day = day + 1;
          var month_url = month < 10 ? "0" + (month + 1).toString() : month.toString();
          var day_url = day < 10 ? "0" + (day + 1).toString() : day.toString();
          var tgl_fix = year.toString() + "-" + month_url + "-" + day_url;
          event.push({
           title: '\n\n\n',
           start: new Date(year, month, day),
           tgl: year.toString() + "-" + month.toString() + "-" + day,
           backgroundColor: '#00a65a',
           borderColor: '#00a65a',
           url: url.base_url("pekerjaan") + "view/" + tgl_fix + '/' + upt + '/' + id_place,
          });
         }
        }
       }
      }
     }
    }

//
    var event_result = [];
    var temp = [];
    for (var i = 0; i < event.length; i++) {
//     console.log(event[i]);
     if ($.inArray(event[i].tgl, temp) == -1) {
      event_result.push(event[i]);
      temp.push(event[i].tgl);
     }
    }

//    console.log(event_result);
    $('#calendar').fullCalendar('addEventSource', event_result);

    var fc_time = $('span.fc-time');
    $.each(fc_time, function () {
     $(this).text('');
    });
   }
  });
 },

 detail: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Pekerjaan.module()) + "detailPermit/" + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 expanSidebar: function () {
  var sidebar = $('a.sidebar-toggle');
  sidebar.trigger('click');
 }
};


$(function () {
 Pekerjaan.expanSidebar();
 Pekerjaan.setDateData();

 $('.fc-corner-right[aria-label="next"]').click(function () {
  var date = $('.fc-center').find('h2');
  var date = date.text();
  Pekerjaan.updateCalendarEvent(date);
 });

 $('.fc-corner-left[aria-label="prev"]').click(function () {
  var date = $('.fc-center').find('h2');
  var date = date.text();
  Pekerjaan.updateCalendarEvent(date);
 });

});