var LapEksternal = {
 module: function () {
  return 'lapeksternal';
 },

 add: function () {
  window.location.href = url.base_url(LapEksternal.module()) + "add";
 },

 main: function () {
  window.location.href = url.base_url(LapEksternal.module()) + "index";
 },

 back: function () {
  window.location.href = url.base_url(LapEksternal.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(LapEksternal.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(LapEksternal.module()) + "index";
   }
  }
 },

 searchByUpt: function (elm) {
  var keyWord = $(elm).val();
  if (keyWord != '') {
   window.location.href = url.base_url(LapEksternal.module()) + "searchByUpt" + '/' + keyWord;
  } else {
   window.location.href = url.base_url(LapEksternal.module()) + "index";
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'form_pemohon': {
    'pemohon_id': $('select#pemohon').val(),
    'nama_pemohon': $('#nama_pemohon').val(),
    'perusahaan': $('#perusahaan').val(),
    'no_hp': $('#no_hp').val(),
    'no_telp': $('#no_telp').val(),
    'jabatan': $('#jabatan').val(),
    'email': $('#email').val(),
    'alamat': $('textarea#alamat').val(),
   },
   'form_wp': {
    'tgl_wp': $('#tgl_wp').val(),
    'tgl_pekerjaan': $('#tgl_pekerjaan').val(),
    'tgl_awal': $('#tgl_awal').val(),
    'tgl_akhir': $('#tgl_akhir').val(),
   },
   'form_work': {
    'work_place': $('select#work_place').val(),
    'place': $('select#place').val(),
    'lokasi_kerja': $('#lokasi_kerja').val(),
    'file_str': $('input#file_str').val(),
    'uraian_pekerjaan': $('#uraian_pekerjaan').val(),
   },
   'form_purpose': {
    'upt': LapEksternal.getUptPurpose()
   },
   'form_tj': {
    'data': LapEksternal.getPostTj()
   },
   'form_pelaksana': {
    'data': LapEksternal.getPostPelaksana()
   },
   'form_apd': {
    'data': LapEksternal.getPostApd()
   },
   'form_ibppr': {
    'data': LapEksternal.getPostIbppr()
   },
   'form_jsa': {
    'data': LapEksternal.getPostJsa()
   },
  };

  return data;
 },

 getPostIbppr: function () {
  var data = [];
  var table_data = $('table#tb_ibppr').find('tbody').find('tr');
  $.each(table_data, function () {
   data.push({
    'id': $(this).attr('data_id'),
    'kegiatan': $(this).find('td:eq(0)').find('textarea').val(),
    'potensi_bahaya': $(this).find('td:eq(1)').find('textarea').val(),
    'resiko': $(this).find('td:eq(2)').find('textarea').val(),
    'akibat': $(this).find('td:eq(3)').find('select').val(),
    'paparan': $(this).find('td:eq(4)').find('select').val(),
    'peluang': $(this).find('td:eq(5)').find('select').val(),
    'nilai': $(this).find('td:eq(6)').find('input').val(),
    'tingkat_resiko': $(this).find('td:eq(7)').find('input').val(),
    'pengendalian': $(this).find('td:eq(8)').find('textarea').val(),
    'akibat_after': $(this).find('td:eq(9)').find('select').val(),
    'paparan_after': $(this).find('td:eq(10)').find('select').val(),
    'peluang_after': $(this).find('td:eq(11)').find('select').val(),
    'nilai_after': $(this).find('td:eq(12)').find('input').val(),
    'tingkat_resiko_after': $(this).find('td:eq(13)').find('input').val(),
    'deleted': $(this).hasClass('deleted') ? '1' : '0'
   });
  });

  return data;
 },

 getPostJsa: function () {
  var data = [];
  var table_data = $('table#tb_jsa').find('tbody').find('tr');
  $.each(table_data, function () {
   data.push({
    'id': $(this).attr('data_id'),
    'tahapan': $(this).find('#tahapan').val(),
    'potensi': $(this).find('#potensi').val(),
    'pengendalian': $(this).find('#pengendalian').val(),
    'deleted': $(this).hasClass('deleted') ? '1' : '0'
   });
  });

  return data;
 },

 getPostApd: function () {
  var data = [];
  var table_data = $('table#tb_apd').find('tbody').find('tr');
  $.each(table_data, function () {
   data.push({
    'id': $(this).attr('data_id'),
    'nama_alat': $(this).find('input#nama_alat').val(),
    'ketersediaan': $(this).find('input#ketersediaan').val(),
    'deleted': $(this).hasClass('deleted') ? '1' : '0'
   });
  });

  return data;
 },

 getPostTj: function () {
  var data = [];
  var table_data = $('table#tb_penganggung_jawab').find('tbody').find('tr');
  $.each(table_data, function () {
   data.push({
    'id': $(this).attr('data_id'),
    'nama': $(this).find('input#nama').val(),
    'jabatan': $(this).find('input#jabatan_pengawas').val(),
    'file_sk3': $(this).find('input#file_sk3').prop('files')[0],
    'file_str': $(this).find('input#file_str').val(),
    'deleted': $(this).hasClass('deleted') ? '1' : '0'
   });
  });

  return data;
 },

 getPostPelaksana: function () {
  var data = [];
  var table_data = $('table#tb_pelaksana').find('tbody').find('tr');
  $.each(table_data, function () {
   data.push({
    'id': $(this).attr('data_id'),
    'nama': $(this).find('input#nama').val(),
    'deleted': $(this).hasClass('deleted') ? '1' : '0'
   });
  });

  return data;
 },

 getUptPurpose: function () {
  var upt = '';
  var option = $('select#place').find('option');
  $.each(option, function () {
   if ($(this).is(':selected')) {
    upt = $(this).attr('upt');
   }
  });

  return upt;
 },

 simpan: function (id, e) {
  e.preventDefault();
  var data = LapEksternal.getPostData();

//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  var table_data = $('table#tb_penganggung_jawab').find('tbody').find('tr');
  var index = 0;
  $.each(table_data, function () {
   var file_sk3 = $(this).find('input#file_sk3').prop('files')[0];
   formData.append('file_tj_' + index, file_sk3);
   index += 1;
  });

  formData.append('file_spk', $('input#file_spk').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(LapEksternal.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(LapEksternal.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error(resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(LapEksternal.module()) + "ubah/" + id;
 },

 detail: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(LapEksternal.module()) + "detail/" + id,
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 delete: function (id) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12 text-center'>";
  html += "<h5>Apa anda yakin akan menghapus data ini ?</h5>";
  html += "<div class='text-center'>";
  html += "<button class='btn btn-success font-10'onclick='LapEksternal.execDeleted(" + id + ")'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 execDeleted: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(LapEksternal.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(LapEksternal.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="LapEksternal.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailAgama: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_agama_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_agama_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(2)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="LapEksternal.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 addDetailKesehatan: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var seq = tr.find('td:eq(0)').find('input').attr('id');
  seq = parseInt(seq.toString().replace('tanggal_kesehatan_', ''));
  var next_id = seq + 1;
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(0)').find('input')
          .attr('id', 'tanggal_kesehatan_' + next_id)
          .removeClass('hasDatepicker')
          .removeData('datepicker')
          .unbind()
          .datepicker({
           format: 'yyyy-mm-dd',
           autoclose: true,
          });
  newTr.find('td:eq(3)').html('<i class="fa fa-minus-circle fa-2x hover" onclick="LapEksternal.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 upload: function (elm) {
  $('input#file').click();
 },

 getFilename: function (elm) {
  LapEksternal.checkFile(elm);
 },

 checkFile: function (elm) {
  if (window.FileReader) {
   var data_file = $(elm).get(0).files[0];
   var file_name = data_file.name;
   var data_from_file = data_file.name.split('.');

   var type_file = $.trim(data_from_file[data_from_file.length - 1]);
   if (type_file == 'png' || type_file == 'jpeg' || type_file == 'jpg' || type_file == 'pdf') {
    if (data_file.size <= 1324000) {
     $(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
    } else {
     toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
     message.closeLoading();
    }
   } else {
    toastr.error('File Harus Berformat Png, Jpg, Jpeg, Pdf');
    $(elm).val('');
    message.closeLoading();
   }
  } else {
   toastr.error('FileReader is Not Supported');
   message.closeLoading();
  }
 },

 showLogo: function (elm, e, jenis = 'spk') {
  e.preventDefault();
  $.ajax({
   type: 'POST',
   data: {
    jenis: jenis,
    foto: $.trim($(elm).attr('file'))
   },
   dataType: 'html',
   async: false,
   url: url.base_url(LapEksternal.module()) + "showLogo",
   success: function (resp) {
    bootbox.dialog({
     message: resp,
//     size: 'large'
    });
   }
  });
 },

 changeManual: function (elm) {
  $('div.manual_detail').addClass('display-none');
  $('div.manual_add').removeClass('display-none');
  $('div.manual_add').append("<i class='mdi mdi-close mdi-24px hover' onclick='LapEksternal.cancelChangeManual(this)'></i>");
 },

 cancelChangeManual: function (elm) {
  $('div.manual_detail').removeClass('display-none');
  $('div.manual_add').addClass('display-none');
  $('i.mdi-close').remove();

  var inputFile = '<div class="form-control" data-trigger="fileinput"> ';
  inputFile += '<i class="glyphicon glyphicon-file fileinput-exists"></i>';
  inputFile += '<span class="fileinput-filename"></span>';
  inputFile += '</div> ';
  inputFile += '<span class="input-group-addon btn btn-default btn-file"> ';
  inputFile += '<span class="fileinput-new" onclick="LapEksternal.upload(this)">Select file</span> ';
  inputFile += '<input type="file" style="display: none;" id="file" onchange="LapEksternal.getFilename(this)"/>';
  inputFile += '</span>';
  $('div.manual_upload').html(inputFile);
 },

 showUpdateFoto: function (elm) {
  bootbox.dialog({
   message: 'Ganti Foto'
  });
 },

 showTooltip: function (elm) {

 },

 setDate: function () {
  $('#tanggal').daterangepicker();
 },

 setDataTable: function () {
  $('#tb_content').DataTable({
   'paging': false,
   'lengthChange': true,
   'searching': false,
   'ordering': true,
   'info': false,
   'autoWidth': false
  })
 },

 getDetailTempat: function (elm) {
  var work_place = $(elm).val();
  $.ajax({
   type: 'POST',
   data: {
    work_place: work_place
   },
   dataType: 'html',
   async: false,
   url: url.base_url(LapEksternal.module()) + "getDetailTempat",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('div.content_place').html(resp);
    LapEksternal.setSelect2();
   }
  });
 },

 setSelect2: function () {
  $('select#pemohon').select2();
  $('select#work_place').select2();
  $('select#place').select2();
 },

 checkNeed: function (elm) {
  var checked = $(elm).is(':checked');
  $('textarea#keterangan_need_sistem').val("");
  if (checked) {
   $('div#content_ket_need').removeClass('hidden');
  } else {
   $('div#content_ket_need').addClass('hidden');
  }
 },

 addPenanggunJawab: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:last').html('<i class="fa fa-minus fa-lg hover-content" onclick="LapEksternal.removePenanggunJawab(this)"></i>');
  tr.after(newTr);
 },

 removePenanggunJawab: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id == '') {
   $(elm).closest('tr').remove();
  } else {
   $(elm).closest('tr').addClass('deleted');
   $(elm).closest('tr').addClass('hidden');
  }
 },

 addPelaksana: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:last').html('<i class="fa fa-minus fa-lg hover-content" onclick="LapEksternal.removePelaksana(this)"></i>');
  tr.after(newTr);
 },

 removePelaksana: function (elm) {
  var id = $(elm).closest('tr').attr('data_id');
  if (id == '') {
   $(elm).closest('tr').remove();
  } else {
   $(elm).closest('tr').addClass('deleted');
   $(elm).closest('tr').addClass('hidden');
  }
 },

 addAlatKerja: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('select').val('');
  newTr.find('td:last').html('<i class="fa fa-minus fa-lg hover-content" onclick="LapEksternal.removeAlatKerja(this)"></i>');
  tr.after(newTr);
 },

 removeAlatKerja: function (elm) {
  var id = $(elm).closest('tr').attr('data_id');
  if (id == '') {
   $(elm).closest('tr').remove();
  } else {
   $(elm).closest('tr').addClass('deleted');
   $(elm).closest('tr').addClass('hidden');
  }
 },

 addIbppr: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('select').val('');
  newTr.find('textarea').val('');
  newTr.find('td:last').html('<i class="fa fa-minus fa-lg hover-content" onclick="LapEksternal.removeIbppr(this)"></i>');
  tr.after(newTr);
 },

 removeIbppr: function (elm) {
  var id = $(elm).closest('tr').attr('data_id');
  if (id == '') {
   $(elm).closest('tr').remove();
  } else {
   $(elm).closest('tr').addClass('deleted');
   $(elm).closest('tr').addClass('hidden');
  }
 },

 addJsa: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('textarea').val('');
  newTr.find('td:last').html('<i class="fa fa-minus fa-lg hover-content" onclick="LapEksternal.removeJsa(this)"></i>');
  tr.after(newTr);
 },

 removeJsa: function (elm) {
  if (id == '') {
   $(elm).closest('tr').remove();
  } else {
   $(elm).closest('tr').addClass('deleted');
   $(elm).closest('tr').addClass('hidden');
  }
 },

 getPemohon: function (elm) {
  var pemohon = $(elm).val();
  if (pemohon == "add_pemohon") {
   LapEksternal.resetFormPemohon();
  } else {
   LapEksternal.disabledFormPemohon();
  }
 },

 disabledFormPemohon: function () {
  $('#nama_pemohon').attr('disabled', '');
  $('#perusahaan').attr('disabled', '');
  $('#no_hp').attr('disabled', '');
  $('#no_telp').attr('disabled', '');
  $('#jabatan').attr('disabled', '');
  $('#email').attr('disabled', '');
  $('#alamat').attr('disabled', '');
 },

 resetFormPemohon: function () {
  $('#nama_pemohon').val('');
  $('#nama_pemohon').removeAttr('disabled');
  $('#perusahaan').val('');
  $('#perusahaan').removeAttr('disabled');
  $('#no_hp').val('');
  $('#no_hp').removeAttr('disabled');
  $('#no_telp').val('');
  $('#no_telp').removeAttr('disabled');
  $('#jabatan').val('');
  $('#jabatan').removeAttr('disabled');
  $('#email').val('');
  $('#email').removeAttr('disabled');
  $('#alamat').val('');
  $('#alamat').removeAttr('disabled');
 },

 hitungNilai: function (elm, jenis) {
  var tr = $(elm).closest('tr');

  if (jenis == 'after') {
   var akibat_id = tr.find('td:eq(9)').find('select').val();
   var akibat_nilai = tr.find('td:eq(9)').find('select option[value="' + akibat_id + '"]').attr('nilai');
   var paparan_id = tr.find('td:eq(10)').find('select').val();
   var paparan_nilai = tr.find('td:eq(10)').find('select option[value="' + paparan_id + '"]').attr('nilai');
   var peluang_id = tr.find('td:eq(11)').find('select').val();
   var peluang_nilai = tr.find('td:eq(11)').find('select option[value="' + peluang_id + '"]').attr('nilai');

   var total = parseFloat(akibat_nilai) * parseFloat(paparan_nilai) * parseFloat(peluang_nilai);

   tr.find('td:eq(12)').find('input').val(total);

   var tingkat_resiko = "Tinggi";
   if (total >= 0 && total <= 20) {
    tingkat_resiko = "Rendah";
   }

   if (total > 20 && total <= 40) {
    tingkat_resiko = "Medium";
   }

   tr.find('td:eq(13)').find('input').val(tingkat_resiko);
  } else {
   var akibat_id = tr.find('td:eq(3)').find('select').val();
   var akibat_nilai = tr.find('td:eq(3)').find('select option[value="' + akibat_id + '"]').attr('nilai');
   var paparan_id = tr.find('td:eq(4)').find('select').val();
   var paparan_nilai = tr.find('td:eq(4)').find('select option[value="' + paparan_id + '"]').attr('nilai');
   var peluang_id = tr.find('td:eq(5)').find('select').val();
   var peluang_nilai = tr.find('td:eq(5)').find('select option[value="' + peluang_id + '"]').attr('nilai');

   var total = parseFloat(akibat_nilai) * parseFloat(paparan_nilai) * parseFloat(peluang_nilai);

   tr.find('td:eq(6)').find('input').val(total);

   var tingkat_resiko = "Tinggi";
   if (total >= 0 && total <= 20) {
    tingkat_resiko = "Rendah";
   }

   if (total > 20 && total <= 40) {
    tingkat_resiko = "Medium";
   }

   tr.find('td:eq(7)').find('input').val(tingkat_resiko);
  }
 },

 gantiFile: function (elm, e) {
  e.preventDefault();
  var file_input = $('div#file_input_spk');
  file_input.removeClass('hidden');

  $('div#detail_file_spk').addClass('hidden');
 },

 gantiFileItem: function (elm, e) {
  e.preventDefault();
  var file_input = $(elm).closest('td').find('div#file_input_sk3');
  console.log(file_input);
  file_input.removeClass('hidden');

  $(elm).closest('td').find('div#detail_file_sk3').addClass('hidden');
 },

 showConfirm: function (id, action) {
  var html = "<div class='row'>";
  html += "<div class='col-md-12'>";
  html += "<h5><u>Keterangan Di Tolak</u></h5>";
  html += "<textarea class='form-control required' error='Keterangan' id='keterangan_pop'></textarea>";
  html += '<br/>';
  html += "<div class='text-right'>";
  html += "<button action='" + action + "' data_id='" + id + "' class='btn btn-success font-10'onclick='LapEksternal.execApprove(this)'><i class='fa fa-check'></i>Proses</button>&nbsp;";
  html += "<button class='btn btn-warning font-10' onclick='message.closeDialog()'><i class='fa fa-close'></i>Batal</button>&nbsp;";
  html += "</div>";
  html += "</div>";
  html += "</div>";

  bootbox.dialog({
   message: html,
  });
 },

 reject: function (id) {
  LapEksternal.showConfirm(id, 'reject');
 },

 approve: function (id) {
  LapEksternal.showConfirm(id, 'approve');
 },

 execApprove: function (elm) {
  var id = $(elm).attr('data_id');
  var action = $(elm).attr('action');
  var keterangan = $('#keterangan_pop').val();

  if (keterangan == '') {
   toastr.error("Keterangan Harus Diisi");
   return;
  }
  if (keterangan != '') {
   $.ajax({
    type: 'POST',
    data: {
     permit: id,
     action: action,
     keterangan: keterangan
    },
    dataType: 'json',
    async: false,
    url: url.base_url(LapEksternal.module()) + "execApprove",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Approve...");
    },

    success: function (resp) {
     message.closeLoading();
     message.closeDialog();
     if (resp.is_valid) {
      toastr.success("Berhasil Approve");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
    }
   });
  }
 },

 cetak: function (id) {
  window.location.href = url.base_url("wpinternal") + "cetak/" + id;
 },

 tampilkan: function (elm) {
  var tanggal = $('input#tanggal').val();
  $.ajax({
   type: 'POST',
   data: {
    tanggal: tanggal
   },
   dataType: 'html',
   async: false,
   url: url.base_url(LapEksternal.module()) + "tampilkan",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data Laporan...");
   },

   success: function (resp) {
    message.closeLoading();

    $('table#tb_content').find('tbody').html(resp);
   }
  });
 }
};

$(function () {
 LapEksternal.setDate();
 LapEksternal.setSelect2();
});