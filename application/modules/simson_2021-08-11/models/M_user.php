
<?php

class M_user extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getUser($user_id = "")
	{
		$sql = "
		select u.*
		, ut.id as upt 
		, ut.nama as nama_upt
		from `user` u
		join pegawai p
			on p.id = u.pegawai
		join upt ut
			on ut.id = p.upt 
		where u.deleted = 0
		and u.id = " . $user_id;

		$data = $this->db->query($sql);

		if (!empty($data->result_array())) {
			return $data->row_array();
		}

		return array();
	}
}
