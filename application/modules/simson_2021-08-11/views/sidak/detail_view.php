<div class="row">
	<div class="col-md-12">
		<a class="btn btn-danger" href="<?php echo base_url() ?>simson/sidak/cetak?permit=<?php echo $permit ?>&no_wp=<?php echo $no_wp ?>">CETAK PDF</a>
	</div>
</div>
<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Safety Condition</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_sidak)) { ?>
							<div class="row">
								<div class="col-md-2">
									<label for="">Kondisi Tanah</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['kondisi_tanah'] == '' ? '-' : $data_sidak['kondisi_tanah'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Tegangan TK</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['tegangan_tk'] == '' ? '-' : $data_sidak['tegangan_tk'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Kondisi Lingkungan</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['kondisi_lingkungan'] == '' ? '-' : $data_sidak['kondisi_lingkungan'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Lokasi</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['lokasi'] == '' ? '-' : $data_sidak['lokasi'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Kondisi Cuaca</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['kondisi_cuaca'] == '' ? '-' : $data_sidak['kondisi_cuaca'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Status</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['status'] == '' ? '-' : $data_sidak['status'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Ketinggian TK</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_sidak['ketinggian_tk'] == '' ? '-' : $data_sidak['ketinggian_tk'] ?></label>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Kelengkapan & Peralatan APD</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_sidak)) { ?>
							<div class="row">
								<div class="col-md-2">
									<label for="">Body Harnes</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['body_harnes'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Lanyard</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['lanyard'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Hook / Step</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['hook_or_step'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Pullstrap</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['pullstrap'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Safety Helmet</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['safety_helmet'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Kacamata UV</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['kacamata_uv'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Earplug</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['earplug'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Masker</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['masker'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Wearpack</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['wearpack'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Sepatu Safety</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['sepatu_safety'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Sarung Tangan</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['sarung_tangan'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Stick Grounding</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['stick_grounding'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Grounding Cable</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['grounding_cable'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Stick Isolasi</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['stick_isolasi'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Voltage Detector</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['voltage_detector'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Tambang</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['tambang'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Tagging</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['tagging'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Pembatas Area</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_sidak['pembatas_area'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Gambar Sidak</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_image)) { ?>
							<div class="row">
								<?php foreach ($data_image as $key => $value) { ?>
									<div class="col-md-6">
										<br>
										<img src="data:image/jpeg;base64,<?php echo $value['picture'] ?>" alt="">
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
