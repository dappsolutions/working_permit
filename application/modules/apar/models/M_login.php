
<?php

class M_login extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function signIn($params)
	{
		$sql = "
		select rap.*
		, u.id as user_id
		, u.username as email
		, pg.nama as name
		, ut.id as upt_id
		, w.nama_wilayah
		, pg.id as pegawai_id
		, aa.id as approval_id
		from role_apps_akses rap 
		join `user` u 
			on u.id = rap.`user`
		join apps a 
			on a.id = rap.apps
		join pegawai pg
			on pg.id = u.pegawai			
		join upt ut
			on ut.id = pg.upt
		join role_apps_apar raa
			on raa.role_apps_akses = rap.id
		join wilayah w
			on w.id = raa.wilayah
		left join approval_apar aa 
			on aa.pegawai = pg.id 
		where a.aplikasi = 'peralatan'
		and rap.deleted = 0
		and u.username = '" . $params['username'] . "'
		and u.password = '" . $params['password'] . "'";

		$data = $this->db->query($sql);

		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
			$result['lokasi_tujuan'] = 0;
			$result['id_tujuan'] = 0;
			if ($result['nama_wilayah'] == 'GI') {
				//cari spv gi terkait
				$dataSpv = $this->getDataSpvGi($result);
				if(!empty($dataSpv)){
					$dataLokasiTujuanId = [];
					$idTujuan = [];
					foreach ($dataSpv as $key => $value) {
						$dataLokasiTujuanId[] = $value['id'];
						$idTujuan[] = $value['id_tujuan'];
					}

					// $result['lokasi_tujuan'] = $dataSpv['id'];
					// $result['id_tujuan'] = $dataSpv['id_tujuan'];
					$result['lokasi_tujuan'] = implode(',', $dataLokasiTujuanId);
					$result['id_tujuan'] = implode(',', $idTujuan);
				}
			}
			if($result['nama_wilayah'] == 'ULTG'){
				//cari pic ultg
				$dataPic = $this->getDataPicUltg($result);
				if(!empty($dataPic)){
					$dataLokasiTujuanId = [];
					$idTujuan = [];
					foreach ($dataPic as $key => $value) {
						$dataLokasiTujuanId[] = $value['id'];
						$idTujuan[] = $value['id_tujuan'];
					}
					$result['lokasi_tujuan'] = implode(',', $dataLokasiTujuanId);
					$result['id_tujuan'] = implode(',', $idTujuan);
				}
			}
		}

		return $result;
	}

	public function getDataSpvGi($params)
	{
		$sql = "select 
		lt.*
		from gardu_induk_has_pegawai gihp 
		join lokasi_tujuan lt 
			on lt.id_tujuan = gihp.gardu_induk 
		join lokasi_apar la
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		where gihp.pegawai = " . $params['pegawai_id'] . "
		and gihp.deleted = 0
		and w.nama_wilayah = 'GI'";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			// $result = $data->row_array();
			$result = $data->result_array();
		}

		return $result;
	}
	
	public function getDataPicUltg($params)
	{
		$sql = "select lt.* 
		from lokasi_tujuan lt 
		join lokasi_apar la
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah
		join sub_upt su 
			on su.id = lt.id_tujuan 
		join ultg_pegawai up
			on up.sub_upt = su.id 
		where w.nama_wilayah = 'ULTG'
		and up.pegawai = ".$params['pegawai_id'];

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			// $result = $data->row_array();
			$result = $data->result_array();
		}

		return $result;
	}
}
