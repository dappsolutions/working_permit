
<?php

class M_alat extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				if ($params['wilayah'] == 'GI') {
					$filterWilayah = " and w.nama_wilayah = 'GI'";
				}
			}
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		, lt.id as lokasi_tujuan
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = lpa.jenis_alat 
		join merk_alat ma 
			on ma.id = lpa.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		order by w.nama_wilayah asc, atl.id asc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}
	
	public function getDataApprovalData($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			// $filterWilayah = " and w.nama_wilayah = '".$params['wilayah']."'";
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK') {
					$filterWilayah = " and w.nama_wilayah = 'GI'";
				}

				if ($params['wilayah'] == 'UPT' || $params['wilayah'] == 'UPT/KANTOR') {
					$filterWilayah = " and (w.nama_wilayah = 'UPT' or w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI') ";
				}

				if ($params['wilayah'] == 'GEDUNG') {
					$filterWilayah = " and w.nama_wilayah = 'GEDUNG'";
				}
			}
		}

		$filterTujuan = "";
		if ($filterWilayah != '') {
			if (isset($params['id_tujuan'])) {
				if ($params['id_tujuan'] != '0') {
					if ($params['id_tujuan'] != '') {
						if($params['user_detail_akses']['nama_wilayah'] == 'UPT'){
							if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK' || $params['wilayah'] == 'ULTG' || $params['wilayah'] == 'UPT') {
								$filterTujuan = " ";
							}
						}
						
						if($params['user_detail_akses']['nama_wilayah'] == 'ULTG'){
							if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK') {
								$idTujuanList = $params['user_detail_akses']['list_gi'];
								$filterTujuan = " and lt.id_tujuan in (" . $idTujuanList . ")";
							}
							if ($params['wilayah'] == 'ULTG') {
								$filterTujuan = " and lt.id_tujuan in (" . $params['id_tujuan'] . ")";
							}
						}

						if($params['user_detail_akses']['nama_wilayah'] == 'GI' || $params['user_detail_akses']['nama_wilayah'] == 'GARDU INDUK'){
							$filterTujuan = " and lt.id_tujuan in (" . $params['id_tujuan'] . ")";
						}

					}
				}
			}
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		, lt.id as lokasi_tujuan
		, atp.id as atp_id
		, d.no_document 
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = lpa.jenis_alat 
		join merk_alat ma 
			on ma.id = lpa.merk_alat 		
		join (
			select max(id) id, alat, lokasi_tujuan from alat_transaksi_pemeliharaan group by alat, lokasi_tujuan
		) atp_max
			on atp_max.alat = atl.id
		join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
				and atp.lokasi_tujuan = lt.id 
		join document d 
			on d.id = atp.document 
		where atl.deleted = 0
		and d.approve_by is NULL 	
		and d.deleted = 0
		".$filterWilayah."
		-- ".$filterTujuan."
		order by w.nama_wilayah asc, atl.id asc";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataFilter($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filterDate = "";
		if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] != '') {
			$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
			$params['tanggal_akhir'] = date('Y-m-d', strtotime($params['tanggal_akhir']));
			$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_akhir'] . "')";
		}

		if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] == '') {
			$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
			$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_awal'] . "')";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				if ($params['wilayah'] == 'GI') {
					$filterWilayah = " and w.nama_wilayah = 'GI'";
				}
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		, lt.id as lokasi_tujuan
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = lpa.jenis_alat 
		join merk_alat ma 
			on ma.id = lpa.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		" . $filterDate . "
		order by w.nama_wilayah asc, atl.id asc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataNotifikasiApar($params = array(), $limit = true)
	{

		$dateNotifFilter = "and atp.createddate is null";
		$dateNow = date('Y-m-d');
		list($year, $month, $day) = explode('-', $dateNow);
		if (isset($params['wilayah'])) {
			$dateNotifFilter = "";
			// if (intval($day) == 20) {
			// 	if ($params['wilayah'] == 'ULTG') {
			// 		$dateNotifFilter = "and (cast(atp.createddate as date) < '" . $dateNow . "' or atp.createddate is null)";
			// 	}
			// } elseif (intval($day) >= 25) {
			// 	if ($params['wilayah'] == 'ULTG' || $params['wilayah'] == 'UPT') {
			// 		$dateNotifFilter = "and (cast(atp.createddate as date) < '" . $dateNow . "' or atp.createddate is null)";
			// 	}
			// }
		}

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				$filterWilayah = " and w.nama_wilayah = 'GI'";
			}
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		, atp.createddate as tanggal_inspeksi_terakhir
		, lt.id as lokasi_tujuan
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = lpa.jenis_alat 
		join merk_alat ma 
			on ma.id = lpa.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0	
		" . $dateNotifFilter . "	
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		order by w.nama_wilayah, atl.id asc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['bulan_terakhir_pemeliharaan'] = '';
				$value['has_pemeliharaan'] = 0;
				if ($value['tanggal_inspeksi_terakhir'] != '') {
					$value['bulan_terakhir_pemeliharaan'] = date('Y-m', strtotime($value['tanggal_inspeksi_terakhir']));
					$monthNow = date('Y-m');
					$value['periode_sekarang'] = $monthNow;
					if ($value['bulan_terakhir_pemeliharaan'] == $monthNow) {
						$value['has_pemeliharaan'] = 1;
					} else {
						$value['has_pemeliharaan'] = -1;
					}
				}

				if($value['has_pemeliharaan'] != 1){
					array_push($result, $value);
				}
			}
		}

		return $result;
	}

	public function getDataFilterRabApar($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filterDate = "";
		if (isset($params['tanggal_awal']) && isset($params['tanggal_akhir'])) {
			if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] != '') {
				$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
				$params['tanggal_akhir'] = date('Y-m-d', strtotime($params['tanggal_akhir']));
				$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_akhir'] . "')";
			}

			if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] == '') {
				$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
				$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_awal'] . "')";
			}
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				if ($params['wilayah'] == 'GI') {
					$filterWilayah = " and w.nama_wilayah = 'GI'";
				}
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = lpa.jenis_alat 
		join merk_alat ma 
			on ma.id = lpa.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		" . $filterDate . "
		order by w.nama_wilayah, atl.id, atp.createddate asc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataAlatByWilayah($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			// $filterWilayah = " and w.nama_wilayah = '".$params['wilayah']."'";
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG')";
			} else {
				if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK') {
					$filterWilayah = " and w.nama_wilayah = 'GI'";
				}

				if ($params['wilayah'] == 'UPT' || $params['wilayah'] == 'UPT/KANTOR') {
					$filterWilayah = " and w.nama_wilayah = 'UPT'";
				}

				if ($params['wilayah'] == 'GEDUNG') {
					$filterWilayah = " and w.nama_wilayah = 'GEDUNG'";
				}
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$filterTujuan = "";
		if ($filterWilayah != '') {
			if (isset($params['id_tujuan'])) {
				if ($params['id_tujuan'] != '0') {
					if ($params['id_tujuan'] != '') {
						if($params['user_detail_akses']['nama_wilayah'] == 'UPT'){
							if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK' || $params['wilayah'] == 'ULTG' || $params['wilayah'] == 'UPT') {
								// $filterTujuan = " and lt.id_tujuan = '" . $params['id_tujuan'] . "'";
								$filterTujuan = " ";
							}
						}
						
						if($params['user_detail_akses']['nama_wilayah'] == 'ULTG'){
							if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK') {
								// $filterTujuan = " and lt.id_tujuan = '" . $params['id_tujuan'] . "'";
								$idTujuanList = $params['user_detail_akses']['list_gi'];
								$filterTujuan = " and lt.id_tujuan in (" . $idTujuanList . ")";
							}
							if ($params['wilayah'] == 'ULTG') {
								// $filterTujuan = " and lt.id_tujuan = '" . $params['id_tujuan'] . "'";
								$filterTujuan = " and lt.id_tujuan in (" . $params['id_tujuan'] . ")";
							}
						}

						if($params['user_detail_akses']['nama_wilayah'] == 'GI' || $params['user_detail_akses']['nama_wilayah'] == 'GARDU INDUK'){
							$filterTujuan = " and lt.id_tujuan in (" . $params['id_tujuan'] . ")";
						}

					}
				}
			}
		}

		$sql = "select 
		distinct
		la.nama_lokasi 
		, lt.id_tujuan as id_lokasi
		from lokasi_penempatan_alat lpl		
		join alat a
			on a.id = lpl.alat
		join lokasi_penempatan lp
			on lp.id = lpl.lokasi_penempatan 
		join lokasi_tujuan lt
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w
			on w.id = la.wilayah 
		where a.deleted = 0
		" . $filterWilayah . "
		" . $filterTujuan . "
		order by 
		w.nama_wilayah, a.id, la.nama_lokasi, lp.tempat";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataTempatLokasiApar($params = array(), $limit = true)
	{

		// $filterWilayah = '';
		$filterWilayahTujuan = " and lt.id_tujuan = '" . $params['id_lokasi'] . "'";

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			// $filterWilayah = " and w.nama_wilayah = '".$params['wilayah']."'";
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG')";
			} else {
				if ($params['wilayah'] == 'GI' || $params['wilayah'] == 'GARDU INDUK') {
					$filterWilayah = " and w.nama_wilayah = 'GI'";
				}

				if ($params['wilayah'] == 'UPT' || $params['wilayah'] == 'UPT/KANTOR') {
					$filterWilayah = " and w.nama_wilayah = 'UPT'";
				}

				if ($params['wilayah'] == 'GEDUNG') {
					$filterWilayah = " and w.nama_wilayah = 'GEDUNG'";
				}
			}
		}

		$sql = "select 
		a.no_alat
		, a.id id_alat
		, la.nama_lokasi 
		, lp.tempat 
		, w.nama_wilayah 
		, lt.id_tujuan as id_lokasi
		, d.no_document 
		, cast(d.createddate as date) as tgl_pemeliharaan
		, dt.status
		, lt.id as lokasi_tujuan
		from lokasi_penempatan_alat lpl		
		join alat a
			on a.id = lpl.alat
		join lokasi_penempatan lp
			on lp.id = lpl.lokasi_penempatan 
		join lokasi_tujuan lt
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w
			on w.id = la.wilayah 
		left join (
			select max(id) id, lokasi_tujuan, alat from alat_transaksi_pemeliharaan group by alat, lokasi_tujuan  
		) atp_max
			on atp_max.alat = a.id
			and atp_max.lokasi_tujuan = lt.id
		left join alat_transaksi_pemeliharaan atp 
			on atp.id = atp_max.id
			and atp.lokasi_tujuan  = lt.id 
		left join document d 
			on d.id = atp.document 
			and d.deleted = 0
		left join (
			select max(id) id, document from document_transaction group by document
		) doc_t_max
			on doc_t_max.document = d.id
		left join document_transaction dt 
			on dt.id = doc_t_max.id
		where a.deleted = 0
		" . $filterWilayahTujuan . "
		".$filterWilayah."
		order by 
		w.nama_wilayah, a.id, la.nama_lokasi, lp.tempat";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$temp = [];
			foreach ($data->result_array() as $key => $value) {
				$foreign = $value['tempat'];
				if (!in_array($foreign, $temp)) {
					$item = [];
					$item_sudah_inspeksi = [];
					foreach ($data->result_array() as $v_item) {
						if ($v_item['tempat'] == $foreign) {
							$v_item['has_pemeliharaan'] = 0;
							$v_item['no_document'] = $v_item['no_document'] == '' ? '' : $v_item['no_document'];
							$v_item['status'] = $v_item['status'] == '' ? '' : $v_item['status'];
							$v_item['bulan_terakhir_pemeliharaan'] = '';
							if ($v_item['tgl_pemeliharaan'] != '') {
								$v_item['bulan_terakhir_pemeliharaan'] = date('Y-m', strtotime($v_item['tgl_pemeliharaan']));
								$monthNow = date('Y-m');
								$v_item['periode_sekarang'] = $monthNow;
								if ($v_item['bulan_terakhir_pemeliharaan'] == $monthNow) {
									$v_item['has_pemeliharaan'] = 1;
								} else {
									$v_item['has_pemeliharaan'] = -1;
								}								
							}
							if($v_item['has_pemeliharaan'] == 1){
								$item_sudah_inspeksi[] = $v_item;
							}
							$item[] = $v_item;
						}
					}
					$value['item'] = $item;
					$value['jumlah_item'] = count($item);
					$value['jumlah_item_sudah_inspeksi'] = count($item_sudah_inspeksi);
					array_push($result, $value);
					$temp[] = $foreign;
				}
			}
		}

		return $result;
	}

	public function getDataDetailTempatLokasiApar($params = array(), $limit = true)
	{

		// $filterWilayah = '';
		$filterWilayah = " and lt.id_tujuan = '" . $params['id_lokasi'] . "'";
		$filterNoAlat = " and a.no_alat = '" . $params['qrcode'] . "'";

		$sql = "select 
		a.no_alat
		, a.id id_alat
		, la.nama_lokasi 
		, lp.tempat 
		, w.nama_wilayah 
		, lt.id_tujuan as id_lokasi
		, d.no_document 
		, cast(d.createddate as date) as tgl_pemeliharaan
		, dt.status
		, lt.id as lokasi_tujuan
		from lokasi_penempatan_alat lpl		
		join alat a
			on a.id = lpl.alat
		join lokasi_penempatan lp
			on lp.id = lpl.lokasi_penempatan 
		join lokasi_tujuan lt
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w
			on w.id = la.wilayah 
		left join (
			select max(id) id, lokasi_tujuan, alat from alat_transaksi_pemeliharaan group by alat, lokasi_tujuan  
		) atp_max
			on atp_max.alat = a.id
			and atp_max.lokasi_tujuan = lt.id
		left join alat_transaksi_pemeliharaan atp 
			on atp.id = atp_max.id
		left join document d 
			on d.id = atp.document 
			and d.deleted = 0
		left join (
			select max(id) id, document from document_transaction group by document
		) doc_t_max
			on doc_t_max.document = d.id
		left join document_transaction dt 
			on dt.id = doc_t_max.id
		where a.deleted = 0
		" . $filterWilayah . "
		" . $filterNoAlat . "
		order by 
		w.nama_wilayah, a.id, la.nama_lokasi, lp.tempat";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$temp = [];
			foreach ($data->result_array() as $key => $value) {
				$foreign = $value['tempat'];
				if (!in_array($foreign, $temp)) {
					$item = [];
					$item_sudah_inspeksi = [];
					foreach ($data->result_array() as $v_item) {
						if ($v_item['tempat'] == $foreign) {
							$v_item['has_pemeliharaan'] = 0;
							$v_item['no_document'] = $v_item['no_document'] == '' ? '' : $v_item['no_document'];
							$v_item['status'] = $v_item['status'] == '' ? '' : $v_item['status'];
							$v_item['bulan_terakhir_pemeliharaan'] = '';
							if ($v_item['tgl_pemeliharaan'] != '') {
								$v_item['bulan_terakhir_pemeliharaan'] = date('Y-m', strtotime($v_item['tgl_pemeliharaan']));
								$monthNow = date('Y-m');
								$v_item['periode_sekarang'] = $monthNow;
								if ($v_item['bulan_terakhir_pemeliharaan'] == $monthNow) {
									$v_item['has_pemeliharaan'] = 1;
								} else {
									$v_item['has_pemeliharaan'] = -1;
								}
								if($v_item['has_pemeliharaan'] == 1){
									$item_sudah_inspeksi[] = $v_item;
								}
							}
							$item[] = $v_item;
						}
					}
					$value['item'] = $item;
					$value['jumlah_item'] = count($item);
					$value['jumlah_item_sudah_inspeksi'] = count($item_sudah_inspeksi);
					array_push($result, $value);
					$temp[] = $foreign;
				}
			}
		}

		return $result;
	}

	public function getUserDetailAkses($params){
		$sql = "select rap.*
		, u.id as user_id
		, u.username as email
		, pg.nama as name
		, ut.id as upt_id
		, w.nama_wilayah
		, pg.id as pegawai_id
		, up.sub_upt 
		from role_apps_akses rap 
		join `user` u 
			on u.id = rap.`user`
		join apps a 
			on a.id = rap.apps
		join pegawai pg
			on pg.id = u.pegawai			
		join upt ut
			on ut.id = pg.upt
		join role_apps_apar raa
			on raa.role_apps_akses = rap.id
		join wilayah w
			on w.id = raa.wilayah
		left join ultg_pegawai up 
			on up.pegawai = pg.id
		where a.aplikasi = 'peralatan'
		and rap.deleted = 0
		and u.id = '" . $params['user_id'] . "'";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
			$idGiUnderUltg = [];
			$implodeIdGiUnderUltg = '';
			if($result['sub_upt'] != ''){
				$dataGi = $this->getListGiUnderUltg($result);
				if(!empty($dataGi)){
					foreach ($dataGi as $key => $value) {
						$idGiUnderUltg[] = $value['gardu_induk'];
					}				
					$implodeIdGiUnderUltg = implode(',', $idGiUnderUltg);
				}
			}
			$result['list_gi'] = $implodeIdGiUnderUltg;
		}
		return $result;
	}
	
	public function getListGiUnderUltg($params){
		$sql = "select * from sub_upt_has_gardu suhg where sub_upt = ".$params['sub_upt'];

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}
		return $result;
	}
}
