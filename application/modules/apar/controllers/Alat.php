<?php
// ini_set("memory_limit",-1);
class Alat extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_alat', 'alat');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'apar/alat';
	}

	public function index()
	{
		echo 'alat ' . date('Y-m-d H:i:s');
	}

	public function getData(){
		$data = $_POST;

		// $data['last_id'] = '';
		// $data['user_id'] = '957';
		// $data['upt_id'] = '4';
		// $data['wilayah'] = 'UPT';

		$data = $this->alat->getData($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataFilter(){
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
// 		[last_id] => 14
// I/flutter (14730):     [user_id] => 957
// I/flutter (14730):     [upt_id] => 4
// I/flutter (14730):     [tanggal_awal] =>
// I/flutter (14730):     [tanggal_akhir] =>
// I/flutter (14730):     [wilayah] => UPT

		$data = $this->alat->getDataFilter($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataFilterRabApar(){
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		// $data = [];
		// $data['user_id'] = 957;
		// $data['upt_id'] = 4;
		// $data['tanggal_awal'] = '';
		// $data['tanggal_akhir'] = '';
		// $data['wilayah'] = 'UPT';
		// $data['last_id'] = '';
		
		$data = $this->alat->getDataFilterRabApar($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataAllAparNotifikasiBelumInspeksi(){
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		$data = $this->alat->getDataNotifikasiApar($data);
		$result['data'] = $data;
		echo json_encode($result);
	}

	public function getDataAlatByWilayah(){
		$data = isset($_POST) ? $_POST : [];
		// echo '<pre>';
		// print_r($data);die;
		// $data['last_id'] = '';
		// $data['user_id'] = '1217';
		// $data['upt_id'] = '4';
		// $data['wilayah'] = 'UPT';
		// $data['id_tujuan'] = '0';

		$data['user_detail_akses'] = $this->alat->getUserDetailAkses($data);
		// echo '<pre>';
		// print_r($data);die;
		$data = $this->alat->getDataAlatByWilayah($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataTempatLokasiApar(){
		$data = isset($_POST) ? $_POST : [];
		// echo '<pre>';
		// print_r($data);die;
		// $data['user_id'] = 1194;
		// $data['upt_id'] = 4;
		// $data['wilayah'] = 'GARDU INDUK';
		// $data['id_lokasi'] = 90;
		$data = $this->alat->getDataTempatLokasiApar($data);		
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataDetailTempatLokasiApar(){
		$data = isset($_POST) ? $_POST : [];
			// echo '<pre>';
			// print_r($data);die;
		// $data['id_lokasi'] = 90;
		// $data['qrcode'] = 1;
		$data = $this->alat->getDataDetailTempatLokasiApar($data);		
		$result['data'] = $data;
		echo json_encode($result);
	}


	public function getDataApprovalData(){
		$data = $_POST;

		// $data['last_id'] = '';
		// $data['user_id'] = '957';
		// $data['upt_id'] = '4';
		// $data['wilayah'] = 'UPT';

		$data['user_detail_akses'] = $this->alat->getUserDetailAkses($data);
		$data = $this->alat->getDataApprovalData($data);
		foreach ($data as $key => $value) {
			$params['no_document'] = $value['no_document'];
			Modules::run('apar/sidak/createFilePdf', $params);
		}
		$result['data'] = $data;
		echo json_encode($result);
	}
}
