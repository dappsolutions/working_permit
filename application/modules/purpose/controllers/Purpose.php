<?php

class Purpose extends MX_Controller
{

  public $user;
  public $level;

  public function __construct()
  {
    parent::__construct();
    $this->user = $this->session->userdata('user_id');
    $this->level = $this->session->userdata('level');
  }

  public function getDataAdminUnit($upt)
  {
    $data = Modules::run('database/get', array(
      'table' => 'struktur_approval sa',
      'field' => array('sa.*', 'pg.no_hp', 'pg.email', 'u.player_id', 'pg.nip', 'pg.id as pegawai_id'),
      'join' => array(
        array('user u', 'sa.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
      ),
      'where' => "sa.deleted = 0 and sa.upt = '" . $upt . "' and sa.level = 1"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }

    //   echo '<pre>';
    //   echo $this->db->last_query();die;

    // echo '<pre>';
    // print_r($result);die;
    return $result;
  }

  public function getDataAdminUnitByName()
  {
    $data = Modules::run('database/get', array(
      'table' => 'struktur_approval sa',
      'field' => array('sa.*', 'pg.no_hp', 'u.player_id'),
      'join' => array(
        array('user u', 'sa.user = u.id'),
        array('upt ut', 'sa.upt = ut.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
      ),
      'where' => "sa.deleted = 0 and ut.id = '2' and sa.level = 1"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }


    return $result;
  }

  public function getDataLastApproval($upt)
  {
    $data = Modules::run('database/get', array(
      'table' => 'struktur_approval sa',
      'field' => array('sa.*', 'pg.no_hp', 'u.player_id'),
      'join' => array(
        array('user u', 'sa.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
      ),
      'where' => "sa.deleted = 0 and sa.upt = '" . $upt . "' and sa.is_last = 1"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }


    //   echo '<pre>';
    //   print_r($result);die;
    return $result;
  }

  public function sendSmsToAdminUnit($value = array(), $transaksi_id = '0')
  {
    $data_admin = $this->getDataAdminUnit($value['upt']);
    if (!empty($data_admin)) {
      $sms_message = Modules::run('sms/content_sms_from_pengajuan');
      // //  $is_send_sms = Modules::run('sms/send_sms', $sms_message, $data_admin['no_hp']);
      // $is_send_sms = Modules::run('sms/send_sms_data', $sms_message, $data_admin['no_hp']);
      // Modules::run('sms/simpanLogSms', $is_send_sms['is_valid'], $is_send_sms['message'] . '\n' . $sms_message, $data_admin['no_hp']);

      //send email
      $message = Modules::run('email/content_message_pengajuan');
      // $is_sent = Modules::run('email/send_email_data', 'Working Permit', $message, $data_admin['email']);
      // Modules::run('email/simpanLogEmail', $is_sent, $message, $data_admin['email']);

      /*SEND WA */
      $params = $data_admin;
      $params['message'] = $sms_message;
      $params['transaksi_id'] = $transaksi_id;
      $params['kategori'] = 'WP';
      $this->sendNotifWa($params);
      /*SEND WA */

      //send notif mobile
      Modules::run('fcm/send', $sms_message, $data_admin['player_id']);
    }
  }

  public function sendNotifWa($params = [])
  {
    if (!empty($params)) {
      // $userData = [];
      // $userData['pegawai'] = $params['pegawai_id'];
      // $userData['username'] = $params['nip'];
      // $userData['password'] = '1234';
      // $userData['createddate'] = date('Y-m-d');
      // $userData['deleted'] = 0;
      // $this->db->insert('user', $userData);

      $push = [];
      $push['transaksi_id'] = $params['transaksi_id'];
      $push['nik'] =  $params['nip'];
      $push['status_transaksi'] = 'CREATED';
      $push['remarks'] = $params['message'];
      $push['kategori'] = $params['kategori'];
      $push['createddate'] = date('Y-m-d H:i:s');
      $this->db->insert('notifikasi', $push);
    }
  }

  public function sendSmsToAdminUnitVendorRegister()
  {
    $data_admin = $this->getDataAdminUnitByName();
    if (!empty($data_admin)) {
      $sms_message = Modules::run('sms/content_message_register_user');
      $is_send_sms = Modules::run('sms/send_sms_data', $sms_message, $data_admin['no_hp']);
      Modules::run('sms/simpanLogSms', $is_send_sms['is_valid'], $is_send_sms['message'] . '\n' . $sms_message, $data_admin['no_hp']);

      //send mobile apps
      Modules::run('fcm/send', $sms_message, $data_admin['player_id']);
    }
  }

  public function getNextLevelApproval($upt, $jenis_wp = 'internal')
  {
    $tipe_permit = $jenis_wp == 'internal' ? '1' : '2';
    $level = intval($this->level) + 1;
    $data = Modules::run('database/get', array(
      'table' => 'struktur_approval sa',
      'field' => array('sa.*', 'pg.no_hp', 'pg.email', 'u.player_id', 'pg.id as pegawai_id', 'pg.nip'),
      'join' => array(
        array('user u', 'sa.user = u.id'),
        array('pegawai pg', 'u.pegawai = pg.id'),
      ),
      'where' => "sa.deleted = 0 and sa.upt = '" . $upt . "' "
        . "and sa.level = '" . $level . "' and sa.tipe_permit = '" . $tipe_permit . "'",
      'orderby' => 'sa.createddate desc, sa.id asc'
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
    }


    //   echo '<pre>';
    //   print_r($result);die;
    return $result;
  }

  public function getDetailUnit($unit)
  {
    $data = Modules::run('database/get', array(
      'table' => 'upt u',
      'field' => array('u.*'),
      'where' => "u.id = '" . $unit . "'"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
      $result['upt_id'] = $result['id'];
    }


    return $result;
  }

  public function getDetailSubUnit($sub_unit)
  {
    $data = Modules::run('database/get', array(
      'table' => 'sub_upt su',
      'field' => array('su.*', 'u.nama as nama_upt'),
      'join' => array(
        array('upt u', 'su.upt = u.id')
      ),
      'where' => "su.id = '" . $sub_unit . "'",
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
      $result['upt_id'] = $result['upt'];
      $result['nama'] = $result['nama_upt'] . ' - ' . $result['nama'];;
    }
    //  echo '<pre>';
    //  print_r($result);die;

    return $result;
  }

  public function getDetailGardu($gi)
  {
    $data = Modules::run('database/get', array(
      'table' => 'gardu_induk gi',
      'field' => array('gi.*', 'ut.nama as nama_upt'),
      'join' => array(
        array('upt ut', 'gi.upt = ut.id')
      ),
      'where' => "gi.id = '" . $gi . "'"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
      $result['nama'] = $result['nama_gardu'];
      $result['upt_id'] = $result['upt'];
    }


    return $result;
  }

  public function getDetailSutet($sutet)
  {
    $data = Modules::run('database/get', array(
      'table' => 'sutet s',
      'field' => array('s.*', 'ut.nama as nama_upt', 'ut.id as upt'),
      'join' => array(
        array('gardu_induk gi', 'gi.id = s.gardu_induk'),
        array('upt ut', 'gi.upt = ut.id'),
      ),
      'where' => "s.id = '" . $sutet . "'"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();
      $result['upt_id'] = $result['upt'];
    }


    return $result;
  }

  public function getDetailDataWp($permit)
  {
    $data = Modules::run('database/get', array(
      'table' => 'permit p',
      'field' => array(
        'p.*',
        'pw.lokasi_pekerjaan',
        'pw.uraian_pekerjaan',
        'pw.work_place',
        'pw.place',
        'pv.no_pengajuan as no_pengajuan_vendor',
        'ph.no_hp as no_hp_pemohon',
        'ph.email as email_pemohon',
        'pw.tgl_awal',
        'pw.tgl_akhir',
        'v.nama_vendor',
        'u.player_id',
        'u.pegawai as pegawai_id',
        'u.username'
      ),
      'join' => array(
        array('permit_work pw', 'pw.permit = p.id'),
        array('pemohon ph', 'p.pemohon = ph.id'),
        array('user u', 'u.id = p.user'),
        array('vendor v', 'u.vendor = v.id', 'left'),
        array('pengajuan_vendor pv', 'v.pengajuan_vendor = pv.id', 'left'),
      ),
      'where' => "p.deleted = 0 and p.id = '" . $permit . "'"
    ));

    $result = array();
    if (!empty($data)) {
      $result = $data->row_array();

      $data_place = array();
      switch ($result['work_place']) {
        case 1:
          $data_place = $this->getDetailUnit($result['place']);
          break;
        case 2:
          $data_place = $this->getDetailSubUnit($result['place']);
          break;
        case 3:
          $data_place = $this->getDetailGardu($result['place']);
          break;
        case 4:
          $data_place = $this->getDetailSutet($result['place']);
          break;

        default:
          break;
      }
      $result['nama_place'] = $data_place['nama'];
      $result['upt_id'] = $data_place['upt_id'];
    }


    //   echo '<pre>';
    //   print_r($result);die;
    return $result;
  }

  public function sendNotifWpToApproval($value = array(), $jenis_wp = 'internal')
  {
    $data_notif = [];
    $last_approval = $this->getDataLastApproval($value['upt']);
    //last approval harus sudah di set
    if (!empty($last_approval)) {
      if ($last_approval['user'] == $this->user) {
        $data_wp = $this->getDetailDataWp($value['permit']);

        //send sms pemohon  
        $sms_message_pemohon = '-';
        $message_email_pemohon = '-';
        if ($value['action'] == 'APPROVED') {
          $sms_message_pemohon = Modules::run('sms/content_message_sms_pemohon', $data_wp);
          $message_email_pemohon = Modules::run('email/content_message_email_pemohon', $data_wp);
        } else {
          $sms_message_pemohon = Modules::run('sms/content_message_sms_pemohon_rejected', $data_wp);
          $message_email_pemohon = Modules::run('email/content_message_email_pemohon_rejected', $data_wp);
        }
        // echo $sms_message_pemohon;die;
        // $is_send_sms = Modules::run('sms/send_sms', $sms_message_pemohon, $data_wp['no_hp_pemohon']);
        // $is_send_sms = Modules::run('sms/send_sms_data', $sms_message_pemohon, $data_wp['no_hp_pemohon']);
        // Modules::run('sms/simpanLogSms', $is_send_sms['is_valid'], $is_send_sms['message'] . '\n' . $sms_message_pemohon, $data_wp['no_hp_pemohon']);
        //send email pemohon    
        // $is_sent = Modules::run('email/send_email_notif_pemohon_data', 'Working Permit', $message_email_pemohon, $data_wp);
        // Modules::run('email/simpanLogEmail', $is_sent, $message_email_pemohon, $data_wp['email_pemohon']);

        //send notif mobiles apps
        /*SEND WA */
        $params = $data_wp;
        $params['message'] = $sms_message_pemohon;
        $params['transaksi_id'] = $value['permit'];
        $params['kategori'] = 'WP';
        $params['nip'] = $data_wp['username'];
        $this->sendNotifWa($params);
        /*SEND WA */
        Modules::run('fcm/send', $sms_message_pemohon, $data_wp['player_id']);

        $data_notif[] = [
          'no_hp' => $data_wp['no_hp_pemohon'],
          'nip' => $data_wp['username'],
          'message' => $sms_message_pemohon
        ];
      } else {
        $data_next_apprval = $this->getNextLevelApproval($value['upt'], $jenis_wp);
        if (!empty($data_next_apprval)) {
          //send sms
          $sms_message_next = Modules::run('sms/content_sms_from_pengajuan');
          $cc = array();
          if ($jenis_wp == 'eksternal') {
            if ($this->level == '1') {
              if ($value['upt'] == '4') {
                $data_email = $this->getListStrukturApprovalLevel2NonManAset($value['upt'], $jenis_wp);
                if (!empty($data_email)) {
                  foreach ($data_email as $v_email) {
                    if ($v_email['struktur_approval'] != $data_next_apprval['id']) {
                      array_push($cc, strtolower(trim($v_email['email'])));
                    }
                  }
                }
              }
            }
          }

          //send email umum
          $message = Modules::run('email/content_message_pengajuan');

          //UNTUK MENGIRIM DATA NOTIFIKASI SPV GI JIKA TEMPAT KERJA ADA DI SUTET/GARDU;					
          $data_spv_gi = array();
          if ($this->level == '1') {
            if ($value['work_place'] == '3' || $value['work_place'] == '4') {
              $data_spv_gi = $this->getDataSpvGIEmail($value);
              if (!empty($data_spv_gi)) {
                foreach ($data_spv_gi as $v_spv) {
                  array_push($cc, strtolower(trim($v_spv['email'])));
                }

                //set message spv gi
                if (!empty($cc)) {
                  $data_wp = $this->getDetailDataWp($value['permit']);
                  $data_wp['nama_gardu'] = $data_spv_gi[0]['nama_gardu'];
                  $message = Modules::run('email/content_message_pengajuan_to_spv_gi', $data_wp);

                  /*WA */
                  foreach ($data_spv_gi as $vsp) {
                    $data_notif[] = [
                      'no_hp' => $vsp['no_hp'],
                      'nip' => $vsp['nip'],
                      'message' => $message
                    ];
                  }
                  /*WA */
                }
              }
            }
          }

          //  $send_sms_next = Modules::run('sms/send_sms', $sms_message_next, $data_next_apprval['no_hp']);
          // $send_sms_next = Modules::run('sms/send_sms_data', $sms_message_next, $data_next_apprval['no_hp']);
          // $send_sms_next['message'] = isset($send_sms_next['message']) ? $send_sms_next['message'] : 'No SMS';
          // Modules::run('sms/simpanLogSms', $send_sms_next['is_valid'], $send_sms_next['message'] . '\n' . $sms_message_next, $data_next_apprval['no_hp']);

          // $is_sent = Modules::run('email/send_email_data', 'Working Permit', $message, $data_next_apprval['email'], $cc);
          // Modules::run('email/simpanLogEmail', $is_sent, $message, $data_next_apprval['email']);

          /*SEND WA */
          $params = $data_next_apprval;
          $params['message'] = $sms_message_next;
          $params['transaksi_id'] = $value['permit'];
          $params['kategori'] = 'WP';
          $this->sendNotifWa($params);
          /*SEND WA */
          //send mobiles apps
          Modules::run('fcm/send', $sms_message_next, $data_next_apprval['player_id']);

          /*WA */
          $data_notif[] = [
            'no_hp' => $data_next_apprval['no_hp'],
            'nip' => $data_wp['username'],
            'message' => $sms_message_next
          ];
          /*WA */
        }
      }
    }

    return $data_notif;
  }

  public function getQuerySQlSpvGiByGardu($data)
  {
    $sql = "";
    if ($data['work_place'] == '3') {
      $sql = "select 
      pg.* 
      , gi.nama_gardu
			from gardu_induk_has_pegawai gihp
		join pegawai pg
      on pg.id = gihp.pegawai 
    join gardu_induk gi
			on gi.id = gihp.gardu_induk
		where gihp.gardu_induk = '" . $data['place'] . "'
    order by gihp.id desc";
    }

    return $sql;
  }

  public function getQuerySQlSpvGiBySutet($data)
  {
    $sql = "select 
      pg.*
      , gi.nama_gardu
			from sutet s
			join gardu_induk gi
				on gi.id = s.gardu_induk 
			join upt ut
				on ut.id = gi.upt
			join gardu_induk_has_pegawai giph
				on giph.gardu_induk = gi.id
			join pegawai pg
				on pg.id = giph.pegawai 
			where s.id = '" . $data['place'] . "'
      order by giph.id desc";

    return $sql;
  }

  public function getDataSpvGIEmail($data)
  {
    $sql = $this->getQuerySQlSpvGiByGardu($data);
    if ($data['work_place'] == '4') {
      $sql = $this->getQuerySQlSpvGiBySutet($data);
    }

    $data = Modules::run('database/get_custom', $sql);

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        array_push($result, $value);
      }
    }

    return $result;
  }

  public function getListStrukturApprovalLevel2NonManAset($upt, $jenis_wp)
  {
    $tipe_permit = $jenis_wp == 'internal' ? '1' : '2';
    $query = "select shs.*, pg.email, pg.no_hp 
		from struktur_approval_has_sub_upt shs 
		join struktur_approval sa
			on sa.id = shs.struktur_approval 
		join `user` us
			on us.id = sa.`user` 
		join pegawai pg
			on pg.id = us.pegawai 
		where shs.struktur_approval in (
			select id from struktur_approval where upt = " . $upt . " 
			and `level` = 2 and tipe_permit = " . $tipe_permit . "
		)";

    $data = Modules::run('database/get_custom', $query);
    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        array_push($result, $value);
      }
    }

    return $result;
  }

  public function sendNotifToVendorRegister($data_vendor)
  {
    $sms_message = Modules::run('sms/content_message_perbaharui_user', $data_vendor);
    // $is_send_sms = Modules::run('sms/send_sms', $sms_message, $data_vendor['no_hp']);
    $is_send_sms = Modules::run('sms/send_sms_data', $sms_message, $data_vendor['no_hp']);
    Modules::run('sms/simpanLogSms', $is_send_sms['is_valid'], $is_send_sms['message'] . '\n' . $sms_message, $data_vendor['no_hp']);

    $result['sms'] = $is_send_sms;
    //send email to id registration
    $message = Modules::run('email/content_message_perbaharui_user', $data_vendor);
    $is_sent = Modules::run(
      'email/send_email_data',
      'Working Permit',
      $message,
      $data_vendor['email']
    );
    Modules::run('email/simpanLogEmail', $is_sent, $message, $data_vendor['email']);
    $result['email'] = $is_sent;
    return $result;
  }

  public function sendNotifToVendorRegisterSimson($data_vendor)
  {
    $sms_message = Modules::run('sms/content_message_perbaharui_user', $data_vendor);
    $is_send_sms = Modules::run('sms/send_sms_data', $sms_message, $data_vendor['no_hp']);
    Modules::run('sms/simpanLogSms', $is_send_sms['is_valid'], $is_send_sms['message'] . '\n' . $sms_message, $data_vendor['no_hp']);

    $result['sms'] = $is_send_sms;
    //send email to id registration
    $message = Modules::run('email/content_message_perbaharui_user_simson', $data_vendor);
    $is_sent = Modules::run(
      'email/send_email_data',
      'Working Permit',
      $message,
      $data_vendor['email']
    );
    Modules::run('email/simpanLogEmail', $is_sent, $message, $data_vendor['email']);

    $result['email'] = $is_sent;
    return $result;
  }
}
