<?php

class Purpose extends MX_Controller {

 public $user;
 public $level;

 public function __construct() {
  parent::__construct();
  $this->user = $this->session->userdata('user_id');
  $this->level = $this->session->userdata('level');
 }

 public function getDataAdminUnit($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'struktur_approval sa',
              'field' => array('sa.*', 'pg.no_hp'),
              'join' => array(
                  array('user u', 'sa.user = u.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "sa.deleted = 0 and sa.upt = '" . $upt . "' and sa.level = 1"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataAdminUnitByName($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'struktur_approval sa',
              'field' => array('sa.*', 'pg.no_hp'),
              'join' => array(
                  array('user u', 'sa.user = u.id'),
                  array('upt ut', 'sa.upt = ut.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "sa.deleted = 0 and ut.nama = '" . trim($upt) . "' and sa.level = 1"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataLastApproval($upt) {
  $data = Modules::run('database/get', array(
              'table' => 'struktur_approval sa',
              'field' => array('sa.*', 'pg.no_hp'),
              'join' => array(
                  array('user u', 'sa.user = u.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "sa.deleted = 0 and sa.upt = '" . $upt . "' and sa.is_last = 1"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function sendSmsToAdminUnit($value = array()) {
  $data_admin = $this->getDataAdminUnit($value['upt']);
  if (!empty($data_admin)) {
   $sms_message = Modules::run('sms/content_sms_from_pengajuan');
   $is_send_sms = Modules::run('sms/send_sms', $sms_message, $data_admin['no_hp']);
  }
 }

 public function sendSmsToAdminUnitVendorRegister() {
  $data_admin = $this->getDataAdminUnitByName('Kantor Induk');
  if (!empty($data_admin)) {
   $sms_message = Modules::run('sms/content_message_register_user');
   $is_send_sms = Modules::run('sms/send_sms', $sms_message, $data_admin['no_hp']);
  }
 }

 public function getNextLevelApproval($upt) {
  $level = intval($this->level) + 1;
  $data = Modules::run('database/get', array(
              'table' => 'struktur_approval sa',
              'field' => array('sa.*', 'pg.no_hp', 'pg.email'),
              'join' => array(
                  array('user u', 'sa.user = u.id'),
                  array('pegawai pg', 'u.pegawai = pg.id'),
              ),
              'where' => "sa.deleted = 0 and sa.upt = '" . $upt . "' "
              . "and sa.level = '" . $level . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDetailUnit($unit) {
  $data = Modules::run('database/get', array(
              'table' => 'upt u',
              'field' => array('u.*'),
              'where' => "u.id = '" . $unit . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   $result['upt_id'] = $result['id'];
  }


  return $result;
 }

 public function getDetailSubUnit($sub_unit) {
  $data = Modules::run('database/get', array(
              'table' => 'sub_upt su',
              'field' => array('su.*', 'u.nama as nama_upt'),
              'join' => array(
                  array('upt u', 'su.upt = u.id')
              ),
              'where' => "su.id = '" . $sub_unit . "'",
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   $result['upt_id'] = $result['upt'];
   $result['nama'] = $result['nama_upt'] . ' - ' . $result['nama'];
   ;
  }
//  echo '<pre>';
//  print_r($result);die;

  return $result;
 }

 public function getDetailGardu($gi) {
  $data = Modules::run('database/get', array(
              'table' => 'gardu_induk gi',
              'field' => array('gi.*', 'ut.nama as nama_upt'),
              'join' => array(
                  array('upt ut', 'gi.upt = ut.id')
              ),
              'where' => "gi.id = '" . $gi . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   $result['nama'] = $result['nama_gardu'];
   $result['upt_id'] = $result['upt'];
   ;
  }


  return $result;
 }

 public function getDetailSutet($sutet) {
  $data = Modules::run('database/get', array(
              'table' => 'sutet s',
              'field' => array('s.*', 'ut.nama as nama_upt', 'ut.id as upt'),
              'join' => array(
                  array('gardu_induk gi', 'gi.id = s.gardu_induk'),
                  array('upt ut', 'gi.upt = ut.id'),
              ),
              'where' => "s.id = '" . $sutet . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
   $result['upt_id'] = $result['upt'];
  }


  return $result;
 }

 public function getDetailDataWp($permit) {
  $data = Modules::run('database/get', array(
              'table' => 'permit p',
              'field' => array('p.*',
                  'pw.lokasi_pekerjaan',
                  'pw.uraian_pekerjaan',
                  'pw.work_place',
                  'pw.place',
                  'pv.no_pengajuan as no_pengajuan_vendor',
                  'ph.no_hp as no_hp_pemohon',
                  'ph.email as email_pemohon'),
              'join' => array(
                  array('permit_work pw', 'pw.permit = p.id'),
                  array('pemohon ph', 'p.pemohon = ph.id'),
                  array('vendor v', 'p.createdby = v.id', 'left'),
                  array('pengajuan_vendor pv', 'v.pengajuan_vendor = pv.id', 'left'),
              ),
              'where' => "p.deleted = 0 and p.id = '" . $permit . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();

   $data_place = array();
   switch ($result['work_place']) {
    case 1:
     $data_place = $this->getDetailUnit($result['place']);
     break;
    case 2:
     $data_place = $this->getDetailSubUnit($result['place']);
     break;
    case 3:
     $data_place = $this->getDetailGardu($result['place']);
     break;
    case 4:
     $data_place = $this->getDetailSutet($result['place']);
     break;

    default:
     break;
   }
   $result['nama_place'] = $data_place['nama'];
   $result['upt_id'] = $data_place['upt_id'];
  }


  return $result;
 }

 public function sendNotifWpToApproval($value = array()) {
  $last_approval = $this->getDataLastApproval($value['upt']);
  //last approval harus sudah di set
  if (!empty($last_approval)) {
   if ($last_approval['user'] == $this->user) {
    $data_wp = $this->getDetailDataWp($value['permit']);

	//send sms pemohon  
	$sms_message_pemohon = '-';	
	$message_email_pemohon = '-';	
	if($value['action'] == 'APPROVED'){
		$sms_message_pemohon = Modules::run('sms/content_message_sms_pemohon', $data_wp);
		$message_email_pemohon = Modules::run('email/content_message_email_pemohon', $data_wp);
	}else{
		$sms_message_pemohon = Modules::run('sms/content_message_sms_pemohon_rejected', $data_wp);
		$message_email_pemohon = Modules::run('email/content_message_email_pemohon_rejected', $data_wp);
	}      
    $is_send_sms = Modules::run('sms/send_sms', $sms_message_pemohon, $data_wp['no_hp_pemohon']);
    //send email pemohon    
    $is_sent = Modules::run('email/send_email_notif_pemohon', 'Working Permit', $message_email_pemohon, $data_wp);
   } else {
    $data_next_apprval = $this->getNextLevelApproval($value['upt']);
    if (!empty($data_next_apprval)) {
     //send sms
     $sms_message_next = Modules::run('sms/content_sms_from_pengajuan');
     $send_sms_next = Modules::run('sms/send_sms', $sms_message_next, $data_next_apprval['no_hp']);

     //send email
     $message = Modules::run('email/content_message_pengajuan');
     $is_sent = Modules::run('email/send_email', 'Working Permit', $message, $data_next_apprval['email']);
    }
   }
  }
 }

 public function sendNotifToVendorRegister($data_vendor) {
  $sms_message = Modules::run('sms/content_message_perbaharui_user', $data_vendor);
  $is_send_sms = Modules::run('sms/send_sms', $sms_message, $data_vendor['no_hp']);

  //send email to id registration
  $message = Modules::run('email/content_message_perbaharui_user', $data_vendor);
  $is_sent = Modules::run('email/send_email', 'Working Permit',
                  $message, $data_vendor['email']);
 }

}
