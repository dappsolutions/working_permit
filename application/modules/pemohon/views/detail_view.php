<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nama Pemohon</label>

      <div class="col-sm-4">
       <input type="text" disabled="" class="form-control required" error="Nama Pemohon" id="nama" placeholder="Nama Pemohon"value="<?php echo $nama_pemohon ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nama Perusahaan</label>

      <div class="col-sm-4">
       <input type="text" disabled="" class="form-control required" 
              error="Nama Perusahaan" id="perusahaan" 
              placeholder="Nama Perusahaan"value="<?php echo $perusahaan ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Jabatan</label>

      <div class="col-sm-4">
       <input type="text" disabled="" class="form-control required" 
              error="Jabatan" id="jabatan" 
              placeholder="Jabatan"value="<?php echo $jabatan ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Email</label>

      <div class="col-sm-4">
       <input type="text" disabled="" class="form-control required" 
              error="Email" id="email" 
              placeholder="Email"value="<?php echo $email ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Telp</label>

      <div class="col-sm-4">
       <input type="text" disabled="" class="form-control required" 
              error="No Telp" id="no_telp" 
              placeholder="No Telp"value="<?php echo $no_telp ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Hp</label>

      <div class="col-sm-4">
       <input type="text" disabled="" class="form-control required" 
              error="No Hp" id="no_hp" 
              placeholder="No Hp"value="<?php echo $no_hp ?>">
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Pemohon.back()">Cancel</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
