<?php

class Template extends MX_Controller {

 public function index($data) {
//  echo '<pre>';
//  print_r($_SESSION);die;
  $data['akses'] = $this->session->userdata('hak_akses');
  $data['notif_wp'] = $this->getNotification()['notif_wp'];
  $data['total_notif'] = count($data['notif_wp']);
  echo $this->load->view('template_view', $data, true);
 }

 public function getNotification() {
  $data['notif_wp'] = Modules::run('dashboard/getDataWpDraft');
  return $data;
 }

}
