<aside class="main-sidebar">
 <!-- sidebar: style can be found in sidebar.less -->
 <section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
   <div class="pull-left image">
    <img src="<?php echo base_url() ?>assets/images/users/images.png" class="img-circle" alt="User Image">
   </div>
   <div class="pull-left info" id='user_session'>
    <p><?php echo strtolower($this->session->userdata('username')) ?></p>
    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
   </div>
  </div>


  <?php echo $this->load->view('akses_superadmin') ?>
  <?php echo $this->load->view('akses_internal') ?>
  <?php echo $this->load->view('akses_pegawai') ?>
  <?php echo $this->load->view('akses_approval') ?>
  <?php echo $this->load->view('akses_vendor') ?>
 </section>
 <!-- /.sidebar -->
</aside>