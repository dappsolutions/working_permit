<?php if ($this->session->userdata('hak_akses') == 'approval' && $this->session->userdata('level') == '1') { ?>
	<!-- /.search form -->
	<!-- sidebar menu: : style can be found in sidebar.less -->
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Permohonan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'wpinternal' ?>"><i class="fa fa-file-text-o"></i> WP Masuk Internal</a></li>
				<li><a href="<?php echo base_url() . 'wpeksternal' ?>"><i class="fa fa-file-text-o"></i> WP Masuk Eksternal</a></li>
				<?php if ($this->session->userdata('upt') == '2') { ?>
					<li><a href="<?php echo base_url() . 'registrasi' ?>"><i class="fa fa-file-text-o"></i> Validasi User Vendor</a></li>
					<li><a href="<?php echo base_url() . 'registrasiswa' ?>"><i class="fa fa-file-text-o"></i> Validasi User SWA</a></li>
					<li><a href="<?php echo base_url() . 'registrasisimson' ?>"><i class="fa fa-file-text-o"></i> Validasi User Simson</a></li>
				<?php } ?>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Histori Permohonan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'historyint' ?>"><i class="fa fa-file-text-o"></i> Internal</a></li>
				<li><a href="<?php echo base_url() . 'historyeks' ?>"><i class="fa fa-file-text-o"></i> Eksternal</a></li>
			</ul>
		</li>

		<?php if ($this->session->userdata('upt') == '2') { ?>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Data</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'vendor' ?>"><i class="fa fa-file-text-o"></i> Vendor</a></li>
					<li><a href="<?php echo base_url() . 'pemohon' ?>"><i class="fa fa-file-text-o"></i> Pemohon</a></li>
					<li><a href="<?php echo base_url() . 'alat_kerja' ?>"><i class="fa fa-file-text-o"></i> Alat Kerja</a></li>
					<li><a href="<?php echo base_url() . 'grafik' ?>"><i class="fa fa-file-text-o"></i> Grafik WP</a></li>
					<li><a href="<?php echo base_url() . 'spvgi' ?>"><i class="fa fa-file-text-o"></i> SPV GI</a></li>
					<!-- <li><a href="<?php echo base_url() . 'grafikswa' ?>"><i class="fa fa-file-text-o"></i> Grafik SWA</a></li> -->
				</ul>
			</li>
		<?php } else { ?>
			<?php if ($this->session->userdata('level') == '1') { ?>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-folder"></i>
						<span>Data</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="<?php echo base_url() . 'grafikswa' ?>"><i class="fa fa-file-text-o"></i> Grafik SWA</a></li>
						<li><a href="<?php echo base_url() . 'spvgi' ?>"><i class="fa fa-file-text-o"></i> SPV GI</a></li>
					</ul>
				</li>
			<?php } ?>
		<?php } ?>

		<?php if ($this->session->userdata('upt') == '2') { ?>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>IBPPR</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'akibat' ?>"><i class="fa fa-file-text-o"></i> Akibat</a></li>
					<li><a href="<?php echo base_url() . 'peluang' ?>"><i class="fa fa-file-text-o"></i> Peluang</a></li>
					<li><a href="<?php echo base_url() . 'pemaparan' ?>"><i class="fa fa-file-text-o"></i> Pemaparan</a></li>
				</ul>
			</li>
		<?php } ?>

		<?php if ($this->session->userdata('upt') == '2') { ?>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Struktur Approval</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'struktur' ?>"><i class="fa fa-file-text-o"></i> Struktur</a></li>
				</ul>
			</li>
		<?php } ?>

		<?php if ($this->session->userdata('upt') == '2') { ?>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Master</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'upt' ?>"><i class="fa fa-file-text-o"></i> UPT</a></li>
					<li><a href="<?php echo base_url() . 'basecamp' ?>"><i class="fa fa-file-text-o"></i> Ultg</a></li>
					<li><a href="<?php echo base_url() . 'gardu_induk' ?>"><i class="fa fa-file-text-o"></i> Gardu Induk</a></li>
					<li><a href="<?php echo base_url() . 'bay' ?>"><i class="fa fa-file-text-o"></i> Bay</a></li>
					<li><a href="<?php echo base_url() . 'sutet' ?>"><i class="fa fa-file-text-o"></i> Sutet</a></li>
					<li><a href="<?php echo base_url() . 'pegawai' ?>"><i class="fa fa-file-text-o"></i> Pegawai</a></li>
					<li><a href="<?php echo base_url() . 'user' ?>"><i class="fa fa-file-text-o"></i> User</a></li>
					<li><a href="<?php echo base_url() . 'catatan' ?>"><i class="fa fa-file-text-o"></i> Catatan Khusus</a></li>
					<li><a href="<?php echo base_url() . 'contact' ?>"><i class="fa fa-file-text-o"></i> Contact Person</a></li>
					<li><a href="<?php echo base_url() . 'tahapan' ?>"><i class="fa fa-file-text-o"></i> Tahapan Pekerjaan</a></li>
					<li><a href="<?php echo base_url() . 'potensi' ?>"><i class="fa fa-file-text-o"></i> Potensi Bahaya</a></li>
					<li><a href="<?php echo base_url() . 'pengendalian' ?>"><i class="fa fa-file-text-o"></i> Pengendalian</a></li>
					<li><a href="<?php echo base_url() . 'template_jsa' ?>"><i class="fa fa-file-text-o"></i> Template JSA</a></li>
				</ul>
			</li>
		<?php } ?>

		<?php if ($this->session->userdata('upt') == '2') { ?>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Diagram</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'single_line' ?>"><i class="fa fa-file-text-o"></i> Single Line</a></li>
					<li><a href="<?php echo base_url() . 'sld' ?>"><i class="fa fa-file-text-o"></i> Sld</a></li>
				</ul>
			</li>
		<?php } ?>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'lapinternal' ?>"><i class="fa fa-file-text-o"></i> WP Internal</a></li>
				<li><a href="<?php echo base_url() . 'lapeksternal' ?>"><i class="fa fa-file-text-o"></i> WP Eksternal</a></li>
				<li><a href="<?php echo base_url() . 'lapswa' ?>"><i class="fa fa-file-text-o"></i> WP SWA</a></li>
				<li><a href="<?php echo base_url() . 'lapsimson' ?>"><i class="fa fa-file-text-o"></i> WP Simson</a></li>
				<li><a href="<?php echo base_url() . 'lapkehadiran' ?>"><i class="fa fa-file-text-o"></i> Laporan Kehadiran</a></li>
			</ul>
		</li>

		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>
<?php } ?>