<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type="hidden" value='<?php echo isset($tgl_ibppr_baru) ? $tgl_ibppr_baru : '' ?>' id="tgl_ibppr_baru" class="form-control" />

<div class="row">
	<div class="col-md-12">
		<!-- Horizontal Form -->
		<div class="box box-info padding-16">
			<!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
       </div>
      </div>-->
			<!-- /.box-header -->
			<!-- form start -->
			<div class="box-body">


				<div class="row">
					<div class="col-md-12">
						<!-- Custom Tabs -->
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#form-pemohon" data-toggle="tab"><label class="label label-warning">1</label>&nbsp;Form Pemohon</a></li>
								<li><a href="#pic_jawab" data-toggle="tab"><label class="label label-warning">2</label> Form Pengawas</a></li>
								<li><a href="#pelaksana" data-toggle="tab"><label class="label label-warning">3</label> Form Pelaksana</a></li>
								<li><a href="#apd" data-toggle="tab"><label class="label label-warning">4</label> Form Alat Kerja</a></li>
								<li><a href="#ibppr" data-toggle="tab"><label class="label label-warning">5</label> Form Ibppr</a></li>
								<li><a href="#fik" data-toggle="tab"><label class="label label-warning">6</label> Form Job Safety</a></li>
								<li><a href="#riwayat_tolak" data-toggle="tab"><label class="label label-warning">-</label> Riwayat Penolakan</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="form-pemohon">
									<div class="row">
										<?php echo $this->load->view('detail_pemohon'); ?>
										<?php echo $this->load->view('detail_wp'); ?>
										<?php echo $this->load->view('detail_work_place'); ?>

										<div class="row">
											<?php if (!empty($status_approve)) { ?>
												<?php foreach ($status_approve as $value) { ?>
													<div class="col-md-3">
														<div class="row">
															<div class="col-md-12 text-center">
																<?php $ket_status = "Dibuat"; ?>
																<?php if ($value['status'] != 'DRAFT') { ?>
																	<?php $ket_status = $value['status'] == 'APPROVED' ? "Direview" : "Ditolak"; ?>
																<?php } ?>
																<?php echo $ket_status . ' oleh, ' ?>
															</div>
															<div class="col-md-12 text-center" style="margin-top: 100px;">
																<?php echo $value['nama_aktor'] ?>
															</div>
															<?php $text_color = 'text-warning'; ?>
															<?php if ($value['status'] == 'APPROVED') { ?>
																<?php $text_color = 'text-success'; ?>
															<?php } ?>
															<?php if ($value['status'] == 'REJECTED') { ?>
																<?php $text_color = 'text-danger'; ?>
															<?php } ?>
															<div class="col-md-12 text-center <?php echo $text_color ?>">
																<?php echo '(' . $value['createddate'] . ')' ?>
															</div>
														</div>
													</div>
												<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
								<!-- /.tab-pane -->
								<div class="tab-pane" id="pic_jawab">
									<div class="row">
										<?php echo $this->load->view('detail_pengawas'); ?>
									</div>
								</div>
								<div class="tab-pane" id="pelaksana">
									<div class="row">
										<?php echo $this->load->view('detail_pelaksana'); ?>
									</div>
								</div>
								<div class="tab-pane" id="apd">
									<div class="row">
										<?php echo $this->load->view('detail_apd'); ?>
									</div>
								</div>
								<div class="tab-pane" id="ibppr">
									<div class="row" <?php echo $tgl_ibppr_baru ?>>
										<?php if ($tgl_ibppr_baru == '1') { ?>
											<?php echo $this->load->view('ket_ibbpr_new'); ?>
											<?php echo $this->load->view('detail_ibppr_new'); ?>
										<?php } else { ?>
											<?php echo $this->load->view('ket_ibbpr'); ?>
											<?php echo $this->load->view('detail_ibppr'); ?>
										<?php } ?>
									</div>
								</div>
								<!-- /.tab-pane -->
								<div class="tab-pane" id="fik">
									<div class="row">
										<?php echo $this->load->view('detail_jsa'); ?>
									</div>
								</div>
								<!-- /.tab-pane -->
								<div class="tab-pane" id="riwayat_tolak">
									<div class="row">
										<?php echo $this->load->view('detail_penolakan'); ?>
									</div>
								</div>
							</div>
							<!-- /.tab-content -->
						</div>
						<!-- nav-tabs-custom -->
					</div>
				</div>
				<br />

			</div>
			<!-- /.box-body -->
			<div class="box-footer">
				<div class="text-right">
					<button type="button" class="btn btn-default" onclick="WpInternal.back()">Kembali</button>
					<?php if ($akses == 'approval') { ?>
						<?php if ($level < $_SESSION['level']) { ?>
							<button type="submit" class="btn btn-danger" onclick="WpInternal.reject('<?php echo isset($id) ? $id : '' ?>', event)">
								<i class="fa fa-close"></i>&nbsp;Reject
							</button>
							<button type="submit" class="btn btn-success" onclick="WpInternal.approve('<?php echo isset($id) ? $id : '' ?>', event)">
								<i class="fa fa-check"></i>&nbsp;Approve
							</button>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div>