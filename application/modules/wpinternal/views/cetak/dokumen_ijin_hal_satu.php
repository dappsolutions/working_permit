<html>
 <head>
  <title></title>    
  <style>
   #_wrapper{
    width: 100%;        
    margin: 0 auto;               
   }

   #_content{
    border: 1px solid #999;
    max-width: 100%;
    text-align: center;        
   }

   #_top-content{
    width: 95%;
    max-width: 95%;                
    margin: 1% auto;        
   }      

   #_judul{
    font-size: 100%;
    font-family: arial;
    font-weight: bold;
   }

   h3{
    margin: 0;
    font-size: 100%;
    font-family: arial;
   }     

   #_data{
    font-family: arial;
    font-size: 12px;
   }

   table{
    border-collapse: collapse;
    border: 1px solid black;
   }      

   tr:nth-child(1) > td:nth-child(5){         
    border-right: 1px solid black;
   }

   tr:nth-child(2) > td:nth-child(1){
    border-top: 1px solid black;        
   }

   tr:nth-child(2) > td:nth-child(2), 
   tr:nth-child(2) > td:nth-child(3){        
    border: 1px solid black;
   }

   tr:nth-child(3) > td:nth-child(1), 
   tr:nth-child(3) > td:nth-child(2){        
    border: 1px solid black;
   }

   tr:nth-child(4) > td:nth-child(1), 
   tr:nth-child(4) > td:nth-child(2){        
    border: 1px solid black;
   }

   tr:nth-child(5) > td:nth-child(1), 
   tr:nth-child(5) > td:nth-child(2){        
    border: 1px solid black;
   }

   #_surat{
    width: 45%;
    margin: 0 auto;
    font-family: arial;
    font-weight: bold;         
   }

   #_form{                
    width: 15%;
    font-size: 12px;
    text-align: left;
    margin-left: 80%;
    margin-right:2%;
    padding: 0.5%;
    border: 1px solid black;        
   }

   #no{
    margin-top: 1%;        
    font-family: arial;
   }

   #_isi-content{
    text-align: left;
    margin-left: 2%;
    font-size: 12px;
    font-family: tahoma;
   }

   #_center-content{
    text-align: left;
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;        
    border:1px solid black;
    padding-top: 1.5%;
    padding-left: 1%;        
    padding-bottom: 1.5%;
    font-family: tahoma;
    font-size: 12px;
   }

   #_table-content{
    text-align: left;        
    font-family: tahoma;
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;
   }
  </style>
 </head>  
 <body>     
  <div id="_wrapper">
   <div id="_content">
    <div id="_top-content">          
     <table style="width: 100%;max-width: 100%;">
      <tr>
       <td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>                                                    
       <td>                  
        <h3>&nbsp;PT PLN (PERSERO)</h3>
        <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
       </td>                                          
       <td colspan="70"></td>                
       <td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
      </tr>                
      <tr>
       <td id="_judul" colspan="60" rowspan="4">
      <center>
       <label>
        FORMULIR IJIN KERJA <?php echo strtoupper($data_wp['tipe']) ?>
       </label>
      </center>                  
      </td>                     
      <td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
      <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
      </tr>              
      <tr>                       
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>1 dari 8</label></td>
      </tr>
     </table>                                                                               
    </div>                
    <div><hr/></div>     
    <div  id="_form">
     <b><label>FORM : WP 1A</label></b>
    </div>                  
    <div id="_surat">          
     <u><label style="text-decoration: underline;">PERMOHONAN IJIN KERJA</label>    </u>
     <br/>
     <i><label style="font-style: italic;">Request For Clearance</label></i>
    </div>        
    <div id="no">
     <b><label style="font-weight: bold;">Nomor : <?php echo $data_wp['no_wp'] ?></label></b>
    </div>
    <div id="_isi-content">
     <u><label style="text-decoration: underline;">PERUSAHAAN</label></u>&nbsp;&nbsp;&nbsp;<label>  :   <?php echo $data_wp['perusahaan'] ?> </label>           
     <br/>
     <i><label style="font-style: italic;">COMPANY</label></i>
     <br/>
     <u><label style="text-decoration: underline;">URAIAN PEKERJAAN</label></u><label> :  <?php echo $data_wp['uraian_pekerjaan'] ?></label>
     <br/>
     <label>JOB DESCRIPTION</label>
     <br/>
     <?php
     $ket_padam = 'YA / TIDAK';
//     if ($row->is_need_sistem == 1) {
//      $ket_padam = '<b>YA</b> /  TIDAK';
//     }
     ?>
     <u><label style="text-decoration: underline;">APAKAH DIPERLUKAN PADAM</label></u><label> : ___________________________________<?php echo $ket_padam ?>____</label>
     <br/>
     <i><label style="font-style: italic;">IS SHUTDOWN REQUIRED</label></i>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          
     <label>Yes   /   No</label>
     <br/>
     <u><label style="text-decoration: underline;">APAKAH DIPERLUKAN GROUNDING/EARTHING</label></u><label> : _______________________YA  /  TIDAK____</label>
     <br/>
     <i><label style="font-style: italic;">IS GROUNDING(EARTHING) REQUIRED</label></i>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;          
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <label>Yes   /   No</label>
     <br/>
     <u><label style="text-decoration: underline;">PERALATAN YANG PERLU DIPADAMKAN</label></u><label> : <?php echo '' ?></label>
     <br/>
     <i><label style="font-style: italic;">REQUIRED ISOLATING POINT/EQUIPMENT</label></i>
     <br/>
     <u><label style="text-decoration: underline;">ALAT KESELAMATAN KERJA DISEDIAKAN OLEH</label></u><label> : <?php echo $data_wp['perusahaan'] ?></label>          
     <br/>
     <i><label style="font-style: italic;">SAFETY EQUIPMENT PROVIDED BY</label></i>
    </div>
    <div id="_center-content">
     <u><label style="text-decoration: underline;">LOKASI/AREA KERJA</label></u><label> : <?php echo $data_wp['upt_tujuan'] . ', ' . $data_wp['lokasi_pekerjaan'] ?></label>
     <br/>
     <br/>
     <i><label style="font-style: italic;">JOB LOCATION/AREA</label></i>
     <br/>
     <i><label style="font-style: italic;"></label></i>
    </div>
    <div id="_table-content">
     <table style="width: 100%;border: 1px solid black;">
      <tr>
       <th style="border:1px solid black;font-family: tahoma;">No</th>
       <th style="border:1px solid black;font-family: tahoma;">Uraian</th>
       <th style="border:1px solid black;font-family: tahoma;">Nama</th>
       <th style="width: 30%;border:1px solid black;font-family: tahoma;">Jabatan di Perusahaan</th>
      </tr>           
      <tr>
         <td rowspan="2" style="text-align: center;font-size: 12px;padding-left: 1%;padding-top: 4%;padding-bottom: 4%;border: 1px solid black;font-family: tahoma;"><b><label>1</label></b></td>
         <td style="border: 1px solid black;font-family: tahoma;padding-left: 2%;padding-top: 5%;padding-bottom: 5%;">
          <u><label>PENANGGUNG JAWAB PEKERJAAN </label></u>
          <br/>
          <label style="margin-left: 2%;">JOB DIRECTOR</label>
         </td>
         <td rowspan="2" style="padding:2%; border: 1px solid black;font-family: tahoma;padding-left: 2%;">
          <b><label><?php echo $data_wp['penanggung_jawab_pekerjaan'] ?></label></b>
         </td>
         <td style="padding:2%; border: 1px solid black;font-family: tahoma;padding-left: 2%;">
          <b><label><?php echo $data_wp['jabatan_penanggung_jawab'] ?></label></b>
         </td>
        </tr>
        <tr>                                          
        </tr>

        <tr>
         <td rowspan="1" colspan="4"></td>
        </tr>
        <tr>
         <td rowspan="2" style="text-align: center;font-size: 12px;padding-left: 1%;padding-top: 4%;padding-bottom: 4%;border: 1px solid black;font-family: tahoma;"><b><label>2</label></b></td>
         <td style="border: 1px solid black;font-family: tahoma;padding-left: 2%;padding-top: 5%;padding-bottom: 5%;">
          <u><label>PENGAWAS PEKERJAAN  </label></u>
          <br/>
          <b><label style="margin-left: 2%;">JOB SUPERVISOR</label></b>
         </td>
         <td rowspan="2" style="padding:2%; border: 1px solid black;font-family: tahoma;padding-left: 2%;">
          <b><label><?php echo $data_wp['pengawas_pekerjaan'] ?></label></b>
         </td>
         <td style="padding:2%; border: 1px solid black;font-family: tahoma;padding-left: 2%;">
          <b><label><?php echo $data_wp['jabatan_pengawas_pekerjaan'] ?></label></b>
         </td>
        </tr>
        <tr>                                          
        </tr>

        <tr>
         <td rowspan="1" colspan="4"></td>
        </tr>
        <tr>
         <td rowspan="2" style="text-align: center;font-size: 12px;padding-left: 1%;padding-top: 4%;padding-bottom: 4%;border: 1px solid black;font-family: tahoma;"><b><label>3</label></b></td>
         <td style="border: 1px solid black;font-family: tahoma;padding-left: 2%;padding-top: 5%;padding-bottom: 5%;">
          <u><label>PENGAWAS K3  </label></u>
          <br/>
          <b><label style="margin-left: 2%;">SAFETY SUPERVISOR</label></b>
         </td>
         <td rowspan="2" style="padding:2%; border: 1px solid black;font-family: tahoma;padding-left: 2%;">
            <?php foreach($data_wp['data_pengawas_k3'] as $v_pe){ ?>
               <b><label><?php echo $v_pe['nama'] ?></label></b><br/>
            <?php } ?>
         </td>
         <td style="padding:2%; border: 1px solid black;font-family: tahoma;padding-left: 2%;">
         <?php foreach($data_wp['data_pengawas_k3'] as $v_pe){ ?>
            <b><label><?php echo $v_pe['jabatan'] ?></label></b><br/>
            <?php }?>
         </td>              
        </tr>
        <tr>                            

        </tr>
     </table>
    </div>
   </div>
  </div>   
  <div style="text-align: right;">
  		<?php if(trim($data_wp['permit_veirifikasi']) != ''){ ?>
         <img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['ttd_spv_gi'] ?>" width="20px" height="20px" style="text-align: right;">
		<?php }else{?>
         <img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][0]['file'] ?>" width="20px"
            height="20px" style="text-align: right;">
		<?php }?>
  </div>
 </body>
</html>
