<html>

<head>
  <title></title>
  <style>
    #_wrapper {
      width: 100%;
      margin: 0 auto;
    }

    #_content {
      border: 1px solid #999;
      max-width: 100%;
      text-align: center;
    }

    #_top-content {
      width: 95%;
      max-width: 95%;
      margin: 1% auto;
    }

    #_judul {
      font-size: 100%;
      font-family: arial;
      font-weight: bold;
    }

    h3 {
      margin: 0;
      font-size: 100%;
      font-family: arial;
    }

    #_data {
      font-family: arial;
      font-size: 12px;
    }

    table {
      border-collapse: collapse;
      border: 1px solid black;
    }

    #_surat {
      width: 45%;
      margin: 0 auto;
      font-family: tahoma;
    }

    #_form {
      width: 15%;
      font-size: 12px;
      text-align: left;
      margin-left: 80%;
      margin-right: 2%;
      padding: 0.5%;
      border: 1px solid black;
    }

    #no {
      margin-top: 1%;
      font-family: arial;
    }

    #_isi-content {
      text-align: left;
      margin-left: 2%;
      font-size: 12px;
      font-family: tahoma;
    }

    #_center-content {
      text-align: left;
      margin-top: 2%;
      margin-left: 2%;
      margin-right: 2%;
      border: 1px solid black;
      padding-top: 1.5%;
      padding-left: 1%;
      padding-bottom: 1.5%;
      font-family: tahoma;
      font-size: 12px;
    }

    #_table-content {
      text-align: left;
      font-family: tahoma;
      margin-top: 2%;
      margin-left: 2%;
      margin-right: 2%;
    }
  </style>
</head>

<body>
  <div id="_wrapper">
    <div id="_content">
      <div id="_top-content">
        <table style="width: 100%;max-width: 100%;">
          <tr>
            <td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
            <td>
              <h3>&nbsp;PT PLN (PERSERO)</h3>
              <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
            </td>
            <td colspan="70"></td>
            <td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
          </tr>
          <tr>
            <td id="_judul" colspan="60" rowspan="4">
              <center>
                <label>
                  DOKUMEN WORKING PERMIT/IJIN KERJA INTERNAL
                </label>
              </center>
            </td>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
          </tr>
          <tr>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
          </tr>
          <tr>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
          </tr>
          <tr>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
            <td id="_data" colspan="8" style="border: 1px solid black;"><label>6 dari 8</label></td>
          </tr>
        </table>
      </div>
      <div>
        <hr />
      </div>
      <div id="_form">
        <b><label>FORM : WP 3</label></b>
      </div>
      <div style="font-family: tahoma;">
        GAMBAR SINGLE LINE DIAGRAM (DENAH KERJA YANG AMAN)
      </div>
      <div style="height: 30%;border:2px solid black;margin-left: 2%;margin-right: 2%;margin-top: 2%;padding: 3%;">
        
      </div>
    </div>
</body>

</html>
