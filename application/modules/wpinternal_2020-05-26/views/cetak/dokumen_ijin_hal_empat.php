<html>
 <head>
  <title></title>    
  <style>
   #_wrapper{
    width: 100%;        
    margin: 0 auto;               
   }

   #_content{
    border: 1px solid #999;
    max-width: 100%;
    text-align: center;        
   }

   #_top-content{
    width: 95%;
    max-width: 95%;                
    margin: 1% auto;        
   }      

   #_judul{
    font-size: 100%;
    font-family: arial;
    font-weight: bold;
   }

   h3{
    margin: 0;
    font-size: 100%;
    font-family: arial;
   }     

   #_data{
    font-family: arial;
    font-size: 8px;
   }

   table{
    border-collapse: collapse;
    border: 1px solid black;
   }      

   tr:nth-child(1) > td:nth-child(5){         
    border-right: 1px solid black;
   }

   tr:nth-child(2) > td:nth-child(1){
    border-top: 1px solid black;        
   }

   tr:nth-child(2) > td:nth-child(2), 
   tr:nth-child(2) > td:nth-child(3){        
    border: 1px solid black;
   }

   tr:nth-child(3) > td:nth-child(1), 
   tr:nth-child(3) > td:nth-child(2){        
    border: 1px solid black;
   }

   tr:nth-child(4) > td:nth-child(1), 
   tr:nth-child(4) > td:nth-child(2){        
    border: 1px solid black;
   }

   tr:nth-child(5) > td:nth-child(1), 
   tr:nth-child(5) > td:nth-child(2){        
    border: 1px solid black;
   }

   #_surat{
    width: 45%;        
    margin: 0 auto;
    font-family: tahoma;        
   }

   #_form{                
    width: 15%;
    font-size: 8px;
    text-align: left;
    margin-left: 80%;
    margin-right:2%;
    padding: 0.5%;
    border: 1px solid black;        
   }

   #no{
    margin-top: 1%;        
    font-family: arial;
   }

   #_isi-content{
    text-align: left;
    margin-left: 2%;
    font-size: 8px;
    font-family: tahoma;
   }

   #_center-content{
    text-align: left;
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;        
    border:1px solid black;
    padding-top: 1.5%;
    padding-left: 1%;        
    padding-bottom: 1.5%;
    font-family: tahoma;
    font-size: 8px;
   }

   #_table-content{
    text-align: left;         
    font-family: tahoma;
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;
   }
  </style>
 </head>  
 <body>        
  <div id="_wrapper">
   <div id="_content">
    <div id="_top-content">          
     <table style="width: 100%;max-width: 100%;">
      <tr>
       <td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>                                                    
       <td>                  
        <h3>&nbsp;PT PLN (PERSERO)</h3>
        <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
       </td>                                          
       <td colspan="70"></td>
       <td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
      </tr>                
      <tr>
       <td id="_judul" colspan="60" rowspan="4">
      <center>
       <label>
        FORMULIR IJIN KERJA <?php echo strtoupper($data_wp['tipe']) ?>
       </label>
      </center>                  
      </td>                
      <td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
      <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
      </tr>               
      <tr>                       
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>4 dari 8</label></td>
      </tr>
     </table>                                                                               
    </div>                
    <div><hr/></div> 
    <div  id="_form">
     <b><label>FORM : WP 1C</label></b>
    </div>
    <div id="_table-content" style="font-size: 8px;">
     <table style="width: 100%;">
      <tr style="font-family: 8px;">
       <td colspan="8" style="font-weight: bold;font-family: tahoma; font-size: 8px;">PELAKSANA / CONDUCTED</td>              
      </tr>
      <?php if (!empty($data_wp['pelaksana'])) { ?>
			<?php $no=1; ?>
       <?php foreach ($data_wp['pelaksana'] as $value) { ?>
        <tr>
         <td style="border:1px solid black;text-align: center;width: 5%;font-family: tahoma;font-size:8px;"><?php echo $no++ ?></td>
         <td style="border:1px solid black;width: 20%;font-family: tahoma;font-size: 8px;"><?php
         echo $value['nama'];
          ?>
         </td>
<!--         <td style="border:1px solid black;text-align: center;width: 5%;font-family: tahoma;font-size:8px;">7</td>
         <td style="border:1px solid black;width: 20%;font-family: tahoma;font-size:8px;"><?php
          echo $value['nama'];
          ?></td>
         <td style="border:1px solid black;text-align: center;width: 5%;font-family: tahoma;font-size:8px;">13</td>
         <td style="border:1px solid black;width: 20%;font-family: tahoma;font-size:8px;"><?php
         echo $value['nama'];
          ?></td>
         <td style="border:1px solid black;text-align: center;width: 5%;font-family: tahoma;font-size:8px;">19</td>
         <td style="border:1px solid black;width: 20%;font-family: tahoma;font-size:8px;"><?php
          echo $value['nama'];
          ?></td>-->
        </tr>
       <?php } ?>
      <?php } ?>      
     </table>
    </div>                        
    <div style="margin-left: 2%;font-family: tahoma;margin-right: 2%;">
     <table style="width: 100%;">
      <tr>              
       <td  style="border:1px solid black;padding: 1% 1%;width: 50%;font-family: tahoma;font-size: 10px;"><br/>
        <u>TANGGAL DIMULAI</u> : <?php echo $data_wp['tgl_awal'] ?>
        <br/>
        STARTING DATE                   
       </td>
       <td  style="border:1px solid black;padding: 1% 1%;width: 50%;font-family: tahoma;font-size: 10px;"><br/>
        <u>JAM  DIMULAI</u> : 08.00
        <br/>
        STARTING TIME                   
       </td>
      </tr>
      <tr>              
       <td  style="border:1px solid black;padding: 1% 1%;font-family: tahoma;font-size: 10px;"><br/>
        <u>TANGGAL SELESAI</u> : <?php echo $data_wp['tgl_akhir'] ?>
        <br/>
        EXPIRING DATE                   
       </td>
       <td  style="border:1px solid black;padding: 1% 1%;font-family: tahoma;font-size: 10px;"><br/>
        <u>JAM  SELESAI</u> : 16.00
        <br/>
        STARTING TIME                   
       </td>
      </tr>
     </table>
    </div>
    <br/>
    <div style="margin-left: 2%;margin-right: 2%;text-align: left;font-family: tahoma;font-size: 11px;">          
     <div style="text-align: center;">
      <u><b><label>CATATAN KHUSUS DAN PEMBERITAHUAN:</label></b></u>
      <br/>
      <b><label>SPECIAL CONDITION AND INFORMATION</label></b>
     </div>
    </div>
    <div style="margin-left: 2%;margin-right: 2%;text-align: left;font-family: tahoma;font-size: 12px;">
     Guna mendukung penerapan SMK3 dengan pencapaian <b>Zero Accident</b> dan <b>Zero Human Error</b>, maka kami berkomitmen untuk :
     <ol>
      <?php
      if (!empty($catatan_khusus)) {
       foreach ($catatan_khusus->result_array() as $dt) {
        echo '<li>' . $dt['catatan'] . '</li>';
       }
      }
      ?>
     </ol>
    </div>         
    <br/>        
    <div style="text-align: left;margin-left: 2%;font-family: tahoma;font-size: 12px;">
     <div style="text-align: right;margin-right: 24%;">
      <?php echo $data_wp['upt_tujuan'] . ', ' . date('d F Y') ?>
     </div>
     <table style="width: 100%;border: none;">
      <tr style="border:none;">
       <td style="font-family: tahoma;font-size: 12px;border: none;">
        &nbsp;&nbsp;<u>MENGETAHUI</u>
        <br/>
        &nbsp;&nbsp;Approved By
       </td>
       <td style="font-family: tahoma;font-size: 12px;border: none;">
        &nbsp;&nbsp;<u>MENGETAHUI</u>
        <br/>
        Acknowledged By
       </td>              
       <td style="font-family: tahoma;font-size: 12px;border: none;">
        <u>PEMOHON</u>
        <br/>
        Requested By
       </td>
      </tr>
      <tr style="border: none;">
       <td style="font-family: tahoma;font-size: 12px;border: none;padding-top: 3%;">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url() . 'files/berkas/ttd/'.$data_wp['paraf'][count($data_wp['paraf']) - 1]['ttd']?>" width="50px" height="50px">
        <br/>                    
        <u><b>(<?php echo $data_wp['approval_last']['nama_pegawai'] ?>)</b></u>
        <br/>                
        <?php
        if ($data_wp['upt_tujuan'] == 'Kantor Induk') {
         echo '<label style="font-size:14px;">MANAGER UPT / DAL K3L.</label>';
        } else {
         echo '<label style="font-size:14px;">MANAGER UPT / DAL K3L.</label>';
        }
        ?> &nbsp; <img src="<?php echo base_url() . 'files/berkas/paraf/' . $data_wp['approval_last']['file'] ?>" width="20px" height="20px">
       </td>
			 <td style="font-family: tahoma;font-size: 12px;border: none;padding-top: 5%;">
			 <?php if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') { ?>
			 <?php }else{?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url() . 'files/berkas/ttd/'.$data_wp['paraf'][0]['ttd']?>" width="50px" height="50px">
			 <?php }?>			 	
        <u><b>(<?php
          if ($data_wp['tipe'] == 'EKSTERNAL') {
           if ($data_wp['jenis_lokasi']  == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') {
            echo $data_wp['spv_gi'];
           } else if ($data_wp['jenis_lokasi'] == "Kantor Induk") {
            echo $data_wp['paraf'][0]['nama_pegawai'];
           } else {
            echo $data_wp['paraf'][0]['nama_pegawai'];
         }
          } else {
           if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') {
            echo $data_wp['spv_gi'];
           } else {
            echo $data_wp['paraf'][0]['nama_pegawai'];
           }
          }
          ?>)</b></u>
        <br/> 
				<?php if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') { ?>
					SPV GI
				<?php }else{ ?>
					SPV / LAKSK4 / Admin UIT
				<?php } ?>
        <?php
//                if($row->gardu_induk_id != 0 && $row->level_id == 1){
//                 echo strtoupper($row->gardu_induk);
//                }else{
//                  echo 'LK2';
//                }
        // if ($data_wp['tipe'] == 'EKSTERNAL') {
        //  if ($data_wp['jenis_lokasi'] == 'GARDU INDUK') {
        //   echo strtoupper($data_wp['nama_place']);
        //  } else {
        //   echo 'K3';
        //  }
        // } else {
        //  if ($data_wp['tipe'] == 'GARDU INDUK') {
        //   echo strtoupper($data_wp['nama_place']);
        //  } else {
        //   echo 'K3';
        //  }
        // }
        ?>
       </td>              
       <td style="font-family: tahoma;font-size: 12px;border: none;padding-top: 5%;">
        <u><b>(<?php echo $data_wp['nama_pemohon'] ?>)</b></u>                                
       </td>
      </tr>
     </table>         
    </div>              
   </div>      
  </div>
  <div style="text-align: right;">
   <img src="<?php echo base_url() . 'files/berkas/paraf/' . $data_wp['paraf'][0]['file'] ?>" width="20px" height="20px" style="text-align: right;">
  </div>
 </body>
</html>
