<!--<div class="col-md-6">
 &nbsp;
</div>-->
<div class="col-md-12">
 <div class="table-responsive">
  <table class="table">
   <thead>
    <tr class="bg-danger">
     <th colspan="3">Keterangan Penilaian Resiko</th>
    </tr>
    <tr class="">
     <th class="text-center">Akibat</th>
     <th class="text-center">Paparan</th>
     <th class="text-center">Peluang</th>
    </tr>
   </thead>
   <tbody>
    <tr>
     <td>1 - Cidera Ringan (P3K)</td>
     <td>0 - Tidak Ada Paparan / Pajanan</td>
     <td>0.1 - Tidak Mungkin Terjadi</td>
    </tr>
    <tr>
     <td>3 - Perawatan Medis</td>
     <td>0.5 - Sangat Jarang (1x/Thn)</td>
     <td>0.5 - Sangat Kecil Kemungkinan</td>
    </tr>
    <tr>
     <td>7 - Cacat</td>
     <td>1 - Jarang (> 1x/Thn)</td>
     <td>1 - Kecil Kemungkinan</td>
    </tr>
    <tr>
     <td>15 - Satu Kematian</td>
     <td>2 - Tidak Sering (1x/Bln)</td>
     <td>3 - Masih Ada Kemungkinan</td>
    </tr>
    <tr>
     <td>40 - Beberapa Kematian (> 1)</td>
     <td>3 - Kadang - Kadang (1x/Mgg)</td>
     <td>6 - Besar Akan Terjadi (50-50)</td>
    </tr>
    <tr>
     <td>100 - Banyak Kematian (> 1)</td>
     <td>6 - Sering (1x/Hari)</td>
     <td>10 - Hampir Pasti Terjadi</td>
    </tr>
    <tr>
     <td>&nbsp;</td>
     <td>10 - Terus Menerus (> 1x/Hari)</td>
     <td>&nbsp;</td>
    </tr>
   </tbody>
  </table>

 </div>
</div>