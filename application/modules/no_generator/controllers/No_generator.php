<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class No_generator extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function digit_count($length, $value)
    {
        while (strlen($value) < $length)
            $value = '0' . $value;
        return $value;
    }

    public function generateNoPengajuanVendor()
    {
        $no = 'VENDOR' . date('y') . strtoupper(date('M'));
        $data = Modules::run('database/get', array(
            'table' => 'pengajuan_vendor',
            'like' => array(
                array('no_pengajuan', $no)
            ),
            'orderby' => 'id desc'
        ));

        $seq = 1;
        if (!empty($data)) {
            $data = $data->row_array();
            $seq = str_replace($no, '', $data['no_pengajuan']);
            $seq = intval($seq) + 1;
        }

        $seq = $this->digit_count(3, $seq);
        $no .= $seq;
        return $no;
    }

    public function generateNoPengajuanWP($int = "INT")
    {
        $no = 'WP' . $int . date('y') . strtoupper(date('M'));
        $data = Modules::run('database/get', array(
            'table' => 'permit',
            'like' => array(
                array('no_wp', $no)
            ),
            'orderby' => 'id desc'
        ));

        $seq = 1;
        if (!empty($data)) {
            $data = $data->row_array();
            $seq = str_replace($no, '', $data['no_wp']);
            $seq = intval($seq) + 1;
        }

        $seq = $this->digit_count(3, $seq);
        $no .= $seq;
        return $no;
    }


    public function generateNoTransLog()
    {
        $no = 'TRANS' . date('y') . strtoupper(date('M'));
        $data = Modules::run('database/get', array(
            'table' => 'permit_transaction_log',
            'like' => array(
                array('no_trans', $no)
            ),
            'orderby' => 'id desc'
        ));

        $seq = 1;
        if (!empty($data)) {
            $data = $data->row_array();
            $seq = str_replace($no, '', $data['no_trans']);
            $seq = intval($seq) + 1;
        }

        $seq = $this->digit_count(4, $seq);
        $no .= $seq;
        return $no;
    }

    public function generateNoJsa()
	{
		$no = 'JSA' . date('y') . strtoupper(date('M'));
		$data = Modules::run('database/get', array(
			'table' => 'permit_jsa',
			'like' => array(
				array('no_jsa', $no)
			),
			'orderby' => 'id desc'
		));

		$seq = 1;
		if (!empty($data)) {
			$data = $data->row_array();
			$seq = str_replace($no, '', $data['no_jsa']);
			$seq = intval($seq) + 1;
		}

		$seq = $this->digit_count(3, $seq);
		$no .= $seq;
		return $no;
	}

    public function generateNoDocument()
	{
		$no = 'DOC' . date('y') . strtoupper(date('M'));
		$data = Modules::run('database/get', array(
			'table' => 'document',
			'like' => array(
				array('no_document', $no)
			),
			'orderby' => 'id desc'
		));

		$seq = 1;
		if (!empty($data)) {
			$data = $data->row_array();
			$seq = str_replace($no, '', $data['no_document']);
			$seq = intval($seq) + 1;
		}

		// echo $seq;die;
		$seq = $this->digit_count(4, $seq);
		$no .= $seq;
		return $no;
	}

    public function generateNoPengajuanIkaVendor($user = '')
    {
        $no = 'IK'.$user . date('y') . strtoupper(date('M'));
        $data = Modules::run('database/get', array(
            'table' => 'instruksi_kerja',
            'like' => array(
                array('no_instruksi', $no)
            ),
            'orderby' => 'id desc'
        ));

        $seq = 1;
        if (!empty($data)) {
            $data = $data->row_array();
            $seq = str_replace($no, '', $data['no_instruksi']);
            $seq = intval($seq) + 1;
        }

        $seq = $this->digit_count(3, $seq);
        $no .= $seq;
        return $no;
    }
    
    public function generateNoDocumentIkaVendor($data, $createddate = '')
    {
        // echo $createddate;die;
        $year = date('Y', strtotime($createddate));
        $month = date('m', strtotime($createddate));
        $romawiMonth = $this->getRomawiMonth($month);
        $doc = $data->id.'/LSE/IKA-DG/'.$romawiMonth.'/'.$year;
        return $doc;
    }

    public function getRomawiMonth($month){
        switch (intval($month)) {
            case 1:
                return 'I';
                break;
            case 2:
                return 'II';
                break;
            case 3:
                return 'III';
                break;
            case 4:
                return 'IV';
                break;
            case 5:
                return 'V';
                break;
            case 6:
                return 'VI';
                break;
            case 7:
                return 'VII';
                break;
            case 8:
                return 'VIII';
                break;
            case 9:
                return 'IX';
                break;
            case 10:
                return 'X';
                break;
            case 11:
                return 'XI';
                break;
            case 12:
                return 'XII';
                break;
            
            default:
                # code...
                break;
        }
    }
}
