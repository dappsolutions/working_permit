<?php

class User extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_user', 'user');
	}

	public function getUser($user_id = "")
	{
		$result = $this->user->getUser($user_id);
		return $result;
	}
}
