
<?php

class M_login extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function signIn($params)
	{
		$sql = "
		select u.*
		, u.id as user_id
		, u.username as email
		, pg.nama as name
		, gihp.id as gihp_id
		, pt.tipe 
		, ut.id as upt_id
		from `user` u	
		join pegawai pg
			on pg.id = u.pegawai		
		join pegawai_type as pt
			on pt.id = pg.pegawai_type
		join upt as ut
			on ut.id = pg.upt
		left join (
			select max(id) as id, pegawai from gardu_induk_has_pegawai group by pegawai
		)	gihp_max 		
			on gihp_max.pegawai = pg.id
		left join gardu_induk_has_pegawai gihp
			on gihp_max.id = gihp.id
		where u.username = '" . $params['username'] . "'
		and u.password = '" . $params['password'] . "'";

		$data = $this->db->query($sql);

		$result = [];
		if (!empty($data->result_array())) {			
			$data = $data->row_array();
			$data['akses'] = $data['tipe'];
			if($data['gihp_id'] != ''){
				$data['akses'] = 'GI';
			}else{
				$dataManUltg = $this->getDataManUltg($data);
				if(!empty($dataManUltg)){
					$data['akses'] = 'ULTG';
				}else{
					$dataAdminUpt = $this->getDataAdminUpt($data);
					if(!empty($dataAdminUpt)){
						$data['akses'] = 'UPT';
					}
				}
			}
			$result = $data;
		}

		return $result;
	}

	public function getDataManUltg($data){
		$sql = "select * from struktur_approval sa
		join struktur_approval_has_sub_upt sahsu 
			on sahsu.struktur_approval = sa.id 
		where sa.upt = ".$data['upt_id']."
		and sa.`user` = '".$data['id']."'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {			
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDataAdminUpt($data){
		$sql = "select * from struktur_approval sa
		where sa.upt = ".$data['upt_id']."
		and sa.`user` = '".$data['id']."'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {			
			$result = $datadb->row_array();
		}
		return $result;
	}
}
