<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Working Permit | Register</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/square/blue.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
</head>

<body class="hold-transition register-page">
	<div class="row">
		<div class="col-md-5">
			<div class="register-box">
				<div class="register-logo">
					<a href="#"><b>Registrasi User</b> Simson</a>
				</div>

				<div class="register-box-body">
					<p class="login-box-msg">Registrasi User</p>

					<form action="" method="post">
						<div class="form-group has-feedback">
							<input type="text" class="form-control required" error="Nama Pegawai" placeholder="Nama Pegawai" id="nama">
							<span class="glyphicon glyphicon-user form-control-feedback"></span>
						</div>
						<div class="form-group has-feedback">
							<input type="email" class="form-control required" id="email" error="Email" placeholder="Email">
							<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						</div>
						<div class="form-group has-feedback">
							<input type="number" class="form-control required" id="no_hp" placeholder="No Hp" error="No Hp">
							<span class="glyphicon glyphicon-phone form-control-feedback"></span>
						</div>
						<div class="form-group has-feedback">
							<select class="form-control required" error="Upt" id="upt" style="width: 100%;">
								<option value="">Pilih Upt</option>
								<?php if (!empty($list_upt)) { ?>
									<?php foreach ($list_upt as $value) { ?>
										<?php $selected = '' ?>
										<?php if (isset($upt)) { ?>
											<?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
										<?php } ?>
										<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
						<div class="row">
							<!-- /.col -->
							<div class="col-md-12">
								<button type="submit" class="btn btn-primary btn-block btn-flat" onclick="RegisterSimson.register(this, event)">Register</button>
								<a href="<?php echo base_url() ?>" class="btn btn-danger btn-block btn-flat">Batal</a>
							</div>
							<!-- /.col -->
						</div>
					</form>

				</div>
				<!-- /.form-box -->
			</div>
		</div>

		<div class="col-md-7" style="margin-top: 16px;">
			<div class="box padding-16">
				<div class="box-header with-border">
					<h4 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo strtoupper('daftar user simson') ?></h4>
					<div class="divider"></div>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<div class="box-body">
					<div class="divider"></div>
					<br />
					<?php if (isset($keyword)) { ?>
						<br />
						<div class="row">
							<div class="col-md-6">
								Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
							</div>
						</div>
					<?php } ?>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<div class="content_vendor" style="height: 450px;">
									<table class="table table-bordered" id="tb_content">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>No</th>
												<th>Nama Pegawai</th>
												<th>Email</th>
												<th>No Hp</th>
												<th>Tujuan Unit</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php if (!empty($content)) { ?>
												<?php $no = 1; ?>
												<?php foreach ($content as $value) { ?>
													<?php $tr_color = "bg-warning"; ?>
													<?php $status = "DIAJUKAN" ?>
													<?php if ($value['approveby'] != '') { ?>
														<?php $tr_color = "bg-success"; ?>
														<?php $status = "DIACC" ?>
													<?php } ?>
													<?php if ($value['rejectedby'] != '') { ?>
														<?php $tr_color = "bg-danger"; ?>
														<?php $status = "DITOLAK" ?>
													<?php } ?>
													<tr class="<?php echo $tr_color ?>">
														<td><?php echo $no++ ?></td>
														<td><?php echo $value['nama'] ?></td>
														<td><?php echo $value['email'] ?></td>
														<td><?php echo $value['no_hp'] ?></td>
														<td><?php echo $value['nama_upt'] ?></td>
														<td><?php echo $status ?></td>
													</tr>
												<?php } ?>
											<?php } else { ?>
												<tr>
													<td colspan="6" class="text-center">Tidak ada data ditemukan</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery 3 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/icheck.min.js"></script>

	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
	<script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/js/message.js"></script>
	<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/controllers/registersimson.js"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});
		});
	</script>
</body>

</html>