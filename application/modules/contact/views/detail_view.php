<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="row">
	<div class="col-md-12">
		<!-- Horizontal Form -->
		<div class="box box-info padding-16">
			<!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
			<div class="box-header with-border" style="margin-top: 12px;">
				<h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Nama Upt</label>

						<div class="col-sm-4">
							<select disabled name="" id="upt" class="form-control required" error="UPT">
								<?php if (!empty($list_upt)) { ?>
									<?php foreach ($list_upt as $key => $value) { ?>
										<?php $selected = "" ?>
										<?php if (isset($id)) { ?>
											<?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
										<?php } ?>
										<option value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Nama</label>

						<div class="col-sm-4">
							<input disabled type="text" class="required form-control" error="Nama" id="nama" value="<?php echo isset($nama) ? $nama : '' ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-sm-2 control-label">No HP</label>

						<div class="col-sm-4">
							<input disabled type="text" class="required form-control" error="No HP" id="no_hp" value="<?php echo isset($no_hp) ? $no_hp : '' ?>">
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="Contact.back()">Cancel</button>
				</div>
				<!-- /.box-footer -->
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
