<?php

class Fcm extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function send($message = "", $to = "")
	{

		$content      = array(
			"en" => $message
		);
		$headings      = array(
			"en" => 'Informasi Working Permit'
		);
		$fields = array(
			'app_id' => "ccd27271-600a-451b-9862-2a5d3b732d05",
			// 'included_segments' => array(
			// 	'Subscribed Users'
			// ),
			// 'include_player_ids' => array(
			// 	'1104b1d6-3185-42de-b3f6-8ffeddf7890f'
			// ),
			'include_player_ids' => array(
				$to
			),
			'data' => array(
				"nowp" => "",
			),
			'contents' => $content,
			'headings' => $headings,
		);

		$fields = json_encode($fields);
		print("\nJSON sent:\n");
		print($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json; charset=utf-8',
			'Authorization: Basic Y2MwNDdmMzUtYWRjNi00MDhjLWIzN2UtNGFmNDUxMDM0MGNi'
		));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);
	}

	public function sendWa($params)
	{
		echo 'sendwa';die;
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://api.fonnte.com/send',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array(
				'target' => '6281233933300',
				'message' => 'test message free',
				'countryCode' => '62', //optional
			),
			CURLOPT_HTTPHEADER => array(
				'Authorization: btWRrqbj#CKpp4-rHSw-' //change TOKEN to your actual token
			),
		));
	}
}
