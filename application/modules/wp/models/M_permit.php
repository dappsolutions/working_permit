
<?php

class M_permit extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataPekerjaDanPelaksana($params = array())
	{

		$filter_data = "";
		$sql = " 
		select
		p.*
		from permit p 
		where p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['pelaksana'] = $this->getPelaksana($value);
				$value['penanggung_jawab'] = $this->getPenanggungJawab($value);
				$value['pengawas_pekerjaan'] = $this->getPelaksanaPekerjaan($value);
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataPekerjaDanPelaksanaDetailAtt($params = array())
	{

		$filter_data = "";
		$dateAbsen = date('Y-m-d', strtotime($params['tgl_absen']));
		$sql = " 	select 
		pa.*
		from document d
		join permit_attandance pa
			on pa.document = d.id
		join permit p 
			on p.id=pa.permit 
		where d.doc_type = 'DOCT_ATT'
		and p.no_wp = '" . $params['no_wp'] . "'
		and cast(pa.createddate as date) = '" . $dateAbsen . "'
	";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['name'] = $value['nama_pekerja'];
				$value['is_masuk'] = $value['hadir'];
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getPelaksana($data)
	{
		$sql = "select * from permit_pelaksana pp
		where pp.permit = " . $data['id'] . " and pp.deleted = 0";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$value['name'] = $value['nama'];
				$value['tipe'] = 'pelaksana';
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getData($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$filterVerify = '';
		if (isset($data['filter_verifi'])) {
			$filterVerify = "and pv_max.id is null ";
		}

		$filterTujuanLokasi = '';
		if (isset($data['lokasi_tujuan'])) {
			if ($data['lokasi_tujuan'] != '') {
				if (
					$data['lokasi_tujuan'] != 'UPT, ULTG & GI PROBOLINGGO' ||
					$data['lokasi_tujuan'] != 'ULTG & GI BANGIL' ||
					$data['lokasi_tujuan'] != 'ULTG & GI JEMBER'
				) {
					$garduInduk = $this->getPegawaiGarduInduk($data);
					if($filterVerify != ''){
						if(!empty($garduInduk)){
							$filterTujuanLokasi = " AND pw.work_place = '3' and pw.place = '".$garduInduk['id']."'";
						}
					}else{
						if(!empty($garduInduk)){
							$filterTujuanLokasi = " AND pw.work_place = '3' and pw.place = '".$garduInduk['id']."'";
						}
					}
				}
			}
		}

		$sql = "select 
		d.*,
		pw.uraian_pekerjaan,
		pw.tgl_awal,
		pw.tgl_akhir,
		p.no_wp,
		cast(d.createddate as date) as createddate
		from permit p
		join (
			select max(id) as id, permit from permit_attandance group by permit
		) pa_max
			on pa_max.permit = p.id 
		join permit_attandance pa
			on pa.id = pa_max.id
		join document d
			on d.id=pa.document 
		left join (
				select max(id) as id, permit from permit_verifikasi group by permit
			) as pv_max
				on pv_max.permit = p.id 
		join permit_work as pw
			on pw.permit = p.id
		where d.doc_type = 'DOCT_ATT' and d.createdby ='" . $data['users'] . "'
		" . $filterVerify . "
		" . $filterTujuanLokasi . "
		order by d.createddate desc
		" . $filterLimit;

		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getPegawaiGarduInduk($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$sql = "select 
		gi.*,
		p.nip
		from gardu_induk gi
		join gardu_induk_has_pegawai as gip
			on gip.gardu_induk = gi.id
		join pegawai as p
			on p.id = gip.pegawai
		where gi.nama_gardu = '" . $data['lokasi_tujuan'] . "'
		and gi.deleted = 0
		order by gi.id asc
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDataOutstandingReadyVerifikasi($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$filterVerify = "and pv_max.id is null ";

		$filterplace = '';
		if (!empty($data['place'])) {
			$dataPlace = [];
			foreach ($data['place'] as $vG) {
				$dataPlace[] = $vG['gardu_induk'];
			}
			$dataPlace = implode(',', $dataPlace);
			$filterplace = " AND pw.place in (" . $dataPlace . ")";
		}

		if (!empty($data['place_sub_unit'])) {
			$dataPlace = [];
			foreach ($data['place_sub_unit'] as $vG) {
				$dataPlace[] = $vG['sub_unit'];
			}
			$dataPlace = implode(',', $dataPlace);
			$filterplace = " AND pw.place in (" . $dataPlace . ") and pw.work_place = 2";
		}

		if (!empty($data['place_admin_unit'])) {
			$dataPlace = [];
			foreach ($data['place_admin_unit'] as $vG) {
				$dataPlace[] = $vG['upt'];
			}
			$dataPlace = implode(',', $dataPlace);
			$filterplace = " AND pw.place in (" . $dataPlace . ") and pw.work_place = 1";
		}

		$sql = "select 
		d.*,
		p.no_wp,
		pw.uraian_pekerjaan,
		pw.tgl_awal,
		pw.tgl_akhir,
		p.tipe_permit,
		cast(d.createddate as date) as createddate
		from permit p
		join (
			select max(id) as id, permit from permit_attandance group by permit
		) pa_max
			on pa_max.permit = p.id 
		join permit_attandance pa
			on pa.id = pa_max.id
		join document d
			on d.id=pa.document 
		left join (
				select max(id) as id, permit from permit_verifikasi group by permit
			) as pv_max
				on pv_max.permit = p.id 
		join permit_work as pw
			on pw.permit = p.id
		where d.doc_type = 'DOCT_ATT'
		" . $filterVerify . "
		" . $filterplace . "
		" . $filterLimit;
		// echo '<pre>';
		// print_r($sql);die;

		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getListKehadiran($data)
	{
		$sql = "select 
		d.*,
		p.no_wp,
		pw.uraian_pekerjaan,
		pw.tgl_awal,
		pw.tgl_akhir,
		cast(d.createddate as date) as createddate,
		pac.createddate  as waktu_checkout,
		pav.nopol,
		pav.jenis_kendaraan
		from document d
		join (
			select max(id) as id, document from permit_attandance group by document
		) pa_max
			on pa_max.document = d.id 
		join permit_attandance pa
			on pa.id = pa_max.id
		join permit p
			on p.id=pa.permit 
		join permit_work as pw
			on pw.permit = p.id
		left join permit_attandance_checkout pac
			on pac.document = d.id
		left join permit_attandance_vehicle pav
			on pav.document = d.id
		where d.doc_type = 'DOCT_ATT' and p.no_wp ='" . $data['permit'] . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getDataVerifikasi($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$filterdate = '';
		if (isset($data['tgl'])) {
			$filterdate = " AND ('" . $data['tgl'] . "' >= pw.tgl_awal  AND  '" . $data['tgl'] . "' <= pw.tgl_akhir) ";
			if (isset($data['tgl_akhir'])) {
				$filterdate = " AND ( '" . $data['tgl'] . "' >= pw.tgl_awal AND  '" . $data['tgl_akhir'] . "' <= pw.tgl_akhir) ";
			}
		}

		$filterpermit = '';
		if (isset($data['tipe_permit'])) {
			$filterpermit = " AND p.tipe_permit = '" . $data['tipe_permit'] . "' ";
		}

		$filterplace = '';
		$dataPlaceWitGardu = [];
		if (!empty($data['place'])) {
			$dataPlace = [];
			foreach ($data['place'] as $vG) {
				$dataPlace[] = $vG['gardu_induk'];
				$dataPlaceWitGardu[] = $vG['gardu_induk'];
			}
			$dataPlace = implode(',', $dataPlace);
			$filterplace = " AND pw.place in (" . $dataPlace . ")";
		}

		if (!empty($data['place_sutet'])) {
			$dataPlace = [];
			foreach ($data['place_sutet'] as $vG) {
				$dataPlace[] = $vG['id'];
				$dataPlaceWitGardu[] = $vG['id'];
			}
			// echo '<pre>';
			// print_r($dataPlaceWitGardu);die;
			$dataPlace = implode(',', $dataPlaceWitGardu);
			$filterplace = " AND pw.place in (" . $dataPlace . ") and (pw.work_place = 4 or pw.work_place = 3)";
		}

		// echo $filterplace;die;

		if (!empty($data['place_sub_unit'])) {
			$dataPlace = [];
			foreach ($data['place_sub_unit'] as $vG) {
				$dataPlace[] = $vG['sub_unit'];
			}
			$dataPlace = implode(',', $dataPlace);
			$filterplace = " AND pw.place in (" . $dataPlace . ") and pw.work_place = 2";
		}

		if (!empty($data['place_admin_unit'])) {
			$dataPlace = [];
			foreach ($data['place_admin_unit'] as $vG) {
				$dataPlace[] = $vG['upt'];
			}
			$dataPlace = implode(',', $dataPlace);
			$filterplace = " AND pw.place in (" . $dataPlace . ") and pw.work_place = 1";
		}

		$sql = "select 
		d.*,
		p.no_wp,
		pw.uraian_pekerjaan,
		pw.tgl_awal,
		pw.tgl_akhir,
		p.tipe_permit,
		p.id as permit_id
		from document d 
		join (
			select max(id) as id, document from permit_verifikasi group by document
		) pa_max
			on pa_max.document = d.id 
		join permit_verifikasi pa
			on pa.id = pa_max.id
		join permit p 
			on p.id=pa.permit 
		join permit_work pw
			on pw.permit = p.id
		where d.doc_type = 'DOCT_VER' and d.createdby = '" . $data['users'] . "' 
		" . $filterdate . " " . $filterpermit . ' ' . $filterplace . '		 order by d.createddate desc 
		' . $filterLimit;

		// echo '<pre>';
		// print_r($sql);die;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getApd($data)
	{
		$sql = "select 
		pa.*,
		p.no_wp	
		from permit_apd as pa
		join permit p 
			on p.id=pa.permit 
		where  p.no_wp = '" . $data['no_wp'] . "'
		and pa.deleted = 0";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getDataVerifHal3($data)
	{
		$sql = "select 
		pa.*,
		p.no_wp	
		from permit_verifikasi as pa
		join permit p 
			on p.id=pa.permit 
		where  p.no_wp = '" . $data['no_wp'] . "' 
		and pa.hal = '3' 
		and pa.ttd_k3 is null 
		and pa.ttd_penanggung_jawab is null";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function isExistDataVerif($data)
	{
		$sql = "select 
		pa.*,
		p.no_wp	
		from permit_verifikasi as pa
		join permit p 
			on p.id=pa.permit 
		where  p.no_wp = '" . $data['no_wp'] . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getDetailPermit($data)
	{
		$sql = "select * from permit p
		where p.no_wp = '" . $data['no_wp'] . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDataPlaceSpv($data)
	{
		$sql = "select pgi.* from `user` usr
		join pegawai as p
			on p.id = usr.pegawai
		join gardu_induk_has_pegawai as pgi
			on pgi.pegawai = p.id
		where usr.id = '" . $data['users'] . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->result_array();
		}
		return $result;
	}

	public function getPenanggungJawab($data)
	{
		$result[] = array(
			'id' => $data['id'],
			'permit' => $data['id'],
			'nama' => $data['pengawas_k3'],
			'name' => $data['pengawas_k3'],
			'tipe' => 'k3',
			'jabatan' => $data['jabatan_pengawas_k3']
		);
		return $result;
	}

	public function getPelaksanaPekerjaan($data)
	{
		$result[] = array(
			'id' => $data['id'],
			'permit' => $data['id'],
			'nama' => $data['pengawas_pekerjaan'],
			'name' => $data['pengawas_pekerjaan'],
			'tipe' => 'pengawas',
			'jabatan' => $data['jabatan_pengawas_pekerjaan']
		);
		return $result;
	}

	public function getDetailWp($no_wp)
	{
		$sql = "select * from permit where no_wp = '" . $no_wp . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDetailUser($users)
	{
		$sql = "select * from `user` where id = " . $users;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDetailWorkPlaceWp($params)
	{
		$dateScan = date('Y-m-d');
		$sql = "select pw.* 
		, wp.jenis 
		from permit p
		join permit_work pw
			on pw.permit = p.id
		join work_place wp 
			on wp.id = pw.work_place 
		where p.no_wp = '" . $params['no_wp'] . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDetailSpvGi($params = array())
	{
		$sql = "
		select 
		gi.*
		, pg.nama as spv_gi
		, pg.posisi_lengkap 
		, pg.posisi 
		, pg.id as pegawai_id
		from 
		gardu_induk gi 
		join gardu_induk_has_pegawai gip
			on gip.gardu_induk = gi.id 
		join pegawai pg
			on pg.id = gip.pegawai 
		where gi.id = '" . trim($params['place']) . "'
		and gip.deleted = 0
		order by gip.id desc";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getDetailSpvGiInSutet($params = array())
	{
		$sql = "select 
		gihp.*
		, gihp.pegawai as pegawai_id
		from sutet s
		join gardu_induk gi 
			on gi.id = s.gardu_induk 
		join gardu_induk_has_pegawai gihp 
			on gihp.gardu_induk = gi.id
		where gihp.deleted = 0
		and s.id = '" . trim($params['place']) . "'
		order by gihp.id DESC ";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getDetailUlg($params = array())
	{
		$sql = "select pghsu.* 
		, pg.id as pegawai_id 
		from pegawai_has_sub_upt pghsu 
		join pegawai pg 
			on pg.id = pghsu.pegawai 
		join `user` as u
			on u.pegawai = pg.id
		where pghsu.sub_unit = " . $params['place'] . "
		";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getDetailSpvUnit($params = array())
	{
		$sql = "select sa.* 
		, u.pegawai as pegawai_id 
		from struktur_approval sa 
		join `user` u 
			on u.id = sa.`user` 
		where sa.upt = " . $params['place'] . "
		and sa.`level` = 1
		";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDetailSpvUltgUnitData($params = array())
	{
		$sql = "select pghsu.* 
		, pg.id as pegawai_id 
		from pegawai_has_sub_upt pghsu 
		join pegawai pg 
			on pg.id = pghsu.pegawai 
		join `user` as u
			on u.pegawai = pg.id
		where u.id = " . $params['users'] . "
		";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getDetailSpvUnitData($params = array())
	{
		$sql = "select sa.* 
		, u.pegawai as pegawai_id 
		from struktur_approval sa 
		join `user` u 
			on u.id = sa.`user` 
		where u.id = " . $params['users'] . "
		and sa.`level` = 1
		";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getDetailSpvSutetData($params = array())
	{
		$sql = "select 
		s.*
		from sutet s
		join gardu_induk gi 
			on gi.id = s.gardu_induk 
		join gardu_induk_has_pegawai gihp 
			on gihp.gardu_induk = gi.id
		join `user` u 
			on u.pegawai = gihp.pegawai 
		where gihp.deleted = 0
		and u.id = " . $params['users'] . "
		";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function verifyWpPekerjaan($data, $limit = '100')
	{
		$result['is_valid'] = false;
		$dataUser = $this->getDetailUser($data['users']);
		$pegawaiId = '';
		if (!empty($dataUser)) {
			$pegawaiId = $dataUser['pegawai'];
		}
		$dataWpWorkPlace = $this->getDetailWorkPlaceWp($data);
		if (!empty($dataWpWorkPlace)) {
			if (trim($dataWpWorkPlace['jenis']) == 'UNIT') {
				$dataSpvUnit = $this->getDetailSpvUnit($dataWpWorkPlace);
				if (!empty($dataSpvUnit)) {
					if ($dataSpvUnit['pegawai_id'] == $pegawaiId) {
						$result['is_valid'] = true;
					}
				}
			}
			if (trim($dataWpWorkPlace['jenis']) == 'GARDU INDUK') {
				$dataSpvGi = $this->getDetailSpvGi($dataWpWorkPlace);
				if (!empty($dataSpvGi)) {
					foreach ($dataSpvGi as $key => $value) {
						if ($value['pegawai_id'] == $pegawaiId) {
							$result['is_valid'] = true;
							break;
						}
					}
				}
			}
			if (trim($dataWpWorkPlace['jenis']) == 'SUTET / SUTT') {
				$dataSpvGiSutet = $this->getDetailSpvGiInSutet($dataWpWorkPlace);
				if (!empty($dataSpvGiSutet)) {
					foreach ($dataSpvGiSutet as $key => $value) {
						if ($value['pegawai_id'] == $pegawaiId) {
							$result['is_valid'] = true;
							break;
						}
					}
				}
			}
			if (trim($dataWpWorkPlace['jenis']) == 'SUB UNIT') {
				$dataUltgUser = $this->getDetailUlg($dataWpWorkPlace);
				if (!empty($dataUltgUser)) {
					foreach ($dataUltgUser as $key => $value) {
						if ($value['pegawai_id'] == $pegawaiId) {
							$result['is_valid'] = true;
							break;
						}
					}
				}
			}
		}
		return $result;
	}

	public function getDetailWorkPlaceWpWithDate($params)
	{
		$dateScan = date('Y-m-d');
		$sql = "select pw.* 
		, wp.jenis 
		from permit p
		join permit_work pw
			on pw.permit = p.id
		join work_place wp 
			on wp.id = pw.work_place 
		where p.no_wp = '" . $params['no_wp'] . "'
		and ('".$dateScan."' >= pw.tgl_awal and '".$dateScan."' <= pw.tgl_akhir)"; //kusus satpam
		// echo $sql;die;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getDataValidasiPekerjaan($data)
	{
		$dataWpWorkPlace = $this->getDetailWorkPlaceWpWithDate($data);
		$result['is_valid'] = true;
		if (empty($dataWpWorkPlace)) {
			$result['is_valid'] = false;
		}
		return $result;
	}
}
