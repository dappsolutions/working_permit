
<?php

class M_tamu extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDetailWp($no_wp)
	{
		$sql = "select * from permit where no_wp = '" . $no_wp . "'";
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getData($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$filterBertemu = "";
		if (isset($data['akses'])) {			
			if ($data['akses'] != 'satpam') {
				if ($data['akses'] != '-') {
					$filterBertemu = "where t.bertemu = '" . $data['nip'] . "'";
				}else{
					// echo 'asdasd';die;
					if (isset($data['email_tamu'])) {
						// if ($data['email_tamu'] != '') {
							$filterBertemu = "where t.email = '".$data['email_tamu']."'";
						// }
					}
				}
			}else{				
				if (isset($data['email_tamu'])) {
					// if ($data['email_tamu'] != '') {
						$filterBertemu = "where t.email = '".$data['email_tamu']."'";
					// }
				}
			}
		}

		$filterTgl = '';
		if (isset($data['tgl'])) {
			if ($filterBertemu != '') {
				$filterTgl = " AND t.tanggal_bertemu = '" . $data['tgl'] . "'";
			} else {
				$filterTgl = "WHERE t.tanggal_bertemu = '" . $data['tgl'] . "'";
			}
		}

		$filterLokasi = '';
		if (isset($data['lokasi_tujuan'])) {
			if ($filterBertemu != '' || $filterTgl != '') {
				if ($data['lokasi_tujuan' != '']) {
					if (
						$data['lokasi_tujuan'] == 'UPT, ULTG & GI PROBOLINGGO' ||
						$data['lokasi_tujuan'] == 'ULTG & GI BANGIL' ||
						$data['lokasi_tujuan'] == 'ULTG & GI JEMBER'
					) {
						$filterLokasi = " AND t.lokasi_tujuan = '" . $data['lokasi_tujuan'] . "'";
					} else {
						$garduIndukPegawai = $this->getPegawaiGarduInduk($data);
						if (!empty($garduIndukPegawai)) {
							$filterBertemu = "AND t.bertemu = '" . $garduIndukPegawai['nip'] . "'";
						}
					}
				}
			} else {
				if ($data['lokasi_tujuan'] != '') {
					if (
						$data['lokasi_tujuan'] == 'UPT, ULTG & GI PROBOLINGGO' ||
						$data['lokasi_tujuan'] == 'ULTG & GI BANGIL' ||
						$data['lokasi_tujuan'] == 'ULTG & GI JEMBER'
					) {
						$filterLokasi = "WHERE t.lokasi_tujuan = '" . $data['lokasi_tujuan'] . "'";
					} else {
						$garduIndukPegawai = $this->getPegawaiGarduInduk($data);
						if (!empty($garduIndukPegawai)) {
							$filterBertemu = "AND t.bertemu = '" . $garduIndukPegawai['nip'] . "'";
						}
					}
				}
			}
		}

		$sql = "select 
		distinct
		t.*
		, p.posisi
		from tamu as t		
		join pegawai p
			on p.nip = t.bertemu
		" . $filterBertemu . "
		" . $filterTgl . "
		" . $filterLokasi . "
		order by t.id desc
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getPegawaiGarduInduk($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$sql = "select 
		gi.*,
		p.nip
		from gardu_induk gi
		join gardu_induk_has_pegawai as gip
			on gip.gardu_induk = gi.id
		join pegawai as p
			on p.id = gip.pegawai
		where gi.nama_gardu = '" . $data['lokasi_tujuan'] . "'
		and gi.deleted = 0
		order by gi.id asc
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
		}
		return $result;
	}

	public function getListTamu($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$sql = "select 
		t.*
		from detail_tamu as t		
		where t.tamu = '" . $data['id'] . "'
		order by t.id asc
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getDataDetail($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}


		$sql = "select 
		t.*
		from tamu as t
		where t.id = '" . $data['id'] . "'
		order by t.id desc
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			$result = $datadb->row_array();
			$result['url_foto'] = '';
			if ($result['foto'] != '') {
				$result['url_foto'] = base_url() . $result['foto'];
			}
		}
		return $result;
	}
	
	public function getDataFotoKendaraan($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}


		$sql = "select 
		t.*
		from tamu_deviden_pict as t
		where t.tamu = '" . $data['id'] . "'
		order by t.id desc
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$value['url_foto'] = base_url() . $value['foto'];
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getDataPegawai($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$filterTujuan = '';
		$joinSpvGi = '';
		$fieldAdd = '';
		if (isset($data['lokasi'])) {
			$filterTujuan = " AND pt.context = '" . $data['lokasi'] . "'";
			if ($data['lokasi'] == 'GI') {
				$filterTujuan = ' AND gi.id not in (113, 116, 109)';
				$joinSpvGi = 'join gardu_induk_has_pegawai gihp
					on gihp.pegawai = p.id
					join gardu_induk as gi
						on gi.id = gihp.gardu_induk';
				$fieldAdd = ', gi.nama_gardu';
			}
		}

		$sql = "select
		distinct 
		p.nip 
		, p.nama 
		, us.nama as nama_upt
		, p.posisi 
		" . $fieldAdd . "
		from pegawai p 
		join `user` u 
			on u.pegawai = p.id 
		join upt us
			on us.id = p.upt 
		join pegawai_type pt
			on pt.id = p.pegawai_type
		" . $joinSpvGi . "
		where p.upt = 5 and p.nip is not null 
		" . $filterTujuan . "
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				if (isset($value['nama_gardu'])) {
					$value['posisi'] = strtoupper($value['nama_gardu']);
				}
				$value['approval'] = $value['nip'] . strtoupper(' - ' . $value['posisi']);
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getDataLokasiTujuan($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$sql = "select * from pegawai_type pt where parent_id = 'TU_TAMU' 
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		$temp = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				if (!in_array($value['context'], $temp)) {
					$result[] = $value;
					$temp[] = $value['context'];
				}
			}
		}

		$garduInduk = $this->getDataLokasiGISatpam($data);
		foreach ($garduInduk as $key => $value) {
			$result[] = $value;
		}
		return $result;
	}

	public function getDataLokasiGISatpam($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$sql = "select 
		id,
		nama_gardu as tipe,
		nama_gardu as context,
		'gardu' as term_id,
		'gardu' as context_id,
		'TU_TAMU' as parent_id,
		nama_gardu
		from gardu_induk where upt = '5' 
		" . $filterLimit;
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$value['context'] = $value['nama_gardu'];
				$result[] = $value;
			}
		}
		return $result;
	}

	public function getListTamuForStatusAccNotification($data, $limit = '100')
	{
		$filterLimit = '';
		if ($limit != '') {
			$filterLimit = 'limit ' . $limit;
		}

		$filterStatus = '';
		if (!isset($data['acc_nik'])) {
			if (isset($data['status'])) {
				if ($data['status'] == 'APPROVED') {
					$filterStatus = "AND t.reason_reject is null AND t.approve_date is not null";
				}
				if ($data['status'] == 'REJECTED') {
					$filterStatus = "AND t.reason_reject is not null AND t.approve_date is not null";
				}
			}
		}

		if (isset($data['acc_nik'])) {
			$sql = "select 
			t.*
			, n.id as notif_id
			from tamu as t		
			left join notifikasi n
				on n.transaksi_id = t.id
				and n.kategori = 'TAMU'
				and n.nik = '" . $data['acc_nik'] . "'
			where t.approve_user = '" . $data['acc_nik'] . "'
			and n.read_status is null
			" . $filterStatus . "
			order by t.id asc
			" . $filterLimit;
		} else {
			$sql = "select 
			t.*
			, n.id as notif_id
			from tamu as t		
			left join notifikasi n
				on n.transaksi_id = t.id
				and n.kategori = 'TAMU'
				and n.nik = '" . $data['email'] . "'
			where t.email = '" . $data['email'] . "'
			and n.read_status is null
			" . $filterStatus . "
			order by t.id asc
			" . $filterLimit;
		}
		$datadb = $this->db->query($sql);
		$result = [];
		if (!empty($datadb->result_array())) {
			foreach ($datadb->result_array() as $key => $value) {
				$push = [];
				$push['transaksi_id'] = $value['id'];
				$push['nik'] =  isset($data['acc_nik']) ? $data['acc_nik'] : $data['email'];
				$push['tanggal_read'] = date('Y-m-d');
				$push['read_status'] = 'READ';
				$push['status_transaksi'] = 'CREATED';
				if (isset($data['status'])) {
					if ($data['status'] == 'APPROVED') {
						$push['status_transaksi'] = 'APPROVED';
						$push['remarks'] = 'Pengajuan Tamu No : ' . $value['id'] . ' Telah Disetujui';
					}
					if ($data['status'] == 'REJECTED') {
						$push['remarks'] = 'Pengajuan Tamu No : ' . $value['id'] . ' Telah Ditolak';
						$push['status_transaksi'] = 'REJECTED';
					}
				}
				$push['kategori'] = 'TAMU';
				$push['createddate'] = date('Y-m-d H:i:s');
				$this->db->insert('notifikasi', $push);
				$result[] = $value;
			}
		}
		return $result;
	}
}
