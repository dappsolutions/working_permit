<?php

class Verifikasi extends MX_Controller
{
 public function __construct()
 {
  parent::__construct();
  date_default_timezone_set('Asia/Jakarta');
  $this->load->model('m_permit', 'permit');
 }

 public function index()
 {
  echo 'verifikasi ' . date('Y-m-d H:i:s');
 }

 public function getData()
 {
  $data = $_POST;
//   $data['users'] = '1342';
  $dataPlaceUser = $this->permit->getDataPlaceSpv($data);
	$dataSpvSubUnit = $this->permit->getDetailSpvUltgUnitData($data);
	$dataSpvAdminUnit = $this->permit->getDetailSpvUnitData($data);
	$dataSpvSutet = $this->permit->getDetailSpvSutetData($data);
//   echo '<pre>';
//   print_r($dataPlaceUser);die;
  $data['place'] = !empty($dataPlaceUser) ? $dataPlaceUser : [];
  $data['place_sub_unit'] = !empty($dataSpvSubUnit) ? $dataSpvSubUnit : [];
  $data['place_admin_unit'] = !empty($dataSpvAdminUnit) ? $dataSpvAdminUnit : [];
  $data['place_sutet'] = !empty($dataSpvSutet) ? $dataSpvSutet : [];
  $datadb = $this->permit->getDataVerifikasi($data);
  $result['is_valid'] = 0;
  $result['data'] = array();
  $result['message'] = "Tidak ada data ditemukan";
  if (!empty($datadb)) {
   $result['is_valid'] = 1;
   $result['data'] = $datadb;
   $result['message'] = "Sukses";
  }
  echo json_encode($result);
 }

 public function submit()
 {
  $data = $_POST;
  $data_verifikasi = json_decode($data['data']);
  $data_kondisi_apd = json_decode($data['data_kondisi_apd']);
  $data_checklist = json_decode($data['data_checklist']);
  // echo '<pre>';
  // print_r($data_checklist);die;
  $datawp = $this->permit->getDetailWp($data['no_wp']);
  $result['is_valid'] = false;
  $noDocument = Modules::run('no_generator/generateNoDocument');

  $dataVerif = $this->permit->isExistDataVerif($data);

  if(!empty($dataVerif)){
   $result['message'] = 'Anda Sudah Melakukan Verifikasi pada No WP : '.$data['no_wp'].' Sebelumnya';
  }else{
   $this->db->trans_begin();
   try {
    $push = [];
    $push['no_document'] = $noDocument;
    $push['createddate'] = date('Y-m-d H:i:s');
    $push['createdby'] = $data['users'];
    $push['deleted'] = 0;
    $push['doc_type'] = 'DOCT_VER';
    if (isset($data['keterangan_reject'])) {
      $push['verbatim'] = $data['keterangan_reject'];
    }
    $this->db->insert('document', $push);
    $doc_id = $this->db->insert_id();
 
    if (!empty($data_verifikasi)) {
     foreach ($data_verifikasi as $key => $value) {
      $push = [];
      $push['permit'] = $datawp['id'];
      $push['hal'] = $value->hal;
      $push['document'] = $doc_id;
      if (trim($value->ttd_k3) != '') {
       $this->upload($value->ttd_k3, 'ttd_k3_' . $noDocument . '_' . $value->hal . '.png');
       $push['ttd_k3'] = 'ttd_k3_' . $noDocument . '_' . $value->hal . '.png';
      }
      if (trim($value->ttd_penanggung_jawab) != '') {
       $this->upload($value->ttd_penanggung_jawab, 'ttd_pengawas_' . $noDocument . '_' . $value->hal . '.png');
       $push['ttd_penanggung_jawab'] = 'ttd_pengawas_' . $noDocument . '_' . $value->hal . '.png';
      }
      $push['createddate'] = date('Y-m-d H:i:s');
      /*KONDISI APD */
      if($value->hal == '3'){
       $push['kondisi_apd'] = $data['data_kondisi_apd'];
       $push['checklist_aktifitas'] = $data['data_checklist'];
      }
      /*KONDISI APD */
      $this->db->insert('permit_verifikasi', $push);
 
      /*CEK DUPLKASI HALAMAN 3 */
      $dataHal3 = $this->permit->getDataVerifHal3($data);
      if(!empty($dataHal3)){
       foreach ($dataHal3 as $key => $value) {
        $this->db->delete('permit_verifikasi', array('id'=> $value['id']));
       }
      }
      /*CEK DUPLKASI HALAMAN 3 */
     }
 
     if(!empty($data_kondisi_apd)){
      foreach ($data_kondisi_apd as $key => $value) {
       $push = [];
       $push['kondisi_baik'] = $value->kondisi_baik;
       $push['kondisi_rusak'] = $value->kondisi_rusak;
       $this->db->update('permit_apd', $push, array('id'=> $value->id));
      }
     }
    }
    $this->db->trans_commit();
 
    $data['datawp'] = $datawp;
    $this->sendEmailToAll($data);
    $result['is_valid'] = true;
   } catch (\Throwable $th) {
    $result['message'] = $th->getMessage();
    $this->db->trans_rollback();
   }
  }

  echo json_encode($result);
 }

 public function sendEmailToAll($params)
 {
  $dataUser = Modules::run('database/get', array(
   'table' => 'user',
   'where' => array('id' => $params['users'])
  ));
  if (!empty($dataUser)) {
   $dataUser = $dataUser->row_array();
   $params['subject'] = 'Verifikasi WP ' . $params['no_wp'];
   $params['no_wp'] = $params['no_wp'];
   $params['id_wp'] = $params['datawp']['id'];
   $params['message'] = '<p>Berikut kami sampaikan WP No ' . $params['no_wp'] . ' Sudah Dilakukan Verifikasi , Silakan Cek Di Cetakan Dashboard Anda pada No Wp ' . $params['no_wp'] . '</p>
   <p>Semoga PLN menjadi yang terbaik</p>
   <p>Amienn...</p>';

   Modules::run('email/send_email_data_other', $params['subject'], $params['message'], trim($dataUser['username']));

   /*KIRIM KE USER ADMIN WP */
   $dataPermitStatus =  Modules::run('database/get', array(
    'table' => 'permit_status ps',
    'field'=> array(
     'ps.*',
     'u.username'
    ),
    'join'=> array(
     array('user u', 'u.id = ps.user')
    ),
    'where' => array('ps.permit' => $params['datawp']['id'], 'ps.level'=>'1'),
    'orderby'=> 'ps.id asc'
   ));

   if(!empty($dataPermitStatus)){
    $dataPermitStatus = $dataPermitStatus->row_array();
    Modules::run('email/send_email_data_other', $params['subject'], $params['message'], trim($dataPermitStatus['username']));
   }

   /*KIRIM KE USER ADMIN WP */
  }
 }

 public function upload($foto, $name_file)
 {
  if ($this->is_base64_encoded($foto)) {
   $image_base64 = base64_decode($foto);
   $file = $name_file;
   $path = 'files/berkas/ttd/';
   if (!file_exists('../' . $path)) {
    mkdir('../' . $path, 0755, true);
   }
   try {
    //code...
    file_put_contents($path . $file, $image_base64);
   } catch (\Throwable $th) {
    //throw $th;
    echo $th->getMessage();
    die;
   }
  }
 }

 public function is_base64_encoded($data)
 {
  if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
   return TRUE;
  } else {
   return FALSE;
  }
 }
}
