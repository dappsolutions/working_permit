<?php

class Permit extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('Asia/Jakarta');
    $this->load->model('m_permit', 'permit');
  }

  public function index()
  {
    echo 'permit ' . date('Y-m-d H:i:s');
  }

  public function getDataPekerjaDanPelaksana()
  {
    $data = $_POST;
    // $data['no_wp'] = 'WPEKS20JAN003';
    $datadb = $this->permit->getDataPekerjaDanPelaksana($data);
    $result['is_valid'] = 0;
    $result['data'] = array();
    $result['message'] = "Tidak ada data ditemukan";
    if (!empty($datadb)) {
      $result['is_valid'] = 1;
      $result['data'] = $datadb;
      $result['message'] = "Sukses";
    }
    echo json_encode($result);
  }

  public function getDataPekerjaDanPelaksanaDetailAtt()
  {
    $data = $_POST;
    $datadb = $this->permit->getDataPekerjaDanPelaksanaDetailAtt($data);
    $result['is_valid'] = 0;
    $result['data'] = array();
    $result['message'] = "Tidak ada data ditemukan";
    if (!empty($datadb)) {
      $result['is_valid'] = 1;
      $result['data'] = $datadb;
      $result['message'] = "Sukses";
    }
    echo json_encode($result);
  }

  public function getApd()
  {
    $data = $_POST;
    $datadb = $this->permit->getApd($data);
    $result['is_valid'] = 0;
    $result['data'] = array();
    $result['message'] = "Tidak ada data ditemukan";
    if (!empty($datadb)) {
      $result['is_valid'] = 1;
      $result['data'] = $datadb;
      $result['message'] = "Sukses";
    }
    echo json_encode($result);
  }

  public function getData()
  {
    $data = $_POST;
    // $data['users'] = 1336;
    // $data['filter_verifi'] = '-';
    $datadb = $this->permit->getData($data);
    $result['is_valid'] = 0;
    $result['data'] = array();
    $result['message'] = "Tidak ada data ditemukan";
    if (!empty($datadb)) {
      $result['is_valid'] = 1;
      $result['data'] = $datadb;
      $result['message'] = "Sukses";
    }
    echo json_encode($result);
  }

  public function getDataOutstandingReadyVerifikasi()
  {
    $data = $_POST;
    // $data['users'] = 1336;
    // $data['filter_verifi'] = '-';
    $dataPlaceUser = $this->permit->getDataPlaceSpv($data);
    $dataSpvSubUnit = $this->permit->getDetailSpvUltgUnitData($data);
    $dataSpvAdminUnit = $this->permit->getDetailSpvUnitData($data);

    $data['place'] = !empty($dataPlaceUser) ? $dataPlaceUser : [];
    $data['place_sub_unit'] = !empty($dataSpvSubUnit) ? $dataSpvSubUnit : [];
    $data['place_admin_unit'] = !empty($dataSpvAdminUnit) ? $dataSpvAdminUnit : [];
    $datadb = $this->permit->getDataOutstandingReadyVerifikasi($data);
    $result['is_valid'] = 0;
    $result['data'] = array();
    $result['message'] = "Tidak ada data ditemukan";
    if (!empty($datadb)) {
      $result['is_valid'] = 1;
      $result['data'] = $datadb;
      $result['message'] = "Sukses";
    }
    echo json_encode($result);
  }

  public function getListKehadiran()
  {
    $data = $_POST;
    $datadb = $this->permit->getListKehadiran($data);
    $result['is_valid'] = 0;
    $result['data'] = array();
    $result['message'] = "Tidak ada data ditemukan";
    if (!empty($datadb)) {
      $result['is_valid'] = 1;
      $result['data'] = $datadb;
      $result['message'] = "Sukses";
    }
    echo json_encode($result);
  }

  public function submitKehadiran()
  {
    $data = $_POST;
    // $data['no_wp'] = 'WPEKS20JAN003';
    $data['data_peserta'] = json_decode($data['data_peserta']);
    $data['data_k3'] = json_decode($data['data_k3']);
    $data['data_pengawas'] = json_decode($data['data_pengawas']);

    $result['is_valid'] = 0;
    $result['message'] = "";
    $dataPermit = $this->permit->getDetailPermit($data);
    $noDocument = Modules::run('no_generator/generateNoDocument');
    $this->db->trans_begin();
    try {
      //code...
      $push = [];
      $push['no_document'] = $noDocument;
      $push['createddate'] = date('Y-m-d H:i:s');
      $push['createdby'] = $data['users'];
      $push['deleted'] = 0;
      $push['doc_type'] = 'DOCT_ATT';
      if (isset($data['keterangan_reject'])) {
        $push['verbatim'] = $data['keterangan_reject'];
      }
      $this->db->insert('document', $push);
      $doc_id = $this->db->insert_id();

      if (!empty($data['data_peserta'])) {
        foreach ($data['data_peserta'] as $key => $value) {
          $push = [];
          $push['document'] = $doc_id;
          $push['permit'] = $dataPermit['id'];
          $push['nama_pekerja'] = $value->name;
          $push['hadir'] = $value->is_masuk == '0' ? '0' : $value->is_masuk;
          $push['tipe'] = $value->tipe;
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $data['users'];
          $this->db->insert('permit_attandance', $push);
        }
      }

      if (!empty($data['data_k3'])) {
        foreach ($data['data_k3'] as $key => $value) {
          $push = [];
          $push['document'] = $doc_id;
          $push['permit'] = $dataPermit['id'];
          $push['nama_pekerja'] = $value->name;
          $push['hadir'] = $value->is_masuk == '0' ? '0' : $value->is_masuk;
          $push['tipe'] = $value->tipe;
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $data['users'];
          $this->db->insert('permit_attandance', $push);
        }
      }

      if (!empty($data['data_pengawas'])) {
        foreach ($data['data_pengawas'] as $key => $value) {
          $push = [];
          $push['document'] = $doc_id;
          $push['permit'] = $dataPermit['id'];
          $push['nama_pekerja'] = $value->name;
          $push['hadir'] = $value->is_masuk == '0' ? '0' : $value->is_masuk;
          $push['tipe'] = $value->tipe;
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $data['users'];
          $this->db->insert('permit_attandance', $push);
        }
      }

      if (isset($data['nopol'])) {
        $push = [];
        $push['document'] = $doc_id;
        $push['permit'] = $dataPermit['id'];
        $push['nopol'] = $data['nopol'];
        $push['jenis_kendaraan'] = $data['jenis_kendaraan'];
        $push['createddate'] = date('Y-m-d H:i:s');
        $push['createdby'] = $data['users'];      
        $push['tgl_absen'] = date('Y-m-d');      
        $this->db->insert('permit_attandance_vehicle', $push);
      }

      $this->db->trans_commit();
      $result['is_valid'] = 1;
    } catch (\Throwable $th) {
      //throw $th;
      $this->db->trans_rollback();
      $result['message'] = $th->getMessage();
    }
    echo json_encode($result);
  }

  public function verifyWpPekerjaan()
  {
    $data = $_POST;
    // $data['users'] = 1336;
    // $data['filter_verifi'] = '-';
    $result = $this->permit->verifyWpPekerjaan($data);
    if ($result['is_valid'] == false) {
      $result['message'] = "Pekerjaan Tidak Valid";
    }
    echo json_encode($result);
  }

  public function getDataValidasiPekerjaan()
  {
    $data = $_POST;
    /*GI  */
    // $data['users'] = 1345;
    // $data['no_wp'] = 'WPEKS23APR003';
    /*GI  */
    /*SUB UNIT */
    // $data['users'] = 1426;
    // $data['no_wp'] = 'WPEKS23APR583';
    /*SUB UNIT */
    /*UNIT */
    //   $data['users'] = 1459;
    // $data['no_wp'] = 'WPINT23JUN088';
    /*UNIT */
    $result = $this->permit->getDataValidasiPekerjaan($data);
    if ($result['is_valid'] == false) {
      $result['message'] = "Pekerjaan Sudah Kadaluwarsa";
    }
    echo json_encode($result);
  }

  public function checkout()
  {
    $data = $_POST;
    // $data['no_wp'] = 'WPEKS20JAN003';
    $result['is_valid'] = false;
    $result['message'] = "";
    $dataPermit = $this->permit->getDetailPermit($data);
    $noDocument = Modules::run('no_generator/generateNoDocument');
    $this->db->trans_begin();
    try {
      $push = [];
      $push['document'] = $data['document'];
      $push['permit'] = $dataPermit['id'];
      $push['tgl_absen'] = $data['tgl_absen'];
      $push['createddate'] = date('Y-m-d H:i:s');
      $push['createdby'] = $data['users'];
      $this->db->insert('permit_attandance_checkout', $push);

      $this->db->trans_commit();
      $result['is_valid'] = true;
    } catch (\Throwable $th) {
      //throw $th;
      $this->db->trans_rollback();
      $result['message'] = $th->getMessage();
    }
    echo json_encode($result);
  }
}
