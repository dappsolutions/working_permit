<?php

class Tamu extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_tamu', 'tamu');
	}

	public function index()
	{
		echo 'tamu ' . date('Y-m-d H:i:s');
	}

	public function submit()
	{
		$data = $_POST;

		$result['is_valid'] = false;
		$this->db->trans_begin();
		try {
			//code...
			$fileName = '';
			$pathName = '';
			if (isset($data['foto'])) {
				$resultImageTamu = $this->uploadImage($data['foto']);
				if ($resultImageTamu['is_valid'] == true) {
					$fileName = $resultImageTamu['filename'];
					$pathName = $resultImageTamu['path'];
				}
			}
			list($nip, $posisi) = explode('-', $data['bertemu']);
			$push = [];
			$push['nama'] = strtoupper($data['nama']);
			$push['alamat'] = strtoupper($data['alamat']);
			$push['lokasi_tujuan'] = strtoupper($data['lokasi_tujuan']);
			$push['bertemu'] = trim($nip);
			$push['keperluan'] = strtoupper($data['keperluan']);
			$push['tanggal_bertemu'] = $data['tanggal_bertemu'];
			$push['createddate'] = date('Y-m-d H:i:s');
			$push['createdby'] = $data['users'];
			$push['approve_user'] = $nip;
			$push['foto'] = $fileName;
			$push['path'] = $pathName;
			if (isset($data['jumlah_tamu'])) {
				$push['jumlah_tamu'] = $data['jumlah_tamu'];
			}
			if (isset($data['email'])) {
				$push['email'] = $data['email'];
			}
			if (isset($data['bertemu_dengan'])) {
				$push['bertemu_dengan'] = $data['bertemu_dengan'];
			}
			$this->db->insert('tamu', $push);
			$doc_id = $this->db->insert_id();
			if (isset($data['data_jumlah_tamu'])) {
				$data_jumlah_tamu = json_decode($data['data_jumlah_tamu']);
				if (!empty($data_jumlah_tamu)) {
					foreach ($data_jumlah_tamu as $key => $value) {
						$push = [];
						$push['nama'] = strtoupper($value->nama);
						$push['remarks'] = '';
						$push['tamu'] = $doc_id;
						$push['createddate'] = date('Y-m-d H:i:s');
						$push['createdby'] = $data['users'];
						$this->db->insert('detail_tamu', $push);
					}
				}
			}
			$result['id'] = $doc_id;

			$this->db->trans_commit();
			$result['is_valid'] = true;
		} catch (\Throwable $th) {
			//throw $th;
			$this->db->trans_rollback();
			$result['message'] = $th->getMessage();
		}
		echo json_encode($result);
	}

	public function submitVerifikasi()
	{
		$data = $_POST;
		$id = $data['id'];
		$data_foto = $data['foto'];
		$data_foto = explode(',,,,', $data_foto);

		$result['is_valid'] = false;
		$this->db->trans_begin();
		try {
			//code...
			$fileName = '';
			$pathName = '';
			foreach ($data_foto as $key => $value) {
				$resultImageTamu = $this->uploadImage($value);
				if ($resultImageTamu['is_valid'] == true) {
					$fileName = $resultImageTamu['filename'];
					$pathName = $resultImageTamu['path'];
				}
				$push = [];
				$push['tamu'] = $id;
				$push['foto'] = $fileName;
				$push['path'] = $pathName;
				$push['createddate'] = date('Y-m-d H:i:s');
				$push['createdby'] = $data['users'];
				$this->db->insert('tamu_deviden_pict', $push);
			}

			$push = [];
			$push['nopol'] = strtoupper($data['nopol']);
			$push['updateddate'] = date('Y-m-d H:i:s');
			$push['updatedby'] = $data['users'];
			if (isset($data['jenis_kendaraan'])) {
				$push['jenis_kendaraan'] = strtoupper($data['jenis_kendaraan']);
			}
			$this->db->update('tamu', $push, ['id' => $id]);
			$result['id'] = $id;

			$this->db->trans_commit();
			$result['is_valid'] = true;
		} catch (\Throwable $th) {
			//throw $th;
			$this->db->trans_rollback();
			$result['message'] = $th->getMessage();
		}
		echo json_encode($result);
	}

	public function uploadImage($base64string)
	{
		$result['is_valid'] = false;
		try {
			$uploadpath   = 'files/berkas/guest/';
			$imagebase64  = base64_decode($base64string);
			$fileName = uniqid() . '.png';
			$file         = $uploadpath . $fileName;
			file_put_contents($file, $imagebase64);
			$result['is_valid'] = true;
			$result['filename'] = $file;
			$result['path'] = $uploadpath;
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
		}
		return $result;
	}

	public function approve()
	{
		$data = $_POST;

		$result['is_valid'] = false;
		$this->db->trans_begin();
		try {
			//code...
			$push = [];
			$push['tanggal_bertemu'] = $data['tanggal_bertemu'];
			$push['jam_bertemu'] = $data['jam_bertemu'];
			$push['approve_date'] = date('Y-m-d H:i:s');
			if ($data['reason_reject'] != '') {
				$push['reason_reject'] = strtoupper($data['reason_reject']);
			}
			$this->db->update('tamu', $push, [
				'id' => $data['id']
			]);

			$this->db->trans_commit();
			$result['is_valid'] = true;
		} catch (\Throwable $th) {
			//throw $th;
			$this->db->trans_rollback();
			$result['message'] = $th->getMessage();
		}
		echo json_encode($result);
	}
	
	public function checkout()
	{
		$data = $_POST;

		$result['is_valid'] = false;
		$this->db->trans_begin();
		try {
			//code...
			$push = [];
			$push['checkout'] = date('Y-m-d H:i:s');
			$this->db->update('tamu', $push, [
				'id' => $data['id']
			]);

			$this->db->trans_commit();
			$result['is_valid'] = true;
		} catch (\Throwable $th) {
			//throw $th;
			$this->db->trans_rollback();
			$result['message'] = $th->getMessage();
		}
		echo json_encode($result);
	}

	public function getData()
	{
		$data = $_POST;
		// $data['users'] = 977;
		$datadb = $this->tamu->getData($data);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}

	public function getListTamu()
	{
		$data = $_POST;
		$datadb = $this->tamu->getListTamu($data);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}
	
	public function getListFotoKendaraan()
	{
		$data = $_POST;
		$datadb = $this->tamu->getDataFotoKendaraan($data);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}

	public function getDataDetail()
	{
		$data = $_POST;
		// $data['users'] = 977;
		$datadb = $this->tamu->getDataDetail($data);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}

	public function getDataPegawai()
	{
		$params = $_POST;
		$datadb = $this->tamu->getDataPegawai($params);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}

	public function getDataLokasiTujuan()
	{
		$params = $_POST;
		$datadb = $this->tamu->getDataLokasiTujuan($params);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}

	public function submitAddTamu()
	{
		$data = $_POST;
		$data_jumlah_tamu = json_decode($data['data_jumlah_tamu']);

		$result['is_valid'] = false;
		$this->db->trans_begin();
		try {

			$push = [];
			$push['jumlah_tamu'] = count($data_jumlah_tamu);
			Modules::run('database/_update', 'tamu', $push, array('id' => $data['id']));

			if (!empty($data_jumlah_tamu)) {
				Modules::run('database/_delete', 'detail_tamu', array('tamu' => $data['id']));
				foreach ($data_jumlah_tamu as $key => $value) {
					$push = [];
					$push['nama'] = strtoupper($value->nama);
					$push['remarks'] = '';
					$push['tamu'] = $data['id'];
					$push['checlist'] = $value->checlist == '1' ? '1' : null;
					$push['createddate'] = date('Y-m-d H:i:s');
					$push['createdby'] = $data['users'];
					$this->db->insert('detail_tamu', $push);
				}
			}
			$result['id'] = $data['id'];

			$this->db->trans_commit();
			$result['is_valid'] = true;
		} catch (\Throwable $th) {
			//throw $th;
			$this->db->trans_rollback();
			$result['message'] = $th->getMessage();
		}
		echo json_encode($result);
	}

	public function getDataTamuHasAcc()
	{
		$data = $_POST;
		$datadb = $this->tamu->getListTamuForStatusAccNotification($data);
		$result['is_valid'] = false;
		$result['data'] = array();
		$result['message'] = "Tidak ada data ditemukan";
		if (!empty($datadb)) {
			$result['is_valid'] = true;
			$result['data'] = $datadb;
			$result['message'] = "Sukses";
		}
		echo json_encode($result);
	}
}
