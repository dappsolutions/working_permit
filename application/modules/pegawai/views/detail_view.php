<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Upt</label>
      <div class="col-sm-4">
       <select disabled class="form-control required" error="Upt" id="upt" style="width: 100%;">
        <option value="">Pilih Upt</option>
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nip Pegawai</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Nip Pegawai" id="nip" 
              placeholder="Nip Pegawai"
              value="<?php echo isset($nip) ? $nip : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nama Pegawai</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Nama Pegawai" id="nama" 
              placeholder="Nama Pegawai"
              value="<?php echo isset($nama) ? $nama : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Email</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Email" id="email" 
              placeholder="Email"
              value="<?php echo isset($email) ? $email : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Hp</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="No Hp" id="no_hp" 
              placeholder="6285xxxxx"
              value="<?php echo isset($no_hp) ? $no_hp : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Posisi</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Posisi" id="posisi" 
              placeholder="Posisi"
              value="<?php echo isset($posisi) ? $posisi : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Posisi Lengkap</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Posisi Lengkap" id="posisi_lengkap" 
              placeholder="Posisi Lengkap"
              value="<?php echo isset($posisi_lengkap) ? $posisi_lengkap : '' ?>">
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Pegawai.back()">Cancel</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
