<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: tahoma;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			/* margin-left: 2%;
   margin-right: 2%; */
		}

	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_table-content" style="font-size: 12px;">
				<img src="<?php echo base_url() . 'files/berkas/spk/' . $data_wp['file_sld'] ?>" height="680px"
					style="text-align: center;width:100%;">
			</div>
			<br />
			<div style="text-align: left;margin-left: 2%;font-family: tahoma;font-size: 14px;">
				<div style="text-align: right;margin-right: 4%;">
					<?php echo $data_wp['upt_tujuan'] . ', ' . date('d F Y') ?>
				</div>
				<br />
				<table style="width: 100%; border: none;">
					<tr style="border:none">
						<td style="font-family: tahoma;text-align: center; border: none;">
							<u>MENYETUJUI :</u>
						</td>
						<td style="font-family: tahoma;text-align: center; border: none;">MENGETAHUI</td>
						<td style="font-family: tahoma;text-align: center; border: none;">PEMOHON</td>
					</tr>
					<tr style="border:none">
						<td style="font-family: tahoma;text-align: center; border: none;">Approved By</td>
						<td>&nbsp;</td>
						<td style="font-family: tahoma;text-align: center; border: none;">Requested By</td>
					</tr>
					<tr style="border:none">
						<td style="text-align: center; border: none;">
							<img
								src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][count($data_wp['paraf']) - 1]['ttd'] ?>"
								width="60px" height="60px">
						</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr style="border:none">
						<td style="font-family: tahoma;text-align: center; border: none;">
							<u><b>(<?php echo $data_wp['paraf'][count($data_wp['paraf']) - 1]['nama_pegawai'] ?>)</b></u>
						</td>
						<td style="font-family: tahoma;text-align: center; border: none;">
							<?php if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') { ?>
							<?php if(trim($data_wp['permit_veirifikasi']) != ''){ ?>
							<?php if($data_wp['ttd_spv_gi'] == ''){ ?>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<?php }else{?>
							<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['ttd_spv_gi'] ?>" width="50px"
								height="50px">
							<?php }?>
							<?php }?>
							<?php } else { ?>
							<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][0]['ttd'] ?>" width="50px"
								height="50px">
							<?php } ?>
							<br>
							<u><b>(<?php
              if ($data_wp['tipe'] == 'EKSTERNAL') {
               if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') {
                echo $data_wp['spv_gi'];
               } else if ($data_wp['nama_place'] == "Kantor Induk") {
                echo $data_wp['paraf'][1]['nama_pegawai'];
               } else {
                echo $data_wp['paraf'][0]['nama_pegawai'];
               }
              } else {
               if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') {
                echo $data_wp['spv_gi'];
               } else {
                echo $data_wp['paraf'][0]['nama_pegawai'];
               }
              }
              ?>)</b></u>
						</td>
						<td style="font-family: tahoma;text-align: center; border: none;">
							&nbsp; <img src="<?php echo base_url() . 'files/berkas/qrcode/' . $data_wp['user'] ?>.png" width="50px"
								height="50px">
							<br>
							<u><b>(<?php echo $data_wp['nama_pemohon'] ?>)</b></u>
						</td>
					</tr>
					<tr style="border:none">
						<td style="font-family: tahoma;text-align: center; border: none;">
							<?php
       if ($data_wp['nama_place'] == 'Kantor Induk') {
        echo '<label>MANAGER UPT/DAL K3L</label>';
       } else {
        echo '<label>MANAGER UPT/DAL K3L</label>';
       }
       ?> &nbsp; <img src="<?php echo base_url() . 'files/berkas/paraf/' . $data_wp['approval_last']['file']; ?>"
								width="20px" height="20px">
						</td>
						<td style="font-family: tahoma;text-align: center; border: none;">
							<?php if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') { ?>
							SPV GI
							<?php } else { ?>
							SPV / LAKSK4 / Admin UIT
							<?php } ?>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div style="text-align: right;">
		<?php if(trim($data_wp['permit_veirifikasi']) != ''){ ?>
		<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['ttd_spv_gi'] ?>" width="20px" height="20px"
			style="text-align: right;">
		<?php }else{?>
		<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][0]['file'] ?>" width="20px" height="20px"
			style="text-align: right;">
		<?php }?>
	</div>
</body>

</html>
