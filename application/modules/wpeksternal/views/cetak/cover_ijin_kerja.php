<html>
 <head>
  <title></title>    
  <style>
   #_wrapper{
    width: 100%;        
    margin: 0 auto;               
   }

   #_content{
    border: 1px solid #999;
    max-width: 100%;
    text-align: center;        
   }

   #_top-content{        
    margin: 0 auto;
    font-family: arial;
   }

   #_int{
    margin: 2% auto;            
    font-family: arial;
   }


   #_bottom-content{
    font-family: arial;
   }

   #_info-content{
    border: 1px solid black;
    margin-left: 10%;
    margin-right: 10%;
    text-align: left;
    padding-left: 2%;
   }      

   #_cover{
    margin-left: 3%;
   }
   h2{
    margin: 0.5%;
   }       
  </style>
 </head>  
 <body>          
  <div style="text-align: right;"><?php echo $data_wp['no_wp'] ?></div>
  <div id="_wrapper">      
   <div id="_content">

    <div>&nbsp;</div>
    <div id="_top-content">             
     <h2 style="text-decoration: underline;">DOKUMEN IJIN KERJA</h2>
     <h2>WORKING PERMIT DOCUMENT</h2>          
    </div>                              
    <div id="_int">          
     <h2 style="text-decoration: underline;"><?php echo $data_wp['tipe'] ?></h2>
    </div>
    <div id="_cover">
     <img src="<?php echo base_url() ?>files/img/smk3.png" height="45%">
     <br/>
     <img src="<?php echo base_url() ?>files/berkas/qrcode/<?php echo $data_wp['no_wp'] ?>.png" height="20%">
    </div>        
    <div>&nbsp;</div>
    <div>&nbsp;</div>

    <div id="_info-content"> 
     <h2 style="text-align: center;"><?php echo strtoupper($data_wp['lokasi_pekerjaan']) ?></h2>
     <table style="font-size: 15px;font-weight: bold;">
      <tr>
       <td>Pekerjaan</td>
       <td>:</td>
       <td><?php echo $data_wp['uraian_pekerjaan'] ?></td>
      </tr>
      <tr>
       <td>Lokasi</td>
       <td>:</td>
       <td><?php echo $data_wp['nama_place'] ?></td>              
      </tr>
      <tr>
       <td>Tanggal Pekerjaan</td>
       <td>:</td>
       <td><?php echo $data_wp['tgl_awal'] . ' - ' . $data_wp['tgl_akhir'] ?></td>
      </tr>
      <tr>
       <td>Pelaksana</td>
       <td>:</td>
       <td><?php echo $data_wp['perusahaan'] ?></td>              
      </tr>
     </table>
     <div>
      &nbsp;
     </div>
    </div>
    <div id="_bottom-content">
     <h3>PT PLN (PERSERO)</h3>
     <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
    </div>        
   </div>
  </div>    
  <div style="text-align: right;">
   <?php if(trim($data_wp['permit_veirifikasi']) != ''){ ?>
    <img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['ttd_spv_gi'] ?>" width="20px" height="20px" style="text-align: right;">
   <?php }else{ ?>
    <img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][0]['file'] ?>" width="20px" height="20px" style="text-align: right;">
   <?php } ?>
  </div>
 </body>
</html>
