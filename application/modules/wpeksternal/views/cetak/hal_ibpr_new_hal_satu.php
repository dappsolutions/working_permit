<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		tr:nth-child(1)>td:nth-child(5) {
			border-right: 1px solid black;
		}

		tr:nth-child(2)>td:nth-child(1) {
			border-top: 1px solid black;
		}

		tr:nth-child(2)>td:nth-child(2),
		tr:nth-child(2)>td:nth-child(3) {
			border: 1px solid black;
		}

		tr:nth-child(3)>td:nth-child(1),
		tr:nth-child(3)>td:nth-child(2) {
			border: 1px solid black;
		}

		tr:nth-child(4)>td:nth-child(1),
		tr:nth-child(4)>td:nth-child(2) {
			border: 1px solid black;
		}

		tr:nth-child(5)>td:nth-child(1),
		tr:nth-child(5)>td:nth-child(2) {
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: tahoma;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
		}

	</style>
</head>
<div id="_wrapper">
	<div id="_content">
		<div id="_top-content">
			<table style="width: 100%;max-width: 100%;">
				<tr>
					<td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
					<td>
						<h3>&nbsp;PT PLN (PERSERO)</h3>
						<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
					</td>
					<td colspan="70"></td>
					<td style="border-right:1px solid black;padding-left: 4%;"><img
							src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img
							src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
				</tr>
				<tr>
					<td id="_judul" colspan="60" rowspan="4">
						<center>
							<label>
								FORMULIR IDENTIFIKASI BAHAYA PENILAIAN DAN PENGENDALIAN RISIKO/PELUANG (IBPPRP)
							</label>
						</center>
					</td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;">
						<label><?php echo 'FK3.TJBTB.02.00.01' ?></label></td>
				</tr>
				<tr>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo '00 / 01' ?></label>
					</td>
				</tr>
				<tr>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;">
						<label><?php echo '1 Mei 2020' ?></label></td>
				</tr>
				<tr>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>7 dari 8</label></td>
				</tr>
			</table>
		</div>
		<div>
			<hr />
		</div>
		<div style="font-family: tahoma;margin-left: 2%;margin-right: 2%;text-align: left;font-size: 12px;">
			<table style="width:100%;">
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>Lokasi Kerja</b>
					</td>
					<td
						style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>:</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<?php echo $data_wp['nama_place'] ?></td>
					<td style="border:1px solid black;" rowspan="3">
						<table style="width:100%;border:none;">
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;"><b>Dibuat
										Oleh,</b></td>
							</tr>
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;">
									Pemohon/Pelaksana</td>
							</tr>
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;">
								<?php if($data_wp['upt_tujuan'] == 'UPT PROBOLINGGO'){ ?>
									&nbsp; <img
										src="<?php echo base_url() . 'files/berkas/qrcode/' . $data_wp['user'] ?>.png"
										width="50px" height="50px">
									<?php }?>
									<br>
									<u><b>(<?php echo $data_wp['nama_pemohon'] ?>)</b></u>
								</td>
							</tr>
							<!-- <tr style="border:none;">
								<td style="border:none;">&nbsp;</td>
							</tr> -->
							<tr style="border:none;">
								<td style="border:none;">&nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border:1px solid black;" rowspan="3">
						<table style="width:100%;border:none;">
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;">
									<b>Diperiksa Oleh,</b></td>
							</tr>
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;">PJ K3
									Kam/PJ OP K3</td>
							</tr>
							<tr style="border:none;">
								<td style="border:none;">&nbsp;</td>
							</tr>
							<tr style="border:none;">
								<td style="border:none;font-family: tahoma;font-size: 12px;text-align:center;">
									<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][0]['ttd'] ?>"
										width="50px" height="50px">
									<br>
									(<?php echo $data_wp['paraf'][0]['nama_pegawai']; ?>)
								</td>
							</tr>
							<tr style="border:none;">
								<td style="border:none;">&nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border:1px solid black;" rowspan="3">
						<table style="width:100%;border:none;">
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;">
									<b>Disetujui Oleh,</b></td>
							</tr>
							<tr style="border:none;">
								<td style="text-align:center;font-family: tahoma;font-size: 12px;border:none;">MUPT / PJ
									DAL K3L</td>
							</tr>
							<tr style="border:none;">
								<td style="border:none;">&nbsp;</td>
							</tr>
							<tr style="border:none;">
								<td style="border:none;font-family: tahoma;font-size: 12px;text-align:center;">
									<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['approval_last']['ttd'] ?>"
										width="70px" height="70px">
									<br>
									<b>(<?php echo $data_wp['approval_last']['nama_pegawai'] ?>)</b>
								</td>
							</tr>
							<tr style="border:none;">
								<td style="border:none;">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px">
						<b>Jenis Pekerjaan</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;text-align:center;">:</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px">
						<?php echo $data_wp['tipe'] ?></td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px">
						<b>Tanggal</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;text-align:center;">:</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px">
						<?php echo date('d F Y', strtotime($data_wp['tanggal_wp'])) ?></td>
				</tr>
			</table>
		</div>
		<br>
		<br>
		<div style="height: 40%;margin-left: 2%;margin-right: 2%;">
			<table style="width: 100%;">
				<tr class="">
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 5%;">
						No</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 15%;">
						Pihak Berkepentingan Terkait</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 13%;">
						Kegiatan</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 17%;">
						Potensi Bahaya</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 12%;">
						Risiko</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 6%;">
						P/R</th>
					<th colspan="3"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 12%;"
						class="text-center">Penilaian Awal</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 20%;"
						class="text-center">Pengendalian Risiko</th>
					<th colspan="3"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 12%;"
						class="text-center">Penilaian Akhir</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 12%;"
						class="text-center">Status Pengendalian</th>
					<th rowspan="2"
						style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;width: 12%;"
						class="text-center">Penanggung Jawab</th>
				</tr>
				<tr class="">
					<th style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">Kon</th>
					<th style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">Kem</th>
					<th style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">TR</th>
					<th style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">Kon</th>
					<th style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">Kem</th>
					<th style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">TR</th>
				</tr>
				<?php $no = 1; ?>
				<?php if (!empty($data_wp['ibppr'])) { ?>
				<?php foreach ($data_wp['ibppr'] as $ib) { ?>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $no++; ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['pihak'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['kegiatan'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: left;font-size: 12px;">
						<?php echo $ib['potensi'] ?>
					</td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['resiko'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['pr'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['kon'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['kem'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['tr'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: left;font-size: 12px;">
						<?php echo $ib['pengendalian_risiko'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['kon_after'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['kem_after'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['tr_after'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['status_pengendalian'] ?></td>
					<td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 12px;">
						<?php echo $ib['penanggung_jawab'] ?></td>
				</tr>
				<?php } ?>
				<?php } ?>

			</table>
			<br />
		</div>
		<div>&nbsp;</div>
	</div>
	<div style="text-align: right;">
		<?php if(trim($data_wp['permit_veirifikasi']) != ''){ ?>
		<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['ttd_spv_gi'] ?>" width="20px" height="20px"
			style="text-align: right;">
		<?php }else{?>
		<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['paraf'][0]['file'] ?>" width="20px"
			height="20px" style="text-align: right;">
		<?php }?>
	</div>
	</body>

</html>
