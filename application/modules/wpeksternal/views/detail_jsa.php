<div class="col-md-12">
	<h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Job Safety Analysis</u></h4>
	<hr />
	<div class="box-body" style="margin-top: -12px;">
		<?php if (!empty($list_jsa)) { ?>
			<?php foreach ($list_jsa as $key => $value) { ?>
				<div class="table-responsive">
				<br>
					<div class='row'>
						<div class='text-center col-md-5'>
							<input type="text" class='form-control required' readonly value="<?php echo $value[0]['judul_jsa'] ?>" error="Judul JSA" id='judul_jsa' placeholder="Judul JSA">
						</div>
					</div>
					<br>
					<table class="table table-bordered" id="tb_jsa" data_id='<?php echo $key ?>'>
						<thead>
							<tr class="bg-primary-light text-white">
								<th>Tahapan Pekerjaan</th>
								<th>Potensi Bahaya</th>
								<th>Pengendalian</th>
							</tr>
						</thead>
						<tbody>
							<?php if (!empty($value)) { ?>
								<?php foreach ($value as $v_item) { ?>
									<tr>
										<td>
											<textarea disabled id="tahapan" class="form-control required" error="Tahapan"><?php echo $v_item['tahapan_pekerjaan'] ?></textarea>
										</td>
										<td>
											<textarea disabled id="potensi" class="form-control required" error="Potensi"><?php echo $v_item['potensi_bahaya'] ?></textarea>
										</td>
										<td>
											<textarea disabled id="pengendalian" class="form-control required" error="Pengendalian"><?php echo $v_item['pengendalian'] ?></textarea>
										</td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr>
									<td colspan="3"> Tidak ada data ditemukan</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
	<!-- /.box-footer -->
</div>
