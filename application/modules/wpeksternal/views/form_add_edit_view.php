<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type="hidden" value='<?php echo isset($status_wp) ? $status_wp : '' ?>' id="status_wp" class="form-control" />
<input type="hidden" value='<?php echo isset($tgl_ibppr_baru) ? $tgl_ibppr_baru : '' ?>' id="tgl_ibppr_baru" class="form-control" />

<div class="row">
	<div class="col-md-12">
		<!-- Horizontal Form -->
		<div class="box box-info padding-16">
			<!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
       </div>
      </div>-->
			<!-- /.box-header -->
			<!-- form start -->
			<div class="box-body">


				<div class="row">
					<div class="col-md-12">
						<h5>Note: Upload File hindari simbol unik seperti : (. , & [] - :) lebih baik memakai (_) underscore</h5>
						<!-- Custom Tabs -->
						<div class="nav-tabs-custom">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#form-pemohon" data-toggle="tab"><label class="label label-warning">1</label>&nbsp;Form Pemohon</a></li>
								<li><a href="#pic_jawab" data-toggle="tab"><label class="label label-warning">2</label> Form Pengawas</a></li>
								<li><a href="#pelaksana" data-toggle="tab"><label class="label label-warning">3</label> Form Pelaksana</a></li>
								<li><a href="#apd" data-toggle="tab"><label class="label label-warning">4</label> Form Alat Kerja</a></li>
								<li><a href="#ibppr" data-toggle="tab"><label class="label label-warning">5</label> Form Ibppr</a></li>
								<li><a href="#fik" data-toggle="tab"><label class="label label-warning">6</label> Form Job Safety</a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="form-pemohon">
									<div class="row">
										<?php echo $this->load->view('form_pemohon'); ?>
										<?php echo $this->load->view('form_wp'); ?>
										<?php echo $this->load->view('form_work_place'); ?>
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success" state='pemohon' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.simpan(this, event)">SIMPAN FORM PEMOHON</button>
										</div>
									</div>
								</div>
								<!-- /.tab-pane -->
								<div class="tab-pane" id="pic_jawab">
									<div class="row">
										<?php echo $this->load->view('form_pengawas'); ?>
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success" state='penanggung_jawab' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.simpan(this, event)">SIMPAN FORM PENGAWAS</button>
										</div>
									</div>
								</div>

								<div class="tab-pane" id="pelaksana">
									<div class="row">
										<?php echo $this->load->view('form_pelaksana'); ?>
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success" state='pelaksana' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.simpan(this, event)">SIMPAN FORM PELAKSANA</button>
										</div>
									</div>
								</div>

								<div class="tab-pane" id="apd">
									<div class="row">
										<?php echo $this->load->view('form_apd'); ?>
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success" state='apd' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.simpan(this, event)">SIMPAN FORM APD</button>
										</div>
									</div>
								</div>

								<div class="tab-pane" id="ibppr">
									<div class="row">
										<?php if ($tgl_ibppr_baru == '1') { ?>
											<?php echo $this->load->view('ket_ibbpr_new'); ?>
											<?php echo $this->load->view('form_ibbpr_new'); ?>
										<?php } else { ?>
											<?php echo $this->load->view('ket_ibbpr'); ?>
											<?php echo $this->load->view('form_ibbpr'); ?>
										<?php } ?>
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success" state='ibppr' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.simpan(this, event)">SIMPAN FORM IBPPR</button>
										</div>
									</div>
								</div>

								<!-- /.tab-pane -->
								<div class="tab-pane" id="fik">
									<div class="row">
										<?php echo $this->load->view('form_jsa'); ?>
									</div>

									<div class="row">
										<div class="col-md-12 text-right">
											<button class="btn btn-success" state='jsa' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.simpan(this, event)">SIMPAN FORM JSA</button>
											&nbsp;
											<button class="btn btn-primary" state='selesai' id_permit='<?php echo isset($id) ? $id : '' ?>' onclick="WpEksternal.selesai('<?php echo isset($id) ? $id : '' ?>', event)">SELESAI</button>
										</div>
									</div>
								</div>
								<!-- /.tab-pane -->
							</div>
							<!-- /.tab-content -->
						</div>
						<!-- nav-tabs-custom -->
					</div>
				</div>

			</div>
			<!-- /.box-body -->
			<!--   <div class="box-footer">
       <button type="button" class="btn btn-default" onclick="WpEksternal.back()">Cancel</button>
       <button type="submit" class="btn btn-success pull-right" onclick="WpEksternal.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
      </div>-->
			<!-- /.box-footer -->
		</div>
		<!-- /.box -->
	</div>
</div>
