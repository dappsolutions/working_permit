<div class="row">
	<div class="col-md-12">
		<a class="btn btn-danger" href="<?php echo base_url() ?>swa/sidak/cetak?permit=<?php echo $permit ?>&no_wp=<?php echo $no_wp ?>">CETAK PDF</a>
	</div>
</div>
<br>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Data SWA</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_swa)) { ?>
							<div class="row">
								<div class="col-md-2">
									<label for="">Nama SWA</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_swa['nama_swa'] == '' ? '-' : $data_swa['nama_swa'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Email SWA</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_swa['email_swa'] == '' ? '-' : $data_swa['email_swa'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Lokasi Pekerjaan</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_swa['lokasi_pekerjaan'] == '' ? '-' : $data_swa['lokasi_pekerjaan'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Uraian Pekerjaan</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_swa['uraian_pekerjaan'] == '' ? '-' : $data_swa['uraian_pekerjaan'] ?></label>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Kelengkapan</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_apd)) { ?>
							<div class="row">
								<div class="col-md-2">
									<label for="">Working Permit</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_apd['wp'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">SOP/IK</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_apd['sop'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Job Safety Analisis</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_apd['jsa'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Pengawas K3 & Pengawas Pekerjaan yang Berkompeten</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_apd['pengawas_k3'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Pekerja Memiliki Sertifikat Kompetensi</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_apd['sertifikat_kom'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Peralatan Kerja yang Layak dan \nSesuai dengan Jenis Pekerjaan</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_apd['peralatan'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Rambu - rambu K3 dan LOTO</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_swa['rambu_k3'] == 'yes' ? 'checked' : '' ?>>
								</div>
								<div class="col-md-2">
									<label for="">Alat Pelindung Diri (APD) yang Layak dan \nTersedia</label>
								</div>
								<div class="col-md-4">
									<input type="checkbox" <?php echo $data_swa['apd'] == 'yes' ? 'checked' : '' ?>>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Data Pekerjaan</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_pekerjaan)) { ?>
							<div class="row">
								<div class="col-md-2">
									<label for="">Pelaksanaan Pekerjaan dihentikan sementara pada jam</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['jam_pekerjaan_dihentikan'] == '' ? '-' : $data_pekerjaan['jam_pekerjaan_dihentikan'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Pekerjaan Dilanjutkan Kembali pada jam</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['jam_pekerjaan_dilanjutkan'] == '' ? '-' : $data_pekerjaan['jam_pekerjaan_dilanjutkan'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Keterangan Penghentian Pekerjaan</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['ket_pekerjaan_dihentikan'] == '' ? '-' : $data_pekerjaan['ket_pekerjaan_dihentikan'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Temuan 1</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['temuan_1'] == '' ? '-' : $data_pekerjaan['temuan_1'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Jenis Temuan 1</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['jenis_temuan_1'] == '' ? '-' : $data_pekerjaan['jenis_temuan_1'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Temuan 2</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['temuan_2'] == '' ? '-' : $data_pekerjaan['temuan_2'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Jenis Temuan 2</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['jenis_temuan_2'] == '' ? '-' : $data_pekerjaan['jenis_temuan_2'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Rekomendasi</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['rekomendasi'] == '' ? '-' : $data_pekerjaan['rekomendasi'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Nama Pengawas K3 / Pengawas Pekerjaan</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['nama_pengawas_k3'] == '' ? '-' : $data_pekerjaan['nama_pengawas_k3'] ?></label>
								</div>
								<div class="col-md-2">
									<label for="">Nama PT / Perusahaan</label>
								</div>
								<div class="col-md-4">
									<label for="" class="label label-primary"><?php echo $data_pekerjaan['nama_perusahaan'] == '' ? '-' : $data_pekerjaan['nama_perusahaan'] ?></label>
								</div>
							</div>
							<br>
							<div class="row">
								<div class="col-md-2">
									<label for="">Tanda Tangan Pengawas K3 / Pengawas Pekerjaan</label>
								</div>
								<div class="col-md-4">
									<?php if (trim($data_pekerjaan['ttd_k3']) != '') { ?>
										<img src="data:image/jpeg;base64,<?php echo $data_pekerjaan['ttd_k3'] ?>" alt="" width="100" height="100">
									<?php } ?>
								</div>
								<div class="col-md-2">
									<label for="">Tanda Tangan Pemegang SWA</label>
								</div>
								<div class="col-md-4">
									<?php if (trim($data_pekerjaan['ttd_swa']) != '') { ?>
										<img src="data:image/jpeg;base64,<?php echo $data_pekerjaan['ttd_swa'] ?>" alt="" width="100" height="100">
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Gambar Sidak</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<?php if (!empty($data_image)) { ?>
							<div class="row">
								<?php foreach ($data_image as $key => $value) { ?>
									<div class="col-md-6">
										<br>
										<img src="data:image/jpeg;base64,<?php echo $value['picture'] ?>" alt="">
									</div>
								<?php } ?>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>