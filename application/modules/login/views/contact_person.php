<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered" id="tb_content">
				<thead>
					<tr class="bg-primary-light text-white">
						<th>No</th>
						<th>Upt</th>
						<th>Nama</th>
						<th>No Hp</th>
					</tr>
				</thead>
				<tbody>
					<?php if (!empty($data)) { ?>
						<?php $no = 1; ?>
						<?php foreach ($data as $value) { ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $value['nama_upt'] ?></td>
								<td><?php echo $value['nama'] ?></td>
								<td><?php echo $value['no_hp'] ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td colspan="10" class="text-center">Tidak ada data ditemukan</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
