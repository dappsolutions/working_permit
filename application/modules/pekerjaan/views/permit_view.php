
<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box padding-16">
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <h4>Pekerjaan Tanggal : <?php echo date('d M Y', strtotime($date)) ?></h4>
        <h5><?php echo $nama_place ?></h5>
        <div class="input-group">
<!--         <input type="text" class="form-control" onkeyup="WpEksternal.search(this, event)" id="keyword" placeholder="Pencarian">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
        </div>
       </div>          
      </div>
      <div class="divider"></div>
      <br/> 
      <div class="row">
       <div class="col-md-12">        
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_content">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>Action</th>
            <th>Status</th>   
            <th>No Pengajuan Wp</th>
            <th>Vendor</th>
            <th>Tanggal Wp</th>
            <!-- <th>Tanggal Pekerjaan</th> -->
            <th>Tanggal Awal Pelaksanaan</th>
            <th>Tanggal Akhir Pelaksanaan</th>
            <th>Tempat Pekerjaan</th>
            <th>Lokasi Pekerjaan</th>
            <th>Uraian Pekerjaan</th>
            <!-- <th>Tujuan</th> -->                     
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = 1; ?>
            <?php foreach ($content as $value) { ?>
             <?php $bg_color = ''; ?>
             <?php if ($value['status'] == 'DRAFT') { ?>
              <?php $bg_color = 'bg-warning'; ?>
             <?php } ?>
             <?php if ($value['status'] == 'REJECTED') { ?>
              <?php $bg_color = 'bg-danger'; ?>
             <?php } ?>
             <?php if ($value['status'] == 'APPROVED') { ?>
              <?php $bg_color = 'bg-success'; ?>
             <?php } ?>
             <tr class="<?php echo $bg_color ?>" data_id="<?php echo $value['id'] ?>">						 
              <td><b><?php echo $no++ ?></b></td>
              <td class="text-center">
               <i data-toggle="tooltip" title="Detail" class="fa fa-file-text grey-text  hover" onclick="Pekerjaan.detail('<?php echo $value['id'] ?>')"></i>
               &nbsp;          
              </td>
              <?php if ($value['status'] == 'DRAFT') { ?>
               <td><b><?php echo $value['status'] ?></b></td>
              <?php } else { ?>              
               <td><b><?php echo $value['status'] ?></b><br/>[Level : <?php echo $value['level'] ?>]</td>
              <?php } ?> 
              <td><b><?php echo $value['no_wp'] ?></b></td>
              <td><b><?php echo $value['nama_vendor'] ?></b></td>
              <td><b><?php echo $value['tanggal_wp'] ?></b></td>
              <!-- <td><b><?php echo $value['tgl_pekerjaan'] ?></b></td> -->
              <td><b><?php echo $value['tgl_awal'] ?></b></td>
              <td><b><?php echo $value['tgl_akhir'] ?></b></td>
              <td><b><?php echo $value['nama_place'] ?></b></td>
              <td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
              <td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
              <!-- <td><b><?php echo $value['nama_upt'] ?></b></td> -->                           

             </tr>
             <tr data_id="<?php echo $value['id'] ?>">
              <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;Single Line Diagram</td>
              <td colspan="9">
               <?php if ($value['nama_single_line'] != '') { ?>
                <b><?php echo $value['nama_single_line'] ?></b>
                &nbsp;
                <i class="fa fa-image" file="<?php echo $value['file_single_line'] ?>" onclick="WpEksternal.showFile(this, 'diagram')"></i>
                <br/>
               <?php } ?>           
              </td>
             </tr>
             <tr data_id="<?php echo $value['id'] ?>">
              <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;Sld</td>
              <td colspan="9">
               <?php if ($value['nama_sld'] != '') { ?>
                <b><?php echo $value['nama_sld'] ?></b>
                &nbsp;
                <i class="fa fa-image" file="<?php echo $value['file_sld'] ?>" onclick="WpEksternal.showFile(this, 'diagram')"></i>
                <br/>
               <?php } ?>                          
              </td>
             </tr>
             <tr data_id="<?php echo $value['id'] ?>">
              <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;High Risk Management</td>
              <td colspan="9">
               <?php if ($value['risk_tipe'] != '') { ?>
                <b><?php echo $value['risk_tipe'] ?></b>
                <br/>
               <?php } ?>               
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="20" class="text-center">Tidak ada data ditemukan</td>
            </tr>
           <?php } ?>           
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>       
  </div>
 </div>