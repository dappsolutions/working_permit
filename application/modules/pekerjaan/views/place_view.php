<div class="box">
 <div class="box-body">
  <div class="row">
   <div class="col-md-12">
    <h4>UNIT </h4>
   </div>
  </div>
  <div class="row">
   <!-- ./col -->
   <?php if (!empty($list_unit)) { ?>
    <?php foreach ($list_unit as $value) { ?>
     <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
       <div class="inner">
        <h3 id="total_draft"><?php echo '' ?></h3>

        <p><?php echo $value['nama'] ?></p>
       </div>
       <div class="icon">
        <!--<i class="ion ion-android-menu"></i>-->
       </div>
       <a href="<?php echo base_url() . $module . '/subplace/' . $value['id'] . '/upt' ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
     </div>
    <?php } ?> 
   <?php } ?> 
   <!-- ./col -->
  </div>
 </div>
</div>