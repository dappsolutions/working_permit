<?php

class Pekerjaan extends MX_Controller
{

	public $hak_akses;
	public $upt;
	public $level;
	public $user;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		// echo '<pre>';
		// print_r($_SESSION);die;
		$this->hak_akses = $this->session->userdata('hak_akses');
		$this->upt = $this->session->userdata('upt_id');
		$this->level = $this->session->userdata('level');
		$this->user = $this->session->userdata('user_id');
	}

	public function getModuleName()
	{
		return 'pekerjaan';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/fullcalendar/dist/fullcalendar.min.css">',
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/wpeksternal_v1-5.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/pekerjaan_v2.js"></script>',
		);

		return $data;
	}

	public function getTableName()
	{
		return "permit";
	}

	public function event($id_place, $upt = "upt", $date = '')
	{
		$data['view_file'] = 'index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Pekerjaan";
		$data['title_content'] = 'Pekerjaan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');

		//  $data['list_year'] = $this->getYear();
		//  $data['data_wp'] = $this->getDataWpPekerjaan($date);
		$data['date'] = $date;
		//  $data['list_data'] = $this->getCurrentEventPermit($id_place, $upt);
		$data['upt'] = $upt;
		$data['id_place'] = $id_place;
		$data['nama_place'] = $this->getNamaPlace($id_place, $upt);
		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getNamaPlace($id, $upt)
	{
		$nama = '';
		if ($upt == 'upt') {
			$data = $this->getDetailUnit($id);
			$nama = $data['nama'];
		} else if ($upt == 'subupt') {
			$data = $this->getDetailSubUnit($id);
			$nama = $data['nama'];
		} else if ($upt == 'gi') {
			$data = $this->getDetailGardu($id);
			$nama = $data['nama'];
		} else if ($upt == 'sutet') {
			$data = $this->getDetailSutet($id);
			$nama = $data['nama'];
		}

		return $nama;
	}

	public function getCurrentEventPermit()
	{
		$id = $_POST['id_place'];
		$upt = $_POST['upt'];
		$date = date('Y-m');
		//  echo '<pre>';
		//  print_r($_POST);die;
		$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 1 and pw.place = '" . $id . "'";
		if ($upt == 'subupt') {
			$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 2 and pw.place = '" . $id . "'";
		}
		if ($upt == 'gi') {
			$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 3 and pw.place = '" . $id . "'";
		}
		if ($upt == 'sutet') {
			$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 4 and pw.place = '" . $id . "'";
		}
		$data = Modules::run('database/get', array(
			'table' => "permit_work pw",
			'field' => array('distinct(pw.tgl_awal) as tgl_pekerjaan', 'pw.tgl_akhir', 'ps.status'),
			'join' => array(
				array('permit p', 'p.id = pw.permit'),
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'pw.permit = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
			),
			"like" => array(
				array('pw.tgl_awal', $date)
			),
			'where' => $where
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				list($year, $month, $day) = explode('-', $value['tgl_pekerjaan']);
				list($year_end, $month_end, $day_end) = explode('-', $value['tgl_akhir']);
				$value['year'] = intval($year);
				$value['month'] = intval($month) - 1;
				$value['day'] = intval($day);
				$value['year_end'] = intval($year_end);
				$value['month_end'] = intval($month_end) - 1;
				$value['day_end'] = intval($day_end);
				array_push($result, $value);
			}
		}
		//  echo '<pre>';
		//  print_r($result);die;
		echo json_encode(array('data' => $result));
	}

	public function place()
	{
		$data['view_file'] = 'place_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tempat Pekerjaan";
		$data['title_content'] = 'Tempat Pekerjaan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['list_unit'] = $this->getListUnit();
		//  $data['list_subunit'] = $this->getListSubUnit();
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function subplace($unit)
	{
		$data['view_file'] = 'sub_place_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tempat Pekerjaan";
		$data['title_content'] = 'Tempat Pekerjaan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['list_unit'] = $this->getListUnit();
		$data['unit'] = $unit;
		//  $data['list_subunit'] = $this->getListSubUnit();
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getDetailUltg($ultg)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt',
			'where' => array('id' => $ultg)
		));

		$ultg_name = "";
		if (!empty($data)) {
			$ultg_name = $data->row_array()['nama'];
		}

		return $ultg_name;
	}

	public function subunitgardu($unit = '', $ultg = "")
	{
		$data['view_file'] = 'subunitgi_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tempat Pekerjaan";
		$data['title_content'] = 'Tempat Pekerjaan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['data'] = $this->getListGarduInduk($unit, $ultg);
		$data['unit'] = $unit;
		$data['ultg'] = $this->getDetailUltg($ultg);
		// echo '<pre>';
		// print_r($data);
		// die;
		echo Modules::run('template', $data);
	}

	public function subunitsutet($gi = '')
	{
		$data['view_file'] = 'subunitsutet_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tempat Pekerjaan";
		$data['title_content'] = 'Tempat Pekerjaan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['data'] = $this->getListSutet($gi);
		$data['gi'] = $gi;
		$data['nama_place'] = $this->getNamaPlace($gi, "gi");
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function subunitultg($unit = '')
	{
		$data['view_file'] = 'subunitultg_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tempat Pekerjaan";
		$data['title_content'] = 'Tempat Pekerjaan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['data'] = $this->getListUltg($unit);
		$data['unit'] = $unit;
		$data['nama_place'] = $this->getNamaPlace($unit, "upt");
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getListUltg($upt)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt su',
			'field' => array('su.*'),
			'where' => "su.deleted = 0 and su.upt = '" . $upt . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListSutet($gi)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sutet s',
			'field' => array('s.*'),
			'where' => "s.deleted = 0 and s.gardu_induk = '" . $gi . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getListGarduInduk($unit, $ultg = "")
	{
		$data = array();
		if ($ultg != '') {
			$data = Modules::run('database/get', array(
				'table' => 'sub_upt_has_gardu shg',
				'field' => array('gi.*'),
				'join' => array(
					array('gardu_induk gi', 'gi.id = shg.gardu_induk', 'gi.deleted = 0')
				),
				'where' => "shg.sub_upt = " . $ultg . ""
			));
		} else {
			$data = Modules::run('database/get', array(
				'table' => 'gardu_induk gi',
				'field' => array('gi.*'),
				'where' => "gi.deleted = 0 and gi.upt = '" . $unit . "'"
			));
		}

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListUnit()
	{
		$data = Modules::run('database/get', array(
			'table' => "upt u",
			'field' => array('u.*'),
			'where' => "u.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListSubUnit()
	{
		$data = Modules::run('database/get', array(
			'table' => "sub_upt u",
			'field' => array('u.*'),
			'where' => "u.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataPermitDb($date)
	{
		$id = $_POST['id_place'];
		$upt = $_POST['upt'];
		//  echo '<pre>';
		//  print_r($_POST);die;
		$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 1 and pw.place = '" . $id . "'";
		if ($upt == 'subupt') {
			$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 2 and pw.place = '" . $id . "'";
		}

		if ($upt == 'gi') {
			$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 3 and pw.place = '" . $id . "'";
		}

		if ($upt == 'sutet') {
			$where = "p.deleted = 0 and ps.status != 'REJECTED' and pw.work_place = 4 and pw.place = '" . $id . "'";
		}

		$data = Modules::run('database/get', array(
			'table' => "permit_work pw",
			'field' => array('distinct(pw.tgl_awal) as tgl_pekerjaan', 'pw.tgl_akhir', 'ps.status'),
			'join' => array(
				array('permit p', 'p.id = pw.permit'),
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'pw.permit = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
			),
			"like" => array(
				array('pw.tgl_awal', $date)
			),
			'where' => $where
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				list($year, $month, $day) = explode('-', $value['tgl_pekerjaan']);
				list($year_end, $month_end, $day_end) = explode('-', $value['tgl_akhir']);
				$value['year'] = intval($year);
				$value['month'] = intval($month) - 1;
				$value['day'] = intval($day);
				$value['year_end'] = intval($year_end);
				$value['month_end'] = intval($month_end) - 1;
				$value['day_end'] = intval($day_end);
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataPermit()
	{
		$date = date('Y-m', strtotime($_POST['date']));
		//  $date = "2020-01";

		$result = $this->getDataPermitDb($date);

		//  echo '<pre>';
		//  print_r($result);die;
		echo json_encode(array('data' => $result));
	}

	public function getListPermit($date, $upt, $id_place)
	{
		$where = "p.deleted =0 and (CAST('" . $date . "' AS DATE) between CAST(pw.tgl_awal as date) and cast(pw.tgl_akhir as date)) ";
		if ($upt == 'subupt') {
			$where .= " and pw.work_place = 2 and pw.place = '" . $id_place . "'";
		} else {
			if ($upt == 'gi') {
				$where .= " and pw.work_place = 3 and pw.place = '" . $id_place . "'";
			}

			if ($upt == 'sutet') {
				$where .= " and pw.work_place = 4 and pw.place = '" . $id_place . "'";
			}

			if ($upt != 'sutet' && $upt != 'gi') {
				$where .= " and pw.work_place = 1 and pw.place = '" . $id_place . "'";
			}
		}

		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array(
				'p.*', 'ph.email',
				'ph.nama_pemohon', 'ph.perusahaan',
				'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
				'ph.alamat', 'ph.jabatan',
				'pw.tgl_pekerjaan', 'pw.tgl_awal',
				'pw.tgl_akhir', 'pw.work_place',
				'pw.place', 'pw.lokasi_pekerjaan',
				'pw.uraian_pekerjaan',
				'wp.jenis as jenis_place',
				'pst.status', 'ut.nama as nama_upt',
				'pst.level',
				'sl.nama as nama_single_line',
				'sl.file as file_single_line',
				'sld.nama as nama_sld',
				'sld.file as file_sld',
				'rm.nama as risk_tipe', 'pw.is_need_sistem', 'vdn.nama_vendor'
			),
			'join' => array(
				array('pemohon ph', 'ph.id = p.pemohon'),
				array('permit_work pw', 'pw.permit = p.id'),
				array('work_place wp', 'pw.work_place = wp.id'),
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array('upt ut', 'ut.id = pp.upt'),
				array('(select max(id) id, permit from permit_single_line group by permit) psld', 'p.id = psld.permit', 'left'),
				array('permit_single_line psl', 'psl.id = psld.id', 'left'),
				array('single_line sl', 'psl.single_line = sl.id', 'left'),
				array('(select max(id) id, permit from permit_risk group by permit) prk', 'p.id = prk.permit', 'left'),
				array('permit_risk psk', 'prk.id = psk.id', 'left'),
				array('risk_management rm', 'rm.id = psk.risk_management', 'left'),
				array('(select max(id) id, permit from permit_sld group by permit) psldg', 'p.id = psldg.permit', 'left'),
				array('permit_sld psldi', 'psldi.id = psldg.id', 'left'),
				array('user usr_ven', 'p.user = usr_ven.id', 'left'),
				array('vendor vdn', 'vdn.id = usr_ven.vendor', 'left'),
				array('sld sld', 'psldi.sld = sld.id', 'left'),
			),
			'where' => $where,
			'orderby' => 'p.id desc'
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama_place'] = $value['jenis_place'];
				if ($this->hak_akses != 'vendor') {
					$value['nama_place'] = $this->getNameOfPlace($value);
				}
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDetailUnit($unit)
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt u',
			'field' => array('u.*'),
			'where' => "u.id = '" . $unit . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['id'];
		}


		return $result;
	}

	public function getDetailSubUnit($sub_unit)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt su',
			'field' => array('su.*', 'u.nama as nama_upt'),
			'join' => array(
				array('upt u', 'su.upt = u.id')
			),
			'where' => "su.id = '" . $sub_unit . "'",
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['upt'];
			$result['nama'] = $result['nama_upt'] . ' - ' . $result['nama'];;
		}
		//  echo '<pre>';
		//  print_r($result);die;

		return $result;
	}

	public function getDetailGardu($gi)
	{
		$data = Modules::run('database/get', array(
			'table' => 'gardu_induk gi',
			'field' => array('gi.*', 'ut.nama as nama_upt', 'pg.nama as nama_pegawai'),
			'join' => array(
				array('upt ut', 'gi.upt = ut.id'),
				array('gardu_induk_has_pegawai pgh', 'pgh.gardu_induk = gi.id', 'left'),
				array('pegawai pg', 'pg.id = pgh.pegawai', 'left'),
			),
			'where' => "gi.id = '" . $gi . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['nama'] = $result['nama_gardu'];
			$result['upt_id'] = $result['upt'];
			$result['nama_pegawai'] = $result['nama_pegawai'];
		}


		return $result;
	}

	public function getDetailSutet($sutet)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sutet s',
			'field' => array(
				's.*', 'ut.nama as nama_upt',
				'ut.id as upt', 'gi.nama_gardu', 'pg.nama as nama_pegawai'
			),
			'join' => array(
				array('gardu_induk gi', 'gi.id = s.gardu_induk'),
				array('upt ut', 'gi.upt = ut.id'),
				array('gardu_induk_has_pegawai pgh', 'pgh.gardu_induk = gi.id', 'left'),
				array('pegawai pg', 'pg.id = pgh.pegawai', 'left'),
			),
			'where' => "s.id = '" . $sutet . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['upt'];
			$result['nama'] = $result['nama_gardu'] . ' : SUTT/SUTET - ' . $result['nama'];
			$result['nama_pegawai'] = $result['nama_pegawai'];
		}


		return $result;
	}

	public function getNameOfPlace($data)
	{
		switch ($data['work_place']) {
			case 1:
				$data_place = $this->getDetailUnit($data['place']);
				break;
			case 2:
				$data_place = $this->getDetailSubUnit($data['place']);
				break;
			case 3:
				$data_place = $this->getDetailGardu($data['place']);
				break;
			case 4:
				$data_place = $this->getDetailSutet($data['place']);
				break;

			default:
				break;
		}
		$nama_place = $data_place['nama'];

		return $nama_place;
	}

	public function view($date, $upt, $id_place)
	{

		$data['view_file'] = 'permit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Pekerjaan";
		$data['title_content'] = 'Pekerjaan';
		$data['date'] = $date;
		$content = $this->getListPermit($date, $upt, $id_place);
		$data['content'] = $content;
		$data['nama_place'] = $this->getNamaPlace($id_place, $upt);

		echo Modules::run('template', $data);
	}

	public function detailPermit($id)
	{
		$data = Modules::run('historyeks/getDetailDataHistoryeks', $id);
		//  echo '<pre>';
		//  print_r($data);die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pekerjaan";
		$data['title_content'] = "Detail Pekerjaan";
		$data['list_pemohon'] = Modules::run('historyeks/getListPemohon');
		$data['list_place'] = Modules::run('historyeks/getListWorkPlace');
		$data['list_tj'] = Modules::run('historyeks/getListTjPermit', $id);
		$data['list_pelaksana'] = Modules::run('historyeks/getListPelaksanaPermit', $id);
		$data['list_apd'] = Modules::run('historyeks/getListApdPermit', $id);
		$data['list_jsa'] = Modules::run('historyeks/getListJsaPermit', $id);
		$data['list_akibat'] = Modules::run('historyeks/getListAkibat');
		$data['list_paparan'] = Modules::run('historyeks/getListPemaparan');
		$data['list_peluang'] = Modules::run('historyeks/getListPeluang');
		$data['list_ibppr'] = Modules::run('historyeks/getListPermitIbppr', $id);
		$data['status_approve'] = Modules::run('historyeks/getListStatusPermit', $id);
		$data['akses'] = $this->hak_akses;
		echo $this->load->view('wpeksternal/detail_view', $data, true);
	}
}
