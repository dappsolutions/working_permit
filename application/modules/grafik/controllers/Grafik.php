<?php

class Grafik extends MX_Controller
{

    public $hak_akses;
    public $upt;
    public $level;
    public $user;

    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set("Asia/Jakarta");
        // echo '<pre>';
        // print_r($_SESSION);die;
        $this->hak_akses = $this->session->userdata('hak_akses');
        $this->upt = $this->session->userdata('upt_id');
        $this->level = $this->session->userdata('level');
        $this->user = $this->session->userdata('user_id');
    }

    public function getModuleName()
    {
        return 'grafik';
    }

    public function getHeaderJSandCSS()
    {
        $data = array(
            '<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
            '<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
            '<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.css">',
            '<script src="' . base_url() . 'assets/js/controllers/grafik.js"></script>',
        );

        return $data;
    }

    public function index($date = '', $upt = "")
    {
        $data['view_file'] = 'index';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Grafik";
        $data['title_content'] = 'Grafik';
        $data['pengajuan'] = array();
        $data['hak_akses'] = $this->session->userdata('hak_akses');
        $data['list_year'] = $this->getYear();
        $data['list_upt'] = $this->getListUpt();
        $data['data_wp'] = $this->getDataWpGrafik($date, $upt);
        $data['date'] = $date;
        $data['upt'] = $upt;
        //  echo "<pre>";
        //  echo $this->db->last_query();
        //  die;
        //  echo '<pre>';
        //  print_r($data['list_upt']);die;
        echo Modules::run('template', $data);
    }

    public function getListUpt()
    {
        $data = Modules::run('database/get', array(
            'table' => 'upt',
            'where' => "deleted = 0"
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


        return $result;
    }

    public function getYear()
    {
        $year = date('Y');
        $data = array();
        for ($year = intval(date('Y')); $year < intval(date('Y')) + 3; $year++) {
            $data[$year] = $year;
        }

        return $data;
    }

    public function getDataWpGrafik($date = '', $upt = '')
    {
        if ($date == '') {
            $date = date('Y');
        }

        $filterUpt = '';
        if($upt != ''){
            $filterUpt = " and pp.upt = '".$upt."'";
        }

        $data = Modules::run('database/get', array(
            'table' => 'permit pt',
            'field' => array('pt.*'),
            'join' => array(
                array('permit_purpose pp', 'pp.permit = pt.id')
            ),
            'like' => array(
                array('pt.createddate', $date)
            ),
            'inside_brackets' => true,
            'where' => "pt.deleted = 0 ".$filterUpt,
            'limit' => 20000
        ));

        // echo '<pre>';
        // echo $this->db->last_query();
        // die;


        $result = array();
        $totat_jan = 0;
        $totat_feb = 0;
        $totat_mar = 0;
        $totat_apr = 0;
        $totat_mei = 0;
        $totat_jun = 0;
        $totat_jul = 0;
        $totat_ags = 0;
        $totat_sep = 0;
        $totat_okt = 0;
        $totat_nov = 0;
        $totat_des = 0;

        //eksternal
        $totat_jan_eks = 0;
        $totat_feb_eks = 0;
        $totat_mar_eks = 0;
        $totat_apr_eks = 0;
        $totat_mei_eks = 0;
        $totat_jun_eks = 0;
        $totat_jul_eks = 0;
        $totat_ags_eks = 0;
        $totat_sep_eks = 0;
        $totat_okt_eks = 0;
        $totat_nov_eks = 0;
        $totat_des_eks = 0;

        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                $permit_date = date('m', strtotime($value['createddate']));
                switch ($permit_date) {
                    case "01":
                        if ($value['tipe_permit'] == 1) {
                            $totat_jan += 1;
                        } else {
                            $totat_jan_eks += 1;
                        }
                        break;
                    case "02":
                        if ($value['tipe_permit'] == 1) {
                            $totat_feb += 1;
                        } else {
                            $totat_feb_eks += 1;
                        }
                        break;
                    case "03":
                        if ($value['tipe_permit'] == 1) {
                            $totat_mar += 1;
                        } else {
                            $totat_mar_eks += 1;
                        }
                        break;
                    case "04":
                        if ($value['tipe_permit'] == 1) {
                            $totat_apr += 1;
                        } else {
                            $totat_apr_eks += 1;
                        }
                        break;
                    case "05":
                        if ($value['tipe_permit'] == 1) {
                            $totat_mei += 1;
                        } else {
                            $totat_mei_eks += 1;
                        }
                        break;
                    case "06":
                        if ($value['tipe_permit'] == 1) {
                            $totat_jun += 1;
                        } else {
                            $totat_jun_eks += 1;
                        }
                        break;
                    case "07":
                        if ($value['tipe_permit'] == 1) {
                            $totat_jul += 1;
                        } else {
                            $totat_jul_eks += 1;
                        }
                        break;
                    case "08":
                        if ($value['tipe_permit'] == 1) {
                            $totat_ags += 1;
                        } else {
                            $totat_ags_eks += 1;
                        }
                        break;
                    case "09":
                        if ($value['tipe_permit'] == 1) {
                            $totat_sep += 1;
                        } else {
                            $totat_sep_eks += 1;
                        }
                        break;
                    case "10":
                        if ($value['tipe_permit'] == 1) {
                            $totat_okt += 1;
                        } else {
                            $totat_okt_eks += 1;
                        }
                        break;
                    case "11":
                        if ($value['tipe_permit'] == 1) {
                            $totat_nov += 1;
                        } else {
                            $totat_nov_eks += 1;
                        }
                        break;
                    case "12":
                        if ($value['tipe_permit'] == 1) {
                            $totat_des += 1;
                        } else {
                            $totat_des_eks += 1;
                        }
                        break;

                    default:
                        break;
                }
            }
        }


        for ($i = 1; $i < 13; $i++) {
            $permit_date = $i < 10 ? '0' . $i : $i;
            switch ($permit_date) {
                case "01":
                    array_push($result, array(
                        'y' => 'Jan',
                        'a' => $totat_jan,
                        'b' => $totat_jan_eks
                    ));
                    break;
                case "02":
                    array_push($result, array(
                        'y' => 'Feb',
                        'a' => $totat_feb,
                        'b' => $totat_feb_eks
                    ));
                    break;
                case "03":
                    array_push($result, array(
                        'y' => 'Mar',
                        'a' => $totat_mar,
                        'b' => $totat_mar_eks
                    ));
                    break;
                case "04":
                    array_push($result, array(
                        'y' => 'Apr',
                        'a' => $totat_apr,
                        'b' => $totat_apr_eks
                    ));
                    break;
                case "05":
                    array_push($result, array(
                        'y' => 'Mei',
                        'a' => $totat_mei,
                        'b' => $totat_mei_eks
                    ));
                    break;
                case "06":
                    array_push($result, array(
                        'y' => 'Jun',
                        'a' => $totat_jun,
                        'b' => $totat_jun_eks
                    ));
                    break;
                case "07":
                    array_push($result, array(
                        'y' => 'Jul',
                        'a' => $totat_jul,
                        'b' => $totat_jul_eks
                    ));
                    break;
                case "08":
                    array_push($result, array(
                        'y' => 'Ags',
                        'a' => $totat_ags,
                        'b' => $totat_ags_eks
                    ));
                    break;
                case "09":
                    array_push($result, array(
                        'y' => 'Sep',
                        'a' => $totat_sep,
                        'b' => $totat_sep_eks
                    ));
                    break;
                case "10":
                    array_push($result, array(
                        'y' => 'Okt',
                        'a' => $totat_okt,
                        'b' => $totat_okt_eks
                    ));
                    break;
                case "11":
                    array_push($result, array(
                        'y' => 'Nov',
                        'a' => $totat_nov,
                        'b' => $totat_nov_eks
                    ));
                    break;
                case "12":
                    array_push($result, array(
                        'y' => 'Des',
                        'a' => $totat_des,
                        'b' => $totat_des_eks
                    ));
                    break;

                default:
                    break;
            }
        }

        return json_encode($result);
    }
    
    public function getDataWpGrafikApi($date = '', $upt = '')
    {
        if ($date == '') {
            $date = date('Y');
        }

        $filterUpt = '';
        if($upt != ''){
            $filterUpt = " and pp.upt = '".$upt."'";
        }

        $data = Modules::run('database/get', array(
            'table' => 'permit pt',
            'field' => array('pt.*'),
            'join' => array(
                array('permit_purpose pp', 'pp.permit = pt.id')
            ),
            'like' => array(
                array('pt.createddate', $date)
            ),
            'inside_brackets' => true,
            'where' => "pt.deleted = 0 ".$filterUpt,
            'limit' => 20000
        ));

        // echo '<pre>';
        // echo $this->db->last_query();
        // die;


        $result = array();
        $totat_jan = 0;
        $totat_feb = 0;
        $totat_mar = 0;
        $totat_apr = 0;
        $totat_mei = 0;
        $totat_jun = 0;
        $totat_jul = 0;
        $totat_ags = 0;
        $totat_sep = 0;
        $totat_okt = 0;
        $totat_nov = 0;
        $totat_des = 0;

        //eksternal
        $totat_jan_eks = 0;
        $totat_feb_eks = 0;
        $totat_mar_eks = 0;
        $totat_apr_eks = 0;
        $totat_mei_eks = 0;
        $totat_jun_eks = 0;
        $totat_jul_eks = 0;
        $totat_ags_eks = 0;
        $totat_sep_eks = 0;
        $totat_okt_eks = 0;
        $totat_nov_eks = 0;
        $totat_des_eks = 0;

        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                $permit_date = date('m', strtotime($value['createddate']));
                switch ($permit_date) {
                    case "01":
                        if ($value['tipe_permit'] == 1) {
                            $totat_jan += 1;
                        } else {
                            $totat_jan_eks += 1;
                        }
                        break;
                    case "02":
                        if ($value['tipe_permit'] == 1) {
                            $totat_feb += 1;
                        } else {
                            $totat_feb_eks += 1;
                        }
                        break;
                    case "03":
                        if ($value['tipe_permit'] == 1) {
                            $totat_mar += 1;
                        } else {
                            $totat_mar_eks += 1;
                        }
                        break;
                    case "04":
                        if ($value['tipe_permit'] == 1) {
                            $totat_apr += 1;
                        } else {
                            $totat_apr_eks += 1;
                        }
                        break;
                    case "05":
                        if ($value['tipe_permit'] == 1) {
                            $totat_mei += 1;
                        } else {
                            $totat_mei_eks += 1;
                        }
                        break;
                    case "06":
                        if ($value['tipe_permit'] == 1) {
                            $totat_jun += 1;
                        } else {
                            $totat_jun_eks += 1;
                        }
                        break;
                    case "07":
                        if ($value['tipe_permit'] == 1) {
                            $totat_jul += 1;
                        } else {
                            $totat_jul_eks += 1;
                        }
                        break;
                    case "08":
                        if ($value['tipe_permit'] == 1) {
                            $totat_ags += 1;
                        } else {
                            $totat_ags_eks += 1;
                        }
                        break;
                    case "09":
                        if ($value['tipe_permit'] == 1) {
                            $totat_sep += 1;
                        } else {
                            $totat_sep_eks += 1;
                        }
                        break;
                    case "10":
                        if ($value['tipe_permit'] == 1) {
                            $totat_okt += 1;
                        } else {
                            $totat_okt_eks += 1;
                        }
                        break;
                    case "11":
                        if ($value['tipe_permit'] == 1) {
                            $totat_nov += 1;
                        } else {
                            $totat_nov_eks += 1;
                        }
                        break;
                    case "12":
                        if ($value['tipe_permit'] == 1) {
                            $totat_des += 1;
                        } else {
                            $totat_des_eks += 1;
                        }
                        break;

                    default:
                        break;
                }
            }
        }


        for ($i = 1; $i < 13; $i++) {
            $permit_date = $i < 10 ? '0' . $i : $i;
            switch ($permit_date) {
                case "01":
                    array_push($result, array(
                        'y' => 'Jan',
                        'a' => $totat_jan,
                        'b' => $totat_jan_eks
                    ));
                    break;
                case "02":
                    array_push($result, array(
                        'y' => 'Feb',
                        'a' => $totat_feb,
                        'b' => $totat_feb_eks
                    ));
                    break;
                case "03":
                    array_push($result, array(
                        'y' => 'Mar',
                        'a' => $totat_mar,
                        'b' => $totat_mar_eks
                    ));
                    break;
                case "04":
                    array_push($result, array(
                        'y' => 'Apr',
                        'a' => $totat_apr,
                        'b' => $totat_apr_eks
                    ));
                    break;
                case "05":
                    array_push($result, array(
                        'y' => 'Mei',
                        'a' => $totat_mei,
                        'b' => $totat_mei_eks
                    ));
                    break;
                case "06":
                    array_push($result, array(
                        'y' => 'Jun',
                        'a' => $totat_jun,
                        'b' => $totat_jun_eks
                    ));
                    break;
                case "07":
                    array_push($result, array(
                        'y' => 'Jul',
                        'a' => $totat_jul,
                        'b' => $totat_jul_eks
                    ));
                    break;
                case "08":
                    array_push($result, array(
                        'y' => 'Ags',
                        'a' => $totat_ags,
                        'b' => $totat_ags_eks
                    ));
                    break;
                case "09":
                    array_push($result, array(
                        'y' => 'Sep',
                        'a' => $totat_sep,
                        'b' => $totat_sep_eks
                    ));
                    break;
                case "10":
                    array_push($result, array(
                        'y' => 'Okt',
                        'a' => $totat_okt,
                        'b' => $totat_okt_eks
                    ));
                    break;
                case "11":
                    array_push($result, array(
                        'y' => 'Nov',
                        'a' => $totat_nov,
                        'b' => $totat_nov_eks
                    ));
                    break;
                case "12":
                    array_push($result, array(
                        'y' => 'Des',
                        'a' => $totat_des,
                        'b' => $totat_des_eks
                    ));
                    break;

                default:
                    break;
            }
        }

        echo json_encode(array(
            'data_wp'=> $result
        ));
    }
}
