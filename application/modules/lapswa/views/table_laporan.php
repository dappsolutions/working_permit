<?php if (!empty($content)) { ?>
	<?php $no = 1; ?>
	<?php foreach ($content as $value) { ?>
		<?php $bg_color = ''; ?>
		<?php if ($value['status'] == 'DRAFT') { ?>
			<?php $bg_color = 'bg-warning'; ?>
		<?php } ?>
		<?php if ($value['status'] == 'REJECTED') { ?>
			<?php $bg_color = 'bg-danger'; ?>
		<?php } ?>
		<?php if ($value['status'] == 'APPROVED') { ?>
			<?php $bg_color = 'bg-success'; ?>
		<?php } ?>
		<tr class="<?php echo $bg_color ?>">
			<td class="text-center">
				<a href="<?php echo base_url() ?>swa/sidak/showDetailPengajuan?permit=<?php echo $value['id'] ?>&no_wp=<?php echo $value['no_wp'] ?>" onclick="LapSwa.showPengajuan(this,event)"><i class="fa fa-file-text grey-text  hover"></i></a>
			</td>
			<td><b><?php echo $no++ ?></b></td>
			<td><b><?php echo $value['no_trans_swa'] ?></b></td>
			<td><b><?php echo $value['tipe'] ?></b></td>
			<td><b><?php echo $value['no_wp'] ?></b></td>
			<td><b><?php echo $value['nama_swa'] ?></b></td>
			<td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
			<td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
			<td><b><?php echo $value['perusahaan'] ?></b></td>
			<td><b><?php echo $value['waktu_inspeksi'] ?></b></td>
			<td><b><?php echo $value['jenis_temuan_1'] ?></b></td>
			<td><b><?php echo $value['jenis_temuan_2'] ?></b></td>
			<td><b><?php echo $value['jam_pekerjaan_dihentikan'] ?></b></td>
			<td><b><?php echo $value['jam_pekerjaan_dilanjutkan'] ?></b></td>
		</tr>
	<?php } ?>
<?php } else { ?>
	<tr>
		<td colspan="20" class="text-center">Tidak ada data ditemukan</td>
	</tr>
<?php } ?>