<?php

class Spvgi extends MX_Controller
{

  public $segment;
  public $limit;
  public $page;
  public $last_no;
  public $akses;

  public function __construct()
  {
    parent::__construct();
    $this->limit = 25;
    $this->akses = $this->session->userdata('hak_akses');
  }

  public function getModuleName()
  {
    return 'spvgi';
  }

  public function getHeaderJSandCSS()
  {
    $version  = str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz');
    $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/spvgi.js?v=' . $version . '"></script>'
    );

    return $data;
  }

  public function getTableName()
  {
    return 'gardu_induk_has_pegawai';
  }

  public function getRootModule()
  {
    return "Data";
  }

  public function index()
  {
    // echo '<pre>';
    // print_r($_SESSION);die;
    $this->segment = 3;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;

    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = $this->getRootModule() . " - Spv Gi";
    $data['title_content'] = 'Spv Gi';
    $content = $this->getDataSpvGi();
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function getTotalDataSpvGi($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('u.nama_gardu', $keyword),
        array('pg.nama', $keyword),
      );
    }

    $where = "su.deleted = 0";
    if ($this->akses == 'superadmin') {
      $where = "su.deleted = 0";
    }

    if (isset($_SESSION['upt'])) {
      $where .= " AND u.upt = '" . $_SESSION['upt'] . "'";
    }

    switch ($keyword) {
      case "":
        $total = Modules::run('database/count_all', array(
          'table' => $this->getTableName() . ' su',
          'field' => array('su.*', 'u.nama_gardu', 'pg.nama as nama_pegawai'),
          'join' => array(
            array('gardu_induk u', 'su.gardu_induk = u.id'),
            array('pegawai pg', 'pg.id = su.pegawai', 'left'),
          ),
          'is_or_like' => true,
          'like' => $like,
          'where' => $where
        ));
        break;
      default:
        $total = Modules::run('database/count_all', array(
          'table' => $this->getTableName() . ' su',
          'field' => array('su.*', 'u.nama_gardu', 'pg.nama as nama_pegawai'),
          'join' => array(
            array('gardu_induk u', 'su.gardu_induk = u.id'),
            array('pegawai pg', 'pg.id = su.pegawai', 'left'),
          ),
          'is_or_like' => true,
          'like' => $like,
          'inside_brackets' => true,
          'where' => $where
        ));
        break;
    }

    return $total;
  }

  public function getDataSpvGi($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('u.nama_gardu', $keyword),
        array('pg.nama', $keyword),
      );
    }

    $where = "su.deleted = 0";
    if ($this->akses == 'superadmin') {
      $where = "su.deleted = 0";
    }

    if (isset($_SESSION['upt'])) {
      $where .= " AND u.upt = '" . $_SESSION['upt'] . "'";
    }


    switch ($keyword) {
      case "":
        $data = Modules::run('database/get', array(
          'table' => $this->getTableName() . ' su',
          'field' => array('su.*', 'u.nama_gardu', 'pg.nama as nama_pegawai'),
          'join' => array(
            array('gardu_induk u', 'su.gardu_induk = u.id'),
            array('pegawai pg', 'pg.id = su.pegawai'),
          ),
          'like' => $like,
          'is_or_like' => true,
          'limit' => $this->limit,
          'offset' => $this->last_no,
          'where' => $where
        ));
        break;
      default:
        $data = Modules::run('database/get', array(
          'table' => $this->getTableName() . ' su',
          'field' => array('su.*', 'u.nama_gardu', 'pg.nama as nama_pegawai'),
          'join' => array(
            array('gardu_induk u', 'su.gardu_induk = u.id'),
            array('pegawai pg', 'pg.id = su.pegawai'),
          ),
          'like' => $like,
          'is_or_like' => true,
          'limit' => $this->limit,
          'offset' => $this->last_no,
          'inside_brackets' => true,
          'where' => $where
        ));
        break;
    }

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataSpvGi($keyword)
    );
  }

  public function getDetailDataSpvGi($id)
  {
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' t',
      'field' => array('t.*', 'pg.file_ttd', 'pg.pegawai_type', 'usr.password', 'usr.username as email', 'usr.id as user_id', 'pg.posisi'),
      'join' => array(
        array('pegawai as pg', 'pg.id = t.pegawai'),
        array('pegawai_type as pt', 'pt.id = pg.pegawai_type', 'left'),
        array('user as usr', 'usr.pegawai = pg.id', 'left'),
      ),
      'where' => "t.id = '" . $id . "'"
    ));

    $data = $data->row_array();
    return $data;
  }

  public function getListGardu()
  {
    $where = 'gi.deleted = 0 ';
    if (isset($_SESSION['upt'])) {
      $where .= " AND gi.upt = '" . $_SESSION['upt'] . "'";
    }
    $data = Modules::run('database/get', array(
      'table' => 'gardu_induk as gi',
      'where' => $where
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    return $result;
  }

  public function getListPegawai($upt = "")
  {
    $where = "p.deleted = 0";
    if (isset($_SESSION)) {
      $where = "p.upt = '" . $_SESSION['upt'] . "'";
    }
    $data = Modules::run('database/get', array(
      'table' => 'pegawai p',
      'field' => array('p.*', 'u.id as user_id'),
      'join' => array(
        array('user u', 'p.id = u.pegawai', 'left')
      ),
      'where' => $where,
      'limit' => 5000
    ));

    // echo '<pre>';
    // echo $this->db->last_query();
    // die;

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    return $result;
  }

  public function getListPegawaiType($upt = "")
  {
    $data = Modules::run('database/get', array(
      'table' => 'pegawai_type p',
      'field' => array('p.*'),
      'limit' => 5000
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }


    return $result;
  }

  public function add()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Gardu_induk";
    $data['title_content'] = 'Tambah Gardu_induk';
    $data['list_gardu'] = $this->getListGardu();
    $data['list_type'] = $this->getListPegawaiType();
    $data['list_pegawai'] = $this->getListPegawai();
    echo Modules::run('template', $data);
  }

  public function ubah($id)
  {
    $data = $this->getDetailDataSpvGi($id);
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Ubah Gardu_induk";
    $data['title_content'] = 'Ubah Gardu_induk';
    $data['list_gardu'] = $this->getListGardu();
    $data['list_type'] = $this->getListPegawaiType();
    $data['list_pegawai'] = $this->getListPegawai();
    echo Modules::run('template', $data);
  }

  public function detail($id)
  {
    $data = $this->getDetailDataSpvGi($id);
    //   echo '<pre>';
    //   print_r($data);die;
    $data['view_file'] = 'detail_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Detail Gardu_induk";
    $data['title_content'] = "Detail Gardu_induk";
    $data['list_gardu'] = $this->getListGardu();
    $data['list_type'] = $this->getListPegawaiType();
    $data['list_pegawai'] = $this->getListPegawai();
    echo Modules::run('template', $data);
  }

  public function getPostDataSpvGi($value)
  {
    $data['gardu_induk'] = $value->gardu_induk;
    $data['pegawai'] = $value->pegawai;
    return $data;
  }

  public function simpan()
  {
    $data = json_decode($this->input->post('data'));
    $id = $this->input->post('id');
    $file = $_FILES;
    $is_valid = false;


    $is_save = true;
    $this->db->trans_begin();
    try {
      $post = $this->getPostDataSpvGi($data->form);
      if ($id == '') {
        Modules::run('database/_delete', $this->getTableName(), array('gardu_induk' => $data->form->gardu_induk));

        if (!empty($file)) {
          if (isset($file['file_ttd'])) {
            $response_upload = $this->uploadData('file_ttd', 'ttd');
            if ($response_upload['is_valid']) {
              $push = [];
              $push['file_ttd'] = $file['file_ttd']['name'];
              Modules::run('database/_update', 'pegawai', $push, array('id' => $data->form->pegawai));
            } else {
              $is_save = false;
              $message = $response_upload['response'];
            }
          }
        }

        $id = Modules::run('database/_insert', $this->getTableName(), $post);

        $push = [];
        $push['email'] = $data->form->email;
        $push['pegawai_type'] = $data->form->pegawai_type;
        $push['posisi'] = $data->form->jabatan;
        $push['posisi_lengkap'] = $data->form->jabatan;
        Modules::run('database/_update', 'pegawai', $push, array('id' => $data->form->pegawai));

        /*USER  */
        if ($data->form->user_id == '') {
          $push = [];
          $push['username'] = $data->form->email;
          $push['password'] = $data->form->password;
          $push['pegawai'] = $data->form->pegawai;
          $push['deleted'] = 0;
          $user_id = Modules::run('database/_insert', 'user', $push);
        } else {
          $push = [];
          $push['username'] = $data->form->email;
          $push['password'] = $data->form->password;
          $push['pegawai'] = $data->form->pegawai;
          Modules::run('database/_update', 'user', $push, array('id' => $data->form->user_id));
        }
        /*USER  */
      } else {
        //update   
        if (!empty($file)) {
          if (isset($file['file_ttd'])) {
            if ($data->form->file_str_ttd != $file['file_ttd']['name']) {
              $response_upload = $this->uploadData('file_ttd', 'ttd');
              if ($response_upload['is_valid']) {
                $push = [];
                $push['file_ttd'] = $file['file_ttd']['name'];
                Modules::run('database/_update', 'pegawai', $push, array('id' => $data->form->pegawai));
              } else {
                $is_save = false;
                $message = $response_upload['response'];
              }
            }
          }
        }

        Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

        $push = [];
        $push['email'] = $data->form->email;
        $push['pegawai_type'] = $data->form->pegawai_type;
        $push['posisi'] = $data->form->jabatan;
        $push['posisi_lengkap'] = $data->form->jabatan;
        Modules::run('database/_update', 'pegawai', $push, array('id' => $data->form->pegawai));

        /*USER  */
        if ($data->form->user_id == '') {
          $push = [];
          $push['username'] = $data->form->email;
          $push['password'] = $data->form->password;
          $push['pegawai'] = $data->form->pegawai;
          $push['deleted'] = 0;
          $user_id = Modules::run('database/_insert', 'user', $push);
        } else {
          $push = [];
          $push['username'] = $data->form->email;
          $push['password'] = $data->form->password;
          $push['pegawai'] = $data->form->pegawai;
          Modules::run('database/_update', 'user', $push, array('id' => $data->form->user_id));
        }
        /*USER  */
      }
      if ($is_save) {
        $is_valid = true;
      }
      if ($is_valid) {
        $this->db->trans_commit();
      } else {
        $this->db->trans_rollback();
      }
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
  }

  public function search($keyword)
  {
    $this->segment = 4;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;
    $keyword = urldecode($keyword);

    $data['keyword'] = $keyword;
    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Gardu_induk";
    $data['title_content'] = 'Data Gardu_induk';
    $content = $this->getDataSpvGi($keyword);
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function delete($id)
  {
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid));
  }

  public function uploadData($name_of_field, $dir)
  {
    $config['upload_path'] = 'files/berkas/' . $dir . '/';
    $config['allowed_types'] = 'png|jpg|jpeg';
    $config['max_size'] = '1000';
    $config['max_width'] = '2000';
    $config['max_height'] = '2000';

    $this->load->library('upload', $config);

    $is_valid = false;
    if (!$this->upload->do_upload($name_of_field)) {
      $response = $this->upload->display_errors();
    } else {
      $response = $this->upload->data();
      $is_valid = true;
    }

    return array(
      'is_valid' => $is_valid,
      'response' => $response
    );
  }

  public function showLogo()
  {
    $foto = str_replace(' ', '_', $this->input->post('foto'));
    $data['foto'] = $foto;
    $data['dir'] = $_POST['dir'];
    echo $this->load->view('foto', $data, true);
  }
}
