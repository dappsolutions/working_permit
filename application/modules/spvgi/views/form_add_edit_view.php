<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='user_id' class='form-control' value='<?php echo isset($user_id) ? $user_id : '' ?>' />

<div class="row">
	<div class="col-md-12">
		<!-- Horizontal Form -->
		<div class="box box-info padding-16">
			<!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
			<div class="box-header with-border" style="margin-top: 12px;">
				<h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Gardu Induk</label>
						<div class="col-sm-4">
							<select class="form-control required" error="Gardu Induk" id="gardu_induk" style="width: 100%;">
								<option value="">Pilih Gardu Induk</option>
								<?php if (!empty($list_gardu)) { ?>
									<?php foreach ($list_gardu as $value) { ?>
										<?php $selected = '' ?>
										<?php if (isset($gardu_induk)) { ?>
											<?php $selected = $gardu_induk == $value['id'] ? 'selected' : '' ?>
										<?php } ?>
										<option <?php echo $selected ?> value="<?php echo $value['id'] ?>">
											<?php echo $value['nama_gardu'] ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Pegawai</label>
						<div class="col-sm-4">
							<select class="form-control required" error="Pegawai" id="pegawai" style="width: 100%;">
								<option value="">Pilih Pegawai</option>
								<?php if (!empty($list_pegawai)) { ?>
									<?php foreach ($list_pegawai as $value) { ?>
										<?php $selected = '' ?>
										<?php if (isset($pegawai)) { ?>
											<?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
										<?php } ?>
										<option <?php echo $selected ?> value="<?php echo $value['id'] ?>">
											<?php echo $value['nama'] ?> - <?php echo $value['nip'] ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>

					<?php $hidden = ""; ?>
					<?php $hidden = isset($file_ttd) ? 'hidden' : '' ?>
					<div class="form-group <?php echo $hidden ?>" id="file_ttd_input">
						<label for="" class="col-sm-2 control-label">TTD</label>
						<div class="col-sm-4">
							<input type="file" id="file_ttd" class="form-control" onchange="SpvGi.checkFile(this)">
						</div>
					</div>

					<?php $hidden = ""; ?>
					<?php $hidden = isset($file_ttd) ? '' : 'hidden' ?>
					<div class="form-group <?php echo $hidden ?>" id="detail_file_ttd">
						<label for="" class="col-sm-2 control-label">File TTD</label>
						<div class="col-sm-4">
							<div class="input-group">
								<input disabled type="text" id="file_str_ttd" class="form-control" value="<?php echo isset($file_ttd) ? $file_ttd : '' ?>">
								<span class="input-group-addon">
									<i class="fa fa-image hover-content" file="<?php echo isset($file_ttd) ? $file_ttd : '' ?>" onclick="SpvGi.showLogo(this, event, 'ttd')"></i>
								</span>
								<span class="input-group-addon">
									<i class="fa fa-close hover-content" onclick="SpvGi.gantiFile(this, event, 'ttd')"></i>
								</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-4">
							<input type="text" class="required form-control" error="Email" id="email" value="<?php echo isset($email) ? $email : '' ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-4">
							<input type="text" class="required form-control" error="Password" id="password" value="<?php echo isset($password) ? $password : '' ?>">
						</div>
					</div>
					
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Jabatan</label>
						<div class="col-sm-4">
							<input type="text" class="required form-control" error="Jabatan" id="jabatan" value="<?php echo isset($posisi) ? $posisi : '' ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Departemen</label>
						<div class="col-sm-4">
							<select class="form-control required" error="Departemen" id="pegawai_type" style="width: 100%;">
								<option value="">Pilih Departemen</option>
								<?php if (!empty($list_type)) { ?>
									<?php foreach ($list_type as $value) { ?>
										<?php $selected = '' ?>
										<?php if (isset($pegawai_type)) { ?>
											<?php $selected = $pegawai_type == $value['id'] ? 'selected' : '' ?>
										<?php } ?>
										<option <?php echo $selected ?> value="<?php echo $value['id'] ?>">
											<?php echo $value['tipe'] ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" class="btn btn-default" onclick="SpvGi.back()">Cancel</button>
					<button type="submit" class="btn btn-success pull-right" onclick="SpvGi.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
				</div>
				<!-- /.box-footer -->
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>