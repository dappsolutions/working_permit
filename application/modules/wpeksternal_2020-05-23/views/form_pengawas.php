<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Pengawas</u></h4>
 <hr/>   

 <div class="form-group">
	<label for="" class='text-danger'>Format File K3 : K3_NAMA_PEMILIK</label> 
	<br>
	<label for="" class='text-success'>CONTOH :</label> 
	<br>
	<label for="" class='text-success'>K3_RUDI_SETIANTO</label> 
  </div>
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_penganggung_jawab">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Nama</th>
      <th>Jabatan</th>
      <th>File Sertifikat K3</th>
      <th class="text-center">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_tj)) { ?>
      <?php if (!empty($list_tj)) { ?>
       <?php foreach ($list_tj as $value) { ?>
        <tr data_id="<?php echo $value['id'] ?>">
         <td>
          <input type="text" value="<?php echo $value['nama'] ?>" id="nama" class="form-control required" error="Nama"/>
         </td>
         <td>
          <input type="text" value="<?php echo $value['jabatan'] ?>" id="jabatan_pengawas" class="form-control required" error="Jabatan"/>
         </td>
         <td>
          <?php $hidden = ""; ?>
          <?php $hidden = $value['file_sk3'] != '' ? '' : 'hidden' ?>
          <div class="form-group <?php echo $hidden ?>" id='detail_file_sk3'>
           <div class="input-group">
            <input disabled type="text" id="file_str" class="form-control" value="<?php echo $value['file_sk3'] ?>">
            <span class="input-group-addon">
             <i class="fa fa-file-pdf-o hover-content" file="<?php echo $value['file_sk3'] ?>" 
                onclick="WpEksternal.showLogo(this, event, 'sk3')"></i>         
            </span>
            <span class="input-group-addon">
             <i class="fa fa-close hover-content"
                onclick="WpEksternal.gantiFileItem(this, event)"></i>
            </span>
           </div> 
          </div>
          <?php $hidden = ""; ?>
          <?php $hidden = $value['file_sk3'] == '' ? '' : 'hidden' ?>
          <div class="<?php echo $hidden ?>" id="file_input_sk3">
           <input type="file" id="file_sk3" class="form-control" onchange="WpEksternal.checkFile(this)">
          </div>          
         </td>
         <td class="text-center">
          <i class="fa fa-trash fa-lg hover-content" onclick="WpEksternal.removePenanggunJawab(this)"></i>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?> 
     <?php $required = isset($list_tj) ? '' : 'required' ?>
     <tr data_id="">
      <td>
       <input type="text" value="" id="nama" class="form-control <?php echo $required ?>" error="Nama"/>
      </td>
      <td>
       <input type="text" value="" id="jabatan_pengawas" class="form-control <?php echo $required ?>" error="Jabatan"/>
      </td>
      <td>
       <input type="file" id="file_sk3" class="form-control" onchange="WpEksternal.checkFile(this)">
      </td>
      <td class="text-center">
       <i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addPenanggunJawab(this)"></i>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>
