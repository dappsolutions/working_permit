<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Pelaksana</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_pelaksana">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Nama</th>
      <th class="text-center">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_pelaksana)) { ?>
      <?php if (!empty($list_pelaksana)) { ?>
       <?php foreach ($list_pelaksana as $value) { ?>
        <tr data_id="<?php echo $value['id'] ?>">
         <td>
          <input type="text" value="<?php echo $value['nama'] ?>" id="nama" class="form-control required" error="Nama"/>
         </td>
         <td class="text-center">
          <i class="fa fa-trash fa-lg hover-content" onclick="WpEksternal.removePelaksana(this)"></i>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>
     <?php $required = isset($list_pelaksana) ? '' : 'required' ?>
     <tr data_id="">
      <td>
       <input type="text" value="" id="nama" class="form-control <?php echo $required ?>" error="Nama"/>
      </td>
      <td class="text-center">
       <i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addPelaksana(this)"></i>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>