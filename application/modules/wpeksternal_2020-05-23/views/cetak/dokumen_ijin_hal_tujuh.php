<html>
  <head>
    <title></title>    
    <style>
      #_wrapper{
        width: 100%;        
        margin: 0 auto;               
      }
      
      #_content{
        border: 1px solid #999;
        max-width: 100%;
        text-align: center;        
      }
      
      #_top-content{
        width: 95%;
        max-width: 95%;                
        margin: 1% auto;        
      }      
            
      #_judul{
        font-size: 100%;
        font-family: arial;
        font-weight: bold;
      }
      
      h3{
        margin: 0;
        font-size: 100%;
        font-family: arial;
      }     
      
      #_data{
        font-family: arial;
        font-size: 12px;
      }
      
      table{
        border-collapse: collapse;
        border: 1px solid black;
      }      
      
      tr:nth-child(1) > td:nth-child(5){         
        border-right: 1px solid black;
      }
      
      tr:nth-child(2) > td:nth-child(1){
        border-top: 1px solid black;        
      }
      
      tr:nth-child(2) > td:nth-child(2), 
      tr:nth-child(2) > td:nth-child(3){        
        border: 1px solid black;
      }
      
      tr:nth-child(3) > td:nth-child(1), 
      tr:nth-child(3) > td:nth-child(2){        
        border: 1px solid black;
      }
      
      tr:nth-child(4) > td:nth-child(1), 
      tr:nth-child(4) > td:nth-child(2){        
        border: 1px solid black;
      }
      
      tr:nth-child(5) > td:nth-child(1), 
      tr:nth-child(5) > td:nth-child(2){        
        border: 1px solid black;
      }
      
      #_surat{
        width: 45%;        
        margin: 0 auto;
        font-family: tahoma;        
      }
      
      #_form{                
        width: 15%;
        font-size: 12px;
        text-align: left;
        margin-left: 80%;
        margin-right:2%;
        padding: 0.5%;
        border: 1px solid black;        
      }
      
      #no{
        margin-top: 1%;        
        font-family: arial;
      }
      
      #_isi-content{
        text-align: left;
        margin-left: 2%;
        font-size: 12px;
        font-family: tahoma;
      }
      
      #_center-content{
        text-align: left;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;        
        border:1px solid black;
        padding-top: 1.5%;
        padding-left: 1%;        
        padding-bottom: 1.5%;
        font-family: tahoma;
        font-size: 12px;
      }
      
      #_table-content{
        text-align: left;         
        font-family: tahoma;
        margin-top: 2%;
        margin-left: 2%;
        margin-right: 2%;
      }
    </style>
  </head>  
  <body>
    <?php foreach($data->result() as $row){ ?>
    <?php            
    if($row->level_id == 1){
      $level = 'INTERNAL';
    }else{
      $level = 'EKSTERNAL';
    }
    ?>
    <div id="_wrapper">
      <div id="_content">
        <div id="_top-content">          
          <table style="width: 100%;max-width: 100%;">
              <tr>
                <td><img src="<?php echo base_url().'files/img/_logo.png' ?>"></td>                                                    
                <td>                  
                  <h3>&nbsp;PT PLN (PERSERO)</h3>
                  <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
                </td>                                          
                <td colspan="70"></td>
                <td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url().'files/img/small_smk3.png' ?>"><img src="<?php echo base_url().'files/img/18001.png' ?>"></td>
              </tr>                
              <tr>
                <td id="_judul" colspan="60" rowspan="4">
                  <center>
                    <label>
                      FORMULIR IDENTIFIKASI BAHAYA, PENILAIAN DAN PENGENDALIAN RESIKO (IBPPR)
                    </label>
                  </center>                  
                </td>                
                <td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
                <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dokumen['no_dokumen'] ?></label></td>
              </tr>
              <tr>                                
                <td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
                <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dokumen['edisi'] ?></label></td>
              </tr>
              <tr>                                
                <td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
                <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo date('d F Y', strtotime($dokumen['berlaku_efektif'])) ?></label></td>
              </tr>               
              <tr>                       
                <td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
                <td id="_data" colspan="8" style="border: 1px solid black;"><label>7 dari 8</label></td>
              </tr>
            </table>                                                                               
        </div>                
        <div><hr/></div>         
        <div style="font-family: tahoma;margin-left: 2%;text-align: left;font-size: 14px;">          
          <b>LOKASI : <?php echo $lokasi_kerja_fix ?></b>
        </div>
        <div style="height: 40%;margin-left: 2%;margin-right: 2%;">
          <table style="width: 100%;">
            <tr>
              <td rowspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;width: 5%;"><b>No</b></td>
              <td rowspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;width: 15%;"><b>Kegiatan / Fasilitas</b></td>
              <td colspan="3" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;width: 30%;"><b>Potensi Bahaya</b></td>
              <td rowspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;width: 8%;"><b>Resiko</b></td>
              <td colspan="5" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;width: 30%;"><b>Penilaian</b></td>
              <td rowspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;width: 12%;"><b>Pengendalian</b></td>
            </tr>
            <tr>                                          
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Sumber</b></td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Mekanisme</b></td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Target</b></td>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Akibat (a)</b></td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Paparan (b)</b></td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Peluang (c)</b></td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Nilai (axbxc)</b></td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;"><b>Tingkat Resiko</b></td>              
            </tr>
            <tr>
              <td rowspan="10" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td rowspan="10" style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
            <tr>              
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
              <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 14px;">&nbsp;</td>
            </tr>
          </table>
          <br/>          
          <table style="width: 100%;">
            <tr>
              <td style="border:1px solid black;text-align: center;width: 40%;font-size: 14px;"><b>Menyetujui</b></td>
              <td colspan="2" style="padding-left: 1%;;border:1px solid black;text-align: left;width: 60%;font-size: 14px;">Catatan : 	Identifikasi Potensi Bahaya ini dibuat dengan sebenar-benarnya dan telah di sampaikan kepada 
	semua pekerja melalui safety breafing / Tool box meeting.
              </td>              
            </tr>
            <tr>
              <td rowspan="7" style="border:1px solid black;text-align: center;vertical-align: bottom;font-size: 14px;">
                <b>(<?php // echo $spv_gi['spv']  
                echo $ttd_spv['nama'];
                ?>)</b>
                <br/>
                <!--SPV GITET <?php //echo $lokasi_kerja_fix ?>-->
              </td>
              <td colspan="2" style="border:1px solid black;text-align: right;padding-right: 2%;font-size: 12px;"><b><?php echo $row->kota.', '.date('d F Y') ?></b></td>              
            </tr>
            <tr>              
              <td  style="border:1px solid black;text-align: center;font-size: 14px;"><b>Dievaluasi Oleh</b></td>
              <td style="border:1px solid black;text-align: center;font-size: 14px;"><b>Dibuat Oleh</b></td>
            </tr>
            <tr>              
              <td rowspan="5"  style="border:1px solid black;text-align: center;padding-top: 6%;font-size: 14px;">(<?php echo $row->pengawas_k3 ?>)</td>
              <td rowspan="5" style="border:1px solid black;text-align: center;padding-top: 6%;font-size: 14px;">(<?php echo $row->pengawas_pekerjaan ?>)</td>
            </tr>
          </table>          
        </div>    
        <div>&nbsp;</div>
    </div>
    <?php }?>
  </body>
</html>