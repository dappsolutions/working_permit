<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box padding-16">
					<!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-4">
								<div class="input-group">
									<input type="text" class="form-control" id="tanggal" readonly placeholder="Tanggal">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-4">
								<button id="tampil" class="btn btn-primary" onclick="LapSimson.tampilkan(this)">Tampilkan</button>
								<a class="btn btn-success" download="<?php echo 'Laporan Simson' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_content', 'Laporan Simson');">Export</a>
							</div>
						</div>
						<div class="divider"></div>
						<br />

						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="tb_content">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>Action</th>
												<th>No</th>
												<th>No Simson</th>
												<th>No Pengajuan Wp</th>
												<th>Tanggal Wp</th>
												<th>Tanggal Pekerjaan</th>
												<th>Tanggal Awal Pelaksanaan</th>
												<th>Tanggal Akhir Pelaksanaan</th>
												<th>Kategori Tempat Pekerjaan</th>
												<th>Lokasi Pekerjaan</th>
												<th>Uraian Pekerjaan</th>
												<th>Tujuan</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											<?php if (!empty($content)) { ?>
												<?php $no = 1; ?>
												<?php foreach ($content as $value) { ?>
													<?php $bg_color = ''; ?>
													<?php if ($value['status'] == 'DRAFT') { ?>
														<?php $bg_color = 'bg-warning'; ?>
													<?php } ?>
													<?php if ($value['status'] == 'REJECTED') { ?>
														<?php $bg_color = 'bg-danger'; ?>
													<?php } ?>
													<?php if ($value['status'] == 'APPROVED') { ?>
														<?php $bg_color = 'bg-success'; ?>
													<?php } ?>
													<tr class="<?php echo $bg_color ?>">
														<td class="text-center">
															<a href="<?php echo base_url() ?>simson/sidak/showDetailPengajuan?permit=<?php echo $value['id'] ?>&no_wp=<?php echo $value['no_wp'] ?>" onclick="LapSimson.showPengajuan(this,event)"><i class="fa fa-file-text grey-text  hover"></i></a>
														</td>
														<td><b><?php echo $no++ ?></b></td>
														<td><b><?php echo $value['no_trans_swa'] ?></b></td>
														<td><b><?php echo $value['no_wp'] ?></b></td>
														<td><b><?php echo $value['tanggal_wp'] ?></b></td>
														<td><b><?php echo $value['tgl_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['tgl_awal'] ?></b></td>
														<td><b><?php echo $value['tgl_akhir'] ?></b></td>
														<td><b><?php echo $value['jenis_place'] ?></b></td>
														<td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['nama_upt'] ?></b></td>
														<?php if ($value['status'] == 'DRAFT') { ?>
															<td><b><?php echo $value['status'] ?></b></td>
														<?php } else { ?>
															<td><b><?php echo $value['status'] ?></b><br />[Level : <?php echo $value['level'] ?>]</td>
														<?php } ?>
													</tr>
												<?php } ?>
											<?php } else { ?>
												<tr>
													<td colspan="20" class="text-center">Tidak ada data ditemukan</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>