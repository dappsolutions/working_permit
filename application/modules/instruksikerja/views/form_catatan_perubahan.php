<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_catatan">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Edisi / Revisi</th>
							<th>Tanggal</th>
							<th>Halaman</th>
							<th>Paragraf</th>
							<th>Alasan</th>
							<th>Disetujui Oleh</th>
							<th>Jabatan</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_catatan)) { ?>
							<?php if (!empty($data_catatan)) { ?>
								<?php foreach ($data_catatan as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input type="text" value="<?php echo $value['edisi'] ?>" id="edisi" class="form-control required" error="Edisi" />
										</td>
										<td>
											<input type="text" readonly value="<?php echo $value['tanggal'] ?>" id="tanggal" class="form-control required" error="Tanggal" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['halaman'] ?>" id="halaman" class="form-control required" error="Halaman" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['paragraf'] ?>" id="paragraf" class="form-control required" error="Paragraf" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['alasan'] == '' ? 'Tidak ada alasan' : $value['alasan'] ?>" id="alasan" class="form-control required" error="Asalan" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['disetujui_oleh'] ?>" id="disetujui_oleh" class="form-control required" error="Disetujui oleh" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['jabatan'] ?>" id="jabatan" class="form-control required" error="Jabatan" />
										</td>
										<td class="text-center">
											<i class="fa fa-trash fa-lg hover-content" onclick="InstruksiKerja.removeCatatan(this)"></i>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<tr data_id="">
							<td>
								<input type="text" value="" id="edisi" class="form-control required" error="Edisi" />
							</td>
							<td>
								<input type="text" readonly value="<?php echo isset($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : date('Y-m-d') ?>" id="tanggal" class="form-control required" error="Tanggal" />
							</td>
							<td>
								<input type="text" value="" id="halaman" class="form-control required" error="Halaman" />
							</td>
							<td>
								<input type="text" value="" id="paragraf" class="form-control required" error="Paragraf" />
							</td>
							<td>
								<input type="text" value="" id="alasan" class="form-control required" error="Asalan" />
							</td>
							<td>
								<input type="text" value="" id="disetujui_oleh" class="form-control required" error="Disetujui oleh" />
							</td>
							<td>
								<input type="text" value="" id="jabatan" class="form-control required" error="Jabatan" />
							</td>
							<td class="text-center">
								<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addCatatan(this)"></i>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" state='catatan' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>
	</div>
</div>
