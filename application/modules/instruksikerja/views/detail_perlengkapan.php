<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<h4>Perlengkapan K3</h4>
			<div class="table-responsive">
				<table class="table table-bordered" id="table_perlengkapan">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Nama Alat</th>
							<th>Satuan</th>
							<th>Volume</th>
							<th>Penanggung Jawab</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_perlengkapan)) { ?>
							<?php if (!empty($data_perlengkapan)) { ?>
								<?php foreach ($data_perlengkapan as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input readonly type="text" value="<?php echo $value['nama_alat'] ?>" id="nama_alat" class="form-control required" error="Nama Alat" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['satuan'] ?>" id="satuan" class="form-control required" error="Satuan" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['volume'] ?>" id="volume" class="form-control required" error="Volume" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['penanggung_jawab'] ?>" id="penanggung_jawab" class="form-control required" error="Penanggung Jawab" />
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<h4>Alat Kerja</h4>
			<div class="table-responsive">
				<table class="table table-bordered" id="table_alat_kerja">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Nama Alat</th>
							<th>Satuan</th>
							<th>Volume</th>
							<th>Penanggung Jawab</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_alat_kerja)) { ?>
							<?php if (!empty($data_alat_kerja)) { ?>
								<?php foreach ($data_alat_kerja as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input readonly type="text" value="<?php echo $value['nama_alat'] ?>" id="nama_alat" class="form-control required" error="Nama Alat" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['satuan'] ?>" id="satuan" class="form-control required" error="Satuan" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['volume'] ?>" id="volume" class="form-control required" error="Volume" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['penanggung_jawab'] ?>" id="penanggung_jawab" class="form-control required" error="Penanggung Jawab" />
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<h4>Material</h4>
			<div class="table-responsive">
				<table class="table table-bordered" id="table_material">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Nama Alat</th>
							<th>Satuan</th>
							<th>Volume</th>
							<th>Penanggung Jawab</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_material)) { ?>
							<?php if (!empty($data_material)) { ?>
								<?php foreach ($data_material as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input readonly type="text" value="<?php echo $value['nama_alat'] ?>" id="nama_alat" class="form-control required" error="Nama Alat" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['satuan'] ?>" id="satuan" class="form-control required" error="Satuan" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['volume'] ?>" id="volume" class="form-control required" error="Volume" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['penanggung_jawab'] ?>" id="penanggung_jawab" class="form-control required" error="Penanggung Jawab" />
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
