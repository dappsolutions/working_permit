<div class="col-md-12">
	<div class="box-body" style="margin-top: -12px;">
		<div class="table-responsive">
			<table class="table table-bordered" id="tb_lampiran">
				<thead>
					<tr class="bg-primary-light text-white">
						<th>File</th>
						<th class="text-center">Action</th>
					</tr>
				</thead>
				<tbody>
					<?php if (isset($data_lampiran)) { ?>
						<?php if (!empty($data_lampiran)) { ?>
							<?php foreach ($data_lampiran as $value) { ?>
								<td>
									<?php $hidden = ""; ?>
									<?php $hidden = $value['file'] != '' ? '' : 'hidden' ?>
									<div class="form-group <?php echo $hidden ?>" id='detail_file'>
										<div class="input-group">
											<input disabled type="text" id="file_str" class="form-control" value="<?php echo $value['file'] ?>">
											<span class="input-group-addon">
												<i class="fa fa-file-pdf-o hover-content" file="<?php echo $value['file'] ?>" onclick="InstruksiKerja.showLogo(this, event)"></i>
											</span>
											<span class="input-group-addon">
												<i class="fa fa-close hover-content" onclick="InstruksiKerja.gantiFileItem(this, event)"></i>
											</span>
										</div>
									</div>
									<?php $hidden = ""; ?>
									<?php $hidden = $value['file'] == '' ? '' : 'hidden' ?>
									<div class="<?php echo $hidden ?>" id="file_input_file">
										<input type="file" id="file" class="form-control" onchange="InstruksiKerja.checkFile(this)">
									</div>
								</td>
								<td class="text-center">
									<i class="fa fa-trash fa-lg hover-content" onclick="InstruksiKerja.removeLampiran(this)"></i>
								</td>
								</tr>
							<?php } ?>
						<?php } ?>
					<?php } ?>
					<?php $required = isset($data_lampiran) ? '' : 'required' ?>
					<tr data_id="">
						<td>
							<input type="file" id="file" class="form-control" onchange="InstruksiKerja.checkFile(this)">
						</td>
						<td class="text-center">
							<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addLampiran(this)"></i>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>

	</div>
	<!-- /.box-footer -->
</div>
