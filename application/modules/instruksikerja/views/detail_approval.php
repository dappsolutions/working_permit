<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_approval">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>NIP</th>
							<th>NAMA</th>
							<th>JABATAN</th>
							<th>STATUS</th>
							<th>WAKTU</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_approval)) { ?>
							<?php if (!empty($data_approval)) { ?>
								<?php foreach ($data_approval as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<?php echo $value['nip'] ?>
										</td>
										<td>
											<?php echo $value['pegawai_detail']['nama'] ?>
										</td>
										<td>
											<?php echo $value['pegawai_detail']['posisi'] ?>
										</td>
										<td>
											<?php if ($value['remarks'] != '') { ?>
												<?php echo $value['updated_at'] == '' ? 'Menunggu' : 'Ditolak' ?>
												<br>
												<p style="color: red;"><?php echo $value['remarks'] ?></p>
											<?php } else { ?>
												<?php echo $value['updated_at'] == '' ? 'Menunggu' : 'Disetujui' ?>
												<br>
											<?php } ?>
											<?php if ($nip_pegawai_akses != '') { ?>
												<?php if ($value['updated_at'] == '') { ?>
													<?php if ($value['nip'] == $nip_pegawai_akses) { ?>
														<div class="row">
															<div class="col-md-6">
																<button type="submit" class="btn btn-success pull-right" nip="<?php echo $value['nip'] ?>" onclick="InstruksiKerja.approve(this, event)"><i class="fa fa-check"></i>&nbsp;Approve</button>
															</div>
															<div class="col-md-6">
																<button type="submit" class="btn btn-danger" nip="<?php echo $value['nip'] ?>" onclick="InstruksiKerja.reject(this, event)">
																	<i class="fa fa-close"></i>&nbsp;Reject
																</button>
															</div>
														</div>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										</td>
										<td>
											<?php echo $value['updated_at'] ?>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
