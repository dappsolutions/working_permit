<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_analisa_resiko">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Aktifitas</th>
							<th>Risiko</th>
							<th>Dampak</th>
							<th>Mitigasi</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_analisa_resiko)) { ?>
							<?php if (!empty($data_analisa_resiko)) { ?>
								<?php foreach ($data_analisa_resiko as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input readonly type="text" value="<?php echo $value['aktifitas'] ?>" id="aktifitas" class="form-control required" error="Aktifitas" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['resiko'] ?>" id="resiko" class="form-control required" error="Resiko" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['dampak'] ?>" id="dampak" class="form-control required" error="Dampak" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['mitigasi'] ?>" id="mitigasi" class="form-control required" error="Mitigasi" />
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
