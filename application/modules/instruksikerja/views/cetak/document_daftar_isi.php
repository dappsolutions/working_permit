<html>
<style>
 #_wrapper {
  width: 100%;
  margin: 0 auto;
 }

 #_content {
  border: 0.2px solid #999;
  max-width: 100%;
  padding: 16px;
  font-size: 10px;
 }

 #_top-content {
  width: 95%;
  max-width: 95%;
  margin: 1% auto;
 }

 #_judul {
  font-size: 100%;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: bold;
 }

 h3 {
  margin: 0;
  font-size: 100%;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_data {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
 }

 /* table {
  border-collapse: collapse;
  border: 1px solid black;
 }

 tr:nth-child(1)>td:nth-child(5) {
  border-right: 1px solid black;
 }

 tr:nth-child(2)>td:nth-child(1) {
  border-top: 1px solid black;
 }

 tr:nth-child(2)>td:nth-child(2),
 tr:nth-child(2)>td:nth-child(3) {
  border: 1px solid black;
 }

 tr:nth-child(3)>td:nth-child(1),
 tr:nth-child(3)>td:nth-child(2) {
  border: 1px solid black;
 }

 tr:nth-child(4)>td:nth-child(1),
 tr:nth-child(4)>td:nth-child(2) {
  border: 1px solid black;
 }

 tr:nth-child(5)>td:nth-child(1),
 tr:nth-child(5)>td:nth-child(2) {
  border: 1px solid black;
 } */

 #_surat {
  width: 45%;
  margin: 0 auto;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: bold;
 }

 #_form {
  width: 15%;
  font-size: 12px;
  text-align: left;
  margin-left: 80%;
  margin-right: 2%;
  padding: 0.5%;
  border: 1px solid black;
 }

 #no {
  margin-top: 1%;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_isi-content {
  text-align: left;
  margin-left: 2%;
  font-size: 12px;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_center-content {
  text-align: left;
  margin-top: 2%;
  margin-left: 2%;
  margin-right: 2%;
  border: 1px solid black;
  padding-top: 1.5%;
  padding-left: 1%;
  padding-bottom: 1.5%;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
 }

 #_table-content {
  text-align: left;
  font-family: Arial, Helvetica, sans-serif;
  margin-top: 2%;
  margin-left: 2%;
  margin-right: 2%;
 }

 /*!
 * froala_editor v4.0.19 (https://www.froala.com/wysiwyg-editor)
 * License https://froala.com/wysiwyg-editor/terms/
 * Copyright 2014-2023 Froala Labs
 */

 .image-container img:first-child {
  /* CSS styles for the first <img> tag */
  border: 2px solid #000;
  width: 200px;

 }

 p {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 10px;
 }

 .td {
  border: 1px solid #000;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 10px;
  padding: 8px;
 }

 .td_none{
  border: none;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 11px;
  padding: 8px;
 }
</style>
</head>

<body>
 <p style="text-align: center;font-size: 14px;font-weight: bold;">IV. DAFTAR ISI</p>
 <table style="width: 100%;border: none;">
  <tr>
   <td class="td_none">&nbsp;</td>
   <td class="td_none">URAIAN</td>
   <td class="td_none" style="">Halaman</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">I.</td>
   <td class="td_none">LEMBAR PENGESAHAN ............................................................................................................................................</td>
   <td class="td_none">2</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">II.</td>
   <td class="td_none">DAFTAR DISTRIBUSI ................................................................................................................................................</td>
   <td class="td_none">3</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">III.</td>
   <td class="td_none">CATATAN PERUBAHAN DOKUMEN ...........................................................................................................................</td>
   <td class="td_none">4</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">IV.</td>
   <td class="td_none">DAFTAR ISI ..............................................................................................................................................................</td>
   <td class="td_none">5</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">V.</td>
   <td class="td_none">ANALISA RISIKO .......................................................................................................................................................</td>
   <td class="td_none">5</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">VI.</td>
   <td class="td_none">ISI INSTRUKSI KERJA ................................................................................................................................................</td>
   <td class="td_none">6</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">1.	Tujuan .................................................................................................................................................................</td>
   <td class="td_none">6</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">2.	Ruang Lingkup .....................................................................................................................................................</td>
   <td class="td_none">6</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">3.	Referensi ..............................................................................................................................................................</td>
   <td class="td_none">6</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">4.	Definisi dan Istilah ................................................................................................................................................</td>
   <td class="td_none">6</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">5.	Peralatan kerja - perlengkapan K3 dan material ..................................................................................................</td>
   <td class="td_none">6</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a)	Alat Kerja ......................................................................................................................................................</td>
   <td class="td_none">7</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b)	Perlengkapan K3 ...........................................................................................................................................</td>
   <td class="td_none">8</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c)	Material .........................................................................................................................................................</td>
   <td class="td_none">9</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">6.	Pelaksanaan pekerjaan ..........................................................................................................................................</td>
   <td class="td_none">10</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A.	Langkah pelaksanaan tahap (Persiapan) ........................................................................................................</td>
   <td class="td_none">10</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B.	Pelaksanaan pekerjaan ...................................................................................................................................</td>
   <td class="td_none">11</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">7.	Dokumentasi ...........................................................................................................................................................</td>
   <td class="td_none">12</td>
  </tr>
  <tr style="border:none;">
   <td class="td_none">&nbsp;</td>
   <td class="td_none">8.	Lampiran .................................................................................................................................................................</td>
   <td class="td_none">13</td>
  </tr>
 </table>
</body>

</html>
