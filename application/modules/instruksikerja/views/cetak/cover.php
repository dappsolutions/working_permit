<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			margin: 0 auto;
			font-family: Arial, Helvetica, sans-serif;
		}

		#_int {
			margin: 2% auto;
			font-family: Arial, Helvetica, sans-serif;
		}


		#_bottom-content {
			font-family: Arial, Helvetica, sans-serif;
		}

		#_info-content {
			/* border: 1px solid black; */
			margin-left: 20%;
			margin-right: 10%;
			text-align: center;
			padding-left: 2%;
		}

		#_cover {
			margin-left: 3%;
		}

		h2 {
			margin: 0.5%;
		}
	</style>
</head>

<body>
	<div style="text-align: right;">-</div>
	<div id="_wrapper">
		<div id="_content">

			<div>&nbsp;</div>
			<div id="_top-content">
				<h2>SISTEM MANAJEMEN K3</h2>
			</div>
			<div id="_cover">
				<br>
				<br>
				<br>
				<img src="<?php echo base_url() ?>files/img/smk3.png" height="100">
				<br />
				<br />
				<h3 style="font-size: 16px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;"><b>INSTRUKSI KERJA</b></h3>
				<h3 style="font-size: 16px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;"><b><?php echo $pekerjaan ?></b></h3>
			</div>
			<div>&nbsp;</div>
			<div>&nbsp;</div>
			<br>
			<br>
			<div id="_info-content">
				<table style="font-size: 16px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;width: 100%;">
					<tr>
						<td style="padding: 3px;">NO. DOK</td>
						<td style="padding: 3px;">:</td>
						<td style="padding: 3px;"><?php echo $no_document ?></td>
					</tr>
					<tr>
						<td style="padding: 3px;">EDISI</td>
						<td style="padding: 3px;">:</td>
						<td style="padding: 3px;">01</td>
					</tr>
					<tr>
						<td style="padding: 3px;">REVISI</td>
						<td style="padding: 3px;">:</td>
						<td style="padding: 3px;">00</td>
					</tr>
					<tr>
						<td style="padding: 3px;">TANGGAL</td>
						<td style="padding: 3px;">:</td>
						<td style="padding: 3px;"><?php echo $tanggal_instruksi ?></td>
					</tr>
				</table>
				<div>
					&nbsp;
				</div>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<div id="_bottom-content">
				<?php if ($instruksi->file_logo_vendor != '') { ?>
					<img src="<?php echo base_url() . $instruksi->path_logo_vendor . '/' . $instruksi->file_logo_vendor ?>" height="105" width="70">
				<?php } else { ?>
					<img src="<?php echo base_url() . 'files/img/_logo.png' ?>" height="105" width="70">
				<?php } ?>
				<h3>PT PLN (PERSERO)</h3>
				<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
			</div>
		</div>
	</div>
	<div style="text-align: right;">
	</div>
</body>

</html>
