
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
</head>
<body>
<table style="width: 100%;max-width: 100%;border-collapse: collapse;">
 <tr>
  <td style="border-top:0.2px solid #000;border-left:0.2px solid #000;border-bottom:0.2px solid #000;;padding:16px;"><img style="height: 55px;width: 35px;" src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
  <td style="border-top:0.2px solid #000;border-bottom:0.2px solid #000;padding:16px;text-align: justify;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;">
   <label for="" >PT PLN (PERSERO) UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</label>
  </td>
  <td style="border-top:0.2px solid #000;padding-left: 16px;" colspan="7"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"></td>
  <td style="border-right:0.2px solid black;border-top:0.2px solid #000;"><img style="" src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
 </tr>
 <tr>
  <td id="" style="font-size: 14px;
   border-left:0.2px solid #000;
   border-bottom:0.2px solid #000;
			font-family: Arial, Helvetica, sans-serif;
			font-weight: bold;text-align:center;" colspan="8" rowspan="4">
    <label>
     INSTRUKSI KERJA 
    </label>
    <br>
    <?php echo strtoupper($instruksi->nama_pekerjaan_kontrak) ?>
  </td>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;"><label>No. Dokumen</label></td>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;border-right: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;">
   <label><?php echo $no_document ?></label>
  </td>
 </tr>
 <tr>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;"><label>Edisi / Revisi</label></td>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;border-right: 0.2px solid black;">
   <label>01/00</label>
  </td>
 </tr>
 <tr>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;"><label>Berlaku Efektif</label></td>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;border-right: 0.2px solid black;border-bottom: 0.2px solid black;">
   <label>28 Februari 2023</label>
  </td>
 </tr>
 <tr>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;border-bottom: 0.2px solid black;"><label>Halaman</label></td>
  <td id="" style="border-left: 0.2px solid black;border-top: 0.2px solid black;font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;padding:3px;border-right: 0.2px solid black;border-bottom: 0.2px solid black;"><label>{PAGENO} dari {nbpg}</label></td>
 </tr>
</table>
<br>
</body>
</html>
