<html>
<style>
 #_wrapper {
  width: 100%;
  margin: 0 auto;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_content {
  border: 0.2px solid #999;
  max-width: 100%;
  padding: 16px;
  font-size: 10px;
 }

 #_top-content {
  width: 95%;
  max-width: 95%;
  margin: 1% auto;
 }

 #_judul {
  font-size: 100%;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: bold;
 }

 h3 {
  margin: 0;
  font-size: 100%;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_data {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
 }

 table {
  border-collapse: collapse;
  border: 1px solid black;
 }

 tr:nth-child(1)>td:nth-child(5) {
  border-right: 1px solid black;
 }

 tr:nth-child(2)>td:nth-child(1) {
  border-top: 1px solid black;
 }

 tr:nth-child(2)>td:nth-child(2),
 tr:nth-child(2)>td:nth-child(3) {
  border: 1px solid black;
 }

 tr:nth-child(3)>td:nth-child(1),
 tr:nth-child(3)>td:nth-child(2) {
  border: 1px solid black;
 }

 tr:nth-child(4)>td:nth-child(1),
 tr:nth-child(4)>td:nth-child(2) {
  border: 1px solid black;
 }

 tr:nth-child(5)>td:nth-child(1),
 tr:nth-child(5)>td:nth-child(2) {
  border: 1px solid black;
 }

 #_surat {
  width: 45%;
  margin: 0 auto;
  font-family: Arial, Helvetica, sans-serif;
  font-weight: bold;
 }

 #_form {
  width: 15%;
  font-size: 12px;
  text-align: left;
  margin-left: 80%;
  margin-right: 2%;
  padding: 0.5%;
  border: 1px solid black;
 }

 #no {
  margin-top: 1%;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_isi-content {
  text-align: left;
  margin-left: 2%;
  font-size: 12px;
  font-family: Arial, Helvetica, sans-serif;
 }

 #_center-content {
  text-align: left;
  margin-top: 2%;
  margin-left: 2%;
  margin-right: 2%;
  border: 1px solid black;
  padding-top: 1.5%;
  padding-left: 1%;
  padding-bottom: 1.5%;
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
 }

 #_table-content {
  text-align: left;
  font-family: Arial, Helvetica, sans-serif;
  margin-top: 2%;
  margin-left: 2%;
  margin-right: 2%;
 }

 p {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
	}

	td{
		border: 1px solid #000;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
		padding: 3px;
	}

 /* .fr-view img {
  position: relative;
  max-width: 100%;
 }

 img.fr-draggable {
  user-select: none;
 }

 .fr-element img {
  cursor: pointer;
  padding: 0 1px;
 } */
</style>
</head>

<body>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br>
 <br>
 <?php echo $tahapan->tahap_persiapan ?>
</body>

</html>
