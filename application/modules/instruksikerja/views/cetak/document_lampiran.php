<html>
<style>
	#_wrapper {
		width: 100%;
		margin: 0 auto;
	}

	#_content {
		border: 0.2px solid #999;
		max-width: 100%;
		padding: 16px;
		font-size: 10px;
	}

	#_top-content {
		width: 95%;
		max-width: 95%;
		margin: 1% auto;
	}

	#_judul {
		font-size: 100%;
		font-family: Arial, Helvetica, sans-serif;
		font-weight: bold;
	}

	h3 {
		margin: 0;
		font-size: 100%;
		font-family: Arial, Helvetica, sans-serif;
	}

	#_data {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}

	table {
		border-collapse: collapse;
		border: 1px solid black;
	}

	tr:nth-child(1)>td:nth-child(5) {
		border-right: 1px solid black;
	}

	tr:nth-child(2)>td:nth-child(1) {
		border-top: 1px solid black;
	}

	tr:nth-child(2)>td:nth-child(2),
	tr:nth-child(2)>td:nth-child(3) {
		border: 1px solid black;
	}

	tr:nth-child(3)>td:nth-child(1),
	tr:nth-child(3)>td:nth-child(2) {
		border: 1px solid black;
	}

	tr:nth-child(4)>td:nth-child(1),
	tr:nth-child(4)>td:nth-child(2) {
		border: 1px solid black;
	}

	tr:nth-child(5)>td:nth-child(1),
	tr:nth-child(5)>td:nth-child(2) {
		border: 1px solid black;
	}

	#_surat {
		width: 45%;
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-weight: bold;
	}

	#_form {
		width: 15%;
		font-size: 12px;
		text-align: left;
		margin-left: 80%;
		margin-right: 2%;
		padding: 0.5%;
		border: 1px solid black;
	}

	#no {
		margin-top: 1%;
		font-family: Arial, Helvetica, sans-serif;
	}

	#_isi-content {
		text-align: left;
		margin-left: 2%;
		font-size: 12px;
		font-family: Arial, Helvetica, sans-serif;
	}

	#_center-content {
		text-align: left;
		margin-top: 2%;
		margin-left: 2%;
		margin-right: 2%;
		border: 1px solid black;
		padding-top: 1.5%;
		padding-left: 1%;
		padding-bottom: 1.5%;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}

	#_table-content {
		text-align: left;
		font-family: Arial, Helvetica, sans-serif;
		margin-top: 2%;
		margin-left: 2%;
		margin-right: 2%;
	}

	/*!
 * froala_editor v4.0.19 (https://www.froala.com/wysiwyg-editor)
 * License https://froala.com/wysiwyg-editor/terms/
 * Copyright 2014-2023 Froala Labs
 */

	.image-container img:first-child {
		/* CSS styles for the first <img> tag */
		border: 2px solid #000;
		width: 200px;

	}

	p {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
	}

	.td {
		border: 1px solid #000;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
		padding: 8px;
	}
</style>
</head>

<body>
	<!-- <table style="width: 100%;border: none;">
		<tr>
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">6.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">DOKUMENTASI</td>
		</tr>
	</table> -->

	<!-- <table style="width: 100%;border:none;"> -->
	<?php $no = 1; ?>
	<?php foreach ($documentasi as $key => $value) { ?>
		<!-- <tr style="border:none;">
			<td>&nbsp;</td>
				<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">
					<p><?php echo $no++ ?></p>
					<p><img src="<?php echo base_url() . $value['path'] . '/' . $value['file'] ?>" alt=""></p>
			</td>
			</tr> -->
	<?php } ?>
	<?php foreach ($data_dokumen as $key => $value) { ?>
		<!-- <tr style="border:none;">
				<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;"><?php echo $no++ ?>. </td>
				<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
					<?php echo trim($value['remarks']) ?>
				</td>
			</tr> -->
	<?php } ?>
	<!-- </table> -->
	<table style="width: 100%;border: none;">
		<tr>
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">6.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">DOKUMENTASI</td>
		</tr>
		<?php $no = 1; ?>
		<?php foreach ($data_dokumen as $key => $value) { ?>
			<tr style="border:none;">
				<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;"><?php echo $no++ ?>. </td>
				<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
					<?php echo trim($value['remarks']) ?>
				</td>
			</tr>
			<tr style="border: none;">
				<td colspan="2">
					<p><img src="<?php echo base_url() . $value['path'] . '/' . $value['file'] ?>" alt=""></p>
				</td>
			</tr>
		<?php } ?>
	</table>
	<br>
	<table style="width: 100%;border: none;">
		<tr>
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">7.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">LAMPIRAN</td>
		</tr>
		<?php $no = 1; ?>
		<?php foreach ($data_lampiran as $key => $value) { ?>
			<tr style="border:none;">
				<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;"><?php echo $no++ ?>. </td>
				<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;">
					<?php echo trim($value['remarks']) ?>
				</td>
			</tr>
			<tr style="border: none;">
				<td colspan="2">
					<p><img src="<?php echo base_url() . $value['path'] . '/' . $value['file'] ?>" alt=""></p>
				</td>
			</tr>
		<?php } ?>
	</table>
</body>

</html>
