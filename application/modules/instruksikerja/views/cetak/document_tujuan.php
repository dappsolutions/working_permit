<html>
<style>
	#_wrapper {
		width: 100%;
		margin: 0 auto;
	}

	#_content {
		border: 0.2px solid #999;
		max-width: 100%;
		padding: 16px;
		font-size: 11px;
	}

	#_top-content {
		width: 95%;
		max-width: 95%;
		margin: 1% auto;
	}

	#_judul {
		font-size: 100%;
		font-family: Arial, Helvetica, sans-serif;
		font-weight: bold;
	}

	h3 {
		margin: 0;
		font-size: 100%;
		font-family: Arial, Helvetica, sans-serif;
	}

	#_data {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12px;
	}

	table {
		border-collapse: collapse;
		border: 1px solid black;
	}

	tr:nth-child(1)>td:nth-child(5) {
		border-right: 1px solid black;
	}

	tr:nth-child(2)>td:nth-child(1) {
		border-top: 1px solid black;
	}

	tr:nth-child(2)>td:nth-child(2),
	tr:nth-child(2)>td:nth-child(3) {
		border: 1px solid black;
	}

	tr:nth-child(3)>td:nth-child(1),
	tr:nth-child(3)>td:nth-child(2) {
		border: 1px solid black;
	}

	tr:nth-child(4)>td:nth-child(1),
	tr:nth-child(4)>td:nth-child(2) {
		border: 1px solid black;
	}

	tr:nth-child(5)>td:nth-child(1),
	tr:nth-child(5)>td:nth-child(2) {
		border: 1px solid black;
	}

	#_surat {
		width: 45%;
		margin: 0 auto;
		font-family: Arial, Helvetica, sans-serif;
		font-weight: bold;
	}

	#_form {
		width: 15%;
		font-size: 11px;
		text-align: left;
		margin-left: 80%;
		margin-right: 2%;
		padding: 0.5%;
		border: 1px solid black;
	}

	#no {
		margin-top: 1%;
		font-family: Arial, Helvetica, sans-serif;
	}

	#_isi-content {
		text-align: left;
		margin-left: 2%;
		font-size: 12px;
		font-family: Arial, Helvetica, sans-serif;
	}

	#_center-content {
		text-align: left;
		margin-top: 2%;
		margin-left: 2%;
		margin-right: 2%;
		border: 1px solid black;
		padding-top: 1.5%;
		padding-left: 1%;
		padding-bottom: 1.5%;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}

	#_table-content {
		text-align: left;
		font-family: Arial, Helvetica, sans-serif;
		margin-top: 2%;
		margin-left: 2%;
		margin-right: 2%;
	}

	/*!
 * froala_editor v4.0.19 (https://www.froala.com/wysiwyg-editor)
 * License https://froala.com/wysiwyg-editor/terms/
 * Copyright 2014-2023 Froala Labs
 */

	.image-container img:first-child {
		/* CSS styles for the first <img> tag */
		border: 2px solid #000;
		width: 200px;

	}

	p {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
	}

	li {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
	}

	.td {
		border: 1px solid #000;
		font-family: Arial, Helvetica, sans-serif;
		font-size: 10px;
		padding: 8px;
	}
</style>
</head>

<body>
	<p style="text-align: left;font-size: 14px;font-weight: bold;">VI. ISI INSTRUKSI KERJA</p>
	<table style="width: 100%;border: none;">
		<tr>
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">1.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">TUJUAN</td>
		</tr>
		<tr style="border: none;">
			<td>&nbsp;</td>
			<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;text-align: justify;"><?php echo str_replace('Powered by', '', str_replace('Froala Editor', '', $tahapan->tujuan)) ?></td>
		</tr>
		<tr style="border: none;">
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">2.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">RUANG LINGKUP</td>
		</tr>
		<tr style="border: none;">
			<td>&nbsp;</td>
			<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;text-align: justify;"><?php echo str_replace('Powered by', '', str_replace('Froala Editor', '', $tahapan->ruang_lingkup)) ?></td>
		</tr>
		<tr style="border: none;">
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">3.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">REFERENSI</td>
		</tr>
		<tr style="border: none;">
			<td>&nbsp;</td>
			<td style="font-size: 12px;font-family: Arial, Helvetica, sans-serif;text-align: justify;"><?php echo str_replace('Powered by', '', str_replace('Froala Editor', '', $tahapan->referensi)) ?></td>
		</tr>
		<tr style="border: none;">
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">4.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">PERALATAN KERJA - PERLENGKAPAN K3 DAN MATERIAL</td>
		</tr>
	</table>
	<br>
	<table style="width: 100%;border: none;">
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">A.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">PERLENGKAPAN K3</td>
		</tr>
	</table>
	<table style="width: 100%;margin-top: 8px;">
		<tr>
			<td class="td" style="text-align: center;font-weight:bold;">No</td>
			<td class="td" style="text-align: center;font-weight:bold;">Nama Alat</td>
			<td class="td" style="text-align: center;font-weight:bold;">Satuan</td>
			<td class="td" style="text-align: center;font-weight:bold;">Volume</td>
			<td class="td" style="text-align: center;font-weight:bold;">Penanggung Jawab</td>
		</tr>
		<?php $no = 1 ?>
		<?php foreach ($alat_kerja as $key => $value) { ?>
			<tr>
				<td class="td" style="text-align: center;"><?php echo $no++ ?></td>
				<td class="td"><?php echo strtoupper($value['nama_alat']) ?></td>
				<td class="td"><?php echo strtoupper($value['satuan']) ?></td>
				<td class="td"><?php echo $value['volume'] ?></td>
				<td class="td"><?php echo strtoupper($value['penanggung_jawab']) ?></td>
			</tr>
		<?php } ?>
	</table>
	<br>
	<table style="width: 100%;border: none;">
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">B.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">ALAT KERJA</td>
		</tr>
	</table>
	<table style="width: 100%;margin-top: 8px;">
		<tr>
			<td class="td" style="text-align: center;font-weight:bold;">No</td>
			<td class="td" style="text-align: center;font-weight:bold;">Nama Alat</td>
			<td class="td" style="text-align: center;font-weight:bold;">Satuan</td>
			<td class="td" style="text-align: center;font-weight:bold;">Volume</td>
			<td class="td" style="text-align: center;font-weight:bold;">Penanggung Jawab</td>
		</tr>
		<?php $no = 1 ?>
		<?php foreach ($perlengkapan as $key => $value) { ?>
			<tr>
				<td class="td" style="text-align: center;"><?php echo $no++ ?></td>
				<td class="td"><?php echo strtoupper($value['nama_alat']) ?></td>
				<td class="td"><?php echo strtoupper($value['satuan']) ?></td>
				<td class="td"><?php echo $value['volume'] ?></td>
				<td class="td"><?php echo strtoupper($value['penanggung_jawab']) ?></td>
			</tr>
		<?php } ?>
	</table>
	<br>
	<table style="width: 100%;border: none;">
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">C.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">MATERIAL</td>
		</tr>
	</table>
	<table style="width: 100%;margin-top: 8px;">
		<tr>
			<td class="td" style="text-align: center;font-weight:bold;">No</td>
			<td class="td" style="text-align: center;font-weight:bold;">Nama Alat</td>
			<td class="td" style="text-align: center;font-weight:bold;">Satuan</td>
			<td class="td" style="text-align: center;font-weight:bold;">Volume</td>
			<td class="td" style="text-align: center;font-weight:bold;">Penanggung Jawab</td>
		</tr>
		<?php $no = 1 ?>
		<?php foreach ($material as $key => $value) { ?>
			<tr>
				<td class="td" style="text-align: center;"><?php echo $no++ ?></td>
				<td class="td"><?php echo strtoupper($value['nama_alat']) ?></td>
				<td class="td"><?php echo strtoupper($value['satuan']) ?></td>
				<td class="td"><?php echo $value['volume'] ?></td>
				<td class="td"><?php echo strtoupper($value['penanggung_jawab']) ?></td>
			</tr>
		<?php } ?>
	</table>
	<br>
	<table style="width: 100%;border: none;">
		<tr>
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">5.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">PELAKSANAAN PEKERJAAN</td>
		</tr>
	</table>
	<?php echo str_replace('Powered by', '', str_replace('Froala Editor', '', $tahapan->pelaksanaan_pekerjaan)) ?>
	<table style="width: 100%;border: none;">
		<tr style="border:none;">
			<td colspan="2" style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">Langkah - langkah pelaksanaan :</td>
		</tr>
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">A.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">Tahap (Persiapan) :</td>
		</tr>
	</table>
	<?php echo str_replace('Powered by', '', str_replace('Froala Editor', '', $tahapan->tahap_persiapan)) ?>
	<table style="width: 100%;border: none;">
		<tr style="border: none;">
			<td style="width: 30px;font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">B.</td>
			<td style="font-size: 12px;font-weight: bold;font-family: Arial, Helvetica, sans-serif;">Pelaksanaan Pekerjaan :</td>
		</tr>
	</table>
	<?php echo str_replace('Powered by', '', str_replace('Froala Editor', '', $tahapan->sub_pelaksanaan_pekerjaan)) ?>
	<div>
		<?php $nohead = 1 ?>
		<?php foreach ($data_uraian as $key => $value) { ?>
			<p style="font-size: 11px;"><?php echo $nohead++ ?>. <?php echo $value['uraian'] ?></p>
			<?php if (!empty($value['detail_item'])) { ?>
				<table style="width: 100%;margin-top: 8px;">
					<tr>
						<td class="td" style="text-align: center;font-weight:bold;width: 50px;">No</td>
						<td class="td" style="text-align: center;font-weight:bold;">Uraian Pekerjaan</td>
						<td class="td" style="text-align: center;font-weight:bold;width: 50px;">PLN <br /> (Checklist)</td>
						<td class="td" style="text-align: center;font-weight:bold;width: 50px;">PT. <?php echo $data_vendor->nama_vendor ?><br /> (Checklist)</td>
					</tr>
					<?php $no = 1 ?>
					<?php foreach ($value['detail_item'] as $vdetail) { ?>
						<tr>
							<td class="td" style="text-align: center;font-size: 10px;"><?php echo $no++ ?></td>
							<td class="td" style="font-size: 10px;"><?php echo strtoupper($vdetail['uraian']) ?></td>
							<td class="td">&nbsp;</td>
							<td class="td">&nbsp;</td>
						</tr>
					<?php } ?>
				</table>
			<?php } ?>
		<?php } ?>
	</div>
</body>

</html>
