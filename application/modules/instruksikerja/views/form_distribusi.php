<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_distribution">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Jabatan</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_distribusi)) { ?>
							<?php if (!empty($data_distribusi)) { ?>
								<?php foreach ($data_distribusi as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input type="text" value="<?php echo $value['jabatan'] ?>" id="jabatan" class="form-control required" error="Jabatan" />
										</td>
										<td class="text-center">
											<i class="fa fa-trash fa-lg hover-content" onclick="InstruksiKerja.addDistribusi(this)"></i>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<tr data_id="">
							<td>
								<input type="text" value="" id="jabatan" class="form-control required" error="Jabatan" />
							</td>
							<td class="text-center">
								<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addDistribusi(this)"></i>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" state='distribusi' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>
	</div>
</div>
