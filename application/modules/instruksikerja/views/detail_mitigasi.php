<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_mitigasi_resiko">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Bay Dikerjakan</th>
							<th>Potensi Risiko</th>
							<th>Mitigasi Resiko</th>
							<th>Arah Trip</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_mitigasi_resiko)) { ?>
							<?php if (!empty($data_mitigasi_resiko)) { ?>
								<?php foreach ($data_mitigasi_resiko as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input readonly type="text" value="<?php echo $value['bay_dikerjakan'] ?>" id="bay_dikerjakan" class="form-control required" error="Bay Dikerjakan" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['terminal_proteksi'] ?>" id="terminal_proteksi" class="form-control required" error="Terminal Proteksi" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['mitigasi_resiko'] ?>" id="mitigasi_resiko" class="form-control required" error="Mitigasi Resiko" />
										</td>
										<td>
											<input readonly type="text" value="<?php echo $value['arah_trip'] ?>" id="arah_trip" class="form-control required" error="Arah Trip" />
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
