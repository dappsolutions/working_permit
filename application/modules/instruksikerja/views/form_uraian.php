<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;" id="box-sub-pelaksanaan">
			<?php if (isset($data_uraian)) { ?>
				<?php if (!empty($data_uraian)) { ?>
					<?php foreach ($data_uraian as $key => $value) { ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">Sub Pelaksanaan Pekerjaan</label>

									<div class="col-sm-10">
										<textarea id="sub_pelaksanaan_pekerjaan" class="form-control" rows="7"><?php echo isset($value['sub_pelaksanaan']) ? $value['sub_pelaksanaan'] : '' ?></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="text-right" id="action-delete-sub">
									<?php if($key > 0){ ?>
										<a href="" onclick="InstruksiKerja.hapusSubPelaksaan(this, event)">Hapus Sub</a>
									<?php } ?>
								</div>
								<div class="table-responsive">
									<table class="table table-bordered" id="table_uraian">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>Uraian Pekerjaan</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if (isset($value['detail_item'])) { ?>
												<?php if (!empty($value['detail_item'])) { ?>
													<?php foreach ($value['detail_item'] as $v_item) { ?>
														<tr data_id="<?php echo $v_item['id'] ?>">
															<td>
																<input type="text" value="<?php echo $v_item['uraian'] ?>" id="uraian" class="form-control required" error="Uraian Pekerjaan" />
															</td>
															<td class="text-center">
																<i class="fa fa-trash fa-lg hover-content" onclick="InstruksiKerja.removeUraian(this)"></i>
															</td>
														</tr>
													<?php } ?>
												<?php } ?>
											<?php } ?>
											<tr data_id="">
												<td>
													<input type="text" value="" id="uraian" class="form-control required" error="Uraian Pekerjaan" />
												</td>
												<td class="text-center">
													<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addUraian(this)"></i>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php }else{ ?>
					<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">Sub Pelaksanaan Pekerjaan</label>

									<div class="col-sm-10">
										<textarea id="sub_pelaksanaan_pekerjaan" class="form-control" rows="7"></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="text-right" id="action-delete-sub"></div>
								<div class="table-responsive">
									<table class="table table-bordered" id="table_uraian">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>Uraian Pekerjaan</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<tr data_id="">
												<td>
													<input type="text" value="" id="uraian" class="form-control required" error="Uraian Pekerjaan" />
												</td>
												<td class="text-center">
													<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addUraian(this)"></i>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
				<?php } ?>
			<?php } ?>
			<br>
			<a href="" onclick="InstruksiKerja.addSubPelaksanaan(this, event)">Tambah Sub Pelaksanaan</a>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" state='uraian' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>
	</div>
</div>
