<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" method="post">
			<div class="box-body">
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Tujuan</label>

					<div class="col-sm-6">
						<div id="tujuan"><?php echo isset($data_tahapan->tujuan) ? $data_tahapan->tujuan : '' ?></div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Ruang Lingkup</label>

					<div class="col-sm-6">
						<div id="ruang_lingkup"><?php echo isset($data_tahapan->ruang_lingkup) ? $data_tahapan->ruang_lingkup : '' ?></div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Referensi</label>

					<div class="col-sm-6">
						<div id="referensi"><?php echo isset($data_tahapan->referensi) ? $data_tahapan->referensi : '' ?></div>
					</div>
				</div>
				<!-- <div class="form-group">
					<label for="" class="col-sm-2 control-label">Arah Trip</label>

					<div class="col-sm-6">
						<div id="arah_trip_editor"><?php echo isset($data_tahapan->arah_trip) ? $data_tahapan->arah_trip : '' ?></div>
					</div>
				</div> -->
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Pelaksanaan Pekerjaan</label>

					<div class="col-sm-6">
						<div id="pelaksanaan_pekerjaan"><?php echo isset($data_tahapan->pelaksanaan_pekerjaan) ? $data_tahapan->pelaksanaan_pekerjaan : '' ?></div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Tahap Persiapan</label>

					<div class="col-sm-6">
						<div id="tahap_persiapan"><?php echo isset($data_tahapan->tahap_persiapan) ? $data_tahapan->tahap_persiapan : '' ?></div>
					</div>
				</div>			
			</div>
			<div class="box-footer">
				<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
				<button type="submit" class="btn btn-success pull-right" state='tahapan' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
			</div>
			<!-- /.box-body -->
			<!-- /.box-footer -->
		</form>

	</div>
</div>
