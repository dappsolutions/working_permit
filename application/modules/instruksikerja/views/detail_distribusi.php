<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_distribution">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Jabatan</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_distribusi)) { ?>
							<?php if (!empty($data_distribusi)) { ?>
								<?php foreach ($data_distribusi as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input readonly type="text" value="<?php echo $value['jabatan'] ?>" id="jabatan" class="form-control required" error="Jabatan" />
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>				
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
