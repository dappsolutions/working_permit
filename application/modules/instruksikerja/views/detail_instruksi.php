<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal" method="post">
			<div class="box-body">
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">No Instruksi Kerja</label>

					<div class="col-sm-4">
						<input type="text" readonly class="form-control required" error="No Instruksi Kerja" id="no_instruksi" placeholder="No Instruksi Kerja" value="<?php echo isset($no_instruksi) ? $no_instruksi : '' ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Tgl. Instruksi Kerja</label>

					<div class="col-sm-4">
						<div class="input-group">
							<input type="text" class="form-control required" readonly error="Tgl. Instruksi Kerja" id="tanggal_instruksi" placeholder="Tgl. Instruksi Kerja" value="<?php echo isset($tanggal_instruksi) ? $tanggal_instruksi : '' ?>">
							<span class="input-group-addon">
								<i class="fa fa-calendar-o"></i>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Nama Pekerjaan Kontrak</label>

					<div class="col-sm-4">
						<input type="text" class="form-control required" readonly error="Nama Pekerjaan" id="nama_pekerjaan_kontrak" placeholder="nama_pekerjaan_kontrak" value="<?php echo isset($nama_pekerjaan_kontrak) ? $nama_pekerjaan_kontrak : '' ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">No. Kontrak</label>

					<div class="col-sm-4">
						<input type="text" class="form-control required" readonly error="No. Kontrak" id="no_kontrak" placeholder="no_kontrak" value="<?php echo isset($no_kontrak) ? $no_kontrak : '' ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Judul Pekerjaan</label>

					<div class="col-sm-4">
						<input type="text" readonly class="form-control required" error="Pekerjaan" id="pekerjaan" placeholder="Pekerjaan" value="<?php echo isset($pekerjaan) ? $pekerjaan : '' ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Tujuan Unit</label>

					<div class="col-sm-4">
						<select readonly class="form-control required" error="Tujuan Unit" id="tujuan_unit">
							<option value="">Pilih Tujuan Unit</option>
							<?php if (!empty($data_upt)) { ?>
								<?php foreach ($data_upt as $value) { ?>
									<?php $selected = '' ?>
									<?php if (isset($tujuan_unit)) { ?>
										<?php $selected = $tujuan_unit == $value['id'] ? 'selected' : '' ?>
									<?php } ?>
									<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<?php $hidden = ""; ?>
				<?php $hidden = $file_logo_vendor != '' ? '' : 'hidden' ?>
				<div class="form-group <?php echo $hidden ?>" id='detail_file_logo_vendor'>
					<label for="" class="col-sm-2 control-label">Logo Vendor</label>
					<div class="col-sm-4">						
						<div class="input-group">
							<input disabled type="text" id="file_str_logo_vendor" class="form-control" value="<?php echo $file_logo_vendor ?>">
							<span class="input-group-addon">
								<i class="fa fa-file-pdf-o hover-content" url="<?php echo base_url() . $path_logo_vendor . '/' . $file_logo_vendor ?>" file="<?php echo $file_logo_vendor ?>" onclick="InstruksiKerja.showLogo(this, event)"></i>
							</span>
							<span class="input-group-addon">
								<i class="fa fa-close hover-content" onclick="InstruksiKerja.gantiFileItemLogoVendor(this, event)"></i>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Analisa Risiko</label>

					<div class="col-sm-6">
						<div id="analisa_resiko"><?php echo isset($analisa_resiko) ? $analisa_resiko : '' ?></div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label">Mitigasi Resiko</label>

					<div class="col-sm-6">
						<div id="mitigasi_resiko_new"><?php echo isset($mitigasi_resiko) ? $mitigasi_resiko : '' ?></div>
					</div>
				</div>
			</div>
			<!-- /.box-body -->
			<!-- <div class="box-footer">
				<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
				<button type="submit" class="btn btn-success pull-right" onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
			</div> -->
			<!-- /.box-footer -->
		</form>

	</div>
</div>
