<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="row">
				<div class="col-md-12">
					DISUSUN
					<?php if (isset($data_approval)) { ?>
						<?php if (!empty($data_approval1)) { ?>
							<div class="table-responsive">
								<table class="table table-bordered" id="table_penyusun">
									<thead>
										<tr class="bg-primary-light text-white">
											<th>Nama Penyusun</th>
											<th>Email</th>
											<th>No. HP</th>
											<th>Jabatan</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($data_approval1 as $key => $value) { ?>
											<?php if ($value['roles'] == 'ROLES 1') { ?>
												<tr pegawai="<?php echo $value['pegawai_detail']['id'] ?>" data_id="<?php echo $value['id'] ?>">
													<td id="">
														<input type="text" value="<?php echo $value['pegawai_detail']['nama'] ?>" id="nama_penyusun" class="form-control required" error="Nama Penyusun" />
														<br>
														USERNAME : <b><?php echo $value['pegawai_detail']['nip'] ?></b>
														PASS : <b>1234</b>
													</td>
													<td>
														<input type="text" value="<?php echo $value['pegawai_detail']['email'] ?>" id="email_penyusun" class="form-control required" error="Email Penyusun" />
													</td>
													<td>
														<input type="text" value="<?php echo $value['pegawai_detail']['no_hp'] ?>" id="no_hp_penyusun" class="form-control required" error="No. HP Penyusun" />
													</td>
													<td>
														<input type="text" value="<?php echo $value['pegawai_detail']['posisi'] ?>" id="jabatan_penyusun" class="form-control required" error="Jabatan Penyusun" />
													</td>
													<td class="text-center">
														<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles(this)"></i>
													</td>
												</tr>
											<?php } ?>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } else { ?>
							<div class="table-responsive">
								<table class="table table-bordered" id="table_penyusun">
									<thead>
										<tr class="bg-primary-light text-white">
											<th>Nama Penyusun</th>
											<th>Email</th>
											<th>No. HP</th>
											<th>Jabatan</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr data_id="" pegawai="">
											<td>
												<input type="text" value="" id="nama_penyusun" class="form-control required" error="Nama Penyusun" />
											</td>
											<td>
												<input type="text" value="" id="email_penyusun" class="form-control required" error="Email Penyusun" />
											</td>
											<td>
												<input type="text" value="" id="no_hp_penyusun" class="form-control required" error="No. HP Penyusun" />
											</td>
											<td>
												<input type="text" value="" id="jabatan_penyusun" class="form-control required" error="Jabatan Penyusun" />
											</td>
											<td class="text-center">
												<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles(this)"></i>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php } ?>
					<?php } else { ?>
						<div class="table-responsive">
							<table class="table table-bordered" id="table_penyusun">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>Nama Penyusun</th>
										<th>Email</th>
										<th>No. HP</th>
										<th>Jabatan</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr data_id="" pegawai="">
										<td>
											<input type="text" value="" id="nama_penyusun" class="form-control required" error="Nama Penyusun" />
										</td>
										<td>
											<input type="text" value="" id="email_penyusun" class="form-control required" error="Email Penyusun" />
										</td>
										<td>
											<input type="text" value="" id="no_hp_penyusun" class="form-control required" error="No. HP Penyusun" />
										</td>
										<td>
											<input type="text" value="" id="jabatan_penyusun" class="form-control required" error="Jabatan Penyusun" />
										</td>
										<td class="text-center">
											<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles(this)"></i>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					DIPERIKSA
					<?php if (isset($data_approval2)) { ?>
						<?php if (!empty($data_approval2)) { ?>
							<div class="table-responsive">
								<table class="table table-bordered" id="table_diperiksa">
									<thead>
										<tr class="bg-primary-light text-white">
											<th>Nama Pemeriksa</th>
											<th>Email</th>
											<th>No. HP</th>
											<th>Jabatan</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($data_approval2 as $key => $value) { ?>
											<tr pegawai="<?php echo $value['pegawai_detail']['id'] ?>" data_id="<?php echo $value['id'] ?>">
												<td id="">
													<select name="" onchange="InstruksiKerja.setDetailRoles2(this, event)" class="form-control required" error="NAMA DIPERIKSA" id="nama_diperiksa">
														<?php foreach ($data_roles2 as $vroles) { ?>
															<option value="<?php echo $vroles['nip'] ?>" <?php echo $vroles['nip'] == $value['pegawai_detail']['nip'] ? 'selected' : '' ?>><?php echo $vroles['nip'] ?> - <?php echo $vroles['nama'] ?> - <?php echo $vroles['jabatan'] ?></option>
														<?php } ?>
													</select>
												</td>
												<td>
													<input type="text" readonly value="<?php echo $value['pegawai_detail']['email'] ?>" id="email_periksa" class="form-control required" error="Email" />
												</td>
												<td>
													<input type="text" readonly value="<?php echo $value['pegawai_detail']['no_hp'] ?>" id="no_hp_periksa" class="form-control required" error="No. HP" />
												</td>
												<td>
													<input type="text" readonly value="<?php echo $value['pegawai_detail']['posisi'] ?>" id="jabatan_periksa" class="form-control required" error="Jabatan" />
												</td>
												<td class="text-center">
													<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles2(this)"></i>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } else { ?>
							<div class="table-responsive">
								<table class="table table-bordered" id="table_diperiksa">
									<thead>
										<tr class="bg-primary-light text-white">
											<th>Nama Pemeriksa</th>
											<th>Email</th>
											<th>No. HP</th>
											<th>Jabatan</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr data_id="" pegawai="">
											<td>
												<select onchange="InstruksiKerja.setDetailRoles2(this, event)" name="" id="nama_diperiksa">
													<?php foreach ($data_roles2 as $vroles) { ?>
														<option email="<?php echo $vroles['email'] ?>" jabatan="<?php echo $vroles['jabatan'] ?>" no_hp="<?php echo $vroles['no_hp'] ?>" value="<?php echo $vroles['nip'] ?>"><?php echo $vroles['nip'] ?> - <?php echo $vroles['nama'] ?> - <?php echo $vroles['jabatan'] ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<input type="text" value="" readonly id="email_periksa" class="form-control required" error="Email" />
											</td>
											<td>
												<input type="text" value="" readonly id="no_hp_periksa" class="form-control required" error="No. HP" />
											</td>
											<td>
												<input type="text" value="" readonly id="jabatan_periksa" class="form-control required" error="Jabatan" />
											</td>
											<td class="text-center">
												<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles2(this)"></i>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php } ?>
					<?php } else { ?>
						<div class="table-responsive">
							<table class="table table-bordered" id="table_diperiksa">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>Nama Pemeriksa</th>
										<th>Email</th>
										<th>No. HP</th>
										<th>Jabatan</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr data_id="" pegawai="">
										<td>
											<select onchange="InstruksiKerja.setDetailRoles2(this, event)" name="" id="nama_diperiksa">
												<?php foreach ($data_roles2 as $vroles) { ?>
													<option email="<?php echo $vroles['email'] ?>" jabatan="<?php echo $vroles['jabatan'] ?>" no_hp="<?php echo $vroles['no_hp'] ?>" value="<?php echo $vroles['nip'] ?>"><?php echo $vroles['nip'] ?> - <?php echo $vroles['nama'] ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<input type="text" value="" readonly id="email_periksa" class="form-control required" error="Email" />
										</td>
										<td>
											<input type="text" value="" readonly id="no_hp_periksa" class="form-control required" error="No. HP" />
										</td>
										<td>
											<input type="text" value="" readonly id="jabatan_periksa" class="form-control required" error="Jabatan" />
										</td>
										<td class="text-center">
											<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles2(this)"></i>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					DISAHKAN
					<?php if (isset($data_approval3)) { ?>
						<?php if (!empty($data_approval3)) { ?>
							<div class="table-responsive">
								<table class="table table-bordered" id="table_disahkan">
									<thead>
										<tr class="bg-primary-light text-white">
											<th>Nama Pemeriksa</th>
											<th>Email</th>
											<th>No. HP</th>
											<th>Jabatan</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($data_approval3 as $key => $value) { ?>
											<tr pegawai="<?php echo $value['pegawai_detail']['id'] ?>" data_id="<?php echo $value['id'] ?>">
												<td id="">
													<select name="" onchange="InstruksiKerja.setDetailRoles2(this, event)" class="form-control required" error="NAMA DIPERIKSA" id="nama_diperiksa">
														<?php foreach ($data_roles3 as $vroles) { ?>
															<option value="<?php echo $vroles['nip'] ?>" <?php echo $vroles['nip'] == $value['pegawai_detail']['nip'] ? 'selected' : '' ?>><?php echo $vroles['nip'] ?> - <?php echo $vroles['nama'] ?> - <?php echo $vroles['jabatan'] ?></option>
														<?php } ?>
													</select>
												</td>
												<td>
													<input type="text" readonly value="<?php echo $value['pegawai_detail']['email'] ?>" id="email_periksa" class="form-control required" error="Email" />
												</td>
												<td>
													<input type="text" readonly value="<?php echo $value['pegawai_detail']['no_hp'] ?>" id="no_hp_periksa" class="form-control required" error="No. HP" />
												</td>
												<td>
													<input type="text" readonly value="<?php echo $value['pegawai_detail']['posisi'] ?>" id="jabatan_periksa" class="form-control required" error="Jabatan" />
												</td>
												<td class="text-center">
													<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles2(this)"></i>
												</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						<?php } else { ?>
							<div class="table-responsive">
								<table class="table table-bordered" id="table_disahkan">
									<thead>
										<tr class="bg-primary-light text-white">
											<th>Nama Pemeriksa</th>
											<th>Email</th>
											<th>No. HP</th>
											<th>Jabatan</th>
											<th class="text-center">Action</th>
										</tr>
									</thead>
									<tbody>
										<tr data_id="" pegawai="">
											<td>
												<select onchange="InstruksiKerja.setDetailRoles2(this, event)" name="" id="nama_diperiksa">
													<?php foreach ($data_roles3 as $vroles) { ?>
														<option email="<?php echo $vroles['email'] ?>" jabatan="<?php echo $vroles['jabatan'] ?>" no_hp="<?php echo $vroles['no_hp'] ?>" value="<?php echo $vroles['nip'] ?>"><?php echo $vroles['nip'] ?> - <?php echo $vroles['nama'] ?> - <?php echo $vroles['jabatan'] ?></option>
													<?php } ?>
												</select>
											</td>
											<td>
												<input type="text" value="" readonly id="email_periksa" class="form-control required" error="Email" />
											</td>
											<td>
												<input type="text" value="" readonly id="no_hp_periksa" class="form-control required" error="No. HP" />
											</td>
											<td>
												<input type="text" value="" readonly id="jabatan_periksa" class="form-control required" error="Jabatan" />
											</td>
											<td class="text-center">
												<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles3(this)"></i>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						<?php } ?>
					<?php } else { ?>
						<div class="table-responsive">
							<table class="table table-bordered" id="table_disahkan">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>Nama Pemeriksa</th>
										<th>Email</th>
										<th>No. HP</th>
										<th>Jabatan</th>
										<th class="text-center">Action</th>
									</tr>
								</thead>
								<tbody>
									<tr data_id="" pegawai="">
										<td>
											<select onchange="InstruksiKerja.setDetailRoles2(this, event)" name="" id="nama_diperiksa">
												<?php foreach ($data_roles3 as $vroles) { ?>
													<option email="<?php echo $vroles['email'] ?>" jabatan="<?php echo $vroles['jabatan'] ?>" no_hp="<?php echo $vroles['no_hp'] ?>" value="<?php echo $vroles['nip'] ?>"><?php echo $vroles['nip'] ?> - <?php echo $vroles['nama'] ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<input type="text" value="" readonly id="email_periksa" class="form-control required" error="Email" />
										</td>
										<td>
											<input type="text" value="" readonly id="no_hp_periksa" class="form-control required" error="No. HP" />
										</td>
										<td>
											<input type="text" value="" readonly id="jabatan_periksa" class="form-control required" error="Jabatan" />
										</td>
										<td class="text-center">
											<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAprovalRoles3(this)"></i>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" state='approval' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>
	</div>
</div>
