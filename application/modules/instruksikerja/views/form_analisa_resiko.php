<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_analisa_resiko">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Aktifitas</th>
							<th>Risiko</th>
							<th>Dampak</th>
							<th>Mitigasi</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_analisa_resiko)) { ?>
							<?php if (!empty($data_analisa_resiko)) { ?>
								<?php foreach ($data_analisa_resiko as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input type="text" value="<?php echo $value['aktifitas'] ?>" id="aktifitas" class="form-control required" error="Aktifitas" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['resiko'] ?>" id="resiko" class="form-control required" error="Risiko" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['dampak'] ?>" id="dampak" class="form-control required" error="Dampak" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['mitigasi'] ?>" id="mitigasi" class="form-control required" error="Mitigasi" />
										</td>
										<td class="text-center">
											<i class="fa fa-trash fa-lg hover-content" onclick="InstruksiKerja.removeAnalisaResiko(this)"></i>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<tr data_id="">
							<td>
								<input type="text" value="" id="aktifitas" class="form-control required" error="Aktifitas" />
							</td>
							<td>
								<input type="text" value="" id="resiko" class="form-control required" error="Risiko" />
							</td>
							<td>
								<input type="text" value="" id="dampak" class="form-control required" error="Dampak" />
							</td>
							<td>
								<input type="text" value="" id="mitigasi" class="form-control required" error="Mitigasi" />
							</td>
							<td class="text-center">
								<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addAnalisaResiko(this)"></i>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" state='analisa_resiko' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>
	</div>
</div>
