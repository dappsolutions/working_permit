<input type="hidden" id="sess_nip">
<input type="hidden" id="vendor_id" value="<?php echo $vendor_id ?>">
<input type="hidden" id="user_id" value="<?php echo $user_id ?>">

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box padding-16">
					<!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="table-data">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>No</th>
												<th>No instruksi Kerja</th>
												<th>Pekerjaan</th>
												<th>Tanggal</th>
												<th>Tujuan</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
												<tr>
													<td colspan="3" class="text-center">Tidak ada data ditemukan</td>
												</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							
						</ul>
					</div>
				</div>

				<a href="#" class="float" onclick="InstruksiKerja.add()">
					<i class="fa fa-plus my-float fa-lg"></i>
				</a>
			</div>
		</div>
	</div>
</div>
