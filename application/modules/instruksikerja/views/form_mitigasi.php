<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
			<div class="table-responsive">
				<table class="table table-bordered" id="table_mitigasi_resiko">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Bay Dikerjakan</th>
							<th>Potensi Risiko</th>
							<th>Mitigasi Risiko</th>
							<th>Arah Trip</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($data_mitigasi_resiko)) { ?>
							<?php if (!empty($data_mitigasi_resiko)) { ?>
								<?php foreach ($data_mitigasi_resiko as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<input type="text" value="<?php echo $value['bay_dikerjakan'] ?>" id="bay_dikerjakan" class="form-control required" error="Bay Dikerjakan" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['terminal_proteksi'] ?>" id="terminal_proteksi" class="form-control required" error="Terminal Proteksi" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['mitigasi_resiko'] ?>" id="mitigasi_resiko" class="form-control required" error="Mitigasi Risiko" />
										</td>
										<td>
											<input type="text" value="<?php echo $value['arah_trip'] ?>" id="arah_trip" class="form-control required" error="Arah Trip" />
										</td>
										<td class="text-center">
											<i class="fa fa-trash fa-lg hover-content" onclick="InstruksiKerja.removeMitigasiResiko(this)"></i>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<tr data_id="">
							<td>
								<input type="text" value="" id="bay_dikerjakan" class="form-control required" error="Bay Dikerjakan" />
							</td>
							<td>
								<input type="text" value="" id="terminal_proteksi" class="form-control required" error="Terminal Proteksi" />
							</td>
							<td>
								<input type="text" value="" id="mitigasi_resiko" class="form-control required" error="Mitigasi Risiko" />
							</td>
							<td>
								<input type="text" value="" id="arah_trip" class="form-control required" error="Arah Trip" />
							</td>
							<td class="text-center">
								<i class="fa fa-plus fa-lg hover-content" onclick="InstruksiKerja.addMitigasiResiko(this)"></i>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="box-footer">
			<button type="button" class="btn btn-default" onclick="InstruksiKerja.back()">Cancel</button>
			<button type="submit" class="btn btn-success pull-right" state='mitigasi' onclick="InstruksiKerja.simpan('<?php echo isset($id) ? $id : '' ?>', event, this)"><i class="fa fa-check"></i>&nbsp;Proses</button>
		</div>
	</div>
</div>
