<div class="row">
	<div class="col-md-12">
		<div class="box-body" style="margin-top: -12px;">
		<?php if (isset($data_uraian)) { ?>
				<?php if (!empty($data_uraian)) { ?>
					<?php foreach ($data_uraian as $key => $value) { ?>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">Sub Pelaksanaan Pekerjaan</label>

									<div class="col-sm-10">
										<textarea readonly id="sub_pelaksanaan_pekerjaan" class="form-control" rows="7"><?php echo isset($value['sub_pelaksanaan']) ? $value['sub_pelaksanaan'] : '' ?></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="table-responsive">
									<table class="table table-bordered" id="table_uraian">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>Uraian Pekerjaan</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if (isset($value['detail_item'])) { ?>
												<?php if (!empty($value['detail_item'])) { ?>
													<?php foreach ($value['detail_item'] as $v_item) { ?>
														<tr data_id="<?php echo $v_item['id'] ?>">
															<td>
																<input type="text" readonly value="<?php echo $v_item['uraian'] ?>" id="uraian" class="form-control required" error="Uraian Pekerjaan" />
															</td>
														</tr>
													<?php } ?>
												<?php } ?>
											<?php } ?>
											<tr data_id="">
												<td>
													<input readonly type="text" value="" id="uraian" class="form-control required" error="Uraian Pekerjaan" />
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php }else{ ?>
					<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="" class="col-sm-2 control-label">Sub Pelaksanaan Pekerjaan</label>

									<div class="col-sm-10">
										<textarea id="sub_pelaksanaan_pekerjaan" readonly class="form-control" rows="7"></textarea>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="text-right" id="action-delete-sub"></div>
								<div class="table-responsive">
									<table class="table table-bordered" id="table_uraian">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>Uraian Pekerjaan</th>
											</tr>
										</thead>
										<tbody>
											<tr data_id="">
												<td>
													<input type="text" value="" readonly id="uraian" class="form-control required" error="Uraian Pekerjaan" />
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</div>
