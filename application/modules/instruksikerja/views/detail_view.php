<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="row">
	<div class="col-md-12">
		<!-- Horizontal Form -->
		<div class="box box-info padding-16">
			<!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
			<div class="box-header with-border" style="margin-top: 12px;">
				<h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->

			<div class="row">
				<div class="col-md-12">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#form-instruksi" data-toggle="tab"><label class="label label-warning">1</label>&nbsp;Form Instruksi Kerja</a></li>
							<li><a href="#form-distribution" data-toggle="tab"><label class="label label-warning">2</label> Form Distribusi Dokumen</a></li>
							<li><a href="#form-catatan" data-toggle="tab"><label class="label label-warning">3</label> Form Catatan Perubahan</a></li>
							<li><a href="#form-analisa-resiko" data-toggle="tab"><label class="label label-warning">4</label> Form Analisa Risiko</a></li>
							<li><a href="#form-mitigasi" data-toggle="tab"><label class="label label-warning">5</label> Form Mitigasi Risiko</a></li>
							<li><a href="#form-tahapan" data-toggle="tab"><label class="label label-warning">6</label> Form Tahapan Pekerjaan</a></li>
							<li><a href="#form-perlengkapan" data-toggle="tab"><label class="label label-warning">7</label> Form Perlengkapan</a></li>
							<li><a href="#form-uraian" data-toggle="tab"><label class="label label-warning">8</label> Form Uraian Pekerjaan</a></li>
							<li><a href="#form-lampiran" data-toggle="tab"><label class="label label-warning">9</label> Form Dokumentasi & Lampiran</a></li>
							<li><a href="#form-approval" data-toggle="tab"><label class="label label-warning">10</label> Tracking Approval</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="tab-content">
						<div class="tab-pane active" id="form-instruksi">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_instruksi') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-distribution">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_distribusi') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-catatan">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_catatan_perubahan') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-analisa-resiko">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_analisa_resiko') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-mitigasi">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_mitigasi') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-perlengkapan">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_perlengkapan') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-tahapan">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('form_tahapan') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-uraian">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_uraian') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-lampiran">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_lampiran') ?>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="form-approval">
							<div class="row">
								<div class="col-md-12">
									<?php echo $this->load->view('detail_approval') ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.box -->
	</div>
</div>
