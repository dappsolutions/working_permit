<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Instruksikerja as GlobalInstruksikerja;

class Instruksikerja extends MY_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;
	public $akses;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 25;
		$this->akses = $this->session->userdata('hak_akses');
		// $this->load->model('master/UptModel');
		$this->load->model('transaksi/InstruksiDistribusi');
		$this->load->model('transaksi/InstruksiAnalisaResiko');
		$this->load->model('transaksi/InstruksiMitigasiResiko');
		$this->load->model('transaksi/InstruksiAlatKerja');
		$this->load->model('transaksi/InstruksiPerlengkapan');
		$this->load->model('transaksi/InstruksiMaterial');
		$this->load->model('transaksi/InstruksiKerjaTahapan');
		$this->load->model('transaksi/InstruksiKerjaModel');
		$this->load->model('transaksi/InstruksiKerjaTujuan');
		$this->load->model('transaksi/InstruksiKerjaUraian');
		$this->load->model('transaksi/InstruksiKerjaLampiran');
		$this->load->model('transaksi/InstruksiKerjaDokumen');
		$this->load->model('transaksi/InstruksiKerjaLampiranText');
		// $this->load->model('transaksi/InstruksiKerjaApproval');
		$this->load->model('transaksi/RoutingApprovalIka');
		$this->load->model('transaksi/RolesApprovalIka');
		$this->load->model('transaksi/InstruksiCatatan');
		$this->load->model('master/VendorModel');
		$this->load->model('master/UserModel');
	}

	public function getModuleName()
	{
		return 'instruksikerja';
	}

	public function getHeaderJSandCSS()
	{
		$version  = str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz');
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/instruksikerja.js?v=' . $version . '"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'instruksi_kerja';
	}

	public function getRootModule()
	{
		return "Permohonan";
	}

	public function index()
	{
		// Modules::run('email/sendWa', []);die;
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['user_id'] = $_SESSION['user_id'];
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = $this->getRootModule() . " - Instruksi Kerja";
		$data['title_content'] = 'Instruksi Kerja';
		$data['vendor_id'] = $_SESSION['vendor_id'];
		echo Modules::run('template', $data);
	}

	public function getData()
	{
		Capsule::enableQueryLog();
		$data['data'] = [];
		$data['recordsTotal'] = 0;
		$data['recordsFiltered'] = 0;
		$nipAkses = $_SESSION['nip_pegawai'];

		$datadb = InstruksiKerjaModel::select([
			'instruksi_kerja.*',
		])
			->with(['tujuanDetail', 'approvalData'])
			->whereNull('instruksi_kerja.deleted')
			// ->where('instruksi_kerja.id', '22')
		;
		if (isset($_POST)) {
			$data['recordsTotal'] = $datadb->get()->count();
			if (isset($_POST['search']['value'])) {
				$keyword = $_POST['search']['value'];
				$datadb->where(function ($query) use ($keyword) {
					$query->where('instruksi_kerja.no_instruksi', 'LIKE', '%' . $keyword . '%');
					$query->orWhere('instruksi_kerja.tanggal_instruksi', 'LIKE', '%' . $keyword . '%');
					$query->orWhere('instruksi_kerja.pekerjaan', 'LIKE', '%' . $keyword . '%');
				});
			}
			if (isset($_POST['order'][0]['column'])) {
				$datadb->orderBy('instruksi_kerja.id', $_POST['order'][0]['dir']);
			}
			$data['recordsFiltered'] = $datadb->get()->count();

			if (isset($_POST['length'])) {
				$datadb->limit($_POST['length']);
			}
			if (isset($_POST['start'])) {
				$datadb->offset($_POST['start']);
			}
		}
		$resultData = [];
		foreach ($datadb->get()->toArray() as $key => $value) {
			$approvalData = $value['approval_data'];
			$value['sudah_lengkap_acc'] = '';
			if ($nipAkses != '') {
				$foundOustandingAcc = false;
				foreach ($approvalData as $vApp) {
					if ($vApp['updated_at'] == '') {
						if ($vApp['nip'] == $nipAkses) {
							$foundOustandingAcc = true;
							break;
						}
					}
				}
				if ($foundOustandingAcc) {
					$resultData[] = $value;
				}
			} else {
				/*VENDOR AKSES */

				/*CEK ALL SUDAH ACC */
				$counterAcc = 0;
				foreach ($approvalData as $vApp) {
					if ($vApp['updated_at'] != '') {
						// if ($vApp['nip'] == $nipAkses) {
						$counterAcc += 1;
						// }
					}
				}
				if ($counterAcc == count($approvalData)) {
					if (count($approvalData) > 0) {
						$value['sudah_lengkap_acc'] = 'Sudah Lengkap';
					}
				}
				$value['jumlah_sudah_acc'] = $counterAcc;
				$value['total_approval'] = count($approvalData);
				/*CEK ALL SUDAH ACC */
				$resultData[] = $value;
			}
		}
		$data['data'] = $resultData;
		$data['draw'] = $_POST['draw'];
		$query = Capsule::getQueryLog();
		echo json_encode($data);
	}

	public function getDetailDataCatatan($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' t',
			'field' => array('t.*'),
			'where' => "t.id = '" . $id . "'"
		));

		$data = $data->row_array();
		return $data;
	}

	public function getListUpt()
	{
		$datadb = UptModel::where('deleted', 0)->get()->toArray();
		return $datadb;
	}

	public function getListDokumen($id)
	{
		$datadb = InstruksiKerjaDokumen::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListLampiran($id)
	{
		$datadb = InstruksiKerjaLampiranText::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListTrackApproval($id)
	{
		$datadb = InstruksiKerjaApproval::where('instruksi_kerja_approval.instruksi_kerja', $id)
			->select(['instruksi_kerja_approval.*'])
			->with(['pegawaiDetail'])
			->get()->toArray();
		return $datadb;
	}

	public function getListRoles($roles)
	{
		$datadb = RolesApprovalIka::where('roles', $roles)->get()->toArray();
		return $datadb;
	}

	public function getListDistribusi($id = '0')
	{
		$datadb = InstruksiDistribusi::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}


	public function getListAnlisaResiko($id = '0')
	{
		$datadb = InstruksiAnalisaResiko::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListUraianPekerjaan($id = '0')
	{
		$datadb = InstruksiKerjaUraian::where('instruksi_kerja', $id)
			->orderBy('sub_pelaksanaan')
			->get()->toArray();
		$result = [];
		$temp = [];
		foreach ($datadb as $key => $value) {
			$foreignKey = $value['sub_pelaksanaan'];
			if (!in_array($foreignKey, $temp)) {
				$dataItem = [];
				foreach ($datadb as $v_item) {
					if ($v_item['sub_pelaksanaan'] == $foreignKey) {
						$dataItem[] = $v_item;
					}
				}
				$value['detail_item'] = $dataItem;
				$result[] = $value;
				$temp[] = $foreignKey;
			}
		}
		return $result;
	}

	public function getLisLampiran($id = '0')
	{
		$datadb = InstruksiKerjaLampiranText::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListMitigasiResiko($id = '0')
	{
		$datadb = InstruksiMitigasiResiko::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListAlatKerja($id = '0')
	{
		$datadb = InstruksiAlatKerja::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListPerlengkapan($id = '0')
	{
		$datadb = InstruksiPerlengkapan::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListMaterial($id = '0')
	{
		$datadb = InstruksiMaterial::where('instruksi_kerja', $id)->get()->toArray();
		return $datadb;
	}

	public function getListTahapanPekerjaan($id = '0')
	{
		$datadb = InstruksiKerjaTahapan::where('instruksi_kerja', $id)->first();
		return $datadb;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Instruksi Kerja";
		$data['data_upt'] = $this->getListUpt();
		$data['data_distribusi'] = $this->getListDistribusi();
		$data['data_analisa_resiko'] = $this->getListAnlisaResiko();
		$data['data_mitigasi_resiko'] = $this->getListMitigasiResiko();
		$data['data_alat_kerja'] = $this->getListAlatKerja();
		$data['data_perlengkapan'] = $this->getListPerlengkapan();
		$data['data_material'] = $this->getListMaterial();
		$data['data_tahapan'] = $this->getListTahapanPekerjaan();
		$data['data_uraian'] = $this->getListUraianPekerjaan();
		$data['title_content'] = 'Tambah Instruksi Kerja';
		$data['file_logo_vendor'] = '';
		$data['path_logo_vendor'] = '';
		$data['data_roles2'] = $this->getListRoles('ROLES 2');
		$data['data_roles3'] = $this->getListRoles('ROLES 3');
		$userId = $this->session->userdata('user_id');
		$data['no_instruksi'] = Modules::run('no_generator/generateNoPengajuanIkaVendor', $userId);
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataCatatan($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Instruksi Kerja";
		$data['title_content'] = 'Ubah Instruksi Kerja';
		$data['data_distribusi'] = $this->getListDistribusi($id);
		$data['data_analisa_resiko'] = $this->getListAnlisaResiko($id);
		$data['data_mitigasi_resiko'] = $this->getListMitigasiResiko($id);
		$data['data_alat_kerja'] = $this->getListAlatKerja($id);
		$data['data_perlengkapan'] = $this->getListPerlengkapan($id);
		$data['data_material'] = $this->getListMaterial($id);
		$data['data_tahapan'] = $this->getListTahapanPekerjaan($id);
		$data['data_uraian'] = $this->getListUraianPekerjaan($id);
		$data['data_upt'] = $this->getListUpt();
		$data['data_dokumen'] = $this->getListDokumen($id);
		$data['data_lampiran'] = $this->getLisLampiran($id);
		$data['data_approval'] = $this->getListTrackApproval($id);
		$data['data_approval2'] = [];
		$approval1 = [];
		foreach ($data['data_approval'] as $key => $value) {
			if ($value['roles'] == 'ROLES 1') {
				$approval1[] = $value;
			}
		}

		$approval2 = [];
		foreach ($data['data_approval'] as $key => $value) {
			if ($value['roles'] == 'ROLES 2') {
				$approval2[] = $value;
			}
		}

		$data['data_approval3'] = [];
		$approval3 = [];
		foreach ($data['data_approval'] as $key => $value) {
			if ($value['roles'] == 'ROLES 3') {
				$approval3[] = $value;
			}
		}
		$data['data_approval3'] = $approval3;
		$data['data_approval2'] = $approval2;
		$data['data_approval1'] = $approval1;
		$data['data_roles2'] = $this->getListRoles('ROLES 2');
		$data['data_roles3'] = $this->getListRoles('ROLES 3');
		$data['data_catatan'] = $this->getListCatatanPerubahan($data, $id);
		echo Modules::run('template', $data);
	}

	public function getListCatatanPerubahan($params, $id)
	{
		$datadb = InstruksiCatatan::where('instruksi_kerja', $id)
			->get()->toArray();
		if (!empty($datadb)) {
			return $datadb;
		} else {
			$dataApproval = $params['data_approval'];
			$result = [];
			foreach ($dataApproval as $key => $value) {
				$result[] = [
					'id' => '',
					'edisi' => '00/00',
					'tanggal' => date('Y-m-d', strtotime($params['created_at'])),
					'halaman' => '-',
					'paragraf' => '-',
					'alasan' => 'Tidak ada alasan',
					'disetujui_oleh' => $value['pegawai_detail']['nama'],
					'nip_disetujui' => $value['pegawai_detail']['nip'],
					'jabatan' => $value['pegawai_detail']['posisi']
				];
			}
			return $result;
		}
		return $datadb;
	}

	public function detail()
	{
		$id = $_GET['id'];
		$data = $this->getDetailDataCatatan($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Instruksi Kerja";
		$data['title_content'] = 'Detail Instruksi Kerja';
		$data['data_distribusi'] = $this->getListDistribusi($id);
		$data['data_analisa_resiko'] = $this->getListAnlisaResiko($id);
		$data['data_mitigasi_resiko'] = $this->getListMitigasiResiko($id);
		$data['data_alat_kerja'] = $this->getListAlatKerja($id);
		$data['data_perlengkapan'] = $this->getListPerlengkapan($id);
		$data['data_material'] = $this->getListMaterial($id);
		$data['data_tahapan'] = $this->getListTahapanPekerjaan($id);
		$data['data_uraian'] = $this->getListUraianPekerjaan($id);
		$data['data_lampiran'] = $this->getLisLampiran($id);
		$data['data_upt'] = $this->getListUpt();
		$data['data_dokumen'] = $this->getListDokumen($id);
		$data['data_lampiran'] = $this->getLisLampiran($id);
		$data['data_approval'] = $this->getListTrackApproval($id);
		$data['data_roles2'] = $this->getListRoles('ROLES 2');
		$data['data_roles3'] = $this->getListRoles('ROLES 3');
		$data['nip_pegawai_akses'] = $_SESSION['nip_pegawai'];
		$data['data_catatan'] = $this->getListCatatanPerubahan($data, $id);
		echo Modules::run('template', $data);
	}

	public function getPostDataCatatan($value)
	{
		$data['catatan'] = $value->catatan;
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		// echo '<pre>';
		// print_r($data);die;
		$id = $this->input->post('id');
		$result['is_valid'] = false;
		$file = $_FILES;
		$userId = $this->session->userdata('user_id');

		$directoryUploaded = str_replace(':', '', $data->instruksi->no_instruksi);
		$pathDirUploaded = 'files/berkas/ika/' . $directoryUploaded;
		if (!is_dir($pathDirUploaded)) {
			mkdir('./files/berkas/ika/' . $directoryUploaded, 0777, TRUE);
		}

		$lampiranUplaoded = [];
		$dokumenUplaoded = [];
		$messageErrorUploader = [];
		$logoVendorUploaded = [];
		$isUploaded = true;
		if (!empty($file)) {
			$counter = 0;
			foreach ($file as $key => $value) {
				$name_of_field = 'file_lampiran_' . $counter;
				if (isset($file[$name_of_field])) {
					$response_upload = $this->uploadData($data->instruksi->no_instruksi, $name_of_field);
					if ($response_upload['is_valid']) {
						$lampiranUplaoded[] = [
							'file' => $response_upload['response']['file_name'],
							'path' => $pathDirUploaded
						];
					} else {
						$isUploaded = false;
						$messageErrorUploader[] = $response_upload['response'];
					}
				}

				$name_of_field = 'file_dokumen_' . $counter;
				if (isset($file[$name_of_field])) {
					$response_upload = $this->uploadData($data->instruksi->no_instruksi, $name_of_field);
					if ($response_upload['is_valid']) {
						$dokumenUplaoded[] = [
							'file' => $response_upload['response']['file_name'],
							'path' => $pathDirUploaded
						];
					} else {
						$isUploaded = false;
						$messageErrorUploader[] = $response_upload['response'];
					}
				}
			}
		}


		/*UPLOAD LOGO VENDOR */
		if (isset($file['file_logo_vendor'])) {
			$name_of_field = 'file_logo_vendor';
			$response_upload = $this->uploadData($data->instruksi->no_instruksi, $name_of_field);
			if ($response_upload['is_valid']) {
				$logoVendorUploaded = [
					'file' => $response_upload['response']['file_name'],
					'path' => $pathDirUploaded
				];
			} else {
				$isUploaded = false;
				$messageErrorUploader[] = $response_upload['response'];
			}
		}
		/*UPLOAD LOGO VENDOR */

		if (!empty($messageErrorUploader)) {
			$result['message'] = implode(', ', $messageErrorUploader);
			echo json_encode($result);
			die;
		}


		// $dataApprovalIka = RoutingApprovalIka::where('upt', $data->instruksi->tujuan_unit)->get()->toArray();
		// echo '<pre>';
		// print_r($data);die;
		$dataNotifikasi = [];
		$this->db->trans_begin();
		try {
			if ($id == '') {
				$actor = [];
				$actor['user'] = $userId;
				$actor['createddate'] = date('Y-m-d H:i:s');
				$actor['createdby'] = $userId;
				$this->db->insert('actor', $actor);
				$actorId = $this->db->insert_id();

				$document = [];
				$no_document = Modules::run('no_generator/generateNoDocument');
				$document['no_document'] = $no_document;
				$document['createddate'] = date('Y-m-d H:i:s');
				$document['createdby'] = $actorId;
				$document['doc_type'] = 'DOCT_IKA';
				$this->db->insert('document', $document);
				$docId = $this->db->insert_id();
			}

			$instruksi = [];
			if ($id == '') {
				$instruksi['document'] = $docId;
				$instruksi['created_at'] = date('Y-m-d H:i:s');
			}
			$instruksi['requestor'] = $userId;
			$instruksi['no_instruksi'] = $data->instruksi->no_instruksi;
			$instruksi['tanggal_instruksi'] = $data->instruksi->tanggal_instruksi;
			$instruksi['pekerjaan'] = $data->instruksi->pekerjaan;
			$instruksi['tujuan_unit'] = $data->instruksi->tujuan_unit;
			$instruksi['analisa_resiko'] = $data->instruksi->analisa_resiko;
			$instruksi['mitigasi_resiko'] = $data->instruksi->mitigasi_resiko;
			$instruksi['nama_pekerjaan_kontrak'] = $data->instruksi->nama_pekerjaan_kontrak;
			$instruksi['no_kontrak'] = $data->instruksi->no_kontrak;
			if (isset($logoVendorUploaded['file'])) {
				$instruksi['file_logo_vendor'] = $logoVendorUploaded['file'];
				$instruksi['path_logo_vendor'] = $logoVendorUploaded['path'];
			}
			if ($id == '') {
				$this->db->insert('instruksi_kerja', $instruksi);
			} else {
				$this->db->update('instruksi_kerja', $instruksi, ['id' => $id]);
			}
			$id = $id == '' ? $this->db->insert_id() : $id;
			// echo $id;die;

			$this->db->delete('instruksi_kerja_distribution', ['instruksi_kerja' => $id]);
			foreach ($data->distribusi as $key => $value) {
				$distributsi = [];
				$distributsi['instruksi_kerja'] = $id;
				$distributsi['jabatan'] = $value->jabatan;
				if ($distributsi['jabatan'] != '') {
					$this->db->insert('instruksi_kerja_distribution', $distributsi);
				}
			}

			$this->db->delete('instruksi_kerja_catatan', ['instruksi_kerja' => $id]);
			foreach ($data->catatan as $key => $value) {
				$catatan = [];
				$catatan['instruksi_kerja'] = $id;
				$catatan['edisi'] = $value->edisi;
				$catatan['tanggal'] = $value->tanggal;
				$catatan['halaman'] = $value->halaman;
				$catatan['paragraf'] = $value->paragraf;
				$catatan['alasan'] = $value->alasan;
				$catatan['disetujui_oleh'] = $value->disetujui_oleh;
				$catatan['jabatan'] = $value->jabatan;
				if ($catatan['edisi'] != '') {
					if ($value->deleted == '0') {
						$this->db->insert('instruksi_kerja_catatan', $catatan);
					}
				}
			}

			$this->db->delete('instruksi_kerja_analisa_resiko', ['instruksi_kerja' => $id]);
			foreach ($data->analisa_resiko as $key => $value) {
				$analisa = [];
				$analisa['instruksi_kerja'] = $id;
				$analisa['aktifitas'] = $value->aktifitas;
				$analisa['resiko'] = $value->resiko;
				$analisa['dampak'] = $value->dampak;
				$analisa['mitigasi'] = $value->mitigasi;
				if ($analisa['aktifitas'] != '') {
					if ($value->deleted == '0') {
						$this->db->insert('instruksi_kerja_analisa_resiko', $analisa);
					}
				}
			}

			$this->db->delete('instruksi_kerja_mitigasi_resiko', ['instruksi_kerja' => $id]);
			foreach ($data->mitigasi_resiko as $key => $value) {
				$mitigasi = [];
				$mitigasi['instruksi_kerja'] = $id;
				$mitigasi['bay_dikerjakan'] = $value->bay_dikerjakan;
				$mitigasi['terminal_proteksi'] = $value->terminal_proteksi;
				$mitigasi['mitigasi_resiko'] = $value->mitigasi_resiko;
				$mitigasi['arah_trip'] = $value->arah_trip;
				if ($mitigasi['bay_dikerjakan'] != '') {
					if ($value->deleted == '0') {
						$this->db->insert('instruksi_kerja_mitigasi_resiko', $mitigasi);
					}
				}
			}

			$this->db->delete('instruksi_kerja_alat_kerja', ['instruksi_kerja' => $id]);
			foreach ($data->alat_kerja as $key => $value) {
				$alat = [];
				$alat['instruksi_kerja'] = $id;
				$alat['nama_alat'] = $value->nama_alat;
				$alat['satuan'] = $value->satuan;
				$alat['volume'] = $value->volume;
				$alat['penanggung_jawab'] = $value->penanggung_jawab;
				if ($alat['nama_alat'] != '') {
					if ($value->deleted == '0') {
						$this->db->insert('instruksi_kerja_alat_kerja', $alat);
					}
				}
			}

			$this->db->delete('instruksi_kerja_perlengkapan', ['instruksi_kerja' => $id]);
			foreach ($data->perlengkapan as $key => $value) {
				$perlengkapan = [];
				$perlengkapan['instruksi_kerja'] = $id;
				$perlengkapan['nama_alat'] = $value->nama_alat;
				$perlengkapan['satuan'] = $value->satuan;
				$perlengkapan['volume'] = $value->volume;
				$perlengkapan['penanggung_jawab'] = $value->penanggung_jawab;
				if ($perlengkapan['nama_alat'] != '') {
					if ($value->deleted == '0') {
						$this->db->insert('instruksi_kerja_perlengkapan', $perlengkapan);
					}
				}
			}

			$this->db->delete('instruksi_kerja_material', ['instruksi_kerja' => $id]);
			foreach ($data->material as $key => $value) {
				$material = [];
				$material['instruksi_kerja'] = $id;
				$material['nama_alat'] = $value->nama_alat;
				$material['satuan'] = $value->satuan;
				$material['volume'] = $value->volume;
				$material['penanggung_jawab'] = $value->penanggung_jawab;
				if ($material['nama_alat'] != '') {
					if ($value->deleted == '0') {
						$this->db->insert('instruksi_kerja_material', $material);
					}
				}
			}

			$this->db->delete('instruksi_kerja_uraian_pekerjaan', ['instruksi_kerja' => $id]);
			foreach ($data->uraian as $key => $value) {
				$sub_pelaksan = $value->sub_pelaksanaan_pekerjaan;
				foreach ($value->detail_item as $v_item) {
					$uraian = [];
					$uraian['instruksi_kerja'] = $id;
					$uraian['uraian'] = $v_item->uraian;
					$uraian['sub_pelaksanaan'] = $sub_pelaksan;
					if ($sub_pelaksan != '' && $v_item->uraian != '') {
						if ($v_item->deleted == '0') {
							$this->db->insert('instruksi_kerja_uraian_pekerjaan', $uraian);
						}
					}
				}
			}

			$this->db->delete('instruksi_kerja_dokumen_text', ['instruksi_kerja' => $id]);
			foreach ($data->dokumen as $key => $value) {
				$doc = [];
				$doc['instruksi_kerja'] = $id;
				$doc['remarks'] = $value->remarks;
				if (isset($dokumenUplaoded[$key]['file'])) {
					$doc['file'] = $dokumenUplaoded[$key]['file'];
					$doc['path'] = $dokumenUplaoded[$key]['path'];
					if ($doc['remarks'] != '') {
						$this->db->insert('instruksi_kerja_dokumen_text', $doc);
					}
				}
			}

			$this->db->delete('instruksi_kerja_lampiran_text', ['instruksi_kerja' => $id]);
			foreach ($data->lampiran as $key => $value) {
				$lampira = [];
				$lampira['instruksi_kerja'] = $id;
				$lampira['remarks'] = $value->remarks;
				if (isset($lampiranUplaoded[$key]['file'])) {
					$lampira['file'] = $lampiranUplaoded[$key]['file'];
					$lampira['path'] = $lampiranUplaoded[$key]['path'];
					if ($lampira['remarks'] != '') {
						$this->db->insert('instruksi_kerja_lampiran_text', $lampira);
					}
				}
			}

			$this->db->delete('instruksi_kerja_tujuan', ['instruksi_kerja' => $id]);
			$tahapan = [];
			$tahapan['instruksi_kerja'] = $id;
			$tahapan['tujuan'] = $data->tahapan->tujuan;
			$tahapan['ruang_lingkup'] = $data->tahapan->ruang_lingkup;
			$tahapan['referensi'] = $data->tahapan->referensi;
			// $tahapan['arah_trip'] = $data->tahapan->arah_trip;
			$tahapan['pelaksanaan_pekerjaan'] = $data->tahapan->pelaksanaan_pekerjaan;
			// $tahapan['sub_pelaksanaan_pekerjaan'] = str_replace("<p><img", "<p style='text-align:center;'><img", $data->tahapan->sub_pelaksanaan_pekerjaan);
			$tahapan['tahap_persiapan'] = str_replace("<p><img", "<p style='text-align:center;'><img", $data->tahapan->tahap_persiapan);
			$this->db->insert('instruksi_kerja_tujuan', $tahapan);

			$this->db->delete('instruksi_kerja_lampiran', ['instruksi_kerja' => $id]);
			foreach ($lampiranUplaoded as $key => $value) {
				$lampiran = [];
				$lampiran['instruksi_kerja'] = $id;
				$lampiran['file'] = $value['file'];
				$lampiran['path'] = $value['path'];
				$this->db->insert('instruksi_kerja_lampiran', $lampiran);
			}

			if ($_POST['id'] == '') {
				$docTrans = [];
				$docTrans['status'] = 'DRAFT';
				$docTrans['createdby'] = $actorId;
				$docTrans['document'] = $docId;
				$this->db->insert('document_transaction', $docTrans);
			}

			$canEdited = true;
			// echo '<pre>';
			// print_r($dataApprovalIka);die;
			// if ($_POST['id'] != '') {
			// 	$canEdited = false;
			// }


			if ($canEdited) {
				/*DISUSUN OLEH */
				if ($data->state == 'approval') {
					// $this->db->delete('instruksi_kerja_approval', ['instruksi_kerja' => $id]);
					foreach ($data->approval_disusun as $key => $value) {
						/*CREATE PEHAWAI DAN USER */
						if ($value->nama_penyusun != '' && $value->email_penyusun != '' && $value->no_hp_penyusun != '' && $value->jabatan_penyusun != '') {
							$nip = substr($_SESSION['vendor_id'] . str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXZY'), 0, 9);
							$pegawai = [];
							$pegawai['pegawai_type'] = '24';
							// $pegawai['upt'] = '0';
							if ($value->id == '') {
								$pegawai['nip'] = $nip;
							}
							$pegawai['nama'] = $value->nama_penyusun;
							$pegawai['email'] = $value->email_penyusun;
							$pegawai['no_hp'] = $value->no_hp_penyusun;
							$pegawai['posisi'] = $value->jabatan_penyusun;
							$pegawai['posisi_lengkap'] = $value->jabatan_penyusun;
							$pegawai['createddate'] = date('Y-m-d');
							$pegawai['deleted'] = 0;
							if ($value->id == '') {
								$this->db->insert('pegawai', $pegawai);
							} else {
								$this->db->update('pegawai', $pegawai, ['id' => $value->pegawai]);
							}
							$pegawaiId = $value->id == '' ?  $this->db->insert_id() : $value->pegawai;
	
							if ($value->id == '') {
								$userData = [];
								$userData['pegawai'] = $pegawaiId;
								$userData['username'] = $nip;
								$userData['password'] = '1234';
								$userData['createddate'] = date('Y-m-d');
								$userData['deleted'] = 0;
								$this->db->insert('user', $userData);
	
								/*CREATE PEHAWAI DAN USER */
								$ikaAcc = [];
								$ikaAcc['nip'] = $nip;
								$ikaAcc['instruksi_kerja'] = $id;
								$ikaAcc['roles'] = 'ROLES 1';
								$ikaAcc['created_at'] = date('Y-m-d H:i:s');
								$this->db->insert('instruksi_kerja_approval', $ikaAcc);
	
								$push = [];
								$push['transaksi_id'] = $id;
								$push['nik'] =  $nip;
								$push['status_transaksi'] = 'CREATED';
								if ($data->id == '') {
									$push['remarks'] = 'Pengajuan Instruksi Kerja ID : ' . $data->instruksi->no_instruksi . ' Telah Dibuat';
								} else {
									$push['remarks'] = 'Pengajuan Instruksi Kerja ID : ' . $data->instruksi->no_instruksi . ' Telah Dirubah Penggunga';
								}
								$push['kategori'] = 'IK';
								$push['createddate'] = date('Y-m-d H:i:s');
								$this->db->insert('notifikasi', $push);
								/*PUSH WA */
								$pushWa = [];
								$pushWa['message'] = $push['remarks'];
								$pushWa['nip'] = $push['nik'];
								Modules::run('email/sendWa', $pushWa);
								$dataNotifikasi[] = [
									'message' => $push['remarks'],
									'nip' => $push['nik'],
									'no_hp' => $value->no_hp_penyusun,
								];
								/*PUSH WA */
							}
						}
					}

					foreach ($data->approval_diperiksa as $key => $value) {
						/*CREATE PEHAWAI DAN USER */
						$createPegawai = false;
						$createUser = false;
						$pegawai = [];
						$pegawai['pegawai_type'] = '24';
						// $pegawai['upt'] = '0';
						if ($value->id == '') {
							$pegawai['nip'] = $value->nama_diperiksa;
						}
						$pegawai['nama'] = $value->nama_diperiksa;
						$pegawai['email'] = $value->email_periksa;
						$pegawai['no_hp'] = $value->no_hp_periksa;
						$pegawai['posisi'] = $value->jabatan_periksa;
						$pegawai['posisi_lengkap'] = $value->jabatan_periksa;
						$pegawai['createddate'] = date('Y-m-d');
						$pegawai['deleted'] = 0;
						if ($value->id == '') {
							$dataPegawai = Modules::run('database/get', array(
								'table' => 'pegawai',
								'where' => array('nip' => $value->nama_diperiksa)
							));

							if (!empty($dataPegawai)) {
								$dataPegawai = $dataPegawai->row_array();
								$pegawaiUpdate = $pegawai;
								unset($pegawaiUpdate['nama']);
								$this->db->update('pegawai', $pegawaiUpdate, ['id' => $dataPegawai['id']]);
								$createUser = true;
								$pegawaiId = $dataPegawai['id'];
							} else {
								$this->db->insert('pegawai', $pegawai);
								$createPegawai = true;
								$pegawaiId = $this->db->insert_id();
							}
						} else {
							$this->db->update('pegawai', $pegawai, ['id' => $value->pegawai]);
						}
						if ($createPegawai) {
							$pegawaiId = $pegawaiId;
						} else {
							$pegawaiId = $value->pegawai;
						}

						if ($pegawaiId == '') {
							$dataPegawai = Modules::run('database/get', array(
								'table' => 'pegawai',
								'where' => array('nip' => $value->nama_diperiksa)
							));

							if (!empty($dataPegawai)) {
								$dataPegawai = $dataPegawai->row_array();
								$pegawaiId = $dataPegawai['id'];
							}
						}

						$dataUser = Modules::run('database/get', array(
							'table' => 'user',
							'where' => array('pegawai' => $pegawaiId)
						));
						if (empty($dataUser)) {
							$createUser = true;
						}

						if ($value->id == '' || $createUser) {
							$userData = [];
							$userData['pegawai'] = $pegawaiId;
							$userData['username'] = $value->nama_diperiksa;
							$userData['password'] = $value->nama_diperiksa;
							$userData['createddate'] = date('Y-m-d');
							$userData['deleted'] = 0;
							if ($createPegawai || $createUser) {
								$this->db->insert('user', $userData);
							}

							/*CREATE PEHAWAI DAN USER */
							$ikaAcc = [];
							$ikaAcc['nip'] = $value->nama_diperiksa;
							$ikaAcc['instruksi_kerja'] = $id;
							$ikaAcc['roles'] = 'ROLES 2';
							$ikaAcc['created_at'] = date('Y-m-d H:i:s');
							$this->db->insert('instruksi_kerja_approval', $ikaAcc);

							$push = [];
							$push['transaksi_id'] = $id;
							$push['nik'] =  $value->nama_diperiksa;
							$push['status_transaksi'] = 'CREATED';
							if ($data->id == '') {
								$push['remarks'] = 'Pengajuan Instruksi Kerja ID : ' . $data->instruksi->no_instruksi . ' Telah Dibuat';
							} else {
								$push['remarks'] = 'Pengajuan Instruksi Kerja ID : ' . $data->instruksi->no_instruksi . ' Telah Dirubah Penggunga';
							}
							$push['kategori'] = 'IK';
							$push['createddate'] = date('Y-m-d H:i:s');
							$this->db->insert('notifikasi', $push);
							/*PUSH WA */
							$pushWa = [];
							$pushWa['message'] = $push['remarks'];
							$pushWa['nip'] = $push['nik'];
							Modules::run('email/sendWa', $pushWa);
							$dataNotifikasi[] = [
								'message' => $push['remarks'],
								'nip' => $push['nik'],
								'no_hp' => $value->no_hp_periksa,
							];
							/*PUSH WA */
						}
					}

					foreach ($data->approval_disahkan as $key => $value) {
						/*CREATE PEHAWAI DAN USER */
						$createPegawai = false;
						$createUser = false;
						$pegawai = [];
						$pegawai['pegawai_type'] = '24';
						// $pegawai['upt'] = '0';
						if ($value->id == '') {
							$pegawai['nip'] = $value->nama_diperiksa;
						}
						$pegawai['nama'] = $value->nama_diperiksa;
						$pegawai['email'] = $value->email_periksa;
						$pegawai['no_hp'] = $value->no_hp_periksa;
						$pegawai['posisi'] = $value->jabatan_periksa;
						$pegawai['posisi_lengkap'] = $value->jabatan_periksa;
						$pegawai['createddate'] = date('Y-m-d');
						$pegawai['deleted'] = 0;
						if ($value->id == '') {
							$dataPegawai = Modules::run('database/get', array(
								'table' => 'pegawai',
								'where' => array('nip' => $value->nama_diperiksa)
							));

							if (!empty($dataPegawai)) {
								$dataPegawai = $dataPegawai->row_array();
								$pegawaiUpdate = $pegawai;
								unset($pegawaiUpdate['nama']);
								$this->db->update('pegawai', $pegawaiUpdate, ['id' => $dataPegawai['id']]);
								$createUser = true;
								$pegawaiId = $dataPegawai['id'];
							} else {
								$this->db->insert('pegawai', $pegawai);
								$createPegawai = true;
								$pegawaiId = $this->db->insert_id();
							}
						} else {
							$this->db->update('pegawai', $pegawai, ['id' => $value->pegawai]);
						}
						if ($createPegawai) {
							$pegawaiId = $pegawaiId;
						} else {
							$pegawaiId = $value->pegawai;
						}

						if ($pegawaiId == '') {
							$dataPegawai = Modules::run('database/get', array(
								'table' => 'pegawai',
								'where' => array('nip' => $value->nama_diperiksa)
							));

							if (!empty($dataPegawai)) {
								$dataPegawai = $dataPegawai->row_array();
								$pegawaiId = $dataPegawai['id'];
							}
						}

						$dataUser = Modules::run('database/get', array(
							'table' => 'user',
							'where' => array('pegawai' => $pegawaiId)
						));

						if (empty($dataUser)) {
							$createUser = true;
						}

						if ($value->id == '' || $createUser) {
							$userData = [];
							$userData['pegawai'] = $pegawaiId;
							$userData['username'] = $value->nama_diperiksa;
							$userData['password'] = $value->nama_diperiksa;
							$userData['createddate'] = date('Y-m-d');
							$userData['deleted'] = 0;
							if ($createPegawai || $createUser) {
								$this->db->insert('user', $userData);
							}

							/*CREATE PEHAWAI DAN USER */
							$ikaAcc = [];
							$ikaAcc['nip'] = $value->nama_diperiksa;
							$ikaAcc['instruksi_kerja'] = $id;
							$ikaAcc['roles'] = 'ROLES 3';
							$ikaAcc['created_at'] = date('Y-m-d H:i:s');
							$this->db->insert('instruksi_kerja_approval', $ikaAcc);

							$push = [];
							$push['transaksi_id'] = $id;
							$push['nik'] =  $value->nama_diperiksa;
							$push['status_transaksi'] = 'CREATED';
							if ($data->id == '') {
								$push['remarks'] = 'Pengajuan Instruksi Kerja ID : ' . $data->instruksi->no_instruksi . ' Telah Dibuat';
							} else {
								$push['remarks'] = 'Pengajuan Instruksi Kerja ID : ' . $data->instruksi->no_instruksi . ' Telah Dirubah Penggunga';
							}
							$push['kategori'] = 'IK';
							$push['createddate'] = date('Y-m-d H:i:s');
							$this->db->insert('notifikasi', $push);
							/*PUSH WA */
							$pushWa = [];
							$pushWa['message'] = $push['remarks'];
							$pushWa['nip'] = $push['nik'];
							Modules::run('email/sendWa', $pushWa);
							$dataNotifikasi[] = [
								'message' => $push['remarks'],
								'nip' => $push['nik'],
								'no_hp' => $value->no_hp_periksa,
							];
							/*PUSH WA */
						}
					}
				}
				/*DISUSUN OLEH */
			}
			$this->db->trans_commit();
			$result['is_valid'] = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
			$result['message'] = $ex->getMessage();
		}

		$result['id'] = $id;
		$result['data_notif'] = $dataNotifikasi;
		$result['is_valid_notif'] = empty($dataNotifikasi) ? false : true;
		echo json_encode($result);
	}

	public function approve()
	{
		$id = $this->input->post('id');
		$result['is_valid'] = false;
		$userId = $this->session->userdata('user_id');
		$instruksi = $_POST['instruksi'];
		$keterangan = isset($_POST['keterangan']) ? $_POST['keterangan'] : '';
		$nipAkses = $_SESSION['nip_pegawai'];
		$dataPegawai = Modules::run('database/get', array(
			'table' => 'pegawai',
			'where' => array('nip' => $nipAkses)
		));
		$namaPegawai = $nipAkses;
		if (!empty($dataPegawai)) {
			$dataPegawai = $dataPegawai->row_array();
			$namaPegawai = $dataPegawai['nama'];
		}

		$dataRequestor = Modules::run('database/get', array(
			'table' => 'user u',
			'field' => array('u.*', 'v.no_hp'),
			'join'=> array(
				array('vendor v', 'v.id = u.vendor')
			),
			'where' => array('u.id' => $userId)
		));
	 $noHpRequestor = '0';
		if (!empty($dataRequestor)) {
			$dataRequestor = $dataRequestor->row_array();
			$noHpRequestor = $dataRequestor['no_hp'];
		}


		$this->db->trans_begin();
		try {
			$dataInstruksi = InstruksiKerjaModel::where('id', $instruksi)->first();
			$actor = [];
			$actor['user'] = $userId;
			$actor['createddate'] = date('Y-m-d H:i:s');
			$actor['createdby'] = $userId;
			$this->db->insert('actor', $actor);
			$actorId = $this->db->insert_id();

			$updateAcc = [];
			$updateAcc['updated_at'] = date('Y-m-d H:i:s');
			if ($keterangan != '') {
				$updateAcc['remarks'] = $keterangan;
			}
			$this->db->update('instruksi_kerja_approval', $updateAcc, ['id' => $id]);

			$docTrans = [];
			$docTrans['status'] = $keterangan == '' ? 'APPROVED' : 'REJECTED';
			$docTrans['createdby'] = $actorId;
			$docTrans['document'] = $dataInstruksi->document;
			$this->db->insert('document_transaction', $docTrans);
			$this->db->trans_commit();
			/*PUSH WA */
			$pushWa = [];
			$pushWa['message'] = 'Pengajuan Transaksi ID : ' . $dataInstruksi->no_instruksi . ' Telah ' . $docTrans['status'] . ' ' . $keterangan . ' oleh ' . $nipAkses . ' - ' . $namaPegawai;
			// $pushWa['nip'] = $push['nik'];
			Modules::run('email/sendWa', $pushWa);
			$dataNotifikasi[] = [
				'message' => $pushWa['message'],
				'nip' => $userId,
				'no_hp' => $noHpRequestor,
			];
			/*PUSH WA */
			$result['is_valid'] = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
			$result['message'] = $ex->getMessage();
		}

		$result['id'] = $id;
		$result['is_valid_notif'] = empty($dataNotifikasi) ? false : true;
		$result['data_notif'] = $dataNotifikasi;
		echo json_encode($result);
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Catatan";
		$data['title_content'] = 'Data Catatan';
		$content = $this->getDataCatatan($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function uploadData($dir, $name_of_field)
	{
		$config['upload_path'] = 'files/berkas/ika/' . $dir . '/';
		$config['allowed_types'] = 'png|jpg|PNG';
		$config['max_size'] = '2000';
		$config['max_width'] = '5000';
		$config['max_height'] = '5000';

		$this->load->library('upload', $config);

		$is_valid = false;
		if (!$this->upload->do_upload($name_of_field)) {
			$response = $this->upload->display_errors();
		} else {
			$response = $this->upload->data();
			$is_valid = true;
		}

		return array(
			'is_valid' => $is_valid,
			'response' => $response
		);
	}

	public function showLogo()
	{
		// $foto = str_replace(' ', '_', $this->input->post('foto'));
		$foto = $_POST['foto'];
		$data['foto'] = $foto;
		echo $this->load->view('foto', $data, true);
	}

	public function upload_image()
	{
		$data = $_POST;
		$file = $_FILES;

		$directoryUploaded = str_replace(':', '', $data['no_instruksi']);
		$pathDirUploaded = 'files/berkas/ika/' . $directoryUploaded . '/tahapan';
		if (!is_dir($pathDirUploaded)) {
			mkdir('./files/berkas/ika/' . $directoryUploaded . '/tahapan', 0777, TRUE);
		}

		$dir = $data['no_instruksi'] . '/tahapan';

		$lampiranUplaoded = [];
		$messageErrorUploader = [];
		$isUploaded = true;
		if (!empty($file)) {
			$counter = 0;
			$name_of_field = 'image_param';
			if (isset($file[$name_of_field])) {
				$response_upload = $this->uploadData($dir, $name_of_field);
				if ($response_upload['is_valid']) {
					$link = base_url() . $pathDirUploaded . '/' . $response_upload['response']['file_name'];
					$lampiranUplaoded = [
						'file' => $response_upload['response']['file_name'],
						'path' => $pathDirUploaded,
						'link' => $link
					];
				} else {
					$isUploaded = false;
					$lampiranUplaoded = [
						'response' => $response_upload['response'],
						'link' => base_url()
					];
				}
			}
		}

		echo json_encode($lampiranUplaoded);
	}

	public function cetak()
	{
		$cssFroala = $_SERVER['DOCUMENT_ROOT'] . '/working_permit/assets/plugins/froala-editor/css/froala_editor.pkgd.min.css';
		$stylesheet = file_get_contents($cssFroala);

		$data = $_GET;
		$instruksi = InstruksiKerjaModel::where('instruksi_kerja.id', $data['id'])
			->select(['instruksi_kerja.*'])
			->with(['documentDetail'])
			->first();
		$tglCreated = date('Y-m-d', strtotime($instruksi['documentDetail']->createddate));
		$tahapan = InstruksiKerjaTahapan::where('instruksi_kerja', $data['id'])->first();
		$analisa_resiko = InstruksiAnalisaResiko::where('instruksi_kerja', $data['id'])->get()->toArray();
		$blocking_resiko = InstruksiMitigasiResiko::where('instruksi_kerja', $data['id'])->get()->toArray();
		$tujuan = InstruksiKerjaTujuan::where('instruksi_kerja', $data['id'])->first();
		$alat_kerja = InstruksiAlatKerja::where('instruksi_kerja', $data['id'])->get()->toArray();
		$perlengkapan = InstruksiPerlengkapan::where('instruksi_kerja', $data['id'])->get()->toArray();
		$material = InstruksiMaterial::where('instruksi_kerja', $data['id'])->get()->toArray();
		$uraian_pekerjaan = InstruksiKerjaUraian::where('instruksi_kerja', $data['id'])->get()->toArray();
		$documentasi = InstruksiKerjaLampiran::where('instruksi_kerja', $data['id'])->get()->toArray();
		$documentasiText = InstruksiKerjaDokumen::where('instruksi_kerja', $data['id'])->get()->toArray();
		$lampiranText = InstruksiKerjaLampiranText::where('instruksi_kerja', $data['id'])->get()->toArray();
		$dataApproval = InstruksiKerjaApproval::where('instruksi_kerja', $data['id'])
			->select(['instruksi_kerja_approval.*'])
			->with(['pegawaiDetail'])
			->get()->toArray();
		$dataRequestor = UserModel::where('id', $instruksi->requestor)->first();
		$dataVendor = VendorModel::where('id', $dataRequestor->vendor)->first();
		$data['data_uraian'] = $this->getListUraianPekerjaan($instruksi->id);

		$data['instruksi'] = $instruksi;
		$data['tahapan'] = $tahapan;
		$data['analisa_resiko'] = $analisa_resiko;
		$data['blocking_resiko'] = $blocking_resiko;
		$data['tujuan'] = $tujuan;
		$data['alat_kerja'] = $alat_kerja;
		$data['perlengkapan'] = $perlengkapan;
		$data['material'] = $material;
		$data['uraian_pekerjaan'] = $uraian_pekerjaan;
		$data['documentasi'] = $documentasi;
		$data['data_approval'] = $dataApproval;
		$data['data_vendor'] = $dataVendor;
		$data['data_dokumen'] = $documentasiText;
		$data['data_lampiran'] = $lampiranText;
		$data['tanggal_instruksi'] = $this->getTanggalIndo($instruksi->tanggal_instruksi);
		$data['data_distribusi'] = $this->getListDistribusi($instruksi->id);
		$data['data_approval'] = $this->getListTrackApproval($instruksi->id);
		$data['data_approval2'] = [];
		$approval2 = [];
		foreach ($data['data_approval'] as $key => $value) {
			if ($value['roles'] == 'ROLES 2') {
				$approval2[] = $value;
			}
		}
		$data['data_approval3'] = [];
		$approval3 = [];
		foreach ($data['data_approval'] as $key => $value) {
			if ($value['roles'] == 'ROLES 3') {
				$approval3[] = $value;
			}
		}
		$approval1 = [];
		foreach ($data['data_approval'] as $key => $value) {
			if ($value['roles'] == 'ROLES 1') {
				$approval1[] = $value;
			}
		}
		$data['data_approval3'] = $approval3;
		$data['data_approval2'] = $approval2;
		$data['data_approval1'] = $approval1;
		$data['data_catatan'] = $this->getListCatatanPerubahan($data, $instruksi->id);

		$data['pekerjaan'] = ucwords($data['instruksi']->pekerjaan);
		$data['no_document'] = Modules::run('no_generator/generateNoDocumentIkaVendor', $instruksi, $tglCreated);
		$mpdf = Modules::run('mpdf/getInitPdf');
		$doc_instruksi = $this->load->view('cetak/document_instruksi', $data, true);
		$doc_tahapan = $this->load->view('cetak/document_tahapan', $data, true);
		$doc_distribusi = $this->load->view('cetak/document_distribusi', $data, true);
		$doc_distribusi_second = $this->load->view('cetak/document_daftar_dsitribusi', $data, true);
		$doc_catatan = $this->load->view('cetak/document_catatan', $data, true);
		$doc_daftar_isi = $this->load->view('cetak/document_daftar_isi', $data, true);
		$doc_analisa_resiko = $this->load->view('cetak/document_analisa_resiko', $data, true);
		$doc_blocking_resiko = $this->load->view('cetak/document_mitigasi', $data, true);
		$doc_tujuan = $this->load->view('cetak/document_tujuan', $data, true);
		$doc_lampiran = $this->load->view('cetak/document_lampiran', $data, true);
		// echo $doc_tahapan;die;

		$cover = $this->load->view('cetak/cover', $data, true);
		$header = $this->load->view('cetak/header', $data, true);
		$footer = $this->load->view('cetak/footer', $data, true);
		$mpdf->use_kwt = true;
		$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
		$mpdf->WriteHTML($cover);
		$mpdf->setAutoBottomMargin = 'stretch';
		$mpdf->setAutoTopMargin = 'stretch';
		$mpdf->SetHTMLHeader($header);
		$mpdf->SetHTMLFooter($footer);
		// $mpdf->WriteHTML($cover);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_distribusi);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_distribusi_second);
		$mpdf->addPage('L');
		$mpdf->WriteHTML($doc_catatan);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_daftar_isi);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_analisa_resiko);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_blocking_resiko);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_tujuan);
		$mpdf->AddPage();
		$mpdf->WriteHTML($doc_lampiran);
		$mpdf->Output('IKA' . $data['id'] . date('Y-m-d') . '.pdf', 'I');
	}

	public function getTanggalIndo($tanggal)
	{
		$month = date('m', strtotime($tanggal));
		switch (intval($month)) {
			case '1':
				$month = 'Januari';
				break;
			case '2':
				$month = 'Februari';
				break;
			case '3':
				$month = 'Maret';
				break;
			case '4':
				$month = 'April';
				break;
			case '5':
				$month = 'Mei';
				break;
			case '6':
				$month = 'Juni';
				break;
			case '7':
				$month = 'Juli';
				break;
			case '8':
				$month = 'Agustus';
				break;
			case '9':
				$month = 'September';
				break;
			case '10':
				$month = 'Oktober';
				break;
			case '11':
				$month = 'November';
				break;
			case '12':
				$month = 'Desember';
				break;

			default:
				# code...
				break;
		}

		$year = date('Y', strtotime($tanggal));
		$day = date('d', strtotime($tanggal));
		return $day . ' ' . $month . ' ' . $year;
	}
}
