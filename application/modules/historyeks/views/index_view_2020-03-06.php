
<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box padding-16">
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="input-group">
         <input type="text" class="form-control" onkeyup="Historyeks.search(this, event)" id="keyword" placeholder="Pencarian">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
       </div>          
      </div>
      <div class="divider"></div>
      <br/>
      <?php if (isset($keyword)) { ?>
       <br/>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>  

      <?php if ($hak_akses == 'superadmin' || ($hak_akses == 'approval' && $level == '1' && $upt == '2')) { ?>
       <div class="row">
        <div class="col-md-3">
         <select class="form-control" id="unit" error="Unit" onchange="Historyeks.searchByUpt(this)">
          <option value="">Pilih Unit</option>
          <?php if (!empty($list_unit)) { ?>
           <?php foreach ($list_unit as $value) { ?>
            <?php $selected = '' ?>
            <?php if (isset($upt_id)) { ?>
             <?php $selected = $upt_id == $value['id'] ? 'selected' : '' ?>
            <?php } ?>
            <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
           <?php } ?>
          <?php } ?>
         </select>
        </div>
       </div>
      <?php } ?>       
      <div class="row">
       <div class="col-md-12">        
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_content">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>No Pengajuan Wp</th>
            <th>Tanggal Wp</th>
            <!-- <th>Tanggal Pekerjaan</th> -->
            <th>Tanggal Awal Pelaksanaan</th>
            <th>Tanggal Akhir Pelaksanaan</th>
            <th>Kategori Tempat Pekerjaan</th>
            <th>Lokasi Pekerjaan</th>
            <th>Uraian Pekerjaan</th>
            <th>Tujuan</th>
            <th>Status</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <?php $bg_color = ''; ?>
             <?php if ($value['status'] == 'DRAFT') { ?>
              <?php $bg_color = 'bg-warning'; ?>
             <?php } ?>
             <?php if ($value['status'] == 'REJECTED') { ?>
              <?php $bg_color = 'bg-danger'; ?>
             <?php } ?>
             <?php if ($value['status'] == 'APPROVED') { ?>
              <?php $bg_color = 'bg-success'; ?>
             <?php } ?>
             <tr class="<?php echo $bg_color ?>">
              <td><b><?php echo $no++ ?></b></td>
              <td><b><?php echo $value['no_wp'] ?></b></td>
              <td><b><?php echo date('d-m-Y', strtotime($value['tanggal_wp'])) ?></b></td>
              <!-- <td><b><?php echo $value['tgl_pekerjaan'] ?></b></td> -->
              <td><b><?php echo date('d-m-Y', strtotime($value['tgl_awal'])) ?></b></td>
              <td><b><?php echo date('d-m-Y', strtotime($value['tgl_akhir'])) ?></b></td>
              <td><b><?php echo $value['jenis_place'] ?></b></td>
              <td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
              <td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
              <td><b><?php echo $value['nama_upt'] ?></b></td>
              <?php if ($value['status'] == 'DRAFT') { ?>
               <td><b><?php echo $value['status'] ?></b></td>
              <?php } else { ?>              
               <td><b><?php echo $value['status'] ?></b><br/>[Level : <?php echo $value['level'] ?>]</td>
              <?php } ?>              
              <td class="text-center">
               <i class="fa fa-trash grey-text hover" onclick="Historyeks.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-file-text grey-text  hover" onclick="Historyeks.detail('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <?php if($value['level'] == '3'){ ?>
                <i class="fa fa-print grey-text  hover" onclick="WpEksternal.cetak('<?php echo $value['id'] ?>')"></i>
               <?php } ?>  
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="20" class="text-center">Tidak ada data ditemukan</td>
            </tr>
           <?php } ?>           
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>

     <div class="box-footer clearfix">
      <ul class="pagination pagination-sm no-margin pull-right">
       <?php echo $pagination['links'] ?>
      </ul>
     </div>
    </div>    
   </div>
  </div>       
 </div>
</div>
