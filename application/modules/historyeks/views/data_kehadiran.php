<div class="row">
 <div class="col-md-12">
   <div class="table-responsive">
   <table class="" style="width: 100%;">
     <thead>
      <tr>
       <th class="td_jsa">No</th>
       <th class="td_jsa">Nama</th>
       <th class="td_jsa">Kehadiran</th>
       <th class="td_jsa">Tipe</th>
       <th class="td_jsa">Tanggal</th>
      </tr>
     </thead>
     <tbody>
      <?php if(!empty($dataAtta)){ ?>
       <?php $no = 1 ?>
       <?php foreach ($dataAtta as $key => $value) {?>
        <tr>
         <td class="td_jsa"><?php echo $no++ ?></td>
         <td class="td_jsa"><?php echo strtoupper($value['nama_pekerja']) ?></td>
         <td class="td_jsa"><?php echo $value['hadir'] == '1' ? 'Hadir' : 'Tidak Hadir' ?></td>
         <td class="td_jsa"><?php echo strtoupper($value['tipe']) ?></td>
         <td class="td_jsa"><?php echo date('Y-m-d', strtotime($value['createddate'])) ?></td>
        </tr>
       <?php }?>
      <?php }else{ ?>
       <tr>
        <td colspan="3">Tidak ada data ditemukan</td>
       </tr>
      <?php } ?>
     </tbody>
   </table>
   </div>
 </div>
</div>

<style>
	.td_jsa {
		border: 1px solid black;
		font-family: tahoma;
		font-size: 12px;
		text-align: center;
		padding: 8px;
	}
</style>