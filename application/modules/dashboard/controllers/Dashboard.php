<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Illuminate\Database\Capsule\Manager as Capsule;

class Dashboard extends MY_Controller
{

	public $hak_akses;
	public $upt;
	public $level;
	public $user;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->hak_akses = $this->session->userdata('hak_akses');
		$this->upt = $this->session->userdata('upt');
		$this->level = $this->session->userdata('level');
		$this->user = $this->session->userdata('user_id');
		$this->load->model('master/UserModel');
	}

	public function getModuleName()
	{
		return 'dashboard';
	}

	public function getHeaderJSandCSS()
	{
		$version  = str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz');
		$data = array(
			'<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
			'<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
			'<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.resize.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.pie.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.categories.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.css">',
			'<script src="' . base_url() . 'assets/js/controllers/dashboard.js?v=' . $version . '"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/grafikswa.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/grafiksa.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/grafiksimson.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data = $_GET;
		$date = isset($data['year']) ? $data['year'] : date('Y');
		$data['year'] = $date;
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Dashboard";
		$data['title_content'] = 'Dashboard';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['total_user'] = $this->getTotalUser();
		$data['total_wp_approve'] = $this->getTotalWpApprove();
		$data['total_wp_draft'] = $this->getTotalWpDraft();
		$data['total_wp_reject'] = $this->getTotalWpReject();
		$data['data_pengajuan'] = $this->getDataWpDraft();
		$data['list_year'] = Modules::run('grafikswa/getYear');
		$data['list_month'] =  Modules::run('grafikswa/getMonth');
		$data['list_upt'] = Modules::run('grafikswa/getListUpt');
		$data['list_upt_wp'] = Modules::run('grafik/getListUpt');
		$data['list_pemegang'] = array();
		if (isset($data['month']) && isset($data['upt'])) {
			if ($data['month'] != '' && $data['upt'] != '') {
				$data['list_pemegang'] = $this->grafik->getDataGrupPengajuan($data);
			}
		}
		$data['data_wp'] = Modules::run('grafikswa/getDataWpGrafik', $data, 'SWA');
		$data['data_wp_sa'] = []; //Modules::run('grafikswa/getDataWpGrafik', $data, 'SA');
		$data['data_wp_simson'] = Modules::run('grafiksimson/getDataWpGrafik', $data);
		// echo '<pre>';
		// print_r($data['data_wp']);die;
		$data['date'] = $date;
		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		// echo '<pre>';
		// print_r($data['data_wp']);
		// die;
		echo Modules::run('template', $data);
	}

	public function getTotalUser()
	{
		$total = Modules::run('database/count_all', array(
			'table' => 'user',
			'where' => "deleted = 0"
		));
		return $total;
	}

	public function getTotalWpApprove()
	{
		$where = "p.deleted = 0 and pst.status = 'APPROVED'";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'APPROVED'";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 and pst.status = 'APPROVED' "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'APPROVED' "
					. "and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pst.status = 'APPROVED' and pp.upt = '" . $this->upt . "'";
		}

		//
		//  echo $where;die;
		$total = Modules::run('database/count_all', array(
			'table' => 'permit p',
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
			),
			'where' => $where
		));
		return $total;
	}

	public function getTotalWpDraft()
	{

		$where = "p.deleted = 0 and pst.status = 'DRAFT'";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'DRAFT'";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 and pst.status = 'DRAFT' "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'DRAFT' "
					. "and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pst.status = 'DRAFT' and pp.upt = '" . $this->upt . "'";
		}

		$total = Modules::run('database/count_all', array(
			'table' => 'permit p',
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
			),
			'where' => $where
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		return $total;
	}

	public function getDataWpDraft()
	{
		$where = "p.deleted = 0";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pp.upt = '" . $this->upt . "'";
		}

		// echo '<pre>';
		// print_r($_SESSION);
		// die;

		// echo $where;
		// die;
		$data = Modules::run('database/get', array(
			'table' => 'permit p',
			'field' => array('p.*', 'pst.status'),
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
			),
			'where' => $where,
			'limit' => 10,
			'orderby' => 'p.id desc'
		));
		// echo "<pre>";
		// echo $this->db->last_query();
		// die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getTotalWpReject()
	{

		$where = "p.deleted = 0 and pst.status = 'REJECTED'";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'REJECTED'";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 and pst.status = 'REJECTED' "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'REJECTED' "
					. "and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pst.status = 'REJECTED' and pp.upt = '" . $this->upt . "'";
		}

		$total = Modules::run('database/count_all', array(
			'table' => 'permit p',
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
			),
			'where' => $where
		));
		return $total;
	}

	public function tes()
	{
		// $datadb = Capsule::table('user')->get();
		// echo '<pre>';
		// print_r($datadb);
		// // $this->capsule->Q		
		// // $data = User::where('id', 1)->get()->toArray();
		// // echo '<pre>';
		// // print_r($data);die;

		$mpdf = Modules::run('mpdf/getInitPdf');
		$mpdf->SetHTMLHeader(
			'<div style="text-align: right; font-weight: bold;">My document</div>',
			'O'
		);

		$mpdf->SetHTMLFooter('
<table width="100%" style="vertical-align: bottom; font-family: serif; 
    font-size: 8pt; color: #000000; font-weight: bold; font-style: italic;">
    <tr>
        <td width="33%">{DATE j-m-Y}</td>
        <td width="33%" align="center">{PAGENO}/{nbpg}</td>
        <td width="33%" style="text-align: right;">My document</td>
    </tr>
</table>');

		$mpdf->WriteHTML('Hello World');

		// 		$html = '
		// <html>
		// <head>
		// <style>
		//     @page {
		//         size: auto;
		//         odd-header-name: MyHeader1;
		//         odd-footer-name: MyFooter1;
		//     }
		//     @page chapter2 {
		//         odd-header-name: MyHeader2;
		//         odd-footer-name: MyFooter2;
		//     }
		//     @page noheader {
		//         odd-header-name: _blank;
		//         odd-footer-name: _blank;
		//     }
		//     div.chapter2 {
		//         page-break-before: always;
		//         page: chapter2;
		//     }
		//     div.noheader {
		//         page-break-before: always;
		//         page: noheader;
		//     }
		// </style>
		// </head>
		// <body>
		//     <pageheader name="MyHeader1" content-right="OKEEE"
		//         header-style="font-weight: bold; color: #000000;" line="on">
		// 				</pageheader>
		//     <pagefooter name="MyFooter1" content-left="{DATE j-m-Y}"
		//         content-center="{PAGENO}/{nbpg}" footer-style="font-size: 8pt;" />
		//     <pageheader name="MyHeader2" content-right="Chapter 2"
		//         header-style="font-weight: bold; color: #000000;" line="on" />
		//     <pagefooter name="MyFooter2" content-left="{DATE j-m-Y}"
		//         content-center="2: {PAGENO} Dokumen ini tidak dapat digandakan tanpa izin tertulis dari PT PLN (Persero)" footer-style="font-size: 8pt;" />

		//     <div>Here is the text of the first chapter</div>
		//     <div class="chapter2">Text of Chapter 2</div>
		//     <div class="noheader">No-Header page</div>
		// </body>
		// </html>';

		// 		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}

	public function froala()
	{
		echo $this->load->view('tesfroala');
	}
}
