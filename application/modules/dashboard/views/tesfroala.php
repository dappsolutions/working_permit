<!DOCTYPE html>
<html>

<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/froala-editor/css/froala_editor.pkgd.min.css" type="text/css">
</head>

<body>

 <div id="example"></div>
 <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/froala-editor/js/froala_editor.pkgd.min.js"></script>
 <script>
  let editor = new FroalaEditor('#example', {
   // Set the image upload parameter.
   imageUploadParam: 'image_param',

   // Set the image upload URL.
   imageUploadURL: '/upload_image',

   // Additional upload params.
   imageUploadParams: {
    id: 'my_editor'
   },

   // Set request type.
   imageUploadMethod: 'POST',

   // Set max image size to 5MB.
   imageMaxSize: 5 * 1024 * 1024,

   // Allow to upload PNG and JPG.
   imageAllowedTypes: ['jpeg', 'jpg', 'png'],
   events: {
    'image.beforeUpload': function(images) {
     // Return false if you want to stop the image upload.
    },
    'image.uploaded': function(response) {
     // Image was uploaded to the server.
    },
    'image.inserted': function($img, response) {
     // Image was inserted in the editor.
    },
    'image.replaced': function($img, response) {
     // Image was replaced in the editor.
    },
    'image.error': function(error, response) {
     // Bad link.
     if (error.code == 1) {}

     // No link in upload response.
     else if (error.code == 2) {}

     // Error during image upload.
     else if (error.code == 3) {}

     // Parsing response failed.
     else if (error.code == 4) {}

     // Image too text-large.
     else if (error.code == 5) {}

     // Invalid image type.
     else if (error.code == 6) {}

     // Image can be uploaded only to same domain in IE 8 and IE 9.
     else if (error.code == 7) {}

     // Response contains the original server response to the request if available.
    }
   }
  }, function() {
   console.log(editor.html.get())
  });

  // $('div[style="z-index:9999;width:100%;position:relative"]').remove(); untuk menghilangkan watermak
 </script>
</body>

</html>