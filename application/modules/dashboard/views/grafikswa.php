<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary padding-16">
					<div class="box-header with-border">
						<i class="fa fa-bar-chart-o"></i>

						<h3 class="box-title">Grafik SWA</h3>
						<br />

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-3">
								<select class="form-control col-sm-3" error="Tahun" id="tahun" onchange="GrafikSwa.changeFilter(this, 'dashboard')">
									<?php if (!empty($list_year)) { ?>
										<?php foreach ($list_year as $value) { ?>
											<?php $selected = $value == $date ? 'selected' : '' ?>
											<option <?php echo $selected ?> value="<?php echo $value ?>"><?php echo $value ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<select class="form-control col-sm-3" id="month" onchange="GrafikSwa.changeFilter(this, 'dashboard')">
									<option value="">Pilih Bulan</option>
									<?php if (!empty($list_month)) { ?>
										<?php foreach ($list_month as $value) { ?>
											<?php $selected = '' ?>
											<?php if(isset($month)){ ?>
												<?php $selected = $value == $month ? 'selected' : '' ?>
											<?php } ?>
											<option <?php echo $selected ?> value="<?php echo $value ?>"><?php echo $value ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<select class="form-control col-sm-3" id="upt" onchange="GrafikSwa.changeFilter(this, 'dashboard')">
									<option value="">Pilih UPT</option>
									<?php $nama_upt = '' ?>
									<?php if (!empty($list_upt)) { ?>
										<?php foreach ($list_upt as $value) { ?>
											<?php $selected = '' ?>
											<?php if (isset($upt)) { ?>
												<?php if ($upt != '') { ?>
													<?php $selected = $value['id'] == $upt ? 'selected' : '' ?>
													<?php if ($selected != '') { ?>
														<?php $nama_upt = $value['nama'] ?>
													<?php } ?>
												<?php } ?>
											<?php } ?>
											<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3 hide">
								<?php $disabled = "disabled" ?>
								<?php if (isset($month) && isset($upt)) { ?>
									<?php if ($month != '' && $upt != '') { ?>
										<?php $disabled = "" ?>
									<?php } ?>
								<?php } ?>
								<select class="form-control col-sm-3" <?php echo $disabled ?> id="pelaksana" onchange="GrafikSwa.changeFilter(this, 'dashboard')">
									<option value="">Pilih Pelaksana Swa</option>
									<?php $nama_pemegang = '' ?>
									<?php if (!empty($list_pemegang)) { ?>
										<?php foreach ($list_pemegang as $value) { ?>
											<?php $selected = '' ?>
											<?php if (isset($pelaksana)) { ?>
												<?php if ($pelaksana != '') { ?>
													<?php $selected = $value['pegawai'] == $pelaksana ? 'selected' : '' ?>
													<?php if ($selected != '') { ?>
														<?php $nama_pemegang = $value['nama_swa'] ?>
													<?php } ?>
												<?php } ?>
											<?php } ?>
											<option <?php echo $selected ?> value="<?php echo $value['pegawai'] ?>"><?php echo $value['nama_swa'] ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<p>
								<h4><i><b><?php echo isset($year) ? $year : date('Y') ?></b> <b><?php echo isset($month) ? $month != '' ? '/ ' . $month : '' : '' ?></b> </b> <b><?php echo isset($upt) ? $upt != '' ? '/ ' . $nama_upt : '' : '' ?></b> <b><?php echo isset($pelaksana) ? $pelaksana != '' ? '/ ' . $nama_pemegang : '' : '' ?></b></i></h4>
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div id="bar-chart" style="height: 400px;"></div>
							</div>
						</div>
						<br />
						<br />
						<br />
					</div>
					<!-- /.box-body-->
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" value='<?php echo $data_wp ?>' id="data_wp" class="form-control" />