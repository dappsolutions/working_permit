<?php echo $this->load->view('top_dashboard', array(), true); ?>


<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-9">
    <div class="box padding-16">
     <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo ucfirst($title_content) ?></h3>
      <div class="divider"></div>
     </div>
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <!--<br/>-->          
      <div class="divider"></div>
      <div class="row">
       <div class="col-md-12">
        <h4>Top 10 Pengajuan Working Permit</h4>        
        <div class="table-responsive">
         <table class="table table-bordered table-striped" id="tb_content">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>No Pengajuan WP</th>
            <th>Status</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($data_pengajuan)) { ?>
            <?php $no = 1; ?>
            <?php foreach ($data_pengajuan as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['no_wp'] ?></td>
              <?php $text_color = 'bg-warning' ?>
              <?php if ($value['status'] == 'DRAFT') { ?>
               <?php $text_color = 'bg-warning' ?>
              <?php } ?>
              <?php if ($value['status'] == 'APPROVED') { ?>
               <?php $text_color = 'bg-success' ?>
              <?php } ?>
              <?php if ($value['status'] == 'REJECTED') { ?>
               <?php $text_color = 'bg-danger' ?>
              <?php } ?>
              <td class="<?php echo $text_color ?>"><?php echo $value['status'] ?></td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="2" class="text-center">Tidak ada data ditemukan</td>
            </tr>
           <?php } ?>           
          </tbody>
         </table>
        </div>
       </div>
      </div>
	 <div class="row">
       <div class="col-md-12 text-right">
        <!-- <h5>Pengajuan WP Hanya Bisa pada Jam 08:00 - 16:00</h5> -->
       </div>
      </div>
      <br/>
     </div>
    </div>

    <!--    <a href="#" class="float">
         <i class="fa fa-plus my-float fa-lg"></i>
        </a>-->
   </div>

   <div class="col-md-3">
    <div class="box box-primary padding-16">
     <div class="box-header with-border">
      <i class="fa fa-bar-chart-o"></i>

      <h3 class="box-title">Grafik WP</h3>

      <div class="box-tools pull-right">
       <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
       </button>
       <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
      </div>
     </div>
     <div class="box-body">
      <div id="chart_wp" style="height: 300px;"></div>
     </div>
     <!-- /.box-body-->
    </div>
   </div>
  </div>       
 </div>
</div>
