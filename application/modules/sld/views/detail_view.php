<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nama Sld</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Nama Sld" id="nama" 
              placeholder="Nama Sld"
              value="<?php echo isset($nama) ? $nama : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-4">
       <div class="input-group">
        <input disabled type="text" class="form-control" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-addon"><i class="fa fa-image hover-content" file="<?php echo isset($file) ? $file : '' ?>" onclick="Sld.showLogo(this, event)"></i></span>
       </div>
      </div>      
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Sld.back()">Cancel</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
