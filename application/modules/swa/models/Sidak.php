<?php
// ini_set("memory_limit",-1);
class Sidak extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_sidak', 'sidak');
		$this->load->model('m_permit', 'permit');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'swa/sidak';
	}

	public function index()
	{
		echo 'sidak ' . date('Y-m-d H:i:s');
	}

	public function simpan()
	{
		$data = $_POST;
		$data_wp = $this->permit->getDetailData($data);
		$data_image = (array) json_decode($_POST['data_image']);
		$data_apd = (array) json_decode($_POST['data_apd']);
		$data_swa = (array) json_decode($_POST['data_swa']);
		$data_pekerjaan = (array) json_decode($_POST['data_pekerjaan']);
		$data['data_wp'] = $data_wp;
		$data['data_image'] = $data_image;
		$data['data_apd'] = $data_apd;
		$data['data_swa'] = $data_swa;
		$data['data_pekerjaan'] = $data_pekerjaan;
		$data['no_trans'] = Modules::run('no_generator/generateNoTransLog');

		// echo '<pre>';
		// print_r($data['data_apd']);
		// die;
		$result = $this->sidak->simpan($data);

		if ($result['is_valid'] == '1') {
			$this->sendEmail($data);
		}


		// echo '<pre>';
		// print_r($data['data_image']);
		// die;
		echo json_encode($result);
		// Modules::run('simson/output/get', $result);
	}

	public function sendEmail($params = array())
	{
		$params['subject'] = 'Formulir SWA';
		$params['no_wp'] = $params['no_wp'];
		$params['id_wp'] = $params['data_wp']['id'];
		$params['email'] = $params['data_swa']['email_swa'];
		$params['message'] = '<p>Berikut kami sampaikan laporan SWA, Silkan klik link dibawah ini untuk melihat formulir</p>
		<p>Semoga PLN menjadi yang terbaik</p>
		<p>Amienn...</p>
		<p><a href="' . base_url() . 'swa/sidak/cetak?permit=' . $params['id_wp'] . '&no_wp=' . $params['no_wp'] . '">Formulir SWA</a></p>';

		Modules::run('email/send_email_data_other', $params['subject'], $params['message'], $params['email']);
	}

	public function getDetailData()
	{
		$data = $_POST;
		$this->createFilePdf();
		// $data['no_wp'] = "WPEKS20MAY001";
		$data_safety = $this->sidak->getDataSafety($data);
		$result['data_swa'] = $data_safety;
		$result['data_apd'] = $data_safety;
		$result['data_pekerjaan'] = $data_safety;
		Modules::run('simson/output/get', $result);
	}

	public function getDetailImage()
	{
		$data = $_POST;
		// $data['no_wp'] = 'WPINT20JAN002';
		$data_image = $this->sidak->getDataSidakImage($data);
		$result['data'] = $data_image;
		Modules::run('simson/output/get', $result);
	}

	public function showDetailPengajuan()
	{
		$data = $_GET;
		// echo '<pre>';
		// print_r($data);
		// die;
		$data_safety = $this->sidak->getDataSafetyAll($data);
		$data['data_swa'] = $data_safety;
		// echo '<pre>';
		// print_r($data);
		// die;
		$data['data_apd'] = $data_safety;
		$data['data_pekerjaan'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		// echo '<pre>';
		// print_r($data['data_swa']);
		// die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengajuan";
		$data['title_content'] = "Detail Pengajuan";
		echo Modules::run('template', $data);
	}

	public function getDokumenDp3Aktif()
	{
		$data = Modules::run('database/get', array(
			'table' => 'dp3',
			'where' => "deleted = 0 and id = 2"
		));

		$data = $data->row_array();
		$data['berlaku_efektif'] = Modules::run('helper/getIndoDate', $data['berlaku_efektif']);
		return $data;
	}

	public function getDataStatusPekerjaan($data)
	{
		if (!empty($data)) {
			if ($data['wp'] == '' || $data['wp'] == 'no') {
				return "STOP";
			}

			if ($data['sop'] == '' || $data['sop'] == 'no') {
				return "STOP";
			}

			if ($data['jsa'] == '' || $data['jsa'] == 'no') {
				return "STOP";
			}

			if ($data['pengawas_k3'] == '' || $data['pengawas_k3'] == 'no') {
				return "STOP";
			}

			if ($data['sertifikat_kom'] == '' || $data['sertifikat_kom'] == 'no') {
				return "STOP";
			}

			if ($data['peralatan'] == '' || $data['peralatan'] == 'no') {
				return "STOP";
			}

			if ($data['rambu_k3'] == '' || $data['rambu_k3'] == 'no') {
				return "STOP";
			}

			if ($data['apd'] == '' || $data['apd'] == 'no') {
				return "STOP";
			}

			return "LANJUT";
		}

		return "STOP";
	}

	public function cetak()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_GET;
		$data_safety = $this->sidak->getDataSafety($data);
		$data['keputusan_pekerjaan'] = $this->getDataStatusPekerjaan($data_safety);
		$data['data_swa'] = $data_safety;
		$data['data_apd'] = $data_safety;
		$data['data_pekerjaan'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		// echo '<pre>';
		// print_r($data);
		// die;
		$data['dp3'] = $this->getDokumenDp3Aktif();

		$image_ttd_k3 = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_k3']);
		$image_ttd_swa = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_swa']);

		$data['img_ttdk3'] = '<img height="50" width="50" src="' . $image_ttd_k3 . '"/>';
		$data['img_ttdswa'] = '<img height="50" width="50" src="' . $image_ttd_swa . '"/>';

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$formulir_swa = $this->load->view('sidak/cetak/formulir_swa', $data, true);
		$mpdf->WriteHTML($formulir_swa);
		$mpdf->Output('FORMULIR SWA - ' . $data['no_wp'] . '.pdf', 'I');
	}

	public function createFilePdf()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_POST;
		// $data['no_wp'] = "WPEKS20MAY001";
		$data_safety = $this->sidak->getDataSafety($data);
		$data['keputusan_pekerjaan'] = $this->getDataStatusPekerjaan($data_safety);
		$data['data_swa'] = $data_safety;
		$data['data_apd'] = $data_safety;
		$data['data_pekerjaan'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		$data['dp3'] = $this->getDokumenDp3Aktif();

		$image_ttd_k3 = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_k3']);
		$image_ttd_swa = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_swa']);

		$data['img_ttdk3'] = '<img height="50" width="50" src="' . $image_ttd_k3 . '"/>';
		$data['img_ttdswa'] = '<img height="50" width="50" src="' . $image_ttd_swa . '"/>';

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$formulir_swa = $this->load->view('sidak/cetak/formulir_swa', $data, true);
		$mpdf->WriteHTML($formulir_swa);
		$mpdf->Output('files/berkas/dokumen_sidak/FORMULIRSWA-' . $data['no_wp'] . '.pdf', 'F');
	}

	public function base64ToImage($params, $imageData)
	{
		$imageData = 'data:image/jpeg;base64,' . $imageData;
		list($type, $imageData) = explode(';', $imageData);
		// echo $imageData;
		// die;
		list(, $extension) = explode('/', $type);
		list(, $imageData)      = explode(',', $imageData);
		$fileName = $_SERVER["DOCUMENT_ROOT"] . '/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		// $fileName = $_SERVER["DOCUMENT_ROOT"] . '/working_permit/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		$imageData = base64_decode($imageData);
		file_put_contents($fileName, $imageData);

		return $fileName;
	}
}
