
<?php

class M_permit extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND p.id < " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			$filter_cari = " and (pw.uraian_pekerjaan like '%" . $params['keyword'] . "%' or pw.lokasi_pekerjaan like '%" . $params['keyword'] . "%' or p.no_wp like '%" . $params['keyword'] . "%')";
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 2";
		}

		$sql = " 
		select
		p.*
		, ps.`level` 
		, ps.status 
		, pw.lokasi_pekerjaan 
		, pw.uraian_pekerjaan 
		, pw.tgl_akhir
		, pw.tgl_awal
		, ph.perusahaan as nama_vendor
		, pv.id as permit_verifikasi
		from permit p 
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_work pw 
			on pw.permit = p.id 
		join permit_status ps_awal
			on ps_awal.permit = p.id 
			and ps_awal.`level` = 1 and ps_awal.status = 'APPROVED'
		join `user` u_app
			on u_app.id = ps_awal.`user` 
		join pegawai pg
			on pg.id = u_app.pegawai 
		join pemohon ph
			on ph.id = p.pemohon 
		left join (select max(id) id, permit from permit_verifikasi group by permit) p_ver_max
			on p_ver_max.permit = p.id 
		left join permit_verifikasi pv
			on pv.id = p_ver_max.id
		where ps.status = 'APPROVED'	
		and pg.upt = " . $params['data_user']['upt'] . "	
		-- and NOT EXISTS (select * from permit_transaction_log ptl where ptl.permit = p.id and ptl.group_transaction=2)
		" . $filter_data . "
		" . $filter_cari . "
		order by p.id desc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value["jam_sidak"] = "-";
				$value["latitude"] = "-";
				$value["longitude"] = "-";
				$value["sidak_user"] = "-";
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataSudahSidak($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND p.id < " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			$filter_cari = " and (pw.uraian_pekerjaan like '%" . $params['keyword'] . "%' or pw.lokasi_pekerjaan like '%" . $params['keyword'] . "%' or p.no_wp like '%" . $params['keyword'] . "%')";
		}

		$filterUserSidak = '';
		if(isset($params['nip'])){
			if ($params['nip'] != '') {
				$filterUserSidak = " and ptl.createdby = '".$params['user_id']."'";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 2";
		}

		$sql = " 

		select 
		p.*
		, ps.`level` 
		, ps.status 
		, pw.lokasi_pekerjaan 
		, pw.uraian_pekerjaan 
		, pw.tgl_akhir
		, pw.tgl_awal
		, ph.perusahaan as nama_vendor
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_status ps_awal
			on ps_awal.permit = p.id 
			and ps_awal.`level` = 1 and ps_awal.status = 'APPROVED'
		join `user` u_app
			on u_app.id = ps_awal.`user` 
		join pegawai pg
			on pg.id = u_app.pegawai
		join permit_work pw 
			on pw.permit = p.id 
		join (select max(id) id, permit from permit_transaction_log group by permit) p_log_max
			on p_log_max.permit = p.id 
		join permit_transaction_log ptl
			on ptl.id = p_log_max.id
		join pemohon ph
			on ph.id = p.pemohon 
		where ptl.group_transaction = 2
		and pg.upt = " . $params['data_user']['upt'] . "
		" . $filter_data . "
		" . $filterUserSidak . "
		" . $filter_cari . "
		order by p.id desc
		" . $filter_limit;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value["jam_sidak"] = "-";
				$value["latitude"] = "-";
				$value["longitude"] = "-";
				$value["sidak_user"] = "-";
				$value['query'] = $sql;
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDetailData($params)
	{
		$sql = "
		select 
		p.*
		, ph.nama_pemohon 
		, ph.perusahaan 
		, ph.no_hp 
		, ph.no_telp 
		, ph.jabatan 
		, ph.alamat 
		, pw.tgl_awal 
		, pw.tgl_akhir
		, p.jabatan_penanggung_jawab 
		, p.pengawas_k3 
		, p.jabatan_pengawas_k3
		, p.jabatan_pengawas_pekerjaan
		, pw.uraian_pekerjaan 
		, pw.lokasi_pekerjaan 
		, wp.jenis as tempat
		, ut.nama as tempat_kerja_upt
		, gi.nama_gardu as tempat_kerja_gardu
		, su.nama as tempat_kerja_ultg
		, st.nama as tempat_kerja_sutt
		, gib.bay as tempat_kerja_bay
		, case
			when wp.jenis = 'UNIT'
				THEN ut.nama 
			when wp.jenis = 'SUB UNIT'
				THEN su.nama 
			when wp.jenis = 'GARDU INDUK'
				THEN gi.nama_gardu 
			when wp.jenis = 'SUTET / SUTT'
				THEN st.nama
			when wp.jenis = 'BAY'
				THEN gib.bay 
		end as tempat_kerja
		, pw.keterangan_need_sistem 
		, pw.is_need_sistem 
		, ptl.lat 
		, ptl.lng 
		, ph.perusahaan as nama_vendor
		, pv.id as permit_verifikasi
		from permit p
		join pemohon ph
			on ph.id = p.pemohon
		join permit_work pw 
			on pw.permit = p.id
		join work_place wp 
			on wp.id = pw.work_place 
		left join upt ut
			on ut.id = pw.place 
		left join gardu_induk gi 
			on gi.id = pw.place 
		left join sub_upt su 
			on su.id = pw.place 
		left join sutet st
			on st.id = pw.place
		left join gardu_induk_bay gib 
			on gib.id = pw.place 
		left join (select max(id) id, permit from permit_transaction_log group by permit) p_log_max
			on p_log_max.permit = p.id 
		left join permit_transaction_log ptl
			on ptl.id = p_log_max.id
			left join (select max(id) id, permit from permit_verifikasi group by permit) p_ver_max
			on p_ver_max.permit = p.id 
			left join permit_verifikasi pv
			on pv.id = p_ver_max.id
		where p.no_wp = '" . $params['no_wp'] . "'";

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}
}
