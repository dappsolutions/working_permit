
<?php

class M_grafik extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataMonthString($month)
	{
		$str = "";
		switch ($month) {
			case 'Januari':
				$str = "01";
				break;
			case 'Februari':
				$str = "02";
				break;
			case 'Maret':
				$str = "03";
				break;
			case 'April':
				$str = "04";
				break;
			case 'Mei':
				$str = "05";
				break;
			case 'Juni':
				$str = "06";
				break;
			case 'Juli':
				$str = "07";
				break;
			case 'Agustus':
				$str = "08";
				break;
			case 'September':
				$str = "09";
				break;
			case 'Oktober':
				$str = "10";
				break;
			case 'November':
				$str = "11";
				break;
			case 'Desember':
				$str = "12";
				break;

			default:
				# code...
				break;
		}

		return $str;
	}


	public function getDataGrupPengajuan($params)
	{

		// echo '<pre>';
		// print_r($params);
		// die;
		$month = $this->getDataMonthString($params['month']);
		// $month = $params['month'] < 10 ? '0' . $params['month'] : $params['month'];
		// echo $month;
		// die;

		$filter_upt = "";
		if ($params['upt'] != '9') {
			$filter_upt = "and p.upt = " . $params['upt'] . "";
		}

		$sql = "
		select 
		distinct
		phs.nama_swa 
		, p.id as pegawai
		, p.nip
		from permit_has_swa phs
		join `user` u  
			on u.id = phs.createdby
		join pegawai p 
			on p.id = u.pegawai  
		where phs.createddate like '%" . $params['year'] . "-" . $month . "%' and phs.deleted = 0
		" . $filter_upt;

		// echo '<pre>';
		// echo $sql;
		// die;

		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getData($params)
	{
		$data_group_pengajuan = $this->getDataGrupPengajuan($params);
		$params['month'] = $this->getDataMonthString($params['month']);
		// echo '<pre>';
		// print_r($data_group_pengajuan);
		// die;
		$filter_upt = "";
		if ($params['upt'] != '9') {
			$filter_upt = "and p.upt = " . $params['upt'] . "";
		}

		$result = array();
		foreach ($data_group_pengajuan as $key => $value) {
			$month = $params['month'];
			$date = $params['year'] . '-' . $month;
			$sql = "select 
			count(*) as total
			from permit_has_swa phs
			join `user` u  
				on u.id = phs.createdby
			join pegawai p 
				on p.id = u.pegawai  
			where phs.createddate like '%" . $date . "%' and phs.deleted = 0
			and p.id = " . $value['pegawai'] . "
			" . $filter_upt;

			$data = $this->db->query($sql);
			$push = array();
			$push['month'] = substr($value['nama_swa'], 0, 7);
			$push['total'] = $data->row_array()['total'];
			array_push($result, $push);
		}
		// for ($i = 1; $i < 13; $i++) {
		// 	$month = $i < 10 ? '0' . $i : $i;
		// 	$date = $params['year'] . '-' . $month;
		// 	$sql = "select 
		// 	count(*) as total
		// 	from permit_has_swa phs
		// 	join `user` u  
		// 		on u.id = phs.createdby
		// 	join pegawai p 
		// 		on p.id = u.pegawai  
		// 	where phs.createddate like '%" . $date . "%' and phs.deleted = 0
		// 	and p.upt = " . $params['data_user']['upt'];

		// 	// echo '<pre>';
		// 	// echo $sql;
		// 	// die;
		// 	$data = $this->db->query($sql);
		// 	$push = array();
		// 	$monthstr = $this->getDataMonthString($month);
		// 	$push['month'] = $monthstr;
		// 	$push['total'] = $data->row_array()['total'];
		// 	array_push($result, $push);
		// }

		return $result;
	}

	public function getListUpt()
	{
		$sql = "select * from upt where deleted = 0";
		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}

			$push = array();
			$push['id'] = $value['id'] + 1;
			$push['nama'] = 'SEMUA UPT';
			$push['createddate'] = date('Y-m-d H:i:s');
			$push['createdby'] = null;
			$push['updateddate'] = null;
			$push['updatedby'] = null;
			$push['deleted'] = '0';
			array_push($result, $push);
		}

		return $result;
	}

	public function getDataGrupPengajuanTopTen($params)
	{

		$sql = "
		select 
		distinct
		phs.nama_swa 
		, p.id as pegawai
		, p.nip
		from permit_has_swa phs
		join `user` u  
			on u.id = phs.createdby
		join pegawai p 
			on p.id = u.pegawai  
		where phs.createddate like '%" . $params['year'] . "-" . $params['month'] . "%' and phs.deleted = 0
		and p.upt = " . $params['data_user']['upt'] . "
		limit 10";

		// echo '<pre>';
		// echo $sql;
		// die;

		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataGrupPengajuanUpt($params)
	{
		$filter_month = "and phs.createddate like '%" . $params['year'] . "-" . $params['month'] . "%'";
		$filterUpt = "and ut.id != '9' and ut.id != ''";
		$sql = "
		select 
		distinct
		ut.nama as nama_swa 
		, ut.id as pegawai
		, ut.id as nip
		from permit_has_swa phs
		join `user` u  
			on u.id = phs.createdby
		join pegawai p 
			on p.id = u.pegawai  
		join upt ut
			on ut.id = p.upt
		where phs.deleted = 0
		" . $filterUpt . "
		" . $filter_month;

		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataTopTen($params)
	{
		// $data_group_pengajuan = $this->getDataGrupPengajuanTopTen($params);

		$data_group_pengajuan = array();
		// echo '<pre>';
		// print_r($params);
		// die;

		$is_upt = true;
		if (isset($params['month']) && isset($params['upt'])) {
			if ($params['month'] != '' && ($params['upt'] == '9' || $params['upt'] == '')) {
				$data_group_pengajuan = $this->getDataGrupPengajuanUpt($params);
				$is_upt = true;
			} else {
				$data_group_pengajuan = $this->getDataGrupPengajuan($params);
				$is_upt = false;
			}
		}

		// echo '<pre>';
		// print_r($data_group_pengajuan);
		// die;

		$filter_upt = "";
		if (isset($params['upt'])) {
			if ($params['upt'] != '9' && $params['upt'] != '') {
				$filter_upt = "and pg.upt = " . $params['upt'] . "";
			}
		}

		$result = array();
		foreach ($data_group_pengajuan as $key => $value) {
			$date = $params['year'] . "-" . $params['month'];

			$filterData = "and pg.id = " . $value['pegawai'] . "";
			if ($is_upt) {
				$filterData = "and ut.nama = '" . $value['nama_swa'] . "'";
			}

			$sql = "
					SELECT 
					count(*) as total
		FROM `permit` `p`
		JOIN `pemohon` `ph` ON `ph`.`id` = `p`.`pemohon`
		JOIN `permit_work` `pw` ON `pw`.`permit` = `p`.`id`
		JOIN `work_place` `wp` ON `pw`.`work_place` = `wp`.`id`
		JOIN (select id, permit from permit_status where status = 'DRAFT' group by permit) pss ON `p`.`id` = `pss`.`permit`
		JOIN `permit_status` `ps` ON `pss`.`id` = `ps`.`id`
		JOIN (select max(id) id, permit from permit_status group by permit) pss_s ON `p`.`id` = `pss_s`.`permit`
		JOIN `permit_status` `pst` ON `pss_s`.`id` = `pst`.`id`
		JOIN `permit_purpose` `pp` ON `p`.`id` = `pp`.`permit`		
		JOIN `permit_transaction_log` `ptl_swa` ON `ptl_swa`.`permit` = `p`.`id` and `ptl_swa`.`group_transaction` = 2
		JOIN `permit_has_swa` `phs` ON `phs`.`permit_transaction_log` = `ptl_swa`.`id` and `phs`.`deleted` = 0
		JOIN `permit_has_swa_pekerjaan` `phswp` ON `phswp`.`permit_transaction_log` = `ptl_swa`.`id`
		JOIN `permit_has_swa_kelengkapan` `phsk` ON `phsk`.`permit_transaction_log` = `ptl_swa`.`id`
		join `user` u  
			on u.id = phs.createdby
		join pegawai pg 
			on pg.id = u.pegawai
		JOIN `upt` `ut` ON `ut`.`id` = `pg`.`upt`
		WHERE `p`.`deleted` =0 ".$filterData." and `pst`.`status` != 'DRAFT'
		".$filter_upt."
		and phs.createddate like '%" . $date . "%'";

			// $sql = "select 
			// count(*) as total
			// from permit_has_swa phs
			// join `user` u  
			// 	on u.id = phs.createdby
			// join pegawai p 
			// 	on p.id = u.pegawai  
			// join upt ut
			// 	on ut.id = p.upt
			// where phs.createddate like '%" . $date . "%' and phs.deleted = 0
			// " . $filterData . "
			// " . $filter_upt . "
			// ";


			// echo $sql;
			// die;

			$data = $this->db->query($sql);
			$push = array();
			$push['month'] = substr($value['nama_swa'], 0, 7);
			$push['month'] = str_replace('UPT', '', $push['month']);
			$push['total'] = $data->row_array()['total'];
			array_push($result, $push);
		}
		return $result;
	}
}
