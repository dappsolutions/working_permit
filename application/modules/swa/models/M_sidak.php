
<?php

class M_sidak extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function simpan($params, $userData = [])
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$this->db->trans_begin();
		try {
			//code...
			$push = array();
			$push['no_trans'] = $params['no_trans'];
			$push['permit'] = $params['data_wp']['id'];
			$push['group_transaction'] = 2;
			$push['lat'] = $params['lat'];
			$push['lng'] = $params['lng'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('permit_transaction_log', $push);

			$log_id = $this->db->insert_id();

			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['wp'] = $params['data_apd']['wp'];
			$push['sop'] = $params['data_apd']['sop'];
			$push['jsa'] = $params['data_apd']['jsa'];
			$push['pengawas_k3'] = $params['data_apd']['pengawas_k3'];
			$push['sertifikat_kom'] = $params['data_apd']['sertifikat_kom'];
			$push['peralatan'] = $params['data_apd']['peralatan'];
			$push['rambu_k3'] = $params['data_apd']['rambu_k3'];
			$push['apd'] = $params['data_apd']['apd'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$push['permit_transaction_log'] = $log_id;
			$this->db->insert('permit_has_swa_kelengkapan', $push);



			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['nama_swa'] = $params['data_swa']['nama_swa'];
			$push['email_swa'] = $params['data_swa']['email_swa'];
			$push['lokasi_pekerjaan'] = $params['data_swa']['lokasi_pekerjaan'];
			$push['uraian_pekerjaan'] = $params['data_swa']['uraian_pekerjaan'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$push['permit_transaction_log'] = $log_id;
			if(!empty($userData)){
				$push['tipe'] = $userData['tipe'];
			}
			$this->db->insert('permit_has_swa', $push);

			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['jam_pekerjaan_dihentikan'] = $params['data_pekerjaan']['jam_pekerjaan_dihentikan'];
			$push['jam_pekerjaan_dilanjutkan'] = $params['data_pekerjaan']['jam_pekerjaan_dilanjutkan'];
			$push['ket_pekerjaan_dihentikan'] = $params['data_pekerjaan']['ket_pekerjaan_dihentikan'];
			$push['temuan_1'] = $params['data_pekerjaan']['temuan_1'];
			$push['jenis_temuan_1'] = $params['data_pekerjaan']['jenis_temuan_1'];
			$push['temuan_2'] = $params['data_pekerjaan']['temuan_2'];
			$push['jenis_temuan_2'] = $params['data_pekerjaan']['jenis_temuan_2'];
			$push['rekomendasi'] = $params['data_pekerjaan']['rekomendasi'];
			$push['nama_pengawas_k3'] = $params['data_pekerjaan']['nama_pengawas_k3'];
			$push['nama_perusahaan'] = $params['data_pekerjaan']['nama_perusahaan'];
			$push['ttd_k3'] = $params['data_pekerjaan']['ttd_k3'];
			$push['ttd_swa'] = $params['data_pekerjaan']['ttd_swa'];
			$push['ket_melanjutkan_pekerjaan'] = $params['data_pekerjaan']['ket_melanjutkan_pekerjaan'];
			$push['status_temuan_1'] = $params['data_pekerjaan']['status_temuan_1'];
			$push['status_temuan_2'] = $params['data_pekerjaan']['status_temuan_2'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$push['permit_transaction_log'] = $log_id;
			$this->db->insert('permit_has_swa_pekerjaan', $push);

			if(isset($params['data_image'])){
				if (!empty($params['data_image'])) {
					foreach ($params['data_image'] as $val) {
						$push = array();
						$push['permit_transaction_log'] = $log_id;
						$push['picture'] = $val->encodeImages;
						$this->db->insert('permit_has_sidak_detail_pict', $push);
					}
				}
			}


			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function simpanGambar($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$dataTrans = $this->getDetailTransaksi($params);

		$this->db->trans_begin();
		try {

			$push = array();
			$push['permit_transaction_log'] = $dataTrans['id'];
			$push['picture'] = $params['image'];
			$this->db->insert('permit_has_sidak_detail_pict', $push);
			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDetailTransaksi($params)
	{
		$sql = "select * from permit_transaction_log where no_trans = '" . $params['no_trans'] . "'";
		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function batalPengajuan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$dataTrans = $this->getDetailTransaksi($params);
		// echo '<pre>';
		// print_r($dataTrans);die;

		$this->db->trans_begin();
		try {
			$this->db->delete('permit_has_swa_kelengkapan', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_has_swa', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_has_swa_pekerjaan', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_has_sidak_detail_pict', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_transaction_log', array('id' => $dataTrans['id']));
			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDataSafety($params)
	{
		$filter = "";
		if (isset($params['ptlid'])) {
			$filter = "and ptl.no_trans = '" . $params['ptlid'] . "'";
			$sql = "
		
		select 
		phs.*
		, phsk.*
		, phsp.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id " . $filter . "
		join permit_has_swa phs 
			on phs.permit = p.id and ptl.id = phs.permit_transaction_log
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id and ptl.id = phsk.permit_transaction_log
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id and ptl.id = phsp.permit_transaction_log
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'";

			// echo '<pre>';
			// echo $sql;
			// die;
		} else {
			$sql = "
		
		select 
		phs.*
		, phsk.*
		, phsp.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join (select max(id) id, permit from permit_transaction_log group by permit) p_log_max
			on p_log_max.permit = p.id 
		join permit_transaction_log ptl
			on ptl.id = p_log_max.id 
		join (select max(id) id, permit from permit_has_swa group by permit) phs_max
			on phs_max.permit = p.id 
		join permit_has_swa phs 
			on phs_max.id = phs.id and p.id = phs.permit
		join (select max(id) id, permit from permit_has_swa_kelengkapan group by permit) phsk_max
			on phsk_max.permit = p.id 
		join permit_has_swa_kelengkapan phsk 
			on phsk_max.id = phsk.id and p.id = phsk.permit
		join (select max(id) id, permit from permit_has_swa_pekerjaan group by permit) phsp_max
			on phsp_max.permit = p.id 
		join permit_has_swa_pekerjaan phsp  
			on phsp_max.id = phsp.id and p.id = phsp.permit
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'";
		}


		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataSidakImage($params)
	{

		$filter = "";
		if (isset($params['ptlid'])) {
			$filter = "and ptl.no_trans = '" . $params['ptlid'] . "'";
			$sql = "
			select 
			ptl.id
			, phsdp.picture 
			, ptl.no_trans
			from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id " . $filter . "
		join permit_has_swa phs 
			on phs.permit = p.id and ptl.id = phs.permit_transaction_log
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id and ptl.id = phsk.permit_transaction_log
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id and ptl.id = phsp.permit_transaction_log
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 2
			and p.no_wp = '" . $params['no_wp'] . "'";
		} else {
			$sql = "
			select 
			ptl.id
			, phsdp.picture 
			, ptl.no_trans
			from permit p
			join (select max(id) id, permit from permit_status group by permit) pt_max
				on pt_max.permit = p.id
			join permit_status ps 
				on ps.id = pt_max.id and ps.`level` =3
			join (select max(id) id, permit from permit_transaction_log group by permit) p_log_max
				on p_log_max.permit = p.id 
			join permit_transaction_log ptl
				on ptl.id = p_log_max.id 
			join (select max(id) id, permit from permit_has_swa group by permit) phs_max
				on phs_max.permit = p.id 
			join permit_has_swa phs 
				on phs_max.id = phs.id and p.id = phs.permit
			join (select max(id) id, permit from permit_has_swa_kelengkapan group by permit) phsk_max
				on phsk_max.permit = p.id 
			join permit_has_swa_kelengkapan phsk 
				on phsk_max.id = phsk.id and p.id = phsk.permit
			join (select max(id) id, permit from permit_has_swa_pekerjaan group by permit) phsp_max
				on phsp_max.permit = p.id 
			join permit_has_swa_pekerjaan phsp  
				on phsp_max.id = phsp.id and p.id = phsp.permit
			join permit_has_sidak_detail_pict phsdp
				on phsdp.permit_transaction_log = ptl.id
			where ptl.group_transaction = 2
			and p.no_wp = '" . $params['no_wp'] . "'";
		}


		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getDataSafetyAll($params)
	{
		$sql = "
		
		select 
		phs.*
		, phsk.*
		, phsp.*
		, ptl.no_trans
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id 
		join permit_has_swa phs 
			on phs.permit = p.id and ptl.id = phs.permit_transaction_log 
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id and ptl.id = phsk.permit_transaction_log 
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id and ptl.id = phsp.permit_transaction_log 
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'
		order by ptl.id asc";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				$params['ptlid'] = $val['no_trans'];
				$val['data_image'] = $this->getDataSidakImage($params);
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getDataDetailSidakPengajuan($params)
	{
		$sql = "
		select 
		ptl.id
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id 
		join permit_has_swa phs 
			on phs.permit = p.id 
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id 
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'
		order by ptl.id ASC ";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getStrukturAccPertama($params)
	{
		$sql = "
		select 
			p.upt 
			from role_apps_akses raa
			join `user` u 
				on u.id = raa.`user` 
			join pegawai p 
				on p.id = u.pegawai 
			join upt ut
				on ut.id = p.upt 
			join apps a 
				on a.id = raa.apps 
			where u.id = " . $params['user_id'] . "
			and  a.aplikasi = 'SWA'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$data_struktur = $this->getDataStruktur($data);
			$result = $data_struktur;
		}

		return $result;
	}

	public function getDataStruktur($params)
	{
		$sql = "select 
		sa.*
		, u.username as email
		from struktur_approval sa
		join `user` u 
			on u.id = sa.`user` 
		where sa.upt = " . $params['upt'] . "
		and sa.`level` = 1
		and sa.deleted = 0
		LIMIT 1";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$result = $data;
		}

		return $result;
	}
}
