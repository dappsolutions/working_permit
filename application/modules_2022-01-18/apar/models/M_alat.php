
<?php

class M_alat extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				$filterWilayah = " and w.nama_wilayah = 'GI'";
			}
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = atl.jenis_alat 
		join merk_alat ma 
			on ma.id = atl.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		order by atl.id desc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataFilter($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filterDate = "";
		if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] != '') {
			$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
			$params['tanggal_akhir'] = date('Y-m-d', strtotime($params['tanggal_akhir']));
			$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_akhir'] . "')";
		}

		if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] == '') {
			$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
			$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_awal'] . "')";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				$filterWilayah = " and w.nama_wilayah = 'GI'";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = atl.jenis_alat 
		join merk_alat ma 
			on ma.id = atl.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		" . $filterDate . "
		order by atl.id desc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataNotifikasiApar($params = array(), $limit = true)
	{

		$dateNotifFilter = "and atp.createddate is null";
		$dateNow = date('Y-m-d');
		list($year, $month, $day) = explode('-', $dateNow);
		if (isset($params['wilayah'])) {
			if (intval($day) == 20) {
				if ($params['wilayah'] == 'ULTG') {
					$dateNotifFilter = "and (cast(atp.createddate as date) < '" . $dateNow . "' or atp.createddate is null)";
				}
			}elseif(intval($day) >= 25){
				if ($params['wilayah'] == 'ULTG' || $params['wilayah'] == 'UPT') {
					$dateNotifFilter = "and (cast(atp.createddate as date) < '" . $dateNow . "' or atp.createddate is null)";
				}
			}
		}

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				$filterWilayah = " and w.nama_wilayah = 'GI'";
			}
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		, atp.createddate as tanggal_inspeksi_terakhir
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = atl.jenis_alat 
		join merk_alat ma 
			on ma.id = atl.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0	
		".$dateNotifFilter."	
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		order by atl.id desc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataFilterRabApar($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND atl.id > " . $params["last_id"];
		}


		$filter_cari = "";
		if (isset($params['keyword'])) {
			if ($params['keyword'] != '') {
				$filter_cari = " and (atl.no_alat like '%" . $params['keyword'] . "%' or w.nama_wilayah like '%" . $params['keyword'] . "%' or la.nama_lokasi like '%" . $params['keyword'] . "%' or lp.tempat like '%" . $params['keyword'] . "%')";
			}
		}

		$filterDate = "";
		if(isset($params['tanggal_awal']) && isset($params['tanggal_akhir'])){
			if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] != '') {
				$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
				$params['tanggal_akhir'] = date('Y-m-d', strtotime($params['tanggal_akhir']));
				$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_akhir'] . "')";
			}
	
			if ($params['tanggal_awal'] != '' && $params['tanggal_akhir'] == '') {
				$params['tanggal_awal'] = date('Y-m-d', strtotime($params['tanggal_awal']));
				$filterDate = "and (cast(atp.createddate as date) >= '" . $params['tanggal_awal'] . "' and cast(atp.createddate as date) <= '" . $params['tanggal_awal'] . "')";
			}
		}

		$filterWilayah = "";
		if (isset($params['wilayah'])) {
			if ($params['wilayah'] == 'ULTG') {
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";
			} else {
				$filterWilayah = " and w.nama_wilayah = 'GI'";
			}
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 100";
		}

		$sql = " 		
		select 
		atl.id
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, ja.jenis 
		, ma.nama_merk 
		, atp.berat 
		, atp.period_start as tanggal
		, atp.pembersihan 
		, atp.tekanan 
		from alat atl
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join jenis_alat ja 
			on ja.id = atl.jenis_alat 
		join merk_alat ma 
			on ma.id = atl.merk_alat 
		left join (
			select max(id) id, alat from alat_transaksi_pemeliharaan group by alat
		) atp_max
			on atp_max.alat = atl.id
		left join 	alat_transaksi_pemeliharaan atp
			on atp.id = atp_max.id
		where atl.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		" . $filterWilayah . "
		" . $filterDate . "
		order by w.nama_wilayah, atl.no_alat, atp.createddate asc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}
}
