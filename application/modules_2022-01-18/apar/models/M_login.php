
<?php

class M_login extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function signIn($params)
	{
		$sql = "
		select rap.*
		, u.id as user_id
		, u.username as email
		, pg.nama as name
		, ut.id as upt_id
		, w.nama_wilayah
		from role_apps_akses rap 
		join `user` u 
			on u.id = rap.`user`
		join apps a 
			on a.id = rap.apps
		join pegawai pg
			on pg.id = u.pegawai			
		join upt ut
			on ut.id = pg.upt
		join role_apps_apar raa
			on raa.role_apps_akses = rap.id
		join wilayah w
			on w.id = raa.wilayah
		where a.aplikasi = 'peralatan'
		and rap.deleted = 0
		and u.username = '" . $params['username'] . "'
		and u.password = '" . $params['password'] . "'";

		$data = $this->db->query($sql);

		if (!empty($data->result_array())) {
			return $data->row_array();
		}

		return array();
	}
}
