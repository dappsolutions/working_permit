
<?php

class M_document extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($params = array(), $limit = true)
	{

		$filter_data = "";
		if (isset($params['last_id'])) {
			$filter_data = $params["last_id"] == "" ? "" : " AND d.id > " . $params["last_id"];
		}

		// echo $filter_data;die;

		$filter_cari = "";
		if (isset($params['keyword'])) {
			$filter_cari = " and (pw.uraian_pekerjaan like '%" . $params['keyword'] . "%' or pw.lokasi_pekerjaan like '%" . $params['keyword'] . "%' or p.no_wp like '%" . $params['keyword'] . "%')";
		}

		$filter_limit = "";
		if ($limit) {
			$filter_limit = "LIMIT 1";
		}

		$filterWilayah = "";
		if(isset($params['wilayah'])){
			if($params['wilayah'] == 'ULTG'){
				$filterWilayah = " and (w.nama_wilayah = 'ULTG' or w.nama_wilayah = 'GI')";	
			}else{
				$filterWilayah = " and w.nama_wilayah = 'GI'";	
			}
		}

		$sql = " 
		select 
		d.id
		, d.no_document
		, atl.no_alat
		, w.nama_wilayah 
		, la.nama_lokasi 
		, lp.tempat 
		, atp.period_start
		, p.nama as nama_pegawai
		, d.createddate
		, dt.status 
		from document d
		join actor a 
			on a.id = d.createdby
		join (
			select min(id) id, document from alat_transaksi_pemeliharaan group by document
		) al_min
			on al_min.document = d.id
		join alat_transaksi_pemeliharaan atp 
			on atp.id= al_min.id
		join (
			select max(id) id, document from document_transaction group by document
		) doc_t_max
			on doc_t_max.document = d.id
		join document_transaction dt 
			on dt.id = doc_t_max.id
		join alat atl
			on atl.id = atp.alat
		join lokasi_penempatan_alat lpa 
			on lpa.alat = atl.id 
		join lokasi_penempatan lp 
			on lp.id  = lpa.lokasi_penempatan 
		join lokasi_tujuan lt 
			on lt.id = lp.lokasi_tujuan 
		join lokasi_apar la 
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		join `user` u 
			on u.id = a.`user` 
		join pegawai p 
			on p.id = u.pegawai 
		where d.deleted = 0
		" . $filter_data . "
		" . $filter_cari . "
		".$filterWilayah."
		order by d.id asc
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}
}
