<?php
// ini_set("memory_limit",-1);
class Alat extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_alat', 'alat');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'apar/alat';
	}

	public function index()
	{
		echo 'alat ' . date('Y-m-d H:i:s');
	}

	public function getData(){
		$data = $_POST;
		$data = $this->alat->getData($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataFilter(){
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		$data = $this->alat->getDataFilter($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataFilterRabApar(){
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		// $data = [];
		$data = $this->alat->getDataFilterRabApar($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
	public function getDataAllAparNotifikasiBelumInspeksi(){
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		$data = $this->alat->getDataNotifikasiApar($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
}
