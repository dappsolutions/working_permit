<div class="box">
 <div class="box-body">
  <div class="row">
   <div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
     <div class="inner">
      <h3 id="total_draft"><?php echo '' ?></h3>

      <p>UPT</p>
     </div>
     <div class="icon">
      <!--<i class="ion ion-android-menu"></i>-->
     </div>
     <a href="<?php echo base_url() . $module . '/event/' . $unit . '/upt' ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
   </div>
   <div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
     <div class="inner">
      <h3 id="total_draft"><?php echo '' ?></h3>

      <p>Gardu Induk</p>
     </div>
     <div class="icon">
      <!--<i class="ion ion-android-menu"></i>-->
     </div>
     <a href="<?php echo base_url() . $module . '/subunitgardu/' . $unit ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
   </div>
   <div class="col-lg-4 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
     <div class="inner">
      <h3 id="total_draft"><?php echo '' ?></h3>

      <p>ULTG</p>
     </div>
     <div class="icon">
      <!--<i class="ion ion-android-menu"></i>-->
     </div>
     <a href="<?php echo base_url() . $module . '/subunitultg/' . $unit ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
   </div>
  </div>
 </div>
</div>