<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<?php $active_class = "" ?>
				<?php if (!isset($jenis_pekerjaan_param)) { ?>
					<?php $no = 1; ?>
					<?php foreach ($data_jenis_pekerjaan as $key => $value) { ?>
						<?php if ($no == 1) { ?>
							<li class="active"><a href="#form-<?php echo $no ?>" no_wp="<?php echo $no_wp ?>" jenis_pekerjaan="<?php echo $value['jenis_pekerjaan'] ?>" onclick="LapSimson.getData(this, event)" permit="<?php echo  $permit ?>" data-toggle="tab"><?php echo $value['jenis_pekerjaan'] ?></a></li>
						<?php } else { ?>
							<li class=""><a href="#form-<?php echo $no ?>" no_wp="<?php echo $no_wp ?>" jenis_pekerjaan="<?php echo $value['jenis_pekerjaan'] ?>" onclick="LapSimson.getData(this, event)" permit="<?php echo $permit ?>" data-toggle="tab"><?php echo $value['jenis_pekerjaan'] ?></a></li>
						<?php } ?>
						<?php $no += 1 ?>
					<?php } ?>
				<?php } else { ?>
					<?php $no = 1; ?>
					<?php foreach ($data_jenis_pekerjaan as $key => $value) { ?>
						<?php if ($value['jenis_pekerjaan'] == $jenis_pekerjaan_param) { ?>
							<li class="active"><a href="#form-<?php echo $no ?>" no_wp="<?php echo $no_wp ?>" jenis_pekerjaan="<?php echo $value['jenis_pekerjaan'] ?>" onclick="LapSimson.getData(this, event)" permit="<?php echo  $permit ?>" data-toggle="tab"><?php echo $value['jenis_pekerjaan'] ?></a></li>
						<?php } else { ?>
							<li class=""><a href="#form-<?php echo $no ?>" no_wp="<?php echo $no_wp ?>" jenis_pekerjaan="<?php echo $value['jenis_pekerjaan'] ?>" onclick="LapSimson.getData(this, event)" permit="<?php echo  $permit ?>" data-toggle="tab"><?php echo $value['jenis_pekerjaan'] ?></a></li>
						<?php } ?>
					<?php } ?>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="tab-content">
			<?php $no = 1 ?>
			<?php foreach ($data_jenis_pekerjaan as $key => $value) { ?>
				<div class="tab-pane <?php echo $no == 1 ? 'active' : '' ?>" id="form-<?php echo $no ?>">
					<div class="nav-tabs-custom">
						<ul class="nav nav-tabs">
							<?php $i = 1 ?>
							<?php foreach ($data_sidak_all as $v_trans) { ?>
								<?php if ($i == 1) { ?>
									<li class="active"><a href="#formtrans-<?php echo $v_trans['no_trans'] ?>" data-toggle="tab"><?php echo $v_trans['no_trans'] ?></a></li>
								<?php } else { ?>
									<li class=""><a href="#formtrans-<?php echo $v_trans['no_trans'] ?>" data-toggle="tab"><?php echo $v_trans['no_trans'] ?></a></li>
								<?php } ?>
								<?php $i += 1 ?>
							<?php } ?>
						</ul>
					</div>
				</div>
				<?php $no += 1 ?>
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content">
						<?php $x = 1 ?>
						<?php foreach ($data_sidak_all as $v_sidak) { ?>
							<?php $data_sidak = $v_sidak ?>
							<div class="tab-pane <?php echo $x == 1 ? 'active' : '' ?>" id="formtrans-<?php echo $v_sidak['no_trans'] ?>">
								<div class="row">
									<div class="col-md-12">
										<a class="btn btn-danger" href="<?php echo base_url() ?>simson/sidak/cetak?permit=<?php echo $permit ?>&no_wp=<?php echo $no_wp ?>&no_trans=<?php echo $v_sidak['no_trans'] ?>">CETAK PDF</a>
									</div>
								</div>
								<br>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h5>Safety Condition</h5>
											</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<?php if (!empty($data_sidak)) { ?>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Kondisi Tanah</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['kondisi_tanah'] == '' ? '-' : $data_sidak['kondisi_tanah'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Tegangan TK</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['tegangan_tk'] == '' ? '-' : $data_sidak['tegangan_tk'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Kondisi Lingkungan</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['kondisi_lingkungan'] == '' ? '-' : $data_sidak['kondisi_lingkungan'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Lokasi</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['lokasi'] == '' ? '-' : $data_sidak['lokasi'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Kondisi Cuaca</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['kondisi_cuaca'] == '' ? '-' : $data_sidak['kondisi_cuaca'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Status</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['status'] == '' ? '-' : $data_sidak['status'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Ketinggian TK</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['ketinggian_tk'] == '' ? '-' : $data_sidak['ketinggian_tk'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Score</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_sidak['score'] == '' ? '-' : $data_sidak['score'] ?></label>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h5>Kelengkapan & Peralatan APD</h5>
											</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<?php if (!empty($data_sidak)) { ?>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Body Harnes</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['body_harnes'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Lanyard</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['lanyard'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Hook / Step</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['hook_or_step'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Pullstrap</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['pullstrap'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Safety Helmet</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['safety_helmet'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Kacamata UV</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['kacamata_uv'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Earplug</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['earplug'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Masker</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['masker'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Wearpack</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['wearpack'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Sepatu Safety</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['sepatu_safety'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Sarung Tangan</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['sarung_tangan'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Stick Grounding</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['stick_grounding'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Grounding Cable</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['grounding_cable'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Stick Isolasi</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['stick_isolasi'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Voltage Detector</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['voltage_detector'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Tambang</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['tambang'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Tagging</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['tagging'] == 'yes' ? 'checked' : '' ?>>
																</div>
																<div class="col-md-2">
																	<label for="">Pembatas Area</label>
																</div>
																<div class="col-md-4">
																	<input type="checkbox" <?php echo $data_sidak['pembatas_area'] == 'yes' ? 'checked' : '' ?>>
																</div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h5>Gambar Sidak</h5>
											</div>
											<div class="panel-body">
												<div class="row">
													<div class="col-md-12">
														<?php if (!empty($data_image)) { ?>
															<div class="row">
																<?php foreach ($data_image as $v_image) { ?>
																	<?php if ($v_image['no_trans'] == $data_sidak['no_trans']) { ?>
																		<div class="col-md-6">
																			<br>
																			<img src="data:image/jpeg;base64,<?php echo $v_image['picture'] ?>" alt="">
																		</div>
																	<?php } ?>
																<?php } ?>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php $x += 1 ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
