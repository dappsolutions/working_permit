<?php

class Sidak extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_sidak', 'sidak');
		$this->load->model('m_permit', 'permit');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/lapsimson.js"></script>'
		);

		return $data;
	}

	public function index()
	{
		echo 'sidak ' . date('Y-m-d H:i:s');
	}

	public function simpan()
	{
		$data = $_POST;
		$data_wp = $this->permit->getDetailData($data);
		$data_image = (array) json_decode($_POST['data_image']);
		$data_apd = (array) json_decode($_POST['data_apd']);
		$data_safety = (array) json_decode($_POST['data_safety']);
		$data_struktur_acc = $this->sidak->getStrukturAccPertama($data);
		$data['data_wp'] = $data_wp;
		$data['data_image'] = $data_image;
		$data['data_apd'] = $data_apd;
		$data['data_safety'] = $data_safety;
		$data['no_trans'] = Modules::run('no_generator/generateNoTransLog');
		$data['penilaian'] = $this->cekHasilPenilaianSimpan($data);
		$data['struktur_acc'] = $data_struktur_acc;
		$result = $this->sidak->simpan($data);

		if ($result['is_valid'] == '1') {
			$this->sendEmail($data);
		}

		// echo '<pre>';
		// print_r($data['data_image']);
		// die;
		echo json_encode($result);
	}


	public function sendEmail($params = array())
	{
		if (!empty($params['struktur_acc'])) {
			$params['subject'] = 'Formulir Simson';
			$params['no_wp'] = $params['no_wp'];
			$params['id_wp'] = $params['data_wp']['id'];
			$params['email'] = $params['struktur_acc']['email'];
			$params['message'] = '<p>Berikut kami sampaikan laporan Simson, Silkan klik link dibawah ini untuk melihat formulir</p>
			<p>Semoga PLN menjadi yang terbaik</p>
			<p>Amienn...</p>
			<p><a href="' . base_url() . 'simson/sidak/cetak?permit=' . $params['id_wp'] . '&no_wp=' . $params['no_wp'] . '">Formulir Simson</a></p>';

			$cc = array();
			// if (!empty($params['struktur_acc'])) {
			// 	$cc[] = $params['struktur_acc']['email'];
			// }
			Modules::run('email/send_email_data_other', $params['subject'], $params['message'], $params['email'], $cc);
		}
	}

	public function getDetailData()
	{
		$data = $_POST;
		$this->createFilePdf();
		$data_safety = $this->sidak->getDataSafety($data);
		$result['data_safety'] = $data_safety;
		$result['data_apd'] = $data_safety;
		Modules::run('simson/output/get', $result);
	}

	public function getDetailImage()
	{
		$data = $_POST;
		// $data['no_wp'] = 'WPINT20JAN002';
		$data_image = $this->sidak->getDataSidakImage($data);
		$result['data'] = $data_image;
		Modules::run('simson/output/get', $result);
	}

	public function getModuleName()
	{
		return 'simson/sidak';
	}

	public function showDetailPengajuan()
	{
		$data = $_GET;

		// echo '<pre>';
		// print_r($data);
		// die;
		$data['data_jenis_pekerjaan'] = $this->sidak->getDataJenisPekerjaanPermit($data);
		$data['jenis_pekerjaan'] = isset($data['jenis_pekerjaan_param']) ? $data['jenis_pekerjaan_param'] : $data['data_jenis_pekerjaan'][0]['jenis_pekerjaan'];
		$data['data_sidak_all'] = $this->sidak->getListDataSafety($data);
		$data['data_image'] = $this->sidak->getListDataSidakImage($data);
		// echo '<pre>';
		// print_r($data['data_sidak_all']);
		// die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengajuan";
		$data['title_content'] = "Detail Pengajuan";
		echo Modules::run('template', $data);
	}



	public function getDokumenDp3Aktif()
	{
		$data = Modules::run('database/get', array(
			'table' => 'dp3',
			'where' => "deleted = 0 and id = 2"
		));

		$data = $data->row_array();
		$data['berlaku_efektif'] = Modules::run('helper/getIndoDate', $data['berlaku_efektif']);
		return $data;
	}

	public function cetak()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_GET;
		$data_wp = $this->permit->getDetailData($data);
		$data_safety = $this->sidak->getDataSafetyByNoTrans($data);
		$data['data_safety'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImageByNoTrans($data);
		$data['dp3'] = $this->getDokumenDp3Aktif();

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$data['wp'] = $data_wp;
		$formulir_swa = $this->load->view('sidak/cetak/formulir_simson', $data, true);
		$mpdf->WriteHTML($formulir_swa);
		$mpdf->Output('FORMULIR SIMSON - ' . $data['no_wp'] . '.pdf', 'I');
	}

	public function createFilePdf()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_POST;
		// $data['no_wp'] = 'WPEKS21MAY001';
		// $data['jenis_pekerjaan'] = 'Relay dan Panel Kontrol';
		$data_wp = $this->permit->getDetailData($data);
		$data_safety = $this->sidak->getDataSafety($data);
		$data['data_safety'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		$data['dp3'] = $this->getDokumenDp3Aktif();

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$data['wp'] = $data_wp;
		$formulir_swa = $this->load->view('sidak/cetak/formulir_simson', $data, true);
		$mpdf->WriteHTML($formulir_swa);
		$mpdf->Output('files/berkas/dokumen_sidak/FORMULIRSIMSON-' . $data['no_wp'] . '.pdf', 'F');
	}


	public function base64ToImage($params, $imageData)
	{
		$imageData = 'data:image/jpeg;base64,' . $imageData;
		list($type, $imageData) = explode(';', $imageData);
		// echo $imageData;
		// die;
		list(, $extension) = explode('/', $type);
		list(, $imageData)      = explode(',', $imageData);
		// $fileName = $_SERVER["DOCUMENT_ROOT"] . '/working_permit/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		$fileName = $_SERVER["DOCUMENT_ROOT"] . '/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		$imageData = base64_decode($imageData);
		file_put_contents($fileName, $imageData);

		return $fileName;
	}

	public function cekDataBobotTraining($data_apd = array(), $data_training = array())
	{
		$nilai = 0;
		if ($data_apd['body_harnes'] == 'yes') {
			$nilai += $data_training['nilai_true_body_harnes'];
		} else {
			$nilai += $data_training['nilai_false_body_harnes'];
		}

		if ($data_apd['lanyard'] == 'yes') {
			$nilai += $data_training['nilai_true_lanyard'];
		} else {
			$nilai += $data_training['nilai_false_lanyard'];
		}

		if ($data_apd['hook_or_step'] == 'yes') {
			$nilai += $data_training['nilai_true_hook'];
		} else {
			$nilai += $data_training['nilai_false_hook'];
		}

		if ($data_apd['pullstrap'] == 'yes') {
			$nilai += $data_training['nilai_true_pullstrap'];
		} else {
			$nilai += $data_training['nilai_false_pullstrap'];
		}

		if ($data_apd['safety_helmet'] == 'yes') {
			$nilai += $data_training['nilai_true_safety_helmet'];
		} else {
			$nilai += $data_training['nilai_false_safety_helmet'];
		}

		if ($data_apd['kacamata_uv'] == 'yes') {
			$nilai += $data_training['nilai_true_kacamata'];
		} else {
			$nilai += $data_training['nilai_false_kacamata'];
		}

		if ($data_apd['earplug'] == 'yes') {
			$nilai += $data_training['nilai_true_earplug'];
		} else {
			$nilai += $data_training['nilai_false_earplug'];
		}

		if ($data_apd['masker'] == 'yes') {
			$nilai += $data_training['nilai_true_masker'];
		} else {
			$nilai += $data_training['nilai_false_masker'];
		}

		if ($data_apd['wearpack'] == 'yes') {
			$nilai += $data_training['nilai_true_wearpack'];
		} else {
			$nilai += $data_training['nilai_false_wearpack'];
		}

		if ($data_apd['sepatu_safety'] == 'yes') {
			$nilai += $data_training['nilai_true_sepatu'];
		} else {
			$nilai += $data_training['nilai_false_sepatu'];
		}

		if ($data_apd['sarung_tangan'] == 'yes') {
			$nilai += $data_training['nilai_true_sarungtangan'];
		} else {
			$nilai += $data_training['nilai_false_sarungtangan'];
		}

		if ($data_apd['stick_grounding'] == 'yes') {
			$nilai += $data_training['nilai_true_stick'];
		} else {
			$nilai += $data_training['nilai_false_stick'];
		}

		if ($data_apd['grounding_cable'] == 'yes') {
			$nilai += $data_training['nilai_true_grounding'];
		} else {
			$nilai += $data_training['nilai_false_grounding'];
		}

		if ($data_apd['stick_isolasi'] == 'yes') {
			$nilai += $data_training['nilai_true_isolasi'];
		} else {
			$nilai += $data_training['nilai_false_isolasi'];
		}

		if ($data_apd['voltage_detector'] == 'yes') {
			$nilai += $data_training['nilai_true_voltage'];
		} else {
			$nilai += $data_training['nilai_false_voltage'];
		}

		if ($data_apd['scaffolding'] == 'yes') {
			$nilai += $data_training['nilai_true_tangga'];
		} else {
			$nilai += $data_training['nilai_false_tangga'];
		}

		if ($data_apd['tambang'] == 'yes') {
			$nilai += $data_training['nilai_true_tambang'];
		} else {
			$nilai += $data_training['nilai_false_tambang'];
		}

		if ($data_apd['tagging'] == 'yes') {
			$nilai += $data_training['nilai_true_tagging'];
		} else {
			$nilai += $data_training['nilai_false_tagging'];
		}

		if ($data_apd['pembatas_area'] == 'yes') {
			$nilai += $data_training['nilai_true_kerja'];
		} else {
			$nilai += $data_training['nilai_false_kerja'];
		}

		return $nilai;
	}

	public function cekHasilPenilaian($permit = "")
	{
		$params['no_wp'] = 'WPEKS20MAY001';
		$data_safety = $this->sidak->getDataSafetyDetail($params);
		$data_apd = $this->sidak->getDataApdDetail($params);
		$data_training = $this->sidak->getDataTraining($data_safety);
		$result = array();
		if (!empty($data_training)) {
			$nilai_bobot_training = $this->cekDataBobotTraining($data_apd, $data_training);
			$result['status'] = $data_training['status_kondisi'];
			$result['score'] = $data_training['score'];
			$result['nilai_apd'] = $nilai_bobot_training;
		}
		// echo '<pre>';
		// print_r($result);
		// die;
		return $result;
	}

	public function cekHasilPenilaianSimpan($params = array())
	{
		// $params['no_wp'] = 'WPEKS20MAY001';
		$data_safety = $params['data_safety'];
		$data_apd = $params['data_apd'];
		$data_training = $this->sidak->getDataTraining($data_safety, $params['jenis_pekerjaan']);
		$result = array();
		if (!empty($data_training)) {
			$nilai_bobot_training = $this->cekDataBobotTraining($data_apd, $data_training);
			$result['status'] = $data_training['status_kondisi'];
			$result['score'] = $data_training['score'];
			$result['nilai_apd'] = $nilai_bobot_training;
		}
		// echo '<pre>';
		// print_r($result);
		// die;
		return $result;
	}

	public function cekHasilPenilaianScoreSafety($params = array())
	{
		$data = $_POST;
		$data_wp = $this->permit->getDetailData($data);
		$data_apd = (array) json_decode($_POST['data_apd']);
		$data_safety = (array) json_decode($_POST['data_safety']);
		$data['data_wp'] = $data_wp;
		$data['data_apd'] = $data_apd;
		$data['data_safety'] = $data_safety;
		$data['penilaian'] = $this->cekHasilPenilaianSimpan($data);

		$result['data'] = $data['penilaian'];
		echo json_encode($result);
	}
}
