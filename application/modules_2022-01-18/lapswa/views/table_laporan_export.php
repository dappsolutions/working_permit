<?php if (!empty($content)) { ?>
	<?php $no = 1; ?>
	<?php foreach ($content as $value) { ?>
		<?php $bg_color = ''; ?>
		<?php if ($value['status'] == 'DRAFT') { ?>
			<?php $bg_color = 'bg-warning'; ?>
		<?php } ?>
		<?php if ($value['status'] == 'REJECTED') { ?>
			<?php $bg_color = 'bg-danger'; ?>
		<?php } ?>
		<?php if ($value['status'] == 'APPROVED') { ?>
			<?php $bg_color = 'bg-success'; ?>
		<?php } ?>
		<tr class="<?php echo $bg_color ?>">
			<td><b><?php echo $no++ ?></b></td>
			<td><b><?php echo $value['no_wp'] ?></b></td>
			<td><b><?php echo $value['nama_swa'] ?></b></td>
			<td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
			<td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
			<td><b><?php echo $value['perusahaan'] ?></b></td>
			<td><b><?php echo $value['waktu_inspeksi'] ?></b></td>
			<td><b><?php echo $value['wp'] ?></b></td>
			<td><b><?php echo $value['sop'] ?></b></td>
			<td><b><?php echo $value['jsa'] ?></b></td>
			<td><b><?php echo $value['pengawas_k3'] ?></b></td>
			<td><b><?php echo $value['sertifikat_kom'] ?></b></td>
			<td><b><?php echo $value['peralatan'] ?></b></td>
			<td><b><?php echo $value['rambu_k3'] ?></b></td>
			<td><b><?php echo $value['apd'] ?></b></td>
			<td><b><?php echo $value['jam_pekerjaan_dihentikan'] ?></b></td>
			<td><b><?php echo $value['jam_pekerjaan_dilanjutkan'] ?></b></td>
			<td><b><?php echo $value['ket_pekerjaan_dihentikan'] ?></b></td>
			<td><b><?php echo $value['jenis_temuan_1'] ?></b></td>
			<td><b><?php echo $value['temuan_1'] ?></b></td>
			<td><b><?php echo $value['status_temuan_1'] == '1' ? 'yes' : '' ?></b></td>
			<td><b><?php echo $value['status_temuan_1'] == '0' ? 'yes' : '' ?></b></td>
			<td><b><?php echo $value['jenis_temuan_2'] ?></b></td>
			<td><b><?php echo $value['temuan_2'] ?></b></td>
			<td><b><?php echo $value['status_temuan_2'] == '1' ? 'yes' : '' ?></b></td>
			<td><b><?php echo $value['status_temuan_2'] == '0' ? 'yes' : '' ?></b></td>
			<td><b><?php echo $value['rekomendasi'] ?></b></td>
			<td><b><?php echo $value['ket_melanjutkan_pekerjaan'] ?></b></td>
		</tr>
	<?php } ?>
<?php } else { ?>
	<tr>
		<td colspan="20" class="text-center">Tidak ada data ditemukan</td>
	</tr>
<?php } ?>
