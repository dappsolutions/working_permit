<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box padding-16">
					<!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-4">
								<div class="input-group">
									<input type="text" class="form-control" id="tanggal" readonly placeholder="Tanggal">
									<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-4">
								<button id="tampil" class="btn btn-primary" onclick="LapSwa.tampilkan(this)">Tampilkan</button>
								<a class="btn btn-success" download="<?php echo 'Laporan Eksternal' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_content_export', 'Laporan Eksternal');">Export</a>
							</div>
						</div>
						<div class="divider"></div>
						<br />

						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="tb_content">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>Action</th>
												<th>No</th>
												<th>No Pengajuan Wp</th>
												<th>Nama Pemegang SWA</th>
												<th>Lokasi Pekerjaan</th>
												<th>Uraian Pekerjaan</th>
												<th>Nama Perusahaan</th>
												<th>Waktu Inspeksi</th>
												<th>Jenis Temuan 1</th>
												<th>Jenis Temuan 2</th>
												<th>Pelaksanaan Pekerjaan Dihentikan Sementara Pada Jam</th>
												<th>Pekerjaan Dilanjutkan Kembali Pada Jam</th>
											</tr>
										</thead>
										<tbody>
											<?php if (!empty($content)) { ?>
												<?php $no = 1; ?>
												<?php foreach ($content as $value) { ?>
													<?php $bg_color = ''; ?>
													<?php if ($value['status'] == 'DRAFT') { ?>
														<?php $bg_color = 'bg-warning'; ?>
													<?php } ?>
													<?php if ($value['status'] == 'REJECTED') { ?>
														<?php $bg_color = 'bg-danger'; ?>
													<?php } ?>
													<?php if ($value['status'] == 'APPROVED') { ?>
														<?php $bg_color = 'bg-success'; ?>
													<?php } ?>
													<tr class="<?php echo $bg_color ?>">
														<td class="text-center">
															<a href="<?php echo base_url() ?>swa/sidak/showDetailPengajuan?permit=<?php echo $value['id'] ?>&no_wp=<?php echo $value['no_wp'] ?>" onclick="LapSwa.showPengajuan(this,event)"><i class="fa fa-file-text grey-text  hover"></i></a>
														</td>
														<td><b><?php echo $no++ ?></b></td>
														<td><b><?php echo $value['no_wp'] ?></b></td>
														<td><b><?php echo $value['nama_swa'] ?></b></td>
														<td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['perusahaan'] ?></b></td>
														<td><b><?php echo $value['waktu_inspeksi'] ?></b></td>
														<td><b><?php echo $value['jenis_temuan_1'] ?></b></td>
														<td><b><?php echo $value['jenis_temuan_2'] ?></b></td>
														<td><b><?php echo $value['jam_pekerjaan_dihentikan'] ?></b></td>
														<td><b><?php echo $value['jam_pekerjaan_dilanjutkan'] ?></b></td>
													</tr>
												<?php } ?>
											<?php } else { ?>
												<tr>
													<td colspan="20" class="text-center">Tidak ada data ditemukan</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="row hide">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="tb_content_export">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>No</th>
												<th>No Pengajuan Wp</th>
												<th>Nama Pemegang SWA</th>
												<th>Lokasi Pekerjaan</th>
												<th>Uraian Pekerjaan</th>
												<th>Nama Perusahaan</th>
												<th>Waktu Inspeksi</th>												
												<th>Working Permit</th>
												<th>SOP / IK</th>
												<th>Job Safety Analisi</th>
												<th>Pengawas K3 & Pengawas Pekerjaan yang Berkompeten</th>
												<th>Pekerja Memiliki Sertifikat Kompetensi</th>
												<th>Peralatan Kerja yang Layak dan Sesuai dengan Jenis Pekerjaan</th>
												<th>Rambu - rambu K3 & LOTO</th>
												<th>Alat Pelindung Diri (APD) yang Layak dan Tersedia</th>												
												<th>Pelaksanaan Pekerjaan Dihentikan Sementara Pada Jam</th>
												<th>Pekerjaan Dilanjutkan Kembali Pada Jam</th>
												<th>Keterangan Penghentian Pekerjaan</th>
												<th>Jenis Temuan 1</th>
												<th>Temuan 1</th>
												<th>Tindakan Tidak Aman</th>
												<th>Kondisi Tidak Aman</th>
												<th>Jenis Temuan 2</th>
												<th>Temuan 2</th>
												<th>Tindakan Tidak Aman</th>
												<th>Kondisi Tidak Aman</th>
												<th>Rekomendasi</th>
												<th>Keterangan Melanjutkan Pekerjaan</th>
											</tr>
										</thead>
										<tbody>
											<?php if (!empty($content)) { ?>
												<?php $no = 1; ?>
												<?php foreach ($content as $value) { ?>
													<?php $bg_color = ''; ?>
													<?php if ($value['status'] == 'DRAFT') { ?>
														<?php $bg_color = 'bg-warning'; ?>
													<?php } ?>
													<?php if ($value['status'] == 'REJECTED') { ?>
														<?php $bg_color = 'bg-danger'; ?>
													<?php } ?>
													<?php if ($value['status'] == 'APPROVED') { ?>
														<?php $bg_color = 'bg-success'; ?>
													<?php } ?>
													<tr class="<?php echo $bg_color ?>">
														<td><b><?php echo $no++ ?></b></td>
														<td><b><?php echo $value['no_wp'] ?></b></td>
														<td><b><?php echo $value['nama_swa'] ?></b></td>
														<td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
														<td><b><?php echo $value['perusahaan'] ?></b></td>
														<td><b><?php echo $value['waktu_inspeksi'] ?></b></td>
														<td><b><?php echo $value['wp'] ?></b></td>
														<td><b><?php echo $value['sop'] ?></b></td>
														<td><b><?php echo $value['jsa'] ?></b></td>
														<td><b><?php echo $value['pengawas_k3'] ?></b></td>
														<td><b><?php echo $value['sertifikat_kom'] ?></b></td>
														<td><b><?php echo $value['peralatan'] ?></b></td>
														<td><b><?php echo $value['rambu_k3'] ?></b></td>
														<td><b><?php echo $value['apd'] ?></b></td>
														<td><b><?php echo $value['jam_pekerjaan_dihentikan'] ?></b></td>
														<td><b><?php echo $value['jam_pekerjaan_dilanjutkan'] ?></b></td>
														<td><b><?php echo $value['ket_pekerjaan_dihentikan'] ?></b></td>
														<td><b><?php echo $value['jenis_temuan_1'] ?></b></td>
														<td><b><?php echo $value['temuan_1'] ?></b></td>
														<td><b><?php echo $value['status_temuan_1'] == '1' ? 'yes' : '' ?></b></td>
														<td><b><?php echo $value['status_temuan_1'] == '0' ? 'yes' : '' ?></b></td>
														<td><b><?php echo $value['jenis_temuan_2'] ?></b></td>
														<td><b><?php echo $value['temuan_2'] ?></b></td>
														<td><b><?php echo $value['status_temuan_2'] == '1' ? 'yes' : '' ?></b></td>
														<td><b><?php echo $value['status_temuan_2'] == '0' ? 'yes' : '' ?></b></td>
														<td><b><?php echo $value['rekomendasi'] ?></b></td>
														<td><b><?php echo $value['ket_melanjutkan_pekerjaan'] ?></b></td>
													</tr>
												<?php } ?>
											<?php } else { ?>
												<tr>
													<td colspan="20" class="text-center">Tidak ada data ditemukan</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
