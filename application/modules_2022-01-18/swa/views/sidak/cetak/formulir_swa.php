<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: arial;
			font-weight: bold;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
		}

		.content-top {
			font-family: tahoma;
		}
	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_top-content">
				<table style="width: 100%;max-width: 100%;">
					<tr>
						<td style="border-right: none;"><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
						<td style="border-right: none;border-left: none;">
							<h3>&nbsp;PT PLN (PERSERO)</h3>
							<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
						</td>
						<td colspan="70"></td>
						<td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
					</tr>
					<tr>
						<td id="_judul" colspan="60" rowspan="4" style="border-top: 1px solid black;">
							<center>
								<label>
									FORMULIR STOP WORKING AUTHORITY (SWA)
								</label>
							</center>
						</td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>1 dari 1</label></td>
					</tr>
				</table>
			</div>
			<div>
				<hr />
			</div>

			<div class="content-top" style="padding: 16px;">
				<table style="border: none;width: 80%;font-family: tahoma;font-size:12px;">
					<tr style="border: none;">
						<td>1.</td>
						<td>Nama Pemegang SWA</td>
						<td>:</td>
						<td><?php echo $data_wp['data_swa']['nama_swa'] ?></td>
					</tr>
					<tr style="border: none;">
						<td>2.</td>
						<td>Lokasi Pekerjaan</td>
						<td>:</td>
						<td><?php echo $data_wp['data_apd']['lokasi_pekerjaan'] ?></td>
					</tr>
					<tr style="border: none;">
						<td>3.</td>
						<td>Jenis Pekerjaan</td>
						<td>:</td>
						<td><?php echo $data_wp['data_apd']['uraian_pekerjaan'] ?></td>
					</tr>
					<tr style="border: none;">
						<td>4.</td>
						<td>Waktu Inspeksi</td>
						<td>:</td>
						<td><?php echo $data_wp['data_swa']['createddate'] ?></td>
					</tr>
				</table>
			</div>

			<div class="label-title" style="text-align: left;padding: 16px;font-family: tahoma;margin-top: -40px;">
				<h5>A. KEGIATAN</h5>
				<table style="width: 100%;">
					<tr>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">NO</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KRITERIA</th>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KRITERIA</th>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KRITERIA</th>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;" rowspan="3">1</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">a. Working Permit yang telah di sah kan oleh pejabat yang berwenang</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['wp'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
						<td rowspan="7" style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['keputusan_pekerjaan'] ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">b. SOP (Prosedur/lnstruksi Kerja/Formulir Checklist)</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['sop'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">c. Job Safety Analysis (JSA) telah di sah kan oleh pejabat yang berwenang</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['jsa'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">2</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Pengawas K3 dan Pengawas Pekerjaan yang Berkompeten</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['pengawas_k3'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">3</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Pekerja memiliki sertifikat kompetensi</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['sertifikat_kom'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">4</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Peralatan Kerja yang layak dan sesuai dengan jenis pekerjaan</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['peralatan'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">5</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Rambu - rambu K3 dan LOTO</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['rambu_k3'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">6</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Alat Pelindung Diri (APD) yang layak dan tersedia sesuai dengan kebutuhan dilapangan</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							<?php echo $data_wp['data_pekerjaan']['apd'] == 'yes' ? 'Ada' : 'Tidak Ada' ?>
						</td>
					</tr>
				</table>
			</div>

			<div class="label-title" style="text-align: left;padding: 16px;font-family: tahoma;margin-top: -40px;">
				<h5>B. TINDAKAN</h5>
				<table style="width: 100%;">
					<tr>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">NO</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">JENIS TINDAKAN</th>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">WAKTU</th>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KETERANGAN</th>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">1</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Pelaksanaan pekerjaan dihentikan sementara</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['jam_pekerjaan_dihentikan'] == '' ? '-' : date('H:i', strtotime($data_wp['data_pekerjaan']['jam_pekerjaan_dihentikan'])) ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['ket_pekerjaan_dihentikan'] ?></td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">2</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Pekerjaan dilanjutkan kembali</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['jam_pekerjaan_dilanjutkan'] == '' ? '-' : date('H:i', strtotime($data_wp['data_pekerjaan']['jam_pekerjaan_dilanjutkan'])) ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['ket_melanjutkan_pekerjaan'] ?></td>
					</tr>
				</table>
			</div>

			<div class="label-title" style="text-align: left;padding: 16px;font-family: tahoma;margin-top: -40px;">
				<h5>C. TEMUAN Dl LAPANGAN (TINDAKAN TIDAK AMAN & KONDISI TIDAK AMAN)</h5>
				<table style="width: 100%;">
					<tr>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">NO</th>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">TEMUAN</th>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;" colspan="2">JENIS TEMUAN</th>
					</tr>
					<tr>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">TINDAKAN TIDAK AMAN</th>
						<th style="width: 30%;border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KONDISI TIDAK AMAN</th>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">1</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['temuan_1'] ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['status_temuan_1'] == '1' ? '✓' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['status_temuan_1'] == '0' ? '✓' : '-' ?></td>
					</tr>
					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">2</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['temuan_2'] ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['status_temuan_2'] == '1' ? '✓' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_wp['data_pekerjaan']['status_temuan_2'] == '0' ? '✓' : '-' ?></td>
					</tr>
				</table>

				<br>
				<h5>D. REKOMENDASI</h5>
				<div style="border:1px solid black;height:100px;padding: 8px;font-family: tahoma;">
					<?php echo $data_wp['data_pekerjaan']['rekomendasi'] ?>
				</div>
			</div>

			<div class="ttd" style="padding: 16px;">
				<table style="width: 100%;border:none;">
					<tr>
						<th style="border: none;text-align: center;font-family: tahoma;font-size:12px;">PENGAWAS K3 / PENGAWAS PEKERJAAN</th>
						<th style="border: none;text-align: center;font-family: tahoma;font-size:12px;">PEMEGANG SWA</th>
					</tr>
					<tr>
						<td style="border: none;text-align: center;font-family: tahoma;">
							<?php if ($data_wp['data_pekerjaan']['ttd_k3'] != '') { ?>
								<?php echo $data_wp['data_swa']['nama_perusahaan'] ?>
							<?php } ?>
						</td>
						<td style="border: none;text-align: center;">
							<?php if ($data_wp['data_pekerjaan']['ttd_swa'] != '') { ?>
								&nbsp;
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td style="border: none;text-align: center;">
							<?php if ($data_wp['data_pekerjaan']['ttd_k3'] != '') { ?>
								<?php echo $data_wp['img_ttdk3'] ?>
							<?php } ?>
						</td>
						<td style="border: none;text-align: center;">
							<?php if ($data_wp['data_pekerjaan']['ttd_swa'] != '') { ?>
								<?php echo $data_wp['img_ttdswa'] ?>
							<?php } ?>
						</td>
					</tr>
					<tr>
						<td style="border: none;text-align: center;font-family: tahoma;">
							<?php if ($data_wp['data_pekerjaan']['ttd_k3'] != '') { ?>
								<?php echo strtoupper($data_wp['data_swa']['nama_pengawas_k3']) ?>
							<?php } ?>
						</td>
						<td style="border: none;text-align: center;font-family: tahoma;">
							<?php if ($data_wp['data_pekerjaan']['ttd_swa'] != '') { ?>
								<?php echo $data_wp['data_swa']['nama_swa'] ?>
							<?php } ?>
						</td>
					</tr>
				</table>

				<h5 style="text-align: left;font-family: tahoma;">E. DOKUMENTASI</h5>
				<table style="width: 100%;">
					<?php if (!empty($data_wp['data_image'])) { ?>
						<?php $counter = 1; ?>
						<?php foreach ($data_wp['data_image'] as $key => $value) { ?>
							<tr>
								<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">
									<?php echo $value['elm_image'] ?>
								</td>
							</tr>
							<?php $counter += 1; ?>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">-</td>
							<td style="font-family: tahoma;padding: 8px;border: 1px solid black;font-size:12px;">-</td>
						</tr>
						<tr>
							<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">-</td>
							<td style="font-family: tahoma;padding: 8px;border: 1px solid black;font-size:12px;">-</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>

</html>
