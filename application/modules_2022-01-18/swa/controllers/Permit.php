<?php

class Permit extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_permit', 'permit');
	}

	public function index()
	{
		echo 'login ' . date('Y-m-d H:i:s');
	}

	public function getData()
	{

		$data = $_POST;
		// $data['user_id'] = 940;
		$data['data_user'] = Modules::run('simson/user/getUser', $data['user_id']);
		$data = $this->permit->getData($data);

		$result['data'] = $data;
		// echo json_encode($result);
		Modules::run('simson/output/get', $result);
	}

	public function getDetailData()
	{

		$data = $_POST;
		// $data['id'] = 15;
		// $data['no_wp'] = 'WPEKS20MAY001';
		$data = $this->permit->getDetailData($data);

		$result['data'] = $data;
		echo json_encode($result);
	}

	public function getListWpSudahSidak()
	{
		$data = $_POST;
		// $data['user_id'] = 940;
		$data['data_user'] = Modules::run('simson/user/getUser', $data['user_id']);
		$data = $this->permit->getDataSudahSidak($data);
		$result['data'] = $data;

		Modules::run('simson/output/get', $result);
	}

	public function getDataSearch()
	{

		$data = $_POST;
		$data['data_user'] = Modules::run('simson/user/getUser', $data['user_id']);
		$data = $this->permit->getData($data, false);

		$result['data'] = $data;
		Modules::run('simson/output/get', $result);
	}

	public function getListWpSudahSidakSearch()
	{
		$data = $_POST;
		// $data['user_id'] = 940;
		// $data['keyword'] = 'asd';
		$data['data_user'] = Modules::run('simson/user/getUser', $data['user_id']);
		$data = $this->permit->getDataSudahSidak($data, false);
		$result['data'] = $data;

		Modules::run('simson/output/get', $result);
	}
}
