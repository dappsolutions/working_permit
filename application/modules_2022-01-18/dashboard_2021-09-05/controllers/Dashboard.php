<?php

class Dashboard extends MX_Controller
{

	public $hak_akses;
	public $upt;
	public $level;
	public $user;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		// echo '<pre>';
		// print_r($_SESSION);die;
		$this->hak_akses = $this->session->userdata('hak_akses');
		$this->upt = $this->session->userdata('upt');
		$this->level = $this->session->userdata('level');
		$this->user = $this->session->userdata('user_id');
	}

	public function getModuleName()
	{
		return 'dashboard';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
			'<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
			'<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.resize.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.pie.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/Flot/jquery.flot.categories.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Dashboard";
		$data['title_content'] = 'Dashboard';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['total_user'] = $this->getTotalUser();
		$data['total_wp_approve'] = $this->getTotalWpApprove();
		$data['total_wp_draft'] = $this->getTotalWpDraft();
		$data['total_wp_reject'] = $this->getTotalWpReject();
		$data['data_pengajuan'] = $this->getDataWpDraft();
		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getTotalUser()
	{
		$total = Modules::run('database/count_all', array(
			'table' => 'user',
			'where' => "deleted = 0"
		));
		return $total;
	}

	public function getTotalWpApprove()
	{
		$where = "p.deleted = 0 and pst.status = 'APPROVED'";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'APPROVED'";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 and pst.status = 'APPROVED' "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'APPROVED' "
					. "and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pst.status = 'APPROVED' and pp.upt = '" . $this->upt . "'";
		}

		//
		//  echo $where;die;
		$total = Modules::run('database/count_all', array(
			'table' => 'permit p',
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
			),
			'where' => $where
		));
		return $total;
	}

	public function getTotalWpDraft()
	{

		$where = "p.deleted = 0 and pst.status = 'DRAFT'";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'DRAFT'";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 and pst.status = 'DRAFT' "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'DRAFT' "
					. "and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pst.status = 'DRAFT' and pp.upt = '" . $this->upt . "'";
		}

		$total = Modules::run('database/count_all', array(
			'table' => 'permit p',
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
			),
			'where' => $where
		));
		return $total;
	}

	public function getDataWpDraft()
	{
		$where = "p.deleted = 0";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pp.upt = '" . $this->upt . "'";
		}

		//  echo $where;die;
		$data = Modules::run('database/get', array(
			'table' => 'permit p',
			'field' => array('p.*', 'pst.status'),
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
			),
			'where' => $where,
			'limit' => 10,
			'orderby' => 'p.id desc'
		));
		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getTotalWpReject()
	{

		$where = "p.deleted = 0 and pst.status = 'REJECTED'";
		if ($this->level == "") {
			if ($this->hak_akses == 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'REJECTED'";
			}
			if ($this->hak_akses == 'vendor') {

				$where = "p.deleted = 0 and pst.status = 'REJECTED' "
					. "and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
			}

			if ($this->hak_akses != 'vendor' && $this->hak_akses != 'superadmin') {
				$where = "p.deleted = 0 and pst.status = 'REJECTED' "
					. "and ps.user = '" . $this->user . "'";
			}
		} else {
			$where = "p.deleted = 0 and pst.status = 'REJECTED' and pp.upt = '" . $this->upt . "'";
		}

		$total = Modules::run('database/count_all', array(
			'table' => 'permit p',
			'join' => array(
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
			),
			'where' => $where
		));
		return $total;
	}
}
