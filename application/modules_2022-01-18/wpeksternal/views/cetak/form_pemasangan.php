<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: arial;
			font-weight: bold;
			font-size: 12px;
			border-bottom: 1px solid black;
		}

		#_form {
			font-size: 12px;
			text-align: right;
			margin-left: 87%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			font-size: 12px;
			font-family: arial;
		}

		#_isi {
			margin-left: 2%;
			margin-top: 2%;
			text-align: left;
			font-family: tahoma;
			font-size: 10px;
		}

		#_isi>table {
			font-size: 10px;
		}

		#_isi>table {
			border-top: none;
			border-bottom: none;
			border-right: none;
			border-left: none;
		}

		#_isi>table>tr {
			border-top: none;
			border-bottom: none;
			border-right: none;
			border-left: none;
		}

		#_isi>table>tr>td {
			border-top: none;
			border-bottom: none;
			border-right: none;
			border-left: none;
		}

		#_ttd {
			font-family: tahoma;
			font-size: 10px;
			text-align: right;
			padding-right: 10%;
		}

		#_gm {
			font-family: tahoma;
			font-size: 10px;
			text-align: right;
			padding-right: 14%;
		}

		#_footer-content {
			font-family: tahoma;
			font-size: 10px;
			padding-left: 3.5%;
		}

		#_foot-satu {
			display: table-cell !important;
		}

		#_foot-dua {
			display: table-cell !important;
		}
	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_top-content">
				<table style="width: 100%;max-width: 100%;">
					<tr>
						<td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
						<td>
							<h3>&nbsp;PT PLN (PERSERO)</h3>
							<h3>TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
						</td>
						<td colspan="70"></td>
						<td><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"></td>
						<td><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
					</tr>
					<tr>
						<td id="_judul" colspan="60" rowspan="4">
							<center>
								<label>
									FORMULIR PEMASANGAN DAN PELEPASAN
								</label>
							</center>
							<center>
								<label>
									LOCK OUT & TAG OUT
								</label>
							</center>
						</td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dp3['no_dokumen'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dp3['edisi'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dp3['berlaku_efektif'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>2 dari 1</label></td>
					</tr>
				</table>
			</div>
			<div>
				<hr />
			</div>
			<div id="_isi">
				<table style="border:none;font-size: 14px;font-family: tahoma;">
					<tr style="border:none;">
						<td style="border:none;">Lokasi</td>
						<td style="border:none;">:</td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">No SPK</td>
						<td style="border:none;">:</td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">Nama Pekerjaan</td>
						<td style="border:none;">:</td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">Tanggal Pekerjaan</td>
						<td style="border:none;">:</td>
						<td colspan="5" style="border:none;"></td>
					</tr>
				</table>
				<br>

				<div class="content-table">
					<table style="width: 98%;font-family: tahoma;">
						<thead>
							<tr>
								<th style="text-align: center;border:1px solid #000;padding:6px;font-family: tahoma;">TANGGAL</th>
								<th style="text-align: center;border:1px solid #000;padding:6px;font-family: tahoma;">NAMA PERALATAN</th>
								<th style="text-align: center;border:1px solid #000;padding:6px;font-family: tahoma;">JAM PASANG</th>
								<th style="text-align: center;border:1px solid #000;padding:6px;font-family: tahoma;">JAM LEPAS</th>
								<th style="text-align: center;border:1px solid #000;padding:6px;font-family: tahoma;">PELAKSANA</th>
							</tr>
						</thead>
						<tbody>
							<?php for ($i = 0; $i < 9; $i++) { ?>
								<tr>
									<td style="border:1px solid #000;padding:16px;">&nbsp;</td>
									<td style="border:1px solid #000;padding:16px;">&nbsp;</td>
									<td style="border:1px solid #000;padding:16px;">&nbsp;</td>
									<td style="border:1px solid #000;padding:16px;">&nbsp;</td>
									<td style="border:1px solid #000;padding:16px;">&nbsp;</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
				<br>

				<div class="tgl-pengisian" style="text-align: center;font-family: tahoma;">
					<p>...........................,..................................,20...</p>
				</div>

				<br />
				<div class="content-ttd" style="display: flex;">
					<table style="width: 100%;border: none;">
						<tbody>
							<tr>
								<td style="text-align: center;">
									<div style="text-align: center;margin-right: 5%;font-family: tahoma;">
										<label>&nbsp;</label>
										<br />
										<label>Pengawas Manuver</label>
										<br />
										<br />
										<br />
										<label>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</label>
									</div>
								</td>
								<td style="text-align: center;">
									<div style="text-align: center;margin-right: 5%;font-family: tahoma;">
										<label>Mengetahui</label>
										<br />
										<label>Pengawas K3</label>
										<br />
										<br />
										<br />
										<label>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br />
			</div>
		</div>
	</div>
</body>

</html>