<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: arial;
			font-weight: bold;
			font-size: 12px;
			border-bottom: 1px solid black;
		}

		#_form {
			font-size: 12px;
			text-align: right;
			margin-left: 87%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			font-size: 12px;
			font-family: arial;
		}

		#_isi {
			margin-left: 2%;
			margin-top: 2%;
			text-align: left;
			font-family: tahoma;
			font-size: 10px;
		}

		#_isi>table {
			font-size: 10px;
		}

		#_isi>table {
			border-top: none;
			border-bottom: none;
			border-right: none;
			border-left: none;
		}

		#_isi>table>tr {
			border-top: none;
			border-bottom: none;
			border-right: none;
			border-left: none;
		}

		#_isi>table>tr>td {
			border-top: none;
			border-bottom: none;
			border-right: none;
			border-left: none;
		}

		#_ttd {
			font-family: tahoma;
			font-size: 10px;
			text-align: right;
			padding-right: 10%;
		}

		#_gm {
			font-family: tahoma;
			font-size: 10px;
			text-align: right;
			padding-right: 14%;
		}

		#_footer-content {
			font-family: tahoma;
			font-size: 10px;
			padding-left: 3.5%;
		}

		#_foot-satu {
			display: table-cell !important;
		}

		#_foot-dua {
			display: table-cell !important;
		}
	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_top-content">
				<table style="width: 100%;max-width: 100%;">
					<tr>
						<td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
						<td>
							<h3>&nbsp;PT PLN (PERSERO)</h3>
							<h3>TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
						</td>
						<td colspan="70"></td>
						<td><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"></td>
						<td><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
					</tr>
					<tr>
						<td id="_judul" colspan="60" rowspan="4">
							<center>
								<label>
									FORMULIR PROSEDUR PELAKSANAAN PEKERJAAN
								</label>
							</center>
							<center>
								<label>
									PADA INSTALASI TINGGI / EXTRA TINGGI
								</label>
							</center>
						</td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dp3['no_dokumen'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dp3['edisi'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $dp3['berlaku_efektif'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>1 dari 2</label></td>
					</tr>
				</table>
			</div>
			<div>
				<hr />
			</div>
			<div id="_form">
				<label>FORM 1.1</label>
			</div>
			<div id="_surat">
				<label>SURAT PENUNJUKAN PENGAWAS (<?php echo $tipe ?>)</label>
			</div>
			<div id="no">
				<label><?php echo $no_dp3 ?></label>
			</div>
			<div id="_isi">
				<table style="border:none;font-size: 10px;font-family: tahoma;">
					<tr style="border:none;">
						<td style="border:none;">Pekerjaan</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo $uraian_pekerjaan ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">SPP No</td>
						<td style="border:none;">:</td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">Lokasi</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo $nama_place ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">Hari / Tanggal</td>
						<td style="border:none;">:</td>
						<td colspan="5" style="border:none;"><?php
																									echo $tgl_awal . ' - ' . $tgl_akhir;
																									?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">Manajer </td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo $upt_tujuan ?></td>
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td colspan="5" style="border:none;">menugaskan kepada :</td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Nama</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo $spv_gi ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo !empty($spv_gi_data) ? $spv_gi_data['nip'] : '-' ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jabatan</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo !empty($spv_gi_data) ? $spv_gi_data['posisi'] : '-' ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;" colspan="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk bertindak sebagai Pengawas Manuver dengan uraian tugas sebagai berikut :</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td colspan="50" style="border:none;padding-left:  8%;">
							<ul>
								<li>Mengawasi pelaksana manuver</li>
								<li>Mengawasi pemasangan dan pelepasan tagging di panel control serta rambu pengaman/gembok di switch yard</li>
								<li>Mengawasi pemasangan dan pelepasan system pentanahan</li>
								<li>Menjelaskan bersama Pengawas K3 kepada Pengawas Pekerjaan dan Pelaksana Pekerjaan tentang daerah aman dan tidak aman untuk dikerjakan</li>
							</ul>
						</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Nama</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo $pengawas_k3 ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo !empty($pengawas_k3_data) ? $pengawas_k3_data['nip'] : '-' ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jabatan</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo !empty($pengawas_k3_data) ? $pengawas_k3_data['posisi'] : '-' ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;" colspan="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk bertindak sebagai Pengawas K3 dengan uraian tugas sebagai berikut :</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td colspan="50" style="border:none;padding-left:  8%;">
							<ul>
								<li>Memeriksa kondisi personil sebelum bekerja</li>
								<li>Mengawasi kondisi/tempat-tempat yang berbahaya</li>
								<li>Mengawasi tingkah laku/sikap personil yang membahayakan diri sendiri atau orang lain</li>
								<li>Mengawasi penggunaan perlengkapan keselamatan kerja</li>
							</ul>
						</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Nama</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo $pengawas_pekerjaan ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NIP</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo !empty($pengawas_pekerjaan_data) ? $pengawas_pekerjaan_data['nip'] : '-' ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jabatan</td>
						<td style="border:none;">:</td>
						<td style="border:none;"><?php echo !empty($pengawas_pekerjaan_data) ? $pengawas_pekerjaan_data['posisi'] : '-' ?></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;" colspan="50">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Untuk bertindak sebagai Pengawas Pekerjaan dengan uraian tugas sebagai berikut :</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td colspan="50" style="border:none;padding-left:  8%;">
							<ul>
								<li>Memimpin koordinasi rencana pelaksanaan maneuver dengan Pengawas K3 dan Pengawas maneuver</li>
								<li>Memimpin Briefing rencana pelaksanaan pekerjaan dan pembagian tugas Pengawas K3 yang ditutup dengan doa bersama</li>
								<li>Mengawasi Pemasangan dan Pelepasan Pentanahan local</li>
								<li>Mengawasi Pemasangan dan Pelepasan tagging, gembok dan rambu pengamanan</li>
								<li>Menjelaskan metode pekerjaan</li>
								<li>Menunjuk personil pelaksana pekerjaan sebagai pelaksana, pengaman instalasi Gardu Induk Listrik untuk memasang dan melepas tagging, gembok dan rambu pengaman di Switch yard</li>
								<li>Memimpin evaluasi pelaksanaan pekerjaan dan melaksanakan doa penutup</li>
							</ul>
						</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
					<tr style="border:none;">
						<td style="border:none;" colspan="50">
							<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dengan penunjukan ini, Manajer APB/APP tetap bertanggung jawab sampai pekerjaan selesai.</label>
							<br />
							<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jika salah satu Pengawas berhalangan hadir, maka Penanggung jawab Pekerjaan berhak menunjuk petugas lain</label>
							<br />
							<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;yang di anggap mampu dan kompeten untuk menggantikan</label>
						</td>
						<td style="border:none;"></td>
						<td style="border:none;"></td>
					</tr>
				</table>
				<!--          <div id="_ttd">
                 <label><?php echo date('d M Y') ?></label>                       
               </div>-->
				<!--          <div id="_gm">
                 <label>Manajer APP <?php echo $unit ?></label>
                 <br/>
               </div>-->
				<!--          <div id="_footer-content">
                 <label style="border-bottom: 1px solid black;">Yang Menerima Penunjukan :</label>
                 <br/>
                 <label>1. Tanda tangan Pengawas Manuver ………....…..………..</label>
                 <br/>
                 <br/>
                 <label>2. Tanda tangan Pengawas K3 ………………….…..…………</label>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <img src="<?php echo base_url() . 'files/ttd_paraf/' . $ttd_manager['ttd_manager'] ?>" width="60px" height="60px">
                 <br/>
                 <br/>
                 <label>3. Tanda tangan Pengawas Pekerjaan ……………………….  </label>           
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp;&nbsp;
                 <label>              
                   (<?php echo $ttd_manager['nama'] ?>)</label>
               </div>-->
				<br />
				<div id="_foot-satu">
					<table style="font-size: 10px;border:0px;">
						<tr style="border:0px;font-size: 10px;">
							<td style="border:0px;font-size: 10px;">Yang Menerima Penunjukan :</td>
						</tr>
						<tr style="border:0px;font-size: 10px;">
							<td style="border:0px;font-size: 10px;"><label>1. Tanda tangan Pengawas Manuver ………....…..………..</label></td>
						</tr>
						<tr style="border:0px;font-size: 10px;">
							<td style="font-size: 10px;"><label>2. Tanda tangan Pengawas K3 ………………….…..…………</label></td>
						</tr>
						<tr style="border:0px;font-size: 10px;">
							<td style="border:0px;font-size: 10px;"><label>3. Tanda tangan Pengawas Pekerjaan ………………………. </label></td>
						</tr>
					</table>
				</div>
				<br />
				<div id="_foot-dua" style="text-align: right;margin-right: 5%;">
					<label><?php echo date('d M Y') ?></label>
					<br />
					<label>Manajer APP <?php echo $upt_tujuan ?></label>
					<br />
					<br /><img src="<?php echo base_url() . 'files/berkas/ttd/' . $approval_last['ttd'] ?>" width="90px" height="90px">
					<br />
					<label>
						(<?php echo $approval_last['nama_pegawai'] ?>)</label>
				</div>
				<br />
			</div>
		</div>
	</div>
</body>

</html>