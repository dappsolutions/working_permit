<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		tr:nth-child(1)>td:nth-child(5) {
			border-right: 1px solid black;
		}

		tr:nth-child(2)>td:nth-child(1) {
			border-top: 1px solid black;
		}

		tr:nth-child(2)>td:nth-child(2),
		tr:nth-child(2)>td:nth-child(3) {
			border: 1px solid black;
		}

		tr:nth-child(3)>td:nth-child(1),
		tr:nth-child(3)>td:nth-child(2) {
			border: 1px solid black;
		}

		tr:nth-child(4)>td:nth-child(1),
		tr:nth-child(4)>td:nth-child(2) {
			border: 1px solid black;
		}

		tr:nth-child(5)>td:nth-child(1),
		tr:nth-child(5)>td:nth-child(2) {
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: tahoma;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
		}
	</style>
</head>
<div id="_wrapper">
	<div id="_content">
		<div id="_top-content">
			<table style="width: 100%;max-width: 100%;">
				<tr>
					<td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
					<td>
						<h3>&nbsp;PT PLN (PERSERO)</h3>
						<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
					</td>
					<td colspan="70"></td>
					<td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
				</tr>
				<tr>
					<td id="_judul" colspan="60" rowspan="4">
						<center>
							<label>
								FORMULIR IDENTIFIKASI BAHAYA PENILAIAN DAN PENGENDALIAN RISIKO/PELUANG (IBPPRP)
							</label>
						</center>
					</td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
				</tr>
				<tr>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
				</tr>
				<tr>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
				</tr>
				<tr>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
					<td id="_data" colspan="8" style="border: 1px solid black;"><label>8 dari 8</label></td>
				</tr>
			</table>
		</div>
		<div>
			<hr />
		</div>
		<div style="font-family: tahoma;margin-left: 2%;margin-right: 2%;text-align: left;font-size: 12px;">
			<table style="width:100%;">
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>KEMUNGKINAN</b>
					</td>
					<td colspan="5" style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>KONSEKUENSI</b>
					</td>
					<td rowspan="7" style="border:none;width:20px;">
						&nbsp;
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>TINGKAT RESIKO</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>KEMUNGKINAN</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>KONSEKUENSI</b>
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						&nbsp;
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>1</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>2</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>3</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>4</b>
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>5</b>
					</td>
					<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>E</b> = Extreme Risk
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>E.</b> Hampir pasti akan terjadi/almost certain
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>1.</b> Tidak ada cedera, kerugian materi kecil
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>E</b>
					</td>
					<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>M</b>
					</td>
					<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>M</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>E</b>
					</td>
					<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>E</b>
					</td>
					<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>H</b> = High Risk
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>D.</b> Cenderung untuk terjadi / likely
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>2.</b> Cedera ringan/P3K, kerugian cukup materi sedang
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>D</b>
					</td>
					<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>L</b>
					</td>
					<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>M</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>E</b>
					</td>
					<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>E</b>
					</td>
					<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>M</b> = Moderate Risk
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>C.</b> Mungkin dapat terjadi / moderate
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>3.</b> Hilang hari kerja, kerugian cukup besar
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>C</b>
					</td>
					<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>L</b>
					</td>
					<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>M</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>E</b>
					</td>
					<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>L</b> = Low Risk
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>B.</b> Kecil kemungkinan terjadi / unlikely
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>4.</b> Cacat, kerugian materi besar
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
						<b>B</b>
					</td>
					<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>L</b>
					</td>
					<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>L</b>
					</td>
					<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>M</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>E</b>
					</td>
					<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						&nbsp;
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>A.</b> Jarang terjadi/rare
					</td>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;">
						<b>5.</b> Kematian, kerugian materi sangat besar
					</td>
				</tr>
				<tr>
					<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;height:30px;text-align:center;">
						<b>A</b>
					</td>
					<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>L</b>
					</td>
					<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>L</b>
					</td>
					<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>M</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
						<b>H</b>
					</td>
					<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
					<td style="font-family: tahoma;font-size: 12px;width:120px;">
						&nbsp;
					</td>
					<td style="font-family: tahoma;font-size: 12px;width:120px;">
						&nbsp;
					</td>
					<td style="font-family: tahoma;font-size: 12px;width:120px;">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
		<br>
		<br>

		<div style="font-family: tahoma;margin-left: 2%;margin-right: 2%;text-align: left;font-size: 12px;">
			<b><u>Keterangan</u></b>
			<br>
			<div style="font-family: tahoma;margin-left: 2%;margin-right: 2%;text-align: left;font-size: 12px;">
				<p>
					1. P/R &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = Peluang / Risiko
					<br>
					2. Kon &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = Konsekuensi
					<br>
					3. Kem &nbsp;&nbsp;&nbsp;&nbsp; = Kemungkinan
					<br>
					4. TR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = Tingkat Risiko

				</p>
			</div>
		</div>
		<div>&nbsp;</div>
	</div>
	</body>

</html>
