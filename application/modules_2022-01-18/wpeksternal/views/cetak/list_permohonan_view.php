<script type="text/javascript" src="<?php echo base_url(); ?>files/js/.tablesorter.js"></script>
<div class="_right">
<?php
  echo validation_errors();
  echo form_open($module .'/search');
  echo form_input('keyword', '', 'placeholder="Pencarian"');
  echo form_submit('submit', 'Cari');
  echo form_close();
?>
</div>
<?php
  if(isset($search)) {?>
    <div id="_sub-title">
      Hasil pencarian "<b><?php echo $search?></b>" :
    </div>
<?php }
  $tmpl = array (
                    'table_open' => '<table cellpadding="4" cellspacing="0" class="_table-data">'
              );
  $this->table->set_heading(array('No', 'NO REG', 'NO WP', 'Lokasi', 'Pekerjaan', 'Tanggal Pengajuan'
                            ,'Tanggal Mulai', 'Tangal Selesai', 'Perusahaan', 'Tanggal Cetak', 
                            'Dicetak Oleh', 'Banyak Cetakan',
                            form_checkbox(array('name'=>'checkall', 'id'=>'checkall')), 'Cetak DP3'));
  $counter = $pagination['last_no'];
  if(!empty($data)){
	  foreach($data->result() as $row){     
      $cetak_dp3 = base_url() .$module.'/cetak_dp3/'. $row->id;
      
      if ($row->is_need_sistem == 1 && ($this->session->userdata('level_id') == "1" || $this->session->userdata('struktural_approval_id') == 5)){
          $cetak_dp3 = "<a href='". $cetak_dp3."' class='_btn-url'>Cetak DP3</a>";
      }else
      {
        $cetak_dp3 = '';
      }
      $value = array(
        'name'=>'permohonan_id[]',
        'value'=>$row->id,
        'class'=>'case'
      );      
      if($row->tanggal_cetak == '0000-00-00'){
        $row->tanggal_cetak = '';
      }else{
        $row->tanggal_cetak = date('d M Y', strtotime($row->tanggal_cetak));
      }
      
      if($row->gardu_induk_id != 0){
        $row->lokasi_kerja = $row->jenis.' - '.$row->gardu_induk;
      }else{
        $row->lokasi_kerja = $row->lokasi_kerja;
      }
      
      $this->table->add_row(array(++$counter, $row->id, $row->no_permohonan, $row->lokasi_kerja,
          $row->pekerjaan, date('d M Y', strtotime($row->createddate)),
          date('d M Y', strtotime($row->tanggal_awal_pelaksanaan)),           
          date('d M Y', strtotime($row->tanggal_akhir_pelaksanaan)), $row->perusahaan, $row->tanggal_cetak
          ,
          $row->user_cetak, $row->banyak_cetak,
          form_checkbox($value), $cetak_dp3
      ));      
    }
  } else{
    $this->table->add_row(array(array('data' => 'Tidak ada data ditemukan', 'colspan' => '14')));
  }
  echo '<div class="_full-flow">';
  $this->table->set_template($tmpl);    
  $confirm_msg = 'Apakah anda yakin dengan data Permohonan ?';    
  echo form_open($module.'/cetak', 
      array('onsubmit' => "return confirm('" . $confirm_msg . "')"));
  echo $this->table->generate();
  echo '</div>';
  echo '</br>';
  echo form_submit(array('name'=>'submit', 'value'=>'Cetak', 'id'=>'_btn-url'));  
  echo form_close();  
  echo '<div id="_table-footer">';
  echo '<span>Menampilkan data '. ($counter > 0 ? $pagination['last_no'] + 1 : 0) .' - '. ($counter) .
    ' dari <b>'. $pagination['total_rows'] .'</b> data.</span>';
  echo $pagination['links'];
  echo '</div>';
?>
<script type="text/javascript">
  $(function(){    
    $("#checkall").click(function () {
        $('.case').attr('checked', this.checked);
    });
    $(".case").click(function(){
        if($(".case").length == $(".case:checked").length) {
          $("#checkall").attr("checked", "checked");
        } else {
          $("#checkall").removeAttr("checked");
        }
    });               
  });
</script>