<div class="col-md-12">
	<h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Job Safety Analysis</u></h4>
	<hr />
	<div class="box-body" style="margin-top: -12px;">
		<?php if (isset($list_jsa)) { ?>
			<?php if (!empty($list_jsa)) { ?>
				<?php foreach ($list_jsa as $key => $value) { ?>
					<div class="table-responsive">
						<br>
						<div class='row'>
							<div class='text-center col-md-5'>
								<input type="text" class='form-control required' value="<?php echo $value[0]['judul_jsa'] ?>" error="Judul JSA" id='judul_jsa' placeholder="Judul JSA">
							</div>
						</div>
						<br>
						<table class="table table-bordered table_jsa" id="tb_jsa" data_id="<?php echo $key ?>">
							<thead>
								<tr class="bg-primary-light text-white">
									<th>Tahapan Pekerjaan</th>
									<th>Potensi Bahaya</th>
									<th>Pengendalian</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php if (!empty($value)) { ?>
									<?php foreach ($value as $v_item) { ?>
										<tr permit_jsa='<?php echo $key ?>' data_id="<?php echo $v_item['pji_id'] ?>">
											<td>
												<textarea id="tahapan" class="form-control required" error="Tahapan"><?php echo $v_item['tahapan_pekerjaan'] ?></textarea>
											</td>
											<td>
												<textarea id="potensi" class="form-control required" error="Potensi"><?php echo $v_item['potensi_bahaya'] ?></textarea>
											</td>
											<td>
												<textarea id="pengendalian" class="form-control required" error="Pengendalian"><?php echo $v_item['pengendalian'] ?></textarea>
											</td>
											<td class="text-center">
												<i class="fa fa-trash fa-lg hover-content" onclick="WpEksternal.removeJsa(this)"></i>
											</td>
										</tr>
									<?php } ?>
								<?php } ?>
								<?php $required = isset($list_jsa) ? '' : 'required' ?>
								<?php $counter = 1; ?>
								<?php if ($required != '') { ?>
									<?php $counter = 3; ?>
								<?php } ?>
								<?php for ($i = 0; $i < $counter; $i++) { ?>
									<tr permit_jsa='<?php echo $key ?>' data_id="">
										<td>
											<textarea id="tahapan" class="form-control <?php echo $required ?> " error="Tahapan"></textarea>
										</td>
										<td>
											<textarea id="potensi" class="form-control <?php echo $required ?>" error="Potensi"></textarea>
										</td>
										<td>
											<textarea id="pengendalian" class="form-control <?php echo $required ?>" error="Pengendalian"></textarea>
										</td>
										<td class="text-center">
											<i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addJsa(this)"></i>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				<?php } ?>
			<?php } else { ?>
				<div class="table-responsive">
					<br>
					<div class='row'>
						<div class='text-center col-md-5'>
							<input type="text" class='form-control required' error="Judul JSA" id='judul_jsa' placeholder="Judul JSA">
						</div>
					</div>
					<br>
					<table class="table table-bordered table_jsa" id="tb_jsa" data_id=''>
						<thead>
							<tr class="bg-primary-light text-white">
								<th>Tahapan Pekerjaan</th>
								<th>Potensi Bahaya</th>
								<th>Pengendalian</th>
								<th class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $required = 'required' ?>
							<?php $counter = 1; ?>
							<?php if ($required != '') { ?>
								<?php $counter = 3; ?>
							<?php } ?>
							<?php for ($i = 0; $i < $counter; $i++) { ?>
								<tr data_id="" permit_jsa=''>
									<td>
										<textarea id="tahapan" class="form-control <?php echo $required ?> " error="Tahapan"></textarea>
									</td>
									<td>
										<textarea id="potensi" class="form-control <?php echo $required ?>" error="Potensi"></textarea>
									</td>
									<td>
										<textarea id="pengendalian" class="form-control <?php echo $required ?>" error="Pengendalian"></textarea>
									</td>
									<td class="text-center">
										<i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addJsa(this)"></i>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			<?php } ?>

		<?php } else { ?>
			<div class="table-responsive">
				<br>
				<div class='row'>
					<div class='text-center col-md-5'>
						<input type="text" class='form-control required' error="Judul JSA" id='judul_jsa' placeholder="Judul JSA">
					</div>
				</div>
				<br>
				<table class="table table-bordered table_jsa" id="tb_jsa">
					<thead>
						<tr class="bg-primary-light text-white">
							<th>Tahapan Pekerjaan</th>
							<th>Potensi Bahaya</th>
							<th>Pengendalian</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if (isset($list_jsa)) { ?>
							<?php if (!empty($list_jsa)) { ?>
								<?php foreach ($list_jsa as $value) { ?>
									<tr data_id="<?php echo $value['id'] ?>">
										<td>
											<textarea id="tahapan" class="form-control required" error="Tahapan"><?php echo $value['tahapan_pekerjaan'] ?></textarea>
										</td>
										<td>
											<textarea id="potensi" class="form-control required" error="Potensi"><?php echo $value['potensi_bahaya'] ?></textarea>
										</td>
										<td>
											<textarea id="pengendalian" class="form-control required" error="Pengendalian"><?php echo $value['pengendalian'] ?></textarea>
										</td>
										<td class="text-center">
											<i class="fa fa-trash fa-lg hover-content" onclick="WpEksternal.removeJsa(this)"></i>
										</td>
									</tr>
								<?php } ?>
							<?php } ?>
						<?php } ?>
						<?php $required = isset($list_jsa) ? '' : 'required' ?>
						<?php $counter = 1; ?>
						<?php if ($required != '') { ?>
							<?php $counter = 3; ?>
						<?php } ?>
						<?php for ($i = 0; $i < $counter; $i++) { ?>
							<tr data_id="">
								<td>
									<textarea id="tahapan" class="form-control <?php echo $required ?> " error="Tahapan"></textarea>
								</td>
								<td>
									<textarea id="potensi" class="form-control <?php echo $required ?>" error="Potensi"></textarea>
								</td>
								<td>
									<textarea id="pengendalian" class="form-control <?php echo $required ?>" error="Pengendalian"></textarea>
								</td>
								<td class="text-center">
									<i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addJsa(this)"></i>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		<?php } ?>

		<br>
		<button class='btn btn-warning' onclick="WpEksternal.addJsaItem(this)">Tambah Jsa</button>
	</div>
	<hr>
	<!-- /.box-footer -->

	<div class="box-footer">
		<!--<button type="button" class="btn btn-default" onclick="WpEksternal.back()">Cancel</button>-->
		<!--<button type="submit" class="btn btn-success pull-right" onclick="WpEksternal.selesai('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Selesai</button>-->
	</div>
</div>
