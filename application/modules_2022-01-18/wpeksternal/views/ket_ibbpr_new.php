<!--<div class="col-md-6">
 &nbsp;
</div>-->
<div class="col-md-12">
	<div class="table-responsive">
		<table style="width:100%;">
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:80px;text-align:center;">
					<b>KEMUNGKINAN</b>
				</td>
				<td colspan="5" style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>KONSEKUENSI</b>
				</td>
				<td rowspan="7" style="border:none;width:20px;">
					&nbsp;
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>TINGKAT RESIKO</b>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>KEMUNGKINAN</b>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>KONSEKUENSI</b>
				</td>
			</tr>
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					&nbsp;
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>1</b>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>2</b>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>3</b>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>4</b>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>5</b>
				</td>
				<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<b>E</b> = Extreme Risk
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>E.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Hampir pasti akan terjadi/almost certain</td>
						</tr>
					</table>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>1.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Tidak ada cedera, kerugian materi kecil</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>E</b>
				</td>
				<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>M</b>
				</td>
				<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>M</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>E</b>
				</td>
				<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>E</b>
				</td>
				<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<b>H</b> = High Risk
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>D.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Cenderung untuk terjadi / likely</td>
						</tr>
					</table>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>2.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Cedera ringan/P3K, kerugian cukup materi sedang</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>D</b>
				</td>
				<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>L</b>
				</td>
				<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>M</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>E</b>
				</td>
				<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>E</b>
				</td>
				<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<b>M</b> = Moderate Risk
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>C.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Mungkin dapat terjadi / moderate</td>
						</tr>
					</table>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>3.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Hilang hari kerja, kerugian cukup besar</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>C</b>
				</td>
				<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>L</b>
				</td>
				<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>M</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>E</b>
				</td>
				<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<b>L</b> = Low Risk
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>B.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Kecil kemungkinan terjadi / unlikely</td>
						</tr>
					</table>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>4.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Cacat, kerugian materi besar</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;text-align:center;">
					<b>B</b>
				</td>
				<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>L</b>
				</td>
				<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>L</b>
				</td>
				<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>M</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<td style="background-color:red;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>E</b>
				</td>
				<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					&nbsp;
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>A.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Jarang terjadi/rare</td>
						</tr>
					</table>
				</td>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					<table>
						<tr>
							<td style="margin-top:-8px;"><b>5.</b></td>
							<td>&nbsp;</td>
							<td style="padding:8px;">Kematian, kerugian materi sangat besar</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td style="border:1px solid black;font-family: tahoma;font-size: 12px;width:120px;height:30px;text-align:center;padding:8px;">
					<b>A</b>
				</td>
				<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>L</b>
				</td>
				<td style="background-color:#91d050;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>L</b>
				</td>
				<td style="background-color:#00b0f0;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>M</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<td style="background-color:yellow;border:1px solid black;font-family: tahoma;font-size: 12px;width:40px;text-align:center;">
					<b>H</b>
				</td>
				<!-- <td style="border:none;width:20px;">
						&nbsp;
					</td> -->
				<td style="font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					&nbsp;
				</td>
				<td style="font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					&nbsp;
				</td>
				<td style="font-family: tahoma;font-size: 12px;width:120px;padding:8px;">
					&nbsp;
				</td>
			</tr>
		</table>
	</div>
</div>

<div class="col-md-12">
	<br>
	<b><u>Keterangan</u></b>
	<br>
	<div style="font-family: tahoma;margin-left: 2%;margin-right: 2%;text-align: left;font-size: 12px;">
		<p>
			1. P/R &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = Peluang / Risiko
			<br>
			2. Kon &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = Konsekuensi
			<br>
			3. Kem &nbsp;&nbsp;&nbsp;&nbsp; = Kemungkinan
			<br>
			4. TR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = Tingkat Risiko

		</p>
	</div>
</div>
