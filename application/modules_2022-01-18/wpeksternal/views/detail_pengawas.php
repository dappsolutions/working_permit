<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Pengawas</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_penganggung_jawab">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Nama</th>
      <th>Jabatan</th>
      <th>File Sertifikat K3</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($list_tj)) { ?>
      <?php foreach ($list_tj as $value) { ?>
       <tr>
        <td>
         <input disabled type="text" value="<?php echo $value['nama'] ?>" id="nama" class="form-control required" error="Nama"/>
        </td>
        <td>
         <input disabled type="text" value="<?php echo $value['jabatan'] ?>" id="jabatan_pengawas" class="form-control required" error="Jabatan"/>
        </td>
        <td>
         <div class="input-group">
          <input disabled type="text" class="form-control" value="<?php echo isset($value['file_sk3']) ? $value['file_sk3'] : '' ?>">
          <span class="input-group-addon"><i class="fa fa-file-pdf-o hover-content" file="<?php echo isset($value['file_sk3']) ? $value['file_sk3'] : '' ?>" onclick="WpEksternal.showLogo(this, event, 'sk3')"></i></span>
         </div>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>     
      <tr>
       <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
      </tr>
     <?php } ?>     
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>