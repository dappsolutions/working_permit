<?php

class Wpeksternal extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;
	public $akses;
	public $level;
	public $last_approval;
	public $user;
	public $tgl_ibppr_baru;

	public function __construct()
	{
		parent::__construct();
		// echo '<prE>';
		// print_r($_SESSION);die;
		$this->limit = 25;
		$this->akses = $this->session->userdata('hak_akses');
		$this->level = $this->session->userdata('level');
		$this->last_approval = $this->session->userdata('last_approval');
		$this->user = $this->session->userdata('user_id');
		$this->tgl_ibppr_baru = 20200516;
	}

	public function getModuleName()
	{
		return 'wpeksternal';
	}

	public function getHeaderJSandCSS()
	{
		$version  = str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz');
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/wpeksternal_v1-8.js?v=' . $version . '"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'permit';
	}

	public function getRootModule()
	{
		return "Permohonan";
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = $this->getRootModule() . " - Eksternal";
		$data['title_content'] = 'Eksternal';
		$content = $this->getDataWpeksternal();

		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['open'] = '1';
		// echo '<pre>';
		// print_r($data['content']);
		// die;
		echo Modules::run('template', $data);
	}

	public function getTotalDataWpeksternal($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.no_wp', $keyword),
				array('ph.nama_pemohon', $keyword),
				array('ph.perusahaan', $keyword),
				array('pw.lokasi_pekerjaan', $keyword),
				array('wp.jenis', $keyword),
				array('ps.status', $keyword),
				array('ut.nama', $keyword),
				array('vdn.nama_vendor', $keyword),
			);
		}

		$where = "p.deleted = 0 and p.tipe_permit = 2";
		switch ($this->akses) {
			case "approval":
				$level = $_SESSION['level'] - 1;
				$upt = $_SESSION['upt'];
				$where = "p.deleted = 0 and p.tipe_permit = 2 and pst.level = '" . $level . "' "
					. "and pp.upt = '" . $upt . "' and pst.status != 'REJECTED'";
				break;
			case "vendor":
				$where = "p.deleted = 0 and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
				break;
			default:
				$where = "p.deleted = 0 and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
				break;
		}

		switch ($keyword) {
			case "":
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' p',
					'field' => array(
						'p.*', 'ph.email',
						'ph.nama_pemohon', 'ph.perusahaan',
						'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
						'ph.alamat', 'ph.jabatan',
						'pw.tgl_pekerjaan', 'pw.tgl_awal',
						'pw.tgl_akhir', 'pw.work_place',
						'pw.place', 'pw.lokasi_pekerjaan',
						'pw.uraian_pekerjaan',
						'wp.jenis as jenis_place',
						'pst.status', 'ut.nama as nama_upt',
						'ps.level',
						'sl.nama as nama_single_line',
						'sl.file as file_single_line',
						'sld.nama as nama_sld',
						'sld.file as file_sld',
						'rm.nama as risk_tipe', 'pw.is_need_sistem', 'vdn.nama_vendor'
					),
					'join' => array(
						array('pemohon ph', 'ph.id = p.pemohon'),
						array('permit_work pw', 'pw.permit = p.id'),
						array('work_place wp', 'pw.work_place = wp.id'),
						array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
						array('permit_status ps', 'pss.id = ps.id'),
						array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
						array('permit_status pst', 'pss_s.id = pst.id'),
						array('permit_purpose pp', 'p.id = pp.permit'),
						array('upt ut', 'ut.id = pp.upt'),
						array('(select max(id) id, permit from permit_single_line group by permit) psld', 'p.id = psld.permit', 'left'),
						array('permit_single_line psl', 'psl.id = psld.id', 'left'),
						array('single_line sl', 'psl.single_line = sl.id', 'left'),
						array('(select max(id) id, permit from permit_risk group by permit) prk', 'p.id = prk.permit', 'left'),
						array('permit_risk psk', 'prk.id = psk.id', 'left'),
						array('risk_management rm', 'rm.id = psk.risk_management', 'left'),
						array('(select max(id) id, permit from permit_sld group by permit) psldg', 'p.id = psldg.permit', 'left'),
						array('permit_sld psldi', 'psldi.id = psldg.id', 'left'),
						array('user usr_ven', 'p.user = usr_ven.id', 'left'),
						array('vendor vdn', 'vdn.id = usr_ven.vendor', 'left'),
						array('sld sld', 'psldi.sld = sld.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
						array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
						array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
					),
					'is_or_like' => true,
					'like' => $like,
					'where' => $where
				));
				break;
			default:
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' p',
					'field' => array(
						'p.*', 'ph.email',
						'ph.nama_pemohon', 'ph.perusahaan',
						'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
						'ph.alamat', 'ph.jabatan',
						'pw.tgl_pekerjaan', 'pw.tgl_awal',
						'pw.tgl_akhir', 'pw.work_place',
						'pw.place', 'pw.lokasi_pekerjaan',
						'pw.uraian_pekerjaan',
						'wp.jenis as jenis_place',
						'pst.status', 'ut.nama as nama_upt',
						'ps.level',
						'sl.nama as nama_single_line',
						'sl.file as file_single_line',
						'sld.nama as nama_sld',
						'sld.file as file_sld',
						'rm.nama as risk_tipe', 'pw.is_need_sistem', 'vdn.nama_vendor'
					),
					'join' => array(
						array('pemohon ph', 'ph.id = p.pemohon'),
						array('permit_work pw', 'pw.permit = p.id'),
						array('work_place wp', 'pw.work_place = wp.id'),
						array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
						array('permit_status ps', 'pss.id = ps.id'),
						array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
						array('permit_status pst', 'pss_s.id = pst.id'),
						array('permit_purpose pp', 'p.id = pp.permit'),
						array('upt ut', 'ut.id = pp.upt'),
						array('(select max(id) id, permit from permit_single_line group by permit) psld', 'p.id = psld.permit', 'left'),
						array('permit_single_line psl', 'psl.id = psld.id', 'left'),
						array('single_line sl', 'psl.single_line = sl.id', 'left'),
						array('(select max(id) id, permit from permit_risk group by permit) prk', 'p.id = prk.permit', 'left'),
						array('permit_risk psk', 'prk.id = psk.id', 'left'),
						array('risk_management rm', 'rm.id = psk.risk_management', 'left'),
						array('(select max(id) id, permit from permit_sld group by permit) psldg', 'p.id = psldg.permit', 'left'),
						array('permit_sld psldi', 'psldi.id = psldg.id', 'left'),
						array('user usr_ven', 'p.user = usr_ven.id', 'left'),
						array('vendor vdn', 'vdn.id = usr_ven.vendor', 'left'),
						array('sld sld', 'psldi.sld = sld.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
						array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
						array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
					),
					'is_or_like' => true,
					'like' => $like,
					'inside_brackets' => true,
					'where' => $where
				));
				break;
		}

		return $total;
	}

	public function getDataWpeksternal($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.no_wp', $keyword),
				array('ph.nama_pemohon', $keyword),
				array('ph.perusahaan', $keyword),
				array('pw.lokasi_pekerjaan', $keyword),
				array('wp.jenis', $keyword),
				array('pst.status', $keyword),
				array('ut.nama', $keyword),
				array('vdn.nama_vendor', $keyword),
			);
		}

		$where = "p.deleted = 0 and p.tipe_permit = 2";
		switch ($this->akses) {
			case "approval":
				$level = $_SESSION['level'] - 1;
				$upt = $_SESSION['upt'];
				$where = "p.deleted = 0 and p.tipe_permit = 2 and pst.level = '" . $level . "' "
					. "and pp.upt = '" . $upt . "' and pst.status != 'REJECTED'";
				break;
			case "vendor":
				$where = "p.deleted = 0 and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
				break;
			default:
				$where = "p.deleted = 0 and p.tipe_permit = 2 and ps.user = '" . $this->user . "'";
				break;
		}

		switch ($keyword) {
			case "":
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' p',
					'field' => array(
						'p.*', 'ph.email',
						'ph.nama_pemohon', 'ph.perusahaan',
						'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
						'ph.alamat', 'ph.jabatan',
						'pw.tgl_pekerjaan', 'pw.tgl_awal',
						'pw.tgl_akhir', 'pw.work_place',
						'pw.place', 'pw.lokasi_pekerjaan',
						'pw.uraian_pekerjaan',
						'wp.jenis as jenis_place',
						'pst.status', 'ut.nama as nama_upt',
						'pst.level',
						'sl.nama as nama_single_line',
						'sl.file as file_single_line',
						'sld.nama as nama_sld',
						'sld.file as file_sld',
						'rm.nama as risk_tipe', 'pw.is_need_sistem', 'vdn.nama_vendor',
						'ptl.no_trans as no_trans_simson',
						'ptl_swa.no_trans as no_trans_swa'
					),
					'join' => array(
						array('pemohon ph', 'ph.id = p.pemohon'),
						array('permit_work pw', 'pw.permit = p.id'),
						array('work_place wp', 'pw.work_place = wp.id'),
						array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
						array('permit_status ps', 'pss.id = ps.id'),
						array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
						array('permit_status pst', 'pss_s.id = pst.id'),
						array('permit_purpose pp', 'p.id = pp.permit'),
						array('upt ut', 'ut.id = pp.upt'),
						array('(select max(id) id, permit from permit_single_line group by permit) psld', 'p.id = psld.permit', 'left'),
						array('permit_single_line psl', 'psl.id = psld.id', 'left'),
						array('single_line sl', 'psl.single_line = sl.id', 'left'),
						array('(select max(id) id, permit from permit_risk group by permit) prk', 'p.id = prk.permit', 'left'),
						array('permit_risk psk', 'prk.id = psk.id', 'left'),
						array('risk_management rm', 'rm.id = psk.risk_management', 'left'),
						array('(select max(id) id, permit from permit_sld group by permit) psldg', 'p.id = psldg.permit', 'left'),
						array('permit_sld psldi', 'psldi.id = psldg.id', 'left'),
						array('user usr_ven', 'p.user = usr_ven.id', 'left'),
						array('vendor vdn', 'vdn.id = usr_ven.vendor', 'left'),
						array('sld sld', 'psldi.sld = sld.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
						array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
						array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
					),
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'where' => $where,
					'orderby' => 'p.id desc'
				));
				break;
			default:
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' p',
					'field' => array(
						'p.*', 'ph.email',
						'ph.nama_pemohon', 'ph.perusahaan',
						'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
						'ph.alamat', 'ph.jabatan',
						'pw.tgl_pekerjaan', 'pw.tgl_awal',
						'pw.tgl_akhir', 'pw.work_place',
						'pw.place', 'pw.lokasi_pekerjaan',
						'pw.uraian_pekerjaan',
						'wp.jenis as jenis_place',
						'pst.status', 'ut.nama as nama_upt',
						'pst.level',
						'sl.nama as nama_single_line',
						'sl.file as file_single_line',
						'sld.nama as nama_sld',
						'sld.file as file_sld',
						'rm.nama as risk_tipe', 'pw.is_need_sistem', 'vdn.nama_vendor',
						'ptl.no_trans as no_trans_simson',
						'ptl_swa.no_trans as no_trans_swa'
					),
					'join' => array(
						array('pemohon ph', 'ph.id = p.pemohon'),
						array('permit_work pw', 'pw.permit = p.id'),
						array('work_place wp', 'pw.work_place = wp.id'),
						array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
						array('permit_status ps', 'pss.id = ps.id'),
						array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
						array('permit_status pst', 'pss_s.id = pst.id'),
						array('permit_purpose pp', 'p.id = pp.permit'),
						array('upt ut', 'ut.id = pp.upt'),
						array('(select max(id) id, permit from permit_single_line group by permit) psld', 'p.id = psld.permit', 'left'),
						array('permit_single_line psl', 'psl.id = psld.id', 'left'),
						array('single_line sl', 'psl.single_line = sl.id', 'left'),
						array('(select max(id) id, permit from permit_risk group by permit) prk', 'p.id = prk.permit', 'left'),
						array('permit_risk psk', 'prk.id = psk.id', 'left'),
						array('risk_management rm', 'rm.id = psk.risk_management', 'left'),
						array('(select max(id) id, permit from permit_sld group by permit) psldg', 'p.id = psldg.permit', 'left'),
						array('permit_sld psldi', 'psldi.id = psldg.id', 'left'),
						array('user usr_ven', 'p.user = usr_ven.id', 'left'),
						array('vendor vdn', 'vdn.id = usr_ven.vendor', 'left'),
						array('sld sld', 'psldi.sld = sld.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
						array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
						array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
					),
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'inside_brackets' => true,
					'where' => $where,
					'orderby' => 'p.id desc'
				));
				break;
		}
		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama_place'] = $value['jenis_place'];
				if ($this->akses != 'vendor') {
					$value['nama_place'] = $this->getNameOfPlace($value);
				}

				if ($_SESSION['level'] == '2') {
					if ($_SESSION['second_approval_internal'] == '') {
						array_push($result, $value);
					}
				} else {
					array_push($result, $value);
				}
				//  if($_SESSION['level'] == '2'){
				// 	if($value['work_place'] == 2 || $value['work_place'] == 3){
				// 		$name_of_place = $value['nama_place'];
				// 		$is_show = $this->getDataGarduAndULTGApprovalDua($name_of_place);
				// 		// echo $is_show;die;
				// 		// echo $name_of_place;die;
				// 		if($is_show){
				// 			array_push($result, $value);
				// 		}
				// 	}else{
				// 		array_push($result, $value);	
				// 	}
				// }else{
				// array_push($result, $value);
				// }
				// array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataWpeksternal($keyword)
		);
	}

	public function getDataGarduAndULTGApprovalDua($name_of_place)
	{
		$upt = $_SESSION['upt'];
		$query = "select su.id,su.nama as ultg
	, gi.nama_gardu
	, sha.struktur_approval
	from sub_upt_has_gardu sug
		join sub_upt su
			on su.id = sug.sub_upt
		join gardu_induk gi
			on gi.id = sug.gardu_induk
		join struktur_approval_has_sub_upt sha
			on sha.sub_upt = su.id
		join struktur_approval sa
			on sa.id = sha.struktur_approval	
		where su.upt = " . $upt . " and sa.`user` = '" . $this->user . "' 
		and (gi.nama_gardu = '" . $name_of_place . "' or su.nama = '" . $name_of_place . "')";

		// echo '<pre>';
		// echo $query;die;
		$is_exist = 0;
		$data = Modules::run('database/get_custom', $query);
		if (!empty($data)) {
			$is_exist = 1;
		}

		return $is_exist;
	}

	public function getNameOfPlace($data)
	{
		switch ($data['work_place']) {
			case 1:
				$data_place = $this->getDetailUnit($data['place']);
				break;
			case 2:
				$data_place = $this->getDetailSubUnit($data['place']);
				break;
			case 3:
				$data_place = $this->getDetailGardu($data['place']);
				break;
			case 4:
				$data_place = $this->getDetailSutet($data['place']);
				break;

			default:
				break;
		}
		$nama_place = $data_place['nama'];

		return $nama_place;
	}

	public function getDetailDataWpeksternal($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit p',
			'field' => array(
				'p.*', 'ph.email',
				'ph.nama_pemohon', 'ph.perusahaan',
				'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
				'ph.alamat', 'ph.jabatan',
				'pw.tgl_pekerjaan', 'pw.tgl_awal',
				'pw.tgl_akhir', 'pw.work_place',
				'pw.place', 'pw.lokasi_pekerjaan',
				'pw.uraian_pekerjaan', 'ps.level',
				'ps.keterangan as ket_approve',
				'ps.status as status_wp',
				'pw.is_need_sistem',
				'pw.keterangan_need_sistem'
			),
			'join' => array(
				array('pemohon ph', 'ph.id = p.pemohon'),
				array('permit_work pw', 'pw.permit = p.id'),
				array('(select max(id) id, permit from permit_status group by permit) pss', 'p.id = pss.permit', 'left'),
				array('permit_status ps', 'ps.id = pss.id', 'left'),
			),
			'where' => "p.id = '" . $id . "'"
		));

		$data = $data->row_array();
		switch ($data['work_place']) {
			case 1:
				$data_place = $this->getDetailUnit($data['place']);
				break;
			case 2:
				$data_place = $this->getDetailSubUnit($data['place']);
				break;
			case 3:
				$data_place = $this->getDetailGardu($data['place']);
				break;
			case 4:
				$data_place = $this->getDetailSutet($data['place']);
				break;

			default:
				break;
		}
		$data['nama_place'] = $data_place['nama'];
		$data['upt_id'] = $data_place['upt_id'];
		//  echo '<pre>';
		//  print_r($data_place);die;
		return $data;
	}

	public function getListWorkPlace()
	{
		$data = Modules::run('database/get', array(
			'table' => 'work_place',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPemohon()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pemohon p',
			'where' => "p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListAkibat()
	{
		$data = Modules::run('database/get', array(
			'table' => 'akibat',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPemaparan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pemaparan',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPeluang()
	{
		$data = Modules::run('database/get', array(
			'table' => 'peluang',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Wpeksternal";
		$data['title_content'] = 'Tambah Wpeksternal';
		$data['list_place'] = $this->getListWorkPlace();
		$data['list_pemohon'] = $this->getListPemohon();
		$data['list_akibat'] = $this->getListAkibat();
		$data['list_paparan'] = $this->getListPemaparan();
		$data['list_peluang'] = $this->getListPeluang();
		$data['tgl_ibppr_baru'] = intval(date('Ymd')) >= $this->tgl_ibppr_baru ? '1' : '0';

		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataWpeksternal($id);
		$createddate = date('Ymd', strtotime($data['createddate']));
		//  echo '<pre>';
		//  print_r($data);die;
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Wpeksternal";
		$data['title_content'] = 'Ubah Wpeksternal';
		$data['tgl_ibppr_baru'] = intval($createddate) >= $this->tgl_ibppr_baru ? '1' : '0';
		$data['list_place'] = $this->getListWorkPlace();
		$data['list_pemohon'] = $this->getListPemohon();
		$data['list_akibat'] = $this->getListAkibat();
		$data['list_paparan'] = $this->getListPemaparan();
		$data['list_peluang'] = $this->getListPeluang();
		$data['list_tj'] = $this->getListTjPermit($id);
		$data['list_pelaksana'] = $this->getListPelaksanaPermit($id);
		$data['list_apd'] = $this->getListApdPermit($id);
		$data['list_jsa'] = $this->getListJsaPermit($id);
		// echo '<pre>';
		// print_r($data['list_jsa']);die;
		$data['list_ibppr'] = $this->getListPermitIbppr($id);
		echo Modules::run('template', $data);
	}

	public function getListTjPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_penanggung_jawab ppj',
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPelaksanaPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_pelaksana pp',
			'where' => "pp.deleted = 0 and pp.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListApdPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_apd pa',
			'where' => "pa.deleted = 0 and pa.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListJsaPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_jsa pa',
			'field' => array(
				'pa.*', 'pji.tahapan_pekerjaan',
				'pji.potensi_bahaya', 'pji.pengendalian', 'pji.permit_jsa', 'pji.id as pji_id'
			),
			'join' => array(
				array('permit_jsa_item pji', 'pji.permit_jsa = pa.id'),
			),
			'where' => "pa.deleted = 0 and pa.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				if (!in_array($value['id'], $temp)) {
					$detail = array();
					foreach ($data->result_array() as $v_item) {
						if ($value['id'] == $v_item['permit_jsa']) {
							array_push($detail, $v_item);
						}
					}
					$result[$value['id']] = $detail;
					$temp[] = $value['id'];
				}
			}
		}


		return $result;
	}

	public function getListPermitIbppr($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_ibppr pi',
			'where' => "pi.deleted = 0 and pi.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListStatusPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_status ps',
			'field' => array(
				'ps.*',
				'pg.nama as nama_pegawai',
				'vd.nama_vendor',
				'u.pegawai', 'u.vendor', 'u.username', 'pg.posisi_lengkap'
			),
			'join' => array(
				array('user u', 'ps.user = u.id'),
				array('pegawai pg', 'u.pegawai = pg.id', 'left'),
				array('vendor vd', 'u.vendor = vd.id', 'left'),
			),
			'where' => "ps.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama_aktor'] = $value['username'];
				if ($value['vendor'] != '') {
					$value['nama_aktor'] = $value['nama_vendor'];
				}
				if ($value['pegawai'] != '') {
					$value['nama_aktor'] = $value['nama_pegawai'] . ' <br/> ' . $value['posisi_lengkap'];
				}
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListStatusRejectPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_status ps',
			'field' => array(
				'ps.*',
				'pg.nama as nama_pegawai',
				'vd.nama_vendor',
				'u.pegawai', 'u.vendor', 'u.username', 'pg.posisi_lengkap'
			),
			'join' => array(
				array('user u', 'ps.user = u.id'),
				array('pegawai pg', 'u.pegawai = pg.id', 'left'),
				array('vendor vd', 'u.vendor = vd.id', 'left'),
			),
			'where' => "ps.permit = '" . $id . "' and ps.status = 'REJECTED'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama_aktor'] = $value['username'];
				if ($value['vendor'] != '') {
					$value['nama_aktor'] = $value['nama_vendor'];
				}
				if ($value['pegawai'] != '') {
					$value['nama_aktor'] = $value['nama_pegawai'] . ' <br/> ' . $value['posisi_lengkap'];
				}
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function detail($id)
	{
		$data = $this->getDetailDataWpeksternal($id);
		$createddate = date('Ymd', strtotime($data['createddate']));
		$this->make_barcode($data['no_wp']);
		//  echo '<pre>';
		//  print_r($data);die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Wpeksternal";
		$data['title_content'] = "Detail Wpeksternal";
		$data['tgl_ibppr_baru'] = intval($createddate) >= $this->tgl_ibppr_baru ? '1' : '0';
		$data['list_pemohon'] = $this->getListPemohon();
		$data['list_place'] = $this->getListWorkPlace();
		$data['list_tj'] = $this->getListTjPermit($id);
		$data['list_pelaksana'] = $this->getListPelaksanaPermit($id);
		$data['list_apd'] = $this->getListApdPermit($id);
		$data['list_jsa'] = $this->getListJsaPermit($id);
		$data['list_akibat'] = $this->getListAkibat();
		$data['list_paparan'] = $this->getListPemaparan();
		$data['list_peluang'] = $this->getListPeluang();
		$data['list_ibppr'] = $this->getListPermitIbppr($id);
		$data['status_approve'] = $this->getListStatusPermit($id);
		$data['list_reject'] = $this->getListStatusRejectPermit($id);
		// echo '<pre>';
		// print_r($data['list_reject']);
		// die;
		$data['akses'] = $this->akses;
		echo Modules::run('template', $data);
	}

	public function getPostFormWp($value)
	{
		$data['no_wp'] = Modules::run('no_generator/generateNoPengajuanWP', 'EKS');
		$data['tanggal_wp'] = $value->tgl_wp;
		$data['tipe_permit'] = 2;
		$data['penanggung_jawab_pekerjaan'] = $value->penanggung_jawab_pekerjaan;
		$data['jabatan_penanggung_jawab'] = $value->jabatan_penanggung_jawab;
		$data['pengawas_k3'] = $value->pengawas_k3;
		$data['jabatan_pengawas_k3'] = $value->jabatan_pengawas_k3;
		$data['pengawas_pekerjaan'] = $value->pengawas_pekerjaan;
		$data['jabatan_pengawas_pekerjaan'] = $value->jabatan_pengawas_pekerjaan;
		return $data;
	}

	public function getPostPermitWork($value)
	{
		$data['work_place'] = $value->work_place;
		$data['place'] = $value->place;
		$data['lokasi_pekerjaan'] = $value->lokasi_kerja;
		$data['uraian_pekerjaan'] = $value->uraian_pekerjaan;
		$data['is_need_sistem'] = $value->is_need_sistem;
		if ($value->is_need_sistem == 1) {
			$data['keterangan_need_sistem'] = $value->keterangan_need_sistem;
		}
		return $data;
	}

	public function getPostPengawas($value)
	{
		$data['nama'] = $value->nama;
		$data['jabatan'] = $value->jabatan;
		return $data;
	}

	public function getPostPelaksana($value)
	{
		$data['nama'] = $value->nama;
		return $data;
	}

	public function getPostApd($value)
	{
		$data['nama_alat'] = $value->nama_alat;
		$data['ketersediaan'] = $value->ketersediaan;
		return $data;
	}

	public function getPostJsa($value)
	{
		$data['tahapan_pekerjaan'] = $value->tahapan;
		$data['potensi_bahaya'] = $value->potensi;
		$data['pengendalian'] = $value->pengendalian;
		return $data;
	}

	public function getPostIbbpr($value)
	{
		$data['kegiatan'] = $value->kegiatan;
		$data['potensi_bahaya'] = $value->potensi_bahaya;
		$data['resiko'] = $value->resiko;
		$data['akibat'] = $value->akibat;
		$data['paparan'] = $value->paparan;
		$data['peluang'] = $value->peluang;
		$data['nilai'] = $value->nilai;
		$data['tingkat_resiko'] = $value->tingkat_resiko;
		$data['pengendalian'] = $value->pengendalian;
		$data['akibat_after'] = $value->akibat_after;
		$data['paparan_after'] = $value->paparan_after;
		$data['peluang_after'] = $value->peluang_after;
		$data['nilai_after'] = $value->nilai_after;
		$data['tingkat_resiko_after'] = $value->tingkat_resiko_after;
		return $data;
	}

	public function getPostNewIbbpr($value)
	{
		$data['pihak'] = $value->pihak;
		$data['kegiatan'] = $value->kegiatan;
		$data['potensi_bahaya'] = $value->potensi_bahaya;
		$data['resiko'] = $value->resiko;
		$data['pr'] = $value->pr;
		$data['kon'] = $value->kon;
		$data['kem'] = $value->kem;
		$data['tr'] = $value->tr;
		$data['kon_after'] = $value->kon_after;
		$data['kem_after'] = $value->kem_after;
		$data['tr_after'] = $value->tr_after;
		$data['status_pengendalian'] = $value->status_pengendalian;
		$data['pengendalian'] = $value->pengendalian;
		$data['penanggung_jawab'] = $value->tanggung_jawab;
		return $data;
	}

	public function getPostPemohon($value)
	{
		$data['email'] = $this->session->userdata('username');
		$data['nama_pemohon'] = $value->nama_pemohon;
		$data['perusahaan'] = $value->perusahaan;
		$data['jabatan'] = $value->jabatan;
		$data['alamat'] = $value->alamat;
		$data['no_telp'] = $value->no_telp;
		$data['no_hp'] = $value->no_hp;
		return $data;
	}

	public function getPostJsaHeader($value)
	{
		$data['no_jsa'] = Modules::run('no_generator/generateNoJsa');
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$state = $_POST['state'];
		// echo '<pre>';
		// print_r($data);
		// die;
		$user_id = $this->session->userdata('user_id');
		//  echo '<pre>';
		//  print_r($_FILES);die;
		$id = $this->input->post('id');
		$file = $_FILES;
		$is_valid = false;
		$is_save = true;
		$is_login = true;
		$message = "Anda Belum Melakukan Pengisian Pada Form Sebelumnya.";

		$insert = "";
		if ($id == '') {
			$this->db->trans_begin();
		}

		if ($this->user == '') {
			$is_login = false;
			$message = "Session Telah Habis";
		}

		if ($is_login) {
			try {
				//pemohon
				$form_pemohon = $data->form_pemohon;
				$pemohon = $form_pemohon->pemohon_id;
				if ($form_pemohon->pemohon_id == 'add_pemohon') {
					$post_pemohon = $this->getPostPemohon($form_pemohon);
					$pemohon = Modules::run('database/_insert', 'pemohon', $post_pemohon);
				}

				if ($id == '') {

					//     echo $state;die;
					//form permit
					if ($state == 'pemohon') {
						$post_wp = $this->getPostFormWp($data->form_wp);
						$post_wp['pemohon'] = $pemohon;
						$post_wp['user'] = $user_id;

						if (!empty($file)) {
							if (isset($file['file_spk'])) {
								$response_upload = $this->uploadData('file_spk', 'spk');
								if ($response_upload['is_valid']) {
									$post_wp['file_spk'] = $file['file_spk']['name'];
								} else {
									$is_save = false;
									$message = $response_upload['response'];
								}
							}
						}

						$permit = Modules::run('database/_insert', 'permit', $post_wp);
						$id = $permit;

						//permit status
						$post_permit_status['user'] = $user_id;
						$post_permit_status['permit'] = $permit;
						$post_permit_status['status'] = 'DRAFT';
						$post_permit_status['level'] = '0';

						Modules::run('database/_insert', 'permit_status', $post_permit_status);

						//form permit work    
						$post_permit_work = $this->getPostPermitWork($data->form_work);
						$post_permit_work['permit'] = $permit;
						$post_permit_work['tgl_pekerjaan'] = $data->form_wp->tgl_awal;
						$post_permit_work['tgl_awal'] = $data->form_wp->tgl_awal;
						$post_permit_work['tgl_akhir'] = $data->form_wp->tgl_akhir;
						Modules::run('database/_insert', 'permit_work', $post_permit_work);

						//form purpose 
						$post_permit_purpose['upt'] = $data->form_purpose->upt;
						$post_permit_purpose['permit'] = $permit;
						Modules::run('database/_insert', 'permit_purpose', $post_permit_purpose);
					}

					//     echo $_POST['state'];die;

					if ($_POST['state'] == 'penanggung_jawab' && $id != '') {
						//form tj
						$form_penganngng_jawab = $data->form_tj;
						if (!empty($form_penganngng_jawab->data)) {
							$index = 0;
							foreach ($form_penganngng_jawab->data as $value) {
								$post_tj = $this->getPostPengawas($value);
								$post_tj['permit'] = $permit;


								if (!empty($file)) {
									$name_of_file = 'file_tj_' . $index;
									if (isset($file[$name_of_file])) {
										$response_upload = $this->uploadData($name_of_file, 'sk3');
										if ($response_upload['is_valid']) {
											$post_tj['file_sk3'] = $file[$name_of_file]['name'];
										} else {
											$is_save = false;
											$message = $response_upload['response'];
										}
									}
								}

								Modules::run('database/_insert', 'permit_penanggung_jawab', $post_tj);

								$index += 1;
							}
						}
					} else {
						if ($_POST['state'] == 'penanggung_jawab') {
							//       echo 'asdasd';die;
							$is_valid = false;
							$is_save = false;
						}
					}

					if ($_POST['state'] == 'pelaksana' && $id != '') {
						//form pelaksana
						$form_pelaksana = $data->form_pelaksana;
						if (!empty($form_pelaksana->data)) {
							foreach ($form_pelaksana->data as $value) {
								$post_pelaksana = $this->getPostPelaksana($value);
								$post_pelaksana['permit'] = $permit;
								Modules::run('database/_insert', 'permit_pelaksana', $post_pelaksana);
							}
						}
					} else {
						if ($_POST['state'] == 'pelaksana') {
							$is_valid = false;
							$is_save = false;
						}
					}

					if ($_POST['state'] == 'apd' && $id != '') {
						//form apd
						$form_apd = $data->form_apd;
						if (!empty($form_apd->data)) {
							foreach ($form_apd->data as $value) {
								$post_apd = $this->getPostApd($value);
								$post_apd['permit'] = $permit;
								Modules::run('database/_insert', 'permit_apd', $post_apd);
							}
						}
					} else {
						if ($_POST['state'] == 'apd') {
							$is_valid = false;
							$is_save = false;
						}
					}

					if ($_POST['state'] == 'ibppr' && $id != '') {
						//form ibppr
						$form_ibppr = $data->form_ibppr;
						if (!empty($form_ibppr->data)) {
							foreach ($form_ibppr->data as $value) {
								$post_ibbpr = $this->getPostIbbpr($value);
								$post_ibbpr['permit'] = $permit;
								Modules::run('database/_insert', 'permit_ibppr', $post_ibbpr);
							}
						}
					} else {
						if ($_POST['state'] == 'ibppr') {
							$is_valid = false;
							$is_save = false;
						}
					}

					if ($_POST['state'] == 'jsa' && $id != '') {
						//form jsa
						$form_jsa = $data->form_jsa;
						// echo '<pre>';
						// print_r($form_jsa);die;
						if (!empty($form_jsa->data)) {
							foreach ($form_jsa->data as $v_head) {
								$data_item = $v_head->data_item;
								if (!empty($data_item)) {
									//insert into permi jsa		
									$post_jsa_head = $this->getPostJsaHeader($data_item);
									$post_jsa_head['judul_jsa'] = $v_head->judul_jsa;
									$post_jsa_head['permit'] = $permit;
									$permit_jsa = Modules::run('database/_insert', 'permit_jsa', $post_jsa_head);
									foreach ($data_item as $value) {
										$post_jsa = $this->getPostJsa($value);
										$post_jsa['permit_jsa'] = $permit_jsa;
										Modules::run('database/_insert', 'permit_jsa_item', $post_jsa);
									}
								}
							}
						}
					} else {
						if ($_POST['state'] == 'jsa') {
							$is_valid = false;
							$is_save = false;
						}
					}
				} else {
					//update
					$insert = "update";
					$permit = $id;


					if ($_POST['state'] == 'pemohon') {
						//form permit
						$post_wp = $this->getPostFormWp($data->form_wp);
						$post_wp['pemohon'] = $pemohon;
						$post_wp['user'] = $user_id;
						unset($post_wp['no_wp']);

						if (!empty($file)) {
							if (isset($file['file_spk'])) {
								if ($file['file_spk']['name'] != $data->form_work->file_str) {
									$response_upload = $this->uploadData('file_spk', 'spk');
									if ($response_upload['is_valid']) {
										$post_wp['file_spk'] = $file['file_spk']['name'];
									} else {
										$is_save = false;
										$message = $response_upload['response'];
									}
								}
							}
						}

						$this->db->update('permit', $post_wp, array('id' => $permit));


						//form permit work    
						$post_permit_work = $this->getPostPermitWork($data->form_work);
						$post_permit_work['tgl_pekerjaan'] = $data->form_wp->tgl_awal;
						$post_permit_work['tgl_awal'] = $data->form_wp->tgl_awal;
						$post_permit_work['tgl_akhir'] = $data->form_wp->tgl_akhir;
						Modules::run('database/_update', 'permit_work', $post_permit_work, array('permit' => $id));


						//form purpose 
						$post_permit_purpose['upt'] = $data->form_purpose->upt;
						Modules::run('database/_update', 'permit_purpose', $post_permit_purpose, array('permit' => $id));
					}


					//form tj
					if ($_POST['state'] == 'penanggung_jawab') {
						$form_penganngng_jawab = $data->form_tj;
						//    $data_tj = array();
						//    echo '<pre>';
						//    print_r($form_penganngng_jawab);die;
						if (!empty($form_penganngng_jawab->data)) {
							$index = 0;
							foreach ($form_penganngng_jawab->data as $value) {
								if ($value->id == '') {
									if ($value->nama != '') {
										$post_tj = $this->getPostPengawas($value);
										$post_tj['permit'] = $id;

										//      echo '<pre>';
										//      print_r($value);die;
										if (!empty($file)) {
											$name_of_file = 'file_tj_' . $index;
											if (isset($file[$name_of_file])) {
												$file_str = "";
												if (isset($value->file_str)) {
													$file_str = $value->file_str;
												}
												if ($file[$name_of_file]['name'] != $file_str) {
													$response_upload = $this->uploadData($name_of_file, 'sk3');
													if ($response_upload['is_valid']) {
														$post_tj['file_sk3'] = $file[$name_of_file]['name'];
													} else {
														$is_save = false;
														$message = $response_upload['response'];
													}
												}
											}
										}
										Modules::run('database/_insert', 'permit_penanggung_jawab', $post_tj);
										//        array_push($data_tj, $post_tj);
									}
								} else {
									$post_tj = $this->getPostPengawas($value);
									$post_tj['permit'] = $id;

									//      echo '<pre>';
									//      print_r($value);die;
									if (!empty($file)) {
										$name_of_file = 'file_tj_' . $index;
										if (isset($file[$name_of_file])) {
											$file_str = "";
											if (isset($value->file_str)) {
												$file_str = $value->file_str;
											}
											if ($file[$name_of_file]['name'] != $file_str) {
												$response_upload = $this->uploadData($name_of_file, 'sk3');
												if ($response_upload['is_valid']) {
													$post_tj['file_sk3'] = $file[$name_of_file]['name'];
												} else {
													$is_save = false;
													$message = $response_upload['response'];
												}
											}
										}
									}
									$post_tj['deleted'] = $value->deleted;
									Modules::run('database/_update', 'permit_penanggung_jawab', $post_tj, array('id' => $value->id));
									//       array_push($data_tj, $post_tj);
								}

								$index += 1;
							}
						}
					}


					if ($_POST['state'] == 'pelaksana') {
						//form pelaksana
						$form_pelaksana = $data->form_pelaksana;
						if (!empty($form_pelaksana->data)) {
							foreach ($form_pelaksana->data as $value) {
								if ($value->id == '') {
									if ($value->nama != '') {
										$post_pelaksana = $this->getPostPelaksana($value);
										$post_pelaksana['permit'] = $permit;
										Modules::run('database/_insert', 'permit_pelaksana', $post_pelaksana);
									}
								} else {
									$post_pelaksana = $this->getPostPelaksana($value);
									$post_pelaksana['permit'] = $permit;
									$post_pelaksana['deleted'] = $value->deleted;
									Modules::run('database/_update', 'permit_pelaksana', $post_pelaksana, array('id' => $value->id));
								}
							}
						}
					}

					if ($_POST['state'] == 'apd') {
						//form apd
						$form_apd = $data->form_apd;
						if (!empty($form_apd->data)) {
							foreach ($form_apd->data as $value) {
								if ($value->id == '') {
									if ($value->nama_alat != '') {
										$post_apd = $this->getPostApd($value);
										$post_apd['permit'] = $permit;
										Modules::run('database/_insert', 'permit_apd', $post_apd);
									}
								} else {
									$post_apd = $this->getPostApd($value);
									$post_apd['permit'] = $permit;
									$post_apd['deleted'] = $value->deleted;
									Modules::run('database/_update', 'permit_apd', $post_apd, array('id' => $value->id));
								}
							}
						}
					}


					if ($_POST['state'] == 'ibppr') {
						//form ibppr
						$form_ibppr = $data->form_ibppr;
						// echo '<pre>';
						// print_r($form_ibppr->data);
						// die;
						if (!empty($form_ibppr->data)) {
							foreach ($form_ibppr->data as $value) {
								if ($value->id == '') {
									if ($value->kegiatan != '') {
										$post_ibbpr =  $data->tgl_ibppr_baru == '1' ? $this->getPostNewIbbpr($value) : $this->getPostIbbpr($value);
										// echo '<pre>';
										// print_r($post_ibbpr);
										// die;
										// $post_ibbpr = $this->getPostNewIbbpr($value);
										$post_ibbpr['permit'] = $permit;
										Modules::run('database/_insert', 'permit_ibppr', $post_ibbpr);
									}
								} else {
									$post_ibbpr =  $data->tgl_ibppr_baru == '1' ? $this->getPostNewIbbpr($value) : $this->getPostIbbpr($value);
									$post_ibbpr['permit'] = $permit;
									$post_ibbpr['deleted'] = $value->deleted;
									Modules::run('database/_update', 'permit_ibppr', $post_ibbpr, array('id' => $value->id));
								}
							}
						}
					}

					if ($_POST['state'] == 'jsa') {
						//form jsa
						$form_jsa = $data->form_jsa;
						//  echo '<pre>';
						//  print_r($form_jsa);die;
						if (!empty($form_jsa->data)) {
							foreach ($form_jsa->data as $v_head) {
								$data_item = $v_head->data_item;
								$permit_jsa = '';
								// echo $v_head->id;die;
								if ($v_head->id != '') {
									$post_jsa_head = array();
									$post_jsa_head['judul_jsa'] = $v_head->judul_jsa;
									Modules::run('database/_update', 'permit_jsa', $post_jsa_head, array('id' => $v_head->id));
									$permit_jsa = $v_head->id;
								} else {
									$post_jsa_head = $this->getPostJsaHeader($data_item);
									$post_jsa_head['judul_jsa'] = $v_head->judul_jsa;
									$post_jsa_head['permit'] = $id;
									$permit_jsa = Modules::run('database/_insert', 'permit_jsa', $post_jsa_head);
								}
								if (!empty($data_item)) {
									// echo '<pre>';
									// print_r($data_item);die;
									foreach ($data_item as $value) {
										if ($value->id == '') {
											if ($value->tahapan != '') {
												// echo '<pre>';
												// print_r($value);die;
												$post_jsa = $this->getPostJsa($value);
												$post_jsa['permit_jsa'] = $permit_jsa;
												Modules::run('database/_insert', 'permit_jsa_item', $post_jsa);
											}
										} else {
											// echo '<pre>';
											// print_r($value);die;
											$post_jsa = $this->getPostJsa($value);
											$post_jsa['deleted'] = $value->deleted;
											Modules::run('database/_update', 'permit_jsa_item', $post_jsa, array('id' => $value->id));
										}
									}
								}
							}
						}
					}

					//permit status
					//  if ($_POST['state'] == 'jsa') {
					if ($this->akses == 'vendor') {
						if ($data->status_wp == 'REJECTED') {
							$post_permit_status['user'] = $user_id;
							$post_permit_status['permit'] = $permit;
							$post_permit_status['status'] = 'DRAFT';
							$post_permit_status['level'] = '0';


							Modules::run('database/_insert', 'permit_status', $post_permit_status);
						}
					}
					//  }
				}

				if ($is_save) {
					if ($insert == '') {
						$this->db->trans_commit();
						Modules::run('purpose/sendSmsToAdminUnit', array('upt' => $data->form_purpose->upt));
					}
					$is_valid = true;
				}
			} catch (Exception $ex) {
				if ($id == '') {
					$this->db->trans_rollback();
				}
			}
		}


		//  echo $is_save . ' dan ' . $is_valid . ' dan ' . $message;
		//  die;
		echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message, 'is_login' => $is_login));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Wpeksternal";
		$data['title_content'] = 'Data Wpeksternal';
		$content = $this->getDataWpeksternal($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['open'] = Modules::run('wpinternal/getTimeAktifPengajuan');
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function uploadData($name_of_field, $dir)
	{
		$dir = 'spk';
		$config['upload_path'] = 'files/berkas/' . $dir . '/';
		$config['allowed_types'] = 'png|jpg|jpeg|pdf';
		$config['max_size'] = '1000';
		$config['max_width'] = '6000';
		$config['max_height'] = '6000';

		$this->load->library('upload', $config);

		$is_valid = false;
		if (!$this->upload->do_upload($name_of_field)) {
			$response = $this->upload->display_errors();
		} else {
			$response = $this->upload->data();
			$is_valid = true;
		}

		return array(
			'is_valid' => $is_valid,
			'response' => $response
		);
	}

	public function showLogo()
	{
		$foto = str_replace(' ', '_', $this->input->post('foto'));
		$data['foto'] = $foto;
		// $data['jenis'] = $_POST['jenis'];
		$data['jenis'] = 'spk';
		echo $this->load->view('foto', $data, true);
	}

	public function getListUnit()
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt u',
			'field' => array('u.*'),
			'where' => "u.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['upt_id'] = $value['id'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailUnit($unit)
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt u',
			'field' => array('u.*'),
			'where' => "u.id = '" . $unit . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['id'];
		}


		return $result;
	}

	public function getListSubUnit()
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt su',
			'field' => array('su.*', 'u.nama as nama_upt'),
			'join' => array(
				array('upt u', 'su.upt = u.id')
			),
			'where' => "su.deleted = 0",
			'orderby' => 'u.nama asc'
		));

		$result = array();
		if (!empty($data)) {
			//   echo '<pre>';
			//   print_r($data->result_array());die;
			foreach ($data->result_array() as $value) {
				$value['upt_id'] = $value['upt'];
				$value['nama'] = $value['nama_upt'] . ' - ' . $value['nama'];
				array_push($result, $value);
			}
		}
		//  echo '<pre>';
		//  print_r($result);die;

		return $result;
	}

	public function getDetailSubUnit($sub_unit)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt su',
			'field' => array('su.*', 'u.nama as nama_upt'),
			'join' => array(
				array('upt u', 'su.upt = u.id')
			),
			'where' => "su.id = '" . $sub_unit . "'",
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['upt'];
			$result['nama'] = $result['nama_upt'] . ' - ' . $result['nama'];;
		}
		//  echo '<pre>';
		//  print_r($result);die;

		return $result;
	}

	public function getListGardu()
	{
		$data = Modules::run('database/get', array(
			'table' => 'gardu_induk gi',
			'field' => array('gi.*', 'ut.nama as nama_upt'),
			'join' => array(
				array('upt ut', 'gi.upt = ut.id')
			),
			'where' => "gi.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama'] = $value['nama_gardu'];
				$value['upt_id'] = $value['upt'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailGardu($gi)
	{
		$data = Modules::run('database/get', array(
			'table' => 'gardu_induk gi',
			'field' => array('gi.*', 'ut.nama as nama_upt', 'pg.nama as nama_pegawai'),
			'join' => array(
				array('upt ut', 'gi.upt = ut.id'),
				array('gardu_induk_has_pegawai pgh', 'pgh.gardu_induk = gi.id', 'left'),
				array('pegawai pg', 'pg.id = pgh.pegawai', 'left'),
			),
			'where' => "gi.id = '" . $gi . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['nama'] = $result['nama_gardu'];
			$result['upt_id'] = $result['upt'];
			$result['nama_pegawai'] = $result['nama_pegawai'];
		}


		return $result;
	}

	public function getListSutet()
	{
		$data = Modules::run('database/get', array(
			'table' => 'sutet s',
			'field' => array('s.*', 'ut.nama as nama_upt', 'ut.id as upt', 'gi.nama_gardu'),
			'join' => array(
				array('gardu_induk gi', 'gi.id = s.gardu_induk'),
				array('upt ut', 'gi.upt = ut.id'),
			),
			'where' => "s.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['upt_id'] = $value['upt'];
				$value['nama'] = $value['nama_gardu'] . ' : ' . 'SUTET / SUTT - ' . $value['nama'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailSutet($sutet)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sutet s',
			'field' => array(
				's.*', 'ut.nama as nama_upt',
				'ut.id as upt', 'gi.nama_gardu', 'pg.nama as nama_pegawai'
			),
			'join' => array(
				array('gardu_induk gi', 'gi.id = s.gardu_induk'),
				array('upt ut', 'gi.upt = ut.id'),
				array('gardu_induk_has_pegawai pgh', 'pgh.gardu_induk = gi.id', 'left'),
				array('pegawai pg', 'pg.id = pgh.pegawai', 'left'),
			),
			'where' => "s.id = '" . $sutet . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['upt'];
			$result['nama'] = 'SUTET - ' . $result['nama_gardu'] . ' - ' . $result['nama'];
			$result['nama_pegawai'] = $result['nama_pegawai'];
		}


		return $result;
	}

	public function getDetailTempat()
	{
		$work_place = $_POST['work_place'];
		$data = array();
		switch ($work_place) {
			case 1:
				$data = $this->getListUnit();
				break;
			case 2:
				$data = $this->getListSubUnit();
				break;
			case 3:
				$data = $this->getListGardu();
				break;
			case 4:
				$data = $this->getListSutet();
				break;

			default:
				break;
		}

		$conten['list_wp'] = $data;
		echo $this->load->view('list_detail_tempat', $conten, true);
	}

	public function execApprove()
	{
		$permit = $_POST['permit'];
		$action = $_POST['action'];
		$action = $action == 'approve' ? 'APPROVED' : 'REJECTED';
		$user = $this->session->userdata('user_id');

		$data = $this->getDetailDataWpeksternal($permit);

		// echo '<pre>';
		// print_r($data);
		// die;
		$is_valid = false;
		$this->db->trans_begin();
		try {
			$post_data['permit'] = $permit;
			$post_data['status'] = $action;
			$post_data['level'] = $_SESSION['level'];
			$post_data['user'] = $user;
			$post_data['keterangan'] = $_POST['keterangan'];
			Modules::run('database/_insert', 'permit_status', $post_data);

			// if ($this->last_approval == 1) {
			// 	if ($action == 'APPROVED') {
			// 		$this->simpanPdfWp($permit);
			// 	}
			// }

			$this->db->trans_commit();

			$data_send['upt'] = $data['upt_id'];
			$data_send['work_place'] = $data['work_place'];
			$data_send['place'] = $data['place'];
			$data_send['permit'] = $permit;
			$data_send['action'] = $action;
			Modules::run('purpose/sendNotifWpToApproval', $data_send, 'eksternal');
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function getDetailPemohon()
	{
		$pemohon = $_POST['pemohon_id'];
		$data = Modules::run('database/get', array(
			'table' => 'pemohon',
			'where' => "id = '" . $pemohon . "'"
		));

		echo json_encode($data->row_array());
	}

	public function getDataDokumenWp($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array(
				'p.*', 'tp.tipe', 'pw.place',
				'pw.work_place',
				'wp.jenis as jenis_lokasi',
				'pw.lokasi_pekerjaan',
				'pw.tgl_pekerjaan',
				'pw.uraian_pekerjaan',
				'pw.tgl_awal',
				'pw.tgl_akhir',
				'ph.perusahaan',
				'ph.nama_pemohon',
				'ut.nama as upt_tujuan',
				'sl.file as file_single_line',
				'sld.file as file_sld'
			),
			'join' => array(
				array('tipe_permit tp', 'p.tipe_permit = tp.id'),
				array('pemohon ph', 'p.pemohon = ph.id'),
				array('permit_work pw', 'p.id = pw.permit'),
				array('work_place wp', 'pw.work_place = wp.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array('upt ut', 'pp.upt = ut.id'),
				array('(select max(id) id, permit from permit_single_line group by permit) psld', 'p.id = psld.permit', 'left'),
				array('permit_single_line psl', 'psl.id = psld.id', 'left'),
				array('single_line sl', 'psl.single_line = sl.id', 'left'),
				array('(select max(id) id, permit from permit_sld group by permit) psldg', 'p.id = psldg.permit', 'left'),
				array('permit_sld psldi', 'psldi.id = psldg.id', 'left'),
				array('sld sld', 'psldi.sld = sld.id', 'left'),
			),
			'where' => "p.id = '" . $id . "'"
		));

		$data = $data->row_array();
		$data['tgl_awal'] = Modules::run('helper/getIndoDate', $data['tgl_awal']);
		$data['tgl_akhir'] = Modules::run('helper/getIndoDate', $data['tgl_akhir']);
		$data['spv_gi'] = "";
		switch ($data['work_place']) {
			case 1:
				$data_place = $this->getDetailUnit($data['place']);
				break;
			case 2:
				$data_place = $this->getDetailSubUnit($data['place']);
				break;
			case 3:
				$data_place = $this->getDetailGardu($data['place']);
				$data['spv_gi'] = $data_place['nama_pegawai'];
				break;
			case 4:
				$data_place = $this->getDetailSutet($data['place']);
				$data['spv_gi'] = $data_place['nama_pegawai'];
				break;

			default:
				break;
		}
		$data['nama_place'] = $data_place['nama'];
		$data['upt_id'] = $data_place['upt_id'];
		return $data;
	}

	public function getListParafStrukturApproval($upt)
	{
		$sql = "
select sa.*
	, sap.file
	, pg.nama as nama_pegawai
	, pg.nip
	, sa.`level`
	from struktur_approval sa
	join struktur_approval_paraf sap
		on sap.struktur_approval = sa.id
	join user us
		on us.id = sa.`user`
	join pegawai pg
		on pg.id = us.pegawai
	where sa.upt = " . $upt . " and sa.deleted = 0 and sa.tipe_permit = 2
	order by sa.`level` ASC";

		$data = Modules::run('database/get_custom', $sql);
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataApprover($upt, $level, $permit = "")
	{
		$join = "";
		if ($permit != "") {
			$join = "join permit_status ps
		on ps.`user` =  sa.`user` and ps.permit = " . $permit . "";
		}
		$sql = "
select sa.*
	, sap.file
	, pg.nama as nama_pegawai
	, pg.nip
	, sa.`level`
	from struktur_approval sa
	join struktur_approval_paraf sap
		on sap.struktur_approval = sa.id
	join user us
		on us.id = sa.`user`
	join pegawai pg
		on pg.id = us.pegawai
	" . $join . "
	where sa.upt = " . $upt . " and sa.level = '" . $level . "' and sa.deleted = 0 and sa.tipe_permit = 2
	order by sa.`level` ASC";

		$data = Modules::run('database/get_custom', $sql);
		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}


		return $result;
	}

	public function getDokumenDp3Aktif()
	{
		$data = Modules::run('database/get', array(
			'table' => 'dp3',
			'where' => "deleted = 0 and id = 2"
		));

		$data = $data->row_array();
		$data['berlaku_efektif'] = Modules::run('helper/getIndoDate', $data['berlaku_efektif']);
		return $data;
	}

	public function getDataPenanggungJawab($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_penanggung_jawab ppj',
			'field' => array('ppj.*'),
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataJsa($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_jsa ppj',
			'field' => array(
				'ppj.*', 'pji.tahapan_pekerjaan',
				'pji.potensi_bahaya', 'pji.pengendalian', 'pji.permit_jsa'
			),
			'join' => array(
				array('permit_jsa_item pji', 'pji.permit_jsa = ppj.id')
			),
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				if (!in_array($value['id'], $temp)) {
					$detail = array();
					foreach ($data->result_array() as $v_item) {
						if ($value['id'] == $v_item['permit_jsa']) {
							array_push($detail, $v_item);
						}
					}
					$result[$value['id']] = $detail;
					$temp[] = $value['id'];
				}
			}
		}


		return $result;
	}

	public function getDataApd($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_apd ppj',
			'field' => array('ppj.*'),
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataPelaksana($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_pelaksana ppj',
			'field' => array('ppj.*'),
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataPermitIbbpr($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_ibppr ppj',
			'field' => array(
				'ppj.*',
				"REPLACE(potensi_bahaya, '\n', '<br>') as potensi",
				"REPLACE(pengendalian, '\n', '<br>') as pengendalian_risiko",
			),
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getCatatanKhusus()
	{
		$data = Modules::run('database/get', array(
			'table' => 'catatan_khusus c',
			'field' => array('c.*'),
			'where' => "c.deleted = 0"
		));

		return $data;
	}

	public function make_barcode($barcode)
	{
		try {
			$this->load->library('ciqrcode');
			$qr_code = $this->ciqrcode;
			$param['data'] = $barcode;
			$param['level'] = 'H';
			$param['size'] = 20;
			$param['savename'] = 'files/berkas/qrcode/' . $barcode . '.png';
			$qr_code->generate($param);
		} catch (Exception $ex) {
		}
	}

	public function cetak($id)
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		//
		//  echo '<pre>';
		//  var_dump(get_class_methods($mpdf));
		//  die;
		//  $pdf = new mPDF('A4');
		//  $view = $this->load->view('cetak', $data, true);
		//  $mpdf->WriteHTML($view);
		$data_wp = $this->getDataDokumenWp($id);
		// echo '<pre>';
		// print_r($data_wp);
		// die;
		$createddate = date('Ymd', strtotime($data_wp['createddate']));
		$this->make_barcode($data_wp['no_wp']);
		$upt_id = $data_wp['upt_id'];
		$data_wp['paraf'] = $this->getListParafStrukturApproval($upt_id);
		// echo '<pre>';
		// print_r($data_wp);die;
		$data_wp['dp3'] = $this->getDokumenDp3Aktif();
		$data_wp['penanggung_jawab'] = $this->getDataPenanggungJawab($id);
		$data_wp['jsa'] = $this->getDataJsa($id);
		$data_wp['apd'] = $this->getDataApd($id);
		$data_wp['pelaksana'] = $this->getDataPelaksana($id);
		$data_wp['ibppr'] = $this->getDataPermitIbbpr($id);
		$data_wp['approval_dua'] = $this->getDataApprover($upt_id, 2, $id);
		$data_wp['approval_last'] = $this->getDataApprover($upt_id, 3, $id);
		//  echo '<pre>';
		//  print_r($data_wp);
		//  die;
		$data['data_wp'] = $data_wp;
		$data['catatan_khusus'] = $this->getCatatanKhusus();
		//  echo '<pre>';
		//  print_r($data['data_wp']);die;
		$cover = $this->load->view('cetak/cover_ijin_kerja', $data, true);
		//echo $cover;die;
		$hal_1 = $this->load->view('cetak/dokumen_ijin_hal_satu', $data, true);
		$hal_3 = $this->load->view('cetak/dokumen_ijin_hal_tiga', $data, true);
		$hal_4 = $this->load->view('cetak/dokumen_ijin_hal_empat', $data, true);
		$hal_5 = $this->load->view('cetak/dokumen_ijin_hal_lima', $data, true);
		$hal_6 = $this->load->view('cetak/dokumen_ijin_hal_enam', $data, true);
		$hal_ibpr = intval($createddate) >= $this->tgl_ibppr_baru ? $this->load->view('cetak/hal_ibpr_new_hal_satu', $data, true) : $this->load->view('cetak/hal_ibpr', $data, true);
		$hal_ibpr_dua = $this->load->view('cetak/hal_ibpr_new_hal_dua', $data, true);

		$mpdf->WriteHTML($cover);
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_1);
		// echo '<pre>';
		// print_r($data_wp['jsa']);die;
		if (!empty($data_wp['jsa'])) {
			foreach ($data_wp['jsa'] as $value) {
				$data['jsa_data'] = $value;
				$hal_2 = $this->load->view('cetak/dokumen_ijin_hal_dua', $data, true);
				$mpdf->AddPage();
				$mpdf->WriteHTML($hal_2);
			}
		}
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_3);
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_4);
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_5);
		$mpdf->AddPage('L');
		$mpdf->WriteHTML($hal_6);
		$mpdf->AddPage('L');
		$mpdf->WriteHTML($hal_ibpr);
		if (intval($createddate) >= $this->tgl_ibppr_baru) {
			$mpdf->AddPage('L');
			$mpdf->WriteHTML($hal_ibpr_dua);
		}

		$content['file_sk3'] = str_replace(' ', '_', $data_wp['file_spk']);
		//    echo '<pre>';
		//    print_r($content);die;
		$lampiran_file = $this->load->view('cetak/lampiran_file_pengawas', $content, true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($lampiran_file);

		if (!empty($data_wp['penanggung_jawab'])) {
			foreach ($data_wp['penanggung_jawab'] as $value) {
				$content['file_sk3'] = str_replace(' ', '_', $value['file_sk3']);
				//    echo '<pre>';
				//    print_r($content);die;
				$lampiran_file = $this->load->view('cetak/lampiran_file_pengawas', $content, true);
				$mpdf->AddPage();
				$mpdf->WriteHTML($lampiran_file);
			}
		}
		$mpdf->Output('WPEksternall - ' . date('Y-m-d') . '.pdf', 'I');
	}

	public function getDataSpvGi($nama_spv)
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai pg',
			'field' => array('pg.*'),
			'where' => "pg.deleted = 0 and pg.nama = '" . $nama_spv . "'"
		));

		$result = array();
		if (!empty($data)) {
			$data = $data->row_array();
			$result = $data;
		}


		return $result;
	}

	public function getDataPengawasK3($nama_spv)
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai pg',
			'field' => array('pg.*'),
			'where' => "pg.deleted = 0 and pg.nama = '" . $nama_spv . "'"
		));

		$result = array();
		if (!empty($data)) {
			$data = $data->row_array();
			$result = $data;
		}


		return $result;
	}

	public function cetak_dp3($id)
	{
		$pdf = Modules::run('mpdf/getInitPdf');

		$data_wp = $this->getDataDokumenWp($id);
		$data['spv_gi_data'] = $this->getDataSpvGi($data_wp['spv_gi']);
		$data['pengawas_k3_data'] = $this->getDataPengawasK3($data_wp['pengawas_k3']);
		$data['pengawas_pekerjaan_data'] = $this->getDataPengawasK3($data_wp['pengawas_pekerjaan']);
		//  echo '<pre>';
		//  print_r($data);die;
		$upt_id = $data_wp['upt_id'];
		$data_wp['paraf'] = $this->getListParafStrukturApproval($upt_id);
		// echo '<pre>';
		// print_r($data_wp);die;
		$data_wp['dp3'] = $this->getDokumenDp3Aktif();
		$data_wp['penanggung_jawab'] = $this->getDataPenanggungJawab($id);
		$data_wp['jsa'] = $this->getDataJsa($id);
		$data_wp['apd'] = $this->getDataApd($id);
		$data_wp['pelaksana'] = $this->getDataPelaksana($id);
		$data_wp['approval_dua'] = $this->getDataApprover($upt_id, 2);
		$data_wp['approval_last'] = $this->getDataApprover($upt_id, 3);
		//  echo '<pre>';
		//  print_r($data_wp);die;

		$data = $data_wp;

		if (!empty($data_wp['dp3'])) {
			$dokumen_dp3 = $data_wp['dp3'];
			$data['no_dokumen'] = $dokumen_dp3['no_dokumen'];
			$data['edisi'] = $dokumen_dp3['edisi'];
			$data['berlaku_efektif'] = $dokumen_dp3['berlaku_efektif'];
		}

		//'52811/DP3-INT/KI/UPT MALANG/12/2020'
		$data['no_dp3'] = $data_wp['id'] . '/DP3-' . substr(strtoupper($data_wp['tipe']), 0, 3) . '/' . $data['upt_tujuan'] . '/' . date('m/Y');
		//      
		//  echo '<pre>';
		//  print_r($data);die;
		$cover = $this->cover($data);
		$content = $this->content($data);
		$formulir_pemasangan = $this->load->view('form_pemasangan', $data, true);
		$pdf->WriteHTML($cover);
		$pdf->AddPage();
		$pdf->WriteHTML($content);
		$pdf->AddPage();
		$pdf->WriteHTML($formulir_pemasangan);
		$pdf->Output('dp3_' . $data_wp['no_wp'] . '.pdf', 'I');
	}

	public function cover($data)
	{
		$cover = '<html>
      <head>
        <title></title>    
        <style>
          #_wrapper{
            width: 100%;        
            margin: 0 auto;               
          }

          #_content{
            border: 1px solid #999;
            max-width: 100%;
            text-align: center;        
          }


          h2{
            margin: 0.5%;
          }      
        </style>
      </head>  
      <body>    
        <div id="_wrapper">
          <div id="_content">
          <div>&nbsp;</div>
            <div id="_top-content">
              <h2>FORMULIR</h2>
              <h2>PROSEDUR PELAKSANAAN PEKERJAAN</h2>
              <h2>PADA INSTALASI</h2>
              <h2>TEGANGAN TINGGI / EXTRA TINGGI</h2>
              <h2>(BUKU BIRU)</h2>
              <h2>' . strtoupper($data['tipe']) . '</h2>        
            </div>        
            <div>&nbsp;</div>
            <div>&nbsp;</div>          
            <div id="_cover">
              <img src="' . base_url() . 'files/img/smk3.png">
            </div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div id="_bottom-content">
              <h2>PT PLN (PERSERO)</h2>
              <h2>TRNSMISI JAWA BAGIAN TIMUR DAN BALI</h2>
            </div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>
            <div>&nbsp;</div>                    
          </div>
        </div>
      </body>
    </html>';
		return $cover;
	}

	public function content($data)
	{
		$content = $this->load->view('cetak/cetak_dp3', $data, true);
		return $content;
	}

	public function simpanPdfWp($id)
	{
		$mpdf = Modules::run('mpdf/getInitPdf');
		$data_wp = $this->getDataDokumenWp($id);
		$upt_id = $data_wp['upt_id'];
		$data_wp['paraf'] = $this->getListParafStrukturApproval($upt_id);
		$data_wp['dp3'] = $this->getDokumenDp3Aktif();
		$data_wp['penanggung_jawab'] = $this->getDataPenanggungJawab($id);
		$data_wp['jsa'] = $this->getDataJsa($id);
		$data_wp['apd'] = $this->getDataApd($id);
		$data_wp['pelaksana'] = $this->getDataPelaksana($id);
		$data_wp['ibppr'] = $this->getDataPermitIbbpr($id);
		$data_wp['approval_dua'] = $this->getDataApprover($upt_id, 2);
		$data_wp['approval_last'] = $this->getDataApprover($upt_id, 3);
		//  echo '<pre>';
		//  print_r($data_wp);
		//  die;
		$data['data_wp'] = $data_wp;
		//  echo '<pre>';
		//  print_r($data['data_wp']);die;
		$cover = $this->load->view('cetak/cover_ijin_kerja', $data, true);
		//echo $cover;die;
		$hal_1 = $this->load->view('cetak/dokumen_ijin_hal_satu', $data, true);
		// $hal_2 = $this->load->view('cetak/dokumen_ijin_hal_dua', $data, true);
		$hal_3 = $this->load->view('cetak/dokumen_ijin_hal_tiga', $data, true);
		$hal_4 = $this->load->view('cetak/dokumen_ijin_hal_empat', $data, true);
		$hal_5 = $this->load->view('cetak/dokumen_ijin_hal_lima', $data, true);
		$hal_6 = $this->load->view('cetak/dokumen_ijin_hal_enam', $data, true);
		$hal_ibpr = $this->load->view('cetak/hal_ibpr', $data, true);
		$mpdf->WriteHTML($cover);
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_1);
		// $mpdf->AddPage();
		// $mpdf->WriteHTML($hal_2);
		if (!empty($data_wp['jsa'])) {
			foreach ($data_wp['jsa'] as $value) {
				$data['jsa_data'] = $value;
				$hal_2 = $this->load->view('cetak/dokumen_ijin_hal_dua', $data, true);
				$mpdf->AddPage();
				$mpdf->WriteHTML($hal_2);
			}
		}
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_3);
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_4);
		$mpdf->AddPage();
		$mpdf->WriteHTML($hal_5);
		$mpdf->AddPage('L');
		$mpdf->WriteHTML($hal_6);
		$mpdf->AddPage('L');
		$mpdf->WriteHTML($hal_ibpr);
		$mpdf->AddPage();

		$content['file_sk3'] = str_replace(' ', '_', $data_wp['file_spk']);
		//      echo '<pre>';
		//      print_r($content);die;
		$lampiran_file = $this->load->view('cetak/lampiran_file_pengawas', $content, true);
		$mpdf->AddPage();
		$mpdf->WriteHTML($lampiran_file);
		if (!empty($data_wp['penanggung_jawab'])) {

			foreach ($data_wp['penanggung_jawab'] as $value) {
				$content['file_sk3'] = str_replace(' ', '_', $value['file_sk3']);
				//    echo '<pre>';
				//    print_r($content);die;
				$lampiran_file = $this->load->view('cetak/lampiran_file_pengawas', $content, true);
				$mpdf->AddPage();
				$mpdf->WriteHTML($lampiran_file);
			}
		}
		$mpdf->Output('files/berkas/dokumen_wp/WPEksternal - ' . $data_wp['no_wp'] . '.pdf', 'F');
	}

	public function chooseSingleLineDiagram()
	{
		$permit = $_POST['permit'];
		$data = Modules::run('database/get', array(
			'table' => 'single_line',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		$content['data'] = $result;
		$content['permit'] = $permit;
		echo $this->load->view('data_single_line', $content, true);
	}

	public function chooseSldDiagram()
	{
		$permit = $_POST['permit'];
		$data = Modules::run('database/get', array(
			'table' => 'sld',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		$content['data'] = $result;
		$content['permit'] = $permit;
		echo $this->load->view('data_sld', $content, true);
	}

	public function chooseHighRisk()
	{
		$permit = $_POST['permit'];
		$data = Modules::run('database/get', array(
			'table' => 'risk_management',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		$content['data'] = $result;
		$content['permit'] = $permit;
		echo $this->load->view('data_risk', $content, true);
	}

	public function showFile()
	{
		$file_str = str_replace(' ', '_', $this->input->post('file_str'));
		$data['file_str'] = $file_str;
		$data['jenis'] = $_POST['jenis'];
		echo $this->load->view('file_data', $data, true);
	}

	public function execChooseSingleLine()
	{
		$permit = $_POST['permit'];
		$single_line = $_POST['single_line'];

		$is_valid = false;
		$this->db->trans_begin();
		try {
			$post_single['permit'] = $permit;
			$post_single['single_line'] = $single_line;
			Modules::run('database/_insert', 'permit_single_line', $post_single);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function execChooseSld()
	{
		$permit = $_POST['permit'];
		$sld = $_POST['sld'];

		$is_valid = false;
		$this->db->trans_begin();
		try {
			$post_single['permit'] = $permit;
			$post_single['sld'] = $sld;
			Modules::run('database/_insert', 'permit_sld', $post_single);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function execChooseHigh()
	{
		$permit = $_POST['permit'];
		$risk = $_POST['risk'];

		$is_valid = false;
		$this->db->trans_begin();
		try {
			$post_single['permit'] = $permit;
			$post_single['risk_management'] = $risk;
			Modules::run('database/_insert', 'permit_risk', $post_single);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function logErrorUser()
	{
		$err = $_POST['err'];
		$module = $_POST['module'];
		$message = $_POST['message'];
		$user = $this->user;

		$is_valid = false;
		$this->db->trans_begin();
		try {
			$push['user'] = $user;
			$push['status'] = $err;
			$push['module'] = $module;
			$push['message'] = $message;
			Modules::run('database/_insert', 'log_error_user', $push);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}
}
