<?php

class Login extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getModuleName()
	{
		return 'login';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/controllers/login_v2.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Login";
		$data['title_content'] = 'Login';
		echo $this->load->view('index', $data, true);
	}

	public function getContactPerson()
	{
		$data = Modules::run('database/get', array(
			'table' => 'contact_person cp',
			'field' => array(
				'cp.*',
				'ut.id as upt_id',
				'ut.nama as nama_upt',
			),
			'join' => array(
				array('upt ut', 'cp.upt = ut.id', 'left'),
			),
			'where' => array(
				'cp.deleted' => 0
			)
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		$content['data'] = $result;
		echo $this->load->view('contact_person', $content, true);
	}

	public function getDataUserDb($username, $password)
	{
		$data = Modules::run('database/get', array(
			'table' => 'user u',
			'field' => array(
				'u.*', 'p.posisi as hak_akses',
				'p.id as pegawai_id',
				'ut.id as upt_id',
				'v.id as vendor_id',
				'sa.id as struktur_approval_id',
				'sa.level as level',
				'sa.is_last as last_approval',
				'sau.id as second_approval_internal',
			),
			'join' => array(
				array('pegawai p', 'u.pegawai = p.id', 'left'),
				array('upt ut', 'p.upt = ut.id', 'left'),
				array('vendor v', 'u.vendor = v.id', 'left'),
				array('struktur_approval sa', 'u.id = sa.user', 'left'),
				array('struktur_approval_has_sub_upt sau', 'sau.struktur_approval = sa.id', 'left'),
			),
			'where' => array(
				'u.username' => $username,
				'u.password' => $password,
				'u.deleted' => 0
			)
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			if ($result['vendor_id'] != '') {
				$result['hak_akses'] = "vendor";
			}

			if ($result['struktur_approval_id'] != '') {
				$result['hak_akses'] = "approval";
			} else {
				if ($result['vendor_id'] != '') {
					$result['hak_akses'] = "vendor";
				} else {
					$result['hak_akses'] = "pemohon";
				}
			}
		}


		return $result;
	}

	public function getDataPegawai($username, $password)
	{
		$data = Modules::run('database/get', array(
			'table' => 'user u',
			'field' => array('u.*', 'pv.priveledge as hak_akses'),
			'join' => array(
				array('priveledge pv', 'u.priveledge = pv.id'),
			),
			'where' => array(
				'u.username' => $username,
				'u.password' => $password,
				'u.is_active' => true
			)
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataUser($username, $password)
	{
		$data = $this->getDataUserDb($username, $password);
		$result = array();
		if (!empty($data)) {
			$result = $data;
		}
		return $result;
	}

	public function sign_in()
	{
		$this->cors();
		// echo '<pre>';
		// print_r($_POST);die;
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		//  echo '<pre>';
		//  print_r($_POST);
		//  die;
		$is_valid = false;
		try {
			$data = $this->getDataUser($username, $password);
			//   echo '<pre>';
			//   print_r($data);die;
			if (!empty($data)) {
				$is_valid = true;
			}
		} catch (Exception $exc) {
			$is_valid = false;
		}

		$hak_akses = '';
		if ($is_valid) {
			$this->setSessionData($data);
			$hak_akses = $data['hak_akses'];
		}
		echo json_encode(array('is_valid' => $is_valid, 'hak_akses' => $hak_akses));
	}

	public function setSessionData($data)
	{
		$session['user_id'] = $data['id'];
		$session['username'] = $data['username'];
		$session['hak_akses'] = $data['hak_akses'] == '' ? 'superadmin' : strtolower($data['hak_akses']);
		$session['pegawai_id'] = $data['pegawai_id'];
		$session['upt'] = $data['upt_id'];
		$session['last_approval'] = $data['last_approval'];
		$session['level'] = $data['level'];
		$session['user_created'] = $data['createddate'];
		$session['second_approval_internal'] = $data['second_approval_internal'];
		$session['player_id'] = $data['player_id'];
		$this->session->set_userdata($session);
	}

	public function sign_out()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function getDataUserDbViaMobile($username, $password)
	{
		$data = Modules::run('database/get', array(
			'table' => 'user u',
			'field' => array(
				'u.*', 'p.posisi as hak_akses',
				'p.id as pegawai_id',
				'ut.id as upt_id',
				'v.id as vendor_id',
				'sa.id as struktur_approval_id',
				'sa.level as level',
				'sa.is_last as last_approval',
				'sau.id as second_approval_internal',
				'u.username as email',
				'u.id as user'
			),
			'join' => array(
				array('pegawai p', 'u.pegawai = p.id', 'left'),
				array('upt ut', 'p.upt = ut.id', 'left'),
				array('vendor v', 'u.vendor = v.id', 'left'),
				array('struktur_approval sa', 'u.id = sa.user', 'left'),
				array('struktur_approval_has_sub_upt sau', 'sau.struktur_approval = sa.id', 'left'),
			),
			'where' => array(
				'u.username' => $username,
				'u.password' => $password,
				'u.deleted' => 0
			)
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			if ($result['vendor_id'] != '') {
				$result['hak_akses'] = "vendor";
			}

			if ($result['struktur_approval_id'] != '') {
				$result['hak_akses'] = "approval";
			} else {
				if ($result['vendor_id'] != '') {
					$result['hak_akses'] = "vendor";
				} else {
					$result['hak_akses'] = "pemohon";
				}
			}
		}


		return $result;
	}

	public function getDataUserViaMobile($username, $password)
	{
		$data = $this->getDataUserDbViaMobile($username, $password);
		$result = array();
		if (!empty($data)) {
			$result = $data;
		}
		return $result;
	}

	public function updatePlayerIdUser($params)
	{
		$push = array();
		$push['player_id'] = $params['player_id'];
		$push['updateddate'] = date('Y-m-d H:i:s');
		$this->db->update('user', $push, array('id' => $params['id']));
	}

	public function signIn()
	{
		$username = $_GET['username'];
		$password = $_GET['pass'];

		$is_valid = "0";
		$data = array();
		try {
			$data = $this->getDataUserViaMobile($username, $password);
			$data['player_id'] = $_GET['player_id'];
			//   echo '<pre>';
			//   print_r($data);die;
			if (!empty($data)) {
				$this->updatePlayerIdUser($data);
				$is_valid = "1";
			}
		} catch (Exception $exc) {
			$is_valid = "0";
		}

		$result['is_valid'] = $is_valid;
		$result['data'] = $data;
		$result['message'] = "Tidak ada pengguna ditemukan";
		if (!empty($data)) {
			$result['is_valid'] = $is_valid;
			$result['data'] = $data;
			$result['message'] = "Login Sukses";
		}
		echo json_encode($result);
	}

	public function cors()
	{

		// Allow from any origin
		if (isset($_SERVER['HTTP_ORIGIN'])) {
			// Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
			// you want to allow, and if so:
			header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
			header('Access-Control-Allow-Credentials: true');
			header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}

		// Access-Control headers are received during OPTIONS requests
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
				// may also be using PUT, PATCH, HEAD etc
				header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
				header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

			exit(0);
		}
	}
}
