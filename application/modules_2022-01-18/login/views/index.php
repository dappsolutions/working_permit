<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Working Permit | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/square/blue.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
</head>

<body class="hold-transition login-page" style="background: url('<?php echo base_url() . 'assets/images/tower_3.jpg' ?>') !important; background-size:cover !important;">
	<div class="" style="width: 100%; height: 100%;position: fixed;z-index: -1;top: 0; left: 0;opacity: .5;">

	</div>
	<br>
	<div class='row'>
		<div class='col-md-12 text-right' style="padding-right:100px;">
			<button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.pendaftaranUserSwa()">PENDAFTARAN USER SWA</button>
			&nbsp;
			<button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.pendaftaranUserSimson()">PENDAFTARAN USER SIMSON</button>
			&nbsp;
			<button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.monitoring()">MONITORING</button>
			&nbsp;
			<button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.contactPerson()">CONTACT PERSON</button>
		</div>
	</div>
	<div class="login-box">
		<div class="login-logo">
			<a href="<?php echo base_url() . 'login' ?>" style="color:orange;"><b>Working</b>Permit</a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Masuk Aplikasi</p>

			<form action="" method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control required" error="Username" placeholder="Username" id="username">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control required" error="Password" placeholder="Password" id="password">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						&nbsp;
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat" onclick="Login.sign_in(this, event)" style="background: #00a2b9">Masuk</button>
					</div>
					<!-- /.col -->
				</div>
			</form>
			<!-- /.social-auth-links -->
			<br />
			<!--<a href="#">Lupa Password</a><br>-->
			<a href="<?php echo base_url() . 'register' ?>" class="text-center">Registrasi</a>
			<br />
			<div class="text-right">
				<a href="<?php echo base_url() . 'files/WP_Online_manual_book.pdf' ?>" class="text-center">Buku Petunjuk</a>
			</div>
		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery 3 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
	<script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>
	<script src="<?php echo base_url() ?>assets/js/message.js"></script>
	<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/controllers/login_v2.js"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});
		});
	</script>
</body>

</html>
