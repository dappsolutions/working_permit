<?php if ($this->session->userdata('hak_akses') != 'approval' 
        && $this->session->userdata('hak_akses') != 'superadmin'
        && $this->session->userdata('hak_akses') != 'vendor' && $this->session->userdata('hak_akses') != '') { ?>
 <!-- /.search form -->
 <!-- sidebar menu: : style can be found in sidebar.less -->
 <ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Permohonan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'wpinternal' ?>"><i class="fa fa-file-text-o"></i> WP Internal</a></li>
    <!-- <li><a href="<?php echo base_url() . 'wpeksternal' ?>"><i class="fa fa-file-text-o"></i> WP Eksternal</a></li> -->
   </ul>
  </li>   

  <li class="treeview">
   <a href="#">
    <i class="fa fa-folder"></i>
    <span>Histori Permohonan</span>
    <span class="pull-right-container">
     <i class="fa fa-angle-left pull-right"></i>
    </span>
   </a>
   <ul class="treeview-menu">
    <li><a href="<?php echo base_url() . 'historyint' ?>"><i class="fa fa-file-text-o"></i> Internal</a></li>
    <!-- <li><a href="<?php echo base_url() . 'historyeks' ?>"><i class="fa fa-file-text-o"></i> Eksternal</a></li> -->
   </ul>
  </li>   

  <li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>   
 </ul>
<?php } ?>
