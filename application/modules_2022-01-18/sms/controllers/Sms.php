<?php

class Sms extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function send_sms($message = "", $to = "")
	{

		$panjang_no = strlen($to);
		$region = substr($to, 0, 2);
		// echo $region;die;
		$no_fix = $to;
		if ($region == "08") {
			$region = "628";
			$next_no = substr($to, 2, $panjang_no);
			$no_fix = $region . $next_no;
		} else {
			$no_fix = str_replace("+", "", $no_fix);
		}


		$quota = $this->cekQuotaSMSApi();

		//   $message = "opop";
		//   $to = "6285748233712";	

		if (intval($quota) > 0) {
			try {
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => array(
						'user' => 'dodik_api',
						'password' => 'aSRoY9M',
						'SMSText' => $message,
						'otp' => 'Y',
						'GSM' => $no_fix
					)
				));
				$resp = curl_exec($curl);
				//     echo '<pre>';
				//     echo $resp;die;
				// echo $resp;die;
				if ($resp == "") {
					return false;
					// echo 'gagal';
				} else {
					// echo 'sukses';
					return true;
				}
				curl_close($curl);
			} catch (Exception $ex) {
				redirect(base_url());
			}
		} else {
			//   echo 'kuota habis';
			return false;
		}
	}

	public function send_sms_data($message = "", $to = "")
	{

		$panjang_no = strlen($to);
		$region = substr($to, 0, 2);
		// echo $region;die;
		$no_fix = $to;
		if ($region == "08") {
			$region = "628";
			$next_no = substr($to, 2, $panjang_no);
			$no_fix = $region . $next_no;
		} else {
			$no_fix = str_replace("+", "", $no_fix);
		}


		// echo $no_fix;die;
		// $quota = $this->cekQuotaSMSApi();
		$quota = 1;
		// echo '<pre>';
		// print_r($quota);die;
		//   $message = "opop";
		// $no_fix = "6285748233712";	

		$result = array();
		if (intval($quota) > 0) {
			try {
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
					// CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendwa/plain',
					CURLOPT_POST => true,
					CURLOPT_POSTFIELDS => array(
						'user' => 'dodik_api',
						'password' => 'aSRoY9M',
						'SMSText' => $message,
						'GSM' => $no_fix,
						'otp' => 'Y',
						'output' => 'json'
					)
				));
				$resp = curl_exec($curl);
				$resp = json_decode($resp);

				$response = $resp->results;
				// echo $status;die;
				// echo '<pre>';
				// print_r($response);die;
				// echo $resp;die;
				if ($response[0]->status == "0") {
					$result['is_valid'] = 1;
					$result['message'] = "";
					// echo 'gagal';
				} else {
					$result['is_valid'] = 0;

					switch ($response[0]->status) {
						case '-5':
							$result['message'] = 'User dan Password SMS Gateway Tidak Valid';
							break;
						case '-13':
							$result['message'] = 'Missing Destination Number';
							break;

						default:
							# code...
							break;
					}
				}
				curl_close($curl);
			} catch (Exception $ex) {
				$result['is_valid'] = 0;
				// redirect(base_url());
			}
		} else {
			//   echo 'kuota habis';
			$result['is_valid'] = 0;
			$result['message'] = 'Kuota SMS Habis';
		}

		return $result;
	}


	public function simpanLogSms($status, $message, $no_hp)
	{
		$post['status'] = $status;
		$post['message'] = $message;
		$post['no_hp'] = $no_hp;
		$post['waktu_kirim'] = date('Y-m-d H:i:s');

		Modules::run("database/_insert", 'log_sms', $post);
	}

	public function cekQuotaSMS()
	{
		$url = "http://api.nusasms.com/api/command?user=dodik_api&password=aSRoY9M&cmd=CREDITS&output=json";
		$url_data = json_decode(file_get_contents($url));

		//   echo '<pre>';
		//   print_r($url_data);die;
		$status = 0;
		foreach ($url_data as $value) {
			$status = $value[0]->status;
		}

		return intval($status);
	}

	public function send_sms_data_tes($message = "", $to = "")
	{

		$panjang_no = strlen($to);
		$region = substr($to, 0, 2);
		// echo $region;die;
		$no_fix = $to;
		if ($region == "08") {
			$region = "628";
			$next_no = substr($to, 2, $panjang_no);
			$no_fix = $region . $next_no;
		} else {
			$no_fix = str_replace("+", "", $no_fix);
		}


		// echo $no_fix;die;
		// $quota = $this->cekQuotaSMSApi();
		// echo '<pre>';
		// print_r($quota);die;
		//   $message = "opop";
		// $no_fix = "6285748233712";	

		$result = array();
		// if (intval($quota) > 0) {
		try {
			$curl = curl_init();
			// echo '<pre>';
			// print_r($curl);die;
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
				// CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendwa/plain',
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => array(
					'user' => 'dodik_api',
					'password' => 'aSRoY9M',
					'SMSText' => $message,
					'GSM' => $no_fix,
					'output' => 'json'
				)
			));
			$resp = curl_exec($curl);

			echo '<pre>';
			print_r(curl_getinfo($curl));
			print_r(curl_error($curl));
			die;
			$resp = json_decode($resp);

			$response = $resp->results;
			// echo $status;die;
			echo '<pre>';
			print_r($response);
			die;
			// echo $resp;die;
			if ($response[0]->status == "0") {
				$result['is_valid'] = 1;
				$result['message'] = "";
				// echo 'gagal';
			} else {
				$result['is_valid'] = 0;

				switch ($response[0]->status) {
					case '-5':
						$result['message'] = 'User dan Password SMS Gateway Tidak Valid';
						break;
					case '-13':
						$result['message'] = 'Missing Destination Number';
						break;

					default:
						# code...
						break;
				}
			}
			curl_close($curl);
		} catch (Exception $ex) {
			$result['is_valid'] = 0;
			$result['message'] = $ex->getMessage();
			// redirect(base_url());
		}
		// } else {
		// 	//   echo 'kuota habis';
		// 	$result['is_valid'] = 0;
		// 	$result['message'] = 'Kuota SMS Habis';
		// }

		return $result;
	}

	public function cekQuotaSMSApi()
	{

		$url = "http://api.nusasms.com/api/command?user=dodik_api&password=aSRoY9M&cmd=CREDITS&output=json";



		// echo json_encode($fields);die;

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, true);


		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$result = curl_exec($ch);

		curl_close($ch);

		$result = json_decode($result);

		// echo '<pre>';
		// print_r($result);die;
		if (isset($result->value)) {
			return $result->value;
		}

		return 0;
	}

	public function send_next($message, $to)
	{
		//  try{
		//   $curl = curl_init();
		//  curl_setopt_array($curl, array(
		//  CURLOPT_RETURNTRANSFER => 1,
		//  CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
		//  CURLOPT_POST => true,
		//  CURLOPT_POSTFIELDS => array(
		//  'user' => 'dodik_api',
		//  'password' => 'aSRoY9M',
		//  'SMSText' => $message,
		//  'GSM' => $to
		//  )
		//  ));
		//  $resp = curl_exec($curl);
		//  if (!$resp) {
		//   return false;
		//  } else {
		//   return true;
		//  }
		//  curl_close($curl);
		//  } catch (Exception $ex) {
		//   redirect(base_url());
		//  }  
	}

	public function content_message_wp_rejected($code_reg, $keterangan)
	{
		$message = 'WP dengan Code Registrasi ' . $code_reg . ' telah ditolak dengan alasan ' . $keterangan;
		return $message;
	}

	public function content_message_register_user()
	{
		$message .= 'Telah ada pengguna registrasi baru masuk';
		return $message;
	}

	public function content_message_lupa_user($password)
	{
		$message .= 'Password anda yang terlupa adalah ' . $password;
		return $message;
	}

	public function content_message_validasi_user($nama_user, $user, $password)
	{
		$message = 'Hi ' . $nama_user;
		$message .= ' user anda telah divalidasi oleh Admin K3 ';
		$message .= 'anda sekarang mempunyai hak untuk '
			. 'mengajukan permohonan WP menggunakan ';
		$message .= 'Username : ' . $user;
		$message .= 'Password : ' . $password;
		return $message;
	}

	public function content_message_sms($no_wp, $code_reg, $struktural_approval_id_to_send, $is_pemohon, $location, $uraian_perkerjaan)
	{
		$message .= 'WP TRANS-JBTB ';
		$message .= 'Permohonan WP Masuk ';
		$message .= 'Silakan lakukan validasi permohonan pada link ini : ';
		$message .= base_url() . 'login/sign_out';

		if ($is_pemohon) {
			$message = 'WP Trans-JBTB ';
			$message .= 'Permohonan WP No ' . $no_wp . ' dg Reg ' . $code_reg . ' telah disetujui Manajer / DMK2K3, '
				. 'dan File WP terkirim ke Email Pemohon ';
			$message .= 'LOKASI : ' . $location . ', URAIAN PEKERJAAN : ' . $uraian_perkerjaan;
		}
		return $message;
	}

	public function content_message_gi($location, $uraian_perkerjaan, $tempat)
	{
		$message .= 'WP Trans-JBTB ';
		$message .= 'LOKASI : ' . $location . ', URAIAN PEKERJAAN : ' . $uraian_perkerjaan . ', PERUSAHAAN : ' . $tempat;
		return $message;
	}

	public function content_sms_from_pengajuan()
	{
		$message .= 'WP TRANS-JBTB ';
		$message .= 'Permohonan WP Masuk';
		$message .= 'Silakan lakukan validasi permohonan pada link ini : ';
		$message .= base_url() . 'login/sign_out';

		return $message;
	}

	public function content_message_sms_pemohon($data)
	{
		$message = 'WP Trans-JBTB ';
		$message .= 'Permohonan WP No ' . $data['no_wp'] . ' dg Reg ' . $data['no_pengajuan_vendor'] . ' telah disetujui Manajer / DMK2K3, '
			. 'dan File WP terkirim ke Email Pemohon ';
		$message .= 'LOKASI : ' . $data['nama_place'] . ' - ' . $data['lokasi_pekerjaan'] . ', URAIAN PEKERJAAN : ' . $data['uraian_pekerjaan'];
		$message .= ' Silakan mencetak Dokumen Working Permit melalui panel user anda dengan no wp ' . $data['no_wp'];
		return $message;
	}

	public function content_message_sms_pemohon_rejected($data)
	{
		$message = 'WP Trans-JBTB ';
		$message .= 'Permohonan WP No ' . $data['no_wp'] . ' dg Reg ' . $data['no_pengajuan_vendor'] . ' telah disetujui Manajer / DMK2K3, '
			. 'dan File WP terkirim ke Email Pemohon ';
		$message .= 'LOKASI : ' . $data['nama_place'] . ' - ' . $data['lokasi_pekerjaan'] . ', URAIAN PEKERJAAN : ' . $data['uraian_pekerjaan'] . ' - DITOLAK, Mohon cek aplikasi WP';
		return $message;
	}

	public function content_message_perbaharui_user($data_vendor)
	{
		$message = '<p>Hi ' . $data_vendor["nama_perusahaan"] . '</p>';
		$message .= '<p>user anda telah diperbaharui oleh Admin K3</p>';
		$message .= '<p>user anda sekarang</p>';
		$message .= '<p>Username : ' . $data_vendor['username'] . '</p>';
		$message .= '<p>Password : ' . $data_vendor['password'] . '</p>';
		return $message;
	}
}
