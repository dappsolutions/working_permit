<input type='hidden' name='' id='pengajuan_id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <!--   <div class="box box-solid box-primary">
      <div class="box-header ui-sortable-handle" style="cursor: move;">
  <?php echo strtoupper("Buat User") ?>
      </div>
     </div>-->
  <!-- /.box-header -->
  <!-- form start -->
  <form class="form-horizontal" method="post">
   <div class="box-body">
    <div class="form-group">
     <label for="" class="col-sm-2 control-label">Username</label>

     <div class="col-sm-6">
      <input type="text" class="form-control required" 
             error="Username" id="username" 
             placeholder="Username"
             value="">
     </div>
    </div>
    <div class="form-group">
     <label for="" class="col-sm-2 control-label">Password</label>

     <div class="col-sm-6">
      <input type="text" class="form-control required" 
             error="Password" id="password" 
             placeholder="Password"
             value="">
     </div>
    </div>
   </div>
   <!-- /.box-body -->
   <div class="box-footer">
    <button type="button" class="btn btn-default" onclick="message.closeDialog()">Cancel</button>
    <button type="submit" class="btn btn-success pull-right" onclick="Registrasi.createUser('<?php echo isset($id) ? $id : '' ?>', event)">
     <i class="fa fa-check"></i>&nbsp;Proses</button>
   </div>
   <!-- /.box-footer -->
  </form>
  <!-- /.box -->
 </div>
</div>
