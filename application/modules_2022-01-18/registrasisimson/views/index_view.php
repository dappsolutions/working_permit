<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box padding-16">
					<!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="row">
							<div class="col-md-12">
								<div class="input-group">
									<input type="text" class="form-control" onkeyup="RegistrasiSimson.search(this, event)" id="keyword" placeholder="Pencarian">
									<span class="input-group-addon"><i class="fa fa-search"></i></span>
								</div>
							</div>
						</div>
						<div class="divider"></div>
						<br />
						<?php if (isset($keyword)) { ?>
							<br />
							<div class="row">
								<div class="col-md-6">
									Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
								</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered" id="tb_content">
										<thead>
											<tr class="bg-primary-light text-white">
												<th>No</th>
												<th>Nama Pegawai</th>
												<th>Email</th>
												<th>No HP</th>
												<th>Tujuan Unit</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php if (!empty($content)) { ?>
												<?php $no = $pagination['last_no'] + 1; ?>
												<?php foreach ($content as $value) { ?>
													<?php $tr_color = "bg-warning"; ?>
													<?php $status = "PENGAJUAN" ?>
													<?php if ($value['approveby'] != '') { ?>
														<?php $tr_color = "bg-success"; ?>
														<?php $status = "DIACC" ?>
													<?php } ?>
													<?php if ($value['rejectedby'] != '') { ?>
														<?php $tr_color = "bg-danger"; ?>
														<?php $status = "DITOLAK" ?>
													<?php } ?>
													<tr class="<?php echo $tr_color ?>">
														<td><?php echo $no++ ?></td>
														<td><?php echo $value['nama'] ?></td>
														<td><?php echo $value['email'] ?></td>
														<td><?php echo $value['no_hp'] ?></td>
														<td><?php echo $value['nama_upt'] ?></td>
														<td><?php echo $status ?></td>
														<td class="text-center">
															<i class="fa fa-file-text grey-text  hover" data-toggle="tooltip" title="Detail Registrasi" onclick="RegistrasiSimson.detail('<?php echo $value['id'] ?>')"></i>
														</td>
													</tr>
												<?php } ?>
											<?php } else { ?>
												<tr>
													<td colspan="10" class="text-center">Tidak ada data ditemukan</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<?php echo $pagination['links'] ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
