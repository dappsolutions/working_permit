<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="row">
	<div class="col-md-12">
		<!-- Horizontal Form -->
		<div class="box box-info padding-16">
			<!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
			<!-- /.box-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post">
				<div class="box-body">
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Nama Pegawai</label>

						<div class="col-sm-4">
							<input type="text" disabled="" class="form-control required" error="Nama Pegawai" id="nama" placeholder="Nama Pegawai" value="<?php echo $nama ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Email</label>

						<div class="col-sm-4">
							<input type="text" disabled="" class="form-control required" error="Email" id="email" placeholder="Email" value="<?php echo $email ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">No Hp</label>

						<div class="col-sm-4">
							<input type="text" disabled="" class="form-control required" error="No HP" id="no_hp" placeholder="No HP" value="<?php echo $no_hp ?>">
						</div>
					</div>
					<?php $status = "PENGAJUAN" ?>
					<?php if ($approveby != '') { ?>
						<?php $status = "DIACC" ?>
					<?php } ?>
					<?php if ($rejectedby != '') { ?>
						<?php $status = "DITOLAK" ?>
					<?php } ?>
					<div class="form-group">
						<label for="" class="col-sm-2 control-label">Status</label>

						<div class="col-sm-4">
							<input type="text" disabled="" class="form-control required" error="Status" id="status" placeholder="Status" value="<?php echo $status ?>">
						</div>
					</div>
					<?php if ($rejectedby != '') { ?>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">Status</label>

							<div class="col-sm-4">
								<textarea class="form-control" disabled=""><?php echo $keterangan_reject ?></textarea>
							</div>
						</div>
					<?php } ?>

					<hr />
					<?php if ($username != '') { ?>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">Username</label>

							<div class="col-sm-4">
								<input type="text" disabled="" class="form-control required" error="Username" id="username" placeholder="Username" value="<?php echo $username ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="" class="col-sm-2 control-label">Password</label>

							<div class="col-sm-4">
								<input type="text" disabled="" class="form-control required" error="Password" id="password" placeholder="Password" value="<?php echo $password ?>">
							</div>
						</div>
					<?php } ?>
				</div>
				<!-- /.box-body -->
				<!--<div class="box-footer">-->
				<div class="row">
					<div class="col-md-12 text-right">
						<button type="button" class="btn btn-default" onclick="RegistrasiSimson.back()">Cancel</button>
						&nbsp;
						<button type="submit" class="btn btn-success" onclick="RegistrasiSimson.createUser('<?php echo isset($id) ? $id : '' ?>', event)">
							<i class="fa fa-check"></i>&nbsp;Approve
						</button>
						<?php if ($approveby == '' && $rejectedby == '') { ?>
							<button type="submit" class="btn btn-success" onclick="RegistrasiSimson.createUser('<?php echo isset($id) ? $id : '' ?>', event)">
								<i class="fa fa-check"></i>&nbsp;Approve
							</button>
							<button style="margin-left: 8px;" type="submit" class="btn btn-danger" onclick="RegistrasiSimson.reject('<?php echo isset($id) ? $id : '' ?>', event)">
								<i class="fa fa-check"></i>&nbsp;Reject
							</button>
						<?php } ?>
					</div>
				</div>
				<!--</div>-->
				<!-- /.box-footer -->
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
