<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary padding-16">
					<div class="box-header with-border">
						<i class="fa fa-bar-chart-o"></i>

						<h3 class="box-title">Grafik WP</h3>
						<br />

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="col-md-3">
								<select class="form-control col-sm-3" error="Tahun" id="tahun" onchange="Grafik.changeYear(this)">
									<?php if (!empty($list_year)) { ?>
										<?php foreach ($list_year as $value) { ?>
											<?php $selected = $value == $date ? 'selected' : '' ?>
											<option <?php echo $selected ?> value="<?php echo $value ?>"><?php echo $value ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<div id="bar-chart" style="height: 400px;"></div>
							</div>
						</div>
						<br />
						<br />
						<br />
					</div>
					<!-- /.box-body-->
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" value='<?php echo $data_wp ?>' id="data_wp" class="form-control" />


<!-- <style>
	.morris-hover {
		position: absolute;
		z-index: 1000;
	}
</style> -->
