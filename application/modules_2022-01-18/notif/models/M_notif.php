
<?php

class M_notif extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getData($params = array(), $limit = true)
	{

		$filter_limit = "LIMIT 30";

		$sql = " select id, status, content, `to`, data, createddate 
		from log_fcm where `to` = '" . $params['user_id'] . "' 
		order by id DESC 
		" . $filter_limit;

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}
}
