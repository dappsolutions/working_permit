<?php if (!empty($content)) { ?>
 <?php $no = 1; ?>
 <?php foreach ($content as $value) { ?>
  <?php $bg_color = ''; ?>
  <?php if ($value['status'] == 'DRAFT') { ?>
   <?php $bg_color = 'bg-warning'; ?>
  <?php } ?>
  <?php if ($value['status'] == 'REJECTED') { ?>
   <?php $bg_color = 'bg-danger'; ?>
  <?php } ?>
  <?php if ($value['status'] == 'APPROVED') { ?>
   <?php $bg_color = 'bg-success'; ?>
  <?php } ?>
  <tr class="<?php echo $bg_color ?>">
   <td><b><?php echo $no++ ?></b></td>
   <td><b><?php echo $value['no_wp'] ?></b></td>
   <td><b><?php echo $value['tanggal_wp'] ?></b></td>
   <td><b><?php echo $value['tgl_pekerjaan'] ?></b></td>
   <td><b><?php echo $value['tgl_awal'] ?></b></td>
   <td><b><?php echo $value['tgl_akhir'] ?></b></td>
   <td><b><?php echo $value['jenis_place'] ?></b></td>
   <td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
   <td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
   <td><b><?php echo $value['nama_upt'] ?></b></td>
   <?php if ($value['status'] == 'DRAFT') { ?>
    <td><b><?php echo $value['status'] ?></b></td>
   <?php } else { ?>              
    <td><b><?php echo $value['status'] ?></b><br/>[Level : <?php echo $value['level'] ?>]</td>
   <?php } ?>  
  </tr>
 <?php } ?>
<?php } else { ?>
 <tr>
  <td colspan="20" class="text-center">Tidak ada data ditemukan</td>
 </tr>
<?php } ?>  