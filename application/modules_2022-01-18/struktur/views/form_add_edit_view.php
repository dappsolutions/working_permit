<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Upt</label>
      <div class="col-sm-4">
       <select class="form-control required" error="Upt" id="upt" 
               style="width: 100%;" onchange="Struktur.getPegawai(this)">
        <option value="">Pilih Upt</option>
        <?php if (!empty($list_upt)) { ?>
         <?php foreach ($list_upt as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($upt)) { ?>
           <?php $selected = $upt == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Pegawai</label>
      <div class="col-sm-4" id="select_pegawai">
       <select class="form-control required" error="Pegawai" id="pegawai" style="width: 100%;">
        <option upt="" value="">Pilih Pegawai</option>
        <?php if (!empty($list_pegawai)) { ?>
         <?php foreach ($list_pegawai as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($user)) { ?>
           <?php $selected = $user == $value['user_id'] ? 'selected' : '' ?>
          <?php } ?>
          <option upt="<?php echo $value['upt'] ?>" <?php echo $selected ?> value="<?php echo $value['user_id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Tipe Permit</label>
      <div class="col-sm-4">
       <select class="form-control required" error="Tipe" id="tipe" style="width: 100%;">
        <option value="">Pilih Permit</option>
        <?php if (!empty($list_tipe)) { ?>
         <?php foreach ($list_tipe as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($tipe_permit)) { ?>
           <?php $selected = $tipe_permit == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tipe'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>


     <?php $hidden = ""; ?>
     <?php $hidden = isset($file) ? 'hidden' : '' ?>
     <div class="form-group <?php echo $hidden ?>" id="file_input">
      <label for="" class="col-sm-2 control-label">Paraf</label>
      <div class="col-sm-4">
       <input type="file" id="file" class="form-control" onchange="Struktur.checkFile(this)">
      </div>      
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($file) ? '' : 'hidden' ?>
     <div class="form-group <?php echo $hidden ?>" id="detail_file">
      <label for="" class="col-sm-2 control-label">File Paraf</label>
      <div class="col-sm-4">
       <div class="input-group">
        <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-image hover-content" file="<?php echo isset($file) ? $file : '' ?>" 
            onclick="Struktur.showLogo(this, event, 'paraf')"></i>         
        </span>
        <span class="input-group-addon">
         <i class="fa fa-close hover-content"
            onclick="Struktur.gantiFile(this, event, 'paraf')"></i>
        </span>
       </div>
      </div>      
     </div>
     
      <?php $hidden = ""; ?>
     <?php $hidden = isset($ttd) ? 'hidden' : '' ?>
     <div class="form-group <?php echo $hidden ?>" id="file_ttd_input">
      <label for="" class="col-sm-2 control-label">TTD</label>
      <div class="col-sm-4">
       <input type="file" id="file_ttd" class="form-control" onchange="Struktur.checkFile(this)">
      </div>      
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($ttd) ? '' : 'hidden' ?>
     <div class="form-group <?php echo $hidden ?>" id="detail_file_ttd">
      <label for="" class="col-sm-2 control-label">File TTD</label>
      <div class="col-sm-4">
       <div class="input-group">
        <input disabled type="text" id="file_str_ttd" class="form-control" value="<?php echo isset($ttd) ? $ttd : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-image hover-content" file="<?php echo isset($ttd) ? $ttd : '' ?>" 
            onclick="Struktur.showLogo(this, event, 'ttd')"></i>         
        </span>
        <span class="input-group-addon">
         <i class="fa fa-close hover-content"
            onclick="Struktur.gantiFile(this, event, 'ttd')"></i>
        </span>
       </div>
      </div>      
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Level</label>
      <div class="col-sm-2">
       <input type="number" class="form-control required" 
              error="Level" id="level" 
              placeholder="Level"
              value="<?php echo isset($level) ? $level : '' ?>">
      </div>
      <div class="col-sm-2">
       <div class="checkbox">
        <label>
         <?php $checked = isset($is_last) ? $is_last == 1 ? 'checked' : '' : '' ?>
         <input  data-toggle="tooltip" title="Check Untuk Struktur Terakhir" <?php echo $checked ?> type="checkbox" id="last"> Last
        </label>
       </div>
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Struktur.back()">Cancel</button>
     <button type="submit" class="btn btn-success pull-right" onclick="Struktur.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
