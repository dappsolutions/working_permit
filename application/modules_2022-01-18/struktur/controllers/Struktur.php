<?php

class Struktur extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'struktur';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/struktur.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'struktur_approval';
 }

 public function getRootModule() {
  return "Struktur Approval";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Struktur";
  $data['title_content'] = 'Struktur';
  $content = $this->getDataStruktur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataStruktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
       array('s.level', $keyword),
       array('u.nama', $keyword),
       array('tp.tipe', $keyword),
   );
  }

  $where = "s.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "s.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' s',
                'field' => array('s.*', 'p.nama', 'p.nip',
                    'u.nama as nama_upt', 'tp.tipe'),
                'join' => array(
                    array('user us', 's.user = us.id'),
                    array('pegawai p', 'p.id = us.pegawai'),
                    array('upt u', 'u.id = s.upt'),
                    array('tipe_permit tp', 'tp.id = s.tipe_permit'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where,
                'orderby' => 'u.nama, s.level'
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' s',
                'field' => array('s.*', 'p.nama', 'p.nip',
                    'u.nama as nama_upt', 'tp.tipe'),
                'join' => array(
                    array('user us', 's.user = us.id'),
                    array('pegawai p', 'p.id = us.pegawai'),
                    array('upt u', 'u.id = s.upt'),
                    array('tipe_permit tp', 'tp.id = s.tipe_permit'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where,
                'orderby' => 'u.nama, s.level'
    ));
    break;
  }

  return $total;
 }

 public function getDataStruktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.nip', $keyword),
       array('s.level', $keyword),
       array('u.nama', $keyword),
       array('tp.tipe', $keyword),
   );
  }

  $where = "s.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "s.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' s',
                'field' => array('s.*', 'p.nama', 'p.nip',
                    'u.nama as nama_upt', 'tp.tipe'),
                'join' => array(
                    array('user us', 's.user = us.id'),
                    array('pegawai p', 'p.id = us.pegawai'),
                    array('upt u', 'u.id = s.upt'),
                    array('tipe_permit tp', 'tp.id = s.tipe_permit'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where,
                'orderby' => 'u.nama, tp.id, s.level'
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' s',
                'field' => array('s.*', 'p.nama', 'p.nip',
                    'u.nama as nama_upt', 'tp.tipe'),
                'join' => array(
                    array('user us', 's.user = us.id'),
                    array('pegawai p', 'p.id = us.pegawai'),
                    array('upt u', 'u.id = s.upt'),
                    array('tipe_permit tp', 'tp.id = s.tipe_permit'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where,
                'orderby' => 'u.nama, tp.id, s.level'
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataStruktur($keyword)
  );
 }

 public function getDetailDataStruktur($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'sp.file'),
              'join' => array(
                  array('struktur_approval_paraf sp', 't.id = sp.struktur_approval')
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListPegawai($upt = "") {
  $where = "p.deleted = 0";
  if ($upt != "") {
   $where = "p.upt = '" . $upt . "'";
  }
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*', 'u.id as user_id'),
              'join' => array(
                  array('user u', 'p.id = u.pegawai')
              ),
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTipePermit() {
  $data = Modules::run('database/get', array(
              'table' => 'tipe_permit',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Struktur";
  $data['title_content'] = 'Tambah Struktur';
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tipe'] = $this->getListTipePermit();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataStruktur($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Struktur";
  $data['title_content'] = 'Ubah Struktur';
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tipe'] = $this->getListTipePermit();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataStruktur($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Struktur";
  $data['title_content'] = "Detail Struktur";
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_upt'] = $this->getListUpt();
  $data['list_tipe'] = $this->getListTipePermit();
  echo Modules::run('template', $data);
 }

 public function getPostDataStruktur($value) {
  $data['user'] = $value->user;
  $data['upt'] = $value->upt;
  $data['level'] = $value->level;
  $data['tipe_permit'] = $value->tipe;
  $data['is_last'] = $value->last;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $file = $_FILES;
  $is_valid = false;
  $is_save = true;
  $message = "";

//  echo '<pre>';
//  print_r($file);die;
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataStruktur($data->form);
   if ($id == '') {

    if (!empty($file)) {
     if (isset($file['file_ttd'])) {
      $response_upload = $this->uploadData('file_ttd', 'ttd');
      if ($response_upload['is_valid']) {
       $post['ttd'] = $file['file_ttd']['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }

    $id = Modules::run('database/_insert', $this->getTableName(), $post);

    $post_paraf['struktur_approval'] = $id;
    if (!empty($file)) {
     if (isset($file['file'])) {
      $response_upload = $this->uploadData('file', 'paraf');
      if ($response_upload['is_valid']) {
       $post_paraf['file'] = $file['file']['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }

    if ($is_save) {
     Modules::run('database/_insert', 'struktur_approval_paraf', $post_paraf);
    }
   } else {
    //update   
    if (!empty($file)) {
     if (isset($file['file_ttd'])) {
      if ($data->form->file_str_ttd != $file['file_ttd']['name']) {
       $response_upload = $this->uploadData('file_ttd', 'ttd');
       if ($response_upload['is_valid']) {
        $post['ttd'] = $file['file_ttd']['name'];
       } else {
        $is_save = false;
        $message = $response_upload['response'];
       }
      }
     }
    }
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

    $post_paraf['struktur_approval'] = $id;
    if (!empty($file)) {
     if (isset($file['file'])) {
      if ($data->form->file_str != $file['file']['name']) {
       $response_upload = $this->uploadData('file', 'paraf');
       if ($response_upload['is_valid']) {
        $post_paraf['file'] = $file['file']['name'];
       } else {
        $is_save = false;
        $message = $response_upload['response'];
       }
      }
     }
    }

    Modules::run('database/_update', 'struktur_approval_paraf', $post_paraf,
            array('struktur_approval' => $id));
   }
   $this->db->trans_commit();
   if ($is_save) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Struktur";
  $data['title_content'] = 'Data Struktur';
  $content = $this->getDataStruktur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/'.$keyword.'/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field, $dir) {
  $config['upload_path'] = 'files/berkas/' . $dir . '/';
  $config['allowed_types'] = 'png|jpg|jpeg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);

  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  $data['dir'] = $_POST['dir'];
  echo $this->load->view('foto', $data, true);
 }

 public function getPegawai() {
  $upt = $_POST['upt'];
  $data['list_pegawai'] = $this->getListPegawai($upt);
  echo $this->load->view('list_pegawai', $data, true);
 }

}
