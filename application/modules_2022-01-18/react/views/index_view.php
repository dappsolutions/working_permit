<div class="row">
 <div class="col-md-12">
  <div class="panel panel-default">
   <div class="panel-heading">
    <h5>React</h5>
   </div>
   <div class="panel-body">
    <div id="app">
     <script type="text/babel">
      class Hello extends React.Component{
         constructor(){
          super();
          this.state = {
           title: "Belajar",
           subtitle: "Belajar React"
          };          
         }

         changeTitle = () => {
          this.setState({
           title: "Belajar Kembali"
          });
         }

         render(){
          return <div class="row">
           <div class="col-md-12">
            <h5>{this.state.title}</h5>
            <h5>{this.state.subtitle}</h5>
            <button onClick={this.changeTitle} class="btn btn-primary">UBAH JUDUL</button>
           </div>
          </div>
         }
        }

        ReactDOM.render(<Hello/>, document.getElementById('app'));
      </script>
    </div>

    <br>
    <div id="message">
     <script type="text/babel">
      class Message extends React.Component{
          render(){
           return <div class="row">
           <div class="col-md-12">
            <p>{this.props.nama}</p>
           </div>
           </div>
          }
         }
         

         ReactDOM.render(<Message nama="Dodik Rismawan Affrudin"/>, document.getElementById('message'));
        </script>
    </div>

    <br>
    <div id="form">
     <script type="text/babel">
       class Form extends React.Component{
        constructor(){
         super();
         this.state = {
          nama : ""
         };
        }

        setName = (e)=>{
          this.setState({
           nama : e.target.value
          });
        }

        save = ()=>{
         if(this.state.nama == ''){
           toastr.error('Nama Harus Diisi');
         }else{
          alert(this.state.nama);
         }
        }

        render(){
         return <div class="row">
         <div class="col-md-12">
          <input type="text" class="form-control required" id="nama" onChange={this.setName}/>
         </div>
         <div class="col-md-12">
          <br/>          
          <button class="btn btn-success" onClick={this.save}>SIMPAN</button>
         </div>
         </div>
        }
       }

       ReactDOM.render(<Form/>, document.getElementById('form'));
      </script>
    </div>

   </div>
  </div>
 </div>
</div>