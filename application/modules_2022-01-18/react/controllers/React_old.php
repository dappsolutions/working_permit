=<?php

 if (!defined('BASEPATH'))
  exit('No direct script access allowed');

 class React extends MX_Controller
 {

  public function __construct()
  {
   parent::__construct();
  }

  public function getModuleName()
  {
   return 'react';
  }

  public function getHeaderJSandCSS()
  {
   $data = array(
    '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
    '<script src="' . base_url() . 'assets/react/lib/react.production.min.js"></script>',
    '<script src="' . base_url() . 'assets/react/lib/react-dom.production.min.js"></script>',
    '<script src="' . base_url() . 'assets/react/lib/babel.min.js"></script>',
   );

   return $data;
  }

  public function getTableName()
  {
   return 'react';
  }

  public function getRootModule()
  {
   return "React";
  }

  public function index()
  {
   $this->segment = 3;
   $this->page = $this->uri->segment($this->segment) ?
    $this->uri->segment($this->segment) - 1 : 0;
   $this->last_no = $this->page * $this->limit;

   $data['view_file'] = 'index_view';
   $data['header_data'] = $this->getHeaderJSandCSS();
   $data['module'] = $this->getModuleName();
   $data['title'] = $this->getRootModule() . " - React";
   $data['title_content'] = 'React';
   $data['data'] = array();
   echo Modules::run('template', $data);
  }
 }
