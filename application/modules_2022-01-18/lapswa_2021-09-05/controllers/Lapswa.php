<?php

class Lapswa extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;
	public $akses;
	public $user;
	public $level;
	public $upt;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 1;
		$this->akses = $this->session->userdata('hak_akses');
		$this->level = $this->session->userdata('level');
		$this->user = $this->session->userdata('user_id');
		$this->upt = $this->session->userdata('upt');
	}

	public function getModuleName()
	{
		return 'lapswa';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/wpSWA_v1-5.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/lapswa.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'permit';
	}

	public function getRootModule()
	{
		return "Laporan";
	}

	public function index()
	{

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = $this->getRootModule() . " - SWA";
		$data['title_content'] = 'SWA';
		$content = $this->getDataLapinternal();

		$data['content'] = $content['data'];
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['upt'] = $this->upt;
		echo Modules::run('template', $data);
	}

	public function tampilkan()
	{
		list($tgl_awal, $tgl_akhir) = explode('-', $_POST['tanggal']);
		$tgl_awal = date('Y-m-d', strtotime(trim($tgl_awal)));
		$tgl_akhir = date('Y-m-d', strtotime(trim($tgl_akhir)));

		$data_laporan = $this->getDataLapinternalSearch($tgl_awal, $tgl_akhir);
		//   echo '<pre>';
		//   print_r($data_laporan);die;
		$data['content'] = $data_laporan['data'];
		echo $this->load->view('table_laporan', $data, true);
	}

	public function getDataLapinternal($keyword = '', $is_upt = false)
	{

		$where_upt = $is_upt == true ? "and pp.upt = '" . $keyword . "'" : "";
		$where = "p.deleted = 0 and pst.status != 'DRAFT' " . $where_upt;
		switch ($this->akses) {
			case "approval":
				$upt = $_SESSION['upt'];
				$where = "p.deleted = 0 "
					. "and pp.upt = '" . $upt . "' and pst.status != 'DRAFT'";

				if ($this->level == "1" && $this->upt == "2") {
					$where = "p.deleted = 0 and pst.status != 'DRAFT' " . $where_upt;
				}
				break;
			default:
				$where = "p.deleted = 0 "
					. "and ps.user = '" . $this->user . "' and pst.status != 'DRAFT' " . $where_upt;
				break;
		}

		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array(
				'p.*', 'ph.email',
				'ph.nama_pemohon', 'ph.perusahaan',
				'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
				'ph.alamat', 'ph.jabatan',
				'pw.tgl_pekerjaan', 'pw.tgl_awal',
				'pw.tgl_akhir', 'pw.work_place',
				'pw.place', 'pw.lokasi_pekerjaan',
				'pw.uraian_pekerjaan',
				'wp.jenis as jenis_place',
				'pst.status', 'ut.nama as nama_upt', 'pst.level',
				'ptl_swa.no_trans as no_trans_swa'
			),
			'join' => array(
				array('pemohon ph', 'ph.id = p.pemohon'),
				array('permit_work pw', 'pw.permit = p.id'),
				array('work_place wp', 'pw.work_place = wp.id'),
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array('upt ut', 'ut.id = pp.upt'),
				array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit'),
				array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id'),
			),
			'where' => $where,
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
		);
	}

	public function getDataLapinternalSearch($tgl_awal, $tgl_akhir)
	{

		$where_tgl = "and (p.tanggal_wp >= '" . $tgl_awal . "' and p.tanggal_wp <= '" . $tgl_akhir . "')";
		$where = "p.deleted = 0 and pst.status != 'DRAFT' " . $where_tgl;
		switch ($this->akses) {
			case "approval":
				$upt = $_SESSION['upt'];
				$where = "p.deleted = 0 "
					. "and pp.upt = '" . $upt . "' and pst.status != 'DRAFT' " . $where_tgl;

				if ($this->level == "1" && $this->upt == "2") {
					$where = "p.deleted = 0 and pst.status != 'DRAFT' " . $where_tgl;
				}
				break;
			default:
				$where = "p.deleted = 0 "
					. "and ps.user = '" . $this->user . "' and pst.status != 'DRAFT' " . $where_tgl;
				break;
		}

		//  echo $where;die;
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array(
				'p.*', 'ph.email',
				'ph.nama_pemohon', 'ph.perusahaan',
				'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
				'ph.alamat', 'ph.jabatan',
				'pw.tgl_pekerjaan', 'pw.tgl_awal',
				'pw.tgl_akhir', 'pw.work_place',
				'pw.place', 'pw.lokasi_pekerjaan',
				'pw.uraian_pekerjaan',
				'wp.jenis as jenis_place',
				'pst.status', 'ut.nama as nama_upt', 'pst.level',
				'ptl_swa.no_trans as no_trans_swa'
			),
			'join' => array(
				array('pemohon ph', 'ph.id = p.pemohon'),
				array('permit_work pw', 'pw.permit = p.id'),
				array('work_place wp', 'pw.work_place = wp.id'),
				array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
				array('permit_status ps', 'pss.id = ps.id'),
				array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
				array('permit_status pst', 'pss_s.id = pst.id'),
				array('permit_purpose pp', 'p.id = pp.permit'),
				array('upt ut', 'ut.id = pp.upt'),
				array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit'),
				array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id'),
			),
			'where' => $where
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
		);
	}
}
