<?php

class Gardu_induk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'gardu_induk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/gardu_induk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'gardu_induk';
 }

 public function getRootModule() {
  return "Master";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Gardu induk";
  $data['title_content'] = 'Gardu induk';
  $content = $this->getDataGardu_induk();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataGardu_induk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('su.nama_gardu', $keyword),
       array('u.nama', $keyword),
   );
  }

  $where = "su.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "su.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*', 'u.nama as nama_upt'),
                'join' => array(
                    array('upt u', 'su.upt = u.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*', 'u.nama as nama_upt'),
                'join' => array(
                    array('upt u', 'su.upt = u.id')
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataGardu_induk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('su.nama_gardu', $keyword),
       array('u.nama', $keyword),
   );
  }

  $where = "su.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "su.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*', 'u.nama as nama_upt'),
                'join' => array(
                    array('upt u', 'su.upt = u.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' su',
                'field' => array('su.*', 'u.nama as nama_upt'),
                'join' => array(
                    array('upt u', 'su.upt = u.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataGardu_induk($keyword)
  );
 }

 public function getDetailDataGardu_induk($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListUpt() {
  $data = Modules::run('database/get', array(
              'table' => 'upt',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Gardu_induk";
  $data['title_content'] = 'Tambah Gardu_induk';
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataGardu_induk($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Gardu_induk";
  $data['title_content'] = 'Ubah Gardu_induk';
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataGardu_induk($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Gardu_induk";
  $data['title_content'] = "Detail Gardu_induk";
  $data['list_upt'] = $this->getListUpt();
  echo Modules::run('template', $data);
 }

 public function getPostDataGardu_induk($value) {
  $data['upt'] = $value->upt;
  $data['nama_gardu'] = $value->nama;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $post = $this->getPostDataGardu_induk($data->form);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Gardu_induk";
  $data['title_content'] = 'Data Gardu_induk';
  $content = $this->getDataGardu_induk($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

}
