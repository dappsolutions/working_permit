<div class="col-md-4">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Pemohon</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="form-group">
   <label for="" class="control-label">Data Pemohon</label>
   <select disabled="" class="form-control" error="Data Pemohon" 
           id="pemohon" onchange="WpInternal.getPemohon(this)">
    <option value="">Pilih Data Pemohon</option>
    <option value="add_pemohon">Pemohon Baru</option>
    <?php if (!empty($list_pemohon)) { ?>
     <?php foreach ($list_pemohon as $value) { ?>
      <?php $selected = '' ?>
      <?php if (isset($pemohon)) { ?>
       <?php $selected = $pemohon == $value['id'] ? 'selected' : '' ?>
      <?php } ?>
      <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_pemohon'] ?></option>
     <?php } ?>
    <?php } ?>
   </select>   
  </div>

  <div class="form-group">
   <label for="" class="control-label">Nama Pemohon</label>
   <div class="input-group">
    <input disabled="" type="text" class="form-control required" 
           error="Nama Pemohon" id="nama_pemohon" 
           placeholder="Nama Pemohon"
           value="<?php echo isset($nama_pemohon) ? $nama_pemohon : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-user"></i>
    </span>
   </div>   
  </div>
  <div class="form-group">
   <label for="" class="control-label">Perusahaan</label>
   <div class="input-group">
    <input disabled="" type="text" class="form-control required" 
           error="Perusahaan" id="perusahaan" 
           placeholder="Perusahaan"
           value="<?php echo isset($perusahaan) ? $perusahaan : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-building-o"></i>
    </span>
   </div>    
  </div>     
  <div class="form-group">
   <label for="" class="control-label">No Hp</label>
   <div class="input-group">
    <input disabled="" type="text" class="form-control required" 
           error="No Hp" id="no_hp" 
           placeholder="No Hp"
           value="<?php echo isset($no_hp) ? $no_hp : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-phone"></i>
    </span>
   </div>    
  </div>      

  <div class="form-group">
   <label for="" class="control-label">No Telp</label>
   <div class="input-group">
    <input disabled="" type="text" class="form-control required" 
           error="No Telp" id="no_telp" 
           placeholder="No Telp"
           value="<?php echo isset($no_telp) ? $no_telp : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-phone"></i>
    </span>
   </div>     
  </div>

  <div class="form-group">
   <label for="" class="control-label">Jabatan</label>
   <div class="input-group">
    <input disabled="" type="text" class="form-control required" 
           error="Jabatan" id="jabatan" 
           placeholder="Jabatan"
           value="<?php echo isset($jabatan) ? $jabatan : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-ticket"></i>
    </span>
   </div>    
  </div>
  <div class="form-group">
   <label for="" class="control-label">Email</label>
   <div class="input-group">
    <input disabled="" type="text" class="form-control required" 
           error="Email" id="email" 
           placeholder="Email"
           value="<?php echo isset($email) ? $email : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-address-book"></i>
    </span>
   </div>      
  </div>  

  <div class="form-group">
   <label for="" class="control-label">Alamat</label>
   <textarea disabled="" class="form-control required" error="Alamat" id="alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>