<input type="hidden" value="<?php echo $permit ?>" id="permit" class="form-control" />

<div class="row">
 <div class="col-md-12">
  <select class="form-control required" error="Risk Management" id="risk">
   <option value="">Pilih High Risk</option>
   <?php if (!empty($data)) { ?>
    <?php foreach ($data as $value) { ?>
     <?php $selected = '' ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
    <?php } ?>
   <?php } ?>
  </select>
 </div>
</div>
<br/>
<div class="row">
 <div class="col-md-12 text-right">
  <button id="proses" onclick="WpInternal.execChooseHigh(this)" class="btn btn-success">Proses</button>
 </div>
</div>