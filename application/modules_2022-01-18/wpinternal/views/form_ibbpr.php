<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data IBBPR</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table" id="tb_ibppr" style="width: 1000px;">
    <thead>
     <tr class="bg-primary-light text-white">
      <th rowspan="2">Kegiatan</th>
      <th>Potensi Bahaya</th>
      <th rowspan="2">Resiko</th>
      <th colspan="5">Penilaian Resiko Potensi Bahaya</th>
      <th rowspan="2">Pengendalian</th>
      <th colspan="5">Penilaian Setelah Supaya Pengendalian Resiko</th>      
      <th rowspan="2" class="text-center">Action</th>
     </tr>
     <tr class="bg-warning">      
      <th>sumber/mekanisme/target</th>      
      <th>Akibat</th>
      <th>Paparan</th>
      <th>Peluang</th>
      <th>Nilai</th>
      <th>Tingkat Resiko</th>
      <th>Akibat</th>
      <th>Paparan</th>
      <th>Peluang</th>
      <th>Nilai</th>
      <th>Tingkat Resiko</th>
     </tr>
    </thead>
    <tbody>
     <?php $required = 'required'; ?>
     <?php if (isset($list_ibppr)) { ?>
      <?php if (!empty($list_ibppr)) { ?>
       <?php foreach ($list_ibppr as $value) { ?>
        <tr data_id="<?php echo $value['id'] ?>">
         <td>
          <textarea id="kegiatan" style="width: 180px;" class="form-control <?php echo $required ?>" error="Kegiatan"><?php echo $value['kegiatan'] ?></textarea>
         </td>
         <td>
          <textarea id="sumber" class="form-control <?php echo $required ?>" error="Sumber Mekanisme"><?php echo $value['potensi_bahaya'] ?></textarea>
         </td>
         <td>
          <textarea id="resiko" style="width: 180px;" class="form-control <?php echo $required ?>" error="Resiko"><?php echo $value['resiko'] ?></textarea>
         </td>
         <td>
          <select class="form-control <?php echo $required ?>" style="width: 120px;" error="Akibat" id="akibat" 
                  onchange="WpInternal.hitungNilai(this)">
           <option nilai="0"  value="">Pilih Akibat</option>
           <?php if (!empty($list_akibat)) { ?>
            <?php foreach ($list_akibat as $v_a) { ?>
             <?php $selected = $v_a['id'] == $value['akibat'] ? 'selected' : '' ?>
             <option nilai="<?php echo $v_a['nilai'] ?>" <?php echo $selected ?> value="<?php echo $v_a['id'] ?>"><?php echo $v_a['nilai'] ?></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td>
          <select class="form-control <?php echo $required ?>" style="width: 120px;" error="Paparan" id="paparan" 
                  onchange="WpInternal.hitungNilai(this)">
           <option nilai="0"  value="">Pilih Paparan</option>
           <?php if (!empty($list_paparan)) { ?>
            <?php foreach ($list_paparan as $v_a) { ?>
             <?php $selected = $v_a['id'] == $value['paparan'] ? 'selected' : '' ?>
             <option nilai="<?php echo $v_a['nilai'] ?>" <?php echo $selected ?> value="<?php echo $v_a['id'] ?>"><?php echo $v_a['nilai'] ?></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td>
          <select class="form-control <?php echo $required ?>" style="width: 120px;" error="Peluang" id="peluang" 
                  onchange="WpInternal.hitungNilai(this)">
           <option nilai="0"  value="">Pilih Peluang</option>
           <?php if (!empty($list_peluang)) { ?>
            <?php foreach ($list_peluang as $v_a) { ?>
             <?php $selected = $v_a['id'] == $value['peluang'] ? 'selected' : '' ?>
             <option nilai="<?php echo $v_a['nilai'] ?>" <?php echo $selected ?> value="<?php echo $v_a['id'] ?>"><?php echo $v_a['nilai'] ?></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td>
          <input type="number" readonly="" value="<?php echo $value['nilai'] ?>" id="nilai" style="width: 80px;" class="form-control <?php echo $required ?>" error="Nilai"/>
         </td>
         <td>
          <input type="text" readonly="" value="<?php echo $value['tingkat_resiko'] ?>" id="tingkat_resiko" style="width: 120px;" class="form-control <?php echo $required ?>" error="Tingkat Resiko"/>
         </td>
         <td>
          <textarea id="pengendalian" style="width: 180px;" class="form-control <?php echo $required ?>" error="Pengendalian"><?php echo $value['pengendalian'] ?></textarea>
         </td>
         <td>
          <select style="width: 120px;" class="form-control <?php echo $required ?>" error="Akibat" id="akibat" 
                  onchange="WpInternal.hitungNilai(this, 'after')">
           <option nilai="0" value="">Pilih Akibat</option>
           <?php if (!empty($list_akibat)) { ?>
            <?php foreach ($list_akibat as $v_a) { ?>
             <?php $selected = $v_a['id'] == $value['akibat_after'] ? 'selected' : '' ?>
             <option nilai="<?php echo $v_a['nilai'] ?>" <?php echo $selected ?> value="<?php echo $v_a['id'] ?>"><?php echo $v_a['nilai'] ?></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td>
          <select style="width: 120px;" class="form-control <?php echo $required ?>" error="Paparan" id="paparan" 
                  onchange="WpInternal.hitungNilai(this, 'after')">
           <option nilai="0"  value="">Pilih Paparan</option>
           <?php if (!empty($list_paparan)) { ?>
            <?php foreach ($list_paparan as $v_a) { ?>
             <?php $selected = $v_a['id'] == $value['paparan_after'] ? 'selected' : '' ?>
             <option nilai="<?php echo $v_a['nilai'] ?>" <?php echo $selected ?> value="<?php echo $v_a['id'] ?>"><?php echo $v_a['nilai'] ?></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td>
          <select style="width: 120px;" class="form-control <?php echo $required ?>" error="Peluang" id="peluang" 
                  onchange="WpInternal.hitungNilai(this, 'after')">
           <option nilai="0"  value="">Pilih Peluang</option>
           <?php if (!empty($list_peluang)) { ?>
            <?php foreach ($list_peluang as $v_a) { ?>
             <?php $selected = $v_a['id'] == $value['peluang_after'] ? 'selected' : '' ?>
             <option nilai="<?php echo $v_a['nilai'] ?>" <?php echo $selected ?> value="<?php echo $v_a['id'] ?>"><?php echo $v_a['nilai'] ?></option>
            <?php } ?>
           <?php } ?>
          </select>
         </td>
         <td>
          <input type="number" readonly="" value="<?php echo $value['nilai_after'] ?>" style="width: 80px;" id="nilai" class="form-control <?php echo $required ?>" error="Nilai"/>
         </td>
         <td>
          <input type="text" readonly="" value="<?php echo $value['tingkat_resiko_after'] ?>" style="width: 120px;" id="tingkat_resiko" class="form-control <?php echo $required ?>" error="Tingkat Resiko"/>
         </td>
         <td class="text-center">
          <i class="fa fa-trash fa-lg hover-content" onclick="WpInternal.removeIbppr(this)"></i>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>

     <?php $required = isset($list_ibppr) ? '' : 'required' ?>
     <?php $counter = 1; ?>
     <?php if ($required != '') { ?>
      <?php $counter = 3; ?>
     <?php } ?>
     <?php for ($i = 0; $i < $counter; $i++) { ?>
      <tr data_id="">
       <td>
        <textarea id="kegiatan" style="width: 180px;" class="form-control <?php echo $required ?>" error="Kegiatan"></textarea>
       </td>
       <td>
        <textarea id="sumber" class="form-control <?php echo $required ?>" error="Sumber Mekanisme"></textarea>
       </td>
       <td>
        <textarea id="resiko" style="width: 180px;" class="form-control <?php echo $required ?>" error="Resiko"></textarea>
       </td>
       <td>
        <select class="form-control <?php echo $required ?>" style="width: 120px;" error="Akibat" id="akibat" 
                onchange="WpInternal.hitungNilai(this)">
         <option nilai="0"  value="">Pilih Akibat</option>
         <?php if (!empty($list_akibat)) { ?>
          <?php foreach ($list_akibat as $value) { ?>
           <?php $selected = '' ?>
           <option nilai="<?php echo $value['nilai'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nilai'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </td>
       <td>
        <select class="form-control <?php echo $required ?>" style="width: 120px;" error="Paparan" id="paparan" 
                onchange="WpInternal.hitungNilai(this)">
         <option nilai="0"  value="">Pilih Paparan</option>
         <?php if (!empty($list_paparan)) { ?>
          <?php foreach ($list_paparan as $value) { ?>
           <?php $selected = '' ?>
           <option nilai="<?php echo $value['nilai'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nilai'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </td>
       <td>
        <select class="form-control <?php echo $required ?>" style="width: 120px;" error="Peluang" id="peluang" 
                onchange="WpInternal.hitungNilai(this)">
         <option nilai="0"  value="">Pilih Peluang</option>
         <?php if (!empty($list_peluang)) { ?>
          <?php foreach ($list_peluang as $value) { ?>
           <?php $selected = '' ?>
           <option nilai="<?php echo $value['nilai'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nilai'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </td>
       <td>
        <input type="number" readonly="" value="0" id="nilai" style="width: 80px;" class="form-control <?php echo $required ?>" error="Nilai"/>
       </td>
       <td>
        <input type="text" readonly="" value="" id="tingkat_resiko" style="width: 120px;" class="form-control <?php echo $required ?>" error="Tingkat Resiko"/>
       </td>
       <td>
        <textarea id="pengendalian" style="width: 180px;" class="form-control <?php echo $required ?>" error="Pengendalian"></textarea>
       </td>
       <td>
        <select style="width: 120px;" class="form-control <?php echo $required ?>" error="Akibat" id="akibat" 
                onchange="WpInternal.hitungNilai(this, 'after')">
         <option nilai="0" value="">Pilih Akibat</option>
         <?php if (!empty($list_akibat)) { ?>
          <?php foreach ($list_akibat as $value) { ?>
           <?php $selected = '' ?>
           <option nilai="<?php echo $value['nilai'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nilai'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </td>
       <td>
        <select style="width: 120px;" class="form-control <?php echo $required ?>" error="Paparan" id="paparan" 
                onchange="WpInternal.hitungNilai(this, 'after')">
         <option nilai="0"  value="">Pilih Paparan</option>
         <?php if (!empty($list_paparan)) { ?>
          <?php foreach ($list_paparan as $value) { ?>
           <?php $selected = '' ?>
           <option nilai="<?php echo $value['nilai'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nilai'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </td>
       <td>
        <select style="width: 120px;" class="form-control <?php echo $required ?>" error="Peluang" id="peluang" 
                onchange="WpInternal.hitungNilai(this, 'after')">
         <option nilai="0"  value="">Pilih Peluang</option>
         <?php if (!empty($list_peluang)) { ?>
          <?php foreach ($list_peluang as $value) { ?>
           <?php $selected = '' ?>
           <option nilai="<?php echo $value['nilai'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nilai'] ?></option>
          <?php } ?>
         <?php } ?>
        </select>
       </td>
       <td>
        <input type="number" readonly="" value="0" style="width: 80px;" id="nilai" class="form-control <?php echo $required ?>" error="Nilai"/>
       </td>
       <td>
        <input type="text" readonly="" value="" style="width: 120px;" id="tingkat_resiko" class="form-control <?php echo $required ?>" error="Tingkat Resiko"/>
       </td>
       <td class="text-center">
        <i class="fa fa-plus fa-lg hover-content" onclick="WpInternal.addIbppr(this)"></i>
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>