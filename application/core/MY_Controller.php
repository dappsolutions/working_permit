<?php (defined('BASEPATH')) or exit('No direct script access allowed');
use \Illuminate\Database\Capsule\Manager as Capsule;

class MY_Controller extends MX_Controller
{
 public $capsule;
 public function __construct()
 {
  $this->setDBManager();
 }

 private function setDBManager()
 {
  // require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7  
  // $this->capsule = new \Illuminate\Database\Capsule\Manager();
  $this->capsule = new Capsule();

  $this->capsule->addConnection(array(
   'driver'    => 'mysql',
   'host'      => 'localhost',
   'database'  => 'wp',
   'username'  => 'root',
   'password'  => '',
   'charset'   => 'utf8',
   'collation' => 'utf8_unicode_ci',
   'prefix'    => '',
  ));
  $this->capsule->setAsGlobal();
  $this->capsule->bootEloquent();
 }
}
