<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class PegawaiModel extends Eloquent
{
    protected $table = 'pegawai';
    public $timestamps = false;
}
