<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class DocumentModel extends Eloquent
{
    protected $table = 'document';
    public $timestamps = false;
}