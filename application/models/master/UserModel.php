<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class UserModel extends Eloquent
{
    protected $table = 'user';
    public $timestamps = false;
}