<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class UptModel extends Eloquent
{
    protected $table = 'upt';
    public $timestamps = false;
}
