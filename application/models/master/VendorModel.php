<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class VendorModel extends Eloquent
{
    protected $table = 'vendor';
    public $timestamps = false;
}