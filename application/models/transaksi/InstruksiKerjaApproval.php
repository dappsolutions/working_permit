<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
include APPPATH . 'models/master/PegawaiModel.php';
class InstruksiKerjaApproval extends Eloquent
{
    protected $table = 'instruksi_kerja_approval';

				public function pegawaiDetail(){
					return $this->hasOne(PegawaiModel::class, 'nip', 'nip');
				}
}
