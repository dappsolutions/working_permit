<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class RolesApprovalIka extends Eloquent
{
    protected $table = 'roles_approval_ika';
}
