<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class PermitVerifikasi extends Eloquent
{
    protected $table = 'permit_verifikasi';
    public $timestamps = false;
}