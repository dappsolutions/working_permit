<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class InstruksiKerjaDokumen extends Eloquent
{
    protected $table = 'instruksi_kerja_dokumen_text';
}
