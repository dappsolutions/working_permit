<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class InstruksiPerlengkapan extends Eloquent
{
    protected $table = 'instruksi_kerja_perlengkapan';
}
