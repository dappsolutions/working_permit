<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
include APPPATH . 'models/master/DocumentModel.php';
include APPPATH . 'models/master/UptModel.php';
include APPPATH . 'models/transaksi/InstruksiKerjaApproval.php';
class InstruksiKerjaModel extends Eloquent
{
    protected $table = 'instruksi_kerja';

    public function documentDetail(){
        return $this->hasOne(DocumentModel::class, 'id', 'document');
    }

	public function tujuanDetail(){
		return $this->hasOne(UptModel::class, 'id', 'tujuan_unit');
	}

	public function approvalData(){
		return $this->hasMany(InstruksiKerjaApproval::class, 'instruksi_kerja', 'id');
	}
}
