<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
include APPPATH . 'models/transaksi/PermitAttandance.php';
class PermitModel extends Eloquent
{
    protected $table = 'permit';

    public function detailPerson(){
     return $this->hasMany(PermitAttandance::class, 'permit', 'id');
    }
}
