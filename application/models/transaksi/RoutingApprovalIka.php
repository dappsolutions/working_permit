<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class RoutingApprovalIka extends Eloquent
{
    protected $table = 'routing_approval_ika';
}
