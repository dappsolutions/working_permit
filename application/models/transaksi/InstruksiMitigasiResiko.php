<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class InstruksiMitigasiResiko extends Eloquent
{
    protected $table = 'instruksi_kerja_mitigasi_resiko';
}
