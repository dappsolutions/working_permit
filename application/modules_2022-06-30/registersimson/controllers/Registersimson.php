<?php

class Registersimson extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getModuleName()
	{
		return 'register';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/controllers/registersimson.js"></script>',
		);

		return $data;
	}

	public function getTableName()
	{
		return "pegawai";
	}

	public function index()
	{
		$data['view_file'] = 'index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Register";
		$data['title_content'] = 'Register';
		$data['content'] = $this->getDataVendor();
		$data['list_upt'] = $this->getListUpt();
		echo $this->load->view('index', $data, true);
	}


	public function getListUpt()
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDataPegawaiTipe()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai_type',
			'where' => "tipe = 'SIMSON'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}


		return $result;
	}

	public function getDataVendor()
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' pg',
			'field' => array('pg.*', 'raa.approveby', 'raa.rejectedby', 'ut.nama as nama_upt'),
			'join' => array(
				array('user u', 'u.pegawai = pg.id', 'left'),
				array('upt ut', 'ut.id = pg.upt', 'left'),
				array('role_apps_akses raa', 'u.id = raa.user and raa.apps = 2', 'left'),
				array('pegawai_type pt', 'pt.id = pg.pegawai_type', 'left'),
			),
			'where' => "pt.tipe = 'SIMSON'",
			'orderby' => 'raa.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function registerVendor()
	{
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);
		// die;
		$pegawai_type = $this->getDataPegawaiTipe();
		$data['pegawai_type'] = $pegawai_type['id'];

		$is_valid = false;
		$message = "";
		$this->db->trans_begin();
		try {
			$push = array();
			$push['nama'] = $data['nama_pegawai'];
			$push['email'] = $data['email'];
			$push['no_hp'] = $data['no_hp'];
			$push['upt'] = $data['upt'];
			$push['pegawai_type'] = $data['pegawai_type'];
			$id = Modules::run('database/_insert', $this->getTableName(), $push);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}
		echo json_encode(array('is_valid' => $is_valid, 'message' > $message));
	}
}
