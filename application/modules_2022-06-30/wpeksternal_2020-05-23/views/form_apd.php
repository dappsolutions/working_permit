<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Alat Kerja</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_apd">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Nama Alat Kerja</th>
      <th>Ketersediaan</th>
      <th class="text-center">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_apd)) { ?>
      <?php if (!empty($list_apd)) { ?>
       <?php foreach ($list_apd as $value) { ?>
        <tr data_id="<?php echo $value['id'] ?>">
         <td>
          <input type="text" value="<?php echo $value['nama_alat'] ?>" id="nama_alat" class="form-control required" error="Nama Alat"/>
         </td>
         <td>
          <input type="number" value="<?php echo $value['ketersediaan'] ?>" min="1" id="ketersediaan" class="text-right form-control required" error="Ketersediaan"/>
         </td>
         <td class="text-center">
          <i class="fa fa-trash fa-lg hover-content" onclick="WpEksternal.removeAlatKerja(this)"></i>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>
     <?php $required = isset($list_pelaksana) ? '' : 'required' ?>
     <tr data_id="">
      <td>
       <input type="text" value="" id="nama_alat" class="form-control <?php echo $required ?>" error="Nama Alat"/>
      </td>
      <td>
       <input type="number" value="1" min="1" id="ketersediaan" class="text-right form-control <?php echo $required ?>" error="Ketersediaan"/>
      </td>
      <td class="text-center">
       <i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addAlatKerja(this)"></i>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>