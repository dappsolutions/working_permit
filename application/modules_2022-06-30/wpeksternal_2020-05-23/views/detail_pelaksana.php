<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Pelaksana</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_pelaksana">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Nama</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($list_pelaksana)) { ?>
      <?php foreach ($list_pelaksana as $value) { ?>
       <tr>
        <td>
         <input disabled type="text" value="<?php echo $value['nama'] ?>" id="nama" class="form-control required" error="Nama"/>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td>
        Tidak ada data ditemukan
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>