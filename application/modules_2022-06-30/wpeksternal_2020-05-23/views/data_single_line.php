<input type="hidden" value="<?php echo $permit ?>" id="permit" class="form-control" />

<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <div class="table-responsive">
    <table class="table table-bordered" id="tb_content">
     <thead>
      <tr class="bg-primary-light text-white">
       <th>No</th>
       <th>Single Line</th>
       <th>Action</th>
      </tr>
     </thead>
     <tbody>
      <?php if (!empty($data)) { ?>
       <?php $no = 1; ?>
       <?php foreach ($data as $value) { ?>
        <tr class="" data_id="<?php echo $value['id'] ?>">
         <td><b><?php echo $no++ ?></b></td>
         <td><b><?php echo $value['nama'] ?></b></td>          
         <td class="text-center">
          <button class="btn btn-warning" onclick="WpEksternal.execChooseSingleLine(this)">Pilih</button>
          &nbsp;
          <button class="btn btn-success" file="<?php echo $value['file'] ?>" onclick="WpEksternal.showFile(this, 'diagram')">Lihat</button>
         </td>
        </tr>
       <?php } ?>
      <?php } else { ?>
       <tr>
        <td colspan="20" class="text-center">Tidak ada data ditemukan</td>
       </tr>
      <?php } ?>           
     </tbody>
    </table>
   </div>
  </div>
 </div>
</div>