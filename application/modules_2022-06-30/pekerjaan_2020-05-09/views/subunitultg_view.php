<div class="box">
 <div class="box-body">
  <div class="row">
   <div class="col-md-12">
    <h4><?php echo $nama_place ?></h4>
    <div class="table-responsive">
     <table class="table table-bordered">
      <thead>
       <tr>
        <th>ULTG</th>
        <th>Action</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($data)) { ?>
        <?php foreach ($data as $value) { ?>
         <tr>
          <td><?php echo $value['nama'] ?></td>
          <td>
           <a href="<?php echo base_url() . $module . '/event/' . $value['id'] . '/subupt' ?>" class="btn btn-warning"><i class="fa fa-archive"></i> &nbsp;Pekerjaan</a>
          </td>
         </tr>
        <?php } ?>
       <?php } else { ?>
        <tr>
         <td colspan="2">Tidak ada ditemukan</td>
        </tr>
       <?php } ?>
      </tbody>
     </table>

    </div>
   </div>
  </div>
 </div>
</div>