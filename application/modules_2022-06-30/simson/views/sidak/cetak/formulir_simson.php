<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: arial;
			font-weight: bold;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
		}

		.content-top {
			font-family: tahoma;
		}
	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_top-content">
				<table style="width: 100%;max-width: 100%;">
					<tr>
						<td style="border-right: none;"><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
						<td style="border-right: none;border-left: none;">
							<h3>&nbsp;PT PLN (PERSERO)</h3>
							<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
						</td>
						<td colspan="70"></td>
						<td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
					</tr>
					<tr>
						<td id="_judul" colspan="60" rowspan="4" style="border-top: 1px solid black;">
							<center>
								<label>
									CHECKLIST KEPATUHAN K3
								</label>
							</center>
						</td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>1 dari 1</label></td>
					</tr>
				</table>
			</div>
			<div>
				<hr />
			</div>

			<div class="label-title" style="text-align: left;padding: 16px;font-family: tahoma;margin-top: -40px;">
				<table style="width: 100%;">
					<tr>
						<th colspan="7" style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">URAIAN & KONDISI</th>
					</tr>
					<tr>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">NO</th>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">KELENGKAPAN</th>
						<th colspan="2" style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">Pelaksanaan</th>
						<th colspan="2" style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">Kesesuaian</th>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">KETERANGAN</th>
					</tr>
					<tr>
						<th style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">ada</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">tidak</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">ya</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #a9d08e;font-size:12px;">tidak</th>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">1</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Body Harnes</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['body_harnes'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['body_harnes'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">2</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Lanyard</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['lanyard'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['lanyard'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">3</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Hook / Step</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['hook_or_step'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['hook_or_step'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">4</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Pullstrap</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['pullstrap'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['pullstrap'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">5</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Safety Helmet</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['safety_helmet'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['safety_helmet'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">6</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Kacamata UV</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['kacamata_uv'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['kacamata_uv'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">7</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Earplug</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['earplug'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['earplug'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">8</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Masker</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['masker'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['masker'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">9</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Wearpack</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['wearpack'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['wearpack'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">10</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Sepatu Safety</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['sepatu_safety'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['sepatu_safety'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">11</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Sarung Tangan</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['sarung_tangan'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['sarung_tangan'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">12</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Stick Grounding</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['stick_grounding'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['stick_grounding'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">13</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Grounding Cable</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['grounding_cable'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['grounding_cable'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">14</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Stick Isolasi</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['stick_isolasi'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['stick_isolasi'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">15</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Voltage Detector</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['voltage_detector'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['voltage_detector'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">16</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Tambang</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['tambang'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['tambang'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">17</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Scaffolding</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['scaffolding'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['scaffolding'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">18</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Tagging</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['tagging'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['tagging'] != 'yes' ? 'v' : '-' ?></td>
					</tr>

					<tr>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">19</td>
						<td style="font-family: tahoma;padding: 8px;border-right: 1px solid black;font-size:12px;">Pembatas Area</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">
							&nbsp;
						</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;">&nbsp;</td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['pembatas_area'] == 'yes' ? 'v' : '-' ?></td>
						<td style="font-family: tahoma;text-align: center;border-right: 1px solid black;font-size:12px;"><?php echo $data_safety['pembatas_area'] != 'yes' ? 'v' : '-' ?></td>
					</tr>
				</table>
				<br>
				<?php $tempat_kerja = '' ?>
				<?php if ($wp['tempat'] == 'GARDU INDUK') { ?>
					<?php $tempat_kerja = $wp['tempat_kerja_gardu'] ?>
				<?php } ?>
				<?php if ($wp['tempat'] == 'UNIT') { ?>
					<?php $tempat_kerja = $wp['tempat_kerja_upt'] ?>
				<?php } ?>
				<?php if ($wp['tempat'] == 'SUB UNIT') { ?>
					<?php $tempat_kerja = $wp['tempat_kerja_ultg'] ?>
				<?php } ?>
				<?php if ($wp['tempat'] == 'SUTET / SUTT' || $wp['tempat'] == 'SUTET/SUTT') { ?>
					<?php $tempat_kerja = $wp['tempat_kerja_sutt'] ?>
				<?php } ?>
				<i><b>Jenis Pekerjaan</b> : <?php echo $data_safety['jenis_pekerjaan'] ?></i>
				<br />
				<i><b>Uraian Pekerjaan</b> : <?php echo $wp['uraian_pekerjaan'] ?></i>
				<br>
				<i><b>Detail Tempat</b> : <?php echo $tempat_kerja ?></i>
				<br>
				<?php $class_tr = $data_safety['status'] == 'TIDAK AMAN' ? 'color:red;' : 'color:green' ?>
				<i><b>Score</b> : <?php echo $data_safety['score'] ?> (<b style="<?php echo $class_tr ?>"><?php echo $data_safety['status'] ?></b>)</i>
			</div>

			<div class="ttd" style="padding: 16px;">
				<table style="width: 100%;border:none;">
					<tr>
						<th style="border: none;text-align: center;font-family: tahoma;font-size:12px;">PENGAWAS K3</th>
						<th style="border: none;text-align: center;font-family: tahoma;font-size:12px;">PENGAWAS PEKERJAAN</th>
						<th style="border: none;text-align: center;font-family: tahoma;font-size:12px;">SPV GI / GITET</th>
					</tr>
					<tr>
						<td style="border: none;text-align: center;">&nbsp;</td>
						<td style="border: none;text-align: center;">&nbsp;</td>
						<td style="border: none;text-align: center;">&nbsp;</td>
					</tr>
					<tr>
						<td style="border: none;text-align: center;font-family: tahoma;font-size:12px;">
							<?php echo strtoupper($wp['pengawas_k3']) ?>
							<br>
							<?php echo strtoupper($wp['jabatan_pengawas_k3']) ?>
						</td>
						<td style="border: none;text-align: center;font-family: tahoma;font-size:12px;">
							<?php echo strtoupper($wp['pengawas_pekerjaan']) ?>
							<br>
							<?php echo strtoupper($wp['jabatan_pengawas_pekerjaan']) ?>
						</td>
						<td style="border: none;text-align: center;font-family: tahoma;font-size:12px;">
							<?php if (!empty($wp['spv_gi'])) { ?>
								<?php echo strtoupper($wp['spv_gi']['spv_gi']) ?>
								<br>
								<?php echo strtoupper($wp['spv_gi']['posisi']) ?>
							<?php } ?>
						</td>
					</tr>
				</table>

				<br />
				<br />
				<br />
				<h5 style="text-align: left;font-family: tahoma;">E. DOKUMENTASI</h5>
				<table style="width: 100%;">
					<?php if (!empty($data_image)) { ?>
						<?php $counter = 1; ?>
						<?php foreach ($data_image as $key => $value) { ?>
							<tr>
								<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">
									<?php echo $value['elm_image'] ?>
								</td>
							</tr>
							<?php $counter += 1; ?>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">-</td>
							<td style="font-family: tahoma;padding: 8px;border: 1px solid black;font-size:12px;">-</td>
						</tr>
						<tr>
							<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">-</td>
							<td style="font-family: tahoma;padding: 8px;border: 1px solid black;font-size:12px;">-</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>

</html>