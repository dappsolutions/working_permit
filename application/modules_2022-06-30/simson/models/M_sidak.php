
<?php

class M_sidak extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function simpan($params)
	{
		error_reporting(-1);

		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$this->db->trans_begin();
		try {
			//code...
			$push = array();
			$push['no_trans'] = $params['no_trans'];
			$push['permit'] = $params['data_wp']['id'];
			$push['group_transaction'] = 1;
			$push['lat'] = $params['lat'];
			$push['lng'] = $params['lng'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('permit_transaction_log', $push);

			$permit_log = $this->db->insert_id();

			// echo strtoupper($params['data_safety']['resiko_pekerjaan']);
			// die;
			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['body_harnes'] = $params['data_apd']['body_harnes'];
			$push['lanyard'] = $params['data_apd']['lanyard'];
			$push['hook_or_step'] = $params['data_apd']['hook_or_step'];
			$push['pullstrap'] = $params['data_apd']['pullstrap'];
			$push['safety_helmet'] = $params['data_apd']['safety_helmet'];
			$push['kacamata_uv'] = $params['data_apd']['kacamata_uv'];
			$push['earplug'] = $params['data_apd']['earplug'];
			$push['masker'] = $params['data_apd']['masker'];
			$push['wearpack'] = $params['data_apd']['wearpack'];
			$push['sepatu_safety'] = $params['data_apd']['sepatu_safety'];
			$push['sarung_tangan'] = $params['data_apd']['sarung_tangan'];
			$push['stick_grounding'] = $params['data_apd']['stick_grounding'];
			$push['grounding_cable'] = $params['data_apd']['grounding_cable'];
			$push['stick_isolasi'] = $params['data_apd']['stick_isolasi'];
			$push['voltage_detector'] = $params['data_apd']['voltage_detector'];
			$push['scaffolding'] = $params['data_apd']['scaffolding'];
			$push['tambang'] = $params['data_apd']['tambang'];
			$push['tagging'] = $params['data_apd']['tagging'];
			$push['pembatas_area'] = $params['data_apd']['pembatas_area'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$push['permit_transaction_log'] = $permit_log;

			$this->db->insert('permit_has_kelayakan_apd', $push);

			// echo '<pre>';
			// echo $this->db->last_query();
			// die;

			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['kondisi_tanah'] = $params['data_safety']['kondisi_tanah'];
			$push['kondisi_lingkungan'] = $params['data_safety']['kondisi_lingkungan'];
			$push['kondisi_cuaca'] = $params['data_safety']['kondisi_cuaca'];
			$push['ketinggian_tk'] = $params['data_safety']['ketinggian_tk'];
			$push['tegangan_tk'] = $params['data_safety']['tegangan_tk'];
			$push['lokasi'] = $params['data_safety']['lokasi'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$push['permit_transaction_log'] = $permit_log;
			$push['jenis_pekerjaan'] = $params['jenis_pekerjaan'];
			$push['resiko_pekerjaan'] = strtoupper($params['data_safety']['resiko_pekerjaan']);
			if (!empty($params['penilaian'])) {
				$push['score'] = $params['penilaian']['score'];
				$push['status'] = $params['penilaian']['status'];
				$push['nilai_apd'] = $params['penilaian']['nilai_apd'];
			}
			$this->db->insert('permit_has_safety_cond', $push);

			if (!empty($params['data_image'])) {
				foreach ($params['data_image'] as $val) {
					$push = array();
					$push['permit_transaction_log'] = $permit_log;
					$push['picture'] = $val->encodeImages;
					$this->db->insert('permit_has_sidak_detail_pict', $push);
				}
			}


			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDetailTransaksi($params)
	{
		$sql = "select * from permit_transaction_log where no_trans = '" . $params['no_trans'] . "'";
		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function batalPengajuan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$dataTrans = $this->getDetailTransaksi($params);
		// echo '<pre>';
		// print_r($dataTrans);die;

		$this->db->trans_begin();
		try {
			$this->db->delete('permit_has_kelayakan_apd', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_has_safety_cond', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_has_sidak_detail_pict', array('permit_transaction_log' => $dataTrans['id']));
			$this->db->delete('permit_transaction_log', array('id' => $dataTrans['id']));
			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function simpanGambar($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$dataTrans = $this->getDetailTransaksi($params);

		$this->db->trans_begin();
		try {

			$push = array();
			$push['permit_transaction_log'] = $dataTrans['id'];
			$push['picture'] = $params['image'];
			$this->db->insert('permit_has_sidak_detail_pict', $push);
			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDataSafety($params)
	{

		$filter = "";
		if(isset($params['no_transaksi'])){
			$filter = " and ptl.no_trans = '".$params['no_transaksi']."'";
		}

		$sql = "
		select 
		phsc.*
		, phka.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join (
			select max(id) id, permit from permit_has_safety_cond
			where jenis_pekerjaan = '" . $params['jenis_pekerjaan'] . "'
			group by permit  
		) phsc_max
		join permit_has_safety_cond phsc
			on phsc.id = phsc_max.id						
		join permit_transaction_log ptl
			on ptl.id = phsc.permit_transaction_log 
		join permit_has_kelayakan_apd phka
			on phka.permit_transaction_log = ptl.id 
		where ptl.group_transaction = 1
		and p.no_wp = '" . $params['no_wp'] . "'
		".$filter."
		order by ptl.id DESC ";

		// echo '<pre>';
		// echo $sql;
		// die;
		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataSafetyByNoTrans($params)
	{
		$sql = "
		select 
		phsc.*
		, phka.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_has_safety_cond phsc
			on phsc.permit = p.id						
		join permit_transaction_log ptl
			on ptl.id = phsc.permit_transaction_log 
		join permit_has_kelayakan_apd phka
			on phka.permit_transaction_log = ptl.id 
		where ptl.group_transaction = 1
		and ptl.no_trans = '" . $params['no_trans'] . "'";

		// echo '<pre>';
		// echo $sql;
		// die;
		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataSidakImage($params)
	{
		$filter = "";
		if(isset($params['no_transaksi'])){
			$filter = " and no_trans = '".$params['no_transaksi']."'";
		}

		$sql = "
		select 
		ptl.id
		, phsdp.picture 
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join (
			select max(id) id, permit from permit_has_safety_cond
			where jenis_pekerjaan = '" . $params['jenis_pekerjaan'] . "'
			group by permit  
		) phsc_max
		join (
			select max(id) id, permit from permit_transaction_log where group_transaction = 1 ".$filter." group by permit 
		) ptl_max
			on ptl_max.permit = p.id 
		join permit_has_safety_cond phsc
			on phsc.id = phsc_max.id
			and phsc.permit_transaction_log = ptl_max.id			
		join permit_transaction_log ptl
			on ptl.id = phsc.permit_transaction_log 
		join permit_has_kelayakan_apd phka
			on phka.permit_transaction_log = ptl.id 
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 1
		and p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getDataSidakImageByNoTrans($params)
	{
		$sql = "
		select 
		ptl.id
		, phsdp.picture 
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_has_safety_cond phsc
			on phsc.permit = p.id						
		join permit_transaction_log ptl
			on ptl.id = phsc.permit_transaction_log 
		join permit_has_kelayakan_apd phka
			on phka.permit_transaction_log = ptl.id 
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 1
		and ptl.no_trans = '" . $params['no_trans'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getDataSafetyDetail($params)
	{
		$sql = "select * from permit_has_safety_cond phsc
		join permit p 
			on p.id = phsc.permit 
		where p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataApdDetail($params)
	{
		$sql = "
		select phka.* from permit_has_kelayakan_apd phka
		join permit p 
			on p.id = phka.permit 
		where p.no_wp = '" . trim($params['no_wp']) . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataTraining($data_safety = array(), $jenis_pekerjaan = '')
	{
		$sql = "
		select st.* from simson_training st
		where st.kondisi_tanah = '" . trim($data_safety['kondisi_tanah']) . "'
		and st.kondisi_lingkungan = '" . trim($data_safety['kondisi_lingkungan']) . "'
		and st.kondisi_cuaca = '" . trim($data_safety['kondisi_cuaca']) . "'
		and st.ketinggian_tk = '" . trim($data_safety['ketinggian_tk']) . "'
		and st.tegangan_tk = '" . trim($data_safety['tegangan_tk']) . "'
		and st.lokasi_tk = '" . trim($data_safety['lokasi']) . "'
		and st.jenis_pekerjaan = '" . trim($jenis_pekerjaan) . "'";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataJenisPekerjaanPermit($params = array())
	{
		$sql = "
		select 
		DISTINCT 
		phsc.jenis_pekerjaan 
		from permit_has_safety_cond phsc 
		join permit p 
			on p.id = phsc.permit 
		where p.no_wp = '" . $params['no_wp'] . "'";


		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getListDataSafety($params)
	{
		$sql = "
		select 
		phsc.*
		, phka.*
		, ptl.no_trans
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_has_safety_cond phsc
			on phsc.permit = p.id						
			and phsc.jenis_pekerjaan = '" . trim($params['jenis_pekerjaan']) . "'
		join permit_transaction_log ptl
			on ptl.id = phsc.permit_transaction_log 
		join permit_has_kelayakan_apd phka
			on phka.permit_transaction_log = ptl.id 
		where ptl.group_transaction = 1
		and p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// echo $sql;
		// die;
		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}


	public function getListDataSidakImage($params)
	{
		$sql = "
		select 
		ptl.id
		, phsdp.picture 
		, ptl.no_trans
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_has_safety_cond phsc
			on phsc.permit = p.id						
			and phsc.jenis_pekerjaan = '" . trim($params['jenis_pekerjaan']) . "'
		join permit_transaction_log ptl
			on ptl.id = phsc.permit_transaction_log 
		join permit_has_kelayakan_apd phka
			on phka.permit_transaction_log = ptl.id 
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 1
		and p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getStrukturAccPertama($params)
	{
		// $sql = "
		// select 
		// 	p.upt 
		// 	from role_apps_akses raa
		// 	join `user` u 
		// 		on u.id = raa.`user` 
		// 	join pegawai p 
		// 		on p.id = u.pegawai 
		// 	join upt ut
		// 		on ut.id = p.upt 
		// 	join apps a 
		// 		on a.id = raa.apps 
		// 	where u.id = " . $params['user_id'] . "
		// 	and  a.aplikasi = 'simson'";
		$sql = "
		
		select pp.upt from permit p
		join permit_purpose pp 
			on pp.permit = p.id 
		where p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$data_struktur = $this->getDataStruktur($data);
			$result = $data_struktur;
		}

		return $result;
	}

	public function getDataStruktur($params)
	{
		$sql = "select 
		sa.*
		, u.username as email
		from struktur_approval sa
		join `user` u 
			on u.id = sa.`user` 
		where sa.upt = " . $params['upt'] . "
		and sa.`level` = 1
		and sa.deleted = 0
		LIMIT 1";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$result = $data;
		}

		return $result;
	}
}
