
<?php

class M_login extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function signIn($params)
	{
		$sql = "
		select rap.*
		, u.id as user_id
		from role_apps_akses rap 
		join `user` u 
			on u.id = rap.`user`
		join apps a 
			on a.id = rap.apps  	
		join pegawai p
			on p.id = u.pegawai
		join upt ut
			on ut.id = p.upt 		
		where a.aplikasi = 'simson'
		and rap.deleted = 0
		and u.username = '" . $params['username'] . "'
		and u.password = '" . $params['password'] . "'";

		$data = $this->db->query($sql);

		if (!empty($data->result_array())) {
			return $data->row_array();
		}

		return array();
	}
}
