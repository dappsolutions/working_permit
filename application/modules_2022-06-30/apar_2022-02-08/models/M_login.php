
<?php

class M_login extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function signIn($params)
	{
		$sql = "
		select rap.*
		, u.id as user_id
		, u.username as email
		, pg.nama as name
		, ut.id as upt_id
		, w.nama_wilayah
		, pg.id as pegawai_id
		from role_apps_akses rap 
		join `user` u 
			on u.id = rap.`user`
		join apps a 
			on a.id = rap.apps
		join pegawai pg
			on pg.id = u.pegawai			
		join upt ut
			on ut.id = pg.upt
		join role_apps_apar raa
			on raa.role_apps_akses = rap.id
		join wilayah w
			on w.id = raa.wilayah
		where a.aplikasi = 'peralatan'
		and rap.deleted = 0
		and u.username = '" . $params['username'] . "'
		and u.password = '" . $params['password'] . "'";

		$data = $this->db->query($sql);

		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
			$result['lokasi_tujuan'] = 0;
			$result['id_tujuan'] = 0;
			if ($result['nama_wilayah'] == 'GI') {
				//cari spv gi terkait
				$dataSpv = $this->getDataSpvGi($result);
				if(!empty($dataSpv)){
					$result['lokasi_tujuan'] = $dataSpv['id'];
					$result['id_tujuan'] = $dataSpv['id_tujuan'];
				}
			}
		}

		return $result;
	}

	public function getDataSpvGi($params)
	{
		$sql = "select 
		lt.*
		from gardu_induk_has_pegawai gihp 
		join lokasi_tujuan lt 
			on lt.id_tujuan = gihp.gardu_induk 
		join lokasi_apar la
			on la.id = lt.lokasi_apar 
		join wilayah w 
			on w.id = la.wilayah 
		where gihp.pegawai = " . $params['pegawai_id'] . "
		and gihp.deleted = 0
		and w.nama_wilayah = 'GI'";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}
}
