
<?php

class M_sidak extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function simpan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';
		$result['message'] = '';

		$this->db->trans_begin();
		try {
			//code...
			$push = [];
			$push['user'] = $params['user_id'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('actor', $push);
			$actor_id = $this->db->insert_id();

			$push = [];
			$push['no_document'] = $params['no_trans'];
			$push['createddate'] = $date;
			$push['createdby'] = $actor_id;
			$push['doc_type'] = 'DOCT_APAR';
			$push['verbatim'] = $params['keterangan'];
			$this->db->insert('document', $push);
			$doc_id = $this->db->insert_id();

			$dataIdAlatPemeliharaan = [];
			foreach ($params['data_inspeksi'] as $key => $value) {
				$push = [];
				$push['document'] = $doc_id;
				$push['createddate'] = $date;
				$push['createdby'] = $actor_id;
				$push['alat'] = $params['id_apar'];
				$push['berat'] = $value->berat;
				$push['status_alat'] = '4';
				$push['keterangan'] = '';
				$push['period_start'] = $value->tanggal;
				$push['period_end'] = $value->tanggal;
				$push['pembersihan'] = $value->pembersihan;
				$push['tekanan'] = $value->tekanan;
				$push['petugas'] = $value->petugas;
				$push['lokasi_tujuan'] = $params['lokasi_tujuan'];
				$this->db->insert('alat_transaksi_pemeliharaan', $push);
				$id_atp = $this->db->insert_id();
				$dataIdAlatPemeliharaan[$key] = $id_atp;
			}
			
			foreach ($params['data_kelengkapan'] as $key => $value) {
				$push = [];
				$push['document'] = $doc_id;
				$push['createddate'] = $date;
				$push['createdby'] = $actor_id;
				$push['alat'] = $params['id_apar'];
				$push['period_start'] = $value->tanggal;
				$push['hanger'] = $value->hanger;
				$push['ketinggian_apar'] = $value->ketinggian_apar;
				$push['segitiga_apar'] = $value->segitiga_apar;
				$push['indikator_tekanan'] = $value->indikator_tekanan;
				$push['hose'] = $value->hose;
				$push['tabung'] = $value->tabung;
				$push['lokasi_tujuan'] = $params['lokasi_tujuan'];
				$push['alat_transaksi_pemeliharaan'] = $dataIdAlatPemeliharaan[$key];
				$this->db->insert('alat_transaksi_pemeliharaan_kelengkapan', $push);
			}

			$push = array();
			$push['document'] = $doc_id;
			$push['status'] = 'DRAFT';
			$push['createdby'] = $actor_id;
			$this->db->insert('document_transaction', $push);

			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}
	
	public function ubahKeterangan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';
		$result['message'] = '';

		$dataTrans = $this->getDetailTransaksi($params);

		$this->db->trans_begin();
		try {
			//code...
			$push = [];
			$push['user'] = $params['user_id'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('actor', $push);
			$actor_id = $this->db->insert_id();

			$push = [];
			$push['verbatim'] = $params['keterangan'];
			$this->db->update('document', $push, array('id'=> $dataTrans['id']));




			$push = array();
			$push['document'] = $dataTrans['id'];
			$push['status'] = 'APPROVED';
			$push['createdby'] = $actor_id;
			$this->db->insert('document_transaction', $push);

			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function simpanPerubahan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';
		$result['message'] = '';

		$this->db->trans_begin();
		try {
			//code...
			$push = [];
			$push['user'] = $params['user_id'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('actor', $push);
			$actor_id = $this->db->insert_id();


			$push = [];
			$push['createddate'] = $date;
			$push['createdby'] = $actor_id;
			$push['berat'] = $params['berat'];
			$push['period_start'] = $params['tanggal'];
			$push['period_end'] = $params['tanggal'];
			$push['pembersihan'] = $params['pembersihan'];
			$push['tekanan'] = $params['tekanan'];
			$push['petugas'] = $params['petugas'];
			$this->db->update('alat_transaksi_pemeliharaan', $push, array('id'=> $params['id']));
			
			$push = [];
			$push['createddate'] = $date;
			$push['createdby'] = $actor_id;
			$push['period_start'] = $params['tanggal'];
			$push['hanger'] = $params['hanger'];
			$push['ketinggian_apar'] = $params['ketinggian_apar'];
			$push['segitiga_apar'] = $params['segitiga_apar'];
			$push['indikator_tekanan'] = $params['indikator_tekanan'];
			$push['hose'] = $params['hose'];
			$push['tabung'] = $params['tabung'];
			$this->db->update('alat_transaksi_pemeliharaan_kelengkapan', $push, array('alat_transaksi_pemeliharaan'=> $params['id']));

			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function batalPengajuan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';
		$result['message'] = '';

		$dataTrans = $this->getDetailTransaksi($params);
		// echo '<pre>';
		// print_r($dataTrans);die;

		$this->db->trans_begin();
		try {
			$this->db->delete('document_picture', array('document' => $dataTrans['id']));
			$this->db->delete('document_transaction', array('document' => $dataTrans['id']));
			$this->db->delete('alat_transaksi_pemeliharaan', array('document' => $dataTrans['id']));
			$this->db->delete('alat_transaksi_pemeliharaan_kelengkapan', array('document' => $dataTrans['id']));
			$this->db->delete('document', array('id' => $dataTrans['id']));
			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDetailTransaksi($params)
	{
		$sql = "select * from document where no_document = '" . $params['no_trans'] . "'";
		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function simpanGambar($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';
		$result['message'] = '';

		$dataTrans = $this->getDetailTransaksi($params);

		$this->db->trans_begin();
		try {

			$push = array();
			$push['document'] = $dataTrans['id'];
			$push['picture'] = $params['image'];
			$this->db->insert('document_picture', $push);
			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDataSafety($params)
	{
		$filter = "";
		if (isset($params['ptlid'])) {
			$filter = "and ptl.no_trans = '" . $params['ptlid'] . "'";
			$sql = "
		
		select 
		phs.*
		, phsk.*
		, phsp.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id " . $filter . "
		join permit_has_swa phs 
			on phs.permit = p.id and ptl.id = phs.permit_transaction_log
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id and ptl.id = phsk.permit_transaction_log
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id and ptl.id = phsp.permit_transaction_log
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'";

			// echo '<pre>';
			// echo $sql;
			// die;
		} else {
			$sql = "
		
		select 
		phs.*
		, phsk.*
		, phsp.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join (select max(id) id, permit from permit_transaction_log group by permit) p_log_max
			on p_log_max.permit = p.id 
		join permit_transaction_log ptl
			on ptl.id = p_log_max.id 
		join (select max(id) id, permit from permit_has_swa group by permit) phs_max
			on phs_max.permit = p.id 
		join permit_has_swa phs 
			on phs_max.id = phs.id and p.id = phs.permit
		join (select max(id) id, permit from permit_has_swa_kelengkapan group by permit) phsk_max
			on phsk_max.permit = p.id 
		join permit_has_swa_kelengkapan phsk 
			on phsk_max.id = phsk.id and p.id = phsk.permit
		join (select max(id) id, permit from permit_has_swa_pekerjaan group by permit) phsp_max
			on phsp_max.permit = p.id 
		join permit_has_swa_pekerjaan phsp  
			on phsp_max.id = phsp.id and p.id = phsp.permit
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'";
		}


		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataSidakImage($params)
	{

		// echo '<pre>';
		// print_r($params);die;

		$sql = "
		select dp.* from document_picture dp 
		join document d 
			on d.id = dp.document 
		where d.no_document = '" . $params['no_document'] . "'";

		// echo '<pre>';
		// echo $sql;die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getDataSafetyAll($params)
	{
		$sql = "
		
		select 
		phs.*
		, phsk.*
		, phsp.*
		, ptl.no_trans
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id 
		join permit_has_swa phs 
			on phs.permit = p.id and ptl.id = phs.permit_transaction_log 
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id and ptl.id = phsk.permit_transaction_log 
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id and ptl.id = phsp.permit_transaction_log 
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'
		order by ptl.id asc";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				$params['ptlid'] = $val['no_trans'];
				$val['data_image'] = $this->getDataSidakImage($params);
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getDataDetailSidakPengajuan($params)
	{
		$sql = "
		select 
		ptl.id
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join permit_transaction_log ptl
			on ptl.permit = p.id 
		join permit_has_swa phs 
			on phs.permit = p.id 
		join permit_has_swa_kelengkapan phsk 
			on phsk.permit = p.id 
		join permit_has_swa_pekerjaan phsp  
			on phsp.permit = p.id
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 2
		and p.no_wp = '" . $params['no_wp'] . "'
		order by ptl.id ASC ";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function getStrukturAccPertama($params)
	{
		$sql = "
		select 
			p.upt 
			from role_apps_akses raa
			join `user` u 
				on u.id = raa.`user` 
			join pegawai p 
				on p.id = u.pegawai 
			join upt ut
				on ut.id = p.upt 
			join apps a 
				on a.id = raa.apps 
			where u.id = " . $params['user_id'] . "
			and  a.aplikasi = 'SWA'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$data_struktur = $this->getDataStruktur($data);
			$result = $data_struktur;
		}

		return $result;
	}

	public function getDataStruktur($params)
	{
		$sql = "select 
		sa.*
		, u.username as email
		from struktur_approval sa
		join `user` u 
			on u.id = sa.`user` 
		where sa.upt = " . $params['upt'] . "
		and sa.`level` = 1
		and sa.deleted = 0
		LIMIT 1";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$result = $data;
		}

		return $result;
	}

	public function getDataListTransaksiApar($params)
	{
		$sql = "
		select atp.* 
		, d.verbatim 
		, atp.period_start as tanggal
		from alat_transaksi_pemeliharaan atp
		join document d 
			on d.id = atp.document 		
		where d.no_document ='" . $params['no_document'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['verbatim'] = $value['verbatim'] == '' ? '' : $value['verbatim'];
				array_push($result, $value);
			}
		}

		return $result;
	}
	
	public function getDataListTransaksiAparKelengkapan($params)
	{
		$sql = "
		select atp.* 
		, atp.period_start as tanggal
		from alat_transaksi_pemeliharaan_kelengkapan atp
		join document d 
			on d.id = atp.document 		
		where d.no_document ='" . $params['no_document'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDetailDataTransaksi($params)
	{
		$sql = "
		select atp.* 
		, d.verbatim 
		, atp.period_start as tanggal
		from alat_transaksi_pemeliharaan atp
		join document d 
			on d.id = atp.document 		
		where atp.id ='" . $params['id'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['verbatim'] = $value['verbatim'] == '' ? '' : $value['verbatim'];
				array_push($result, $value);
			}
		}

		$result = current($result);
		return $result;
	}
	
	public function getDetailDataTransaksiKelengkapan($params)
	{
		$sql = "
		select atp.* 
		, atp.period_start as tanggal
		from alat_transaksi_pemeliharaan_kelengkapan atp
		join document d 
			on d.id = atp.document 		
		where atp.alat_transaksi_pemeliharaan ='" . $params['id'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		$result = current($result);
		return $result;
	}
	
	public function getDetailDataSidak($params)
	{
		$sql = "		
select 
d.id
, atl.no_alat
, d.verbatim
, w.nama_wilayah 
, la.nama_lokasi 
, lp.tempat 
, atp.period_start
, p.nama as nama_pegawai
, d.createddate
from document d
join actor a 
	on a.id = d.createdby
join (
	select min(id) id, document from alat_transaksi_pemeliharaan group by document
) al_min
	on al_min.document = d.id
join alat_transaksi_pemeliharaan atp 
	on atp.id= al_min.id
join (
	select max(id) id, document from document_transaction group by document
) doc_t_max
	on doc_t_max.document = d.id
join document_transaction dt 
	on dt.id = doc_t_max.id
join alat atl
	on atl.id = atp.alat
join lokasi_penempatan_alat lpa 
	on lpa.alat = atl.id 
join lokasi_penempatan lp 
	on lp.id  = lpa.lokasi_penempatan 
join lokasi_tujuan lt 
	on lt.id = lp.lokasi_tujuan 
join lokasi_apar la 
	on la.id = lt.lokasi_apar 
join wilayah w 
	on w.id = la.wilayah 
join `user` u 
	on u.id = a.`user` 
join pegawai p 
	on p.id = u.pegawai 
where d.deleted = 0 and d.no_document = '".$params['no_document']."'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['verbatim'] = $value['verbatim'] == '' ? '' : $value['verbatim'];
				array_push($result, $value);
			}
		}

		$result = current($result);
		return $result;
	}
}
