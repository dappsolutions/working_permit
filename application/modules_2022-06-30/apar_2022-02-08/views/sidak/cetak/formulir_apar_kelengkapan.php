<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: arial;
			font-weight: bold;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
		}

		.content-top {
			font-family: tahoma;
		}
	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_top-content">
				<table style="width: 100%;max-width: 100%;">
					<tr>
						<td style="border-right: none;"><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
						<td style="border-right: none;border-left: none;">
							<h3>&nbsp;PT PLN (PERSERO)</h3>
							<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
						</td>
						<td colspan="70"></td>
						<td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
					</tr>
					<tr>
						<td id="_judul" colspan="60" rowspan="4" style="border-top: 1px solid black;">
							<center>
								<label>
									FORMULIR PEMELIHARAAN APAR
								</label>
							</center>
						</td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>1 dari 1</label></td>
					</tr>
				</table>
			</div>
			<div>
				<hr />
			</div>

			<div class="content-top" style="padding: 16px;">
				<table style="border: none;width: 50%;font-family: tahoma;font-size:12px;">
					<tr style="border: none;">
						<td>GI / Kantor</td>
						<td>:</td>
						<td><b><?php echo $head_form['nama_lokasi'] ?></b></td>
					</tr>
					<tr style="border: none;">

						<td>Lokasi</td>
						<td>:</td>
						<td><b><?php echo $head_form['tempat'] ?></b></td>
					</tr>
					<tr style="border: none;">

						<td>Tanggal Awal Isi</td>
						<td>:</td>
						<td><b><?php echo date('d F Y', strtotime($head_form['period_start'])) ?></b></td>
					</tr>
					<tr style="border: none;">

						<td>No. APAR</td>
						<td>:</td>
						<td><b><?php echo $head_form['no_alat'] ?></b></td>
					</tr>
					<tr style="border: none;">

						<td>Tanggal Isi Ulang</td>
						<td>:</td>
						<td><b><?php echo date('d F Y', strtotime($head_form['createddate'])) ?></b></td>
					</tr>
				</table>
			</div>

			<div class="label-title" style="text-align: left;padding: 16px;font-family: tahoma;">
				<table style="width: 100%;">
					<tr>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">NO</th>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">TANGGAL</th>
						<th colspan="6" style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KELENGKAPAN</th>
						<th rowspan="2" style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">PETUGAS</th>
					</tr>
					<tr>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">HANGER</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">KETINGGIAN APAR</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">SEGITIGA APAR</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">INDIKATOR TEKANAN</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">HOSE</th>
						<th style="border:1px solid black;font-family: tahoma;background-color: #ccc;font-size:12px;">TABUNG</th>
					</tr>
					<?php $no = 1 ?>
					<?php foreach ($dataKelengkapan as $key => $value) { ?>
						<tr>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;text-align: center;"><?php echo $no++ ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;text-align: center;"><?php echo $value['period_start'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;"><?php echo $value['hanger'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;"><?php echo $value['ketinggian_apar'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;"><?php echo $value['segitiga_apar'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;"><?php echo $value['indikator_tekanan'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;"><?php echo $value['hose'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;"><?php echo $value['tabung'] ?></td>
							<td style="border:1px solid black;font-family: tahoma;font-size:12px;text-align: center;"><?php echo $dataItem[$key]['petugas'] ?></td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>

</html>