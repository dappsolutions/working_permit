<?php

class Vendor extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'vendor';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/vendor.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pengajuan_vendor';
 }

 public function getRootModule() {
  return "Data";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Vendor";
  $data['title_content'] = 'Vendor';
  $content = $this->getDataVendor();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataVendor($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pv.nama_vendor', $keyword),
       array('pv.pimpinan', $keyword),
       array('pv.email', $keyword),
       array('pv.no_hp', $keyword),
   );
  }

  $where = "pv.deleted = 0 and sv.status = 'APPROVED'";
  if ($this->akses == 'superadmin') {
   $where = "pv.deleted = 0 and sv.status = 'APPROVED'";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pv',
                'field' => array('pv.*', 'sv.status'),
                'join' => array(
                    array('vendor v', 'pv.id = v.pengajuan_vendor', 'left'),
                    array('(select max(id) id, pengajuan_vendor from status_pengajuan_vendor group by pengajuan_vendor) pvv', 'pvv.pengajuan_vendor = pv.id'),
                    array('status_pengajuan_vendor sv', 'pvv.id = sv.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where,
                'orderby' => 'pv.id desc'
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pv',
                'field' => array('pv.*', 'sv.status'),
                'join' => array(
                    array('vendor v', 'pv.id = v.pengajuan_vendor', 'left'),
                    array('(select max(id) id, pengajuan_vendor from status_pengajuan_vendor group by pengajuan_vendor) pvv', 'pvv.pengajuan_vendor = pv.id'),
                    array('status_pengajuan_vendor sv', 'pvv.id = sv.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where,
                'orderby' => 'pv.id desc'
    ));
    break;
  }

  return $total;
 }

 public function getDataVendor($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pv.nama_vendor', $keyword),
       array('pv.pimpinan', $keyword),
       array('pv.email', $keyword),
       array('pv.no_hp', $keyword),
   );
  }

  $where = "pv.deleted = 0 and sv.status = 'APPROVED'";
  if ($this->akses == 'superadmin') {
   $where = "pv.deleted = 0 and sv.status = 'APPROVED'";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pv',
                'field' => array('pv.*', 'sv.status'),
                'join' => array(
                    array('vendor v', 'pv.id = v.pengajuan_vendor', 'left'),
                    array('(select max(id) id, pengajuan_vendor from status_pengajuan_vendor group by pengajuan_vendor) pvv', 'pvv.pengajuan_vendor = pv.id'),
                    array('status_pengajuan_vendor sv', 'pvv.id = sv.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where,
                'orderby' => 'pv.id desc'
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pv',
                'field' => array('pv.*', 'sv.status'),
                'join' => array(
                    array('vendor v', 'pv.id = v.pengajuan_vendor', 'left'),
                    array('(select max(id) id, pengajuan_vendor from status_pengajuan_vendor group by pengajuan_vendor) pvv', 'pvv.pengajuan_vendor = pv.id'),
                    array('status_pengajuan_vendor sv', 'pvv.id = sv.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where,
                'orderby' => 'pv.id desc'
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataVendor($keyword)
  );
 }

 public function getDetailDataVendor($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Vendor";
  $data['title_content'] = 'Tambah Vendor';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataVendor($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Vendor";
  $data['title_content'] = 'Ubah Vendor';
  echo Modules::run('template', $data);
 }

 public function getStatusPengajuan($id) {
  $data = Modules::run('database/get', array(
              'table' => 'status_pengajuan_vendor sv',
              'where' => "sv.pengajuan_vendor = '" . $id . "'",
              'orderby' => 'sv.id desc'
  ));

  return $data->row_array();
 }

 public function detail($id) {
  $data = $this->getDetailDataVendor($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Vendor";
  $data['title_content'] = "Detail Vendor";
  $data['status'] = $this->getStatusPengajuan($id);
  $data['user_vendor'] = $this->getDataUserVendor($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getDataUserVendor($vendor) {
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'join' => array(
                  array('vendor v', 'v.id = u.vendor'),
                  array('pengajuan_vendor pv', 'pv.id = v.pengajuan_vendor'),
              ),
              'where' => "v.pengajuan_vendor = '" . $vendor . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }

  return $result;
 }

 public function getPostDataVendor($value) {
  $data['nama'] = $value->nama;
  return $data;
 }

 public function createUser() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $data_pengajuan = $this->getDetailDataVendor($id);
   $post['nama_vendor'] = $data_pengajuan['nama_vendor'];
   $post['pimpinan'] = $data_pengajuan['pimpinan'];
   $post['email'] = $data_pengajuan['email'];
   $post['no_hp'] = $data_pengajuan['no_hp'];
   $post['pengajuan_vendor'] = $id;
   $vendor = Modules::run('database/_insert', 'vendor', $post);

   //created user
   $post_user['username'] = $data->form->username;
   $post_user['password'] = $data->form->password;
   $post_user['vendor'] = $vendor;
   Modules::run('database/_insert', 'user', $post_user);

   //update status
   $vendor_statud['status'] = 'APPROVED';
   $vendor_statud['pengajuan_vendor'] = $id;
   $vendor_statud['user'] = $this->session->userdata('user_id');
   Modules::run('database/_insert', 'status_pengajuan_vendor', $vendor_statud);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function rejectPengajuan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   //update status
   $vendor_statud['status'] = 'REJECTED';
   $vendor_statud['pengajuan_vendor'] = $id;
   $vendor_statud['user'] = $this->session->userdata('user_id');
   $vendor_statud['keterangan'] = $_POST['keterangan'];
   Modules::run('database/_insert', 'status_pengajuan_vendor', $vendor_statud);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Vendor";
  $data['title_content'] = 'Data Vendor';
  $content = $this->getDataVendor($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function approve($id) {
  $data['id'] = $id;
  echo $this->load->view('form_user', $data, true);
 }

 public function reject($id) {
  $data['id'] = $id;
  echo $this->load->view('form_reject', $data, true);
 }

}
