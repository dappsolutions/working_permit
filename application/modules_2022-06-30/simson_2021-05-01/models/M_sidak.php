
<?php

class M_sidak extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function simpan($params)
	{
		$date = date('Y-m-d H:i:s');
		$result['is_valid'] = '0';

		$this->db->trans_begin();
		try {
			//code...
			$push = array();
			$push['no_trans'] = $params['no_trans'];
			$push['permit'] = $params['data_wp']['id'];
			$push['group_transaction'] = 1;
			$push['lat'] = $params['lat'];
			$push['lng'] = $params['lng'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('permit_transaction_log', $push);

			$log_id = $this->db->insert_id();

			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['body_harnes'] = $params['data_apd']['body_harnes'];
			$push['lanyard'] = $params['data_apd']['lanyard'];
			$push['hook_or_step'] = $params['data_apd']['hook_or_step'];
			$push['pullstrap'] = $params['data_apd']['pullstrap'];
			$push['safety_helmet'] = $params['data_apd']['safety_helmet'];
			$push['kacamata_uv'] = $params['data_apd']['kacamata_uv'];
			$push['earplug'] = $params['data_apd']['earplug'];
			$push['masker'] = $params['data_apd']['masker'];
			$push['wearpack'] = $params['data_apd']['wearpack'];
			$push['sepatu_safety'] = $params['data_apd']['sepatu_safety'];
			$push['sarung_tangan'] = $params['data_apd']['sarung_tangan'];
			$push['stick_grounding'] = $params['data_apd']['stick_grounding'];
			$push['grounding_cable'] = $params['data_apd']['grounding_cable'];
			$push['stick_isolasi'] = $params['data_apd']['stick_isolasi'];
			$push['voltage_detector'] = $params['data_apd']['voltage_detector'];
			$push['scaffolding'] = $params['data_apd']['scaffolding'];
			$push['tambang'] = $params['data_apd']['tambang'];
			$push['tagging'] = $params['data_apd']['tagging'];
			$push['pembatas_area'] = $params['data_apd']['pembatas_area'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('permit_has_kelayakan_apd', $push);

			$push = array();
			$push['permit'] = $params['data_wp']['id'];
			$push['kondisi_tanah'] = $params['data_safety']['kondisi_tanah'];
			$push['kondisi_lingkungan'] = $params['data_safety']['kondisi_lingkungan'];
			$push['kondisi_cuaca'] = $params['data_safety']['kondisi_cuaca'];
			$push['ketinggian_tk'] = $params['data_safety']['ketinggian_tk'];
			$push['tegangan_tk'] = $params['data_safety']['tegangan_tk'];
			$push['lokasi'] = $params['data_safety']['lokasi'];
			$push['status'] = $params['data_safety']['status'];
			$push['createddate'] = $date;
			$push['createdby'] = $params['user_id'];
			$this->db->insert('permit_has_safety_cond', $push);

			if (!empty($params['data_image'])) {
				foreach ($params['data_image'] as $val) {
					$push = array();
					$push['permit_transaction_log'] = $log_id;
					$push['picture'] = $val->encodeImages;
					$this->db->insert('permit_has_sidak_detail_pict', $push);
				}
			}


			$this->db->trans_commit();
			$result['is_valid'] = '1';
		} catch (\Throwable $th) {
			//throw $th;
			$result['message'] = $th->getMessage();
			$this->db->trans_rollback();
		}

		return $result;
	}

	public function getDataSafety($params)
	{
		$sql = "
		select 
		phsc.*
		, phka.*
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join (select max(id) id, permit from permit_transaction_log where group_transaction = 1 group by permit) p_log_max
			on p_log_max.permit = p.id 
		join permit_transaction_log ptl
			on ptl.id = p_log_max.id
		join (select max(id) id, permit from permit_has_safety_cond group by permit) phsc_max
			on phsc_max.permit = p.id 
		join permit_has_safety_cond phsc
			on phsc_max.id = phsc.id and p.id = phsc.permit
		join (select max(id) id, permit from permit_has_kelayakan_apd group by permit) phka_max
			on phka_max.permit = p.id 
		join permit_has_kelayakan_apd phka
			on phka_max.id = phka.id and p.id = phka.permit
		where ptl.group_transaction = 1
		and p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// echo $sql;
		// die;
		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataSidakImage($params)
	{
		$sql = "
		select 
		ptl.id
		, phsdp.picture 
		from permit p
		join (select max(id) id, permit from permit_status group by permit) pt_max
			on pt_max.permit = p.id
		join permit_status ps 
			on ps.id = pt_max.id and ps.`level` =3
		join (select max(id) id, permit from permit_transaction_log where group_transaction = 1 group by permit) p_log_max
			on p_log_max.permit = p.id 
		join permit_transaction_log ptl
			on ptl.id = p_log_max.id
		join (select max(id) id, permit from permit_has_safety_cond group by permit) phsc_max
			on p_log_max.permit = p.id 
		join permit_has_safety_cond phsc
			on phsc_max.id = phsc.id and p.id = phsc.permit
		join (select max(id) id, permit from permit_has_kelayakan_apd group by permit) phka_max
			on phka_max.permit = p.id 
		join permit_has_kelayakan_apd phka
			on phka_max.id = phka.id and p.id = phka.permit
		join permit_has_sidak_detail_pict phsdp
			on phsdp.permit_transaction_log = ptl.id
		where ptl.group_transaction = 1
		and p.no_wp = '" . $params['no_wp'] . "'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}
}
