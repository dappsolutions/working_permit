<?php

class Sidak extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_sidak', 'sidak');
		$this->load->model('m_permit', 'permit');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		echo 'sidak ' . date('Y-m-d H:i:s');
	}

	public function simpan()
	{
		$data = $_POST;
		$data_wp = $this->permit->getDetailData($data);
		$data_image = (array) json_decode($_POST['data_image']);
		$data_apd = (array) json_decode($_POST['data_apd']);
		$data_safety = (array) json_decode($_POST['data_safety']);
		$data['data_wp'] = $data_wp;
		$data['data_image'] = $data_image;
		$data['data_apd'] = $data_apd;
		$data['data_safety'] = $data_safety;
		$data['no_trans'] = Modules::run('no_generator/generateNoTransLog');
		$result = $this->sidak->simpan($data);



		// echo '<pre>';
		// print_r($data['data_image']);
		// die;
		echo json_encode($result);
	}

	public function getDetailData()
	{
		$data = $_POST;
		$data_safety = $this->sidak->getDataSafety($data);
		$result['data_safety'] = $data_safety;
		$result['data_apd'] = $data_safety;
		Modules::run('simson/output/get', $result);
	}

	public function getDetailImage()
	{
		$data = $_POST;
		// $data['no_wp'] = 'WPINT20JAN002';
		$data_image = $this->sidak->getDataSidakImage($data);
		$result['data'] = $data_image;
		Modules::run('simson/output/get', $result);
	}

	public function getModuleName()
	{
		return 'simson/sidak';
	}

	public function showDetailPengajuan()
	{
		$data = $_GET;
		$data['data_sidak'] = $this->sidak->getDataSafety($data);
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		// echo '<pre>';
		// print_r($data['data_image']);
		// die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengajuan";
		$data['title_content'] = "Detail Pengajuan";
		echo Modules::run('template', $data);
	}



	public function getDokumenDp3Aktif()
	{
		$data = Modules::run('database/get', array(
			'table' => 'dp3',
			'where' => "deleted = 0 and id = 2"
		));

		$data = $data->row_array();
		$data['berlaku_efektif'] = Modules::run('helper/getIndoDate', $data['berlaku_efektif']);
		return $data;
	}

	public function cetak()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_GET;
		$data_safety = $this->sidak->getDataSafety($data);
		$data['data_safety'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		$data['dp3'] = $this->getDokumenDp3Aktif();

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$formulir_swa = $this->load->view('sidak/cetak/formulir_simson', $data, true);
		$mpdf->WriteHTML($formulir_swa);
		$mpdf->Output('FORMULIR SIMSON - ' . $data['no_wp'] . '.pdf', 'I');
	}

	public function base64ToImage($params, $imageData)
	{
		$imageData = 'data:image/jpeg;base64,' . $imageData;
		list($type, $imageData) = explode(';', $imageData);
		// echo $imageData;
		// die;
		list(, $extension) = explode('/', $type);
		list(, $imageData)      = explode(',', $imageData);
		// $fileName = $_SERVER["DOCUMENT_ROOT"] . '/working_permit/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		$fileName = $_SERVER["DOCUMENT_ROOT"] . '/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		$imageData = base64_decode($imageData);
		file_put_contents($fileName, $imageData);

		return $fileName;
	}
}
