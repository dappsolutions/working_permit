<?php

class Notif extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->load->model('m_notif', 'notif');
	}


	public function simpan()
	{

		$is_valid = "0";
		$data = array();
		try {
			$push = array();
			$push['status'] = 1;
			$push['content'] = $_GET['content'];
			$push['data'] = $_GET['data'];
			$push['to'] = $_GET['user_id'];
			$push['createddate'] = date('Y-m-d H:i:s');
			$this->db->insert('log_fcm', $push);
			array_push($data, array(
				'log_fcm' => $this->db->insert_id()
			));

			$is_valid = "1";
		} catch (Exception $exc) {
			$is_valid = "0";
		}

		$result['is_valid'] = $is_valid;
		$result['data'] = $data;
		$result['message'] = "Gagal Sinkronasi";
		if (!empty($data)) {
			$result['is_valid'] = $is_valid;
			$result['data'] = $data;
			$result['message'] = "Sinkronasi Sukses";
		}
		echo json_encode($result);
	}

	public function getData()
	{
		$params = $_GET;
		// $params['user_id'] = 3;
		$data = $this->notif->getData($params);
		$result['data'] = $data;
		Modules::run('simson/output/get', $result);
	}
}
