<div class="col-md-12">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Alat Kerja</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="table-responsive">
   <table class="table table-bordered" id="tb_apd">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Nama Alat Kerja</th>
      <th>Ketersediaan</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($list_apd)) { ?>
      <?php foreach ($list_apd as $value) { ?>
       <tr>
        <td>
         <input disabled type="text" value="<?php echo $value['nama_alat'] ?>" id="nama_alat" 
                class="form-control required" error="Nama Alat"/>
        </td>
        <td>
         <input disabled type="number" value="<?php echo $value['ketersediaan'] ?>" min="1" id="ketersediaan" 
                class="text-right form-control required" error="Ketersediaan"/>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>     
      <tr>
       <td colspan="2">
        Tidak ada data ditemukan
       </td>
       </td>
      </tr>
     <?php } ?>     
    </tbody>
   </table>
  </div>
 </div>   
 <!-- /.box-footer -->
</div>