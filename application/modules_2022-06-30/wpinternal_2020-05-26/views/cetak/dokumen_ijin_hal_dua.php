<html>
 <head>
  <title></title>    
  <style>
   #_wrapper{
    width: 100%;        
    margin: 0 auto;               
   }

   #_content{
    border: 1px solid #999;
    max-width: 100%;
    text-align: center;        
   }

   #_top-content{
    width: 95%;
    max-width: 95%;                
    margin: 1% auto;        
   }      

   #_judul{
    font-size: 100%;
    font-family: arial;
    font-weight: bold;
   }

   h3{
    margin: 0;
    font-size: 100%;
    font-family: arial;
   }     

   #_data{
    font-family: arial;
    font-size: 12px;
   }

   table{
    border-collapse: collapse;
    border: 1px solid black;
   }      


   #_form{                
    width: 15%;
    font-size: 12px;
    text-align: left;
    margin-left: 80%;
    margin-right:2%;
    padding: 0.5%;
    border: 1px solid black;        
   }      


   ._center-content{        
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;        
    border:1px solid black;
    padding-top: 1.5%;        
    padding-bottom: 1.5%;
    font-family: tahoma;
    font-size: 14px;
   }

   ._center-content > label{
    text-align: center;
    margin-bottom: 2%;
   }
  </style>
 </head>  
 <body>    
  <?php //var_dump($data); ?>
  <div id="_wrapper">
   <div id="_content">
    <div id="_top-content">          
     <table style="width: 100%;max-width: 100%;">
      <tr>
       <td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
       <td>                  
        <h3>&nbsp;PT PLN (PERSERO)</h3>
        <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
       </td>                                          
       <td colspan="70"></td>
       <td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
      </tr>                
      <tr>
       <td id="_judul" colspan="60" rowspan="4">
      <center>
       <label>
        FORMULIR IJIN KERJA <?php echo strtoupper($data_wp['tipe']) ?>
       </label>
      </center>                  
      </td>                
      <td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
      <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
      </tr>              
      <tr>                       
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>2 dari 8</label></td>
      </tr>
     </table>                                                                               
    </div>                
    <div><hr/></div>     
    <div  id="_form">
     <b><label>FORM : WP 1B</label></b>
    </div>                                  
    <br/>
    <div class="_center-content">         
     <label>JOB SAFETY ANALYSIS</label>             
     <hr/>
     <table style="border:none;">
      <tr>
       <td>Nama Pekerjaan</td>
       <td>:</td>
       <td><?php echo $data_wp['uraian_pekerjaan'] ?></td>
      </tr>
      <tr style="border:none;">
       <td style="border:none;">Tanggal</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $data_wp['tgl_awal'] . ' - ' . $data_wp['tgl_akhir'] ?></td>              
      </tr>            
      <tr style="border:none;">
       <td style="border:none;">APP</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php echo $data_wp['upt_tujuan'] ?></td>              
      </tr>
      <tr style="border:none;">
       <td style="border:none;">LOKASI</td>
       <td style="border:none;">:</td>
       <td style="border:none;"><?php
//        if ($row->gardu_induk_id != 0) {
//         $row->lokasi_kerja = $row->gardu_induk;
//        } else {
//         $row->lokasi_kerja = $row->lokasi_kerja;
//        }
        echo strtoupper($data_wp['nama_place']);
        ?></td>              
      </tr>
     </table>
     <br/>
     <table style="width: 100%;">
      <tr style="border:1px solid black;text-align: center;">
       <td style="border:1px solid black;font-family: tahoma;width: 7%">NO</td>
       <td style="border:1px solid black;width: 30%;height: 100%;font-family: tahoma;">TAHAPAN PEKERJAAN</td>
       <td style="border:1px solid black;font-family: tahoma;">POTENSI BAHAYA</td>
       <td style="border:1px solid black;font-family: tahoma;">PENGENDALIAN</td>
      </tr> 
      <?php if (!empty($jsa_data)) { ?>
       <?php foreach ($jsa_data as $value) { ?>
        <tr style="border:1px solid black;">
         <td style="border:1px solid black;font-family: tahoma;text-align: center;font-size: 8px;">1</td>              
         <td style="border:1px solid black;width: 30%;font-family: tahoma;font-size: 8px;">
          <?php
          echo $value['tahapan_pekerjaan'];
          ?>
         </td>
         <td style="border:1px solid black;font-size: 8px;">
          <?php
          echo $value['potensi_bahaya'];
          ?>
         </td>
         <td style="border:1px solid black;font-size: 8px;">
          <?php
          echo $value['pengendalian'];
          ?>
         </td>
        </tr>             
       <?php } ?>
      <?php } ?>      
     </table> 
     <br/>
     <table style="width: 100%;border:none;">
      <tr style="" class="_tb">
       <td style="width: 15%;font-family: tahoma;padding-top: 5%;padding-bottom: 2%;padding-left: 1%;">Dianalisa Oleh</td>
       <td>:</td>
       <td style="width: 15%;font-family: tahoma;">
        <!--SPV GI-->
        <?php
        if ($data_wp['jenis_lokasi'] != "GARDU INDUK") {
         echo 'SPV JARDGI/LAKS K4 / Admin UIT';
        } else {
         echo 'SPV GI';
        }
        ?>
       </td>
       <td style="font-family: tahoma;">              
        (<?php
//                if($row->gardu_induk_id != 0){
//                  echo $row->spv_gi;  
//                }else{
//                  echo $ttd_spv['nama'];
//                }
//               if($row->level_id == 1
//               || $row->level_id == 2){
        if ($data_wp['tipe'] == 'EKSTERNAL') {
         if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') {
          echo $data_wp['spv_gi'];
         } else {
          echo $data_wp['paraf'][0]['nama_pegawai'];
         }
        } else {
					if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi']  == 'SUTET / SUTT') {
						echo $data_wp['spv_gi'];
					} else{
						echo $data_wp['paraf'][0]['nama_pegawai'];
					}        
        }
        ?>)</td>
       <td style="font-family: tahoma;">(..........................)</td>
      </tr>
      <tr style="border:none;" class="_tb">
       <td style="width: 15%;font-family: tahoma;padding-top: 5%;padding-bottom: 2%;padding-left: 1%;">Diperiksa Oleh</td>
       <td>:</td>
       <td style="width: 15%;font-family: tahoma;">PENGAWAS K3</td>
       <td style="font-family: tahoma;">(<?php echo $data_wp['pengawas_k3'] ?>)</td>
       <td style="font-family: tahoma;">(..........................)</td>
      </tr>
      <tr style="border:none;" class="_tb">
       <td style="width: 15%;font-family: tahoma;padding-top: 5%;padding-bottom: 2%;padding-left: 1%;">Disetujui</td>
       <td>:</td>
       <?php
       if ($data_wp['tipe'] == 'EKSTERNAL') {
        ?>
        <td style="width: 15%;font-family: tahoma;">MANAGER ULTG/ MANAGER KONTS</td>              
        <td style="font-family: tahoma;">(<?php echo $data_wp['approval_dua']['nama_pegawai']; ?>)</td>
        <td style="font-family: tahoma;">(<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['approval_dua']['ttd'] ?>" width="70px" height="70px">)</td>
        <?php
       } else {
        ?>
        <td style="width: 15%;font-family: tahoma;">MANAGER ULTG/ MANAGER KONTS</td>
        <td style="font-family: tahoma;">(<?php echo $data_wp['approval_dua']['nama_pegawai']; ?>)</td>
        <td style="font-family: tahoma;">(<img src="<?php echo base_url() . 'files/berkas/ttd/' . $data_wp['approval_dua']['ttd'] ?>" width="70px" height="70px">)</td>
       <?php } ?>              
      </tr> 
     </table>
    </div>        
    <div>&nbsp;</div>
   </div>      
  </div>
  <div style="text-align: right;">
   <img src="<?php echo base_url() . 'files/berkas/paraf/' . $data_wp['paraf'][0]['file'] ?>" width="20px" height="20px" style="text-align: right;">
  </div>
 </body>
</html>
