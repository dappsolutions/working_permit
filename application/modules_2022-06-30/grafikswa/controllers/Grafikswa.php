<?php

class Grafikswa extends MX_Controller
{

	public $hak_akses;
	public $upt;
	public $level;
	public $user;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		// echo '<pre>';
		// print_r($_SESSION);die;
		$this->hak_akses = $this->session->userdata('hak_akses');
		$this->upt = $this->session->userdata('upt_id');
		$this->level = $this->session->userdata('level');
		$this->user = $this->session->userdata('user_id');
		$this->load->model('m_grafik', 'grafik');
	}

	public function getModuleName()
	{
		return 'grafikswa';
	}

	public function getHeaderJSandCSS()
	{
		$version  = str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz');
		$data = array(
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.css">',
			'<script src="' . base_url() . 'assets/js/controllers/grafikswa.js?v=' . $version . '"></script>',
		);

		return $data;
	}

	public function index($date = '')
	{
		$data = $_GET;
		$date = isset($data['year']) ? $data['year'] : date('Y');
		$data['year'] = $date;
		$data['view_file'] = 'index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Grafik Swa";
		$data['title_content'] = 'Grafik Swa';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['list_year'] = $this->getYear();
		$data['list_month'] = $this->getMonth();
		$data['list_upt'] = $this->getListUpt();
		$data['list_pemegang'] = array();
		if (isset($data['month']) && isset($data['upt'])) {
			if ($data['month'] != '' && $data['upt'] != '') {
				$data['list_pemegang'] = $this->grafik->getDataGrupPengajuan($data);
			}
		}

		$data['data_wp'] = $this->getDataWpGrafik($data);
		$data['date'] = $date;
		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getMonth()
	{
		$month = array(
			'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember',
		);

		return $month;
	}

	public function getListUpt()
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt',
			'where' => "deleted = 0 and id > 2"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getYear()
	{
		$year = date('Y');
		$data = array();
		for ($year = intval(date('Y')); $year < intval(date('Y')) + 3; $year++) {
			$data[$year] = $year;
		}

		return $data;
	}

	public function getDataWpGrafik($params)
	{
		// echo '<pre>';
		// print_r($params);
		// die;

		$data = $this->grafik->getData($params);


		// echo '<pre>';
		// print_r($data);
		// die;

		$result = [];
		foreach ($data as $key => $value) {
			array_push($result, array(
				'y' => $value['nama_swa'],
				'a' => $value['total'],
			));
		}

		// echo '<pre>';
		// print_r($result);
		// die;

		return json_encode($result);
	}
}
