<select class="form-control required" error="Pegawai" id="pegawai" style="width: 100%;">
 <option upt="" value="">Pilih Pegawai</option>
 <?php if (!empty($list_pegawai)) { ?>
  <?php foreach ($list_pegawai as $value) { ?>
   <?php $selected = '' ?>
   <?php if (isset($user)) { ?>
    <?php $selected = $user == $value['user_id'] ? 'selected' : '' ?>
   <?php } ?>
   <option upt="<?php echo $value['upt'] ?>" <?php echo $selected ?> value="<?php echo $value['user_id'] ?>"><?php echo $value['nip'] . ' - ' . $value['nama'] ?></option>
  <?php } ?>
 <?php } ?>
</select>