<?php

class Service extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function index() {
  echo 'service working permit';
 }
 
 public function getDetailUpt($upt_id) {
  $data = Modules::run('database/get', array(
              'table' => "upt",
              'where' => "id = '".$upt_id."'"
  ));
  
  $nama = "";
  
  if (!empty($data)) {
   $nama = $data->row_array()['nama'];
  }


  return $nama;
 }

 public function getDetailDataWp($no_wp = "") {
  $data = Modules::run('database/get', array(
              'table' => 'permit p',
              'field' => array('p.*', 'ph.email',
                  'ph.nama_pemohon', 'ph.perusahaan',
                  'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
                  'ph.alamat', 'ph.jabatan',
                  'pw.tgl_pekerjaan', 'pw.tgl_awal',
                  'pw.tgl_akhir', 'pw.work_place',
                  'pw.place', 'pw.lokasi_pekerjaan',
                  'pw.uraian_pekerjaan', 'ps.level',
                  'ps.keterangan as ket_approve',
                  'ps.status as status_wp',
                  'pw.is_need_sistem',
                  'pw.keterangan_need_sistem'
              ),
              'join' => array(
                  array('pemohon ph', 'ph.id = p.pemohon'),
                  array('permit_work pw', 'pw.permit = p.id'),
                  array('(select max(id) id, permit from permit_status group by permit) pss', 'p.id = pss.permit', 'left'),
                  array('permit_status ps', 'ps.id = pss.id', 'left'),
              ),
              'where' => "p.no_wp = '" . $no_wp . "'"
  ));

  $data = $data->row_array();
  switch ($data['work_place']) {
   case 1:
    $data_place = Modules::run('wpeksternal/getDetailUnit', $data['place']);
    break;
   case 2:
    $data_place = Modules::run('wpeksternal/getDetailSubUnit', $data['place']);
    break;
   case 3:
    $data_place = Modules::run('wpeksternal/getDetailGardu', $data['place']);
    break;
   case 4:
    $data_place = Modules::run('wpeksternal/getDetailSutet', $data['place']);
    break;

   default:
    break;
  }
  $data['nama_place'] = $data_place['nama'];
  $data['upt_id'] = $data_place['upt_id'];
  $data['nama_upt_tujuan'] = $this->getDetailUpt($data['upt_id']);
//  echo '<pre>';
//  print_r($data_place);die;
  return $data;
 }

 public function getDetailWp($no_wp = "") {
  $data = $this->getDetailDataWp($no_wp);
  echo json_encode(array('data' => $data));
 }

}
