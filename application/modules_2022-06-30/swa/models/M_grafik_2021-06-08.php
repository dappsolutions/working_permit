
<?php

class M_grafik extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataMonthString($month)
	{
		$str = "";
		switch ($month) {
			case '01':
				$str = "Jan";
				break;
			case '02':
				$str = "Feb";
				break;
			case '03':
				$str = "Mar";
				break;
			case '04':
				$str = "Apr";
				break;
			case '05':
				$str = "Mei";
				break;
			case '06':
				$str = "Jun";
				break;
			case '07':
				$str = "Jul";
				break;
			case '08':
				$str = "Ags";
				break;
			case '09':
				$str = "Sep";
				break;
			case '10':
				$str = "Okt";
				break;
			case '11':
				$str = "Nov";
				break;
			case '12':
				$str = "Des";
				break;

			default:
				# code...
				break;
		}

		return $str;
	}


	public function getData($params)
	{
		$result = array();
		for ($i = 1; $i < 13; $i++) {
			$month = $i < 10 ? '0' . $i : $i;
			$date = $params['year'] . '-' . $month;
			$sql = "select 
			count(*) as total
			from permit_has_swa phs
			join `user` u  
				on u.id = phs.createdby
			join pegawai p 
				on p.id = u.pegawai  
			where phs.createddate like '%" . $date . "%' and phs.deleted = 0
			and p.upt = " . $params['data_user']['upt'];

			// echo '<pre>';
			// echo $sql;
			// die;
			$data = $this->db->query($sql);
			$push = array();
			$monthstr = $this->getDataMonthString($month);
			$push['month'] = $monthstr;
			$push['total'] = $data->row_array()['total'];
			array_push($result, $push);
		}

		return $result;
	}
}
