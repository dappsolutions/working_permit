
<?php

class M_grafik extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataMonthString($month)
	{
		$str = "";
		switch ($month) {
			case 'Januari':
				$str = "01";
				break;
			case 'Februari':
				$str = "02";
				break;
			case 'Maret':
				$str = "03";
				break;
			case 'April':
				$str = "04";
				break;
			case 'Mei':
				$str = "05";
				break;
			case 'Juni':
				$str = "06";
				break;
			case 'Juli':
				$str = "07";
				break;
			case 'Agustus':
				$str = "08";
				break;
			case 'September':
				$str = "09";
				break;
			case 'Oktober':
				$str = "10";
				break;
			case 'November':
				$str = "11";
				break;
			case 'Desember':
				$str = "12";
				break;

			default:
				# code...
				break;
		}

		return $str;
	}


	public function getDataGrupPengajuan($params)
	{

		// echo '<pre>';
		// print_r($params);
		// die;
		$month = $this->getDataMonthString($params['month']);
		// $month = $params['month'] < 10 ? '0' . $params['month'] : $params['month'];
		// echo $month;
		// die;

		$filter_upt = "";
		if ($params['upt'] != '9') {
			$filter_upt = "and p.upt = " . $params['upt'] . "";
		}

		$sql = "
		select 
		distinct
		phs.nama_swa 
		, p.id as pegawai
		, p.nip
		from permit_has_swa phs
		join `user` u  
			on u.id = phs.createdby
		join pegawai p 
			on p.id = u.pegawai  
		where phs.createddate like '%" . $params['year'] . "-" . $month . "%' and phs.deleted = 0
		" . $filter_upt;

		// echo '<pre>';
		// echo $sql;
		// die;

		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getData($params)
	{
		$data_group_pengajuan = $this->getDataGrupPengajuan($params);
		$params['month'] = $this->getDataMonthString($params['month']);
		// echo '<pre>';
		// print_r($data_group_pengajuan);
		// die;
		$filter_upt = "";
		if ($params['upt'] != '9') {
			$filter_upt = "and p.upt = " . $params['upt'] . "";
		}

		$result = array();
		foreach ($data_group_pengajuan as $key => $value) {
			$month = $params['month'];
			$date = $params['year'] . '-' . $month;
			$sql = "select 
			count(*) as total
			from permit_has_swa phs
			join `user` u  
				on u.id = phs.createdby
			join pegawai p 
				on p.id = u.pegawai  
			where phs.createddate like '%" . $date . "%' and phs.deleted = 0
			and p.id = " . $value['pegawai'] . "
			" . $filter_upt;

			$data = $this->db->query($sql);
			$push = array();
			$push['month'] = substr($value['nama_swa'], 0, 7);
			$push['total'] = $data->row_array()['total'];
			array_push($result, $push);
		}
		// for ($i = 1; $i < 13; $i++) {
		// 	$month = $i < 10 ? '0' . $i : $i;
		// 	$date = $params['year'] . '-' . $month;
		// 	$sql = "select 
		// 	count(*) as total
		// 	from permit_has_swa phs
		// 	join `user` u  
		// 		on u.id = phs.createdby
		// 	join pegawai p 
		// 		on p.id = u.pegawai  
		// 	where phs.createddate like '%" . $date . "%' and phs.deleted = 0
		// 	and p.upt = " . $params['data_user']['upt'];

		// 	// echo '<pre>';
		// 	// echo $sql;
		// 	// die;
		// 	$data = $this->db->query($sql);
		// 	$push = array();
		// 	$monthstr = $this->getDataMonthString($month);
		// 	$push['month'] = $monthstr;
		// 	$push['total'] = $data->row_array()['total'];
		// 	array_push($result, $push);
		// }

		return $result;
	}

	public function getListUpt()
	{
		$sql = "select * from upt where deleted = 0";
		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}

			$push = array();
			$push['id'] = $value['id'] + 1;
			$push['nama'] = 'SEMUA UPT';
			$push['createddate'] = date('Y-m-d H:i:s');
			$push['createdby'] = null;
			$push['updateddate'] = null;
			$push['updatedby'] = null;
			$push['deleted'] = '0';
			array_push($result, $push);
		}

		return $result;
	}

	public function getDataGrupPengajuanTopTen($params)
	{

		$sql = "
		select 
		distinct
		phs.nama_swa 
		, p.id as pegawai
		, p.nip
		from permit_has_swa phs
		join `user` u  
			on u.id = phs.createdby
		join pegawai p 
			on p.id = u.pegawai  
		where phs.createddate like '%" . $params['year'] . "-" . $params['month'] . "%' and phs.deleted = 0
		and p.upt = " . $params['data_user']['upt'] . "
		limit 10";

		// echo '<pre>';
		// echo $sql;
		// die;

		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataTopTen($params)
	{
		$data_group_pengajuan = $this->getDataGrupPengajuanTopTen($params);

		$result = array();
		foreach ($data_group_pengajuan as $key => $value) {
			$date = $params['year'] . "-" . $params['month'];
			$sql = "select 
			count(*) as total
			from permit_has_swa phs
			join `user` u  
				on u.id = phs.createdby
			join pegawai p 
				on p.id = u.pegawai  
			where phs.createddate like '%" . $date . "%' and phs.deleted = 0
			and p.id = " . $value['pegawai'] . "
			and p.upt = " . $params['data_user']['upt'] . "
			";

			$data = $this->db->query($sql);
			$push = array();
			$push['month'] = substr($value['nama_swa'], 0, 7);
			$push['total'] = $data->row_array()['total'];
			array_push($result, $push);
		}
		return $result;
	}
}
