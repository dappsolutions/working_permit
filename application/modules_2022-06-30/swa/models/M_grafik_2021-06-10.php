
<?php

class M_grafik extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDataMonthString($month)
	{
		$str = "";
		switch ($month) {
			case '01':
				$str = "Jan";
				break;
			case '02':
				$str = "Feb";
				break;
			case '03':
				$str = "Mar";
				break;
			case '04':
				$str = "Apr";
				break;
			case '05':
				$str = "Mei";
				break;
			case '06':
				$str = "Jun";
				break;
			case '07':
				$str = "Jul";
				break;
			case '08':
				$str = "Ags";
				break;
			case '09':
				$str = "Sep";
				break;
			case '10':
				$str = "Okt";
				break;
			case '11':
				$str = "Nov";
				break;
			case '12':
				$str = "Des";
				break;

			default:
				# code...
				break;
		}

		return $str;
	}


	public function getDataGrupPengajuan($params)
	{
		$sql = "
		select 
		distinct
		phs.nama_swa 
		, p.id as pegawai
		, p.nip
		from permit_has_swa phs
		join `user` u  
			on u.id = phs.createdby
		join pegawai p 
			on p.id = u.pegawai  
		where phs.createddate like '%" . $params['year'] . "%' and phs.deleted = 0
		and p.upt = " . $params['data_user']['upt'] . "";

		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getData($params)
	{
		$data_group_pengajuan = $this->getDataGrupPengajuan($params);
		// echo '<pre>';
		// print_r($data_group_pengajuan);
		// die;
		$result = array();
		foreach ($data_group_pengajuan as $key => $value) {
			$date = $params['year'];
			$sql = "select 
			count(*) as total
			from permit_has_swa phs
			join `user` u  
				on u.id = phs.createdby
			join pegawai p 
				on p.id = u.pegawai  
			where phs.createddate like '%" . $date . "%' and phs.deleted = 0
			and p.id = " . $value['pegawai'] . "
			and p.upt = " . $params['data_user']['upt'];

			$data = $this->db->query($sql);
			$push = array();
			$push['month'] = substr($value['nama_swa'], 0, 7);
			$push['total'] = $data->row_array()['total'];
			array_push($result, $push);
		}
		// for ($i = 1; $i < 13; $i++) {
		// 	$month = $i < 10 ? '0' . $i : $i;
		// 	$date = $params['year'] . '-' . $month;
		// 	$sql = "select 
		// 	count(*) as total
		// 	from permit_has_swa phs
		// 	join `user` u  
		// 		on u.id = phs.createdby
		// 	join pegawai p 
		// 		on p.id = u.pegawai  
		// 	where phs.createddate like '%" . $date . "%' and phs.deleted = 0
		// 	and p.upt = " . $params['data_user']['upt'];

		// 	// echo '<pre>';
		// 	// echo $sql;
		// 	// die;
		// 	$data = $this->db->query($sql);
		// 	$push = array();
		// 	$monthstr = $this->getDataMonthString($month);
		// 	$push['month'] = $monthstr;
		// 	$push['total'] = $data->row_array()['total'];
		// 	array_push($result, $push);
		// }

		return $result;
	}

	public function getListUpt()
	{
		$sql = "select * from upt where deleted = 0";
		$result = array();
		$data = $this->db->query($sql);
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {

				array_push($result, $value);
			}

			$push = array();
			$push['id'] = $value['id'] + 1;
			$push['nama'] = 'SEMUA UPT';
			$push['createddate'] = date('Y-m-d H:i:s');
			$push['createdby'] = null;
			$push['updateddate'] = null;
			$push['updatedby'] = null;
			$push['deleted'] = '0';
			array_push($result, $push);
		}

		return $result;
	}
}
