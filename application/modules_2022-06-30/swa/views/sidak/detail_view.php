<div class="row">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<?php if (!empty($data_swa)) { ?>
					<?php $no = 1; ?>
					<?php foreach ($data_swa as $ktab => $val_tab) { ?>
						<?php if ($no == 1) { ?>
							<li class="active"><a href="#form-<?php echo $no ?>" data-toggle="tab"><label class="label label-warning"><?php echo $no ?></label>&nbsp;<?php echo $val_tab['no_trans'] ?></a></li>
						<?php } else { ?>
							<li class=""><a href="#form-<?php echo $no ?>" data-toggle="tab"><label class="label label-warning"><?php echo $no ?></label>&nbsp;<?php echo $val_tab['no_trans'] ?></a></li>
						<?php } ?>

						<?php $no += 1 ?>
					<?php } ?>
				<?php } ?>
			</ul>
			<div class="tab-content">
				<!-- /.tab-pane -->
				<?php if (!empty($data_swa)) { ?>
					<?php $no = 1; ?>
					<?php foreach ($data_swa as $key => $value) { ?>
						<?php $active = $no == 1 ? 'active' : '' ?>
						<div class="tab-pane <?php echo $active ?>" id="form-<?php echo $no++ ?>">
							<div class="row">
								<div class="col-md-12">
									<a class="btn btn-danger" href="<?php echo base_url() ?>swa/sidak/cetak?permit=<?php echo $permit ?>&no_wp=<?php echo $no_wp ?>&ptlid=<?php echo $value['no_trans'] ?>">CETAK PDF</a>
								</div>
							</div>
							<br>


							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h5>Data Pekerjaan</h5>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<?php if (isset($data_pekerjaan[$key])) { ?>
														<?php if (!empty($data_pekerjaan[$key])) { ?>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Pelaksanaan Pekerjaan dihentikan sementara pada jam</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['jam_pekerjaan_dihentikan'] == '' ? '-' : $data_pekerjaan[$key]['jam_pekerjaan_dihentikan'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Pekerjaan Dilanjutkan Kembali pada jam</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['jam_pekerjaan_dilanjutkan'] == '' ? '-' : $data_pekerjaan[$key]['jam_pekerjaan_dilanjutkan'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Keterangan Penghentian Pekerjaan</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['ket_pekerjaan_dihentikan'] == '' ? '-' : $data_pekerjaan[$key]['ket_pekerjaan_dihentikan'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Temuan 1</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['temuan_1'] == '' ? '-' : $data_pekerjaan[$key]['temuan_1'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Jenis Temuan 1</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['jenis_temuan_1'] == '' ? '-' : $data_pekerjaan[$key]['jenis_temuan_1'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Temuan 2</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['temuan_2'] == '' ? '-' : $data_pekerjaan[$key]['temuan_2'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Jenis Temuan 2</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['jenis_temuan_2'] == '' ? '-' : $data_pekerjaan[$key]['jenis_temuan_2'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Rekomendasi</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['rekomendasi'] == '' ? '-' : $data_pekerjaan[$key]['rekomendasi'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Nama Pengawas K3 / Pengawas Pekerjaan</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['nama_pengawas_k3'] == '' ? '-' : $data_pekerjaan[$key]['nama_pengawas_k3'] ?></label>
																</div>
																<div class="col-md-2">
																	<label for="">Nama PT / Perusahaan</label>
																</div>
																<div class="col-md-4">
																	<label for="" class="label label-primary"><?php echo $data_pekerjaan[$key]['nama_perusahaan'] == '' ? '-' : $data_pekerjaan[$key]['nama_perusahaan'] ?></label>
																</div>
															</div>
															<br>
															<div class="row">
																<div class="col-md-2">
																	<label for="">Tanda Tangan Pengawas K3 / Pengawas Pekerjaan</label>
																</div>
																<div class="col-md-4">
																	<?php if (trim($data_pekerjaan[$key]['ttd_k3']) != '') { ?>
																		<img src="data:image/jpeg;base64,<?php echo $data_pekerjaan[$key]['ttd_k3'] ?>" alt="" width="100" height="100">
																	<?php } ?>
																</div>
																<div class="col-md-2">
																	<label for="">Tanda Tangan Pemegang SWA</label>
																</div>
																<div class="col-md-4">
																	<?php if (trim($data_pekerjaan[$key]['ttd_swa']) != '') { ?>
																		<img src="data:image/jpeg;base64,<?php echo $data_pekerjaan[$key]['ttd_swa'] ?>" alt="" width="100" height="100">
																	<?php } ?>
																</div>
															</div>
														<?php } ?>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h5>Data SWA</h5>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<?php if (!empty($data_swa[$key])) { ?>
														<div class="row">
															<div class="col-md-2">
																<label for="">Nama SWA</label>
															</div>
															<div class="col-md-4">
																<label for="" class="label label-primary"><?php echo $data_swa[$key]['nama_swa'] == '' ? '-' : $data_swa[$key]['nama_swa'] ?></label>
															</div>
															<div class="col-md-2">
																<label for="">Email SWA</label>
															</div>
															<div class="col-md-4">
																<label for="" class="label label-primary"><?php echo $data_swa[$key]['email_swa'] == '' ? '-' : $data_swa[$key]['email_swa'] ?></label>
															</div>
														</div>
														<br>
														<div class="row">
															<div class="col-md-2">
																<label for="">Lokasi Pekerjaan</label>
															</div>
															<div class="col-md-4">
																<label for="" class="label label-primary"><?php echo $data_swa[$key]['lokasi_pekerjaan'] == '' ? '-' : $data_swa[$key]['lokasi_pekerjaan'] ?></label>
															</div>
															<div class="col-md-2">
																<label for="">Uraian Pekerjaan</label>
															</div>
															<div class="col-md-4">
																<label for="" class="label label-primary"><?php echo $data_swa[$key]['uraian_pekerjaan'] == '' ? '-' : $data_swa[$key]['uraian_pekerjaan'] ?></label>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h5>Kelengkapan</h5>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<?php if (!empty($data_apd[$key])) { ?>
														<div class="row">
															<div class="col-md-2">
																<label for="">Working Permit</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_apd[$key]['wp'] == 'yes' ? 'checked' : '' ?>>
															</div>
															<div class="col-md-2">
																<label for="">SOP/IK</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_apd[$key]['sop'] == 'yes' ? 'checked' : '' ?>>
															</div>
														</div>
														<br>
														<div class="row">
															<div class="col-md-2">
																<label for="">Job Safety Analisis</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_apd[$key]['jsa'] == 'yes' ? 'checked' : '' ?>>
															</div>
															<div class="col-md-2">
																<label for="">Pengawas K3 & Pengawas Pekerjaan yang Berkompeten</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_apd[$key]['pengawas_k3'] == 'yes' ? 'checked' : '' ?>>
															</div>
														</div>
														<br>
														<div class="row">
															<div class="col-md-2">
																<label for="">Pekerja Memiliki Sertifikat Kompetensi</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_apd[$key]['sertifikat_kom'] == 'yes' ? 'checked' : '' ?>>
															</div>
															<div class="col-md-2">
																<label for="">Peralatan Kerja yang Layak dan \nSesuai dengan Jenis Pekerjaan</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_apd[$key]['peralatan'] == 'yes' ? 'checked' : '' ?>>
															</div>
														</div>
														<br>
														<div class="row">
															<div class="col-md-2">
																<label for="">Rambu - rambu K3 dan LOTO</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_swa[$key]['rambu_k3'] == 'yes' ? 'checked' : '' ?>>
															</div>
															<div class="col-md-2">
																<label for="">Alat Pelindung Diri (APD) yang Layak dan \nTersedia</label>
															</div>
															<div class="col-md-4">
																<input type="checkbox" <?php echo $data_swa[$key]['apd'] == 'yes' ? 'checked' : '' ?>>
															</div>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h5>Gambar Sidak</h5>
										</div>
										<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<?php if (isset($value['data_image'])) { ?>
														<?php if (!empty($value['data_image'])) { ?>
															<?php foreach ($value['data_image'] as  $v_image) { ?>
																<div class="row">
																	<div class="col-md-6">
																		<br>
																		<img src="data:image/jpeg;base64,<?php echo $v_image['picture'] ?>" alt="">
																	</div>
																</div>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
		</div>
	</div>
</div>