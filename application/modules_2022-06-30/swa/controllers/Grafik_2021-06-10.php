<?php

class Grafik extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_grafik', 'grafik');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'swa/sidak';
	}

	public function index()
	{
		echo 'grafik ' . date('Y-m-d H:i:s');
	}

	public function getData()
	{
		$data = $_POST;
		// $data['user_id'] = 940;
		// $data['year'] = 2021;
		$data['data_user'] = Modules::run('simson/user/getUser', $data['user_id']);
		$datagrafik = $this->grafik->getData($data);

		$result['data'] = $datagrafik;
		Modules::run('simson/output/get', $result);
	}

	public function getListUpt()
	{
		$data = $this->grafik->getListUpt();

		$result['data'] = $data;
		Modules::run('simson/output/get', $result);
	}
}
