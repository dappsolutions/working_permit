<?php
// ini_set("memory_limit",-1);
class Document extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_document', 'doc');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'apar/document';
	}

	public function index()
	{
		echo 'document ' . date('Y-m-d H:i:s');
	}

	public function getData()
	{
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		$data = $this->doc->getData($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
}
