<?php
// ini_set("memory_limit",-1);
class Apps extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_apps', 'apps');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'apar/apps';
	}

	public function index()
	{
		echo 'apps ' . date('Y-m-d H:i:s');
	}

	public function getAppVersion(){
		$data = [];
		$data = $this->apps->getAppVersion($data);
		$result['data'] = $data;
		echo json_encode($result);
	}
	
}
