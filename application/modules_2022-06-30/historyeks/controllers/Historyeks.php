<?php

class Historyeks extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;
	public $akses;
	public $level;
	public $user;
	public $upt;
	public $tgl_ibppr_baru;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
		$this->akses = $this->session->userdata('hak_akses');
		$this->level = $this->session->userdata('level');
		$this->user = $this->session->userdata('user_id');
		$this->upt = $this->session->userdata('upt');
		$this->tgl_ibppr_baru = 20200524;
	}

	public function getModuleName()
	{
		return 'historyeks';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/wpeksternal_v1-5.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/historyeks_v1.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'permit';
	}

	public function getRootModule()
	{
		return "History Permohonan";
	}

	public function index()
	{
		//  echo '<pre>';
		//  print_r($_SESSION);die;
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = $this->getRootModule() . " - Eksternal";
		$data['title_content'] = 'Eksternal';
		$content = $this->getDataHistoryeks();

		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		$data['list_unit'] = $this->getListUnit();
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['upt'] = $this->upt;

		if ($this->upt != '2') {
			$data['list_gardu'] = $this->getListGarduByUpt();
			//   echo '<pre>';
			//   print_r($data['list_gardu']);die;
		}
		echo Modules::run('template', $data);
	}

	public function getTotalDataHistoryeks($keyword = '', $is_upt = false)
	{
		$like = array();
		if ($keyword != '' && !$is_upt) {
			$like = array(
				array('p.no_wp', $keyword),
				array('ph.nama_pemohon', $keyword),
				array('ph.perusahaan', $keyword),
				array('pw.lokasi_pekerjaan', $keyword),
				array('wp.jenis', $keyword),
				array('pst.status', $keyword),
				array('ut.nama', $keyword),
			);
		}

		$where_upt = $is_upt == true ? "and pp.upt = '" . $keyword . "'" : "";
		$where = "p.deleted = 0 and p.tipe_permit = 2 and pst.status != 'DRAFT' " . $where_upt;
		switch ($this->akses) {
			case "approval":
				$upt = $_SESSION['upt'];
				$where = "p.deleted = 0 and p.tipe_permit = 2 and pp.upt = '" . $upt . "' "
					. "and pst.status != 'DRAFT'";

				if ($this->level == "1" && $this->upt == "2") {
					$where = "p.deleted = 0 and p.tipe_permit = 2 and pst.status != 'DRAFT' " . $where_upt;
				}
				break;
			case "vendor":
				$where = "p.deleted = 0 and p.tipe_permit = 2 "
					. "and pst.status != 'DRAFT' and ps.user = '" . $this->user . "' " . $where_upt;
				break;
			default:
				break;
		}

		switch ($keyword) {
			case "":
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' p',
					'field' => array(
						'p.*', 'ph.email',
						'ph.nama_pemohon', 'ph.perusahaan',
						'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
						'ph.alamat', 'ph.jabatan',
						'pw.tgl_pekerjaan', 'pw.tgl_awal',
						'pw.tgl_akhir', 'pw.work_place',
						'pw.place', 'pw.lokasi_pekerjaan',
						'pw.uraian_pekerjaan',
						'wp.jenis as jenis_place',
						'pst.status', 'ut.nama as nama_upt'
					),
					'join' => array(
						array('pemohon ph', 'ph.id = p.pemohon'),
						array('permit_work pw', 'pw.permit = p.id'),
						array('work_place wp', 'pw.work_place = wp.id'),
						array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
						array('permit_status ps', 'pss.id = ps.id'),
						array('permit_purpose pp', 'p.id = pp.permit'),
						array('upt ut', 'ut.id = pp.upt'),
						array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
						array('permit_status pst', 'pss_s.id = pst.id'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
						array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
						array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
						array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
					),
					'is_or_like' => true,
					'like' => $like,
					'where' => $where
				));
				break;
			default:
				if (!$is_upt) {
					$total = Modules::run('database/count_all', array(
						'table' => $this->getTableName() . ' p',
						'field' => array(
							'p.*', 'ph.email',
							'ph.nama_pemohon', 'ph.perusahaan',
							'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
							'ph.alamat', 'ph.jabatan',
							'pw.tgl_pekerjaan', 'pw.tgl_awal',
							'pw.tgl_akhir', 'pw.work_place',
							'pw.place', 'pw.lokasi_pekerjaan',
							'pw.uraian_pekerjaan',
							'wp.jenis as jenis_place',
							'pst.status', 'ut.nama as nama_upt'
						),
						'join' => array(
							array('pemohon ph', 'ph.id = p.pemohon'),
							array('permit_work pw', 'pw.permit = p.id'),
							array('work_place wp', 'pw.work_place = wp.id'),
							array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
							array('permit_status ps', 'pss.id = ps.id'),
							array('permit_purpose pp', 'p.id = pp.permit'),
							array('upt ut', 'ut.id = pp.upt'),
							array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
							array('permit_status pst', 'pss_s.id = pst.id'),
							array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
							array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
							array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
							array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
						),
						'is_or_like' => true,
						'like' => $like,
						'inside_brackets' => true,
						'where' => $where
					));
				} else {
					$total = Modules::run('database/count_all', array(
						'table' => $this->getTableName() . ' p',
						'field' => array(
							'p.*', 'ph.email',
							'ph.nama_pemohon', 'ph.perusahaan',
							'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
							'ph.alamat', 'ph.jabatan',
							'pw.tgl_pekerjaan', 'pw.tgl_awal',
							'pw.tgl_akhir', 'pw.work_place',
							'pw.place', 'pw.lokasi_pekerjaan',
							'pw.uraian_pekerjaan',
							'wp.jenis as jenis_place',
							'pst.status', 'ut.nama as nama_upt'
						),
						'join' => array(
							array('pemohon ph', 'ph.id = p.pemohon'),
							array('permit_work pw', 'pw.permit = p.id'),
							array('work_place wp', 'pw.work_place = wp.id'),
							array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
							array('permit_status ps', 'pss.id = ps.id'),
							array('permit_purpose pp', 'p.id = pp.permit'),
							array('upt ut', 'ut.id = pp.upt'),
							array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
							array('permit_status pst', 'pss_s.id = pst.id'),
							array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
							array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
							array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
							array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
						),
						'is_or_like' => true,
						'like' => $like,
						'where' => $where
					));
				}
				break;
		}

		return $total;
	}

	public function getDataHistoryeks($keyword = '', $is_upt = false, $is_gardu = false)
	{
		$like = array();
		if ($keyword != '' && !$is_upt && !$is_gardu) {
			$like = array(
				array('p.no_wp', $keyword),
				array('ph.nama_pemohon', $keyword),
				array('ph.perusahaan', $keyword),
				array('pw.lokasi_pekerjaan', $keyword),
				array('wp.jenis', $keyword),
				array('pst.status', $keyword),
				array('ut.nama', $keyword),
			);
		}


		$where_upt = $is_upt == true ? "and pp.upt = '" . $keyword . "'" : "";
		$where = "p.deleted = 0 and p.tipe_permit = 2 and pst.status != 'DRAFT' " . $where_upt;
		switch ($this->akses) {
			case "approval":
				$upt = $_SESSION['upt'];
				$where = "p.deleted = 0 and p.tipe_permit = 2 and pp.upt = '" . $upt . "' "
					. "and pst.status != 'DRAFT'";

				if ($this->level == "1" && $this->upt == "2") {
					$where = "p.deleted = 0 and p.tipe_permit = 2 and pst.status != 'DRAFT' " . $where_upt;
				}
				break;
			case "vendor":
				$where = "p.deleted = 0 and p.tipe_permit = 2 "
					. "and pst.status != 'DRAFT' and ps.user = '" . $this->user . "' " . $where_upt;
				break;
			default:
				break;
		}

		//  echo $where;die;

		$join = array(
			array('pemohon ph', 'ph.id = p.pemohon'),
			array('permit_work pw', 'pw.permit = p.id'),
			array('work_place wp', 'pw.work_place = wp.id'),
			array("(select id, permit from permit_status where status = 'DRAFT' group by permit) pss", 'p.id = pss.permit'),
			array('permit_status ps', 'pss.id = ps.id'),
			array('permit_purpose pp', 'p.id = pp.permit'),
			array('upt ut', 'ut.id = pp.upt'),
			array("(select max(id) id, permit from permit_status group by permit) pss_s", 'p.id = pss_s.permit'),
			array('permit_status pst', 'pss_s.id = pst.id'),
			array('(select max(id) id, permit from permit_transaction_log where group_transaction = 1  group by permit) ptl_max', 'p.id = ptl_max.permit', 'left'),
			array('permit_transaction_log ptl', 'ptl.id = ptl_max.id', 'left'),
			array('(select max(id) id, permit from permit_transaction_log where group_transaction = 2  group by permit) ptl_max_swa', 'p.id = ptl_max_swa.permit', 'left'),
			array('permit_transaction_log ptl_swa', 'ptl_swa.id = ptl_max_swa.id', 'left'),
			array('user usr_ven', 'p.user = usr_ven.id', 'left'),
			array('vendor vdn', 'vdn.id = usr_ven.vendor', 'left'),
		);
		switch ($keyword) {
			case "":
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' p',
					'field' => array(
						'p.*', 'ph.email',
						'ph.nama_pemohon', 'ph.perusahaan',
						'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
						'ph.alamat', 'ph.jabatan',
						'pw.tgl_pekerjaan', 'pw.tgl_awal',
						'pw.tgl_akhir', 'pw.work_place',
						'pw.place', 'pw.lokasi_pekerjaan',
						'pw.uraian_pekerjaan',
						'wp.jenis as jenis_place',
						'pst.status', 'ut.nama as nama_upt', 'pst.level',
						'ptl.no_trans as no_trans_simson',
						'ptl_swa.no_trans as no_trans_swa',
						'vdn.nama_vendor',
					),
					'join' => $join,
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'where' => $where,
					'orderby' => 'p.tanggal_wp desc'
				));
				break;
			default:
				if (!$is_upt && !$is_gardu) {
					$data = Modules::run('database/get', array(
						'table' => $this->getTableName() . ' p',
						'field' => array(
							'p.*', 'ph.email',
							'ph.nama_pemohon', 'ph.perusahaan',
							'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
							'ph.alamat', 'ph.jabatan',
							'pw.tgl_pekerjaan', 'pw.tgl_awal',
							'pw.tgl_akhir', 'pw.work_place',
							'pw.place', 'pw.lokasi_pekerjaan',
							'pw.uraian_pekerjaan',
							'wp.jenis as jenis_place',
							'pst.status', 'ut.nama as nama_upt', 'pst.level',
							'ptl.no_trans as no_trans_simson',
							'ptl_swa.no_trans as no_trans_swa',
							'vdn.nama_vendor',
						),
						'join' => $join,
						'like' => $like,
						'is_or_like' => true,
						'limit' => $this->limit,
						'offset' => $this->last_no,
						'inside_brackets' => true,
						'where' => $where,
						'orderby' => 'p.tanggal_wp desc'
					));
				} else {
					if ($is_upt) {
						$data = Modules::run('database/get', array(
							'table' => $this->getTableName() . ' p',
							'field' => array(
								'p.*', 'ph.email',
								'ph.nama_pemohon', 'ph.perusahaan',
								'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
								'ph.alamat', 'ph.jabatan',
								'pw.tgl_pekerjaan', 'pw.tgl_awal',
								'pw.tgl_akhir', 'pw.work_place',
								'pw.place', 'pw.lokasi_pekerjaan',
								'pw.uraian_pekerjaan',
								'wp.jenis as jenis_place',
								'pst.status', 'ut.nama as nama_upt', 'pst.level',
								'ptl.no_trans as no_trans_simson',
								'ptl_swa.no_trans as no_trans_swa',
								'vdn.nama_vendor',
							),
							'join' => $join,
							'like' => $like,
							'is_or_like' => true,
							'limit' => $this->limit,
							'offset' => $this->last_no,
							'where' => $where,
							'orderby' => 'p.tanggal_wp desc'
						));
					}

					//gardu induk
					if ($is_gardu) {
						$where .= " and pw.work_place = 3 and pw.place = '" . $keyword . "'";

						//      $join_gardu = array('gardu_induk gi', 'gi.id = pw.place');
						//      array_push($join, $join_gardu);
						//      echo '<pre>';
						//      print_r($join);die;
						//      echo $where;die;

						$data = Modules::run('database/get', array(
							'table' => $this->getTableName() . ' p',
							'field' => array(
								'p.*', 'ph.email',
								'ph.nama_pemohon', 'ph.perusahaan',
								'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
								'ph.alamat', 'ph.jabatan',
								'pw.tgl_pekerjaan', 'pw.tgl_awal',
								'pw.tgl_akhir', 'pw.work_place',
								'pw.place', 'pw.lokasi_pekerjaan',
								'pw.uraian_pekerjaan',
								'wp.jenis as jenis_place',
								'pst.status', 'ut.nama as nama_upt',
								'pst.level',
								'ptl.no_trans as no_trans_simson',
								'ptl_swa.no_trans as no_trans_swa',
								'vdn.nama_vendor',
							),
							'join' => $join,
							'like' => $like,
							'is_or_like' => true,
							'limit' => $this->limit,
							'offset' => $this->last_no,
							'where' => $where,
							'orderby' => 'p.tanggal_wp desc'
						));

						//      echo "<pre>";
						//      echo $this->db->last_query();
						//      die;
					}
				}
				break;
		}

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataHistoryeks($keyword, $is_upt)
		);
	}

	public function getDetailDataHistoryeks($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit p',
			'field' => array(
				'p.*', 'ph.email',
				'ph.nama_pemohon', 'ph.perusahaan',
				'ph.no_hp', 'ph.no_hp', 'ph.no_telp',
				'ph.alamat', 'ph.jabatan',
				'pw.tgl_pekerjaan', 'pw.tgl_awal',
				'pw.tgl_akhir', 'pw.work_place',
				'pw.place', 'pw.lokasi_pekerjaan',
				'pw.uraian_pekerjaan', 'ps.level', 'ps.keterangan as ket_approve'
			),
			'join' => array(
				array('pemohon ph', 'ph.id = p.pemohon'),
				array('permit_work pw', 'pw.permit = p.id'),
				array('(select max(id) id, permit from permit_status group by permit) pss', 'p.id = pss.permit'),
				array('permit_status ps', 'ps.id = pss.id'),
			),
			'where' => "p.id = '" . $id . "'"
		));

		$data = $data->row_array();
		switch ($data['work_place']) {
			case 1:
				$data_place = $this->getDetailUnit($data['place']);
				break;
			case 2:
				$data_place = $this->getDetailSubUnit($data['place']);
				break;
			case 3:
				$data_place = $this->getDetailGardu($data['place']);
				break;
			case 4:
				$data_place = $this->getDetailSutet($data['place']);
				break;

			default:
				break;
		}
		$data['nama_place'] = $data_place['nama'];
		$data['upt_id'] = $data_place['upt_id'];
		//  echo '<pre>';
		//  print_r($data_place);die;
		return $data;
	}

	public function getListWorkPlace()
	{
		$data = Modules::run('database/get', array(
			'table' => 'work_place',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPemohon()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pemohon p',
			'where' => "p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListAkibat()
	{
		$data = Modules::run('database/get', array(
			'table' => 'akibat',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPemaparan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pemaparan',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPeluang()
	{
		$data = Modules::run('database/get', array(
			'table' => 'peluang',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListTjPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_penanggung_jawab ppj',
			'where' => "ppj.deleted = 0 and ppj.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPelaksanaPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_pelaksana pp',
			'where' => "pp.deleted = 0 and pp.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListApdPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_apd pa',
			'where' => "pa.deleted = 0 and pa.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListJsaPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_jsa pa',
			'field' => array(
				'pa.*', 'pji.tahapan_pekerjaan',
				'pji.potensi_bahaya', 'pji.pengendalian', 'pji.permit_jsa'
			),
			'join' => array(
				array('permit_jsa_item pji', 'pji.permit_jsa = pa.id'),
			),
			'where' => "pa.deleted = 0 and pa.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				if (!in_array($value['id'], $temp)) {
					$detail = array();
					foreach ($data->result_array() as $v_item) {
						if ($value['id'] == $v_item['permit_jsa']) {
							array_push($detail, $v_item);
						}
					}
					$result[$value['id']] = $detail;
					$temp[] = $value['id'];
				}
			}
		}


		return $result;
	}

	public function getListPermitIbppr($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_ibppr pi',
			'where' => "pi.deleted = 0 and pi.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListStatusPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_status ps',
			'field' => array(
				'ps.*',
				'pg.nama as nama_pegawai',
				'vd.nama_vendor',
				'u.pegawai', 'u.vendor', 'u.username', 'pg.posisi_lengkap'
			),
			'join' => array(
				array('user u', 'ps.user = u.id'),
				array('pegawai pg', 'u.pegawai = pg.id', 'left'),
				array('vendor vd', 'u.vendor = vd.id', 'left'),
			),
			'where' => "ps.permit = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama_aktor'] = $value['username'];
				if ($value['vendor'] != '') {
					$value['nama_aktor'] = $value['nama_vendor'];
				}
				if ($value['pegawai'] != '') {
					$value['nama_aktor'] = $value['nama_pegawai'] . ' <br/> ' . $value['posisi_lengkap'];
				}
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function detail($id)
	{
		$data = $this->getDetailDataHistoryeks($id);
		$createddate = date('Ymd', strtotime($data['createddate']));
		//  echo '<pre>';
		//  print_r($data);die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Historyeks";
		$data['title_content'] = "Detail Historyeks";
		$data['tgl_ibppr_baru'] = intval($createddate) >= $this->tgl_ibppr_baru ? '1' : '0';
		$data['list_pemohon'] = $this->getListPemohon();
		$data['list_place'] = $this->getListWorkPlace();
		$data['list_tj'] = $this->getListTjPermit($id);
		$data['list_pelaksana'] = $this->getListPelaksanaPermit($id);
		$data['list_apd'] = $this->getListApdPermit($id);
		$data['list_jsa'] = $this->getListJsaPermit($id);
		$data['list_akibat'] = $this->getListAkibat();
		$data['list_paparan'] = $this->getListPemaparan();
		$data['list_peluang'] = $this->getListPeluang();
		$data['list_ibppr'] = $this->getListPermitIbppr($id);
		$data['status_approve'] = $this->getListStatusPermit($id);
		$data['list_reject'] = $this->getListStatusRejectPermit($id);
		$data['akses'] = $this->akses;
		echo $this->load->view('wpeksternal/detail_view', $data, true);
		//  echo Modules::run('template', $data);
	}


	public function getListStatusRejectPermit($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'permit_status ps',
			'field' => array(
				'ps.*',
				'pg.nama as nama_pegawai',
				'vd.nama_vendor',
				'u.pegawai', 'u.vendor', 'u.username', 'pg.posisi_lengkap'
			),
			'join' => array(
				array('user u', 'ps.user = u.id'),
				array('pegawai pg', 'u.pegawai = pg.id', 'left'),
				array('vendor vd', 'u.vendor = vd.id', 'left'),
			),
			'where' => "ps.permit = '" . $id . "' and ps.status = 'REJECTED'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama_aktor'] = $value['username'];
				if ($value['vendor'] != '') {
					$value['nama_aktor'] = $value['nama_vendor'];
				}
				if ($value['pegawai'] != '') {
					$value['nama_aktor'] = $value['nama_pegawai'] . ' <br/> ' . $value['posisi_lengkap'];
				}
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Historyeks";
		$data['title_content'] = 'Data Historyeks';
		$content = $this->getDataHistoryeks($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/' . $keyword . '/', $this->segment, $total_rows, $this->limit, $this->last_no);
		$data['list_unit'] = $this->getListUnit();
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['upt'] = $this->upt;
		echo Modules::run('template', $data);
	}

	public function searchByUpt($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['upt_id'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Historyeks";
		$data['title_content'] = 'Data Historyeks';
		$content = $this->getDataHistoryeks($keyword, true);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/searchByUpt/' . $keyword . '/', $this->segment, $total_rows, $this->limit, $this->last_no);
		$data['list_unit'] = $this->getListUnit();
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['upt'] = $this->upt;
		echo Modules::run('template', $data);
	}

	public function searchByGardu($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['gardu_id'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Historyeks";
		$data['title_content'] = 'Data Historyeks';
		$content = $this->getDataHistoryeks($keyword, false, true);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/searchByGardu/' . $keyword . '/', $this->segment, $total_rows, $this->limit, $this->last_no);
		$data['list_unit'] = $this->getListUnit();
		$data['hak_akses'] = $this->akses;
		$data['level'] = $this->level;
		$data['upt'] = $this->upt;
		if ($this->upt != '2') {
			$data['list_gardu'] = $this->getListGarduByUpt();
			//   echo '<pre>';
			//   print_r($data['list_gardu']);die;
		}
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function showLogo()
	{
		$foto = str_replace(' ', '_', $this->input->post('foto'));
		$data['foto'] = $foto;
		$data['jenis'] = $_POST['jenis'];
		echo $this->load->view('foto', $data, true);
	}

	public function getListUnit()
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt u',
			'field' => array('u.*'),
			'where' => "u.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['upt_id'] = $value['id'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailUnit($unit)
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt u',
			'field' => array('u.*'),
			'where' => "u.id = '" . $unit . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['id'];
		}


		return $result;
	}

	public function getListSubUnit()
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt su',
			'field' => array('su.*', 'u.nama as nama_upt'),
			'join' => array(
				array('upt u', 'su.upt = u.id')
			),
			'where' => "su.deleted = 0",
			'orderby' => 'u.nama asc'
		));

		$result = array();
		if (!empty($data)) {
			//   echo '<pre>';
			//   print_r($data->result_array());die;
			foreach ($data->result_array() as $value) {
				$value['upt_id'] = $value['upt'];
				$value['nama'] = $value['nama_upt'] . ' - ' . $value['nama'];
				array_push($result, $value);
			}
		}
		//  echo '<pre>';
		//  print_r($result);die;

		return $result;
	}

	public function getDetailSubUnit($sub_unit)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sub_upt su',
			'field' => array('su.*', 'u.nama as nama_upt'),
			'join' => array(
				array('upt u', 'su.upt = u.id')
			),
			'where' => "su.id = '" . $sub_unit . "'",
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['upt'];
			$result['nama'] = $result['nama_upt'] . ' - ' . $result['nama'];;
		}
		//  echo '<pre>';
		//  print_r($result);die;

		return $result;
	}

	public function getListGardu()
	{
		$data = Modules::run('database/get', array(
			'table' => 'gardu_induk gi',
			'field' => array('gi.*', 'ut.nama as nama_upt'),
			'join' => array(
				array('upt ut', 'gi.upt = ut.id')
			),
			'where' => "gi.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama'] = $value['nama_gardu'];
				$value['upt_id'] = $value['upt'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListGarduByUpt()
	{
		$data = Modules::run('database/get', array(
			'table' => 'gardu_induk gi',
			'field' => array('gi.*', 'ut.nama as nama_upt'),
			'join' => array(
				array('upt ut', 'gi.upt = ut.id')
			),
			'where' => "gi.deleted = 0 and ut.id = '" . $this->upt . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['nama'] = $value['nama_gardu'];
				$value['upt_id'] = $value['upt'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailGardu($gi)
	{
		$data = Modules::run('database/get', array(
			'table' => 'gardu_induk gi',
			'field' => array('gi.*', 'ut.nama as nama_upt'),
			'join' => array(
				array('upt ut', 'gi.upt = ut.id')
			),
			'where' => "gi.id = '" . $gi . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['nama'] = $result['nama_gardu'];
			$result['upt_id'] = $result['upt'];;
		}


		return $result;
	}

	public function getListSutet()
	{
		$data = Modules::run('database/get', array(
			'table' => 'sutet s',
			'field' => array('s.*', 'ut.nama as nama_upt', 'ut.id as upt'),
			'join' => array(
				array('gardu_induk gi', 'gi.id = s.gardu_induk'),
				array('upt ut', 'gi.upt = ut.id'),
			),
			'where' => "s.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['upt_id'] = $value['upt'];
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailSutet($sutet)
	{
		$data = Modules::run('database/get', array(
			'table' => 'sutet s',
			'field' => array('s.*', 'ut.nama as nama_upt', 'ut.id as upt'),
			'join' => array(
				array('gardu_induk gi', 'gi.id = s.gardu_induk'),
				array('upt ut', 'gi.upt = ut.id'),
			),
			'where' => "s.id = '" . $sutet . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$result['upt_id'] = $result['upt'];
		}


		return $result;
	}

	public function getDetailTempat()
	{
		$work_place = $_POST['work_place'];
		$data = array();
		switch ($work_place) {
			case 1:
				$data = $this->getListUnit();
				break;
			case 2:
				$data = $this->getListSubUnit();
				break;
			case 3:
				$data = $this->getListGardu();
				break;
			case 4:
				$data = $this->getListSutet();
				break;

			default:
				break;
		}

		$conten['list_wp'] = $data;
		echo $this->load->view('list_detail_tempat', $conten, true);
	}
}
