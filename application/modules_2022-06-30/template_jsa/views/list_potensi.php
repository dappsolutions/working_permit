<div class="row">
 <div class="col-md-12">
  <div class="panel panel-default">
   <div class="panel-heading">
    <h5>Daftar Potensi Bahaya</h5>
   </div>
   <div class="panel-body">
    <div class="row">
     <div class="col-md-12">
      <input type="text" class="form-control" placeholder="Cari Data" onkeyup="TemplateJsa.cariDataPotensi(this, event)">
     </div>
    </div>
    <hr>

    <div class="row">
     <div class="col-md-12">
      <table style="width: 100%;" id="list-data-potensi">
       <?php foreach ($data as $value) { ?>
        <tr>
         <td class="td_jsa">
          <?php echo $value['potensi'] ?>
         </td>
         <td class="td_jsa text-center">
          <label for="" jenis="auto" index_parent="<?php echo $index_parent ?>" index="<?php echo $index ?>" class="label label-success" onclick="TemplateJsa.pilihPotensi(this)">Pilih</label>
         </td>
        </tr>
       <?php } ?>
       <tr>
        <td class="td_jsa">
         <input type="text" class="form-control" id="manual" placeholder="Manual Input">
        </td>
        <td class="td_jsa text-center">
         <label for="" jenis="manual" index_parent="<?php echo $index_parent ?>" index="<?php echo $index ?>" class="label label-success" onclick="TemplateJsa.pilihPotensi(this)">Pilih</label>
        </td>
       </tr>
      </table>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>