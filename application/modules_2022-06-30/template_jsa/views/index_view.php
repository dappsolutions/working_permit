<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box padding-16">
					<!-- /.box-header -->
					<!-- form start -->
					<div class="box-body">
						<div class="divider"></div>
						<br />
						<?php if (isset($keyword)) { ?>
							<br />
							<div class="row">
								<div class="col-md-6">
									Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
								</div>
							</div>
						<?php } ?>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="" id="tb_content_jsa" style="width: 100%;">
										<thead>
											<tr class="">
												<th class="td_jsa">No</th>
												<th class="td_jsa">Tahapan Pekerjaan</th>
												<th class="td_jsa">Potensi Bahaya</th>
												<th class="td_jsa">Pengendalian</th>
												<th class="td_jsa"></th>
											</tr>
										</thead>
										<tbody>
											<?php if (!empty($content)) { ?>
												<?php $no = $pagination['last_no'] + 1; ?>
												<?php foreach ($content as $value) { ?>
													<?php $data_potensi = json_decode($value['potensi_bahaya']) ?>
													<?php $data_pengendalian = json_decode($value['pengendalian']) ?>
													<tr class="input" data_id="">
														<td class="td_jsa"><?php echo $no++ ?></td>
														<td class="td_jsa" id="tahapan_pekerjaan">
															<label for="" id="str_tahapan"><?php echo $value['tahapan_pekerjaan'] ?></label>
															<br>
															<a href="" onclick="TemplateJsa.chooseTahapanPekerjaan(this, event)">Pilih Tahapan Pekerjaan</a>
														</td>
														<td class="td_jsa text-center" id="potensi_bahaya">
															<table style="width: 100%;" id="table-potensi">
																<?php foreach ($data_potensi as $v_pot) { ?>
																	<tr data_id="">
																		<td class="td_jsa">
																			<?php echo $v_pot->potensi ?>
																		</td>
																		<td class="td_jsa text-center">
																			<i class="fa fa-trash" onclick="TemplateJsa.removePotensi(this)"></i>
																		</td>
																	</tr>
																<?php } ?>
																<tr data_id="">
																	<td class="td_jsa">
																		<a href="" onclick="TemplateJsa.choosePotensiBahaya(this, event)">Pilih Potensi Bahaya</a>
																	</td>
																	<td class="td_jsa text-center">
																		<i class="fa fa-plus" onclick="TemplateJsa.addPotensi(this)"></i>
																	</td>
																</tr>
															</table>
														</td>
														<td class="td_jsa text-center" id="pengendalian">
															<table style="width: 100%;" id="table-pengendalian">
																<?php foreach ($data_pengendalian as $v_pen) { ?>
																	<tr data_id="">
																		<td class="td_jsa">
																			<?php echo $v_pen->pengendalian ?>
																		</td>
																		<td class="td_jsa text-center" >
																			<i class="fa fa-trash" onclick="TemplateJsa.removePengendalian(this)"></i>
																		</td>
																	</tr>
																<?php } ?>
																<tr data_id="">
																	<td class="td_jsa">
																		<a href="" onclick="TemplateJsa.choosePengendalian(this, event)">Pilih Pengendalian</a>
																	</td>
																	<td class="td_jsa text-center">
																		<i class="fa fa-plus" onclick="TemplateJsa.addPengendalian(this)"></i>
																	</td>
																</tr>
															</table>
														</td>
														<td class="td_jsa text-center" id="action">
															<i class="fa fa-trash" onclick="TemplateJsa.removeItemTemplate(this)"></i>
														</td>
													</tr>
												<?php } ?>
											<?php } ?>

											<tr class="input" data_id="">
												<td class="td_jsa">-</td>
												<td class="td_jsa" id="tahapan_pekerjaan">
													<a href="" onclick="TemplateJsa.chooseTahapanPekerjaan(this, event)">Pilih Tahapan Pekerjaan</a>
												</td>
												<td class="td_jsa text-center" id="potensi_bahaya">
													<table style="width: 100%;" id="table-potensi">
														<tr data_id="">
															<td class="td_jsa">
																<a href="" onclick="TemplateJsa.choosePotensiBahaya(this, event)">Pilih Potensi Bahaya</a>
															</td>
															<td class="td_jsa text-center">
																<i class="fa fa-plus" onclick="TemplateJsa.addPotensi(this)"></i>
															</td>
														</tr>
													</table>
												</td>
												<td class="td_jsa text-center" id="pengendalian">
													<table style="width: 100%;" id="table-pengendalian">
														<tr data_id="">
															<td class="td_jsa">
																<a href="" onclick="TemplateJsa.choosePengendalian(this, event)">Pilih Pengendalian</a>
															</td>
															<td class="td_jsa text-center">
																<i class="fa fa-plus" onclick="TemplateJsa.addPengendalian(this)"></i>
															</td>
														</tr>
													</table>
												</td>
												<td class="td_jsa text-center" id="action">
													<i class="fa fa-plus" onclick="TemplateJsa.addItemTemplate(this)"></i>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>

					<div class="box-footer clearfix">
						<ul class="pagination pagination-sm no-margin pull-right">
							<?php echo $pagination['links'] ?>
						</ul>
					</div>
				</div>

				<a href="#" class="float" onclick="TemplateJsa.simpan(this, event)">
					<i class="fa fa-check my-float fa-lg"></i>
				</a>
			</div>
		</div>
	</div>
</div>


<style>
	.td_jsa {
		border: 1px solid black;
		font-family: tahoma;
		font-size: 12px;
		text-align: center;
		padding: 8px;
	}
</style>