<?php

class Template_jsa extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;
	public $akses;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 25;
		$this->akses = $this->session->userdata('hak_akses');
	}

	public function getModuleName()
	{
		return 'template_jsa';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/template_jsa.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'permit_jsa_item_template';
	}

	public function getRootModule()
	{
		return "Master";
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = $this->getRootModule() . " - Template JSA";
		$data['title_content'] = 'Template JSA';
		$content = $this->getData();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('t.pengedalian', $keyword),
			);
		}

		$where = "doc.deleted = 0";
		if ($this->akses == 'Superadmin') {
			$where = "doc.deleted = 0";
		}

		switch ($keyword) {
			case "":
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' t',
					'field' => array('t.*', 't.document'),
					'join' => array(
						array('document doc', 'doc.id = t.document')
					),
					'is_or_like' => true,
					'like' => $like,
					'where' => $where
				));
				break;
			default:
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' t',
					'field' => array('t.*', 't.document'),
					'join' => array(
						array('document doc', 'doc.id = t.document')
					),
					'is_or_like' => true,
					'like' => $like,
					'inside_brackets' => true,
					'where' => $where
				));
				break;
		}

		return $total;
	}

	public function getData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('t.pengedalian', $keyword),
			);
		}

		$where = "doc.deleted = 0";
		if ($this->akses == 'superadmin') {
			$where = "doc.deleted = 0";
		}


		switch ($keyword) {
			case "":
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' t',
					'field' => array('t.*', 't.document'),
					'join' => array(
						array('document doc', 'doc.id = t.document')
					),
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'where' => $where
				));
				break;
			default:
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' t',
					'field' => array('t.*', 't.document'),
					'join' => array(
						array('document doc', 'doc.id = t.document')
					),
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'inside_brackets' => true,
					'where' => $where
				));
				break;
		}

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				// echo '<pre>';
				// print_r($value['pengendalian']);die;
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalData($keyword)
		);
	}

	public function getDetailData($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' t',
			'field' => array('t.*'),
			'where' => "t.id = '" . $id . "'"
		));

		$data = $data->row_array();
		return $data;
	}

	public function getListUpt()
	{
		$data = Modules::run('database/get', array(
			'table' => 'upt t',
			'field' => array('t.*'),
			'where' => "t.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->result_array();
		}
		return $result;
	}

	public function getListTahapan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'tahapan_pekerjaan t',
			'field' => array('t.*'),
			'where' => "t.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->result_array();
		}
		return $result;
	}

	public function getListPotensi()
	{
		$data = Modules::run('database/get', array(
			'table' => 'potensi_bahaya t',
			'field' => array('t.*'),
			'where' => "t.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->result_array();
		}
		return $result;
	}

	public function getListPengendalian()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pengendalian t',
			'field' => array('t.*'),
			'where' => "t.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->result_array();
		}
		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Pengendalian";
		$data['title_content'] = 'Tambah Pengendalian';
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailData($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Pengendalian";
		$data['title_content'] = 'Ubah Pengendalian';
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailData($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengendalian";
		$data['title_content'] = "Detail Pengendalian";
		echo Modules::run('template', $data);
	}

	public function getPostDataContact($value)
	{
		$data['pengendalian'] = $value->pengendalian;
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		$is_valid = false;

		// echo '<pre>';
		// print_r($data);die;

		$this->db->trans_begin();
		try {

			$push = [];
			$push['deleted'] = 1;
			$this->db->update('document', $push, array('doc_type', 'DOCT_TJSA'));

			$noDocument = Modules::run('no_generator/generateNoDocument');
			$push = [];
			$push['no_document'] = $noDocument;
			$push['doc_type'] = 'DOCT_TJSA';
			$id = Modules::run('database/_insert', 'document', $push);

			//insert template jsa
			if (isset($data->form_jsa)) {
				foreach ($data->form_jsa as $value) {
					$push = [];
					$push['tahapan_pekerjaan'] = $value->tahapan_pekerjaan;
					$push['potensi_bahaya'] = json_encode($value->potensi_bahaya);
					$push['pengendalian'] = json_encode($value->pengendalian);
					$push['document'] = $id;
					Modules::run('database/_insert', 'permit_jsa_item_template', $push);
				}
			}


			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Contact";
		$data['title_content'] = 'Data Contact';
		$content = $this->getData($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function uploadData($name_of_field)
	{
		$config['upload_path'] = 'files/berkas/taruna/';
		$config['allowed_types'] = 'png|jpg';
		$config['max_size'] = '1000';
		$config['max_width'] = '2000';
		$config['max_height'] = '2000';

		$this->load->library('upload', $config);
		$this->upload->do_upload($name_of_field);
	}

	public function showLogo()
	{
		$foto = str_replace(' ', '_', $this->input->post('foto'));
		$data['foto'] = $foto;
		echo $this->load->view('foto', $data, true);
	}

	public function chooseTahapanPekerjaan()
	{
		$data = $this->getListTahapan();
		$content = $_POST;
		$content['data'] = $data;
		echo $this->load->view('list_tahapan_pekerjaan', $content, true);
		// echo '<pre>';
		// print_r($data);
	}

	public function choosePotensiBahaya()
	{
		$data = $this->getListPotensi();
		$content = $_POST;
		$content['data'] = $data;
		echo $this->load->view('list_potensi', $content, true);
	}

	public function choosePengendalian()
	{
		$data = $this->getListPengendalian();
		$content = $_POST;
		$content['data'] = $data;
		echo $this->load->view('list_pengendalian', $content, true);
	}
}
