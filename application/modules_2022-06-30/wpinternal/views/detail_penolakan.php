<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Riwayat Penolakan</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>No</th>
										<th>Nama Pegawai</th>
										<th>Jabatan</th>
										<th>Status</th>
										<th>Tanggal</th>
										<th>Keterangan</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($list_reject)) { ?>
										<?php $no = 1; ?>
										<?php foreach ($list_reject as $key => $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value['nama_aktor'] ?></td>
												<td><?php echo $value['posisi_lengkap'] ?></td>
												<td><?php echo 'Ditolak' ?></td>
												<td><?php echo $value['createddate'] ?></td>
												<td><?php echo $value['keterangan'] ?></td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="6">Tidak ada data ditemukan</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>