<div class="col-md-4">
	<h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Permohonan Izin</u></h4>
	<hr />
	<div class="box-body" style="margin-top: -12px;">
		<div class="form-group">
			<label for="" class="control-label">Tanggal WP</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Tanggal" id="tgl_wp" placeholder="Tanggal" value="<?php echo isset($tanggal_wp) ? $tanggal_wp : date('Y-m-d') ?>" readonly="">
				<span class="input-group-addon">
					<i class="fa fa-calendar-o"></i>
				</span>
			</div>
		</div>

<!--		<div class="form-group">
			<label for="" class="control-label">Tanggal Pekerjaan</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Tanggal Pekerjaan" id="tgl_pekerjaan" placeholder="Tanggal Pekerjaan" value="<?php echo isset($tgl_pekerjaan) ? $tgl_pekerjaan : '' ?>" readonly="">
				<span class="input-group-addon">
					<i class="fa fa-calendar-o"></i>
				</span>
			</div>
		</div>-->

		<div class="form-group">
			<label for="" class="control-label">Tanggal Awal Pelaksanaan</label>
			<div class="input-group">
				<input type="text" onchange="WpInternal.setTanggalAkhir(this)" class="form-control required" error="Tanggal Awal Pelaksanaan" id="tgl_awal" placeholder="Tanggal Awal Pelaksanaan" value="<?php echo isset($tgl_awal) ? $tgl_awal : '' ?>" readonly="">
				<span class="input-group-addon">
					<i class="fa fa-calendar-o"></i>
				</span>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="control-label">Tanggal Akhir Pelaksanaan</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Tanggal Akhir Pelaksanaan" id="tgl_akhir" placeholder="Tanggal Akhir Pelaksanaan" value="<?php echo isset($tgl_akhir) ? $tgl_akhir : '' ?>" readonly="">
				<span class="input-group-addon">
					<i class="fa fa-calendar-o"></i>
				</span>
			</div>
		</div>

		<div class="form-group">
			<label for="" class="control-label">Penanggung Jawab Pekerjaan</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Penanggung Jawab Pekerjaan" id="penanggung_jawab_pekerjaan" placeholder="Penanggung Jawab Pekerjaan" value="<?php echo isset($penanggung_jawab_pekerjaan) ? $penanggung_jawab_pekerjaan : '' ?>">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>
			</div>
		</div>
		
		<div class="form-group">
			<label for="" class="control-label">Jabatan Penanggung Jawab Pekerjaan</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Jabatan Penanggung Jawab Pekerjaan" id="jabatan_penanggung_jawab" placeholder="Jabatan Penanggung Jawab Pekerjaan" value="<?php echo isset($jabatan_penanggung_jawab) ? $jabatan_penanggung_jawab : '' ?>">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>
			</div>
		</div>
		
		<div class="form-group">
			<label for="" class="control-label">Pengawas K3</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Pengawas K3" id="pengawas_k3" placeholder="Pengawas K3" value="<?php echo isset($pengawas_k3) ? $pengawas_k3 : '' ?>">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>
			</div>
		</div>
		
		<div class="form-group">
			<label for="" class="control-label">Jabatan Pengawas K3</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Jabatan Pengawas K3" id="jabatan_pengawas_k3" placeholder="Jabatan Pengawas K3" value="<?php echo isset($jabatan_pengawas_k3) ? $jabatan_pengawas_k3 : '' ?>">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>
			</div>
		</div>
		
		<div class="form-group">
			<label for="" class="control-label">Pengawas Pekerjaan</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Pengawas Pekerjaan" id="pengawas_pekerjaan" placeholder="Pengawas Pekerjaan" value="<?php echo isset($pengawas_pekerjaan) ? $pengawas_pekerjaan : '' ?>">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>
			</div>
		</div>
		
		<div class="form-group">
			<label for="" class="control-label">Jabatan Pengawas Pekerjaan</label>
			<div class="input-group">
				<input type="text" class="form-control required" error="Jabatan Pengawas Pekerjaan" id="jabatan_pengawas_pekerjaan" placeholder="Jabatan Pengawas Pekerjaan" value="<?php echo isset($jabatan_pengawas_pekerjaan) ? $jabatan_pengawas_pekerjaan : '' ?>">
				<span class="input-group-addon">
					<i class="fa fa-user"></i>
				</span>
			</div>
		</div>
	</div>
	<!-- /.box-footer -->
</div>
