<div class="col-md-4">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Work Place</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="form-group">
   <label for="" class="control-label">Tempat</label>
   <select class="form-control required" error="Tempat" id="work_place" 
           onchange="WpInternal.getDetailTempat(this)">
    <option value="">Pilih Tempat</option>
    <?php if (!empty($list_place)) { ?>
     <?php foreach ($list_place as $value) { ?>
      <?php $selected = '' ?>
      <?php if (isset($work_place)) { ?>
       <?php $selected = $work_place == $value['id'] ? 'selected' : '' ?>
      <?php } ?>
      <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
     <?php } ?>
    <?php } ?>
   </select>
  </div>

  <div class="content_place">
   <div class="form-group">
    <label for="" class="control-label">Detail Tempat</label>
    <select class="form-control required" error="Detail Tempat" id="place">
     <option upt="<?php echo isset($place) ? $upt_id : '' ?>" value="<?php echo isset($place) ? $place : '' ?>"><?php echo isset($nama_place) ? $nama_place : 'Pilih Detail Tempat' ?></option>
    </select>
   </div>
  </div>

  <div class="form-group">
   <label for="" class="control-label">Lokasi Pekerjaan</label>
   <input type="text" value="<?php echo isset($lokasi_pekerjaan) ? $lokasi_pekerjaan : '' ?>" id="lokasi_kerja" class="form-control" error=""/>
  </div>

  <div class="form-group">
   <label for="" class="control-label">Uraian Pekerjaan</label>
   <textarea class="form-control required" error="Keterangan" id="uraian_pekerjaan"><?php echo isset($uraian_pekerjaan) ? $uraian_pekerjaan : '' ?></textarea>
  </div>

  <div class="form-group text-right">    
   <div class="checkbox">
    <label>
     <?php $checked = isset($is_need_sistem) ? $is_need_sistem == 1 ? 'checked' : '' : '' ?>
     <input type="checkbox" id="need_sistem" <?php echo $checked ?> onchange="WpInternal.checkNeed(this)"> Padam
    </label>
   </div>
  </div>

  <?php $hidden = isset($is_need_sistem) ? $is_need_sistem == 1 ? '' : 'hidden' : 'hidden' ?>
  <div class="form-group <?php echo $hidden ?>" id="content_ket_need">
   <label for="" class="control-label">Keterangan Padam</label>
   <textarea class="form-control" error="Keterangan Padam" id="keterangan_need_sistem"><?php echo isset($keterangan_need_sistem) ? $keterangan_need_sistem : '' ?></textarea>
  </div>

  <!--  <div class="form-group">
     <label for="">File SK3</label>
     <input type="file" id="file_sk3" class="form-control">
    </div>-->

  <?php $hidden = ""; ?>
  <?php $hidden = isset($file_spk) ? 'hidden' : '' ?>
  <div class="form-group <?php echo $hidden ?>" id='file_input_spk'>
   <label for="">File SPK</label>
   <input type="file" id="file_spk" class="form-control" onchange="WpInternal.checkFile(this)">
  </div>

  <?php $hidden = ""; ?>
  <?php $hidden = isset($file_spk) ? '' : 'hidden' ?>
  <div class="form-group <?php echo $hidden ?>" id="detail_file_spk">
   <label for="" >File SPK</label>
   <div class="input-group">
    <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($file_spk) ? $file_spk : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_spk) ? $file_spk : '' ?>" 
        onclick="WpInternal.showLogo(this, event)"></i>         
    </span>
    <span class="input-group-addon">
     <i class="fa fa-close hover-content"
        onclick="WpInternal.gantiFile(this, event)"></i>
    </span>
   </div>  
  </div>

  <div class="form-group">
	<label for="" class='text-danger'>Format File SPK : SPK_TH_BLN_TGL_PEKERJAAN</label> 
	<br>
	<label for="" class='text-success'>CONTOH :</label> 
	<br>
	<label for="" class='text-success'>SPK_2020_01_28_BONGKAR_TRAFO_GI_BLIMBING</label> 
  </div>


  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <?php if (isset($ket_approve)) { ?>
   <div class="form-group">
    <label for="">Alasan di Tolak</label>
    <textarea disabled class="form-control required" error="Keterangan" id="uraian_pekerjaan"><?php echo isset($ket_approve) ? $ket_approve : '' ?></textarea>
   </div>
  <?php } ?> 
 </div>   


 <!-- /.box-footer -->
</div>
