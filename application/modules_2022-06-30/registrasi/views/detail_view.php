<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="row">
        <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info padding-16">
                        <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form class="form-horizontal" method="post">
                                <div class="box-body">
                                        <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">No Pengajuan</label>

                                                <div class="col-sm-4">
                                                        <input type="text" disabled="" class="form-control required" error="No Pengajuan" id="no_pengajuan" placeholder="No Pengajuan" value="<?php echo $no_pengajuan ?>">
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Nama Vendor</label>

                                                <div class="col-sm-4">
                                                        <input type="text" disabled="" class="form-control" error="Nama Vendor" id="nama" placeholder="Nama Vendor" value="<?php echo $nama_vendor ?>">
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Email</label>

                                                <div class="col-sm-4">
                                                        <input type="text" disabled="" class="form-control" error="Email" id="email" placeholder="Email" value="<?php echo $email ?>">
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">No Hp</label>

                                                <div class="col-sm-4">
                                                        <input type="text" disabled="" class="form-control" error="No HP" id="no_hp" placeholder="No HP" value="<?php echo $no_hp ?>">
                                                </div>
                                        </div>
                                        <div class="form-group">
                                                <label for="" class="col-sm-2 control-label">Status</label>

                                                <div class="col-sm-4">
                                                        <input type="text" disabled="" class="form-control required" error="Status" id="status" placeholder="Status" value="<?php echo $status['status'] ?>">
                                                </div>
                                        </div>
                                        <?php if ($status['status'] == 'REJECTED') { ?>
                                                <div class="form-group">
                                                        <label for="" class="col-sm-2 control-label">Status</label>

                                                        <div class="col-sm-4">
                                                                <textarea class="form-control" disabled=""><?php echo $status['keterangan'] ?></textarea>
                                                        </div>
                                                </div>
                                        <?php } ?>

                                        <hr />
                                        <?php if (!empty($user_vendor)) { ?>
                                                <div class="form-group">
                                                        <label for="" class="col-sm-2 control-label">Username</label>

                                                        <div class="col-sm-4">
                                                                <input type="text" disabled="" class="form-control required" error="Username" id="username" placeholder="Username" value="<?php echo $user_vendor['username'] ?>">
                                                        </div>
                                                </div>
                                                <div class="form-group">
                                                        <label for="" class="col-sm-2 control-label">Password</label>

                                                        <div class="col-sm-4">
                                                                <input type="text" disabled="" class="form-control required" error="Password" id="password" placeholder="Password" value="<?php echo $user_vendor['password'] ?>">
                                                        </div>
                                                </div>
                                        <?php } ?>
                                </div>
                                <!-- /.box-body -->
                                <!--<div class="box-footer">-->
                                <div class="row">
                                        <div class="col-md-12 text-right">
                                                <button type="button" class="btn btn-default" onclick="Registrasi.back()">Cancel</button>
                                                &nbsp;
                                                <?php if ($status['status'] == 'DRAFT') { ?>
                                                        <button type="submit" class="btn btn-success" onclick="Registrasi.createUser('<?php echo isset($id) ? $id : '' ?>', event)">
                                                                <i class="fa fa-check"></i>&nbsp;Approve
                                                        </button>
                                                        <button style="margin-left: 8px;" type="submit" class="btn btn-danger" onclick="Registrasi.reject('<?php echo isset($id) ? $id : '' ?>', event)">
                                                                <i class="fa fa-check"></i>&nbsp;Reject
                                                        </button>
                                                <?php } ?>
                                        </div>
                                </div>
                                <!--</div>-->
                                <!-- /.box-footer -->
                        </form>
                </div>
                <!-- /.box -->
        </div>
</div>