<html>

<head>
	<title></title>
	<style>
		#_wrapper {
			width: 100%;
			margin: 0 auto;
		}

		#_content {
			border: 1px solid #999;
			max-width: 100%;
			text-align: center;
		}

		#_top-content {
			width: 95%;
			max-width: 95%;
			margin: 1% auto;
		}

		#_judul {
			font-size: 100%;
			font-family: arial;
			font-weight: bold;
		}

		h3 {
			margin: 0;
			font-size: 100%;
			font-family: arial;
		}

		#_data {
			font-family: arial;
			font-size: 12px;
		}

		table {
			border-collapse: collapse;
			border: 1px solid black;
		}

		#_surat {
			width: 45%;
			margin: 0 auto;
			font-family: arial;
			font-weight: bold;
		}

		#_form {
			width: 15%;
			font-size: 12px;
			text-align: left;
			margin-left: 80%;
			margin-right: 2%;
			padding: 0.5%;
			border: 1px solid black;
		}

		#no {
			margin-top: 1%;
			font-family: arial;
		}

		#_isi-content {
			text-align: left;
			margin-left: 2%;
			font-size: 12px;
			font-family: tahoma;
		}

		#_center-content {
			text-align: left;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
			border: 1px solid black;
			padding-top: 1.5%;
			padding-left: 1%;
			padding-bottom: 1.5%;
			font-family: tahoma;
			font-size: 12px;
		}

		#_table-content {
			text-align: left;
			font-family: tahoma;
			margin-top: 2%;
			margin-left: 2%;
			margin-right: 2%;
		}

		.content-top {
			font-family: tahoma;
		}
	</style>
</head>

<body>
	<div id="_wrapper">
		<div id="_content">
			<div id="_top-content">
				<table style="width: 100%;max-width: 100%;">
					<tr>
						<td style="border-right: none;"><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>
						<td style="border-right: none;border-left: none;">
							<h3>&nbsp;PT PLN (PERSERO)</h3>
							<h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
						</td>
						<td colspan="70"></td>
						<td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
					</tr>
					<tr>
						<td id="_judul" colspan="60" rowspan="4" style="border-top: 1px solid black;">
							<center>
								<label>
									FORMULIR STOP WORKING AUTHORITY (SWA)
								</label>
							</center>
						</td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
					</tr>
					<tr>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
						<td id="_data" colspan="8" style="border: 1px solid black;"><label>1 dari 1</label></td>
					</tr>
				</table>
			</div>
			<div>
				<hr />
			</div>

			<div class="ttd" style="padding: 16px;">

				<h5 style="text-align: left;font-family: tahoma;">E. DOKUMENTASI</h5>
				<table style="width: 100%;">
					<?php if (!empty($data_wp['data_image'])) { ?>
						<?php $counter = 1; ?>
						<?php foreach ($data_wp['data_image'] as $key => $value) { ?>
							<tr>
								<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">
									<?php echo $value['elm_image'] ?>
								</td>
							</tr>
							<?php $counter += 1; ?>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">-</td>
							<td style="font-family: tahoma;padding: 8px;border: 1px solid black;font-size:12px;">-</td>
						</tr>
						<tr>
							<td style="font-family: tahoma;text-align: center;border: 1px solid black;font-size:12px;">-</td>
							<td style="font-family: tahoma;padding: 8px;border: 1px solid black;font-size:12px;">-</td>
						</tr>
					<?php } ?>
				</table>
			</div>
		</div>
	</div>
</body>

</html>
