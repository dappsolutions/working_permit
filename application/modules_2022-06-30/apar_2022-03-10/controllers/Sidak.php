<?php
// ini_set("memory_limit",-1);
class Sidak extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_sidak', 'sidak');
		$this->load->model('m_permit', 'permit');
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
		);

		return $data;
	}

	public function getModuleName()
	{
		return 'swa/sidak';
	}

	public function index()
	{
		echo 'sidak ' . date('Y-m-d H:i:s');
	}

	public function simpan()
	{
		$data = $_POST;		
		// echo '<pre>';
		// print_r($data);
		// die;
		$data['data_inspeksi'] = (array) json_decode($_POST['data_inspeksi']);
// 		echo '<pre>';
// 		print_r($data['data_inspeksi']);
// 		die;
		$data['data_kelengkapan'] = (array) json_decode($_POST['data_kelengkapan']);
		// $data_struktur_acc = $this->sidak->getStrukturAccPertama($data);
		// $data['struktur_acc'] = $data_struktur_acc;
		$data['no_trans'] = Modules::run('no_generator/generateNoDocument');

		$result = $this->sidak->simpan($data);

		// if ($result['is_valid'] == '1') {
		// 	$this->sendEmail($data);
		// }


		// // echo '<pre>';
		// // print_r($data['data_image']);
		// // die;
		// $result['is_valid'] = '1';
		// $result['message'] = '';
		$result['no_trans'] = $data['no_trans'];
		echo json_encode($result);
		// // Modules::run('simson/output/get', $result);
	}
	
	public function simpanPerubahan()
	{
		$data = $_POST;		
		$result = $this->sidak->simpanPerubahan($data);
		echo json_encode($result);
	}
	
	public function ubahKeterangan()
	{
		$data = $_POST;		
		$result = $this->sidak->ubahKeterangan($data);
		echo json_encode($result);
	}

	public function simpanGambar()
	{
		$data = $_POST;	
// 		echo '<pre>';
// 		print_r($data);die;
		$result = $this->sidak->simpanGambar($data);

		echo json_encode($result);
	}

	public function batalPengajuan()
	{
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		// $data_wp = $this->permit->getDetailData($data);
		// $data_struktur_acc = $this->sidak->getStrukturAccPertama($data);
		// $data['struktur_acc'] = $data_struktur_acc;
		$result = $this->sidak->batalPengajuan($data);
		// if ($result['is_valid'] == '1') {
		// 	$this->sendEmail($data);
		// }

		echo json_encode($result);
	}
	
	public function sendEmail($params = array())
	{
		$params['subject'] = 'Formulir SWA';
		$params['no_wp'] = $params['no_wp'];
		$params['id_wp'] = $params['data_wp']['id'];
		$params['email'] = $params['data_swa']['email_swa'];
		$params['message'] = '<p>Berikut kami sampaikan laporan SWA, Silkan klik link dibawah ini untuk melihat formulir</p>
		<p>Semoga PLN menjadi yang terbaik</p>
		<p>Amienn...</p>
		<p><a href="' . base_url() . 'swa/sidak/cetak?permit=' . $params['id_wp'] . '&no_wp=' . $params['no_wp'] . '">Formulir SWA</a></p>';

		$cc = array();
		if (!empty($params['struktur_acc'])) {
			$cc[] = $params['struktur_acc']['email'];
		}
		Modules::run('email/send_email_data_other', $params['subject'], $params['message'], $params['email'], $cc);
	}

	public function getDetailData()
	{
		$data = $_POST;
		// $data['id'] = 21;
		// $this->createFilePdf();
		// $data['no_wp'] = "WPEKS20MAY001";
		$data_transaksi = $this->sidak->getDetailDataTransaksi($data);
		$data_kelengkapan = $this->sidak->getDetailDataTransaksiKelengkapan($data);
		$result['data'] = $data_transaksi;
		$result['data_kelengkapan'] = $data_kelengkapan;
		Modules::run('simson/output/get', $result);
	}

	public function getDetailImage()
	{
		$data = $_POST;
		
		// $data['no_document'] = 'DOC22JAN0005';
		$data_image = $this->sidak->getDataSidakImage($data);
		$this->createFilePdf();
		// echo '<pre>';
		// print_r($data_image);die;
		$result['data'] = $data_image;
		Modules::run('simson/output/get', $result);
	}

	public function showDetailPengajuan()
	{
		$data = $_GET;
		// echo '<pre>';
		// print_r($data);
		// die;
		$data_safety = $this->sidak->getDataSafetyAll($data);
		$data['data_swa'] = $data_safety;
		// echo '<pre>';
		// print_r($data);
		// die;
		$data['data_apd'] = $data_safety;
		$data['data_pekerjaan'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		// echo '<pre>';
		// print_r($data['data_swa']);
		// die;
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengajuan";
		$data['title_content'] = "Detail Pengajuan";
		echo Modules::run('template', $data);
	}

	public function getDokumenDp3Aktif()
	{
		$data = Modules::run('database/get', array(
			'table' => 'dp3',
			'where' => "deleted = 0 and id = 2"
		));

		$data = $data->row_array();
		$data['berlaku_efektif'] = Modules::run('helper/getIndoDate', $data['berlaku_efektif']);
		return $data;
	}

	public function getDataStatusPekerjaan($data)
	{
		if (!empty($data)) {
			if ($data['wp'] == '' || $data['wp'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['sop'] == '' || $data['sop'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['jsa'] == '' || $data['jsa'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['pengawas_k3'] == '' || $data['pengawas_k3'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['sertifikat_kom'] == '' || $data['sertifikat_kom'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['peralatan'] == '' || $data['peralatan'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['rambu_k3'] == '' || $data['rambu_k3'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			if ($data['apd'] == '' || $data['apd'] == 'no') {
				return "PEKERJAAN HARUS DIHENTIKAN";
			}

			return "PEKERJAAN DAPAT DILANJUTKAN";
		}

		return "PEKERJAAN HARUS DIHENTIKAN";
	}

	public function cetak()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_GET;
		$data_safety = $this->sidak->getDataSafety($data);
		$data['keputusan_pekerjaan'] = $this->getDataStatusPekerjaan($data_safety);
		$data['data_swa'] = $data_safety;
		$data['data_apd'] = $data_safety;
		$data['data_pekerjaan'] = $data_safety;
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		// echo '<pre>';
		// print_r($data);
		// die;
		$data['dp3'] = $this->getDokumenDp3Aktif();

		$image_ttd_k3 = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_k3']);
		$image_ttd_swa = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_swa']);

		$data['img_ttdk3'] = '<img height="50" width="50" src="' . $image_ttd_k3 . '"/>';
		$data['img_ttdswa'] = '<img height="50" width="50" src="' . $image_ttd_swa . '"/>';

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$formulir_swa = $this->load->view('sidak/cetak/formulir_swa', $data, true);
		$mpdf->WriteHTML($formulir_swa);
		$mpdf->Output('FORMULIR SWA - ' . $data['no_wp'] . '.pdf', 'I');
	}

	public function createFilePdf()
	{
		$mpdf = Modules::run('mpdf/getInitPdf');

		$data = $_POST;
		// $data['no_document'] = "DOC22JAN0005";
		$data['data_image'] = $this->sidak->getDataSidakImage($data);
		$data['dp3'] = $this->getDokumenDp3Aktif();
		$data['head_form'] =$this->sidak->getDetailDataSidak($data);
		// echo '<pre>';
		// print_r($data);die;

		// $image_ttd_k3 = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_k3']);
		// $image_ttd_swa = $this->base64ToImage($data, $data['data_pekerjaan']['ttd_swa']);

		// $data['img_ttdk3'] = '<img height="50" width="50" src="' . $image_ttd_k3 . '"/>';
		// $data['img_ttdswa'] = '<img height="50" width="50" src="' . $image_ttd_swa . '"/>';

		$data_image = array();
		if (!empty($data['data_image'])) {
			foreach ($data['data_image'] as $key => $value) {
				$image = $value['picture'];
				$image = $this->base64ToImage($data, $image);
				$value['elm_image'] = '<img height="350" width="350" src="' . $image . '"/>';
				array_push($data_image, $value);
			}
		}
		$data['data_image'] = $data_image;
		$data['data_wp'] = $data;
		$data['dataItem'] = $this->sidak->getDataListTransaksiApar($data);
		$data['dataKelengkapan'] = $this->sidak->getDataListTransaksiAparKelengkapan($data);
		
		$formulir_apar = $this->load->view('sidak/cetak/formulir_apar', $data, true);
		$formulir_apar_kelengkapan = $this->load->view('sidak/cetak/formulir_apar_kelengkapan', $data, true);
		$dokumentasi_apar = $this->load->view('sidak/cetak/dokumentasi', $data, true);
		$mpdf->WriteHTML($formulir_apar);
		$mpdf->AddPage();
		$mpdf->WriteHTML($formulir_apar_kelengkapan);
		$mpdf->AddPage();
		$mpdf->WriteHTML($dokumentasi_apar);
		$mpdf->Output('files/berkas/dokumen_sidak/FORMULIRAPAR-' . $data['no_document'] . '.pdf', 'F');
		// $mpdf->Output('files/berkas/dokumen_sidak/FORMULIRAPAR-' . $data['no_document'] . '.pdf', 'I');
	}

	public function base64ToImage($params, $imageData)
	{
		$imageData = 'data:image/jpeg;base64,' . $imageData;
		list($type, $imageData) = explode(';', $imageData);
		// echo $imageData;
		// die;
		list(, $extension) = explode('/', $type);
		list(, $imageData)      = explode(',', $imageData);
		// $fileName = $_SERVER["DOCUMENT_ROOT"] . '/files/berkas/ttd/' . $params['no_wp'] . uniqid() . '.' . $extension;
		$fileName = $_SERVER["DOCUMENT_ROOT"] . '/files/berkas/ttd/' . $params['no_document'] . uniqid() . '.' . $extension; //production
// 		$fileName = $_SERVER["DOCUMENT_ROOT"] . '/working_permit/files/berkas/ttd/' . $params['no_document'] . uniqid() . '.' . $extension;
		$imageData = base64_decode($imageData);
		file_put_contents($fileName, $imageData);

		return $fileName;
	}

	public function getListDetailData()
	{
		$content = $_POST;
		// echo '<pre>';
		// print_r($data);die;
		// $content['no_document'] = 'DOC22JAN0005';
		$data = $this->sidak->getDataListTransaksiApar($content);
		$dataKelengkapan = $this->sidak->getDataListTransaksiAparKelengkapan($content);
		$result['data'] = $data;
		$result['data_kelengkapan'] = $dataKelengkapan;
		echo json_encode($result);
	}
	
	
}
