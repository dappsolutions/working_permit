<?php

class Login extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('m_login', 'login');
	}

	public function index()
	{
		echo 'login ' . date('Y-m-d H:i:s');
	}

	public function signIn()
	{
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);
		// die;
		$data = $this->login->signIn($data);

		$result['is_valid'] = "0";
		$result['data'] = array();
		$result['message'] = "Tidak ada pengguna ditemukan";
		if (!empty($data)) {
			$result['is_valid'] = "1";
			$result['data'] = $data;
			$result['message'] = "Login Sukses";
		}
		echo json_encode($result);
	}
}
