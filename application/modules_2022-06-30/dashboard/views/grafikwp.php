<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary padding-16">
          <div class="box-header with-border">
            <i class="fa fa-bar-chart-o"></i>

            <h3 class="box-title">Grafik WP</h3>
            <br />

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-3">
                <select class="form-control col-sm-3" error="Tahun" id="tahun-wp" onchange="Dashboard.changeYearGrafikWp(this)">
                  <?php if (!empty($list_year)) { ?>
                    <?php foreach ($list_year as $value) { ?>
                      <?php $selected = $value == $date ? 'selected' : '' ?>
                      <option <?php echo $selected ?> value="<?php echo $value ?>"><?php echo $value ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
              <div class="col-md-3">
                <select class="form-control col-sm-3" error="UPT" id="upt-wp" onchange="Dashboard.changeYearGrafikWp(this)">
                  <?php if (!empty($list_upt_wp)) { ?>
                    <option value=""></option>
                    <?php foreach ($list_upt_wp as $value) { ?>
                      <?php $selected = '' ?>
                      <?php if (isset($upt)) { ?>
                        <?php $selected = $value['id'] == $upt ? 'selected' : '' ?>
                      <?php } ?>
                      <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
                    <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col-md-12">
                <div id="bar-chart-data-wp" style="height: 400px;"></div>
              </div>
            </div>
            <br />
            <br />
            <br />
          </div>
          <!-- /.box-body-->
        </div>
      </div>
    </div>
  </div>
</div>

<input type="hidden" value='' id="data_wp_new" class="form-control" />