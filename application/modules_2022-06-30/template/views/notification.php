<li>
 <!-- inner menu: contains the actual data -->
 <ul class="menu">
  <?php if (!empty($notif_wp)) { ?>
   <?php foreach ($notif_wp as $value) { ?>
    <li>
     <a href="#">
      <i class="fa fa-file-text-o text-aqua"></i> <b><?php echo $value['no_wp'] ?></b> Telah dibuat
     </a>
    </li>
   <?php } ?>
  <?php } ?>
 </ul>
</li>