<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        <div class="box padding-16">
          <!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <div class="input-group">
                  <input type="text" class="form-control" onkeyup="WpEksternal.search(this, event)" id="keyword" placeholder="Pencarian">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>
            </div>
            <div class="divider"></div>
            <br />
            <?php if (isset($keyword)) { ?>
              <br />
              <div class="row">
                <div class="col-md-6">
                  Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
                </div>
              </div>
            <?php } ?>
            <div class="row">
              <div class="col-md-12">
                <i>
                  <!-- <h5 class="label label-danger">User wajib menyertakan surat keterangan sehat dari dokter untuk semua pelaksana</h5> -->
                  <h5 class="label label-danger">User wajib menyertakan hasil swab antigen yg masih berlaku selama 7 hari</h5>
                </i>
                <div class="table-responsive">
                  <table class="table table-bordered" id="tb_content">
                    <thead>
                      <tr class="bg-primary-light text-white">
                        <th>No</th>
                        <th>Action</th>
                        <th>Status</th>
                        <th>No Pengajuan Wp</th>
                        <th>Vendor</th>
                        <th>Tanggal Wp</th>
                        <!-- <th>Tanggal Pekerjaan</th> -->
                        <th>Tanggal Awal Pelaksanaan</th>
                        <th>Tanggal Akhir Pelaksanaan</th>
                        <th>Tempat Pekerjaan</th>
                        <th>Lokasi Pekerjaan</th>
                        <th>Uraian Pekerjaan</th>
                        <!-- <th>Tujuan</th> -->
                      </tr>
                    </thead>
                    <tbody>
                      <?php if (!empty($content)) { ?>
                        <?php $no = $pagination['last_no'] + 1; ?>
                        <?php foreach ($content as $value) { ?>
                          <?php $bg_color = ''; ?>
                          <?php if ($value['status'] == 'DRAFT') { ?>
                            <?php $bg_color = 'bg-warning'; ?>
                          <?php } ?>
                          <?php if ($value['status'] == 'REJECTED') { ?>
                            <?php $bg_color = 'bg-danger'; ?>
                          <?php } ?>
                          <?php if ($value['status'] == 'APPROVED') { ?>
                            <?php $bg_color = 'bg-success'; ?>
                          <?php } ?>
                          <tr class="<?php echo $bg_color ?>" data_id="<?php echo $value['id'] ?>">
                            <td><b><?php echo $no++ ?></b></td>
                            <td class="text-center">
                              <?php if ($hak_akses == 'superadmin') { ?>
                                <i data-toggle="tooltip" title="Hapus" class="fa fa-trash grey-text hover" onclick="WpEksternal.delete('<?php echo $value['id'] ?>')"></i>
                                &nbsp;
                              <?php } ?>
                              <?php if ($hak_akses == 'approval' && $level == '1') { ?>
                                <i data-toggle="tooltip" title="Hapus" class="fa fa-trash grey-text hover" onclick="WpEksternal.delete('<?php echo $value['id'] ?>')"></i>
                                &nbsp;
                              <?php } ?>


                              <?php if ($hak_akses == 'approval') { ?>
                                <?php if ((($value['status'] == 'DRAFT' || $value['status'] == 'REJECTED') && $level == "1")) { ?>
                                  <i data-toggle="tooltip" title="Ubah" class="fa fa-pencil grey-text  hover" onclick="WpEksternal.ubah('<?php echo $value['id'] ?>')"></i>
                                  &nbsp;
                                <?php } ?>
                              <?php } ?>
                              <?php if ($hak_akses == 'vendor') { ?>
                                <?php if (($value['status'] == 'DRAFT' || $value['status'] == 'REJECTED')) { ?>
                                  <i data-toggle="tooltip" title="Ubah" class="fa fa-pencil grey-text  hover" onclick="WpEksternal.ubah('<?php echo $value['id'] ?>')"></i>
                                  &nbsp;
                                <?php } ?>
                              <?php } ?>
                              <?php if ($hak_akses == 'superadmin') { ?>
                                <i data-toggle="tooltip" title="Ubah" class="fa fa-pencil grey-text  hover" onclick="WpEksternal.ubah('<?php echo $value['id'] ?>')"></i>
                                &nbsp;
                              <?php } ?>
                              <i data-toggle="tooltip" title="Detail" class="fa fa-file-text grey-text  hover" onclick="WpEksternal.detail('<?php echo $value['id'] ?>')"></i>
                              &nbsp;

                              <?php if ($hak_akses == 'superadmin') { ?>
                                <i data-toggle="tooltip" title="Cetak" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetak('<?php echo $value['id'] ?>')"></i>
                                <?php if ($value['is_need_sistem']) { ?>
                                  &nbsp;
                                  <i data-toggle="tooltip" title="Cetak DP3" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetakDp3('<?php echo $value['id'] ?>')"></i>
                                <?php } ?>
                              <?php } ?>

                              <?php if ($hak_akses == 'approval' && $value['level'] == '3') { ?>
                                <i data-toggle="tooltip" title="Cetak" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetak('<?php echo $value['id'] ?>')"></i>
                                <?php if ($value['is_need_sistem']) { ?>
                                  &nbsp;
                                  <i data-toggle="tooltip" title="Cetak DP3" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetakDp3('<?php echo $value['id'] ?>')"></i>
                                <?php } ?>
                              <?php } else { ?>
                                <?php if ($value['level'] == '3') { ?>
                                  <i data-toggle="tooltip" title="Cetak" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetak('<?php echo $value['id'] ?>')"></i>
                                  <?php if ($value['is_need_sistem']) { ?>
                                    &nbsp;
                                    <i data-toggle="tooltip" title="Cetak DP3" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetakDp3('<?php echo $value['id'] ?>')"></i>
                                  <?php } ?>
                                <?php } else { ?>
                                  <?php if ($value['is_need_sistem']) { ?>
                                    &nbsp;
                                    <i data-toggle="tooltip" title="Cetak DP3" class="fa fa-print grey-text  hover" onclick="WpEksternal.cetakDp3('<?php echo $value['id'] ?>')"></i>
                                  <?php } ?>
                                <?php } ?>
                              <?php } ?>
                            </td>
                            <?php if ($value['status'] == 'DRAFT') { ?>
                              <td><b><?php echo $value['status'] ?></b></td>
                            <?php } else { ?>
                              <td><b><?php echo $value['status'] ?></b><br />[Level : <?php echo $value['level'] ?>]</td>
                            <?php } ?>
                            <td><b><?php echo $value['no_wp'] ?></b></td>
                            <td><b><?php echo $value['nama_vendor'] ?></b></td>
                            <td><b><?php echo $value['tanggal_wp'] ?></b></td>
                            <!-- <td><b><?php echo $value['tgl_pekerjaan'] ?></b></td> -->
                            <td><b><?php echo $value['tgl_awal'] ?></b></td>
                            <td><b><?php echo $value['tgl_akhir'] ?></b></td>
                            <td><b><?php echo $value['nama_place'] ?></b></td>
                            <td><b><?php echo $value['lokasi_pekerjaan'] ?></b></td>
                            <td><b><?php echo $value['uraian_pekerjaan'] ?></b></td>
                            <!-- <td><b><?php echo $value['nama_upt'] ?></b></td> -->

                          </tr>
                          <tr data_id="<?php echo $value['id'] ?>">
                            <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;Single Line Diagram</td>
                            <td colspan="9">
                              <?php if ($value['nama_single_line'] != '') { ?>
                                <b><?php echo $value['nama_single_line'] ?></b>
                                &nbsp;
                                <i class="fa fa-image" file="<?php echo $value['file_single_line'] ?>" onclick="WpEksternal.showFile(this, 'diagram')"></i>
                                <br />
                              <?php } ?>

                              <?php if ($hak_akses == 'superadmin') { ?>
                                <a href="#" onclick="WpEksternal.chooseSingleLineDiagram(this)">Set Single Line Diagram</a>
                              <?php } ?>
                              <?php if ($hak_akses == 'approval' && $level == '1') { ?>
                                <a href="#" onclick="WpEksternal.chooseSingleLineDiagram(this)">Set Single Line Diagram</a>
                              <?php } ?>
                            </td>
                          </tr>
                          <tr data_id="<?php echo $value['id'] ?>">
                            <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;Sld</td>
                            <td colspan="9">
                              <?php if ($value['nama_sld'] != '') { ?>
                                <b><?php echo $value['nama_sld'] ?></b>
                                &nbsp;
                                <i class="fa fa-image" file="<?php echo $value['file_sld'] ?>" onclick="WpEksternal.showFile(this, 'diagram')"></i>
                                <br />
                              <?php } ?>

                              <?php if ($hak_akses == 'superadmin') { ?>
                                <a href="#" onclick="WpEksternal.chooseSldDiagram(this)">Set Sld</a>
                              <?php } ?>
                              <?php if ($hak_akses == 'approval' && $level == '1') { ?>
                                <a href="#" onclick="WpEksternal.chooseSldDiagram(this)">Set Sld</a>
                              <?php } ?>
                            </td>
                          </tr>
                          <tr data_id="<?php echo $value['id'] ?>">
                            <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;High Risk Management</td>
                            <td colspan="9">
                              <?php if ($value['risk_tipe'] != '') { ?>
                                <b><?php echo $value['risk_tipe'] ?></b>
                                <br />
                              <?php } ?>

                              <?php if ($hak_akses == 'superadmin') { ?>
                                <a href="#" onclick="WpEksternal.chooseHighRisk(this)">Set High Risk</a>
                              <?php } ?>
                              <?php if ($hak_akses == 'approval' && $level == '1') { ?>
                                <a href="#" onclick="WpEksternal.chooseHighRisk(this)">Set High Risk</a>
                              <?php } ?>
                            </td>
                          </tr>

                          <!--SIMSON-->
                          <?php if ($value['no_trans_simson'] != '') { ?>
                            <tr data_id="<?php echo $value['id'] ?>">
                              <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;Pengajuan Sidak Simson</td>
                              <td colspan="9">
                                <a href="<?php echo base_url() ?>simson/sidak/showDetailPengajuan?permit=<?php echo $value['id'] ?>&no_wp=<?php echo $value['no_wp'] ?>" onclick="WpEksternal.showPengajuan(this,event)"><?php echo $value['no_trans_simson'] ?></a>
                              </td>
                            </tr>
                          <?php } ?>
                          <!--SIMSON-->

                          <!--SWA-->
                          <?php if ($value['no_trans_swa'] != '') { ?>
                            <tr data_id="<?php echo $value['id'] ?>">
                              <td colspan="3"><i class="fa fa-pencil-square-o"></i>&nbsp;Pengajuan Sidak SWA</td>
                              <td colspan="9">
                                <a href="<?php echo base_url() ?>swa/sidak/showDetailPengajuan?permit=<?php echo $value['id'] ?>&no_wp=<?php echo $value['no_wp'] ?>" onclick="WpEksternal.showPengajuan(this,event)"><?php echo $value['no_trans_swa'] ?></a>
                              </td>
                            </tr>
                          <?php } ?>
                          <!--SWA-->
                        <?php } ?>
                      <?php } else { ?>
                        <tr>
                          <td colspan="20" class="text-center">Tidak ada data ditemukan</td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right">
              <?php echo $pagination['links'] ?>
            </ul>
          </div>
        </div>

        <?php if ($open) { ?>
          <a href="#" class="float" onclick="WpEksternal.add()">
            <i class="fa fa-plus my-float fa-lg"></i>
          </a>
        <?php } ?>
      </div>
    </div>
  </div>
</div>