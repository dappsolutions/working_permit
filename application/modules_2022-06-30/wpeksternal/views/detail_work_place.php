<div class="col-md-4">
 <h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data Work Place</u></h4>
 <hr/>   
 <div class="box-body" style="margin-top: -12px;">
  <div class="form-group">
   <label for="" class="control-label">Tempat</label>
   <select disabled class="form-control required" error="Tempat" id="work_place" 
           onchange="WpEksternal.getDetailTempat(this)">
    <option value="">Pilih Tempat</option>
    <?php if (!empty($list_place)) { ?>
     <?php foreach ($list_place as $value) { ?>
      <?php $selected = '' ?>
      <?php if (isset($work_place)) { ?>
       <?php $selected = $work_place == $value['id'] ? 'selected' : '' ?>
      <?php } ?>
      <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
     <?php } ?>
    <?php } ?>
   </select>
  </div>

  <div class="content_place">
   <div class="form-group">
    <label for="" class="control-label">Detail Tempat</label>
    <select disabled class="form-control required" error="Detail Tempat" id="place">
     <option value=""><?php echo $nama_place ?></option>    
    </select>
   </div>
  </div>

  <div class="form-group">
   <label for="" class="control-label">Lokasi Pekerjaan</label>
   <input disabled type="text" value="<?php echo $lokasi_pekerjaan ?>" id="lokasi_kerja" class="form-control" error=""/>
  </div>

  <div class="form-group">
   <label for="" class="control-label">Uraian Pekerjaan</label>
   <textarea disabled class="form-control required" error="Keterangan" id="uraian_pekerjaan"><?php echo isset($uraian_pekerjaan) ? $uraian_pekerjaan : '' ?></textarea>
  </div>

    <div class="form-group text-right">    
     <div class="checkbox">
      <label>
       <?php $checked = isset($is_need_sistem) ? $is_need_sistem == 1 ? 'checked' : '' : '' ?>
       <input readonly="" type="checkbox" <?php echo $checked ?> id="need_sistem" onchange="WpEksternal.checkNeed(this)"> Padam
      </label>
     </div>
    </div>

    <div class="form-group" id="content_ket_need">
     <label for=""  class="control-label">Keterangan Padam</label>
     <textarea readonly="" class="form-control required" error="Keterangan Need Sistem" id="keterangan_need_sistem"><?php echo isset($keterangan_need_sistem) ? $keterangan_need_sistem : '' ?></textarea>
    </div>

  <!--  <div class="form-group">
     <label for="">File SK3</label>
     <input type="file" id="file_sk3" class="form-control">
    </div>-->
  <div class="form-group">
   <label for="">File SPK</label>
   <div class="input-group">
    <input disabled type="text" class="form-control" value="<?php echo isset($file_spk) ? $file_spk : '' ?>">
    <span class="input-group-addon"><i class="fa fa-image hover-content" file="<?php echo isset($file_spk) ? $file_spk : '' ?>" onclick="WpEksternal.showLogo(this, event)"></i></span>
   </div>
  </div>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>
  <?php if ($ket_approve != '') { ?>
   <div class="form-group">
    <label for="">Alasan di Tolak</label>
    <textarea disabled class="form-control required" error="Keterangan" id="uraian_pekerjaan"><?php echo isset($ket_approve) ? $ket_approve : '' ?></textarea>
   </div>
  <?php } ?>  
 </div>   
 <!-- /.box-footer -->
</div>