<html>
 <head>
  <title></title>    
  <style>
   #_wrapper{
    width: 100%;        
    margin: 0 auto;               
   }

   #_content{
    border: 1px solid #999;
    max-width: 100%;
    text-align: center;        
   }

   #_top-content{
    width: 95%;
    max-width: 95%;                
    margin: 1% auto;        
   }      

   #_judul{
    font-size: 100%;
    font-family: arial;
    font-weight: bold;
   }

   h3{
    margin: 0;
    font-size: 100%;
    font-family: arial;
   }     

   #_data{
    font-family: arial;
    font-size: 12px;
   }

   table{
    border-collapse: collapse;
    border: 1px solid black;
   }            


   #_surat{
    width: 45%;        
    margin: 0 auto;
    font-family: tahoma;        
   }

   #_form{                
    width: 15%;
    font-size: 12px;
    text-align: left;
    margin-left: 80%;
    margin-right:2%;
    padding: 0.5%;
    border: 1px solid black;        
   }

   #no{
    margin-top: 1%;        
    font-family: arial;
   }

   #_isi-content{
    text-align: left;
    margin-left: 2%;
    font-size: 12px;
    font-family: tahoma;
   }

   #_center-content{
    text-align: left;
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;        
    border:1px solid black;
    padding-top: 1.5%;
    padding-left: 1%;        
    padding-bottom: 1.5%;
    font-family: tahoma;
    font-size: 12px;
   }

   #_table-content{
    text-align: left;        
    font-family: tahoma;
    margin-top: 2%;
    margin-left: 2%;
    margin-right: 2%;
   }

   .isi{
    margin: 3px;outline: 1px solid black;width: 50px;height: 50px;
   }
  </style>
 </head>  
 <body>  
  <div id="_wrapper">
   <div id="_content">
    <div id="_top-content">          
     <table style="width: 100%;max-width: 100%;">
      <tr>
       <td><img src="<?php echo base_url() . 'files/img/_logo.png' ?>"></td>                                                    
       <td>                  
        <h3>&nbsp;PT PLN (PERSERO)</h3>
        <h3>UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI</h3>
       </td>                                          
       <td colspan="70"></td>
       <td style="border-right:1px solid black;padding-left: 4%;"><img src="<?php echo base_url() . 'files/img/small_smk3.png' ?>"><img src="<?php echo base_url() . 'files/img/18001.png' ?>"></td>
      </tr>                
      <tr>
       <td id="_judul" colspan="60" rowspan="4">
      <center>
       <label>
        FORMULIR IJIN KERJA <?php echo strtoupper($data_wp['tipe']) ?>
       </label>
      </center>                  
      </td>                
      <td id="_data" colspan="8" style="border: 1px solid black;"><label>No. Dokumen</label></td>
      <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['no_dokumen'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Edisi / Revisi</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['edisi'] ?></label></td>
      </tr>
      <tr>                                
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Berlaku Efektif</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label><?php echo $data_wp['dp3']['berlaku_efektif'] ?></label></td>
      </tr>              
      <tr>                       
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>Halaman</label></td>
       <td id="_data" colspan="8" style="border: 1px solid black;"><label>3 dari 8</label></td>
      </tr>
     </table>                                                                               
    </div>                
    <div><hr/></div>                               
    <div id="_surat">          
     <label>DAFTAR</label>
     <br/>
     <label>PERALATAN KERJA DAN APD</label>                    
    </div>                
    <br/>  
    <div style="text-align: right;margin-right: 3%;">
     Keterangan *) Diisi dengan menuliskan angka 
    </div>      
    <div id="_table-content">
     <table style="width: 100%;height: 50%;border: 1px solid black;">
      <tr>
       <td rowspan="2" style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">NO.</td>
       <td rowspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;">ALAT KERJA / APD</td>
       <td rowspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;">KETERSEDIAAN *)</td>
       <td colspan="2" style="border:1px solid black;font-family: tahoma;text-align: center;">KONDISI *)</td>              
       <td rowspan="2" style="width: 30%;border:1px solid black;font-family: tahoma;text-align: center;">KETERANGAN</td>              
      </tr>            
      <tr>                                          
       <td style="width: 10%;border:1px solid black;font-family: tahoma;text-align: center;">BAIK</td>         
       <td style="width: 10%;border:1px solid black;font-family: tahoma;text-align: center;">RUSAK</td>                       
      </tr>
      <?php
      $no = 1;
      if (!empty($data_wp['apd'])) {
       foreach ($data_wp['apd'] as $alt) {
        ?>
        <tr>
         <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;"><?php echo $no++ ?></td>
         <td style="border:1px solid black;font-family: tahoma;text-align: center;"><?php echo $alt['nama_alat'] ?></td>
         <td style="border:1px solid black;font-family: tahoma;text-align: center;"><?php echo $alt['ketersediaan'] ?></td>
         <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
         <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
         <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        </tr>
        <?php
       }
      } else {
       ?>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>            
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>            
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>            
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>            
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>            
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>            
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
       <tr>
        <td style="border:1px solid black;font-family: tahoma; text-align: center;width: 5%;">&nbsp;&nbsp;&nbsp;</td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>
        <td style="border:1px solid black;font-family: tahoma;text-align: center;"></td>              
       </tr>
      <?php } ?>
     </table>
    </div>        
    <br/>
    <div style="text-align: left;margin-left: 2%;font-family: tahoma;">          
     <table style="width: 98%;" border="1">
      <tr>
       <td style="font-family: tahoma;text-align: center;"><b>NO</b></td>
       <td style="font-family: tahoma;text-align: center;"><b>Check List of Activity</b></td>
       <td style="font-family: tahoma;text-align: center;"><b>Ya</b></td>
       <td style="font-family: tahoma;text-align: center;"><b>Tidak</b></td>
      </tr>
      <tr style="">
       <td style="font-family: tahoma;">1</td>
       <td style="font-family: tahoma;">Apakah Pengawas Pekerjaan telah memastikan bahwa semua Pelaksana Pekerjaan telah menggunakan APD (Alat Pelindung Diri) ?</td>
       <td style="font-family: tahoma;"><div style="margin: 3px;border:1px solid black;width: 80px;height: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
       <td style="font-family: tahoma;"><div style="margin: 3px;border:1px solid black;width: 80px;height: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
      </tr>
      <tr style="">
       <td style="font-family: tahoma;">2</td>
       <td style="font-family: tahoma;">Apakah semua Pelaksana Pekerjaan telah memahami potensi bahaya yang akan terjadi ?</td>
       <td style="font-family: tahoma;"><div style="margin: 3px;border:1px solid black;width: 80px;height: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
       <td style="font-family: tahoma;"><div style="margin: 3px;border:1px solid black;width: 80px;height: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
      </tr>
      <tr style="">
       <td style="font-family: tahoma;">3</td>
       <td style="font-family: tahoma;">Apakah semua Pelaksanaan Pekerjaan berdasarkan prosedur/SOP/IK ?</td>
       <td style="font-family: tahoma;"><div style="margin: 3px;border:1px solid black;width: 80px;height: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
       <td style="font-family: tahoma;"><div style="margin: 3px;border:1px solid black;width: 80px;height: 80px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div></td>
      </tr>
     </table>          
    </div> 
    <div style="text-align: left;margin-left: 2%;font-family: tahoma;">          
     Mengetahui,
     <br/>
     <br/>
     <table style="width: 100%;border:none;">
      <tr style="border:none;">
       <td style="font-family: tahoma;border: none;"><b>Pengawas Pekerjaan</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>Pengawas K3</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;">                
        <?php
//                if($row->gardu_induk_id != 0 && $row->level_id == 2){
//                  echo '<b>SPV JARGI</b>';  
//                }else{
//                  echo '<b>SPV LK2</b> ';
//                }
//                if($row->level_id == 2){
//                 if($row->gardu_induk_id != 0){
//                  echo '<b>SPV GI</b>';  
//                  }else{
//                   echo '<b>SPV LK2</b> ';
//                  }                  
//                 }else{
//                  echo '<b>SPV LK2</b> ';
//                 }   
        if ($data_wp['jenis_lokasi'] != "GARDU INDUK") {
         echo '<b>SPV JARGI/LAKS K4/Admin K3</b>';
        } else {
         echo '<b>SPV GI</b>';
        }
        ?>
       </td>
      </tr>
      <tr style="border: none;">
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>(<?php echo $data_wp['pengawas_pekerjaan'] ?>)</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;padding-left: 20%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>(<?php echo $data_wp['pengawas_k3'] ?>)</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;padding-left: 20%;"><b>&nbsp;</b></td>
       <td style="font-family: tahoma;border: none;padding-top: 7%;">
        <?php
//                if($row->gardu_induk_id != 0){
//                  echo '<b>'.$row->spv_gi.'</b>';  
//                }else{
//                  echo '<b>'.$ttd_spv['nama'].'</b>';
//                }
        if ($data_wp['tipe'] == 'EKSTERNAL') {
         if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi'] == 'SUTET / SUTT') {
          echo '<b>' . $data_wp['spv_gi'] . '</b>';
         } else {
          echo '<b>' . $data_wp['paraf'][0]['nama_pegawai'] . '</b>';
         }
        } else {
				 //      echo '<b>'.$ttd_spv['nama'].'</b>';
				 if ($data_wp['jenis_lokasi'] == 'GARDU INDUK' || $data_wp['jenis_lokasi'] == 'SUTET / SUTT') {
					echo '<b>' . $data_wp['spv_gi'] . '</b>';
				 }else{
					echo '<b>' . $data_wp['paraf'][0]['nama_pegawai'] . '</b>';
				 }         
        }
        ?>
       </td>
      </tr>
     </table>          
     <br/>
     <br/>         
    </div>               
   </div>      
  </div>
  <div style="text-align: right;">
   <img src="<?php echo base_url() . 'files/berkas/paraf/' . $data_wp['paraf'][0]['nama_pegawai'] ?>" width="20px" height="20px" style="text-align: right;">
  </div>
 </body>
</html>
