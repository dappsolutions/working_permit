<div class="col-md-12">
	<h4><i class="fa fa-file-text-o"></i>&nbsp;<u>Data IBBPR</u></h4>
	<hr />
	<div class="box-body" style="margin-top: -12px;">
		<div class="table-responsive">
			<table class="table" id="tb_ibppr" style="width: 1000px;">
				<thead>
					<tr class="bg-primary-light text-white">
						<th rowspan="2">Pihak Berkepentingan Terkait</th>
						<th rowspan="2">Kegiatan</th>
						<th rowspan="2">Potensi Bahaya</th>
						<th rowspan="2">Risiko</th>
						<th rowspan="2">P/R</th>
						<th colspan="3" class="text-center">Penilaian Awal</th>
						<th rowspan="2" class="text-center">Pengendalian Risiko</th>
						<th colspan="3" class="text-center">Penilaian Akhir</th>
						<th rowspan="2" class="text-center">Status Pengendalian</th>
						<th rowspan="2" class="text-center">Penanggung Jawab</th>
						<th rowspan="2" class="text-center">Action</th>
					</tr>
					<tr class="bg-warning">
						<th>Kon</th>
						<th>Kem</th>
						<th>TR</th>
						<th>Kon</th>
						<th>Kem</th>
						<th>TR</th>
					</tr>
				</thead>
				<tbody>
					<?php $required = 'required'; ?>
					<?php if (isset($list_ibppr)) { ?>
						<?php if (!empty($list_ibppr)) { ?>
							<?php foreach ($list_ibppr as $value) { ?>
								<tr data_id="<?php echo $value['id'] ?>">
									<td>
										<!-- <textarea id="pihak" style="width: 180px;" class="form-control <?php echo $required ?>" error="Kegiatan"><?php echo $value['pihak'] ?></textarea> -->
										<input style="width: 180px;" value="<?php echo $value['pihak'] ?>" type="text" class="form-control <?php echo $required ?>" id="pihak" placeholder="Pekerja" error="Pekerja">
									</td>
									<td>
										<textarea id="kegiatan" style="width: 180px;" class="form-control <?php echo $required ?>" error="Kegiatan"><?php echo $value['kegiatan'] ?></textarea>
									</td>
									<td>
										<textarea id="sumber" style="width: 180px;" class="form-control <?php echo $required ?>" error="Sumber Mekanisme"><?php echo $value['potensi_bahaya'] ?></textarea>
									</td>
									<td>
										<textarea id="resiko" style="width: 180px;" class="form-control <?php echo $required ?>" error="Resiko"><?php echo $value['resiko'] ?></textarea>
									</td>
									<td>
										<select class="form-control <?php echo $required ?>" style="width: 120px;" error="PR" id="pr" onchange="WpEksternal.hitungNilaiAwalIbppr(this)">
											<option value="">Pilih P/R</option>
											<option value="P" <?php echo $value['pr'] == 'P' ? 'selected' : '' ?>>P</option>
											<option value="R" <?php echo $value['pr'] == 'R' ? 'selected' : '' ?>>R</option>
										</select>
									</td>
									<td>
										<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kon" id="kon" onchange="WpEksternal.hitungNilaiAwalIbppr(this)">
											<option value="">Pilih Kon</option>
											<option value="1" <?php echo $value['kon'] == '1' ? 'selected' : '' ?>>1</option>
											<option value="2" <?php echo $value['kon'] == '2' ? 'selected' : '' ?>>2</option>
											<option value="3" <?php echo $value['kon'] == '3' ? 'selected' : '' ?>>3</option>
											<option value="4" <?php echo $value['kon'] == '4' ? 'selected' : '' ?>>4</option>
											<option value="5" <?php echo $value['kon'] == '5' ? 'selected' : '' ?>>5</option>
										</select>
									</td>
									<td>
										<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kem" id="kem">
											<option value="">Pilih Kem</option>
											<option value="A" <?php echo $value['kem'] == 'A' ? 'selected' : '' ?>>A</option>
											<option value="B" <?php echo $value['kem'] == 'B' ? 'selected' : '' ?>>B</option>
											<option value="C" <?php echo $value['kem'] == 'C' ? 'selected' : '' ?>>C</option>
											<option value="D" <?php echo $value['kem'] == 'D' ? 'selected' : '' ?>>D</option>
											<option value="E" <?php echo $value['kem'] == 'E' ? 'selected' : '' ?>>E</option>
										</select>
									</td>
									<td>
										<label for="" id="tr"><?php echo $value['tr'] ?></label>
									</td>
									<td>
										<textarea id="pengendalian" style="width: 180px;" class="form-control <?php echo $required ?>" error="Pengendalian"><?php echo $value['pengendalian'] ?></textarea>
									</td>
									<td>
										<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kon" id="kon_akhir" onchange="WpEksternal.hitungNilaiAkhirIbppr(this)">
											<option value="">Pilih Kon</option>
											<option value="1" <?php echo $value['kon_after'] == '1' ? 'selected' : '' ?>>1</option>
											<option value="2" <?php echo $value['kon_after'] == '2' ? 'selected' : '' ?>>2</option>
											<option value="3" <?php echo $value['kon_after'] == '3' ? 'selected' : '' ?>>3</option>
											<option value="4" <?php echo $value['kon_after'] == '4' ? 'selected' : '' ?>>4</option>
											<option value="5" <?php echo $value['kon_after'] == '5' ? 'selected' : '' ?>>5</option>
										</select>
									</td>
									<td>
										<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kem" id="kem_akhir" onchange="WpEksternal.hitungNilaiAkhirIbppr(this)">
											<option value="">Pilih Kem</option>
											<option value="A" <?php echo $value['kem_after'] == 'A' ? 'selected' : '' ?>>A</option>
											<option value="B" <?php echo $value['kem_after'] == 'B' ? 'selected' : '' ?>>B</option>
											<option value="C" <?php echo $value['kem_after'] == 'C' ? 'selected' : '' ?>>C</option>
											<option value="D" <?php echo $value['kem_after'] == 'D' ? 'selected' : '' ?>>D</option>
											<option value="E" <?php echo $value['kem_after'] == 'E' ? 'selected' : '' ?>>E</option>
										</select>
									</td>
									<td>
										<label for="" id="tr_akhir"><?php echo $value['tr_after'] ?></label>
									</td>
									<td class="text-center" style="color:#ccc;">
										<?php echo $value['status_pengendalian'] ?>
									</td>
									<td class="text-center">
										<input type="text" placeholder="Manager Unit" id="manager_unit" style="width:140px;" value="<?php echo trim($value['penanggung_jawab']) ?>" class="form-control required" error="Manager Unit">
									</td>
									<td class="text-center">
										<i class="fa fa-trash fa-lg hover-content" onclick="WpEksternal.removeIbppr(this)"></i>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					<?php } ?>

					<?php $required = isset($list_ibppr) ? '' : 'required' ?>
					<?php $counter = 1; ?>
					<?php if ($required != '') { ?>
						<?php $counter = 3; ?>
					<?php } ?>
					<?php for ($i = 0; $i < $counter; $i++) { ?>
						<tr data_id="">
							<td>
								<input type="text" style="width: 180px;" class="form-control <?php echo $required ?>" id="pihak" placeholder="Pekerja" error="Pekerja">
							</td>
							<td>
								<textarea id="kegiatan" style="width: 180px;" class="form-control <?php echo $required ?>" error="Kegiatan"></textarea>
							</td>
							<td>
								<textarea id="sumber" style="width: 180px;" class="form-control <?php echo $required ?>" error="Sumber Mekanisme"></textarea>
							</td>
							<td>
								<textarea id="resiko" style="width: 180px;" class="form-control <?php echo $required ?>" error="Resiko"></textarea>
							</td>
							<td>
								<select class="form-control <?php echo $required ?>" style="width: 120px;" error="PR" id="pr">
									<option value="">Pilih P/R</option>
									<option value="P">P</option>
									<option value="R">R</option>
								</select>
							</td>
							<td>
								<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kon" id="kon" onchange="WpEksternal.hitungNilaiAwalIbppr(this)">
									<option value="">Pilih Kon</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
							</td>
							<td>
								<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kem" id="kem" onchange="WpEksternal.hitungNilaiAwalIbppr(this)">
									<option value="">Pilih Kem</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
									<option value="D">D</option>
									<option value="E">E</option>
								</select>
							</td>
							<td>
								<label for="" id="tr"></label>
							</td>
							<td>
								<textarea id="pengendalian" style="width: 180px;" class="form-control <?php echo $required ?>" error="Pengendalian"></textarea>
							</td>
							<td>
								<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kon" id="kon_akhir" onchange="WpEksternal.hitungNilaiAkhirIbppr(this)">
									<option value="">Pilih Kon</option>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
								</select>
							</td>
							<td>
								<select class="form-control <?php echo $required ?>" style="width: 120px;" error="Kem" id="kem_akhir" onchange="WpEksternal.hitungNilaiAkhirIbppr(this)">
									<option value="">Pilih Kem</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
									<option value="D">D</option>
									<option value="E">E</option>
								</select>
							</td>
							<td>
								<label for="" id="tr_akhir"></label>
							</td>
							<td class="text-center" style="color:#ccc;">
								Acceptable/OK
							</td>
							<td class="text-center">
								<input type="text" placeholder="Manager Unit" id="manager_unit" value="<?php echo isset($penanggung_jawab_pekerjaan) ? $penanggung_jawab_pekerjaan : '' ?>" style="width:140px;" class="form-control <?php echo $required ?>" error="Manager Unit">
								<!-- Manager Unit -->
							</td>
							<td class="text-center">
								<i class="fa fa-plus fa-lg hover-content" onclick="WpEksternal.addIbppr(this)"></i>
							</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- /.box-footer -->
</div>