<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Working Permit | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/square/blue.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>files/img/_logo.png">
</head>

<body class="hold-transition login-page" style="background: url('<?php echo base_url() . 'assets/images/tower_3.jpg' ?>') !important; background-size:cover !important;">
  <div class="" style="width: 100%; height: 100%;position: fixed;z-index: -1;top: 0; left: 0;opacity: .5;">

  </div>
  <br>
  <!-- <div class='row'>
  <div class='col-md-12 text-right' style="padding-right:100px;">
   <button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.pendaftaranUserSwa()">PENDAFTARAN USER SWA</button>
   &nbsp;
   <button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.monitoring()">MONITORING</button>
   &nbsp;
   <button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.contactPerson()">CONTACT PERSON</button>
  </div>
 </div> -->
  <div class="row">
    <div class="col-md-7">
      <div class="login-box">
        <div class="login-logo">
          <span><img src="<?php echo base_url() . 'files/img/_logo.png' ?>" /></span> <a href="<?php echo base_url() . 'login' ?>" style="color:orange;"><b>Working</b>Permit</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
          <p class="login-box-msg">Masuk Aplikasi</p>

          <div class="row">
            <div class="col-md-12">
              <form action="" method="post">
                <div class="form-group has-feedback">
                  <input type="text" class="form-control required" error="Username" placeholder="Username" id="username">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" class="form-control required" error="Password" placeholder="Password" id="password">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                  <div class="col-xs-8">
                    &nbsp;
                  </div>
                  <!-- /.col -->
                  <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" onclick="Login.sign_in(this, event)" style="background: #00a2b9">Masuk</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>
            </div>
          </div>
          <!-- /.social-auth-links -->
          <br />
          <!--<a href="#">Lupa Password</a><br>-->
          <hr />
          <hr style="background-color: #000;">
          <div class="text-center">
            <label for="" class="text-info">PT PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur dan Bali</label>
          </div>
        </div>
        <!-- /.login-box-body -->
      </div>
    </div>
    <div class="col-md-5">
      <div style="padding: 16px;">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h5>List Buku Petunjuk dan Aplikasi Mobile</h5>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><a href="<?php echo base_url() . 'register' ?>" class="text-center label label-primary">Registrasi</a></td>
                        <td><a href="<?php echo base_url() . 'files/prosesdur_izin_kerja.pdf' ?>" class="text-center label label-primary">Prosesdur Ijin Kerja</a></td>
                        <td><a href="<?php echo base_url() . 'files/WP_Online_manual_book.pdf' ?>" class="text-center label label-primary">Buku Petunjuk WP</a></td>
                      </tr>
                      <tr>
                        <td><a href="<?php echo base_url() . 'files/apps/SWA14.apk' ?>" class="text-center label label-primary">Aplikasi SWA Versi 14</a></td>
                        <td><a href="<?php echo base_url() . 'files/apps/NOTIFWPV3.apk' ?>" class="text-center label label-primary">Aplikasi Notifikasi WP</a></td>
                        <td><a href="<?php echo base_url() . 'files/SWA_manual_book.pdf' ?>" class="text-center label label-primary">Buku Petunjuk SWA</a></td>
                      </tr>
                      <tr>
                        <td colspan="2"><a href="<?php echo base_url() . 'files/apps/SIMSONV13.apk' ?>" class="text-center label label-primary">Aplikasi Simson (Safety Briefing) Versi 13</a></td>
                        <td colspan="1"><a href="<?php echo base_url() . 'files/Usebook_Simson.pdf' ?>" class="text-center label label-primary">Buku Petunjuk Aplikasi Simson</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- <div class="row">
                                                        <div class="col-md-3 "><a href="<?php echo base_url() . 'register' ?>" class="text-center label label-primary">Registrasi</a></div>
                                                        <div class="col-md-5 "><a href="<?php echo base_url() . 'files/prosesdur_izin_kerja.pdf' ?>" class="text-center label label-primary">Prosesdur Ijin Kerja</a></div>
                                                        <div class="col-md-4 "><a href="<?php echo base_url() . 'files/WP_Online_manual_book.pdf' ?>" class="text-center label label-primary">Buku Petunjuk WP</a></div>
                                                </div> -->
            <!-- <div class="row">
                                                        <div class="col-md-3 "><a href="<?php echo base_url() . 'files/apps/SWAV4.apk' ?>" class="text-center label label-primary">Aplikasi SWA</a></div>
                                                        <div class="col-md-3 "><a href="<?php echo base_url() . 'files/apps/NOTIFWPV3.apk' ?>" class="text-center label label-primary">Aplikasi Notifikasi WP</a></div>
                                                </div>
                                                <div class="row">
                                                        <div class="col-md-6 "><a href="<?php echo base_url() . 'files/SWA_manual_book.pdf' ?>" class="text-center label label-primary">Buku Petunjuk SWA</a></div>
                                                </div> -->
          </div>
        </div>
      </div>

      <div style="padding: 16px;">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h5>Menu</h5>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.pendaftaranUserSwa()">PENDAFTARAN USER SWA K3L</button></td>
                        <!-- <td><button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.pendaftaranUserSimson()">PENDAFTARAN USER SIMSON</button></td> -->
                        <td><button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.monitoring()">MONITORING</button></td>
                        <td><button class='btn btn-primary btn-flat' style="background: #00a2b9" onclick="Login.contactPerson()">CONTACT PERSON</button></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <br />
  <!-- /.login-box -->

  <!-- jQuery 3 -->
  <script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/icheck.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
  <script src="<?php echo base_url() ?>assets/js/url.js"></script>
  <script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>
  <script src="<?php echo base_url() ?>assets/js/message.js"></script>
  <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
  <script src="<?php echo base_url() ?>assets/js/controllers/login_v2.js?v=2"></script>
  <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  </script>
</body>

</html>