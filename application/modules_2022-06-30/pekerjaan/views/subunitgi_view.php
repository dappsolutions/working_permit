<div class="box">
	<div class="box-body">
		<div class="row">
			<div class="col-md-12">
				<h4><?php echo $ultg == '' ? '' : $ultg; ?></h4>
				<br>
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Gardu Induk</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php if (!empty($data)) { ?>
								<?php foreach ($data as $value) { ?>
									<tr>
										<td><?php echo $value['nama_gardu'] ?></td>
										<td>
											<a href="<?php echo base_url() . $module . '/subunitsutet/' . $value['id'] ?>" class="btn btn-primary"><i class="fa fa-archive"></i> &nbsp;SUTT/SUTET</a>
											&nbsp;
											<a href="<?php echo base_url() . $module . '/event/' . $value['id'] . '/gi' ?>" class="btn btn-warning"><i class="fa fa-archive"></i> &nbsp;Pekerjaan</a>
										</td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr>
									<td colspan="2">Tidak ada ditemukan</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
