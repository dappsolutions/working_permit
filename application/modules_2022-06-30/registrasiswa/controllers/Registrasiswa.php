<?php

class Registrasiswa extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;
	public $akses;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 25;
		$this->akses = $this->session->userdata('hak_akses');
	}

	public function getModuleName()
	{
		return 'registrasiswa';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/registrasiswa.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'pegawai';
	}

	public function getRootModule()
	{
		return "Permohonan";
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = $this->getRootModule() . " - Registrasi User Swa";
		$data['title_content'] = 'Registrasi User Swa';
		$content = $this->getDataRegistrasi();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalDataRegistrasi($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('pg.nama', $keyword),
				array('pg.email', $keyword),
				array('pg.no_hp', $keyword),
			);
		}

		$where = "pg.deleted = 0 and pt.tipe = 'K3'";
		if ($this->akses == 'superadmin') {
			$where = "pg.deleted = 0 and pt.tipe = 'K3'";
		}

		switch ($keyword) {
			case "":
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' pg',
					'field' => array('pg.*', 'raa.approveby', 'raa.rejectedby', 'ut.nama as nama_upt'),
					'join' => array(
						array('user u', 'u.pegawai = pg.id', 'left'),
						array('upt ut', 'ut.id = pg.upt', 'left'),
						array('role_apps_akses raa', 'u.id = raa.user and raa.apps = 3', 'left'),
						array('pegawai_type pt', 'pt.id = pg.pegawai_type', 'left'),
					),
					'is_or_like' => true,
					'like' => $like,
					'where' => $where,
					'orderby' => 'pg.id desc'
				));
				break;
			default:
				$total = Modules::run('database/count_all', array(
					'table' => $this->getTableName() . ' pg',
					'field' => array('pg.*', 'raa.approveby', 'raa.rejectedby', 'ut.nama as nama_upt'),
					'join' => array(
						array('user u', 'u.pegawai = pg.id', 'left'),
						array('upt ut', 'ut.id = pg.upt', 'left'),
						array('role_apps_akses raa', 'u.id = raa.user and raa.apps = 3', 'left'),
						array('pegawai_type pt', 'pt.id = pg.pegawai_type', 'left'),
					),
					'is_or_like' => true,
					'like' => $like,
					'inside_brackets' => true,
					'where' => $where,
					'orderby' => 'pg.id desc'
				));
				break;
		}

		return $total;
	}

	public function getDataRegistrasi($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('pg.nama', $keyword),
				array('pg.email', $keyword),
				array('pg.no_hp', $keyword),
			);
		}

		$where = "pg.deleted = 0 and pt.tipe = 'K3'";
		if ($this->akses == 'superadmin') {
			$where = "pg.deleted = 0 and pt.tipe = 'K3'";
		}


		switch ($keyword) {
			case "":
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' pg',
					'field' => array('pg.*', 'raa.approveby', 'raa.rejectedby', 'ut.nama as nama_upt'),
					'join' => array(
						array('user u', 'u.pegawai = pg.id', 'left'),
						array('upt ut', 'ut.id = pg.upt', 'left'),
						array('role_apps_akses raa', 'u.id = raa.user and raa.apps = 3', 'left'),
						array('pegawai_type pt', 'pt.id = pg.pegawai_type', 'left'),
					),
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'where' => $where,
					'orderby' => 'pg.id desc'
				));
				break;
			default:
				$data = Modules::run('database/get', array(
					'table' => $this->getTableName() . ' pg',
					'field' => array('pg.*', 'raa.approveby', 'raa.rejectedby', 'ut.nama as nama_upt'),
					'join' => array(
						array('user u', 'u.pegawai = pg.id', 'left'),
						array('upt ut', 'ut.id = pg.upt', 'left'),
						array('role_apps_akses raa', 'u.id = raa.user and raa.apps = 3', 'left'),
						array('pegawai_type pt', 'pt.id = pg.pegawai_type', 'left'),
					),
					'like' => $like,
					'is_or_like' => true,
					'limit' => $this->limit,
					'offset' => $this->last_no,
					'inside_brackets' => true,
					'where' => $where,
					'orderby' => 'pg.id desc'
				));
				break;
		}

		// echo '<pre>';
		// echo $this->db->last_query();
		// die;

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataRegistrasi($keyword)
		);
	}

	public function getDetailDataRegistrasi($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' pg',
			'field' => array('pg.*', 'raa.approveby', 'raa.rejectedby', 'ut.nama as nama_upt', 'raa.keterangan_reject', 'u.username', 'u.password'),
			'join' => array(
				array('user u', 'u.pegawai = pg.id', 'left'),
				array('upt ut', 'ut.id = pg.upt', 'left'),
				array('role_apps_akses raa', 'u.id = raa.user and raa.apps = 3', 'left'),
				array('pegawai_type pt', 'pt.id = pg.pegawai_type', 'left'),
			),
			'where' => "pg.id = '" . $id . "'"
		));

		$data = $data->row_array();
		return $data;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Registrasi";
		$data['title_content'] = 'Tambah Registrasi';
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataRegistrasi($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Registrasi";
		$data['title_content'] = 'Ubah Registrasi';
		echo Modules::run('template', $data);
	}


	public function detail($id)
	{
		$data = $this->getDetailDataRegistrasi($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Registrasi User SWA";
		$data['title_content'] = "Detail Registrasi User SWA";
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getPostDataRegistrasi($value)
	{
		$data['nama'] = $value->nama;
		return $data;
	}

	public function createUser()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		$user = $this->session->userdata('user_id');

		$is_valid = false;


		// echo '<pre>';
		// print_r($data);
		// die;

		$password = substr(str_shuffle('1234567890abcdefghijklmn'), 0, 7);
		//  echo $password;die;
		$this->db->trans_begin();
		try {
			$data_pengajuan = $this->getDetailDataRegistrasi($id);
			// echo '<pre>';
			// print_r($data_pengajuan);
			// die;
			$post = array();
			$post['user'] = $user;
			$actor = Modules::run('database/_insert', 'actor', $post);

			//created user
			$post_user['username'] = $data_pengajuan['email'];
			$post_user['password'] = $password;
			$post_user['pegawai'] = $id;
			$user_id = Modules::run('database/_insert', 'user', $post_user);

			//created role apps user
			$vendor_statud['user'] = $user_id;
			$vendor_statud['apps'] = 3;
			$vendor_statud['approveby'] = $actor;
			Modules::run('database/_insert', 'role_apps_akses', $vendor_statud);

			//send sms to id registration
			$post_send['email'] = $data_pengajuan['email'];
			$post_send['no_hp'] = $data_pengajuan['no_hp'];
			$post_send['password'] = $password;
			$post_send['username'] = $data_pengajuan['email'];
			Modules::run('purpose/sendNotifToVendorRegister', $post_send);
			//  echo '<pre>';
			//  print_r($data_send);die;
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function rejectPengajuan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		$user = $this->session->userdata('user_id');
		$is_valid = false;

		// echo '<pre>';
		// print_r($data);
		// die;
		$password = substr(str_shuffle('1234567890abcdefghijklmn'), 0, 7);

		$this->db->trans_begin();
		try {
			$data_pengajuan = $this->getDetailDataRegistrasi($id);
			// echo '<pre>';
			// print_r($data_pengajuan);
			// die;
			$post = array();
			$post['user'] = $user;
			$actor = Modules::run('database/_insert', 'actor', $post);

			//created user
			$post_user['username'] = $data_pengajuan['email'];
			$post_user['password'] = $password;
			$post_user['pegawai'] = $id;
			$user_id = Modules::run('database/_insert', 'user', $post_user);

			//created role apps user
			$vendor_statud['user'] = $user_id;
			$vendor_statud['apps'] = 3;
			$vendor_statud['rejectedby'] = $actor;
			$vendor_statud['keterangan_reject'] = $_POST['keterangan'];
			Modules::run('database/_insert', 'role_apps_akses', $vendor_statud);

			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Registrasi";
		$data['title_content'] = 'Data Registrasi';
		$content = $this->getDataRegistrasi($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function uploadData($name_of_field)
	{
		$config['upload_path'] = 'files/berkas/taruna/';
		$config['allowed_types'] = 'png|jpg';
		$config['max_size'] = '1000';
		$config['max_width'] = '2000';
		$config['max_height'] = '2000';

		$this->load->library('upload', $config);
		$this->upload->do_upload($name_of_field);
	}

	public function showLogo()
	{
		$foto = str_replace(' ', '_', $this->input->post('foto'));
		$data['foto'] = $foto;
		echo $this->load->view('foto', $data, true);
	}

	public function approve($id)
	{
		$data['id'] = $id;
		echo $this->load->view('form_user', $data, true);
	}

	public function reject($id)
	{
		$data['id'] = $id;
		echo $this->load->view('form_reject', $data, true);
	}
}
