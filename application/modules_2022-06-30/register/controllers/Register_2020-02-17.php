<?php

class Register extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'register';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/controllers/register.js"></script>',
  );

  return $data;
 }

 public function getTableName() {
  return "pengajuan_vendor";
 }

 public function index() {
  $data['view_file'] = 'index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Register";
  $data['title_content'] = 'Register';
  $data['content'] = $this->getDataVendor();
  echo $this->load->view('index', $data, true);
 }

 public function getDataVendor() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' pv',
              'field' => array('pv.*', 'sv.status'),
              'join' => array(
                  array('vendor v', 'pv.id = v.pengajuan_vendor', 'left'),
                  array('(select max(id) id, pengajuan_vendor from status_pengajuan_vendor group by pengajuan_vendor) pvv', 'pvv.pengajuan_vendor = pv.id'),
                  array('status_pengajuan_vendor sv', 'pvv.id = sv.id'),
              ),
              'where' => "pv.deleted = 0",
              'orderby' => 'pv.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function registerVendor() {
  $data = $_POST;
  $data['no_pengajuan'] = Modules::run("no_generator/generateNoPengajuanVendor");

  $is_valid = false;
  $this->db->trans_begin();
  try {
   $id = Modules::run('database/_insert', $this->getTableName(), $data);
   //DRAFT
   $vendor_statud['status'] = 'DRAFT';
   $vendor_statud['pengajuan_vendor'] = $id;
   Modules::run('database/_insert', 'status_pengajuan_vendor', $vendor_statud);
   $this->db->trans_commit();

   //send sms to admin lk2 induk
   Modules::run('purpose/sendSmsToAdminUnitVendorRegister');
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  echo json_encode(array('is_valid' => $is_valid));
 }

}
